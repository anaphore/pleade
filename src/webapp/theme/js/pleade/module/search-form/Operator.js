//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Ensemble de fonctions qui permettent de gerer l'affichage des operateurs intra-champ selon le nombre de selections courantes
 * @author Julien Huet
 */
/**
 * @class Ensemble de fonctions qui permettent de gerer l'affichage des operateurs intra-champ selon le nombre de selections courantes
 * @author Julien Huet
 */
var Operator = new Class(/** @lends Operator */{
	
	/**
	* <strong>Constructor</strong><br/>
	* Fonction permettant de gérer les différents opérateurs et leurs affichages 
	*/
	initialize: function(){},

	/**
	* Fonction appelée au chargement de la page pour initialiser
	* l'affichage (ou non) des operateurs intra-champ les listes
	* deroulantes a selection multiple
	*/
	init_dyn_display_op_select : function(){
		// mettre a jour l'affichage des operateurs intra-champ pour les select
		this.update_all_display_op_select();
		var _this = this;
		// pour tous les select avec class="dyn_display_op" on initialise le onchange pour afficher les operateurs intra-champs
		$$('select.dyn_display_op_select').each(function(select){
		 	var n = select.name.replace(/\query/g, '');
			select.addEvent('change', function(){_this.update_display_op_select(n, this)} );
		});
	},

	/**
	* Fonction appelée au chargement de la page pour initialiser
	* l'affichage (ou non) des operateurs intra-champ des listes
	* type checkbox (avec @op-display=true)
	*/
	init_dyn_display_op_checkbox: function(){
		// mettre a jour l'affichage des operateurs intra-champ pour les checkbox
		this.update_all_display_op_checkboxList();
		var _this = this;
		// pour tous les span avec class="dyn_display_op_checkbox" on initialise le onchange pour afficher les operateurs intra-champs
		$$('span.dyn_display_op_checkbox').each(function(span){
			var n = span.id.replace(/checkbox_container/g, '');
			span.getElements('input.pl-form-chckbx').each(function(checkbox){
				checkbox.addEvent('change', function(){ _this.update_display_op_checkboxList(n) } );
			});
		});
	},
	
	/** Mise a jour de l'affichage des operateurs intra-champ pour tous les checkbox */
	update_all_display_op_checkboxList : function(){
		var _this = this;
		//alert("IntraFieldOperator.js update_all_display_op_checkboxList()");
		$$('span.dyn_display_op_checkbox').each(function(span){
			var n = span.id.replace(/checkbox_container/g, '');
			_this.update_display_op_checkboxList(n);
		});
	},

	/** Mise a jour de l'affichage des operateurs intra-champ pour les checkbox du champ "n" */
	update_display_op_checkboxList : function(n){
		var span = $('checkbox_container' + n);
		var opsid = 'ops' + n;
		var countSelected = 0;
		span.getElements('input.pl-form-chckbx').each(function(checkbox){
			if ( checkbox.checked && !checkbox.value.blank() ) {
				countSelected++;
			}
		});
		if ( countSelected >= 2 ) {
			$(opsid).setStyle('display', 'block');
		} else {
			$(opsid).setStyle('display', 'none');
		}
	},

	/**
	* Mise a jour de l'affichage des operateurs intra-champ
	* pour toutes les listes déroulantes caracterisée par la
	* classs "dyn_display_op_select_multiple"
	*/
	update_all_display_op_select: function(){
		// pour les select multiple
		var _this = this;
		$$('select.dyn_display_op_select').each(function(select){
			var n = select.name.replace(/\query/g, '');
			var opsid = 'ops'+n
			_this.update_display_op_select(n, select);
		});
	},

	/**
	* Mise a jour de l'affichage des operateurs intra-champ
	* pour tous les champs de type suggest caracterisé par
	* la classe "dyn_display_op_suggest"
	*/
	update_all_display_op_suggest : function(){
		var _this = this;
		$$('input.dyn_display_op_suggest').each(function(select){
			var n = select.name.replace(/\query/g, '');
			_this.update_display_op_suggest(n);
		});
	},

	/**
	* Mise a jour de l'affichage des operateurs intra-champ pour
	* tous les champs de type "multiple combo" caracterisé par 
	* la classe "dyn_display_op_multiple_combo"
	*/
	update_all_display_op_multiple_combo: function(){
		var _this = this;
		$$('select.dyn_display_op_multiple_combo').each(function(select){
			var n = select.name.replace(/\query/g, '');
			_this.update_display_op_multiple_combo(n);
		});
	},

	/**
	* Mise a jour de l'affichage des operateurs intra-champ
	* d'une liste déroulante a selection multiple
	*/
	update_display_op_select: function(n, select){
		var opsid = 'ops' + n;
		var countSelected = 0;
		$A(select.options).each(function(o,i){
			if ( o.selected && !o.value.blank() ) {
				countSelected++
			}
		});
		if ( countSelected >= 2 ) {
			$(opsid).setStyle('display', 'block');
		} else {
			$(opsid).setStyle('display', 'none');
		}
	},
	
	/** Mise a jour de l'affichage des operateurs intra-champ d'un champ de type suggest */
	update_display_op_suggest: function(n){
		var opsid = 'ops' + n;
		var idv = 'query' + n + '-values';
		var suggestsValuesEl = $(idv);
		var countSelected = suggestsValuesEl.getChildren().length;
		if ( countSelected >= 2 ) {
			$(opsid).setStyle('display', 'block');
		} else {
			$(opsid).setStyle('display', 'none');
		}
	},
	
	/** Mise a jour de l'affichage des operateurs intra-champ d'un champ de type "multiple combo" */
	update_display_op_multiple_combo: function(n){
		var opsid = 'ops' + n;
		var idv = 'query' + n + '-values';
		var suggestsValuesEl = $(idv);
		var countSelected = suggestsValuesEl.getChildren().length;
		if ( countSelected >= 2 ) {
			$(opsid).setStyle('display', 'block');
		} else {
			$(opsid).setStyle('display', 'none');
		}
	}
});
