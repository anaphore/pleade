// $Id: CQLPrefix.java 19706 2010-11-25 16:52:20Z jcwiklinski $

package org.sru.cql;
import java.lang.String;

/**
 * Represents a CQL prefix mapping from short name to long identifier.
 *
 * @version	$Id: CQLPrefix.java 19706 2010-11-25 16:52:20Z jcwiklinski $
 */
public class CQLPrefix {
    /**
     * The short name of the prefix mapping.  That is, the prefix
     * itself, such as <TT>dc</TT>, as it might be used in an index
     * like <TT>dc.title</TT>.
     */
    public String name;

    /**
     * The full identifier name of the prefix mapping.  That is,
     * typically, a URI permanently allocated to a specific index
     * set, such as <TT>http://zthes.z3950.org/cql/1.0<TT>.
     */
    public String identifier;

    /**
     * Creates a new CQLPrefix mapping, which maps the specified name
     * to the specified identifier.
     */
    CQLPrefix(String name, String identifier) {
	this.name = name;
	this.identifier = identifier;
    }
}
