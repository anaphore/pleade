<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: edit-get-logs2xhtml.xsl 15636 2009-07-08 14:29:23Z mpichot $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		| Administration du module EAD-EAC
    | Exporter les logs de publication
    | Cette XSL contient les numéros de colonnes. Elle est importée par les XSL
    | chargée de faire l'export.
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		xmlns:csv="http://apache.org/cocoon/csv/1.0"
		exclude-result-prefixes="xsl i18n xs csv">

<!--
<?xml version="1.0" encoding="UTF-8"?>
<csv:document xmlns:csv="http://apache.org/cocoon/csv/1.0">
    <csv:record number="9">
        <csv:field number="1">FATAL_E</csv:field>
        <csv:field number="2">(2009-09-21) 13:49.05:883</csv:field>
        <csv:field number="3">1253533745843</csv:field>
        <csv:field number="4">ead</csv:field>
        <csv:field number="5">8</csv:field>
        <csv:field number="6">2009-09-21T13:49:05.843 CEST</csv:field>
        <csv:field number="8">INDEXATION_INIT</csv:field>
        <csv:field number="9">1</csv:field>
    </csv:record>
    <csv:record number="10">
        <csv:field number="1">FATAL_E</csv:field>
        <csv:field number="2">(2009-09-21) 13:49.42:329</csv:field>
        <csv:field number="3">1253533745843</csv:field>
        <csv:field number="4">ead</csv:field>
        <csv:field number="5">8</csv:field>
        <csv:field number="6">2009-09-21T13:49:05.843 CEST</csv:field>
        <csv:field number="8">MERGE_BATCH_INDEX</csv:field>
        <csv:field number="9">12</csv:field>
        <csv:field number="10">8</csv:field>
        <csv:field number="11">2009-09-21T13:49:39.827 CEST</csv:field>
        <csv:field number="12">2009-09-21T13:49:42.329 CEST</csv:field>
        <csv:field number="13">FRAD064_IR0121</csv:field>
        <csv:field number="15">1</csv:field>
    </csv:record>
    <csv:record number="11">
        <csv:field number="1">FATAL_E</csv:field>
        <csv:field number="2">(2009-09-21) 13:49.43:392</csv:field>
        <csv:field number="3">1253533745843</csv:field>
        <csv:field number="4">ead</csv:field>
        <csv:field number="5">8</csv:field>
        <csv:field number="6">2009-09-21T13:49:05.843 CEST</csv:field>
        <csv:field number="8">OPTIMIZE_DOCUMENT_BASE</csv:field>
        <csv:field number="9">13</csv:field>
        <csv:field number="10">8</csv:field>
        <csv:field number="11">2009-09-21T13:49:39.827 CEST</csv:field>
        <csv:field number="12">2009-09-21T13:49:42.329 CEST</csv:field>
        <csv:field number="13">FRAD064_IR0121</csv:field>
        <csv:field number="15">1</csv:field>
    </csv:record>
    <csv:record number="12">
        <csv:field number="1">FATAL_E</csv:field>
        <csv:field number="2">(2009-09-21) 13:49.43:445</csv:field>
        <csv:field number="3">1253533745843</csv:field>
        <csv:field number="4">ead</csv:field>
        <csv:field number="5">8</csv:field>
        <csv:field number="6">2009-09-21T13:49:05.843 CEST</csv:field>
        <csv:field number="7">2009-09-21T13:49:43.445 CEST</csv:field>
        <csv:field number="8">INDEXATION_END</csv:field>
        <csv:field number="9">2</csv:field>
        <csv:field number="10">8</csv:field>
        <csv:field number="11">2009-09-21T13:49:39.827 CEST</csv:field>
        <csv:field number="12">2009-09-21T13:49:42.329 CEST</csv:field>
        <csv:field number="15">1</csv:field>
    </csv:record>
    <csv:record number="13">
        <csv:field number="1">FATAL_E</csv:field>
        <csv:field number="2">(2009-09-21) 13:49.43:446</csv:field>
        <csv:field number="3">1253533745843</csv:field>
        <csv:field number="4">ead</csv:field>
        <csv:field number="5">8</csv:field>
        <csv:field number="6">2009-09-21T13:49:05.843 CEST</csv:field>
        <csv:field number="7">2009-09-21T13:49:43.445 CEST</csv:field>
        <csv:field number="8">DOCS_IDS</csv:field>
        <csv:field number="9">8</csv:field>
        <csv:field number="10">0</csv:field>
        <csv:field number="11">{2=FRAD064_IR0130, 1=FRAD064003_IR0031, 8=FRAD064_IR0121, 7=FRAD064_IR0122, 6=FRAD064_IR0123, 5=FRAD064_IR0124, 4=FRAD064_IR0126, 3=FRAD064_IR0127}</csv:field>
        <csv:field number="12">{}</csv:field>
    </csv:record>
    <csv:record number="14">
        <csv:field number="1">FATAL_E</csv:field>
        <csv:field number="2">(2009-09-21) 13:49.43:446</csv:field>
        <csv:field number="3">1253533745843</csv:field>
        <csv:field number="4">ead</csv:field>
        <csv:field number="5">8</csv:field>
        <csv:field number="6">2009-09-21T13:49:05.843 CEST</csv:field>
        <csv:field number="7">2009-09-21T13:49:43.445 CEST</csv:field>
        <csv:field number="8">ERROR_MESSAGES</csv:field>
        <csv:field number="9">{}</csv:field>
    </csv:record>
</csv:document>
-->

	<xsl:variable name="i18n-root" select="string('ead-admin.export-publication.txt')" />
  <xsl:variable name="i18n-cat" select="string('module-eadeac-edit')" />
  
  <!-- Les numéros des colonnes.
  On commence par regarder si le niveau de verbosité des logs est actif (cf. 
  colonne 1). Suivant s'il est actif ou non, les colonnes peuvent avoir un 
  décalage.-->
  <xsl:variable name="colID" select="
    if(/csv:document/csv:record[1]/csv:field[@number='1' and .='FATAL_E']) then '3'
    else '2'
  "/>
  <xsl:variable name="colStarts" select="
    if($colID='3') then '6'
    else '5'
  "/>
  <xsl:variable name="colStep" select="
    if($colID='3') then '8'
    else '7'
  "/>
  <xsl:variable name="colEnds" select="
    if($colID='3') then '7'
    else '6'
  "/>
  <xsl:variable name="colNbDocs" select="
    if($colID='3') then '5'
    else '4'
  "/>
  <xsl:variable name="colDBID" select="
    if($colID='3') then '4'
    else '3'
  "/>  
  <xsl:variable name="colNbDocsAdded" select="
    if($colID='3') then '9'
    else '8'
  "/>
  <xsl:variable name="colNbDocsError" select="
    if($colID='3') then '10'
    else '9'
  "/>
  <xsl:variable name="colDocsAddedID" select="
    if($colID='3') then '11'
    else '10'
  "/>
  <xsl:variable name="colDocsErrorID" select="
    if($colID='3') then '12'
    else '11'
  "/>
  <xsl:variable name="colErrorMessages" select="
    if($colID='3') then '9'
    else '8'
  "/>

</xsl:stylesheet>
