//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion des messages d'attente
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion des messages d'attente
 * @augments Loading
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var LoadingMessage = new Class( /** @lends LoadingMessage.prototype */ {
		Extends: Loading,

		/**
		* Fonction qui permet de creer le div qui contiendra le message
		* @private
		*/
		initLoadingDiv: function(){
			return new Element('div', { 'class': 'loadingMessage' });
		}
});