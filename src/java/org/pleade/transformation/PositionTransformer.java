/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
 */
package org.pleade.transformation;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Stack;

import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.transformation.Transformer;
import org.pleade.utils.EadUtilities;
import org.pleade.utils.EacUtilities;
import org.pleade.utils.PleadeUtilities;
import org.pleade.utils.TeiUtilities;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import fr.gouv.culture.sdx.pipeline.AbstractTransformation;

/**
 * Cette transformation ajoute des attributs de position a differents
 * elements d'un document EAD.
 *
 * <ul>
 * 	<li><code>pleade:pos</code> : La position hierarchique d'un composant (c, archdesc, ...)
 * 	<li><code>pleade:no</code> : Un numero unique associe a tous les elements EAD
 * </ul>
 */
public class PositionTransformer extends AbstractTransformation implements Transformer {

	/** Le nom de l'attribut de positionnement des composants. */
	public final static String POSITION_ATTRIBUTE_NAME = "pos";

	/** Le nom de l'attribut de numerotation des elements. */
	public final static String NO_ATTRIBUTE_NAME = "no";

	/** Une pile pour conserver des informations sur les composants ouverts. */
	private PositionStack positions = new PositionStack();

	/** Un entier pour compter les elements. */
	private int elementNo = 0;

	/** Le type de document source (defaut : ead). **/
	private String doctype = null;


	/**
	 * Methode appelee par Cocoon lorsque cette transformation est utilisee dans un pipeline.
	 * Pour l'instant elle ne fait rien.
	 */
	public void setup(SourceResolver resolver, Map map, String src, Parameters params) throws ProcessingException, SAXException, IOException {
	}


	/**
	 * Debut du document, on reinitialise les variables globales.
	 */
	public void startDocument() throws SAXException {
		this.elementNo = 0;
		this.positions = new PositionStack();
		// On passe la main
		super.startDocument();
	}
	/**
	 * Un element XML. On ajoute un attribut pleade:no et un attribut
	 * pleade:pos s'il s'agit d'un composant.
	 */
	public void startElement(String uri, String loc, String raw, Attributes a) throws SAXException {

		if ( doctype==null || "".equals(doctype) && loc!=null ) {
			doctype = PleadeUtilities.getDocumentType(loc);
		}

		// On incremente le numero de l'element
		elementNo++;

		// Les attributs a sortir, construits a partir des attributs existants
		AttributesImpl newAtts = new AttributesImpl(a);

		// Ajout de l'attribut no
		newAtts.addAttribute(PleadeUtilities.PLEADE_NAMESPACE_URI, NO_ATTRIBUTE_NAME, PleadeUtilities.PLEADE_NAMESPACE_PREFIX + ":" + NO_ATTRIBUTE_NAME, "CDATA", "" + elementNo);

		// Ajout de l'attribut pos pour les composants
		if ( ( "ead".equals(doctype) && EadUtilities.elementIsComponent(loc) )
				|| ( "eac".equals(doctype) && EacUtilities.elementIsComponent(loc) )
				|| ( "tei".equals(doctype) && TeiUtilities.elementIsComponent(loc)
              && !PleadeUtilities.isRoot(loc) ) ) // pour la TEI, on ne veut pas la racine TEI ou TEI.2 qui peut etre un simple composant dans le cas d'une racine teiCorpus
		{

			// On indique qu'on debute un composant
			positions.startComponent();

			// On ajoute l'attribut
			newAtts.addAttribute(PleadeUtilities.PLEADE_NAMESPACE_URI, POSITION_ATTRIBUTE_NAME, PleadeUtilities.PLEADE_NAMESPACE_PREFIX + ":" + POSITION_ATTRIBUTE_NAME, "CDATA", positions.getPositionInformation());
		}

		// On passe la main
		super.startElement(uri, loc, raw, newAtts);
	}

	/**
	 * Fin d'un element dans le document. On doit suivre cet evenement pour notre pile
	 * de composants ouverts.
	 */
	public void endElement(String uri, String loc, String raw) throws SAXException {

    if ( ( "ead".equals(doctype) && EadUtilities.elementIsComponent(loc) )
					|| ( "eac".equals(doctype) && EacUtilities.elementIsComponent(loc) )
          || ( "tei".equals(doctype) && TeiUtilities.elementIsComponent(loc)
              && !PleadeUtilities.isRoot(loc) ) ) // pour la TEI, on ne veut pas la racine TEI ou TEI.2 qui peut etre un simple composant dans le cas d'une racine teiCorpus
    {
      // On indique que l'on termine le composant
			positions.endComponent();
    }

		// On passe la main
		super.endElement(uri, loc, raw);
	}

	/**
	 * Un nombre entier sous la forme d'un objet, avec des methodes
	 * rapides pour incrementer et obtenir la valeur.
	 */
	private class SmallInteger {

		/** L'entier lui-meme. */
		private int i = 0;

		/** Incremente l'entier. */
		public void increment() {
			i++;
		}
		/** Retourne la valeur. */
		public int getValue() {
			return i;
		}
	}

	/**
	 * Une pile qui permet de garder la trace des composants ouverts au cours du
	 * traitement du document.
	 */
	private class PositionStack {

		/** Le format utilise pour formater les informations de positionnement. */
		private DecimalFormat df = new DecimalFormat("00000");

		/** La pile utilisee en interne. */
		private Stack s = new Stack();

		/** Creation de la pile. */
		public PositionStack() {
			// On ajoute tout de suite un element (valeur 0) dans la pile.
			s.push(new SmallInteger());
		}

		/**
		 * Indique le debut d'un composant.
		 */
		public void startComponent() {

			// On doit d'abord incrementer la position du parent.
			if ( !s.empty() ) {
				SmallInteger parent = (SmallInteger)s.peek();
				parent.increment();
			}

			// Et on ajoute un nouvel objet dans la pile, pour le prochain
			s.push(new SmallInteger());
		}

		/*
		 * Indique la fin d'un composant.
		 */
		public void endComponent() {
			// On supprime le dernier composant de la pile interne
			s.pop();
		}

		/**
		 * Formate les informations de positionnement du composant en cours
		 */
		public String getPositionInformation() {
			// La position depend des elements dans la pile, a l'exception du premier
			StringBuffer ret = new StringBuffer();
			for (int i=0; i < s.size() - 1; i++) {
				if ( i > 0 ) ret.append(".");
				SmallInteger si = (SmallInteger)s.get(i);
				ret.append(format(si.getValue()));
			}
			return ret.toString();
		}

		/** Methode pour formater un nombre entier. */
		private String format(int i) {
			return df.format(i);
		}
	}
}
