/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
 */
package org.pleade.reading;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.Context;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.reading.ResourceReader;
import org.apache.commons.io.IOUtils;
import org.apache.excalibur.source.Source;
import org.apache.excalibur.source.SourceNotFoundException;
import org.apache.excalibur.source.SourceValidity;
import org.pleade.utils.ImageInformation;
import org.pleade.utils.ImageUtilities;
import org.pleade.utils.NetUtilities;
import org.xml.sax.SAXException;

/**
 * Un reader Cocoon pour envoyer des images, transformees ou non par ImageMagick
 *
 */
public class ImageMagickReader extends ResourceReader {

	// Les parametres de configuration
	private final static String PARAMETRE_CONFIGURATION_FORMATS_SORTIE = "formats-sortie";
	private final static String PARAMETRE_CONFIGURATION_CHEMIN_IM = "imagemagick-exe";
	private final static String PARAMETRE_CONFIGURATION_CHEMIN_WK = "filigrane-url";
	private final static String PARAMETRE_CONFIGURATION_CACHE = "cache-images-transformees";
	private final static String PARAMETRE_CONFIGURATION_TIMEOUT = "timeout-image-reader";
	private static final String CONFIGURATION_NOM_FICHIER_PAS_FILIGRANE = "nom-fichier-pas-filigrane";
	private static final String CONFIGURATION_ZONES_PAS_FILIGRANE = "zones-pas-filigrane";
	private static final String CONFIGURATION_TAILLE_MINI_FILIGRANE = "taille-mini-filigrane";
	private static final String CONFIGURATION_GRAVITY_FILIGRANE = "filigrane-gravity";
	private static final String PARAMETRE_FICHIER_DESTINATION = "fichier-destination";

	// Le separateur dans la liste des types MIME
	private final static String MIMETYPE_SEPARATEUR = ";";

	// Methode resize ou crop
	private final static int METHOD_RESIZE = 0;
	private final static int METHOD_CROP = 1;
	private final static int METHOD_REFORMAT_ONLY = 2;

	private int timeout = 600000; // 1000 ms x 60 * 10 = 10 minutes par défaut

	// La source
	private String src = null;

	// Les parametres supportes
	private int x;
	private int y;
	private int h;
	private int w;
	private int p;
	private int q;
	private int r;
	private String f;
	private String wkConf; // filigrane / watermark regle dans la Sitemap
	private String wk; // filigrane / watermark regle dans l'URL
	private int ww; // la largeur en cas de filigrane
	private int wh; // La hauteur en cas de filigrane
	private String pleadeZone;

	// Les variables liees a l'interpretation des parametres
	private boolean transform = false; // transforme ou non l'image d'origine
	private boolean convert = false; // active ou non convert
	private boolean watermark = false; // active ou non l'ajout du filigrane
	private boolean reformat = false; // change ou non le format d'image
	private String finalMimeType = null;
	private Source wksrc = null; // la source du filigrane
	private Source wkConfsrc = null; // la source du filigrane regle dans la Sitemap
	private String wkGravity = null; // l'emplacement du filigrane
	private String nomFichierPasFiligrane = "nowatermark";
	private String[] zonesPasFiligrane;
	// private String paramUrlPasFiligrane = "nowatermark";
	// Les dimensions minimales pour l'application du filigrane
	private int wkmini = 150;
	private int hkmini = 150;

	//Éventuellement, le fichier de destination
	private String destFile = null;

	// Les variables suivantes sont liees a la configuration

	// Liste des types MIME supportes
	private Map mimeTypes;

	// Le chemin vers ImageMagick
	private String imPath;

	// Indique si on doit cacher les images transformees ou non (non par defaut)
	private boolean cacheImagesTransformees = false;
	//affichage des informations de deboguage
	private boolean _debug = false;

	private Logger m_logger;



	/**
	 * Configuration du reader. On doit lui passer le chemin d'acces a ImageMagick et les formats de sortie supportes.
	 */
	public void configure(Configuration configuration) throws ConfigurationException {

		// Configuration generale des reader
		super.configure(configuration);

		// Configuration specifique
		Configuration tempConfIM = configuration.getChild(PARAMETRE_CONFIGURATION_CHEMIN_IM);
		if ( tempConfIM == null ) throw new ConfigurationException("Le reader \"imagemagick\" doit etre configure avec un parametre \"" + PARAMETRE_CONFIGURATION_CHEMIN_IM + "\"");
		Configuration tempConfMIME = configuration.getChild(PARAMETRE_CONFIGURATION_FORMATS_SORTIE);
		if ( tempConfMIME == null ) throw new ConfigurationException("Le reader \"imagemagick\" doit etre configure avec un parametre \"" + PARAMETRE_CONFIGURATION_FORMATS_SORTIE + "\"");

		// La liste des types MIME supportes en sortie
		String mimeTypeList = tempConfMIME.getValue();
		if ( mimeTypeList == null ) throw new ConfigurationException("Le reader \"imagemagick\" doit etre configure avec un parametre \"" + PARAMETRE_CONFIGURATION_FORMATS_SORTIE + "\"");
		StringTokenizer st = new StringTokenizer(mimeTypeList, MIMETYPE_SEPARATEUR);
		mimeTypes = new HashMap();
		while ( st.hasMoreTokens() ) {
			String mt = st.nextToken();
			this.mimeTypes.put(mt, mt);
		}

		// On va chercher le parametre de lancement d'ImageMagick
		this.imPath = tempConfIM.getValue();

		// On verifie si on cache les images transformees ou non
		Configuration tempConf3 = configuration.getChild(PARAMETRE_CONFIGURATION_CACHE);
		if ( tempConf3 == null ) throw new ConfigurationException("Le reader \"imagemagick\" doit etre configure avec un parametre \"" + PARAMETRE_CONFIGURATION_CACHE + "\"");
		this.cacheImagesTransformees = tempConf3.getValueAsBoolean(false);

		// Ajout de filigrane programme
		try {
			// l'URL du filigrane
			tempConf3 = configuration.getChild(PARAMETRE_CONFIGURATION_CHEMIN_WK);
			if ( tempConf3==null ) this.wkConf = null;
			else this.wkConf = tempConf3.getValue();
		} catch (ConfigurationException ce) {
			// L'absence de filigrane-url n'est pas une erreur de configuration
			this.wkConf = null;
		}
		try{
			// le nom du fichier de desactivation du filigrane
			tempConf3 = configuration.getChild(CONFIGURATION_NOM_FICHIER_PAS_FILIGRANE);
			if ( tempConf3!=null && !"".equals(tempConf3.getValue()) ) this.nomFichierPasFiligrane = tempConf3.getValue();
		} catch (ConfigurationException ce) {
			// L'absence du parametre n'est pas une erreur de configuration
		}
		try {
			// le nom du parametre d'URL de desactivation du filigrane
			// tempConf3 = configuration.getChild(CONFIGURATION_PARAMETRE_URL_PAS_FILIGRANE);
			// if ( tempConf3!=null && !"".equals(tempConf3.getValue()) ) this.paramUrlPasFiligrane = tempConf3.getValue();
			// la taille minimale du filigrane
			tempConf3 = configuration.getChild(CONFIGURATION_TAILLE_MINI_FILIGRANE);
			if ( tempConf3!=null && !"".equals(tempConf3.getValue()) ) {
				String t, w, h;
				t = tempConf3.getValue();
				if ( t!=null && !"".equals(t) ) {
					int x = t.indexOf("x");
					if ( x==-1 ) {
						throw new ConfigurationException( "Le parametre du reader \"imagemagick\" indiquant la taille minimale d'application du filigrane doit etre de la forme : [largeur]x[hauteur]" );
					}
					else {
						w = t.substring( 0, x );
						h = t.substring( x );
						try {
							wkmini = new Integer (w).intValue();
							hkmini = new Integer (h).intValue();
						} catch(Exception e) {
							throw new ConfigurationException( "Le parametre du reader \"imagemagick\" indiquant la taille minimale d'application du filigrane doit etre de la forme : [largeur]x[hauteur]", e );
						}
					}
				}
			}
		} catch (ConfigurationException ce) {
			// L'absence du parametre n'est pas une erreur de configuration
		}
		// La position du filigrane si necessaire
		try {
			tempConf3 = configuration.getChild(CONFIGURATION_GRAVITY_FILIGRANE);
			if ( tempConf3==null ) this.wkGravity = null;
			else this.wkGravity = tempConf3.getValue();
		} catch (ConfigurationException ce) {
			// L'absence de filigrane-url n'est pas une erreur de configuration
			this.wkGravity = null;
		}
		//Le timeout
		try {
			tempConf3 = configuration.getChild(PARAMETRE_CONFIGURATION_TIMEOUT);
			if ( tempConf3!=null && !"".equals(tempConf3.getValue()) ) this.timeout = tempConf3.getValueAsInteger();
		} catch (ConfigurationException ce) {
			// L'absence du parametre n'est pas une erreur de configuration
		}

		// Zones pour lesquelles on ne veut pas de filigrane
		try {
			tempConf3 = configuration.getChild(CONFIGURATION_ZONES_PAS_FILIGRANE);
			if ( tempConf3 != null ) this.zonesPasFiligrane = tempConf3.getValue().split(";");
		} catch (ConfigurationException ce) {
			// L'absence de zones-pas-filigrane n'est pas une erreur de configuration
		}

		m_logger = getLogger();
		if(m_logger!=null && m_logger.isDebugEnabled()){
			m_logger.debug(
					"[IMReader] Configuration :\n\t"
					+ PARAMETRE_CONFIGURATION_CHEMIN_IM + ": " + this.imPath
					+ "\n\t" + PARAMETRE_CONFIGURATION_FORMATS_SORTIE + ": " + this.mimeTypes
					+ "\n\t"+ PARAMETRE_CONFIGURATION_CACHE + ": " + cacheImagesTransformees
					+ "\n\t"+ PARAMETRE_CONFIGURATION_CHEMIN_WK + ": " + this.wkConf
					+ "\n\t"+ CONFIGURATION_NOM_FICHIER_PAS_FILIGRANE + ": " + this.nomFichierPasFiligrane
					+ "\n\t"+ CONFIGURATION_TAILLE_MINI_FILIGRANE + ": " + this.wkmini + "x" + this.hkmini
					+ "\n\t"+ CONFIGURATION_GRAVITY_FILIGRANE + ": " + this.wkGravity
					+ "\n\t"+ CONFIGURATION_ZONES_PAS_FILIGRANE + ": " + this.zonesPasFiligrane
					+ "\n\t"+ PARAMETRE_CONFIGURATION_TIMEOUT + ": " + this.timeout
			);
		}
	}




	/**
	 * Fonction de mise en place appelee avant chaque generate().
	 */
	public void setup(SourceResolver resolver, Map model, String src, Parameters par) throws ProcessingException, SAXException, IOException {

		// Mise en place generale du ResourceReader ; on le fait a la fin car les en-tetes HTTP
		// sont sorties a ce moment, et ca depend de byteRanges
		super.setup(resolver, model, src, par);

		// On conserve la source
		this.src = src;

		// On recupere nos parametres specifiques

		// On va chercher les parametres de taille comme entiers
		x = par.getParameterAsInteger("x", -1);
		y = par.getParameterAsInteger("y", -1);
		h = par.getParameterAsInteger("h", -1);
		w = par.getParameterAsInteger("w", -1);
		r = par.getParameterAsInteger("r", 0);
		p = par.getParameterAsInteger("p", -1);
		q = par.getParameterAsInteger("q", -1);

		// L'URL du filigrane s'il est la
		wk = par.getParameter("wk", "");
		ww = par.getParameterAsInteger("ww", w); // la largeur finale du filigrane
		wh = par.getParameterAsInteger("wh", h); // la heuteur finale du filigrane

		//le fichier de destination, si applicable
		destFile = par.getParameter(PARAMETRE_FICHIER_DESTINATION, "-");
		if( destFile == null || "".equals(destFile) ) destFile = "-";
		if( !"-".equals(destFile)){
			//attention: le chemin destFile est celui vers l'image,
			//on ne veut créer uniquement les répertoires parents
			File parentdir = new File(destFile).getParentFile();
			//au besoin, on crée récursivement tous les répertoires s'ils n'existent pas
			if(!parentdir.exists()){
				boolean success = parentdir.mkdirs();
				//si la création des répertoires échoue, destFile reprend la valeur "-"
				if( !success ) destFile = "-";
			}
		}

		// La zone Pleade courante
		pleadeZone = par.getParameter("pleade-zone", "");

		// On va chercher le parametre de format
		f = par.getParameter("f", null);

		// On active l'info de debogage s'il le faut
		_debug = par.getParameterAsBoolean("debug", false);
		if (!_debug && m_logger!=null) _debug = m_logger.isDebugEnabled();

		// On ajuste pour la position x,y = (0,0) FIXME: les coordonnées d'une image commencent-elles à 0 ou 1???
		if (x == 0) x = 1;
		if (y == 0) y = 1;

		// Initialiation du filigrane
		initWatermark();

		if(_debug) {
			String msg = "[IMReader] Initialisation de ImageMagickReader \n\tsrc: "+src+"\n\tparameters: "+par;
			System.out.println(msg);
			if(m_logger.isDebugEnabled()) m_logger.debug(msg);
			msg = null;
		}

		// On recupere le nouveau mimeType en fonction du parametre de format
		String mimeType = getInternalMimeType(f);

		// Si un nouveau type MIME est specifie, on doit verifier si on le supporte
		reformat = mimeTypeEstValideEnSortie(mimeType);

		// Si on doit reformater, on conserve le type MIME de sortie, sinon on le prend depuis l'image source
		if ( reformat ) this.finalMimeType = mimeType;
		else finalMimeType = getMimeType();

		// Si on demande un nouveau format mais qu'il n'est pas supporte, alors on
		// considere que la ressource n'est pas trouvee
		if ( !reformat && mimeType != null ) throw new ProcessingException("Le format " + mimeType + " n'est pas supporte");

		// La logique pour savoir si on a une transformation a effectuer
		convert = ( w > 0 || h > 0 || r != 0 || p > 0 || q > 0 || reformat ) ? true : false;	// une converion
		watermark = ( wksrc!=null && wksrc.exists() ) ? true : false;	// l'ajout du filigrane
		// on regarde dans les zones si le filigrane doit être activé ou non
		if ( this.zonesPasFiligrane != null && this.zonesPasFiligrane.length > 0 && !pleadeZone.equals("") && watermark ) {
			for ( int i = 0 ; i < this.zonesPasFiligrane.length ; i++ ) {
				if( pleadeZone.equals(this.zonesPasFiligrane[i]) ) watermark = false;
			}
		}
		transform = ( convert || watermark  ) ? true : false;	// si conversion ou filigrane, alors transformation

		// Si on a une transformation, alors il ne faut pas de byteRanges
		if ( transform ) this.byteRanges = false;
		else this.byteRanges = true;

		// On respecifie le header byteRanges
		if (this.byteRanges) {
			response.setHeader("Accept-Ranges", "bytes");
		}
		else {
			response.setHeader("Accept-Ranges", "none");
		}

	}

	/**
	 * Envoie l'image souhaitee, peut-&eacirc;tre modifi&eacute;e.
	 */
	public void generate() throws IOException, ProcessingException {

		// D'abord si on doit transformer
		if (transform) {

			// Determiner comment on transforme exactement

			// Si on a les quatre parametres, alors c'est un crop, sinon un resize si on a w ou h, sinon reformat
			int method = METHOD_REFORMAT_ONLY;
			if ( x > 0 && y > 0 && w > 0 && h > 0 ) method = METHOD_CROP;
			else if ( w > 0 || h > 0 || p > 0 ) method = METHOD_RESIZE;

			// Les parties rotate, resize, quality et crop de la commande
			String rotate = "", resize = "", quality = "", crop = "", wkcmd = "";
			if ( method == METHOD_RESIZE ) {
				// Un resize, en hauteur et/ou en largeur
				resize = " -resize ";
				if ( w > 0 || h > 0 ) {
					if ( w > 0 ) resize += w;
					resize += "x";
					if ( h > 0 ) resize += h;
					resize += " ";
				}
				else if ( p > 0 ) {
					resize += p + "% ";
				}
			}
			else if (method == METHOD_CROP) {
				// Un crop (tous les parametres sont la)
				crop = " -crop " + w + "x" + h + "+" + x + "+" + y + " ";
			}
			if ( r > 0 ) {
				rotate = " -rotate +" + r + " ";
			}
			if ( q > 0 ) {
				quality = " -quality " + q + " ";
			}

			// Pour trouver le fichier, il faut faire attention a l'URL
			// retournee par inputSource, car elle peut contenir des
			// caracteres interdits, en particulier l'espace ou les accents
			String fileCompletePath = source;

			if(_debug) {
				String msg = "[IMReader] uri : "+inputSource.getURI();
				System.out.println(msg);
				if(m_logger.isDebugEnabled()) m_logger.debug(msg);
				msg = null;
			}

			// On essaie de creer un fichier si on a du "file://"
			if( "file".equals(inputSource.getScheme() ) ) {
				fileCompletePath = getCompletePath( inputSource.getURI() );
				// En tout etat de cause, on ajoute des guillemets
				// TODO (MP) : L'ajout de guillemets ne fonctionne pas du tout sous Linux. Faut-il tester l'OS ?
				// fileCompletePath = "\"" + fileCompletePath + "\"";
				if(_debug) {
					String msg = "[IMReader] Apres les guillemets = " + fileCompletePath;
					System.out.println(msg);
					if(m_logger.isDebugEnabled()) m_logger.debug(msg);
					msg = null;
				}
			}

			// La sortie
			String stdout = ((reformat)?(f+":"):"") + destFile; // soit jpg:-, soit -
			if( !"file".equals( inputSource.getScheme() ) ) {
				// il est necessaire de preciser le format de sortie si la source n'est pas un file:/
				String finalExtension = (f!=null) ? f : org.apache.commons.io.FilenameUtils.getExtension( fileCompletePath );
				stdout = finalExtension + ":" + destFile;
			}

			// La commande ImageMagick a executer
			final String cmd;

			// Ajout de filigrane ?
			if ( watermark ) {
				try {
					if ( wkGravity!=null && !"".equals(wkGravity) ) {
						wkcmd = ( ( "file".equals(wksrc.getScheme()) ) ? getCompletePath( wksrc.getURI() ) : wksrc.getURI() ) +
										" -gravity " + wkGravity + " -composite ";
					}
					else {
						wkcmd = "-size "+ww+"x"+wh+" -background transparent tile:" +
										( ( "file".equals(wksrc.getScheme()) ) ? getCompletePath( wksrc.getURI() ) : wksrc.getURI() ) +
										" -composite ";
					}
					if ( _debug ) {
						String msg = "[IMReader] Path de l'image filigrane: "+wkcmd;
						System.out.println(msg);
						if(m_logger.isDebugEnabled()) m_logger.debug(msg);
						msg=null;
					}
				}
				catch (Exception e) {
					if(m_logger!=null && m_logger.isErrorEnabled()){
						getLogger().error(" Erreur survenue lors de la construction de l'URL du filigrane : "+wk, e);
					}
					wkcmd=""; watermark = false;
				}
			}


			// La commande finale pour convert
			if ( convert || watermark ) {
				cmd = this.imPath + " " + fileCompletePath + rotate + crop + resize + quality + " " + wkcmd + stdout;
			}
			else cmd = null;

			if(_debug){
				String msg = "[IMReader] cmd = " + cmd;
				System.out.println(msg);
				if (m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
			}

			// On execute la commande
			final Process proc = Runtime.getRuntime().exec(cmd);

			// On recupere la sortie dans une autre thread pour eviter des IO locks...
			Thread th_copy = new Thread()
			{
				public void run()
				{
					BufferedInputStream in = null;
					try {
						in =  new BufferedInputStream (proc.getInputStream());
						// On copie la sortie IM dans la sortie du flux (vers le navigateur)
						IOUtils.copy(in, out);
						
						if( destFile != null && !"-".equals(destFile)){
							in =  new BufferedInputStream(new FileInputStream(destFile));
							IOUtils.copy(in, out);
						}
					}
					catch (IOException e) {
						if(m_logger!=null && m_logger.isErrorEnabled()) m_logger.error(" Une erreur est survenue lors du traitement par ImageMagick (" + cmd + ")", e);
					}
					finally {
						close(in);
						in = null;
					}
				}
			};
			th_copy.start();

			// Recuperer les erreurs renvoyees apr ImageMagick
			// Impossible to have 2 parallel streams, only error stream output
			// see <http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html> for
			BufferedReader IMerr=new BufferedReader (new InputStreamReader (proc.getErrorStream()));
			try {
				String line=null;
				while((line=IMerr.readLine()) != null){
					if(getLogger().isErrorEnabled()) getLogger().error(line);
					if(_debug){
						System.out.println("[IMReader]" + line + "\n" +
								"               " + cmd + "\n" +
								"               w: "+w+" ; h:"+" ; x:"+x+" ; y:"+y+" ; p:"+p+" ; q:"+q+" ; r:"+r+" ; wk:"+wk);
					}
				}
			} catch (IOException ioe) {
				if(m_logger!=null && m_logger.isErrorEnabled()) m_logger.error(" Une erreur est survenue lors du traitement par ImageMagick ("+cmd+")",ioe);
			} finally{
				if(IMerr!=null) IMerr.close();
				IMerr = null;
			}

			// On attend la fin du traitement
			try {
				proc.waitFor();
				//On attend la fin du th_copy, afin d'éviter une NPE sur 'out' - pendant 10 minutes maxi
				while(th_copy.isAlive()){
					th_copy.join(this.timeout); // on ferme le thread
				}
				if(_debug){
					String msg = "[IMReader] Waiting for "+cmd;
					System.out.println(msg);
					if (m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
					msg=null;
				}
			}
			catch (Exception e ) {
				if(m_logger.isErrorEnabled()) m_logger.error(" Une erreur est survenue lors du traitement par ImageMagick (" + cmd + ")", e);
			} finally{
				if(proc!=null){
					close(proc.getInputStream());
					close(proc.getOutputStream());
					close(proc.getErrorStream());
					proc.destroy();
				}
			}
		}
		else {
			// On doit envoyer l'image telle quelle, sans modification, alors on appelle
			// simplement le generate() du ResourceReader.
			if(_debug){
				String msg = "[IMReader] Pas de transformation necessaire.";
				System.out.println(msg);
				if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
				msg = null;
			}
			super.generate();
		}
	}

	/**
	 * Methode privee chargee de fermer les objets.
	 * On l'utilise essentiellement pour fermer les flux utilises par ImageMagick
	 * appele par un processus.
	 */
	/*Methode pour Java 1.5 et sup.*/
	private void close (java.io.Closeable c){
		if (c != null) {
			try {
				c.close();
			} catch (IOException e) {
				; // ignored
			} finally{
				c = null;
			}
			return;
		}
		else {
			return;
		}
	}
	/*Meme methode pour Java 1.4*/
/* 	private void close (java.lang.Object o) {
		if ( o == null ) return;
		try {
			if ( o instanceof java.io.InputStream )
					((java.io.InputStream) o).close();
			else if ( o instanceof java.io.OutputStream )
					((java.io.OutputStream) o).close();
		} catch ( IOException e ) {
			; // ignored
		} finally {
			o = null;
			return;
		}
	} */

	/** Initialisation du filigrane si necessaire.
	 * <p>
	 * <ul>
	 * <li>Initialisation de la source du filigrane.
	 * <br/>La source du filigrane peut provenir de l'URL (param wk) ou de la
	 * configuration de l'ImageMagickReader (ie, dans la Sitemap).
	 * <li>Initialisation de la taille du filigrane
	 * <br/>La taille du filigrane (largeur et hauteur) peut provenir de l'URL
	 *  (params wh et ww). S'il n'y en a pas on recupere la taille de l'image
	 *  source.
	 */
	private void initWatermark() throws SourceNotFoundException, IOException {

		// Initialisation de la source du filgrane
		wksrc = null;
		// if ( wk!=null && this.paramUrlPasFiligrane.equals(wk) ) {
		// 	if(_debug) System.out.println("[IMReader] Desactivation du filigrane demande par URL: wk="+wk);
		// 	return;
		// }
		if ( wk!=null && !"".equals(wk) ) {
			wksrc = getWatermarkSource(wk);
			if(_debug) {
				String msg = "[IMReader] Initialisation du filigrane demande en URL: "+wk;
				System.out.println(msg);
				if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
				msg = null;
			}
		}
		if ( wk==null || wksrc==null ) {
			// Pas de param d'URL activant l'ajout du filigrane ou ce dernier n'est pas trouve. On va regarder si on doit l'ajouter par defaut
			if ( this.wkConf!=null && !"".equals(this.wkConf) ) {
				// On veut savoir si on doit desactiver l'ajout de filigrane pour l'image courante
				wk = ( noWatermark(this.src) ) ? null : this.wkConf;
				// On se laisse la possibilite de desactiver l'ajout de filigrane pour cette image
				if (wk != null) {
					wksrc = getWatermarkSource(wk);
					// On en profite pour initialiser la source du filigrane par defaut si ce n'est pas deja fait
					if ( this.wkConf == null ) { this.wkConfsrc = wksrc = getWatermarkSource(wk); }
				}
				if(_debug) {
					String msg = "[IMReader] Initialisation du filigrane par defaut: "+this.wkConf
								+((wk==null) ? ". Pour ce dossier, on la desactive": "" );
					System.out.println(msg);
					if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
					msg = null;
				}
			}
		}
		// On s'assure qu'on a bien des dimensions (largeur *et* hauteur) en cas d'ajout de filigrane
		if ( wk!=null && !"".equals(wk) && (ww==-1 || wh==-1) ) {
			ImageInformation imgInfo = null;
			if (w > -1 && h > -1) {
				ww = w; wh = h;
			} else {
				imgInfo = ImageUtilities.getImageInformation(this.objectModel, getLogger(), this.source, this.inputSource.getInputStream());
				int imgw = imgInfo.getWidth(),
				imgh = imgInfo.getHeight();
				/*Java 1.6*/
				if ( w > -1 ) {
					ww = w;
					wh = new Double ( Math.ceil( ( ( new Float(imgh) / new Float(imgw) ) * w ) ) ).intValue();
				} else if ( h > -1 ) {
					wh = h;
					ww = new Double ( Math.ceil( ( ( new Float (imgw) / new Float(imgh) ) * h ) ) ).intValue();
				} else {
					ww = imgw;
					wh = imgh;
				}
				/*Java 1.4*/
				/*
				try {
				if ( w > -1 ) {
					ww = w;
					// Java 1.6 wh = new Double ( Math.ceil( ( ( new Float(imgh) / new Float(imgw) ) * w ) ) ).intValue();
					wh = new Double ( Math.ceil( ( ( imgh / imgw ) * w ) ) ).intValue();
				} else if ( h > -1 ) {
					wh = h;
					// Java 1.6 ww = new Double ( Math.ceil( ( ( new Float (imgw) / new Float(imgh) ) * h ) ) ).intValue();
					ww = new Double ( Math.ceil( ( ( imgw / imgh ) * h ) ) ).intValue();
				} else {
					ww = imgw;
					wh = imgh;
				}
				} catch (java.lang.NumberFormatException e) {
					ww = imgw;
					wh = imgh;
				}
				*/
			}

			// On controle la taille du filigrane. Si elle est en-deca de la taille minimale, on desactive l'ajout du filigrane
			if ( ww < wkmini || wh < hkmini ) {
				if(_debug) {
					String msg = "[IMReader] Desactivation du filigrane : la taille finale ("+ww+"x"+wh+") est inferieure a la taille minimale ("+wkmini+"x"+hkmini+").";
					System.out.println(msg);
					if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
					msg = null;
				}
				wk = null; wksrc = null; wh = -1; ww = -1; wkGravity = null;
			} else  {
				if (_debug) {
					String msg = "[IMReader] Initialisation de la taille du filigrane: largeur="+ww+" ; hauteur="+wh
							+ ((imgInfo!=null) ? ". Pas de taille en URL, on les prend via ImageInformation." : "");
					System.out.println(msg);
					if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
					msg = null;
				}
				if ( this.wkGravity!=null && !"".equals(this.wkGravity) ) {
					if(_debug) {
						String msg = "[IMReader] Initialisation du placement du filigrane : "+wkGravity;
						System.out.println(msg);
						if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
						msg = null;
					}
					ww = -1; wh = -1;
				}
			}

			imgInfo = null;
		}
	}

	/** Recherche un fichier de desactivation de l'ajout de filigrane */
	private boolean noWatermark(String url){
		boolean ret = false;
		File f = null;
		try {
			f = new File( new URI(url) );
		} catch (IllegalArgumentException iae) {
			f = new File(url);
		} catch (URISyntaxException use) {
			f = new File(url);
		}
		if (f!=null)
			try {
				ret = ( noWatermarkFinder( f.getParentFile() ) == null) ? false : true;
			} catch (Exception e){
				if( m_logger!=null && m_logger.isErrorEnabled() ) m_logger.error("Une erreur est survenue lors de la recherche du fichier de desactivation du filigrane pour "+url, e);
				ret = false;
			}
			if(_debug) {
				String msg = "[IMReader] Desactivation du filigrane (fichier "+ this.nomFichierPasFiligrane+"): "+ret;
				System.out.println(msg);
				if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
				msg = null;
			}
			return ret;
	}
	/** Recherche recurcive du fichier de desactivation de l'ajout de filigrane */
	private File noWatermarkFinder(File d) throws IOException {
		//    	if(_debug){
		//    		System.out.println("[IMReader] noWatermarkFinder sur "
		//    				+d+"\n\td==null: "+(d==null)+" ; d.isDirectory(): "
		//    				+d.isDirectory()+" ; d.canRead(): "+d.canRead()
		//    				+" ; d.exists(): "+d.exists());
		//    	}

		// Si le dossier n'est pas correct, on arrete la
		if ( d == null || !d.exists() || !d.isDirectory() || !d.canRead() ) return null;

		// On construit l'objet fichier qui pourrait etre le bon
		File f = new File(d, this.nomFichierPasFiligrane);

		// S'il existe, on le retourne
		if ( f.exists() && f.isFile() && f.canRead() ) return f;

		// Il n'existe pas, alors on poursuit avec le parent, si on n'est pas a la racine
		File p = d.getParentFile();
		if ( p == null ) return null;
		return noWatermarkFinder(p);
	}

	/*
	 * Retourne la source du filigrane
	 */
	private Source getWatermarkSource (String url) {
		Source ret = null;
		try {
			// resolver.resolveURI(arg0, arg1, arg2); Pour une resolution en prenant en compte un chemin arg0 relatif a un chemin arg1
			ret = resolver.resolveURI(wk);
		} catch(Exception e) {
			if(m_logger!=null && m_logger.isErrorEnabled()) m_logger.error(" Une erreur est survenue lors de la resolution du chemin vers le filigrane : " + wk, e);
			ret = null;
		}
		if(_debug) {
			String msg = "[IMReader] Resolution de l'URL du filigrane: "+url+" = "+ret.exists();
			System.out.println(msg);
			if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
			msg = null;
		}
		return ret;
	}

	/*
	 * Retourne l'URL complete d'un fichier en s'assurant que cela fonctionne sous
	 * UNIX et Windows.
	 */
	private String getCompletePath (String path) {
		String ret = path;
		try {
			// L'URL de la source
			String encodedUrl = NetUtilities.encodeUrl(ret, System.getProperty("file.encoding"));
			//            if(_debug) System.out.println("[IMReader] encodedUrl: "+encodedUrl);
			// Ceci semble fonctionner sous Linux
			// TODO (MP): la methode NetUtilities.encodeUrl() ne fonctionne pas du tout sous Linux
			String fileCompletePathTmp = (new File((new URI(ret)).getPath())).getAbsolutePath();
			//            if(_debug) System.out.println("[IMReader] fileCompletePathTmp = " + fileCompletePathTmp);
			File tmpFile = new File(fileCompletePathTmp);
			if ( tmpFile.exists() ) {
				ret = fileCompletePathTmp;
			}
			else {
				if(_debug) {
					String msg = "[IMReader] Branche Windows";
					System.out.println(msg);
					if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
					msg = null;
				}
				// Ceci semble fonctionner sous Windows
				if ( ret.indexOf("file:/") == 0 ) {
					//                    if(_debug) System.out.println("[IMReader] On a file:/...");
					File tmpF = new File(ret.substring(5));
					//                    if(_debug) System.out.println("[IMReader] On a un tmpF = " + tmpF.getAbsolutePath());
					ret = tmpF.getAbsolutePath();
				}
			}
		}
		catch (URISyntaxException e) {
			// On ne fait rien de special, ne devrait pas arriver
			System.out.println("[IMReader] Erreur survenue lors de la construction de l'URL de l'image : "+e);
			if(m_logger!=null && m_logger.isErrorEnabled()) m_logger.error("Erreur survenue lors de la construction de l'URL de l'image : "+path, e);
			ret = null;
		}
		return ret;
	}

	/**
	 * Indique si la source est cachable ou pas, et si elle a ete modifiee.
	 * Si on transforme, pas de cache. Si on a passe un parametre indiquant de ne pas
	 * cacher (par defaut), alors pas de cache. Sinon on laisse faire la superclasse.
	 */
	public SourceValidity getValidity() {
		SourceValidity sv = null;
		if ( this.transform || !this.cacheImagesTransformees );
		if ( super.getValidity().isValid() == 1 &&  wksrc!=null ) sv = wksrc.getValidity();
		sv = super.getValidity();
		if(_debug) {
			String msg = "[IMReader] SourceValidity = " + sv.toString();
			System.out.println(msg);
			if(m_logger!=null && m_logger.isDebugEnabled()) m_logger.debug(msg);
			msg = null;
		}
		return sv;
	}

	/**
	 * Retourne le type MIME depuis le format demande
	 * @param format    Le format (extension de fichier)
	 * @return          Le type MIME ou null si aucun format ou format non supporte
	 */
	private String getInternalMimeType(String format) {
		// On verifie si on a recu quelque chose
		if ( format == null || format.trim().equals("") ) return null;

		// On construit un fichier de test
		String testFileName = "dummy." + format;

		// On utilise les services de Cocoon pour determiner le type MIME
		Context ctx = ObjectModelHelper.getContext(this.objectModel);
		if (ctx != null) {
			final String mimeType = ctx.getMimeType(testFileName);
			if (mimeType != null) {
				return mimeType;
			}
		}

		// Si on n'a pas pu trouver...
		return null;
	}

	/**
	 * Retourne true si le type MIME est supporte en sortie.
	 * @param mt
	 * @return
	 */
	public boolean mimeTypeEstValideEnSortie(String mt) {
		// Si on recoit rien alors on retourne false
		if (mt == null) return false;

		// On verifie simplement dans notre liste des types MIME obtenus depuisla configuration
		Object o = mimeTypes.get(mt);
		if ( o == null ) return false;
		return true;
	}

	/**
	 * Retourne le type MIME de la ressource, soit depuis le fichier lui-meme soit depuis le format de sortie (conversion).
	 */
	public String getMimeType() {
		if ( finalMimeType != null ) return finalMimeType;
		else return super.getMimeType();
	}




	/**
	 * On recycle nos variables qui doivent l'etre
	 */
	public void recycle() {
		// On appelle la methode generique d'abord
		super.recycle();
		// Puis nos variables
		this.finalMimeType = null;
	}

}
