//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Class permettant de créer un treeview simplement
 * @author Julien Huet
 */
/**
 * @class Class permettant de créer un treeview simplement
 * @author Julien Huet
 * @augments Mootools.Options
 * @augments Mootools.Events
 */
var TreeView = new Class(/** @lends TreeView.prototype */{
	Implements: [Options, Events],

	/**
	* <strong>Constructor</strong><br/> Initialisation du treeview
	* @param {String} url URL a appeler pour recuperer l'arbre UL/LI
	* @param {Element} container div dans lequel sera placé l'arbre
	* @param {String} urlStart debut de l'url utilisée pour créer l'url pour récuperer les noeuds fils lors d'un appel ajax 
	* @param {String} urlEnd fin de l'url utilisée pour créer l'url pour récuperer les noeuds fils lors d'un appel ajax 
	* @param {String} nodeIdToFocus Id du noeud sur lequel on souhaite mettre le focus
	* @param {Element} container qui est scroll lors du focus
	*/
	initialize: function(url, container, urlStart, urlEnd, treeManager, containerToScroll){
		this.treeContainer = new Element('div').setProperty('id',container.get('id')+'-treeContainer');
		container.getParent().setStyle('height','auto');
		container.getParent().setStyle('margin-top','0');
		this.noTreeMessage = ''; //TODO i8n
		this.tree = new MifTreeCustom({
				container: this.treeContainer,
				forest: true,
				types: {
					open: {
						cls: 'mif-tree-no-icon',
						loadable: true
					},
					close: {
						loadable: false,
						cls: 'mif-tree-no-icon'
					}
				},
				dfltType: 'open',
				height: 18
			},
			urlStart,
			urlEnd,
			treeManager,
			containerToScroll
		);
		this.url = url;
		this.nodeIdToFocus = '';
		this.mainContainer = container;
		// mise en place des evenements	
		this.initEvents();
		this.expandOnLoad = false;
		// Nombre de de niveau a etendre au maximum. -1 correspond signifie qu'on etend tous les noeuds
		// 0 correspond à la racine
		this.nbLevelToExpandOnLoad = -1;
	},

	/**
	* Id du noeud sur lequel on souhaite mettre le focus lors du chargement de l'arbre
	* @param {String} nodeId
	*/
	setNodeIdToFocus: function(nodeId){
		this.nodeIdToFocus = nodeId;
	},

	/**
	* Fonction permettant d'initialiser les evenements auxquel l'arbre réagira
	*/
	initEvents: function(){
		this.tree.addEvent('onSelect', this.onSelect);
	},

	/**
	* Fonction permettant de charger l'arbre depuis du code HTML
	* @param {HtmlElement} htmlToLoad html comprennant la liste UL LI
	*/
	load: function(){
		if ( !this.mainContainer ) {
			return false;
		}
		if ( this.mainContainer.getFirst() ) {
			this.mainContainer.getFirst().dispose();
		}

		var loading = new LoadingMessage(this.mainContainer);
		loading.start();

		var realUrl = this.url;
		var treeView = this;
		var req = new Request.HTML({
			url: realUrl,
			method: 'get',
			onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){ 
				var initLoad = treeView.getTree().initTree(responseTree);
				if ( initLoad ) {
					treeView.getTree().load(initLoad);
				} else {
					treeView.mainContainer.set('html', treeView.noTreeMessage);
					loading.stop();
					return false;
				}	
				loading.stop();
				
				// ajout de l'arbre a son container principal
				treeView.mainContainer.adopt(treeView.treeContainer);
				
				if(treeView.nodeIdToFocus){
					treeView.getTree().selectNodeByID(treeView.nodeIdToFocus);
				}
				
				treeView.doOnLoad();
			},
			onFailure: function(){
				alert('Une erreur est survenue lors du chargement de l\'arbre'); //TODO: i18n
			}
		});
		req.send();
	},

	/**
	* Simple getter pour recuperer l'arbre Mif.Tree
	* @return {MifTreeCustom} tree 
	*/
	getTree: function(){
		return this.tree;
	},
	
	/**
	 * Deplier l'arbre à son chargement
	 * Il faut appeler cette fonction avant le chargement de l'arbre
	 * @param {Integer} nbLevel Parametre optionnel. Si il est présent il permet de définir le niveau jusqu'auquel on étend.
	 * Par défaut on etend tout (valeur -1).
	 */
	enableExpandAllOnLoad: function(nbLevel){
		this.expandOnLoad = true;
		if ( $chk(nbLevel) ) {
			this.nbLevelToExpandOnLoad = nbLevel;
		}
	},

	/**
	* Fonction appelée lorsque l'on clique sur un noeud de l'arbre
	* @param {Mif.Tree.Node} node
	*/
	onSelect: function(node){},

	/**
	* Fonction appelée lorsque l'arbre commence à charger.
	*/
	doOnLoad: function(){
		if ( this.expandOnLoad ) {
			this.getTree().expandAll(this.nbLevelToExpandOnLoad);
		}
	}
});