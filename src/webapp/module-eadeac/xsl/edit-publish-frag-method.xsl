<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Dresse la liste des méthodes de fragmentation
		| Par défaut, on le fait pour le module-eadeac, mais l'XSL est assez
		| générique pour le faire pour un autre module.
		+-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
								version="2.0" exclude-result-prefixes="xsl i18n">

	<xsl:param name="i18n-catalogue" select="'module-eadeac-edit'" />
	<xsl:param name="i18n-prefix" select="'edit.ead-publish'" />

	<xsl:template match="/xsl:stylesheet">
		<select name="frag-method">
			<option value="all-components">
				<i18n:text key="{$i18n-prefix}.form.field.frag-method.value.all-components" catalogue="{$i18n-catalogue}">
					<xsl:text>all-components</xsl:text>
				</i18n:text>
			</option>
			<option value="_none">
				<i18n:text key="{$i18n-prefix}.form.field.frag-method.value._none" catalogue="{$i18n-catalogue}">
					<xsl:text>_none</xsl:text>
				</i18n:text>
			</option>
			<xsl:for-each select="xsl:template[starts-with(@name, 'break-by-')]">
				<xsl:variable name="v"
							select="normalize-space(substring-after(@name, 'break-by-'))" />
				<option value="{$v}">
					<i18n:text key="{$i18n-prefix}.form.field.frag-method.value.{$v}" catalogue="{$i18n-catalogue}">
						<xsl:value-of select="$v" />
					</i18n:text>
				</option>
			</xsl:for-each>
		</select>
	</xsl:template>

</xsl:stylesheet>
