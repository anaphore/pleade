<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSLT construisant la version HTML des recherches dans la liste des
		| documents de niveau 1 (top:1)
    +-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	exclude-result-prefixes="xsl sdx i18n">

	<xsl:import href="navigation.xsl" />

	<!-- séparateur entre les composants du lien -->
	<xsl:param name="list-sep" select="'&#xA0;&#x2022; '"/>

	<xsl:template match="/sdx:document">

		<div id="pl-srch-tp-dcs">
			<xsl:choose>
				<xsl:when test="not(sdx:results) or sdx:results/@nb&lt;=0">
					<xsl:choose>
					  <xsl:when test="sdx:results/sdx:query/sdx:query/@type='complex'">
							<i18n:text key="search-top-docs.results.0">Aucun document de premier niveau ne correspond a votre recherche.</i18n:text>
						</xsl:when>
					  <xsl:otherwise>
							<i18n:text key="search-top-docs.results.null">Aucun document de premier niveau n'a été indexé.</i18n:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="sdx:results" />
				</xsl:otherwise>
			</xsl:choose>
		</div>

	</xsl:template>

	<xsl:template match="sdx:results">

		<div class="pl-rslt-nv">
			<xsl:comment>u</xsl:comment>
			<xsl:apply-templates select="." mode="hpp" />
		</div>

		<xsl:apply-templates select="sdx:result" />

	</xsl:template>

	<xsl:template match="sdx:result">

		<div class="search-top-docs" id="{sdx:field[@name='sdxdocid']/@value}">
			<a href="ead.html?id={sdx:field[@name='sdxdocid']}" onclick="windowManager.winFocus(this.href, 'docead');return false;" title="search-top-docs.document.title" i18n:attr="title" >
				<xsl:apply-templates select="sdx:field[@name='fucomptitle']" />
				<xsl:apply-templates select="sdx:field[@name='uunitid']" />
				<xsl:apply-templates select="sdx:field[@name='udate']" />
			</a>
		</div>

	</xsl:template>

	<xsl:template match="sdx:field" priority="-2">
	</xsl:template>

	<xsl:template match="sdx:field[@name='fucomptitle']">
		<span class="cdc-title"><xsl:apply-templates /></span>
	</xsl:template>

	<xsl:template match="sdx:field[@name='udate']">
		<xsl:value-of select="$list-sep"/>
		<span class="cdc-unitdate"><xsl:apply-templates /></span>
	</xsl:template>

	<xsl:template match="sdx:field[@name='uunitid']">
		<xsl:value-of select="$list-sep"/>
		<span class="cdc-unitid"><xsl:apply-templates /></span>
	</xsl:template>

	<xsl:template match="sdx:hilite">
		<span class="pl-hilite"><xsl:apply-templates /></span>
	</xsl:template>

</xsl:stylesheet>
