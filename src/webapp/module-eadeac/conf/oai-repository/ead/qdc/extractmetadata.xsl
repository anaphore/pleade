<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!--
dc.description	:	A transformation for getting qualified Dublin Core metadata from EAD documents.
					input document = 1 marked EAD document (with identified resources)
					output document = many DCQ records (as many as number of identified resources)
dc.date				: April 2004
dc.date.modified	: 28th September 2004
-->

<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:dcterms="http://purl.org/dc/terms/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="pleade xsl">

  <xsl:import href="../../../../xsl/publication/ead/title-methods.xsl"/>
  <xsl:import href="../../../../xsl/link-helpers.xsl"/>


	<!-- NB to reuse :
		need in XSL container :
			** Parameters :
				- a pleade-title param : give the form of the representative title
				- a pleade-title-sep param : give the separator to use to create the representative title
				- a language param : language of the title
				- a includehasPart param : to know if archival subcomponents should be included
				- a inheritDesc param : to know if descriptors of ancestral nodes should be inherited
				- a relationMode param : to fix the form of relation content (url or just filename)
				- a baseURL param : to give the base common URL (url = baseURL + id)

		need to call the extractmetadata template :
			- id param = identifier of the resource (EAD fragment)
	-->
	
	<!-- Une fonction pour trouver une valeur normalisée -->
	<xsl:function name="pleade:get-normalized-form" as="xs:string">
		<xsl:param name="element"/>
		<xsl:choose>
			<xsl:when test="$element/@normal"><xsl:value-of select="$element/@normal"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$element"/></xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- Le template principal -->
	<xsl:template name="extractmetadata">
		<!-- L'identifiant unique du composant -->
		<xsl:param name="id" select="." />
		
		<!-- Déjà un identifiant -->
		<dc:identifier>
			<xsl:value-of select="$id" />
		</dc:identifier>

			<!-- Extract metadata for finding aid : resource by default -->
		<xsl:if test="self::ead">
			<xsl:call-template name="ead" />
		</xsl:if>

			<!-- Extract metadata for collection of archives : resource by default -->
		<xsl:if test="self::archdesc">
			<xsl:call-template name="archdesc" />
		</xsl:if>

			<!-- Extract metadate for hierarchical level of archives -->
		<xsl:if test="self::c|self::c01|self::c02|self::c03|self::c04|self::c05|self::c06|self::c07|self::c08|self::c09|self::c10|self::c11|self::c12">
			<xsl:call-template name="cN" />
		</xsl:if>
	</xsl:template>
	<!-- ********************** end GENEARAL TEMPLATE ************************ -->

	<!-- IMPLICIT METHOD : target element is given by a mapping table -->
	<xsl:template match="*" mode="implicit">
		<xsl:apply-templates select="." mode="meta-oai" />
		<xsl:apply-templates mode="implicit" />
	</xsl:template>

	<!-- EXPLICIT METHOD : target element is given in the @encodinganalog attribute -->
	<!-- For all elements : test if attribute @encodinganalog is used to indicate target element
												and intercept the value of the target element -->
	<!-- Suggestion add other condition : what process (no process, only implicit, only explicit, both), by default it's both  -->
	<xsl:template match="*" mode="explicit">
		<xsl:variable name="current-node" select="current()" />

		<!-- A target DC element is given -->
		<xsl:if test="@encodinganalog and contains(@encodinganalog, 'http://purl.org/dc/')">

			<!-- target elements written in EAD document -->
			<!-- There, $target-el is a node-set() with the name of all target elements for one EAD element (multi-mapping) written like URI -->
			<xsl:variable name="target-el">
				<xsl:call-template name="get-target-element">
					<xsl:with-param name="encodinganalog" select="@encodinganalog" />
				</xsl:call-template>
			</xsl:variable>
			
			<xsl:for-each select="$target-el//target">
				<!-- Check if target element is a DC element -->
				<xsl:if test="contains(., 'http://purl.org/dc/')">
					<xsl:variable name="target-name">
						<xsl:call-template name="get-target-name">
							<xsl:with-param name="target-uri" select="." />
						</xsl:call-template>
					</xsl:variable>
					<!-- Create target element -->
					<xsl:call-template name="el-create">
						<xsl:with-param name="target" select="$target-name" />
						<xsl:with-param name="content">
							<xsl:apply-templates select="$current-node" mode="text" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<!-- To loop -->
		<xsl:apply-templates select="*" mode="explicit" />
	</xsl:template>

	<!-- A temporary XML tree is created which contains all the supposed target elements under the following way:
			<targets>
				<target>...</target>
				<target>...</target>
			</targets>
	-->
	<xsl:template name="get-target-element">
		<xsl:param name="encodinganalog" select="." />
		<targets>
		<!-- Transform @encodinganalog value to a list of target elements -->
			<xsl:variable name="list-target">
				<xsl:choose>
				<!-- @encodinganalog="oai:resource http://purl.org/dc/elements/1.1/ oipte tyrfdghd" -->
					<xsl:when test="contains($encodinganalog, 'oai:resource')">
						<xsl:value-of select="normalize-space(concat(substring-before($encodinganalog, 'oai:resource'), substring-after($encodinganalog, 'oai:resource')))" />
					</xsl:when>
				<!-- @encodinganalog="http://purl.org/dc/elements/1.1/ fzerz rtge" -->
					<xsl:otherwise>
						<xsl:value-of select="normalize-space($encodinganalog)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="target-eclate">
				<xsl:with-param name="targets-list" select="normalize-space($list-target)" />
			</xsl:call-template>
		</targets>
	</xsl:template>

	<!-- Isolate each occurrence of supposed target element from a list -->
	<xsl:template name="target-eclate">
		<xsl:param name="targets-list" select="." />
		<xsl:choose>
			<!-- if many occurrences of supposed target element -->
			<xsl:when test="contains($targets-list, ' ')">
				<target>
					<xsl:value-of select="substring-before($targets-list, ' ')" />
				</target>
				<xsl:if test="normalize-space(substring-after($targets-list, ' ')) != ''">
					<xsl:call-template name="target-eclate">
						<xsl:with-param name="targets-list" select="substring-after($targets-list, ' ')" />
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
			<!-- only one target element -->
			<xsl:otherwise>
				<target>
					<xsl:value-of select="$targets-list" />
				</target>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Give the name of the target element, not like an URI  -->
	<xsl:template name="get-target-name">
		<xsl:param name="target-uri" select="." />
		<xsl:choose>
			<!-- Target element is one of DC simple set -->
			<xsl:when test="contains($target-uri, 'http://purl.org/dc/elements/1.1/')">
				<xsl:value-of select="concat('dc:', normalize-space(substring-after($target-uri, 'http://purl.org/dc/elements/1.1/')))" />
			</xsl:when>
			<!-- Target element is one of the DC qualifiers -->
			<xsl:otherwise>
				<xsl:value-of select="concat('dcterms:', normalize-space(substring-after($target-uri, 'http://purl.org/dc/terms/')))" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Create the DC target element -->
	<xsl:template name="el-create">
		<xsl:param name="target" select="." />
		<xsl:param name="content" select="." />
		<xsl:element name="{$target}">
			<xsl:value-of select="$content" />
		</xsl:element>
	</xsl:template>

	<!-- ***********************************************************

						resource = Finding aid

	**************************************************************** -->
	<!-- Creating the Dublin Core qualified metadata for a finding aid -->
	<xsl:template name="ead">
		<xsl:apply-templates select="eadheader" mode="meta-oai" />
		<xsl:apply-templates select="frontmatter" mode="meta-oai" />

		<!-- Relations -->
		<xsl:comment> Relations </xsl:comment>
		<xsl:if test="archdesc">
			<xsl:variable name="link">
				<xsl:call-template name="get-link">
					<xsl:with-param name="id" select="archdesc/@id" />
					<xsl:with-param name="mode" select="$relationMode" />
					<xsl:with-param name="url-base" select="$baseURL" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="$link != '.xml' and $link != $baseURL">
				<dcterms:references>
					<xsl:value-of select="$link" />
				</dcterms:references>
			</xsl:if>
		</xsl:if>

		<!-- Generated content -->
		<xsl:comment> Contenus générés </xsl:comment>
		<dc:type>dataset</dc:type>
		<dc:type>text</dc:type>
		<dc:format>text/xml</dc:format>
		<dcterms:conformsTo>EAD DTD</dcterms:conformsTo>
	</xsl:template>

	<!-- Processing @audience attribute => dcterms:audience
	<xsl:template match="@audience">
		<xsl:if test=". != ''">
			<xsl:element name="dcterms:audience">
				<xsl:value-of select="normalize-space(.)" />
			</xsl:element>
		</xsl:if>
	</xsl:template> -->

	<!-- Traitement du eadheader, utilisé notamment pour sortir une notice à propos de l'instrument
			de recherche lui-même. -->
	<xsl:template match="eadheader | c[@pleade:original-element = 'eadheader']" mode="meta-oai">
		<!-- Traitement des enfants par la méthode explicite (@encodinganalog) -->
		<xsl:apply-templates mode="explicit"/>
		<!-- Traitement des enfants par la méthode implicit (tableau de correspondance) -->
		<xsl:apply-templates mode="implicit" />
	</xsl:template>

		<!-- eadid => dc:identifier -->
	<xsl:template match="eadid" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:identifier>
				<xsl:value-of select="." />
			</dc:identifier>
		</xsl:if>
			<!--
			<xsl:if test="@url != ''">
				<dc:identifier xsi:type="dcterms:URI">
					<xsl:value-of select="@url" />
				</dc:identifier>
			</xsl:if>-->
	</xsl:template>

		<!-- Concatenation to give a representative title -->
		<!-- titleproper + subtitle + edition => dc:title -->
<!--	<xsl:template match="filedesc/titlestmt" mode="meta-oai">
		<xsl:if test="titleproper != ''">
			<dc:title xml:lang="{$language}">
				<xsl:value-of select="normalize-space(titleproper)" />
				<xsl:if test="subtitle != ''">
					<xsl:text>&#160;: </xsl:text>
					<xsl:value-of select="normalize-space(subtitle)" />
				</xsl:if>
				<xsl:if test="//editionstmt/edition != ''">
					<xsl:text> - </xsl:text>
					<xsl:value-of select="normalize-space(//editionstmt/edition)" />
				</xsl:if>
			</dc:title>
		</xsl:if>
	</xsl:template>-->

		<!-- author => dc:creator -->
	<xsl:template match="filedesc/titlestmt/author" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:creator>
				<xsl:value-of select="normalize-space(.)" />
			</dc:creator>
		</xsl:if>
	</xsl:template>

		<!-- sponsor => dc:contributor -->
	<xsl:template match="filedesc/titlestmt/sponsor" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:contributor>
				<xsl:value-of select="normalize-space(.)" />
			</dc:contributor>
		</xsl:if>
	</xsl:template>

		<!-- note => dc:description -->
	<xsl:template match="filedesc/notestmt/note" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:value-of select="normalize-space(.)" />
			</dc:description>
		</xsl:if>
			<!--
			<xsl:for-each select="descendant::*">
				<xsl:message>NOTESTMT descripteurs <xsl:value-of select="name()" /></xsl:message>
				<xsl:apply-templates select="." />
			</xsl:for-each>
			-->
	</xsl:template>

		<!-- creation => dc:description
		<xsl:template match="profiledesc/creation">
			<xsl:if test=". != ''">
				<dc:description><xsl:value-of select="normalize-space(.)" /></dc:description>
			</xsl:if>
		</xsl:template> -->

		<!-- subject, famname, persname, corpname, name, occupation, function, title => dc:subject
		<xsl:template match="subject[not(@source = 'liste-siecles') and not(@source = 'liste-periodes')] | famname | corpname | persname | name | occupation | function | title" mode="note">
			<xsl:message>Desc</xsl:message>
			<xsl:if test=". != ''">
				<xsl:choose>
					<xsl:when test="@normal != ''">
						<dc:subject><xsl:value-of select="normalize-space(@normal)" /></dc:subject>
					</xsl:when>
					<xsl:otherwise><dc:subject><xsl:value-of select="normalize-space(.)" /></dc:subject></xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:template> -->

		<!-- geogname => dcterms:spatial
		<xsl:template match="geogname" mode="note">
			<xsl:message>Geogname</xsl:message>
			<xsl:if test=". != ''">
				<xsl:choose>
					<xsl:when test="@normal != ''">
						<dc:subject><xsl:value-of select="normalize-space(@normal)" /></dc:subject>
					</xsl:when>
					<xsl:otherwise><dc:subject><xsl:value-of select="normalize-space(.)" /></dc:subject></xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:template> -->

		<!-- subject[@source = 'liste-periodes' or @source = 'liste-siecles'] => dcterms:temporal
		<xsl:template match="subject[@source = 'liste-periodes' or @source = 'liste-siecles']" mode="note">
		<xsl:message>SUBjEct</xsl:message>
			<xsl:if test=". != ''">
				<xsl:choose>
					<xsl:when test="@normal != ''">
						<dcterms:temporal><xsl:value-of select="normalize-space(@normal)" /></dcterms:temporal>
					</xsl:when>
					<xsl:otherwise><dcterms:temporal><xsl:value-of select="normalize-space(.)" /></dcterms:temporal></xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:template> -->

		<!-- date => dcterms:temporal -->
	<xsl:template match="date[ancestor::notestmt]" mode="meta-oai">
		<dcterms:temporal>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:temporal>
	</xsl:template>

		<!-- publisher + @publicid + address => dc:publisher -->
	<xsl:template match="filedesc/publicationstmt/publisher" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:publisher>
				<xsl:value-of select="normalize-space(.)" />
				<xsl:if test="*//eadid/@publicid != ''">
					<xsl:text> (</xsl:text>
					<xsl:value-of select="normalize-space(*//eadid/@publicid)" />
					<xsl:text>)</xsl:text>
				</xsl:if>
				<xsl:if test="//address != ''">
					<xsl:text>&#160;: </xsl:text>
					<xsl:value-of select="normalize-space(//address)" />
				</xsl:if>
			</dc:publisher>
		</xsl:if>
	</xsl:template>

		<!-- creation/date => dcterms:created -->
	<xsl:template match="profiledesc/creation/date" mode="meta-oai">
		<dcterms:created>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:created>
	</xsl:template>

		<!-- publicationstmt/date => dcterms:issued -->
	<xsl:template match="filedesc/publicationstmt/date" mode="meta-oai">
		<dcterms:issued>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:issued>
	</xsl:template>

		<!-- revisiondesc/date => dcterms:modified -->
	<xsl:template match="revisiondesc/date" mode="meta-oai">
		<dcterms:modified>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:modified>
	</xsl:template>

		<!-- langusage => dc:language -->
	<xsl:template match="profiledesc/langusage/language" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:language>
				<xsl:value-of select="normalize-space(.)" />
			</dc:language>
		</xsl:if>
	</xsl:template>

		<!-- seriesstmt => relation.isPartOf -->
	<xsl:template match="filedesc/seriesstmt" mode="meta-oai">
		<xsl:if test=". != ''">
			<dcterms:isPartOf>
				<xsl:value-of select="normalize-space(.)" />
			</dcterms:isPartOf>
		</xsl:if>
	</xsl:template>

	<!-- **************************************************************
							FRONTMATTER element
		*************************************************************** -->
	<!-- Processing FRONTMATTER elements -->
	<xsl:template match="frontmatter" mode="meta-oai">
		<!-- Proceeding explicit method -->
		<xsl:comment> Explicit Method for frontmatter elements </xsl:comment>
		<xsl:apply-templates mode="explicit" />
		<!-- Proceeding implicit method -->
		<xsl:comment> Implicit Method for frontmatter elements </xsl:comment>
		<xsl:apply-templates mode="implicit" />
	</xsl:template>

		<!-- titleproper => dcterms:alternative
		<xsl:template match="titlepage/titleproper">
			<xsl:if test=". != ''">
				<dcterms:alternative>
					<xsl:value-of select="normalize-space(.)" />
					<xsl:if test="//subtitle != ''">
						<xsl:text>&#160;: </xsl:text>
						<xsl:value-of select="normalize-space(//subtitle)" />
					</xsl:if>
				</dcterms:alternative>
			</xsl:if>
		</xsl:template> -->

		<!-- author => dc:creator
		<xsl:template match="titlepage/author">
			<xsl:if test=". != ''">
				<dc:creator><xsl:value-of select="normalize-space(.)" /></dc:creator>
			</xsl:if>
		</xsl:template> -->

		<!-- publisher => dc:publisher
		<xsl:template match="titlepage/publisher">
			<xsl:if test=". != ''">
				<dc:publisher><xsl:value-of select="normalize-space(.)" /></dc:publisher>
			</xsl:if>
		</xsl:template> -->

		<!-- sponsor => dc:contributor
		<xsl:template match="titlepage/sponsor">
			<xsl:if test=". != ''">
				<dc:contributor><xsl:value-of select="normalize-space(.)" /></dc:contributor>
			</xsl:if>
		</xsl:template> -->

		<!-- titlepage/date => coverage.temporal
		<xsl:template match="titlepage/date">
			<xsl:if test=". != ''">
				<dcterms:temporal><xsl:value-of select="." /></dcterms:temporal>
			</xsl:if>
		</xsl:template> -->

		<!-- div => dc:description -->
	<xsl:template match="div[head = 'note' or head = 'note liminaire']" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates select="./*" mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template>

	<!-- *********************************************************************

							resource = archdesc

		********************************************************************** -->
	<!-- Create the Dublin Core qualified metadata for a collection of archives -->
	<xsl:template name="archdesc">
		<!-- Give a representative title -->
		<xsl:variable name="title">
			<xsl:call-template name="get-title">
				<xsl:with-param name="method" select="normalize-space($pleade-title)" />
				<xsl:with-param name="sep" select="$pleade-title-sep" />
				<xsl:with-param name="limit" select="number(normalize-space($pub-properties//property[@name = 'title-limit']))" />
			</xsl:call-template>
		</xsl:variable>
		<dc:title xml:lang="{$language}">
			<xsl:value-of select="$title" />
		</dc:title>

		<!-- @level => dc.type -->
		<xsl:if test="@level != ''">
			<dc:type>
				<xsl:value-of select="@level" />
			</dc:type>
		</xsl:if>

		<!-- Proceeding explicit method -->
		<xsl:comment> Explicit Method </xsl:comment>
		<xsl:apply-templates select="*[not(ancestor-or-self::dsc)]" mode="explicit" />
		<!-- Proceeding implicit method -->
		<xsl:comment> Implicit Method </xsl:comment>
		<xsl:apply-templates select="*[not(ancestor-or-self::dsc)]" mode="implicit" />
		<!-- Inherit descriptors -->
		<xsl:if test="$inheritDesc = 'true'">
			<xsl:apply-templates select="ancestor::*[@pleade:id]" mode="inherit-desc">
				<xsl:with-param name="id" select="@pleade:id" />
			</xsl:apply-templates>
		</xsl:if>

		<!-- Relations -->
		<xsl:comment> Relations </xsl:comment>
		<!-- Link to finding aid -->
		<xsl:variable name="linkFA">
			<xsl:call-template name="get-link">
				<xsl:with-param name="id" select="preceding-sibling::eadheader/eadid" />
				<xsl:with-param name="mode" select="$relationMode" />
				<xsl:with-param name="url-base" select="$baseURL" />
			</xsl:call-template>
		</xsl:variable>
		<dcterms:isReferencedBy>
			<xsl:value-of select="$linkFA" />
		</dcterms:isReferencedBy>

		<!-- Link to sub-resources -->
		<xsl:if test="$includehasPart = 'true'">
			<xsl:for-each select="dsc//c[contains(@encodinganalog, 'oai:resource')]">
				<xsl:variable name="link">
					<xsl:call-template name="get-link">
						<xsl:with-param name="id" select="@id" />
						<xsl:with-param name="mode" select="$relationMode" />
						<xsl:with-param name="url-base" select="$baseURL" />
					</xsl:call-template>
				</xsl:variable>
				<dcterms:hasPart>
					<xsl:value-of select="$link" />
				</dcterms:hasPart>
			</xsl:for-each>
		</xsl:if>

		<!-- Generated content -->
		<xsl:comment> Contenus générés </xsl:comment>
		<dc:type>collection</dc:type>
	
	</xsl:template>

	<!-- *********************************************************************

						resource = hierarchical level, c elements

		********************************************************************* -->

	<!-- processing c|c01|c02|c03|c04|c05|c06|c07|c08|c09|c10|c11|c12 which is a resource -->
	<xsl:template name="cN">

		<!-- Give a representative title -->
		<xsl:variable name="title">
			<xsl:call-template name="get-title">
        <xsl:with-param name="method" select="normalize-space($pleade-title)" />
        <xsl:with-param name="sep" select="$pleade-title-sep" />
        <xsl:with-param name="limit" select="number(normalize-space($pub-properties//property[@name = 'title-limit']))" />
			</xsl:call-template>
		</xsl:variable>
		<dc:title xml:lang="{$language}">
			<xsl:value-of select="normalize-space($title)" />
		</dc:title>

		<!-- @level => dc:type -->
		<xsl:if test="@level and @level != ''">
			<dc:type>
				<xsl:value-of select="@level" />
			</dc:type>
		</xsl:if>
		<xsl:if test="not(userestrict)">
			<xsl:call-template name="get-rights" />
		</xsl:if>
		<xsl:if test="not(accessrestrict)">
			<xsl:call-template name="get-accessrestrict" />
		</xsl:if>

		<!-- Generated content -->

		<!--<xsl:apply-templates select="*" mode="which-element" />-->
		<!-- Proceeding explicit method -->
		<xsl:comment> Explicit Method </xsl:comment>
		<xsl:apply-templates select="*[not(self::c) and not(self::c01) and not(self::c02) and not(self::c03) and not(self::c04) and not(self::c05) and not(self::c06) and not(self::c07) and not(self::c08) and not(self::c09) and not(self::c10) and not(self::c11) and not(self::c12)]" mode="explicit" />
		<!-- Proceeding implicit method -->
		<xsl:comment> Implicit Method </xsl:comment>
		<xsl:apply-templates select="*[not(self::c) and not(self::c01) and not(self::c02) and not(self::c03) and not(self::c04) and not(self::c05) and not(self::c06) and not(self::c07) and not(self::c08) and not(self::c09) and not(self::c10) and not(self::c11) and not(self::c12)]" mode="implicit" />
		<!-- Proceeding subcomponents -->
		<xsl:apply-templates select="c|c01|c02|c03|c04|c05|c06|c07|c08|c09|c10|c11|c12" mode="component" />

		<!-- Inherit descriptors -->
		<xsl:if test="$inheritDesc = 'true'">
			<xsl:apply-templates select="ancestor::*[@pleade:id]" mode="inherit-desc">
				<xsl:with-param name="id" select="@pleade:id" />
			</xsl:apply-templates>
		</xsl:if>

		<!-- Solution OK -->
		<!-- Proceeding descendant elements
		<xsl:for-each select="*">
			<xsl:choose>
				<xsl:when test="self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12">
					<xsl:apply-templates select="." mode="component" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:comment>Explicit Method</xsl:comment>
					<xsl:apply-templates select="." mode="explicit" />
					<xsl:comment>Implicit Method</xsl:comment>
					<xsl:apply-templates select="." mode="implicit" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each> -->

		<!-- Relations -->
		<xsl:comment> Relations </xsl:comment>
		<!-- Ancestry relation -->
		<xsl:for-each select="ancestor::*[contains(@encodinganalog, 'oai:resource') and not(self::ead)]">
			<xsl:variable name="link">
				<xsl:call-template name="get-link">
					<xsl:with-param name="id" select="@id" />
					<xsl:with-param name="mode" select="$relationMode" />
					<xsl:with-param name="url-base" select="$baseURL" />
				</xsl:call-template>
			</xsl:variable>
			<dcterms:isPartOf>
				<xsl:value-of select="$link" />
			</dcterms:isPartOf>
		</xsl:for-each>

		<!-- Link with finding aid -->
		<xsl:variable name="link">
			<xsl:call-template name="get-link">
				<xsl:with-param name="id" select="ancestor::ead/@pleade:id" />
				<xsl:with-param name="mode" select="$relationMode" />
				<xsl:with-param name="url-base" select="$baseURL" />
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$link != '.xml' and $link != $baseURL">
			<dcterms:isReferencedBy>
				<xsl:value-of select="$link" />
			</dcterms:isReferencedBy>
		</xsl:if>
	</xsl:template>

			<!--<xsl:template match="*" mode="which-element">
			<xsl:choose>
				<xsl:when test="self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12">
					<xsl:apply-templates select="." mode="component" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:comment>Explicit Method</xsl:comment>
					<xsl:apply-templates select="." mode="explicit" />
					<xsl:comment>Implicit Method</xsl:comment>
					<xsl:apply-templates select="." mode="implicit" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:template>
		<xsl:template match="text()" mode="which-element" />-->

	<!-- Proceeding descendant elements
	<xsl:template match="c | c01 | c02 | c03 | c04 | c05 | c06 | c07 | c08 | c09 | c10 | c11 | c12">
		<xsl:apply-templates select="." mode="component" />
	</xsl:template>-->

	<!-- Process for c subcomponent element -->
	<xsl:template match="c|c01|c02|c03|c04|c05|c06|c07|c08|c09|c10|c11|c12" mode="component">
		<!--finding the good title for the component element-->
		<xsl:variable name="title">
			<xsl:call-template name="get-title">
				<!--<xsl:with-param name="choosen-title" select="$pleade-title" />-->
				<xsl:with-param name="sep" select="$pleade-title-sep" />
				<!--<xsl:with-param name="l" select="$language" />-->
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:choose>
			<!-- Is a resource -->
			<xsl:when test="contains(@encodinganalog, 'oai:resource')">
				<xsl:if test="$includehasPart = 'true'">
					<xsl:variable name="link">
						<xsl:call-template name="get-link">
							<xsl:with-param name="id" select="@id" />
							<xsl:with-param name="mode" select="$relationMode" />
							<xsl:with-param name="url-base" select="$baseURL" />
						</xsl:call-template>
					</xsl:variable>
					<!-- Descent relation = sub-resource -->
					<dcterms:hasPart>
						<xsl:value-of select="$link" />
					</dcterms:hasPart>
				</xsl:if>
				<!--<xsl:apply-templates select="*" mode="component" />-->
			</xsl:when>
			<!-- Not a resource -->
			<xsl:otherwise>
				<!-- Repatriate descriptors -->
				<xsl:comment> Descripteurs rapatriés du composant archivistique <xsl:value-of select="@id" />
				</xsl:comment>
				<xsl:apply-templates select="*" mode="repatriate" />
				<!-- Proceed other descendant -->
				<xsl:apply-templates select="*" mode="component" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="dsc" mode="component">
		<xsl:apply-templates mode="component" />
	</xsl:template>
	
	<xsl:template match="node()" mode="component" />

	<!-- *********************************************************************
							Common templates
		********************************************************************* -->

	<!-- prefercite => dcterms:bibliographicCitation -->
	<xsl:template match="prefercite" mode="meta-oai">
		<xsl:if test=". != ''">
			<dcterms:biblioraphicCitation>
				<xsl:value-of select="normalize-space(.)" />
			</dcterms:biblioraphicCitation>
		</xsl:if>
	</xsl:template>

	<!-- did => depend on subelements (dc:title, dc:date, dc:creator, dcterms:abstract, dc:publisher) -->
	<xsl:template match="did" mode="meta-oai">
		<xsl:apply-templates select="child::*" mode="meta" />
		<xsl:if test="not(repository)">
			<xsl:call-template name="get-repository" />
		</xsl:if>
		<xsl:if test="not(unitid)">
			<xsl:call-template name="get-unitid" />
		</xsl:if>
		<xsl:if test="not(origination)">
			<xsl:call-template name="get-origination" />
		</xsl:if>
		<xsl:if test="not(descendant::unitdate)">
			<xsl:call-template name="get-date" />
		</xsl:if>
		<xsl:if test="not(langmaterial/language)">
			<xsl:call-template name="get-language" />
		</xsl:if>
	</xsl:template>

		<!-- did/unittitle => dc:title -->
		<!-- Redundant with the representative title
			<xsl:template match="unittitle" mode="meta">
				<xsl:if test=". != ''">
					<dc:title><xsl:value-of select="normalize-space(.)" /></dc:title>
				</xsl:if>
			</xsl:template>
		-->

		<!-- did/unittitle/unitdate => dc:date -->
	<xsl:template match="unitdate" mode="meta">
		<dc:date>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:date>
	</xsl:template>

		<!-- did/unitid => dc:identifier -->
	<xsl:template match="unitid" mode="meta">
		<xsl:if test=". != ''">
			<dc:identifier>
				<xsl:choose>
						<!-- cN/did/unitid -->
					<xsl:when test="ancestor::*[self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12][2]">
						<xsl:choose>
								<!-- @type = 'cote-de-consultation' -->
							<xsl:when test="@type = 'cote-de-consultation'">
								<xsl:value-of select="." />
							</xsl:when>
								<!-- @type != 'cote-de-consultation' -->
							<xsl:otherwise>
									<!-- Up to the first unitid with @type="cote-de-consultation" -->
								<xsl:variable name="begin">
									<xsl:value-of select="ancestor::*[self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12]/did/unitid[@type = 'cote-de-consultation'][1]" />
								</xsl:variable>
								<xsl:choose>
										<!-- $begin != '' -->
									<xsl:when test="$begin != ''">
										<xsl:value-of select="concat($begin, ' - ', .)" />
									</xsl:when>
										<!-- $begin = '', maybe no @type="cote-de-consultation" above -->
									<xsl:otherwise>
										<xsl:value-of select="." />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
						<!-- archdesc -->
					<xsl:otherwise>
						<xsl:value-of select="." />
					</xsl:otherwise>
				</xsl:choose>
					<!--<xsl:value-of select="." />-->
			</dc:identifier>
		</xsl:if>
	</xsl:template>

		<!-- did/origination => dc:creator -->
	<xsl:template match="origination" mode="meta">
		<xsl:if test=". != ''">
			<dc:creator>
				<xsl:choose>
					<xsl:when test="*[self::famname or self::corpname or self::name or self::persname]">
						<xsl:for-each select="famname | corpname | name | persname">
							<xsl:value-of select="pleade:get-normalized-form(.)" />
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="normalize-space(.)" />
					</xsl:otherwise>
				</xsl:choose>
			</dc:creator>
		</xsl:if>
	</xsl:template>

		<!-- did/abstract => dcterms:abstract -->
	<xsl:template match="abstract" mode="meta">
		<xsl:if test=". != ''">
			<dcterms:abstract>
				<xsl:apply-templates mode="text" />
			</dcterms:abstract>
		</xsl:if>
	</xsl:template>

		<!-- did/repository => dc:publisher -->
	<xsl:template match="repository" mode="meta">
		<xsl:if test=". != ''">
			<dc:publisher>
				<xsl:value-of select="normalize-space(.)" />
			</dc:publisher>
		</xsl:if>
	</xsl:template>

		<!-- did/langmaterial/language => dc:language -->
	<xsl:template match="langmaterial" mode="meta">
		<xsl:if test="language != ''">
			<dc:language>
				<xsl:value-of select="normalize-space(language)" />
			</dc:language>
		</xsl:if>
	</xsl:template>

		<!-- did/physdesc => depends on structure -->
	<xsl:template match="physdesc" mode="meta">
		<xsl:choose>
			<xsl:when test="*[self::extent or self::dimensions or self::genreform or self::physfacet]">
				<xsl:for-each select="*">
					<xsl:if test=". != ''">
						<xsl:if test="self::extent or self::dimensions">
							<dcterms:extent>
								<xsl:value-of select="normalize-space(.)" />
							</dcterms:extent>
						</xsl:if>
						<xsl:if test="self::genreform">
							<dc:type>
								<xsl:value-of select="pleade:get-normalized-form(.)" />
							</dc:type>
						</xsl:if>
						<xsl:if test="self::physfacet">
							<xsl:call-template name="physfacet" />
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test=". != ''">
					<dc:format>
						<xsl:value-of select="normalize-space(.)" />
					</dc:format>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- physfacet => dc:format or dcterms:medium -->
	<xsl:template name="physfacet">
		<xsl:choose>
			<xsl:when test="@type = 'support'">
				<dcterms:medium>
					<xsl:value-of select="normalize-space(.)" />
				</dcterms:medium>
			</xsl:when>
			<xsl:otherwise>
				<dc:format>
					<xsl:value-of select="normalize-space(.)" />
				</dc:format>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- scopecontent => dc:description -->
	<xsl:template match="scopecontent" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates select="node()" mode="text" />
			</dc:description>
		</xsl:if>
		<!--
		<xsl:for-each select="descendant::*[self::persname or self::occupation or (self::subject and (not(@source = 'liste-siecles') or not(@source = 'liste-periodes'))) or self::corpname or self::famname or self::name or self::function or self::title]">
			<xsl:if test=". != '' or @normal != ''">
				<xsl:element name="dc:subject">
					<xsl:choose>
						<xsl:when test="@normal != ''">
							<xsl:value-of select="@normal" />
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="descendant::geogname">
			<xsl:if test=". != '' or @normal != ''">
				<dcterms:spatial>
					<xsl:choose>
						<xsl:when test="@normal != ''">
							<xsl:value-of select="@normal" />
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise>
					</xsl:choose>
				</dcterms:spatial>
			</xsl:if>
		</xsl:for-each>
		-->
	</xsl:template>

	<!-- termes d'indexation => dc:subject -->
	<xsl:template match="persname[not(parent::origination)] | occupation | subject[not(@source = 'liste-periodes') and not(@source = 'liste-siecles')] | corpname[not(parent::origination)] | famname[not(parent::origination)] | name[not(parent::origination)] | function | title[not(parent::bibref)]" mode="meta-oai">
		<dc:subject>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:subject>
	</xsl:template>

	<!-- geogname => dcterms:spatial -->
	<xsl:template match="geogname" mode="meta-oai">
		<dcterms:spatial>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:spatial>
	</xsl:template>

	<!-- genreform => dc:type -->
	<xsl:template match="genreform" mode="meta-oai">
		<dc:type>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:type>
	</xsl:template>

	<!-- subject[@source = 'liste-periodes' or @source = 'liste-siecles'] => dcterms:temporal -->
	<xsl:template match="subject[@source = 'liste-periodes' or @source = 'liste-siecles']" mode="meta-oai">
		<dcterms:temporal>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:temporal>
	</xsl:template>

	<!-- controlaccess => dc:subject | dcterms:spatial -->
	<!--<xsl:template match="controlaccess">
		<xsl:for-each select="descendant::*[self::persname or self::occupation or self::subject[@source != 'liste-periodes' or @source != 'liste-siecles'] or self::corpname or self::famname or self::name or self::function or self::title]">
			<xsl:if test=". != '' or @normal != ''">
				<xsl:element name="dc:subject">
					<xsl:choose>
						<xsl:when test="@normal != ''">
							<xsl:value-of select="@normal" />
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="descendant::geogname">
			<xsl:if test=". != '' or @normal != ''">
				<dcterms:spatial>
					<xsl:choose>
						<xsl:when test="@normal != ''">
							<xsl:value-of select="@normal" />
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise>
					</xsl:choose>
				</dcterms:spatial>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="descendant::subject[@source = 'liste-periodes' or @source = 'liste-siecles']">
			<xsl:if test=". != '' or @normal != ''">
				<xsl:element name="dcterms:temporal">
					<xsl:choose>
						<xsl:when test="@normal != ''">
							<xsl:value-of select="@normal" />
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
			</xsl:template>-->

	<!-- accessrestrict => dcterms.accessRights -->
	<xsl:template match="accessrestrict" mode="meta-oai">
		<xsl:if test=". != ''">
			<dcterms:accessRights>
				<xsl:apply-templates mode="text" />
			</dcterms:accessRights>
		</xsl:if>
	</xsl:template>

	<!-- accruals => dc.description
	<xsl:template match="accruals">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template> -->

	<!-- acqinfo => dc.description
	<xsl:template match="acqinfo">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template> -->

	<!-- altformavail => dcterms.hasFormat -->
	<xsl:template match="altformavail" mode="meta-oai">
		<xsl:if test=". != ''">
			<dcterms:hasFormat>
				<xsl:apply-templates mode="text" />
			</dcterms:hasFormat>
		</xsl:if>
	</xsl:template>

	<!-- appraisal => dc.description
	<xsl:template match="appraisal">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template> -->

	<!-- arrangement => -->

	<!-- bibliography => dcterms.references -->
	<xsl:template match="bibliography" mode="meta-oai">
		<xsl:for-each select="descendant::bibref">
			<xsl:if test=". != ''">
				<dcterms:references>
					<xsl:apply-templates mode="text" />
				</dcterms:references>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- bioghist => dc.description
	<xsl:template match="bioghist">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template> -->


	<!-- custodhist => dc.description
	<xsl:template match="custodhist">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template> -->

	<!-- dao => dcterms:hasFormat -->
	<xsl:template match="dao" mode="meta-oai">
		<xsl:if test="@href != ''">
      <xsl:call-template name="select-display"/>
		</xsl:if>
	</xsl:template>

	<!-- daogrp => dcterms:hasFormat -->
	<xsl:template match="daogrp" mode="meta-oai">
		<xsl:for-each select="daoloc[@href != '']">
      <xsl:call-template name="select-display"/>
		</xsl:for-each>
	</xsl:template>

	<!-- descgrp => apply child template -->
	<xsl:template match="descgrp" mode="meta-oai">
		<xsl:apply-templates select="child::*" />
	</xsl:template>

	<!-- odd => dc:description
	<xsl:template match="odd">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template> -->

	<!-- originalsloc => dc:description
	<xsl:template match="originalsloc">
		<xsl:if test=". != ''">
			<dc:description>
				<xsl:apply-templates mode="text" />
			</dc:description>
		</xsl:if>
	</xsl:template>	 -->

	<!-- otherfindaid => dcterms:references -->
	<xsl:template match="otherfindaid" mode="meta-oai">
		<xsl:if test=". != ''">
			<dcterms:references>
				<xsl:value-of select="normalize-space(.)" />
			</dcterms:references>
		</xsl:if>
	</xsl:template>

	<!-- relatedmaterial | separatedmaterial => dc:relation -->
	<!-- TODO : add message to specify the nature of the non-qualified relation.
			For example: Related material : for <relatedmaterial> -->
	<!--
	<xsl:template match="relatedmaterial | separatedmaterial ">
		<xsl:choose>
			<xsl:when test="child::relatedmaterial">
				<xsl:apply-templates select="relatedmaterial" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="descendant::archref">
						<xsl:for-each select="descendant::archref[name(parent::*) != 'unitid']">
							<xsl:if test=". != ''">
								<dc:relation>
									<xsl:value-of select="normalize-space(preceding::head[1])" />
									<xsl:text> - </xsl:text>
									<xsl:value-of select="normalize-space(.)" />
								</dc:relation>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<dc:relation><xsl:value-of select="normalize-space(.)" /></dc:relation>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>-->

	<!-- userestrict => dc:rights -->
	<xsl:template match="userestrict" mode="meta-oai">
		<xsl:if test=". != ''">
			<dc:rights>
				<xsl:apply-templates mode="text" />
			</dc:rights>
		</xsl:if>
	</xsl:template>


	<!-- *********************************************************************
								Specific templates
		********************************************************************* -->

	<!-- Get the correct link form through the method given by relationMode parameter -->
	<xsl:template name="get-link">
		<xsl:param name="id" select="." />
		<xsl:param name="mode" select="$relationMode" />
		<xsl:param name="url-base" select="$baseURL" />
		<xsl:choose>
			<xsl:when test="$mode = 'filename'">
				<xsl:value-of select="concat($id, '.xml')" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($url-base, $id)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- *********************** Repatriation of descriptors ************************** -->

	<!-- repatriate indexation terms => dc:subject -->
	<xsl:template match="persname[not(parent::origination)] | occupation | subject[not(@source = 'liste-periodes') and not(@source = 'liste-siecles')] | corpname[not(parent::origination)] | famname[not(parent::origination)] | name[not(parent::origination)] | function | title[not(parent::bibref)]" mode="repatriate">
		<dc:subject>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:subject>
	</xsl:template>

	<!-- repatriate geogname => dcterms:spatial -->
	<xsl:template match="geogname" mode="repatriate">
		<dcterms:spatial>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:spatial>
	</xsl:template>

	<!-- repatriate genreform => dc:type -->
	<xsl:template match="genreform" mode="repatriate">
		<dc:type>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:type>
	</xsl:template>

	<!-- repatriate subject[@source = 'liste-periodes' or @source = 'liste-siecles'] => dcterms:temporal -->
	<xsl:template match="subject[@source = 'liste-periodes' or @source = 'liste-siecles']" mode="repatriate">
		<dcterms:temporal>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:temporal>
	</xsl:template>
	
	<xsl:template match="*" mode="repatriate">
		<xsl:if test="not(contains(@encodinganalog, 'oai:resource'))">
			<xsl:apply-templates select="*" mode="repatriate" />
		</xsl:if>
	</xsl:template>

	<!-- *************************** Inheritance ******************************* -->

	<!-- select elements to inherit -->
	<xsl:template match="*" mode="inherit-desc">
		<xsl:param name="id" select="''" />
		<xsl:if test="not(self::ead)">
			<!--proceeding the cN, archdesc-->
			<xsl:comment> Descripteurs hérités </xsl:comment>
			<xsl:apply-templates select="*" mode="inherit" />
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="text()" mode="inherit" />
	<xsl:template match="c|c01|c02|c03|c04|c05|c06|c07|c08|c09|c10|c11|c12" mode="inherit" />

	<!-- inherit indexation terms => dc:subject -->
	<xsl:template match="persname[not(parent::origination)] | occupation | subject[not(@source = 'liste-periodes') and not(@source = 'liste-siecles')] | corpname[not(parent::origination)] | famname[not(parent::origination)] | name[not(parent::origination)] | function | title[not(parent::bibref)]" mode="inherit">
		<!--<xsl:message>Desc herite</xsl:message>-->
		<dc:subject>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:subject>
	</xsl:template>

	<!-- inherit geogname => dcterms:spatial -->
	<xsl:template match="geogname" mode="inherit">
		<dcterms:spatial>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:spatial>
	</xsl:template>

	<!-- inherit genreform => dc:type -->
	<xsl:template match="genreform" mode="inherit">
		<dc:type>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dc:type>
	</xsl:template>

	<!-- inherit subject[@source = 'liste-periodes' or @source = 'liste-siecles'] => dcterms:temporal -->
	<xsl:template match="subject[@source = 'liste-periodes' or @source = 'liste-siecles']" mode="inherit">
		<dcterms:temporal>
			<xsl:value-of select="pleade:get-normalized-form(.)" />
		</dcterms:temporal>
	</xsl:template>

	<!-- inherit publicationstmt/publisher and if different ancestor::*/did/repository -->
	<xsl:template name="get-repository">
		<!-- publicationstmt/publisher -->
		<xsl:comment> Publisher hérité </xsl:comment>
		<xsl:variable name="value" select="normalize-space(//publicationstmt/publisher)" />
		<xsl:variable name="publicid" select="//eadid/@publicid" />
		<xsl:variable name="address" select="//publicationstmt/address" />
		<xsl:if test="$value != ''">
			<dc:publisher>
				<xsl:value-of select="$value" />
				<xsl:if test="$publicid != ''">
					<xsl:text> (</xsl:text>
					<xsl:value-of select="normalize-space($publicid)" />
					<xsl:text>)</xsl:text>
				</xsl:if>
				<xsl:if test="$address != ''">
					<xsl:text>&#160;: </xsl:text>
					<xsl:value-of select="normalize-space($address)" />
				</xsl:if>
			</dc:publisher>
		</xsl:if>
	<!-- did/repository ancestors -->
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12]/did/repository[normalize-space(.) != $value]">
			<xsl:comment> Repository hérité </xsl:comment>
			<dc:publisher>
				<xsl:value-of select="normalize-space(.)" />
			</dc:publisher>
		</xsl:for-each>
	</xsl:template>

	<!-- inherit the first ancestor unitid/@type = 'cote-de-consultation' -->
	<xsl:template name="get-unitid">
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12]/did/unitid[@type = 'cote-de-consultation'][1]">
			<xsl:comment> Unitid hérité </xsl:comment>
			<xsl:if test=". != ''">
				<dc:identifier>
					<xsl:value-of select="." />
				</dc:identifier>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- inherit the first ancestor origination -->
	<xsl:template name="get-origination">
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12]/did/origination[1]">
			<xsl:comment> Origination héritée </xsl:comment>
			<xsl:if test=". != ''">
				<dc:creator>
					<xsl:choose>
						<xsl:when test="*[self::famname or self::corpname or self::name or self::persname]">
							<xsl:for-each select="famname | corpname | name | persname">
								<xsl:value-of select="pleade:get-normalized-form(.)" />
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="normalize-space(.)" />
						</xsl:otherwise>
					</xsl:choose>
				</dc:creator>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- inherit the first ancestor date -->
	<xsl:template name="get-date">
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12][did//unitdate][1]">
			<xsl:comment> Date héritée </xsl:comment>
			<xsl:apply-templates select="did//unitdate" mode="meta" />
			<!--
			<xsl:if test=". != '' or @normal != ''">
				<dc:date>
					<xsl:variable name="value">
						<xsl:call-template name="get-normalized-form" />
					</xsl:variable>
					<xsl:value-of select="$value" />
				</dc:date>
			</xsl:if>
			-->
		</xsl:for-each>
	</xsl:template>

	<!-- inherit the first langmaterial ancestor or generate fixed content -->
	<xsl:template name="get-language">
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12]/did/langmaterial[1]">
			<xsl:comment> Langage hérité </xsl:comment>
			<xsl:if test="language != ''">
				<dc:language>
					<xsl:value-of select="language" />
				</dc:language>
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="not(ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12]/did/langmaterial/language)">
			<xsl:comment> Valeur fixée à défaut de pouvoir être héritée </xsl:comment>
			<dc:language>fre</dc:language>
		</xsl:if>
	</xsl:template>

	<!-- inherit the first userestrict ancestor -->
	<xsl:template name="get-rights">
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12][userestrict][1]">
			<xsl:comment> Userestrict hérité </xsl:comment>
			<xsl:if test="userestrict != ''">
				<dc:rights>
					<xsl:value-of select="normalize-space(userestrict)" />
				</dc:rights>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- inherit the first accessrestrict ancestor -->
	<xsl:template name="get-accessrestrict">
		<xsl:for-each select="ancestor::*[self:: archdesc or self::c or self::c01 or self::c02 or self::c03 or self::c04 or self::c05 or self::c06 or self::c07 or self::c08 or self::c09 or self::c10 or self::c11 or self::c12][accessrestrict][1]">
			<xsl:comment> Accessrestrict hérité </xsl:comment>
			<xsl:if test="accessrestrict != ''">
				<dcterms:accessRights>
					<xsl:value-of select="normalize-space(accessrestrict)" />
				</dcterms:accessRights>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!--  ********************************************************************
							Editorial elements : head, p
		********************************************************************** -->
	<xsl:template match="head" mode="text">
		<xsl:value-of select="normalize-space(.)" />
		<xsl:text>&#160;: </xsl:text>
	</xsl:template>
	
	<xsl:template match="text()" mode="text">
		<xsl:value-of select="normalize-space(.)" />
	</xsl:template>
	
	<xsl:template match="p" mode="text">
		<xsl:apply-templates mode="text" />
		<xsl:if test="following-sibling::p">
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
	</xsl:template>

	<!-- default template : to mask the content of elements with no specific template -->
	<xsl:template match="@* | node()" mode="meta-oai"></xsl:template>
	
	<xsl:template match="@* | node()" mode="note"></xsl:template>

	
	<xsl:template match="@* | node()" mode="meta"></xsl:template>
	<!--<xsl:template match="node()" mode="repatriate" />-->
	<xsl:template match="text()" mode="method" />
	
	<xsl:template match="text()" mode="implicit" />
	<xsl:template match="text()" mode="explicit" />


  <!--  ********************************************************************
              links elements : dao, daoloc
    ********************************************************************** -->

  <!-- On traite les différents objets numériques que l'on peut avoir dans des dao|daoloc -->
  <xsl:template name="select-display">
    <!-- L'adresse publique du document -->
    <xsl:variable name="public-url" select="pleade:get-public-url(., @pleade:link-type, $eadid)"/>

    <!--  Le HTML va dépendre du type d'objet -->
      <xsl:choose>

        <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
        <!-- Les images, en série ou non, on va afficher un aperçu et un lien pour voir en grand écran -->
        <xsl:when test="@pleade:link-type!='absolute' and (@pleade:link-type = 'series' or @pleade:mime-type = 'image/jpeg' or @pleade:mime-type = 'image/gif' or @pleade:mime-type = 'image/png')">

          <xsl:apply-templates select="." mode="pleade-image"/>
        </xsl:when>


        <!-- Pour les images absolues, on affiche la vignette avec un lien hypertexte pour ouvrir une popup. -->
        <xsl:when test="starts-with(@pleade:mime-type, 'image/')">
          <xsl:call-template name="show-hasFormat">
            <xsl:with-param name="viewer-url" select="@pleade:url" />
          </xsl:call-template>
        </xsl:when>



      </xsl:choose>
  </xsl:template>

  <!-- Affichage des images prises en charge par la visionneuse ou le serveur d'images -->
  <xsl:template match="dao | daoloc" mode="pleade-image">
  
    <xsl:if test="$isDebug"><xsl:message>      Traite <xsl:value-of select="name()"/> mode pleade-image</xsl:message></xsl:if>
  
    <!-- L'adresse publique du document -->
    <xsl:variable name="public-url" select="pleade:get-public-url(., @pleade:link-type, $eadid)"/>

    <xsl:variable name="thumb" select="if(../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then ../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'][1] else self::*" />

    <xsl:variable name="eadimage-path" select="concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', @pleade:url, 'eadimage.xml?pleade-url=', @pleade:url, '&amp;pleade-img=', @pleade:img)"/>
    <xsl:if test="$isDebug"><xsl:message>        Loading: <xsl:value-of select="concat($eadimage-path,' (', current-time(), ')')"/></xsl:message></xsl:if>
    <xsl:variable name="eadimage" select="if($thumb/@pleade:link-type = 'series' and doc-available($eadimage-path)) then document($eadimage-path)/result/eadimage else ()"/>
    <xsl:if test="$isDebug"><xsl:message>        Loaded: <xsl:value-of select="concat($eadimage-path,' (', current-time(), ')')"/></xsl:message></xsl:if>

    <xsl:choose>
      <xsl:when test="@pleade:link-type!='attached' and @pleade:link-type!='relative' and not($eadimage/@find='true')">
        <xsl:if test="$isDebug"><xsl:message>        Image non attachee et non relative introuvable dans les racines</xsl:message></xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$isDebug"><xsl:message>          Requesting function pleade:thumbnailURL(<xsl:value-of select="concat($thumb,',',$eadimage,',',$public-url)"/>)</xsl:message></xsl:if>
        <!-- L'URL de la vignette est donnée par une fonction -->
        <xsl:variable name="thumbnail-url" select="pleade:thumbnailURL($thumb, $eadimage, $public-url)"/>
        <xsl:if test="$isDebug"><xsl:message>          Ending function pleade:thumbnailURL = <xsl:value-of select="$thumbnail-url"/></xsl:message></xsl:if>
        <!-- SI le rôle est 'first', on cherche la présence d'un 'last' pour le passer également -->
        <xsl:variable name="last" select="if(@role='first' or @role='image:first' or @role='navimages:image:first') then ../*[@role='last' or @role='image:last' or @role='navimages:image:last'][1] else ()"/>
        <!-- On va afficher un lien vers la visionneuse -->
        <xsl:variable name="viewer-url" select="pleade:linkToViewer(., $thumb, $eadimage, $last)"/>
        <xsl:if test="$isDebug"><xsl:message>          Ending function pleade:linkToViewer = <xsl:value-of select="$viewer-url"/></xsl:message></xsl:if>
        <xsl:if test="$isDebug"><xsl:message>          Requesting template medias-viewer-output</xsl:message></xsl:if>
        <xsl:call-template name="show-hasFormat">
          <xsl:with-param name="viewer-url" select="$viewer-url" />
        </xsl:call-template>
        <xsl:if test="$isDebug"><xsl:message>        Ending template medias-viewer-output</xsl:message></xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:if test="$isDebug"><xsl:message>        Traite <xsl:value-of select="name()"/> mode pleade-image OK</xsl:message></xsl:if>

  </xsl:template>



  <!-- Traitement du lien pour l'affichage d'une image dans la visionneuse Pleade. -->
  <xsl:template name="show-hasFormat">
    <xsl:param name="viewer-url" />
    <dcterms:hasFormat><xsl:value-of select="$baseURL"/><xsl:value-of select="$viewer-url"/></dcterms:hasFormat>
  </xsl:template>

  <!--
      | Fonctions pour l'affichage d'une vignette dans la fenêtre EAD
      |  Ces fonctions peuvent être réutilisées pour afficher sous une autre forme les images liées
      -->
  <!--
        Fonction qui retourne l'URL de la vignette
        Les paramètres sont :
          $thumb : dao ou daoloc qui sert pour la vignette
          $eadimage : les caractéristiques de l'image renvoyées par le serveur d'images
          $public-url = pleade:get-public-url(., @pleade:link-type, $eadid)
        Les variables suivantes doivent aussi être définies :
          $public-img-server-base : URL publique du serveur d'images
          $active-img-thumbnail-size : hauteur ou largeur de la vignette
      -->
  <xsl:function name="pleade:thumbnailURL" as="xs:string">
    <xsl:param name="thumb"/>
    <xsl:param name="eadimage"/>
    <xsl:param name="public-url"/>

    <xsl:choose>

      <xsl:when test="$thumb/@pleade:link-type = 'series'">
        <!-- On interroge le serveur d'images pour récupérer ce qui va bien -->
        <xsl:value-of select="concat($public-img-server-base, $eadimage/@src)"/>
      </xsl:when>

      <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
      <!-- Pour les URL absolues on passe par le serveur d'images -->
      <!--<xsl:when test="$thumb/@pleade:link-type = 'absolute'">
        <xsl:value-of select="concat($public-img-server-base, '_functions/image.', pleade:get-extension(@pleade:url), '?url=', $public-url, '&amp;w=', $active-img-thumbnail-size, '&amp;h=', $active-img-thumbnail-size)"/>
      </xsl:when>-->
      <!-- Pour les images relatives, on a un pipeline qui s'en occupe dans le module serveur d'images. -->
      <xsl:when test="$thumb/@pleade:link-type = 'relative'">
        <xsl:value-of select="concat($public-img-server-base,if($thumb[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then pleade:get-public-url($thumb, $thumb/@pleade:link-type, $eadid) else 'ead-docs-dir/',$thumb/@pleade:url, '?w=', $active-img-thumbnail-size, '&amp;h=', $active-img-thumbnail-size)"/>
      </xsl:when>
      <!-- Pour les autres (attached) on passe par un processus Cocoon -->
      <xsl:otherwise>
        <xsl:value-of select="concat('functions/images/resize/', if($thumb[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then pleade:get-public-url($thumb, $thumb/@pleade:link-type, $eadid) else $public-url, '?w=', $active-img-thumbnail-size)"/><!-- INFO (MP) : Ici pas de 'h' car le reader Cocoon chargé du redimensionnement ne sait pas travailler avec les 2 infos (w et h). -->
      </xsl:otherwise>

    </xsl:choose>
  </xsl:function>

  <!--
        Fonction qui retourne la cible du lien vers la visionneuse
        Les paramètres sont :
          $el : élément courant (dao | daoloc)
          $thumb : dao ou daoloc qui sert pour la vignette
          $eadimage : les caractéristiques de l'image renvoyées par le serveur d'images
        Les variables suivantes doivent aussi être définies :
          $img-viewer-base : URL relative de la visionneuse
      -->
  <xsl:function name="pleade:linkToViewer">
    <xsl:param name="el"/>
    <xsl:param name="thumb"/>
    <xsl:param name="eadimage"/>
    <xsl:param name="last"/>

    <xsl:choose>
      <xsl:when test="$el/@pleade:link-type = 'series'">
        <!-- Le début est toujours pareil -->

        <xsl:value-of select="concat($img-viewer-base, $eadimage/@href)"/>
        <xsl:choose>
          <xsl:when test="$el/@role='image' or $el/@role='navimages:image'">
            <xsl:value-of select="concat('?name=', $el/@pleade:img)"/>
          </xsl:when>
          <xsl:when test="$el/@role='first' or $el/@role='image:first' or $el/@role='navimages:image:first'">
            <xsl:value-of select="concat('?np=', $el/@pleade:img)" />
            <xsl:if test="$last">
              <xsl:value-of select="concat('&amp;nd=', $last/@pleade:img)" />
            </xsl:if>
            <xsl:if test="$thumb">
              <xsl:value-of select="concat('&amp;ns=', $thumb/@pleade:img)" />
            </xsl:if>
          </xsl:when>
          <xsl:when test="$thumb[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'] and $thumb/@pleade:url = $el/@pleade:url">
            <xsl:value-of select="concat('?ns=', $thumb/@pleade:img)" />
          </xsl:when>
          <!-- Si on pointe sur une image, on ajoute un ns= -->
          <xsl:when test="$el/@pleade:img!=''">
            <xsl:value-of select="concat('?ns=', $el/@pleade:img)"/>
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <!-- Une image relative -->
      <xsl:when test="$el/@pleade:link-type = 'relative'">
        <xsl:value-of select="concat($img-viewer-base, '_ead/', pleade:get-public-url($el, $el/@pleade:link-type, $eadid),'/viewer.html')" />
      </xsl:when>
      <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
      <!-- Une image absolue -->
      <!--<xsl:when test="@pleade:link-type = 'absolute'">
        <xsl:value-of select="@pleade:url" />
      </xsl:when>-->
      <xsl:otherwise>
        <!-- Une image attachée -->
        <xsl:value-of select="concat($img-viewer-base, '_ead/', pleade:get-public-url($el, $el/@pleade:link-type, $eadid), '/viewer.html')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>


</xsl:stylesheet>
