/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation.index;

import org.apache.avalon.framework.logger.LogEnabled;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Groups index entries (terms) to decrease list size.
 */
public interface Grouper extends LogEnabled {

    /**
     * Receives a new term to group.
     * 
     * @param	t		The new term.
     * @param	hdl		The destination handler.
     * 
     * @throws SAXException
     */
    public void group(Term t, ContentHandler hdl) throws SAXException ;
    
    /**
     * Initialize the grouper; should be called before sending any term to group.
     * 
     * @param	hdl		The destination handler.
     * 
     * @throws SAXException
     */
    public void init(ContentHandler hdl) throws SAXException;
    
    /**
     * Finalize the grouper; should be called after sending all the terms to group.
     * 
     * @param	hdl		The destination handler.
     * 
     * @throws SAXException
     */
    public void finalize(ContentHandler hdl) throws SAXException;

}
