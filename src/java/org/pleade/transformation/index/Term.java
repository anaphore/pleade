/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation.index;

import java.util.Hashtable;

import org.apache.avalon.framework.logger.AbstractLogEnabled;
import org.apache.avalon.framework.logger.LogEnabled;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;


public class Term extends AbstractLogEnabled implements LogEnabled {
	private String value;
	private Hashtable docIds = new Hashtable();
	private String id = null;
	
	public Term(String v) {
		this.value = v;
	}
	public Term(String v, String id) {
		this.value = v;
		addDocId(id);
	}
	public void addDocId(String id) {
		if ( id != null && docIds.get(id) == null ) docIds.put(id, id);
	}
	
	public void toSAX(ContentHandler hdl) throws SAXException {
	    int size = docIds.size();
	    AttributesImpl atts = new AttributesImpl();
	    atts.addAttribute(IndexBuilder.NAMESPACE, "nb", "nb", "CDATA", "" + size);
	    atts.addAttribute(IndexBuilder.NAMESPACE, "id", "id", "CDATA", getId());
	    if (size == 1) {
	        String firstId = (String)docIds.keys().nextElement();
	        atts.addAttribute(IndexBuilder.NAMESPACE, "target", "target", "CDATA", firstId);
	    }
	    hdl.startElement(IndexBuilder.NAMESPACE, "term", "term", atts);
	    hdl.characters(value.toCharArray(), 0, value.length());
	    hdl.endElement(IndexBuilder.NAMESPACE, "term", "term");
	    
	}
	
	public String getId() {
	    if ( this.id == null ) {
	        this.id = "t" + this.hashCode();
	    }
	    return this.id;
	}
	
	public String getValue() {
	    return this.value;
	}
}