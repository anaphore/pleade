<?xml version="1.0" encoding="UTF-8"?>
<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" version="1.0" sdx:copy="for namespace">
   <xsl:import href="skin.xsl"/>
<!-- parameter language to choose a message set -->
   <o:param name="lang-default" select="'fr'"/>

   <xsl:param name="messages-URI" select="concat('messages.', substring($lang, 1, 2))"/><!-- take set of language -->
   <o:variable name="messages" select="document($messages-URI)/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)] | document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
   <xsl:param name="id" select="/sdx:document/sdx:user/@id"/>
   <xsl:template name="title">
		<o:apply-templates select="($messages/xfm:caption[@id=&#34;title1&#34;] )[1]"/>
   </xsl:template>
<!-- modify-form -->

   <xsl:template match="modify">
      <table class="form" style="text-align:center" cellpadding="4" cellspacing="1" border="1" xml:lang="fr">
         <form action="{/sdx:document/@uri}" method="post" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
            <tr>
               <td>
                  <xsl:if test="old">
<!-- old pass -->

                     <div class="legend">
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;div1&#34;] )[1]"/>
                     </div>
                     <div>
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;old-id&#34;] )[1]"/>
                           <span class="xfm:required" style="color:red">*</span>
                           <o:text>&#160;</o:text>
                        </label>
                        <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="old-id" id="old-id">
                           <o:attribute name="value">
                              <o:value-of select="normalize-space($id)"/>
                           </o:attribute>
                        </input>
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;old-pass&#34;] )[1]"/>
                           <span class="xfm:required" style="color:red">*</span>
                           <o:text>&#160;</o:text>
                        </label>
                        <input type="password" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_secret" name="old-pass"/>
                     </div>
                     <br/>
                  </xsl:if>
<!-- new pass -->

                  <div class="legend">
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;div3&#34;] )[1]"/>
                  </div>
                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;new-id&#34;] )[1]"/>
                        <span class="xfm:required" style="color:red">*</span>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" name="new-id"/>
                  </div>
                  <div>
                     <span class="xfm_confirm">
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;new-pass&#34;] )[1]"/>
                           <span class="xfm:required" style="color:red">*</span>
                           <o:text>&#160;</o:text>
                        </label>
                        <input type="password" onchange="for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i+1]; input.focus();" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_secret"/>
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;confirm&#34;] )[1]"/>
                           <span class="xfm:required" style="color:red">*</span>
                           <o:text>&#160;</o:text>
                        </label>
                        <input type="password" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_secret" name="new-pass">
                           <o:attribute name="onchange">for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i-1]; if (this.value != input.value) { alert('<o:call-template name="xfm:replace">
                                 <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;xfm:secret&#34;] )[1])"/>
                              </o:call-template>'); input.focus(); }</o:attribute>
                        </input>
                     </span>
                  </div>
<!-- name -->

                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;firstname&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="firstname" id="firstname">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(/sdx:document/sdx:user[boolean(@superuser)=true()]/@firstname)"/>
                        </o:attribute>
                     </input>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;lastname&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="lastname" id="lastname">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(/sdx:document/sdx:user[boolean(@superuser)=true()]/@lastname)"/>
                        </o:attribute>
                     </input>
                  </div>
<!-- list of langs in localized names -->

                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;lang&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <select onchange="for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i+1]; ; input.value=this.options[this.selectedIndex].value;" type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectOne">
                        <option value=" ">
                           <o:if test="/sdx:document/sdx:user[boolean(@superuser)=true()]/@xml:lang=&#34; &#34;">
                              <o:attribute name="selected">selected</o:attribute>
                           </o:if>
                        </option>
                        <o:variable name="xfm:ad0e143" select="/sdx:document/sdx:user[boolean(@superuser)=true()]/@xml:lang"/>
                        <o:for-each select="$messages/xfm:caption[@parent=&#34;lang&#34;]">
                           <o:sort/>
                           <option value="{@id}">
                              <o:if test="$xfm:ad0e143=@id">
                                 <o:attribute name="selected">selected</o:attribute>
                              </o:if>
                              <o:value-of select="."/>
                           </option>
                        </o:for-each>
                     </select>
                     <input type="text" value="" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectOne" size="5" style="" name="userlang" id="lang">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(/sdx:document/sdx:user[boolean(@superuser)=true()]/@xml:lang)"/>
                        </o:attribute>
                     </input>
                  </div>
                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;email&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="email" id="">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(/sdx:document/sdx:user[boolean(@superuser)=true()]/@email)"/>
                        </o:attribute>
                     </input>
                  </div>
                  <div style="text-align:center">
                     <xsl:choose>
                        <xsl:when test="old">
                           <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="send" title="{$messages/xfm:hint[@id=&#34;send&#34;]}" value="{$messages/xfm:caption[@id=&#34;send&#34;]}"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="modify" title="{$messages/xfm:hint[@id=&#34;modify&#34;]}" value="{$messages/xfm:caption[@id=&#34;modify&#34;]}"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </div>
               </td>
            </tr>
         </form>
      </table>
      <xsl:if test="old">
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;su-text&#34;] )[1]"/>
      </xsl:if>
   </xsl:template>
<!-- ====== SCRIPT ====================== -->

   <o:template name="xfm:script">
      <script type="text/javascript" language="javascript">

    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, '');
        return true;
    }

    function xfm_focus(o)
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus';
        return true;
    }


            function xfm_load()
            {

                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }


            function xfm_submit(form)
            {
                var message="";
                var name="";

    if (name != "new-id")
    {
        name="new-id";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;new-id&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

    if (name != "new-pass")
    {
        name="new-pass";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;new-pass&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

    if (name != "old-id")
    {
        name="old-id";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;old-id&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

    if (name != "old-pass")
    {
        name="old-pass";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;old-pass&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

                if (!message) return true ;
                alert(message);
                return false;
            }

            function xfm_reset(form)
            {

            }

                </script>
   </o:template>
<!-- ======  COMMON TEMPLATES  ======================
        short templates to format messages
        known issues : no correct id for a repeated help button
        then hide/show message can't work
        but is it useful to repeat the same help ?

-->

   <o:template match="xfm:help">
      <o:param name="id" select="@id"/>
      <input type="button" class="xfm_help" tabindex="32767" value="?">
         <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
      </input>
      <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
         <o:attribute name="id">
            <o:value-of select="$id"/>_help</o:attribute>
         <tr>
            <td style="width:90%;" class="xfm_bar">
               <o:value-of select="(caption | title | h1 )[1]"/>
            </td>
            <td style="text-align:right" class="xfm_bar">
               <input type="button" class="xfm_key" value="X">
                  <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td colspan="2" class="xfm_text">
               <o:apply-templates mode="copy"/>
            </td>
         </tr>
      </table>
   </o:template>
   <o:template match="xfm:caption" priority="-1">
      <o:apply-templates mode="copy"/>
   </o:template><!--
        Escape js string (or other search/replace)-->
   <o:template match="* | @* | text()" mode="replace" name="xfm:replace">
      <o:param name="string" select="normalize-space(.)"/>
      <o:param name="search">'</o:param>
      <o:param name="replace">\'</o:param>
      <o:choose>
         <o:when test="contains($string, $search)">
            <o:value-of select="concat(substring-before($string, $search), $replace)"/>
            <o:call-template name="xfm:replace">
               <o:with-param name="string" select="substring-after($string, $search)"/>
               <o:with-param name="search" select="$search"/>
               <o:with-param name="replace" select="$replace"/>
            </o:call-template>
         </o:when>
         <o:otherwise>
            <o:value-of select="$string"/>
         </o:otherwise>
      </o:choose>
   </o:template>
   <o:template match="*" priority="-1" mode="copy">
      <o:element name="{name()}">
         <o:apply-templates select="@*" mode="copy"/>
         <o:apply-templates mode="copy"/>
      </o:element>
   </o:template>
   <o:template match="@*" mode="copy">
      <o:copy/>
   </o:template>
<!-- ======  MESSAGES @xml:lang="fr"  ====================== -->

   <xfm:messages xml:lang="fr">
      <xfm:caption id="title1"> SDX Administration - Super-utilisateur</xfm:caption>
      <xfm:caption id="div1">Super-Utilisateur Actuel</xfm:caption>
      <xfm:caption id="div3">Nouveau Super-Utilisateur</xfm:caption>
   </xfm:messages>
<!-- ======  MESSAGES @xml:lang="en"  ====================== -->

   <xfm:messages xml:lang="en">
      <xfm:caption id="title1"> SDX Administration - Super-user </xfm:caption>
      <xfm:caption id="div1">Active super-user</xfm:caption>
      <xfm:caption id="div3">New super-user</xfm:caption>
   </xfm:messages>
   <o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>