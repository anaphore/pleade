/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.transformation.AbstractTransformer;
import org.xml.sax.SAXException;

/**
 * Un transformateur qui supprime un fichier
 */
public class DeleteFileTransformer extends AbstractTransformer {
    
    /** Le nom du parametre qui contient le nom du fichier. */
    public final static String PARAMETER_FILENAME = "file";
    
    /** Le fichier a supprimer. */
    private File f = null;
    
    /**
     * Mise en place de la transformation, on retient le fichier a supprimer.
     */
    public void setup(SourceResolver resolver, Map objectModel, String src, Parameters parameters) throws ProcessingException, SAXException, IOException {
        String filename = parameters.getParameter(PARAMETER_FILENAME, null);
        if ( filename != null ) {
            File ff = new File(filename);
            // On le retient si le fichier existe et qu'on peut le supprimer
            if ( ff.exists() && ff.canWrite() ) this.f = ff;
        }
    }

    /**
     * Au debut du document, on supprime le fichier.
     */
    public void startDocument() throws SAXException {
        if (f != null) f.delete();
        // On effectue le traitement normal
        super.startDocument();
    }
}
