<?xml version="1.0" encoding="UTF-8"?>
<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" version="1.0" sdx:copy="for namespace">
    <!-- parameter language to choose a message set -->
    <o:param name="lang-default" select="'fr'"/>
    <xsl:param name="lang" select="/sdx:document/@xml:lang"/>
    <!-- take set of language -->
    <o:variable name="messages" select="document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
    <!-- provide by request param, passed by sitemap -->
    <xsl:param name="app"/>
    <!-- (if not <map:parameter name="use-request-parameters" value="true"/>)
select="/sdx:document/sdx:parameters/sdx:parameter[@name='q']/@value" -->
    <!-- modèle racine de page HTML -->
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="*[@show='replace'][@actuate='onload']" mode="head">
        <meta http-equiv="refresh" content="{@time};URL={@href}"/>
    </xsl:template>
    <xsl:template match="sdx:document">
        <html>
            <head>
                <META HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT"/>
                <link rel="stylesheet" type="text/css" href="res/html.css"/>
                <link rel="stylesheet" type="text/css" href="res/xform.css"/>
                <link rel="stylesheet" type="text/css" href="res/sdx.css"/>
                <script type="text/javascript" src="res/selects.js">//</script>
                <xsl:call-template name="xfm:script"/>
                <xsl:apply-templates mode="head"/>
                <title>
                    <xsl:call-template name="title"/>
                </title>
            </head>
            <body>
                <xsl:attribute name="onload">
                    <xsl:apply-templates mode="onload"/>
                </xsl:attribute>
                <xsl:call-template name="page"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template name="title">SDX</xsl:template>
    <xsl:template name="page" match="page">
        <p> </p>
        <!-- page -->
        <table style="width:80%;text-align:center;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <!-- logo-cell -->
                    <table style="width:100%;background:#000;text-align:center;"  cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table style="width:100%;text-align:center;" border="0" cellspacing="1" cellpadding="20">
                                    <tr style="background:#FFF">
                                        <td style="text-align:center" >
                                            <a href="http://sdx.culture.fr/sdx/">
                                                <img src="res/sdx.gif" width="200" height="60" border="0"/>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- /logo-cell -->
                    <!-- bar-cell -->
                    <xsl:call-template name="bar"/>
                    <!-- /bar-cell -->
                    <table style="width:100%;background:#000;text-align:center"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table style="width:100%;text-align:center;" border="0" cellspacing="1" cellpadding="20">
                                    <!-- body-cell -->
                                    <tr style="background:#FFF">
                                        <td>
                                            <xsl:apply-templates select="*"/>
                                        </td>
                                    </tr>
                                    <!-- /body-cell -->
                                    <!-- bottom-cell -->
                                    <tr style="background:#FFF">
                                        <td style="text-align:center;">
                                            <small>
                                                <xsl:call-template name="sdx:user"/>
                                            </small>
                                        </td>
                                    </tr>
                                    <!-- /bottom-cell -->
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- /page -->
        <p> </p>
        <div>
            <small>SDX - version:<xsl:value-of select="/sdx:document/@version"/>, 
                build:<xsl:value-of select="/sdx:document/@build"/>.
            </small>
        </div>
    </xsl:template>
    <!-- display user -->
    <xsl:template name="sdx:user">
        <xsl:for-each select="/sdx:document/sdx:user">
            <xsl:value-of select="@firstname"/> <xsl:value-of select="@lastname"/>
          <xsl:if test="@id">(<xsl:value-of select="@id"/>), </xsl:if>
            <o:apply-templates select="($messages/xfm:caption[@id=&#34;login-as&#34;] )[1]"/>
            <xsl:choose>
                <xsl:when test="@anonymous">
                    <o:apply-templates select="($messages/xfm:caption[@id=&#34;anonymous&#34;] )[1]"/>
                </xsl:when>
                <xsl:when test="@superuser='true'">
                    <o:apply-templates select="($messages/xfm:caption[@id=&#34;su&#34;] )[1]"/>
                </xsl:when>
                <xsl:when test="@admin='true'">
                    <o:apply-templates select="($messages/xfm:caption[@id=&#34;admin-app&#34;] )[1]"/>
                    <xsl:value-of select="@app"/>
                </xsl:when>
                <xsl:when test="@app">
                    <o:apply-templates select="($messages/xfm:caption[@id=&#34;user-app&#34;] )[1]"/>
                    <xsl:value-of select="@app"/>
                </xsl:when>
            </xsl:choose>
            <xsl:text>. </xsl:text>
        </xsl:for-each>
    </xsl:template>
    <!-- links 
                td.klink {	font-weight: bold; color: #FFFFFF; background:#6287C4; cursor:pointer}
                a.klink {display:block; color:White; padding:5; text-decoration: none;}
                a.klink:hover { background-color: #FF3300; color:White}
                a.klink:visited {color:White}
            <table border="0" cellspacing="0" cellpadding="0" style="text-align:center">
                <tr style="background:#000000">
                    <td>
                        <table border="0" cellspacing="1" cellpadding="0">
                            <tr>
                                <td style="background:#FFF;white-space:nowrap" class="klink">
                                    <a class="klink"  href="login.sp?app={$app}&amp;lang={$lang}">&#160;<xfm:caption id="login"/>&#160;</a>
                                </td>
                                <td style="background:#FFF;white-space:nowrap" class="klink">
                                    <a class="klink" href="users.xsp?app={$app}&amp;lang={$lang}">&#160;<xfm:caption id="users"/>&#160;</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            -->
    <!-- BAR -->
    <xsl:template name="bar">
        <xsl:for-each select="//bar[1]">
            <table class="sdx_bar" border="0" cellspacing="0" cellpadding="0" style="width:100%;text-align:center;">
                <form action="{/sdx:document/@uri}" method="get">
                    <tr>
                        <td style="vertical-align:top">
                            <input type="hidden" name="app" value="{$app}"/>
                            <xsl:apply-templates select="button"/>
                        </td>
                        <td style="white-space:nowrap;text-align:right;" class="sdx_bar">
                            <xsl:call-template name="langs"/>
                            <o:apply-templates select="$messages/xfm:help[@id=&#34;bar&#34;][1]"/>
                        </td>
                    </tr>
                </form>
            </table>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="button">
        <input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=current()/@id]}" value="{$messages/xfm:caption[@id=current()/@id]}">
            <xsl:attribute name="style">
                <xsl:text>cursor:pointer; </xsl:text>
                <xsl:if test="boolean(@self)">border:inset 2; </xsl:if>
            </xsl:attribute>
        </input>
        <xsl:apply-templates select="parameter"/>
    </xsl:template>
    <xsl:template match="parameter">
        <input type="hidden" name="{@name}" value="{@value}"/>
    </xsl:template>
    <xsl:template name="langs">
        <!-- langs available -->
        <span style="white-space:nowrap">
            <xsl:text> </xsl:text>
            <o:apply-templates select="($messages/xfm:caption[@id=&#34;lang&#34;] )[1]"/>
            <xsl:text> </xsl:text>
            <select onchange="this.form.submit()" type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectOne" name="lang">
                <o:variable name="xfm:ad0e292" select="substring($lang, 1, 2)"/>
                <o:for-each select="document('messages.langs')/*/xfm:messages">
                    <o:sort select="@xml:lang"/>
                    <option value="{substring(@xml:lang,1,2)}">
                        <o:if test="$xfm:ad0e292=normalize-space(substring(@xml:lang,1,2))">
                            <o:attribute name="selected">selected</o:attribute>
                        </o:if>
                        <o:value-of select="@xml:lang"/>
                    </option>
                </o:for-each>
            </select>
        </span>
    </xsl:template>
    <xsl:template match="superuser-not-set">
        <p>
            <o:apply-templates select="($messages/xfm:caption[@id=&#34;superuser-not-set&#34;] )[1]"/>
        </p>
        <br/>
    </xsl:template>
    <!-- ======== STRUCTURE ========== -->
    <!--
    <xsl:template match="*|text()" mode="onload"/>
    <xsl:template match="sdx:*"/>
    <xsl:template match="*" priority="-1">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="comment()|text()| node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="text()|processing-instruction()|comment()" priority="-1">
        <xsl:copy>
            <xsl:apply-templates select="*|text()|processing-instruction()|comment()"/>
        </xsl:copy>
    </xsl:template>
-->
    <!-- Les autres éléments SDX ne sont pas sortis -->
    <xsl:template match="sdx:*"/>
    <xsl:template match="*"/>
    <xsl:template match="node()" mode="onload"/>
    <xsl:template match="node()" mode="head"/>
    <!-- ====== SCRIPT ====================== -->
    <o:template name="xfm:script">
        <script type="text/javascript" language="javascript">
    
    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, ''); 
        return true;
    }

    function xfm_focus(o) 
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus'; 
        return true;
    }
    

            function xfm_load()
            {
                
                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }

            
            function xfm_submit(form)
            {
                var message="";
                var name="";
                
                if (!message) return true ;
                alert(message);
                return false;
            }
            
            function xfm_reset(form)
            {
            
            }

                </script>
    </o:template>
    <!-- ======  COMMON TEMPLATES  ====================== 
        short templates to format messages
        known issues : no correct id for a repeated help button 
        then hide/show message can't work
        but is it useful to repeat the same help ?
        
-->
    <o:template match="xfm:help">
        <o:param name="id" select="@id"/>
        <input type="button" class="xfm_help" tabindex="32767" value="?">
            <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
        </input>
        <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
            <o:attribute name="id">
                <o:value-of select="$id"/>_help</o:attribute>
            <tr>
                <td style="width:90%" class="xfm_bar">
                    <o:value-of select="(caption | title | h1 )[1]"/>
                </td>
                <td style="text-align:right" class="xfm_bar">
                    <input type="button" class="xfm_key" value="X">
                        <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
                    </input>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="xfm_text">
                    <o:apply-templates mode="copy"/>
                </td>
            </tr>
        </table>
    </o:template>
    <o:template match="xfm:caption" priority="-1">
        <o:apply-templates mode="copy"/>
    </o:template>
    <!--
        Escape js string (or other search/replace)-->
    <o:template match="* | @* | text()" mode="replace" name="xfm:replace">
        <o:param name="string" select="normalize-space(.)"/>
        <o:param name="search">'</o:param>
        <o:param name="replace">\'</o:param>
        <o:choose>
            <o:when test="contains($string, $search)">
                <o:value-of select="concat(substring-before($string, $search), $replace)"/>
                <o:call-template name="xfm:replace">
                    <o:with-param name="string" select="substring-after($string, $search)"/>
                    <o:with-param name="search" select="$search"/>
                    <o:with-param name="replace" select="$replace"/>
                </o:call-template>
            </o:when>
            <o:otherwise>
                <o:value-of select="$string"/>
            </o:otherwise>
        </o:choose>
    </o:template>
    <o:template match="*" priority="-1" mode="copy">
        <o:element name="{name()}">
            <o:apply-templates select="@*" mode="copy"/>
            <o:apply-templates mode="copy"/>
        </o:element>
    </o:template>
    <o:template match="@*" mode="copy">
        <o:copy/>
    </o:template>
    <o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>
