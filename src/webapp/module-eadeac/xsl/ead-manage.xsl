<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Gestion du contenu. Gérer les documents publiés
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml dir">

	<!-- Le code pour traiter la navigation d'une page à l'autre -->
	<xsl:import href="navigation.xsl"/>

	<xsl:param name="data-output-dir" select="''" />
	<xsl:param name="ead-docs-dir" select="''" />
	<xsl:param name="title-limit" select="'150'" />
	<xsl:param name="base" select="'ead'" />
  <xsl:param name="filter_field" select="''"/>
  <xsl:param name="filter_value" select="''"/>

	<!-- Définition de paramètres qui vont permettre d'ajuster les clés i18n et le catalogue -->
	<xsl:param name="i18n-cat" select="'module-eadeac-edit'"/>
  <xsl:param name="i18n-root" select="'edit.ead-manage'" />

  <!-- Imports -->
	<xsl:include href="form2xhtml-commons.xsl"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.html.title">gérer les documents publiés</i18n:text>
				</title>
				<xsl:comment>pleade</xsl:comment>
				<script type="text/javascript" src="i18n/module-eadeac-edit-js.js">//</script>
				<script type="text/javascript" src="js/pleade/common/manager/SuggestManager.js">//</script>	
				<script type="text/javascript" src="js/pleade/admin/eadeac/AdminSuggest.js">//</script>
				<script type="text/javascript" src="js/pleade/admin/eadeac/EditFormValidator.js">//</script>
				<script type="text/javascript" src="js/pleade/admin/eadeac/EadDocumentManager.js">//</script>
	
				<script type="text/javascript">
					var documentManager = '';
					window.addEvent("load", initEadManager);
					function initEadManager(){
						documentManager = new EadDocumentManager('<xsl:value-of select="$base" />');
					}
				</script>
			</head>
			<body>

        <div class="pl-title">
          <xsl:call-template name="build-help">
						<xsl:with-param name="c" select="$i18n-cat"/>
            <xsl:with-param name="v" select="'title'" />
            <xsl:with-param name="k" select="concat($i18n-root, '.title')" />
          </xsl:call-template>
          <h1>
            <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.pl-title">gérer les documents publiés</i18n:text>
          </h1>
        </div>

				<div class="pl-form-intro pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.result.intro" />
				</div>

				<xsl:apply-templates />

				<div class="pl-form-conclu pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.result.conclusion" />
				</div>

			</body>

		</html>

	</xsl:template>

	<xsl:template match="sdx:results">

		<xsl:variable name="nbdocs" select="number(@nb)" />

    <div class="pl-form-container">

    <div class="pl-form-ead-admin">

    <xsl:call-template name="build-help">
			<xsl:with-param name="c" select="$i18n-cat"/>
      <xsl:with-param name="v" select="'docs'" />
      <xsl:with-param name="k" select="concat($i18n-root, '.docs')" />
    </xsl:call-template>

    <form action="?">
      <fieldset>
        <legend><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.filter.title">filtrer</i18n:text></legend>
        <label for="filter_field"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.filter.field">champ</i18n:text></label>
        <select name="filter_field" id="filter_field">
          <option value=""><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.filter.field.choose">choisissez</i18n:text></option>
          <option value="root-id"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.filter.field.id">identifiant</i18n:text></option>
          <option value="unittitle"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.filter.field.title">titre</i18n:text></option>
        </select>
        <label for="filter_value"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.filter.value"> valeur </i18n:text></label>
        <input type="text" name="filter_value" id="filter_value" value="{$filter_value}"/>
        <input type="submit"/>
      </fieldset>
    </form>

		<fieldset>

			<legend>
				<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs.list.legend">liste des documents publiés</i18n:text>
			</legend>

			<xsl:call-template name="pagination"/>

			<table class="pl-tbl-ead-admin">
				<thead>
					<tr>
						<th>
							<a class="pl-tbl-order-asc" href="{concat('?hpp=', /sdx:document/sdx:results/@hpp)}&amp;sf=sdxdocid&amp;so=asc" title="{$i18n-cat}:{$i18n-root}.order.id.asc.title" i18n:attr="title"><span class="access"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.order.asc">ordre alphabétique</i18n:text></span></a>
							<xsl:value-of select="' '"/>
							<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.arrayhead.id">identifiant</i18n:text>
							<xsl:value-of select="' '"/>
							<a class="pl-tbl-order-desc" href="{concat('?hpp=', /sdx:document/sdx:results/@hpp)}&amp;sf=sdxdocid&amp;so=desc" title="{$i18n-cat}:{$i18n-root}.order.id.desc.title" i18n:attr="title"><span class="access"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.order.desc">ordre alphabétique inversé</i18n:text></span></a>
						</th>
						<th>
							<a class="pl-tbl-order-asc" href="{concat('?hpp=', /sdx:document/sdx:results/@hpp)}&amp;sf=fucomptitle&amp;so=asc" title="{$i18n-cat}:{$i18n-root}.order.title.asc.title" i18n:attr="title"><span class="access"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.order.asc">ordre alphabétique</i18n:text></span></a>
							<xsl:value-of select="' '"/>
							<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.arrayhead.title">titre</i18n:text>
							<xsl:value-of select="' '"/>
							<a class="pl-tbl-order-desc" href="{concat('?hpp=', /sdx:document/sdx:results/@hpp)}&amp;sf=fucomptitle&amp;so=desc" title="{$i18n-cat}:{$i18n-root}.order.title.desc.title" i18n:attr="title"><span class="access"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.order.desc">ordre alphabétique inversé</i18n:text></span></a>
						</th>
						<th>
							<a class="pl-tbl-order-asc" href="{concat('?hpp=', /sdx:document/sdx:results/@hpp)}&amp;sf=sdxmoddate&amp;so=asc" title="{$i18n-cat}:{$i18n-root}.order.date.asc.title" i18n:attr="title"><span class="access"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.order.asc">ordre alphabétique</i18n:text></span></a>
							<xsl:value-of select="' '"/>
							<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.arrayhead.lastpublication">dernière publication</i18n:text>
							<xsl:value-of select="' '"/>
							<a class="pl-tbl-order-desc" href="{concat('?hpp=', /sdx:document/sdx:results/@hpp)}&amp;sf=sdxmoddate&amp;so=desc" title="{$i18n-cat}:{$i18n-root}.order.date.desc.title" i18n:attr="title"><span class="access"><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.order.desc">ordre alphabétique inversé</i18n:text></span></a>
						</th>
						<th><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.arrayhead.actions">actions</i18n:text></th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="sdx:result[sdx:field[@name='sdxdocid']/@escapedValue!='']">
	
						<xsl:variable name="v" select="normalize-space(sdx:field[@name='sdxdocid']/@escapedValue)" />
						<xsl:variable name="t1" select="normalize-space(sdx:field[@name='fatitle'][1]/text())" />
						<xsl:variable name="t">
							<xsl:choose>
								<xsl:when test="number($title-limit) and (string-length($t1) &gt; number($title-limit))">
									<xsl:value-of select="concat( substring($t1,1,number($title-limit)), ' [...]' )" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$t1" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<xsl:variable name="trclass">
							<xsl:choose>
								<xsl:when test="position() mod 2 = 0">
									<xsl:text>pl-tbl-tr-even</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>pl-tbl-tr-odd</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<tr class="{$trclass}">
							<td>
								<!-- Affichage de l'identifiant du document -->
								<input type="hidden" name="did" id="did{position()}" value="{$v}" />
								<xsl:value-of select="$v"/>
							</td>
							<td>
								<!-- Affichage du titre (tronqué au besoin) -->
                <a href="../ead.html?id={$v}">
								  <xsl:value-of select="$t" />
                </a>
							</td>
							<td>
								<!-- Pour yyyy-MM-ddTHH:mm:ssZ on renvoie yyyy-MM-dd -->
								<xsl:value-of select="concat(substring-before(sdx:field[@name='sdxmoddate'],'T'), ' ', substring-after(substring-before(sdx:field[@name='sdxmoddate'], 'Z'),'T'))" />
							</td>
							<td class="pl-tbl-ead-admin-actions">
								<xsl:call-template name="display-delete-button">
									<xsl:with-param name="nbdocs" select="$nbdocs"/>
									<xsl:with-param name="position" select="position()"/>
								</xsl:call-template>
								<xsl:variable name="publication-params-url" select="concat('cocoon://functions/ead/get-properties/', sdx:field[@name='root-id'], '/publication.xml')"/>
								<xsl:variable name="active-republish">
									<xsl:choose>
										<xsl:when test="doc-available($publication-params-url)">
											<xsl:variable name="short-ead-docs-dir">
												<xsl:choose>
													<xsl:when test="contains($ead-docs-dir, '//')">
														<xsl:value-of select="substring-after($ead-docs-dir, '//')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ead-docs-dir"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:choose>
												<xsl:when test="contains(document($publication-params-url)/properties/property[@name='docUrl'], $short-ead-docs-dir)">
													<xsl:text>true</xsl:text>
												</xsl:when>
												<xsl:otherwise>false</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>true</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:call-template name="display-publish-again-button">
									<xsl:with-param name="nbdocs" select="$nbdocs"/>
									<xsl:with-param name="position" select="position()"/>
									<xsl:with-param name="active" select="$active-republish"/>
								</xsl:call-template>
								<xsl:call-template name="display-params-button">
									<xsl:with-param name="nbdocs" select="$nbdocs"/>
									<xsl:with-param name="position" select="position()"/>
								</xsl:call-template>
							</td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>

		</fieldset>

		<xsl:if test="@nb &gt; 0">
			<xsl:call-template name="jsButtons"/>
		</xsl:if>

    </div>

    </div>

	</xsl:template>

	<!-- initalisation des actions javascript sur les boutons -->
	<xsl:template name="jsButtons">
		<script type="text/javascript">
			window.addEvent("load", function(){
				//eadManageButtons('<xsl:value-of select="$base" />');
			});
		</script>
	</xsl:template>

	<!-- Affichage de la pagination -->
	<xsl:template name="pagination">
		<div class="navigation-results">
			<p>
        <!-- La clé i18n selon singulier / pluriel -->
        <xsl:variable name="key-suffix">
            <xsl:choose>
                <xsl:when test="/sdx:document/sdx:results/@nb = 1">one</xsl:when>
                <xsl:otherwise>many</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
				<span class="nbresults">
                  <xsl:value-of select="/sdx:document/sdx:results/@nb"/>
                  <xsl:text> </xsl:text>
                  <i18n:text key="docsearch.results.{$key-suffix}"/>
				</span>
				<span class="nbpages"><xsl:text>Page </xsl:text><strong><xsl:value-of select="/sdx:document/sdx:results/@page"/></strong><xsl:text> de </xsl:text><xsl:value-of select="/sdx:document/sdx:results/@pages"/></span>
				<xsl:if test="/sdx:document/sdx:results/@nbPages &gt; 1">
					<span id="{/sdx:document/sdx:results/sdx:result[1]/sdx:field[@name='root-id']}_pagination" class="navpage">
						<xsl:apply-templates select="." mode="hpp">
							<xsl:with-param name="url" select="''" />
							<xsl:with-param name="query">
								<xsl:call-template name="buildNavQuery"/>
							</xsl:with-param>
						</xsl:apply-templates>
					</span>
				</xsl:if>
			</p>
		</div>
	</xsl:template>

	<xsl:template name="buildNavQuery">
		<xsl:value-of select="concat('&amp;hpp=', /sdx:document/sdx:results/@hpp)"/>
		<xsl:for-each select="/sdx:document/sdx:results/sdx:sort/sdx:field">
			<xsl:value-of select="concat('&amp;sf=', @name, '&amp;so=', @order)"/>
		</xsl:for-each>
	</xsl:template>

	<!--+
		|	Templates susceptibles d'être surchargés
		+-->
	<!-- Bouton pour supprimer un document publié -->
	<xsl:template name="display-delete-button">
		<xsl:param name="nbdocs"/>
		<xsl:param name="position"/>
		<button class="yui-button yui-push-button pl-form-button"
							id="pl-form-button1{$position}" type="button"
							title="{$i18n-cat}:{$i18n-root}.delete-doc.title" i18n:attr="title">
		<xsl:if test="$nbdocs &lt;= 0">
			<xsl:attribute name="disabled">
				<xsl:text>disabled</xsl:text>
			</xsl:attribute>
		</xsl:if>
			<span class="noaccess">
				<i18n:text key="{$i18n-root}.delete-doc.text" catalogue="{$i18n-cat}">
					<xsl:text>supprimer</xsl:text>
				</i18n:text>
			</span>
		</button>
	</xsl:template>
	<!-- Bouton pour republier -->
	<xsl:template name="display-publish-again-button">
		<xsl:param name="nbdocs"/>
		<xsl:param name="position"/>
		<xsl:param name="active"/>
		<button class="yui-button yui-push-button pl-form-button"
							id="pl-form-button2{$position}" type="button"
							title="{$i18n-cat}:{$i18n-root}.publish-again.title" i18n:attr="title">
			<xsl:if test="$nbdocs &lt;= 0 or $active = 'false'">
				<xsl:attribute name="disabled">
					<xsl:text>disabled</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<span class="noaccess">
				<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publish-again.text">
					<xsl:text>publier de nouveau</xsl:text>
				</i18n:text>
			</span>
		</button>
	</xsl:template>
	<!-- Bouton pour modifier des paramètres d'affichage -->
	<xsl:template name="display-params-button">
		<xsl:param name="nbdocs"/>
		<xsl:param name="position"/>
		<button class="yui-button yui-push-button pl-form-button" id="pl-form-button3{$position}"
							type="button"
							title="{$i18n-cat}:{$i18n-root}.display-params.title" i18n:attr="title">
			<xsl:if test="$nbdocs &lt;= 0">
				<xsl:attribute name="disabled">
					<xsl:text>disabled</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<span class="noaccess">
				<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.display-params.text">
					<xsl:text>modifier les paramètres d'affichage</xsl:text>
				</i18n:text>
			</span>
		</button>
	</xsl:template>

</xsl:stylesheet>
