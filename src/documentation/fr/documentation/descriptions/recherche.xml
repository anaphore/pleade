<?xml version="1.0"?>
<?xml-stylesheet href="../../_config/docbook.css" type="text/css"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "../../../_config/docbookx.dtd">
<article lang="fr"> 
  <title>Recherche documentaire</title> 
  <titleabbrev>Recherche</titleabbrev> 
  <articleinfo> 
	 <author><firstname>Martin</firstname><surname>Sévigny</surname>
		
		<affiliation> 
		  <orgname>AJLSM</orgname> 
		</affiliation> 
		<email>sevigny@ajlsm.com</email> 
	 </author> 
	 <author><firstname>Florence</firstname><surname>Clavaud</surname>
		
		<affiliation> 
		  <orgname>Anaphore</orgname> 
		</affiliation> 
		<email>florence.clavaud@arkheia.net</email>
		
	 </author>
	 <abstract> 
		<para>PLEADE offre de puissantes
		  fonctionnalités de recherche documentaire,
		  permettant ainsi de retrouver aisément des
		  documents EAD et leurs différentes unités de
		  description.</para> 
	 </abstract><date
	 role="modification">2006-06-19</date> 
  </articleinfo> 
  <para>Dans la section sur les 
	 <ulink url="../concepts/index.xml">concepts
		et objectifs</ulink>, nous avons décrit 
	 <ulink
	  url="../concepts/recherche.xml">l'importance de
		la recherche documentaire dans PELADE, ainsi que
		le modèle de recherche sous-jacent</ulink>. Dans
	 cette section, nous allons décrire comment se
	 présente les différentes fonctionnalités de
	 recherche offertes par PLEADE.</para> 
  <section> 
	 <title>La recherche simple</title> 
	 <para>Pour effectuer des recherches, on doit
		nécessairement passer par un formulaire de
		recherche. Sur toutes les pages d'un site Web
		PLEADE, en haut de page, il existe un formulaire
		de recherche <emphasis
		role="italic">simple</emphasis> qui permet de
		faire une recherche de type
		<emphasis role="italic">plein texte</emphasis>
		dans l'ensemble des documents EAD publiés sur le
		site. Ce type de recherche peut également être
		disponible pour un document EAD, dans le cadre du
		haut de la fenêtre de consultation. Par ailleurs,
		dans les formulaires de recherche avancée, les
		critères de recherche textuels constituent aussi
		des opportunités de recherche simple, la plupart
		du temps dans un champ spécifique.</para> 
	 <para> Même si ce type de recherche peut
		s'avérer limité en terme de précision, il
		correspond à un mode d'accès que l'on retrouve
		sur un grand nombre de sites Web, ce qui
		contribue à rendre l'interface de PLEADE facile à
		utiliser pour le public en général.</para> 
	 <para>Si on tape un mot ou une expression
		dans ce pavé, le moteur de recherche intégré à
		l'application (le logiciel libre
		<emphasis role="italic"><ulink
		url="http://lucene.apache.org/">Lucene</ulink></emphasis>
		du projet Apache) va effectuer une recherche dans
		le texte de l'intégralité du ou des instruments
		de recherche. Les résultats de ce genre de
		recherche risquent donc d'être décevants par
		rapport à ceux que l'on peut obtenir au moyen
		d'un formulaire de recherche avancée, surtout si
		l'on tape un terme d'usage courant : on ramènera
		souvent un nombre important de résultats. </para>
	 
	 <para><emphasis role="bold">Il est possible
		d'utiliser une syntaxe particulière pour préciser
		la requête ou son contexte</emphasis>. Nous
		donnons ici les principales fonctionnalités et
		règles syntaxiques associées utilisables avec
		Lucene, la 
		<ulink
		url="http://lucene.apache.org/java/docs/queryparsersyntax.html">documentation
		  de Lucene</ulink> contient plus de détails : 
		<itemizedlist> 
		  <listitem> 
			 <para>un mot, tel que
				<emphasis role="italic">logiciel</emphasis>, peut
				être tapé directement dans le pavé ; une
				expression, telle que <emphasis
				role="italic">"logiciel libre"</emphasis>, sera
				tapée entre guillemets, comme dans la présente
				phrase ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>pour remplacer un caractère au
				milieu ou à la fin d'un mot, on peut utiliser le
				point d'interrogation : ? (par ex., la question :
				<emphasis role="italic">p?ste</emphasis> ramènera
				toutes les unités documentaires contenant les
				mots piste, poste, peste) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>pour remplacer un nombre indéfini
				de caractères au milieu ou à la fin d'un mot, on
				utilise l'astérisque : * (par ex., la question :
				<emphasis role="italic">économi*</emphasis>
				ramènera toutes les unités documentaires
				contenant l'un ou l'autre des mots économie,
				économique, économiques) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>on peut taper plusieurs mots
				et/ou expressions dans ce pavé ; ils pourront
				être reliés entre eux par les opérateurs booléens
				AND (pour et, qui est l'opérateur de recherche
				par défaut dans la version 2 de PLEADE), OR (pour
				ou), NOT (pour sauf) (taper par ex.
				<emphasis role="italic">économie OR
				économique</emphasis>, puis, pour comparer,
				<emphasis role="italic">économie NOT
				économique</emphasis> ; ou bien
				<emphasis role="italic">développement NOT
				durable</emphasis>) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>on peut utiliser le signe +
				accolé à la première lettre d'un mot ou avant les
				guillemets ouvrants d'une expression, pour
				indiquer que le concept ainsi marqué doit
				obligatoirement être présent dans l'unité
				documentaire ; inversement le signe - indiquera
				que le concept marqué ne doit pas figurer dans
				l'unité documentaire (dans ce dernier cas ce
				concept ne doit pas être seul) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>il est possible de faire suivre
				un mot du signe ~ pour indiquer qu'on cherche ce
				mot tel qu'écrit et tout mot écrit de manière
				approchante (recherche dite floue) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>il est possible de faire suivre
				une expression composée de deux mots (tapée entre
				guillemets) du signe ~ suivi d'un nombre n, afin
				de lancer une requête sur les deux mots tels
				qu'ils ne soient pas à plus de n mots de distance
				(recherche de proximité) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>enfin, on peut utiliser les
				parenthèses pour construire une requête à partir
				de plus de deux critères ( par ex. :
				<emphasis role="italic">(pêche OR poisson) AND
				mer</emphasis>).</para> 
		  </listitem> 
		</itemizedlist></para> 
	 <para>Le tableau suivant donne un exemple de
		requêtes de recherches simples qui peuvent être
		exécutées sur le document EAD
		<emphasis role="italic">Etat sommaire des
		Archives Napoléon</emphasis>, tel que distribué
		avec PLEADE en tant qu'exemple. Pour obtenir les
		mêmes résultats que ceux présentés ci-dessous, il
		faut utiliser la version 2 de PLEADE (dans
		laquelle l'opérateur de recherche par défaut est
		ET) et publier le document avec une fragmentation
		selon le niveau archivistique et ayant comme
		paramètre 
		<parameter>|subfonds|recordgrp|sous-sous-fonds|</parameter>.</para>
	 
	 <table> 
	 <title>Exemples de requêtes simples</title> 
	 <tgroup cols="3"><colspec colnum="1"
		colname="col1" colwidth="*"/><colspec colnum="2"
		colname="col2" colwidth="*"/><colspec colnum="3"
		colname="col3" colwidth="*"/> 
		<thead> 
		  <row><entry
			 colname="col1">Requête</entry><entry
			 colname="col2">Nb de résultats</entry><entry
			 colname="col3">Commentaires</entry> 
		  </row> 
		</thead> 
		<tbody> 
		  <row><entry
			 colname="col1"><parameter>joséphine</parameter></entry><entry
			 colname="col2">5</entry><entry colname="col3">Une
				recherche sur un seul mot.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>napoléon</parameter></entry><entry
			 colname="col2">15</entry><entry
			 colname="col3">Encore une recherche sur un seul
				mot.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>napoléon
				  joséphine</parameter></entry><entry
			 colname="col2">5</entry><entry colname="col3">Une
				recherche sur deux mots, séparés par un opérateur
				booléen ET.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>joséphine
				  napoléon</parameter></entry><entry
			 colname="col2">5</entry><entry colname="col3">La
				même requête, avec les mots inversés (aucune
				différence).</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>napoléon AND
				  joséphine</parameter></entry><entry
			 colname="col2">5</entry><entry colname="col3">La
				même requête, avec une syntaxe
				alternative.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>napoléon
				  +joséphine</parameter></entry><entry
			 colname="col2">5</entry><entry
			 colname="col3">Cette fois, le mot "joséphine"
				doit être présent, et de même pour le mot
				"napoléon". Même requête.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>+napoléon
				  +joséphine</parameter></entry><entry
			 colname="col2">5</entry><entry
			 colname="col3">Même requête.</entry> 
		  </row> 
		  <!-- <row><entry
			 colname="col1"><parameter>napoléon AND
				  joséphine</parameter></entry><entry
			 colname="col2">5</entry><entry
			 colname="col3">Même requête, avec une syntaxe
				alternative.</entry> 
		  </row>  -->
		  <row><entry
			 colname="col1"><parameter>napoléon
				  -joséphine</parameter></entry><entry
			 colname="col2">10</entry><entry colname="col3">Le
				mot 'joséphine" ne doit pas être présent.</entry>
			 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>napoléon NOT
				  joséphine</parameter></entry><entry
			 colname="col2">10</entry><entry
			 colname="col3">Même requête, avec une syntaxe
				alternative.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>napoléon OR
				  bonaparte</parameter></entry><entry
			 colname="col2">16</entry><entry
			 colname="col3">Requête sur deux mots reliés par
				un OU booléen.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>"napoléon
				  bonaparte"</parameter></entry><entry
			 colname="col2">3</entry><entry
			 colname="col3">Requête sur deux mots qui doivent
				se suivre (en ignorant les mots vides).</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>prince</parameter></entry><entry
			 colname="col2">16</entry><entry
			 colname="col3">Requête sur un mot.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>princes</parameter></entry><entry
			 colname="col2">9</entry><entry
			 colname="col3">Requête sur un mot.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>prince?</parameter></entry><entry
			 colname="col2">16</entry><entry
			 colname="col3">Requête sur un mot tronqué (un
				seul caractère).</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>prince*</parameter></entry><entry
			 colname="col2">17</entry><entry
			 colname="col3">Requête sur un mot tronqué
				(plusieurs caractères).</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>wagner</parameter></entry><entry
			 colname="col2">1</entry><entry
			 colname="col3">Requête sur un mot.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>wagner~</parameter></entry><entry
			 colname="col2">3</entry><entry
			 colname="col3">Requête sur un mot, mais avec une
				distance orthographique. Les mots
				<emphasis role="italic">werner</emphasis>,
				<emphasis role="italic">magne</emphasis>,
				<emphasis role="italic">waldner</emphasis>,
				<emphasis role="italic">wagner</emphasis> sont
				effectivement recherchés.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>"campagne
				  d'egypte"</parameter></entry><entry
			 colname="col2">2</entry><entry
			 colname="col3">Requête sur deux mots qui doivent
				se suivre.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>"campagne
				  d'egypte"~10</parameter></entry><entry
			 colname="col2">3</entry><entry
			 colname="col3">Requête sur deux mots qui doivent
				être à un maximum de 10 mots l'un de
				l'autre.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>[archive TO
				  autographe]</parameter></entry><entry
			 colname="col2">20</entry><entry
			 colname="col3">Requête sur un intervalle de mots
				(en ordre alphabétique).</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>unittitle:napoléon</parameter></entry><entry
			 colname="col2">10</entry><entry
			 colname="col3">Requête sur un mot dans un champ
				spécifique.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>scopecontent:joséphine</parameter></entry><entry
			 colname="col2">4</entry><entry
			 colname="col3">Requête sur un mot dans un (autre)
				champ spécifique.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>unittitle:napoléon
				  scopecontent:joséphine</parameter></entry><entry
			 colname="col2">4</entry><entry
			 colname="col3">Combinaison de deux critères de
				recherche dans des champs spécifiques, reliés par
				un ET booléen.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>unittitle:napoléon OR
				  scopecontent:joséphine</parameter></entry><entry
			 colname="col2">10</entry><entry
			 colname="col3">Combinaison de deux critères de
				recherche dans des champs spécifiques, reliés par
				un OU booléen.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>+joséphine +(napoléon
				  OR louis)</parameter></entry><entry
			 colname="col2">5</entry><entry
			 colname="col3">Utilisation des parenthèses pour
				grouper des critères de recherche.</entry> 
		  </row> 
		  <row><entry
			 colname="col1"><parameter>archives
				  bonaparte^10</parameter></entry><entry
			 colname="col2">12</entry><entry
			 colname="col3">Modification de la pondération des
				termes d'une requête. Cette requête donne les
				mêmes résultats de recherche que la requête
				<emphasis role="italic">archives
				bonaparte</emphasis>, mais pas dans le même
				ordre. Dans ce cas précis, le cinquième résultat
				passe au quatrième rang, etc.</entry> 
		  </row> 
		</tbody> 
	 </tgroup> 
	 </table> 
  </section> 
  <section> 
	 <title>La recherche avancée</title> 
	 <para>Au-delà de cette fonctionnalité de
		recherche simple présente sur toutes les pages,
		PLEADE permet à l'administrateur d'un site de
		définir des formulaires de recherche avancée. Ces
		formulaires sont décrits dans un fichier de
		configuration, et l'affichage de même que
		l'interprétation des requêtes de recherche est
		complètement automatisé par PLEADE. La définition
		d'un formulaire de recherche comprend ces
		différents aspects (toutes les informations
		textuelles peuvent être définies dans toutes les
		langues souhaités) :</para> 
	 <orderedlist> 
		<listitem> 
		  <para>Un titre, utilisé au-dessus du
			 formulaire.</para> 
		</listitem> 
		<listitem> 
		  <para>Un titre court, utilisé dans les
			 listes de formulaires disponibles.</para> 
		</listitem> 
		<listitem> 
		  <para>Une introduction, permettant
			 d'expliquer le fonctionnement du
			 formulaire.</para> 
		</listitem> 
		<listitem> 
		  <para>Des critères de recherche, chaque
			 critère étant associé à un ou plusieurs champs de
			 recherche. Les critères peuvent être affichés de
			 différentes manières : zone de texte, liste
			 déroulante avec valeurs provenant du contenu de
			 la base, cases à cocher ou boutons de radio,
			 champ caché, champ date ou dates
			 multiples.</para> 
		</listitem> 
	 </orderedlist> 
	 <para>Une fois un formulaire affiché, un
		utilisateur peut fournir des valeurs aux
		différents critères et soumettre sa requête de
		recherche. Les résultats de recherche seront par
		la suite présentés, avec une segmentation par
		pages pour éviter de trop longues listes de
		résultats et des pages trop lourdes. La
		navigation d'une page à l'autre s'effectue
		aisément.</para> 
	 <para>Les formulaires de recherche peuvent
		être utilisés dans différents contextes au sein
		d'une application PLEADE :</para> 
	 <variablelist> 
		<varlistentry><term>Formulaire
			 général</term> 
		  <listitem> 
			 <para>Un formulaire général est
				accessible depuis le menu déroulant des
				formulaires de recherche, menu accessible sur
				toutes les pages PLEADE. Lorsqu'un tel formulaire
				est utilisé, la recherche s'effectue dans tous
				les documents EAD publiés sur le site.</para> 
		  </listitem> 
		</varlistentry> 
		<varlistentry><term>Formulaire associé à
			 une rubrique</term> 
		  <listitem> 
			 <para>Un formulaire de recherche peut
				être associé à une rubrique dans le 
				<ulink url="organisation.xml">système
				  d'organisation des documents EAD</ulink>.
				Lorsqu'un utilisateur exprime une requête à
				l'aide d'un tel formulaire, la recherche
				s'effectue uniquement dans les documents EAD qui
				sont associés à cette rubrique, et ce sans que
				cette contrainte ne soit exprimée explicitement
				dans le formulaire.</para> 
		  </listitem> 
		</varlistentry> 
		<varlistentry><term>Formulaire associé à un
			 document EAD</term> 
		  <listitem> 
			 <para>Dans la 
				<ulink url="consultation.xml">fenêtre
				  de consultation</ulink> d'un document EAD, il est
				possible de faire afficher un formulaire de
				recherche. Dans un tel contexte, la recherche
				effectuée à l'aide de ce formulaire se fera
				uniquement à l'intérieur du document EAD en cours
				d'affichage.</para> 
		  </listitem> 
		</varlistentry> 
	 </variablelist> 
	 <para>Ces différents contextes d'utilisation
		des formulaires de recherche dans PLEADE
		constitue l'une des principales forces de cet
		outil, car il permet d'offrir aux utilisateurs
		des moyens d'accès variés et souples, mais
		surtout parfaitement adaptés à des groupes de
		documents, voire à un document unique. Cela
		permet d'exploiter toute la richesse structurelle
		et de contenus, de même que toute la variété que
		l'on peut retrouver dans un document EAD.</para> 
	 <para>Pour savoir comment créer des
		formulaires de recherche, il faut consulter la
		partie de la documentation qui traite de la 
		<ulink
		url="../adaptation/index.xml">configuration et de
		  l'adaptation de PLEADE</ulink>, et plus
		particulièrement la partie sur les 
		<ulink
		 url="../adaptation/avancee.xml">formulaires de
		  recherche avancée</ulink>.</para> 
  </section> 
  <section> 
	 <title>Les résultats de recherche</title> 
	 <para>Un résultat de recherche pour PLEADE
		est toujours une unité documentaire, c'est-à-dire
		une unité de description archivistique qui a été
		identifiée comme unité documentaire par l'éditeur
		de contenus au moment de la publication. Cette
		unité de description est présentée de manière
		sommaire, c'est-à-dire avec l'identification du
		contexte (unités de description parent) ainsi que
		le titre forgé de l'unité, qui peut contenir la
		cote, les dates extrêmes, l'intitulé, etc.</para>
	 
	 <para>Les résultats de recherche sont classés
		par ordre de pertinence par rapport aux critères
		de recherche exprimés. Cette approche est encore
		une fois tout à fait conforme aux habitudes de
		recherche que l'on retrouve sur le Web, et
		s'avère très efficace pour des requêtes sur des
		mots libres.</para> 
	 <para>A partir de ces résultats de recherche,
		l'utilisateur peut consulter une unité de
		description, et l'ensemble du document EAD qui la
		contient, tout simplement en cliquant sur son
		intitulé.</para> 
  </section> 
</article> 
