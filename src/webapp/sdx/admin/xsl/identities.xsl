<?xml version="1.0" encoding="UTF-8"?>
<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" version="1.0" sdx:copy="for namespace">
   <xsl:import href="skin.xsl"/>
<!-- parameter language to choose a message set -->
   <o:param name="lang-default" select="'fr'"/>

   <xsl:param name="messages-URI" select="concat('messages.', substring($lang, 1, 2))"/><!-- take set of language -->
   <o:variable name="messages" select="document($messages-URI)/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)] | document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
   <xsl:param name="id"/>
   <xsl:template name="title">
      <o:apply-templates select="($messages/xfm:caption[@id=&#34;title&#34;] )[1]"/>
   </xsl:template>
<!--
    global edit template
    -->

   <xsl:template match="edit">
      <h2>
         <o:apply-templates select="$messages/xfm:help[@id=&#34;identities&#34;][1]"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="$app"/>
      </h2>
      <table style="width:100%;border:none" cellpadding="5" cellspacing="0">
         <tr>
            <td style="width:33%;white-space:nowrap;vertical-align:top">
               <xsl:apply-templates select="sdx:results"/>
            </td>
            <td>
               <xsl:apply-templates select="sdx:user | sdx:group"/>
            </td>
         </tr>
      </table>
      <o:apply-templates select="($messages/xfm:caption[@id=&#34;identities-text&#34;] )[1]"/>
   </xsl:template>
<!--
     form for a user
     -->

   <xsl:template match="edit/sdx:user">
      <table class="form" style="text-align:center;border:1px solid"  cellpadding="4" cellspacing="1" xml:lang="fr">
         <form action="{/sdx:document/@uri}" method="post" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
            <input type="hidden" name="app" value="{$app}"/>
            <tr>
               <td>
                  <div class="legend">
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;user&#34;] )[1]"/>
                  </div>
                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;id&#34;] )[1]"/>
                        <span class="xfm:required" style="color:red">*</span>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="id" id="">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(@id)"/>
                        </o:attribute>
                     </input>
                  </div>
<!-- name -->

                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;firstname&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="firstname" id="">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(@firstname)"/>
                        </o:attribute>
                     </input>
                  </div>
                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;lastname&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="lastname" id="">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(@lastname)"/>
                        </o:attribute>
                     </input>
                  </div>
<!-- pass -->

                  <div>
                     <span class="xfm_confirm">
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;pass&#34;] )[1]"/>
                           <o:text>&#160;</o:text>
                        </label>
                        <input type="password" onchange="for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i+1]; input.focus();" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_secret"/>
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;confirm&#34;] )[1]"/>
                           <o:text>&#160;</o:text>
                        </label>
                        <input type="password" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_secret" name="pass">
                           <o:attribute name="onchange">for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i-1]; if (this.value != input.value) { alert('<o:call-template name="xfm:replace">
                                 <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;xfm:secret&#34;] )[1])"/>
                              </o:call-template>'); input.focus(); }</o:attribute>
                        </input>
                     </span>
                  </div>
<!-- list of langs in localized names -->

                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;lang&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <select onchange="for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i+1]; ; input.value=this.options[this.selectedIndex].value;" type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectOne">
                        <option value=" ">
                           <o:if test="@xml:lang=&#34; &#34;">
                              <o:attribute name="selected">selected</o:attribute>
                           </o:if>
                        </option>
                        <o:variable name="xfm:ad0e127" select="@xml:lang"/>
                        <o:for-each select="$messages/xfm:caption[@parent=&#34;lang&#34;]">
                           <o:sort/>
                           <option value="{@id}">
                              <o:if test="$xfm:ad0e127=@id">
                                 <o:attribute name="selected">selected</o:attribute>
                              </o:if>
                              <o:value-of select="."/>
                           </option>
                        </o:for-each>
                     </select>
                     <input type="text" value="" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectOne" size="5" style="" name="userlang" id="lang">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(@xml:lang)"/>
                        </o:attribute>
                     </input>
                  </div>
                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;email&#34;] )[1]"/>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="email" id="">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(@email)"/>
                        </o:attribute>
                     </input>
                  </div>
<!-- parents -->

<!-- pb: can't edit ancestors
                        <div>
                            <xfm:selectMany xsl:select="../sdx:parents/sdx:group/@id" htm:name="group">
                                <xfm:caption xml:lang="fr">Appartenances</xfm:caption>
                                <xfm:caption xml:lang="en">Groups</xfm:caption>
                                <xfm:itemset nodeset="/sdx:document/sdx:results/sdx:result
                                [sdx:field[@name='sdxdocid'] != $id]
                                [sdx:field[@name='sdxdoctype'] = 'Group']
                                ">
                                    <xfm:value ref="sdx:field[@name='sdxdocid']" />
                                    <xfm:caption ref="sdx:field[@name='sdxdocid']" />
                                </xfm:itemset>
                            </xfm:selectMany>
                        </div>
-->

                  <xsl:for-each select="../sdx:parents">
                     <div title="{$messages/xfm:hint[@id=&#34;div8&#34;][1]}">
                        <label>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;label1&#34;] )[1]"/>
                                     
                                    </label>
                        <xsl:for-each select="sdx:group">
                                           - <xsl:value-of select="@id"/>
                        </xsl:for-each>
                     </div>
                  </xsl:for-each>
                  <br/>
                  <div style="text-align:center">
                     <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="save" title="{$messages/xfm:hint[@id=&#34;save&#34;]}" value="{$messages/xfm:caption[@id=&#34;save&#34;]}"/>
                     <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="delete" title="{$messages/xfm:hint[@id=&#34;delete&#34;]}" value="{$messages/xfm:caption[@id=&#34;delete&#34;]}"/>
                  </div>
               </td>
            </tr>
         </form>
      </table>
   </xsl:template>
<!--
    form for a group
    -->

   <xsl:template match="edit/sdx:group">
      <table class="form" style="border:1px solid;text-align:center" cellpadding="4" cellspacing="1" xml:lang="fr">
         <form action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
            <input type="hidden" name="app" value="{$app}"/>
            <tr>
               <td>
                  <div class="legend">
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;group&#34;] )[1]"/>
                  </div>
                  <div>
                     <label class="xfm_label">
                        <o:text> &#160; </o:text>
                        <o:apply-templates select="($messages/xfm:caption[@id=&#34;id&#34;] )[1]"/>
                        <span class="xfm:required" style="color:red">*</span>
                        <o:text>&#160;</o:text>
                     </label>
                     <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" size="" style="" name="id" id="">
                        <o:attribute name="value">
                           <o:value-of select="normalize-space(@id)"/>
                        </o:attribute>
                     </input>
                  </div>
                  <div>
                     <table border="0" cellpadding="0" cellspacing="0" class="xfm_control">
                        <tr>
                           <td>
                              <label class="xfm_label">
                                 <o:text> &#160; </o:text>
                                 <o:apply-templates select="($messages/xfm:caption[@id=&#34;description&#34;] )[1]"/>
                                 <o:text>&#160;</o:text>
                              </label>
                           </td>
                           <td>
                              <textarea type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_textarea" name="desc">
                                 <o:apply-templates select="text()"/>
                              </textarea>
                           </td>
                        </tr>
                     </table>
                  </div>
                  <div>
<!-- parents -->

                     <xsl:for-each select="../sdx:parents">
                        <div title="{$messages/xfm:hint[@id=&#34;div14&#34;][1]}">
                           <label>
                              <o:apply-templates select="($messages/xfm:caption[@id=&#34;label2&#34;] )[1]"/>
                                     
                                    </label>
                           <xsl:for-each select="sdx:group">
                                           - <xsl:value-of select="@id"/>
                           </xsl:for-each>
                        </div>
                     </xsl:for-each>
                  </div>
<!-- pb: can't edit ancestors
                            <xfm:selectMany xsl:select="../sdx:parents/sdx:group/@id" htm:name="group">
                                <xfm:caption xml:lang="fr">Appartenances</xfm:caption>
                                <xfm:caption xml:lang="en">Groups</xfm:caption>
                                <xfm:itemset nodeset="/sdx:document/sdx:results/sdx:result
                                [sdx:field[@name='sdxdocid'] != $id]
                                [sdx:field[@name='sdxdoctype'] = 'Group']
                                ">
                                    <xfm:value ref="sdx:field[@name='sdxdocid']" />
                                    <xfm:caption ref="sdx:field[@name='sdxdocid']" />
                                </xfm:itemset>
                            </xfm:selectMany>
                        -->

<!-- add member -->

                  <table cellpadding="0" cellspacing="0" style="border:none" class="xfm_control">
                     <tr>
                        <label class="xfm_label">
                           <o:text> &#160; </o:text>
                           <o:apply-templates select="($messages/xfm:caption[@id=&#34;member&#34;] )[1]"/>
                           <o:text>&#160;</o:text>
                        </label>
                        <td>
                           <select multiple="multiple" size="5" onkeydown="return xfm_selectKeydown (this, this.form.member);" type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectMany">
                              <o:variable name="xfm:ad0e267" select="../sdx:members/*/@id"/>
                              <o:for-each select="/sdx:document/edit/sdx:results/sdx:result[sdx:field[@name='sdxdocid'] != $id]">
                                 <o:sort select="sdx:field[@name='sdxdocid']"/>
                                 <option value="{sdx:field[@name='sdxdocid']}">
                                    <o:if test="$xfm:ad0e267=normalize-space(sdx:field[@name='sdxdocid'])">
                                       <o:attribute name="selected">selected</o:attribute>
                                    </o:if>
                                    <o:value-of select="sdx:field[@name='sdxdocid']"/>
                                 </option>
                              </o:for-each>
                           </select>
                        </td>
                        <td>
                           <input class="xfm_key" type="button" value=" &gt; " onclick="for (var i=0; this.form.length; i++) if (this.form[i]==this) break; xfm_selectAdd(this.form[i-1],this.form.member); " title="{$messages/xfm:hint[@id=&#34;xfm:select-add&#34;][1]}"/>
                        </td>
                        <td>
                           <select multiple="multiple" size="5" onkeydown="return xfm_selectKeydown(this)" type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_selectMany" name="member">
                              <o:variable name="xfm:bd0e267" select="../sdx:members/*/@id"/>
                              <o:for-each select="/sdx:document/edit/sdx:results/sdx:result[sdx:field[@name='sdxdocid'] != $id]">
                                 <o:sort select="sdx:field[@name='sdxdocid']"/>
                                 <o:if test="$xfm:bd0e267=normalize-space(sdx:field[@name='sdxdocid'])">
                                    <option value="{sdx:field[@name='sdxdocid']}">
                                       <o:value-of select="sdx:field[@name='sdxdocid']"/>
                                    </option>
                                 </o:if>
                              </o:for-each>
                           </select>
                        </td>
                        <td style="width:1%;">
                           <div>
                              <input class="xfm_key" type="button" value=" - " onclick="optionDel(this.form.member); " title="{$messages/xfm:hint[@id=&#34;xfm:select-del&#34;][1]}"/>
                           </div>
                           <div>
                              <input class="xfm_key" type="button" value=" ^ " onclick="optionUp(this.form.member); " title="{$messages/xfm:hint[@id=&#34;xfm:select-up&#34;][1]}"/>
                           </div>
                           <div>
                              <input class="xfm_key" type="button" value=" v " onclick="optionDown(this.form.member); " title="{$messages/xfm:hint[@id=&#34;xfm:select-down&#34;][1]}"/>
                           </div>
                           <div>
                              <input class="xfm_key" type="button" value=" 0 " onclick="xfm_selectReset(this.form.member); " title="{$messages/xfm:hint[@id=&#34;xfm:select-reset&#34;][1]}"/>
                           </div>
                        </td>
                        <td style="width:100%"/>
                     </tr>
                  </table>
                  <br/>
                  <div style="text-align:center">
                     <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="save" title="{$messages/xfm:hint[@id=&#34;save&#34;]}" value="{$messages/xfm:caption[@id=&#34;save&#34;]}"/>
                     <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="delete" title="{$messages/xfm:hint[@id=&#34;delete&#34;]}" value="{$messages/xfm:caption[@id=&#34;delete&#34;]}"/>
                  </div>
               </td>
            </tr>
         </form>
      </table>
   </xsl:template>
<!--
    list to edit
    -->

   <xsl:template match="sdx:results">
      <div class="high">
         <xsl:text>  </xsl:text>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;users&#34;] )[1]"/>
         <xsl:text>  </xsl:text>
      </div>
      <ul style="margin:0" class="menu">
         <xsl:for-each select="sdx:result[normalize-space(sdx:field[@name='sdxdoctype'])='user']">
            <li>
               <a href="identities.xsp?app={sdx:field[@name='sdxappid']}&amp;id={sdx:field[@name='sdxdocid']}&amp;lang={$lang}">
                  <xsl:value-of select="sdx:field[@name='sdxdocid']"/>
                       <xsl:value-of select="sdx:field[@name='firstname']"/> <xsl:value-of select="sdx:field[@name='lastname']"/>
               </a>
            </li>
         </xsl:for-each>
      </ul>
      <form>
         <input type="button" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" onclick="window.location.href='identities.xsp?app={$app}&amp;id=new&amp;doctype=user&amp;lang={$lang}'" class="xfm_button" name="new" title="{$messages/xfm:hint[@id=&#34;new&#34;][1]}" value="{($messages/xfm:caption[@id=&#34;new&#34;] )[1]}"/>
      </form>
      <br/>
      <div class="high">
         <xsl:text>  </xsl:text>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;groups&#34;] )[1]"/>
         <xsl:text>  </xsl:text>
      </div>
      <ul style="margin:0" class="menu">
<!--
            <a href="identities.xsp?app={$app}&amp;id=new&amp;doctype=group&amp;lang={$lang}">
                <strong>
                    <xfm:caption xml:lang="fr">Nouveau groupe</xfm:caption>
                    <xfm:caption xml:lang="en">New group</xfm:caption>
                </strong>
            </a>
-->

         <xsl:for-each select="sdx:result[normalize-space(sdx:field[@name='sdxdoctype'])='group']">
            <li>
               <a href="identities.xsp?app={sdx:field[@name='sdxappid']}&amp;id={sdx:field[@name='sdxdocid']}&amp;lang={$lang}">
                  <xsl:value-of select="sdx:field[@name='sdxdocid']"/>
               </a>
            </li>
         </xsl:for-each>
      </ul>
      <form>
         <input type="button" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" onclick="window.location.href='identities.xsp?app={$app}&amp;id=new&amp;doctype=group&amp;lang={$lang}'" class="xfm_button" name="new" title="{$messages/xfm:hint[@id=&#34;new&#34;][1]}" value="{($messages/xfm:caption[@id=&#34;new&#34;] )[1]}"/>
      </form>
   </xsl:template>
<!--
    not admin
    -->

   <xsl:template match="notadmin">
      <o:apply-templates select="($messages/xfm:caption[@id=&#34;notadmin&#34;] )[1]"/>
   </xsl:template>
<!-- ====== SCRIPT ====================== -->

   <o:template name="xfm:script">
      <script type="text/javascript" language="javascript">

    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, '');
        return true;
    }

    function xfm_focus(o)
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus';
        return true;
    }


            function xfm_load()
            {

                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }


            function xfm_submit(form)
            {
                var message="";
                var name="";

    if (form.member) selectAll(form.member);

    if (name != "id")
    {
        name="id";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;id&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

    if (name != "id")
    {
        name="id";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;id&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

                if (!message) return true ;
                alert(message);
                return false;
            }

            function xfm_reset(form)
            {

            }

                </script>
   </o:template>
<!-- ======  COMMON TEMPLATES  ======================
        short templates to format messages
        known issues : no correct id for a repeated help button
        then hide/show message can't work
        but is it useful to repeat the same help ?

-->

   <o:template match="xfm:help">
      <o:param name="id" select="@id"/>
      <input type="button" class="xfm_help" tabindex="32767" value="?">
         <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
      </input>
      <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
         <o:attribute name="id">
            <o:value-of select="$id"/>_help</o:attribute>
         <tr>
            <td style="width:90%;" class="xfm_bar">
               <o:value-of select="(caption | title | h1 )[1]"/>
            </td>
            <td style="text-align:right" class="xfm_bar">
               <input type="button" class="xfm_key" value="X">
                  <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td colspan="2" class="xfm_text">
               <o:apply-templates mode="copy"/>
            </td>
         </tr>
      </table>
   </o:template>
   <o:template match="xfm:caption" priority="-1">
      <o:apply-templates mode="copy"/>
   </o:template><!--
        Escape js string (or other search/replace)-->
   <o:template match="* | @* | text()" mode="replace" name="xfm:replace">
      <o:param name="string" select="normalize-space(.)"/>
      <o:param name="search">'</o:param>
      <o:param name="replace">\'</o:param>
      <o:choose>
         <o:when test="contains($string, $search)">
            <o:value-of select="concat(substring-before($string, $search), $replace)"/>
            <o:call-template name="xfm:replace">
               <o:with-param name="string" select="substring-after($string, $search)"/>
               <o:with-param name="search" select="$search"/>
               <o:with-param name="replace" select="$replace"/>
            </o:call-template>
         </o:when>
         <o:otherwise>
            <o:value-of select="$string"/>
         </o:otherwise>
      </o:choose>
   </o:template>
   <o:template match="*" priority="-1" mode="copy">
      <o:element name="{name()}">
         <o:apply-templates select="@*" mode="copy"/>
         <o:apply-templates mode="copy"/>
      </o:element>
   </o:template>
   <o:template match="@*" mode="copy">
      <o:copy/>
   </o:template>
<!-- ======  MESSAGES @xml:lang="fr"  ====================== -->

   <xfm:messages xml:lang="fr">
      <xfm:caption id="title"> SDX Administration -  Application - Identités </xfm:caption>
      <xfm:hint id="div8">Groupes auxquels appartient cette identité</xfm:hint>
      <xfm:caption id="label1">Appartenances</xfm:caption>
      <xfm:hint id="div14">Groupes auxquels appartient cette identité</xfm:hint>
      <xfm:caption id="label2">Appartenances</xfm:caption>
      <xfm:caption id="member">Ajouter des membres</xfm:caption>
      <xfm:hint id="new">Créer un nouvel utilisateur</xfm:hint>
      <xfm:hint id="new">Créer un nouveau groupe</xfm:hint>
   </xfm:messages>
<!-- ======  MESSAGES @xml:lang="en"  ====================== -->

   <xfm:messages xml:lang="en">
      <xfm:caption id="title"> SDX Administration - Application - Identities </xfm:caption>
      <xfm:caption id="label1">Memberships</xfm:caption>
      <xfm:caption id="label2">Memberships</xfm:caption>
      <xfm:caption id="member">Add members</xfm:caption>
   </xfm:messages>
   <o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>