<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: i18n2js.xsl 12373 2008-10-15 14:41:34Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Construction du catalogue i18n pour le Javascript
		| On prend les 2 catalogues i18n (le catalogue Pleade et le catalogue local)
		| On recopie les clés du catalogue Pleade sauf s'il existe une clé locale.
		| On recopie enfin les clés locales qui ne se retrouvent pas dans le
		| catalogue Pleade.
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/aggregate">
		<catalogue javascript-var-name="{pleade/catalogue/@javascript-var-name}">
			<xsl:apply-templates select="pleade/catalogue/*" mode="pleade" />
			<xsl:apply-templates select="local/catalogue/message" mode="new" />
		</catalogue>
	</xsl:template>

	<xsl:template match="message" mode="new">
		<xsl:variable name="pleade-key" select="/aggregate/pleade/catalogue/message[@key = current()/@key]" />
		<xsl:if test="not($pleade-key)">
			<xsl:apply-templates select="." mode="copy" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="message" mode="pleade">
		<xsl:variable name="local-key" select="/aggregate/local/catalogue/message[@key = current()/@key]" />
		<xsl:choose>
		  <xsl:when test="$local-key">
				<xsl:apply-templates select="$local-key" mode="copy" />
			</xsl:when>
		  <xsl:otherwise>
				<xsl:apply-templates select="." mode="copy" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@*|*|text()" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" mode="copy" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
