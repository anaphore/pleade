<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT construisant l'XHTML de la navigation dans les index (via sdx:terms)
		+-->
<xsl:stylesheet
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		version="2.0" exclude-result-prefixes="xsl sdx i18n pleade xs">



	<!-- Les fonctions générales de Pleade -->
	<xsl:import href="../../commons/xsl/functions.xsl"/>

	<!-- Le code pour traiter la navigation d'une page à l'autre -->
	<xsl:import href="navigation.xsl"/>

	<!-- Un paramètre qui nous informe sur le contexte -->
	<xsl:param name="context" select="''"/>

	<!-- Numéro du champ de recherche, utilisé pour générer les évènements sur les liens dans les popups -->
	<xsl:param name="indexnum" select="number(0)"/>

	<!-- Une variable qui indique le contexte de navigation:
			page:				Page indépendante
			search-form-panel:	Panneau dans un formulaire de recherche
	-->
	<xsl:variable name="display-context">
		<xsl:choose>
			<xsl:when test="$context = ''">page</xsl:when>
			<xsl:otherwise><xsl:value-of select="$context"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- La base de documents dans laquelle on cherche -->
	<xsl:param name="base" select="'ead'"/>

	<!-- La "valeur", c'est-à-dire, par exemple, une première lettre -->
	<xsl:param name="value" select="''"/>
	
	<!-- Le champ affiché -->
	<xsl:variable name="field" select="/sdx:document/sdx:terms/@field"/>

	<!--les filtres eventuels-->
	<xsl:variable name="filter">
		<xsl:for-each select="sdx:document/sdx:terms/sdx:filter/sdx:filter[@field != $field]">
			<xsl:value-of select="concat('&amp;field=',@field,'&amp;value=',@value)"/>
		</xsl:for-each>
	</xsl:variable>

	<!--On recalcule value : dans le cas où il y a un des filtre, le value lié au champs ne doit pas exister (erreur sdx)-->
	<xsl:variable name="value2">
		<xsl:if test="$filter=''">
			<xsl:value-of select="$value"/>
		</xsl:if>
	</xsl:variable>
	
	<!-- [CB] FIXME : rajouter pour déboguer le problème des filtres sur les navindex en attendant que SDX dispose de quoi le faire proprement
					Quand on a un filtre et un filtre par lettre (navigation de l'index par lettre) :
						- la navigation par page est automatiquement désactivée (on ne peut pas recalculer le nombre de pages).
						- les sdx:term sont filtrés pour n'afficher que ceux qui correspondent à l'initiale
						- le nombre de résultats est recalculé pour ne compter que les sdx:term qui commencent par l'initiale recherchée
	-->
	<!-- Cas d'une navigation par lettre -->
	<xsl:variable name="navByLetter" as="xs:string">
		<xsl:choose>
			<xsl:when test="sdx:document/sdx:terms/sdx:filter/sdx:filter[@field = $field] and ends-with(sdx:document/sdx:terms/sdx:filter/sdx:filter[@field = $field]/@value, '*')">
				<xsl:value-of select="substring-before(sdx:document/sdx:terms/sdx:filter/sdx:filter[@field = $field]/@value, '*')"/>
			</xsl:when>
			<xsl:when test="contains($value, '*')"><xsl:value-of select="substring-before($value, '*')"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- Le nombre de résultats par page -->
	<xsl:variable name="hpp" select="if ( $filter != '' and $navByLetter != '' ) then '-1' else /sdx:document/sdx:terms/@hpp"/>
	

	<!-- On traite l'élément racine envoyé par SDX -->
	<xsl:template match="/sdx:document" >
		<!-- DEBUG
		<xsl:message>
			hpp = <xsl:value-of select="$hpp"/>
			nb = <xsl:value-of select="/sdx:document/sdx:terms/@nb"/>
			filter = <xsl:value-of select="$filter"/>
			field = <xsl:value-of select="$field"/>
			value = <xsl:value-of select="$value"/>
			value2 = <xsl:value-of select="$value2"/>
			navByLetter = <xsl:value-of select="$navByLetter"/>
		</xsl:message>  -->

		<!-- Selon le contexte, on sort une page complète ou seulement un div -->

		<xsl:choose>
			<xsl:when test="$display-context = 'page'">
				<!-- On sort une page HTMl -->
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<title>
							<i18n:text key="navindex.title">navindex.title</i18n:text>
						</title>
					</head>
					<body>

						<!-- L'en-tête de page -->
						<h1 class="pl-title">
							<!-- La clé i18n contient le nom du champ à afficher -->
							<i18n:text key="navindex.name.{$field}">
								<xsl:text>navindex.name.</xsl:text>
								<xsl:value-of select="$field"/>
							</i18n:text>
						</h1>

						<!-- On traite les termes obtenus -->
						<xsl:apply-templates select="sdx:terms"/>
					</body>
				</html>
			</xsl:when>
			<xsl:when test="$display-context = 'search-form-panel'">
				<!-- On sort seulement un div -->
				<xsl:variable name="divid" select="concat('pl-pg-terms-container-', $field)"/>
				<xsl:variable name="parentdivid" select="concat('index.content.', $field)"/>
				<div id="{$divid}">
					<!-- On traite les termes obtenus -->
					<xsl:apply-templates select="sdx:terms">
						<xsl:with-param name="parentdivid" select="$parentdivid" tunnel="yes"/>
					</xsl:apply-templates>
					<!-- Un javascript pour réinitialiser les liens -->
					<script type="text/javascript">
						searchForm.prepareTermLinks("pl-pg-term", "<xsl:value-of select="$parentdivid"/>", "<xsl:value-of select="$field"/>", "<xsl:value-of select="$indexnum"/>");
						initButtons();
					</script>
				</div>

			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- La liste de termes à afficher-->
	<xsl:template match="sdx:terms">
		<xsl:param name="parentdivid" select="''" tunnel="yes"/>

		<xsl:variable name="is-date-field" select="pleade:is-date-field(@field,sdx:term[1]/@value)" /><!-- Marqueur du type de champ date -->
		<xsl:variable name="mUrl" select="concat('cocoon://functions/list-first-letters.xml?field=', $field)" /><!-- L'URL interne qui ramène les premières lettres de l'index courant -->

		<!-- Marqueur pour activer l'affichage de la barre de sélection des premières lettres
				 Pour les champs date on n'active pas
				 Quand on ne parvient pas à recupérer la liste des premières lettres, on n'active pas.-->
		<xsl:variable name="active-first-letters" select="
												if($is-date-field) then '0'
												else if(doc-available($mUrl)) then '1'
												else '0'" />

		<xsl:if test="$active-first-letters='1' and doc-available($mUrl)"><!-- On affiche la barre de
		sélection des premières lettres -->

			<xsl:variable name="list-first-letters" select="document($mUrl)" /><!-- Le document XML contenant la liste des premières lettres -->
			

				<script type="text/javascript">
						var data<xsl:value-of select="$indexnum"/>={
							"displaycontext": '<xsl:value-of select="$display-context"/>',
							"filter" : '<xsl:value-of select="$filter"/>',
							"field": '<xsl:value-of select="$field"/>',
							"hpp": '<xsl:value-of select="if ( $filter != '') then '-1' else $hpp"/>',
							"base": '<xsl:value-of select="$base"/>',
							"indexnum": '<xsl:value-of select="$indexnum"/>',
							"parentdivid": '<xsl:value-of select="$parentdivid"/>'
						};
						YAHOO.util.Event.on("first-letter<xsl:value-of select="$indexnum"/>", "change", searchForm.changeFirstLetter, data<xsl:value-of select="$indexnum"/>);
						YAHOO.util.Event.on("changeFirstLetterBtn<xsl:value-of select="$indexnum"/>", "click", searchForm.changeFirstLetter, data<xsl:value-of select="$indexnum"/>);
				</script>
				<table class="pl-chng-frst-lttr">
					<tr>
						<td>
							<select name="first-letter{$indexnum}" id="first-letter{$indexnum}">
								<option value="all"><i18n:text key="form.terms.panel.letters.all">form.terms.panel.letters.all</i18n:text></option>
								<xsl:for-each select="$list-first-letters/select/option">
									<option>
										<xsl:copy-of select="@*"/>
										<xsl:if test="$navByLetter = d:decode(@value, 'utf-8')" xmlns:d="java:java.net.URLDecoder">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:if>
										<xsl:copy-of select="./text()"/>
									</option>
								</xsl:for-each>
							</select>
						</td>
						<td>
							<button type="button" id="changeFirstLetterBtn{$indexnum}"
										title="navindex.letters.select.go.button.title" i18n:attr="title"
										class="yui-button yui-push-button">
								<i18n:text key="navindex.letters.select.go.button.text">navindex.letters.select.go.button.text</i18n:text>
							</button>
						</td>
					</tr>
				</table>

		</xsl:if>

		<!--		Pour les popups des formulaires, on utilisera une navigation avec des liens sur des lettres
				plutôt qu'un formulaire, pour ne pas imbriquer deux formulaires dans la page.-->

		<!-- On affiche les termes qu'on a -->
		<div class="document-results">
			<xsl:apply-templates select="." mode="navbar"/>
		</div>

		<!-- Affichage des termes eux-mêmes -->
		<xsl:choose>
			<xsl:when test="@nb!=0">
				<!-- On a des termes, on les affiche -->
				<table>
					<xsl:choose>
						<!-- FIXME : on ne traite que les sdx:term qui commencent par l'initiale recherchée dans le cas où on a un filtre -->
						<xsl:when test="$filter != '' and $navByLetter != ''">
							<xsl:apply-templates select="sdx:term[starts-with(@value, $navByLetter)]">
								<xsl:with-param name="is-date-field" select="$is-date-field" />
							</xsl:apply-templates>	
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="sdx:term">
								<xsl:with-param name="is-date-field" select="$is-date-field" />
							</xsl:apply-templates>
						</xsl:otherwise>
					</xsl:choose>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<!-- Pas de termes, alors on affiche un message -->
				<i18n:text key="navindex.noresults">navindex.noresults</i18n:text>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>


	<!-- Affichage de la barre de navigation dans une liste de termes (page par page) -->
	<xsl:template match="sdx:terms" mode="navbar">
		<xsl:param name="parentdivid" select="''" tunnel="yes"/>
		<!-- La clé i18n selon singulier / pluriel -->
		<xsl:variable name="key-suffix">
			<xsl:choose>
				<xsl:when test="@nb &lt; 2">one</xsl:when>
				<xsl:otherwise>many</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div class="navigation-results">
			<p>
				<!-- Nombre de termes -->
				<span class="nbresults">
					<strong>
						<!-- FIXME : le nombre diffère dans le cas d'une navigation par lettre car certains termes ne seront pas affichés -->
						<xsl:value-of select="if ( $filter != '' and $navByLetter != '' ) then count(sdx:term[starts-with(@value, $navByLetter)]) else @nb"/>
						<xsl:text> </xsl:text>
						<i18n:text key="navindex.results.{$key-suffix}"/>
					</strong>
				</span>

				<!-- Navigation par pages si nécessaire -->
				<xsl:if test="@nbPages > 1">

					<!-- Informations sur la page en cours, le nombre de pages -->
					<span class="nbpages">
						<i18n:text key="navindex.page"/>	<!-- Page -->
						<xsl:text> </xsl:text>
						<strong><xsl:value-of select="@page"/></strong>
						<xsl:text> </xsl:text><i18n:text key="navindex.page.of"/><xsl:text> </xsl:text>	<!-- de -->
						<xsl:value-of select="@pages"/>
					</span>

					<span class="navpage">
						<!-- On appelle notre méthode générique de navigation par pages -->
						<xsl:apply-templates select="." mode="hpp">
							<!-- L'URL de base à appeler -->
							<xsl:with-param name="url">
								<xsl:choose>
									<xsl:when test="$display-context='search-form-panel'">
										<xsl:text>navindex.xsp</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>navindex.html</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<!-- Les paramètres à ajouter -->
							<xsl:with-param name="query">
								<!--<xsl:value-of select="/sdx:document/@query"/>-->
								<xsl:if test="normalize-space($base) != ''">
									<xsl:text>&amp;base=</xsl:text>
									<xsl:value-of select="$base"/>
								</xsl:if>
								<xsl:value-of select="$filter"/>
								<xsl:text>&amp;field=</xsl:text>
								<xsl:value-of select="$field"/>
								<xsl:if test="normalize-space($value2) != ''">
									<xsl:text>&amp;value=</xsl:text>
									<xsl:value-of select="$value2"/>
								</xsl:if>
								<xsl:text>&amp;context=</xsl:text>
								<xsl:value-of select="$display-context"/>
								<xsl:text>&amp;indexnum=</xsl:text>
								<xsl:value-of select="$indexnum"/>
							</xsl:with-param>
							<!-- Les résultats par page -->
							<xsl:with-param name="hpp" select="$hpp"/>
							<xsl:with-param name="onclick">
								<xsl:choose>
									<xsl:when test="$display-context='search-form-panel'">
										<xsl:text>windowManager.loadAjaxContent(this.href, '</xsl:text>
										<xsl:value-of select="$parentdivid"/>
										<xsl:text>'); return false;</xsl:text>
									</xsl:when>
									<xsl:otherwise></xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
						</xsl:apply-templates>
					</span>
				</xsl:if>
			</p>
		</div>
	</xsl:template>

	<!-- Affichage de la liste des lettres de navigation -->
	<xsl:template match="sdx:terms" mode="navindex.navletter"/>

	<!-- Affichage d'une lettre pour la navigation -->
	<xsl:template name="display-navigation-letter">
		<!-- La lettre à traiter -->
		<xsl:param name="letter" select="''"/>
		<!-- La valeur qui sera cherchée -->
		<xsl:variable name="v" select="concat($letter, '*')"/>
		<!-- L'option dans la liste -->
		<option value="{$v}">
			<!-- On vérifie si c'est celle déjà affichée -->
			<xsl:if test="$v = $value2">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<!-- On affiche la lettre -->
			<xsl:value-of select="$letter"/>
		</option>
	</xsl:template>

	<!-- Affichage d'un terme -->
	<xsl:template match="sdx:term">
		<xsl:param name="is-date-field" select="false()" />
		<tr>
			<!-- Le numéro du terme dans la liste -->
			<td class="number">
				<span class="number">
					<xsl:value-of select="if ($filter != '' and $navByLetter != '') then position() else @no"/>
					<xsl:text>. </xsl:text>
				</span>
			</td>
			<!-- Le terme lui-même -->
			<td class="term">

				<xsl:choose>
					<!-- lien pour remplir le champ d'un formulaire -->
					<xsl:when test="$display-context='search-form-panel'">

								<a	i18n:attr="title"
									title="form.field.panel.link.title"
									href="javascript:void(0);"
									class="pl-pg-term"
								>
									<xsl:apply-templates select="@value">
										<xsl:with-param name="is-date-field" select="$is-date-field" />
									</xsl:apply-templates>
								</a>

					</xsl:when>
					<xsl:otherwise>

						<xsl:choose>
							<!-- Si un seul doc, alors on fait un lien vers ce document -->
							<xsl:when test="number(@docs) = 1">
								<a	onclick="return windowManager.winFocus(this.href, 'docead');"
									i18n:attr="title"
									title="navindex.result.link.doc.title"
									href="ead.html?c={@id}"
									class="pl-pg-term"
								>
									<xsl:apply-templates select="@value">
										<xsl:with-param name="is-date-field" select="$is-date-field" />
									</xsl:apply-templates>
								</a>
							</xsl:when>
							<!-- Si plusieurs docs, on lance une recherche -->
							<xsl:otherwise>
								<!-- Le lien sur le terme -->
								<a	i18n:attr="title"
									title="navindex.result.link.docs.title"
									href="results.html?cop1=AND&amp;champ1={$field}&amp;query1={urle:encode(@value, 'UTF-8')}&amp;linkBack=navindex"
									xmlns:urle="java:java.net.URLEncoder"
									class="pl-pg-term"
								>
									<xsl:apply-templates select="@value">
										<xsl:with-param name="is-date-field" select="$is-date-field" />
									</xsl:apply-templates>
								</a>
								<!-- Le nombre d'occurrences -->
								<xsl:text> (</xsl:text>
								<xsl:value-of select="@docs"/>
								<xsl:text> </xsl:text>
								<i18n:text key="navindex.result.occurs">navindex.result.occurs</i18n:text>
								<xsl:text>)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:otherwise>
				</xsl:choose>



			</td>
		</tr>
	</xsl:template>

	<!--+
	    | Traitement de la valeur d'un terme
			|
			| On cherche à afficher les dates (donc format ISO 8601) de manière
			| lisible. Pour yyyy-MM-ddTHH:mm:ssZ on veut renvoyer yyyy-MM-dd.
			| La difficulté est qu'on ne connaît pas le type de champ que l'on traite.
			| On sait seulement que les champs de type "date" par défaut de Pleade
			| sont : "edate", "bdate", "sdxmoddate". La manière utilisée ici pour
			| reconnaître les champs "date" est de regarder s'il s'agit de l'un des
			| champs donnés ci-avant, sinon on supprime les chiffres de la valeur à
			| traiter et s'il ne reste que "- -T::Z" (dans les guillemets, l'espace
			| entre les tirets n'est là que parce qu'on n'a pas le droit de mettre 2
			| tirets à la suite dans un commentaire XML) alors on sait qu'on traite un
			| champ de type "date".
	    +-->
	<xsl:template match="sdx:term/@value">
		<xsl:param name="is-date-field" select="false()" />
		<xsl:variable name="v" select="." />
		<xsl:choose>
		  <xsl:when test="$is-date-field and contains($v,'T')"><!-- Un champ de type date de Pleade -->
				<xsl:value-of select="substring-before($v,'T')" /><!-- Pour yyyy-MM-ddTHH:mm:ssZ on renvoie yyyy-MM-dd -->
			</xsl:when>
		  <xsl:otherwise><!-- Un champ de type autre que date -->
				<xsl:value-of select="$v" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
