<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Construction du formulaire d'administration de la recherche et de la
		|	publication
    +-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		xmlns:netutils="java:org.pleade.utils.NetUtilities"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		exclude-result-prefixes="xsl xsp i18n dir fn netutils pleade">

	<xsl:import href="../../commons/xsl/functions.xsl"/>
		
	<xsl:param name="cp_msg" select="''" />
	<xsl:param name="cp_cur" select="''" />
	<xsl:param name="cp_new" select="''" />
	<xsl:param name="cp_schema" select="''" />
	<xsl:param name="fn" select="''" />
	<xsl:param name="tp" select="''" />
	<xsl:param name="ti" select="''" />
	<xsl:param name="as" select="''" />
	<xsl:param name="ext" select="'.xconf'" />
	<xsl:param name="curdir" select="'.'" />

  <xsl:variable name="i18n-root" select="concat('ead-admin.', $fn)" />

	<xsl:variable name="dir" select="/dir:directory | /directory" />
	<xsl:variable name="files" select="$dir/dir:file[not(ends-with(@name, '.backup') or ends-with(@name, '.old'))]" />
	<xsl:variable name="backups" select="$dir/dir:file[ends-with(@name, '.backup')]" />

	<!-- Les actions possibles sur les fichiers :
				r = récupérer
				c = charger
				a = ajouter
				s = supprimer
				b = revenir au backup
				d = redémarrer l'application
	-->
	<xsl:variable name="actions">
		<xsl:choose>
			<xsl:when test="$as!=''">
				<xsl:value-of select="$as" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>,c,b,</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="type">
		<xsl:choose>
			<xsl:when test="count($dir//dir:file|$dir//file)=0">
				<xsl:text>no_doc</xsl:text>
			</xsl:when>
			<xsl:when test="$tp!=''">
				<xsl:value-of select="$tp" />
			</xsl:when>
			<xsl:when test="(count($files) &gt; 1 and count($files) &lt;= 5)
												or (count($files)=1 and contains($actions, ',a,'))">
				<xsl:text>onglet</xsl:text>
			</xsl:when>
			<xsl:when test="count($files)=1">
				<xsl:text>unique</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>multiple</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

  <!-- Imports -->
	<xsl:include href="form2xhtml-commons.xsl"/>

  <xsl:template match="/">
<!--<xsl:message>
 cp_msg = "<xsl:value-of select="$cp_msg" />"
 cp_cur = "<xsl:value-of select="$cp_cur" />"
 curdir = "<xsl:value-of select="$curdir" />"
    ext = "<xsl:value-of select="$ext" />"
     fn = "<xsl:value-of select="$fn" />"
     tp = "<xsl:value-of select="$tp" />"
     ti = "<xsl:value-of select="$ti" />"
  files = <xsl:value-of select="count($files)" />
   type = <xsl:value-of select="$type" />
actions = <xsl:value-of select="$actions" />
</xsl:message>-->

    <xsl:apply-templates select="." mode="html" />

  </xsl:template>

	<!--+
	    | Construction de la balise <html>
	    +-->
	<xsl:template match="/" mode="html">

		<html>

			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<title>
					<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.html.title">
						<xsl:text>administration</xsl:text>
					</i18n:text>
				</title>
				<xsl:if test="$type='onglet'">
				<xsl:comment>yui</xsl:comment>
				<script type="text/javascript" src="yui/tabview/tabview-min.js">//</script>
				<link type="text/css" href="yui/assets/skins/pleade/tabview.css" rel="stylesheet" />
				<link type="text/css" href="yui/assets/skins/pleade/container.css" rel="stylesheet" />
				</xsl:if>
				<xsl:comment>pleade</xsl:comment>
				<script type="text/javascript" src="i18n/module-eadeac-edit-js.js">//</script>
				<!--<script type="text/javascript" src="js/module-eadeac.js">//</script>-->
				<!--<script type="text/javascript" src="js/module-eadeac-edit.js">//</script>-->
				
				<script type="text/javascript" src="js/pleade/common/util/HelpPanels.js">//</script>
				<script type="text/javascript" src="js/pleade/admin/eadeac/AdminManager.js">//</script>
				
				<script type="text/javascript">
				var osname = "<xsl:value-of select="sys:get-property('os.name')" xmlns:sys="java.lang.System" />";
				var filesep = "/";
				if ( osname.indexOf("Windows")!=-1 ) filesep = "\\";
					var extension = "<xsl:value-of select="$ext" />";
					var files = [<xsl:for-each select="$files"><xsl:if test="position()!=1">,</xsl:if>'<xsl:value-of select="@name" />'</xsl:for-each>];
					var bckps = '';
					var b_tabs = false;
					<xsl:if test="$type='multiple'">
					bckps = [<xsl:apply-templates select="$dir" mode="backups-array-list" />];
					</xsl:if>
					<xsl:if test="$type='onglet'">
						b_tabs = true;
					</xsl:if>
					<xsl:variable name="q">
						<xsl:text>"</xsl:text>
					</xsl:variable>
					window.addEvent("load", init);
					var adminManager = '';
					var ti = '';
					var cp_cur = '';
					<xsl:if test="$ti">
						ti = <xsl:value-of select="$ti"/>;
					</xsl:if>
					<xsl:if test="$cp_cur">
						cp_cur = <xsl:value-of select="concat($q,$cp_cur,$q)"/>;
					</xsl:if>
					function init(){
						if(ti ==''){
							ti = null;
						}
						if(cp_cur ==''){
							cp_cur = null;
						}
						adminManager = new AdminManager(bckps,b_tabs,ti,cp_cur);
					}
				</script>
			</head>

			<body>

        <div class="pl-title">
          <xsl:call-template name="build-help">
            <xsl:with-param name="v" select="'title'" />
            <xsl:with-param name="k" select="concat($i18n-root, '.title')" />
          </xsl:call-template>
          <h1>
            <i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.pl-title">
              <xsl:text>administrer la recherche et la publication</xsl:text>
            </i18n:text>
          </h1>
        </div>

				<div class="pl-form-intro pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.intro" />
				</div>

        <div class="pl-form-container">

				<form action="{$fn}.html" enctype="multipart/form-data" method="post"
							id="ead-admin-ead-form">

              <xsl:attribute name="class">
                <xsl:text>pl-form-ead-admin</xsl:text>
                <xsl:if test="$type='onglet'"><xsl:text>-onglet</xsl:text></xsl:if>
              </xsl:attribute>

					<input type="hidden" value="" name="action" />
					<input type="hidden" value="" name="current" />
					<input type="hidden" value="" name="backup" />
					<input type="hidden" value="{$ext}" name="ext" />
					<xsl:if test="$type='onglet'">
						<input type="hidden" value="" name="tabindex" />
					</xsl:if>

					<xsl:choose>

						<!-- 	Affichage sous forme d'onglets -->
						<xsl:when test="$type='onglet'">

							<div id="tabs" class="yui-navset pl-pgd-tabs">

								<ul id="ead-admin-ead-onglet" class="yui-nav">
									<xsl:apply-templates select="$files" mode="tab-button" />
								</ul>

								<div id="ead-admin-ead-box" class="yui-content">
									<xsl:apply-templates select="$files" mode="tab-content" />
								</div>

							</div>

						</xsl:when>

						<!-- Affichage fichier unique -->
						<xsl:when test="$type='unique'">
							<xsl:apply-templates select="$files" mode="unique-content" />
						</xsl:when>

						<!-- Affichage multiples -->
						<xsl:when test="$type='multiple'">
							<xsl:apply-templates select="$dir" mode="multiple-content" />
						</xsl:when>

						<!-- Affichage pas de document -->
						<xsl:when test="$type='no_doc'">
							<xsl:choose>
								<xsl:when test="contains( $actions, ',a,' )">
									<div class="pl-form-message">
										<i18n:text catalogue="module-eadeac-edit" key="ead-admin.no_doc_addfile.msg">
												il n'existe aucun document de cette catégorie. vous pouvez en ajouter.
										</i18n:text>
									</div>
									<xsl:call-template name="addfile-content" />
								</xsl:when>
								<xsl:otherwise>
									<div class="pl-form-message">
										<i18n:text catalogue="module-eadeac-edit" key="ead-admin.no_doc.msg">
											il n'existe aucun document de cette catégorie.
											<br/> si vous pensez que c'est une erreur, contactez
											l'administrateur système.
										</i18n:text>
									</div>
								</xsl:otherwise>
							</xsl:choose>

						</xsl:when>

					</xsl:choose>

				</form>

        </div>

				<div class="pl-form-message pl-form-conclu">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.conclusion" />
				</div>

			</body>

		</html>

	</xsl:template>

	<!--+
	    |	Construction des onglets : les boutons
	    +-->
	<xsl:template match="dir:file | file" mode="tab-button">

		<xsl:variable name="n" select="@name" />
		<!--<xsl:variable name="l" select="substring-before($n, $ext)" />-->
		<xsl:variable name="l" select="pleade:get-url-file-wo-extension($n)" />

		<li>
			<xsl:if test="(number($ti)&gt;0 and number($ti)=position() ) or position()=1">
				<xsl:attribute name="class">
					<xsl:text>selected</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<a href="#{$n}">
				<em>
					<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.form.tabs.{$l}">
						<xsl:value-of select="$l" />
					</i18n:text>
				</em>
			</a>
		</li>

		<xsl:if test="position()=last()">
			<xsl:if test="contains($actions, ',a,')">
				<li>
					<a href="#addfile">
						<em>
							<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.form.tabs.addfile">
								<xsl:text>ajouter un fichier</xsl:text>
							</i18n:text>
						</em>
					</a>
				</li>
			</xsl:if>
			<xsl:if test="contains($actions, ',d,')">
				<li>
					<a href="#restart">
						<em>
							<i18n:text catalogue="module-eadeac-edit" key="ead-admin.form.tabs.restart">
								<xsl:text>redémarrer l'application</xsl:text>
							</i18n:text>
						</em>
					</a>
				</li>
			</xsl:if>
		</xsl:if>

	</xsl:template>

	<!--+
	    |	Affichage de la gestion de fichiers sous forme d'onglets
	    +-->
	<xsl:template match="dir:file | file" mode="tab-content">

		<xsl:variable name="c">
			<xsl:choose>
				<xsl:when test="position()=1">
					<xsl:text>tab</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>tab yui-hidden</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div id="{@name}" class="{$c}">
			<xsl:apply-templates select="." mode="content">
				<xsl:with-param name="pos" select="position()" />
			</xsl:apply-templates>
		</div>

		<xsl:if test="position()=last()">
			<xsl:if test="contains($actions, ',a,')">
				<div id="addfile" class="tab yui-hidden">
					<xsl:call-template name="addfile-content">
						<xsl:with-param name="pos" select="position() + 1" />
					</xsl:call-template>
				</div>
			</xsl:if>
			<xsl:if test="contains($actions, ',d,')">
				<div id="restart" class="tab yui-hidden">
					<xsl:call-template name="restart-content">
						<xsl:with-param name="pos" select="position() + 1" />
					</xsl:call-template>
				</div>
			</xsl:if>
		</xsl:if>

	</xsl:template>

	<!--+
	    | Affichage de la gestion d'un fichier unique
	    +-->
	<xsl:template match="dir:file | file" mode="unique-content">
		<div id="{@name}">
			<xsl:apply-templates select="." mode="content" />
		</div>
		<xsl:if test="contains($actions, ',a,')">
			<xsl:call-template name="addfile-content">
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="contains($actions, ',d,')">
			<xsl:call-template name="restart-content">
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!--+
	    | Affichage de la gestion de multiple fichiers
	    +-->
	<xsl:template match="dir:directory | directory" mode="multiple-content">

		<div id="{@name}">

			<xsl:if test="$cp_msg">
				<fieldset>
					<legend>
						<xsl:comment>u</xsl:comment>
						<i18n:text catalogue="module-eadeac-edit" key="ead-admin.result.legend">
							<xsl:text>message d'information</xsl:text>
						</i18n:text>
					</legend>
					<div class="pl-form-message">
						<xsl:comment>u</xsl:comment>
						<i18n:text catalogue="module-eadeac-edit" key="ead-admin.msg.{$cp_msg}">
							<xsl:value-of select="$cp_msg" />
						</i18n:text>
					</div>
				</fieldset>
			</xsl:if>

      <xsl:call-template name="build-help">
        <xsl:with-param name="v" select="'info'" />
        <xsl:with-param name="k" select="concat($i18n-root, '.info')" />
      </xsl:call-template>

			<fieldset>

				<legend>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.multiple.legend">
						gestion de fichiers
					</i18n:text>
				</legend>

				<select id="doc-select" class="pl-form-slct" tabindex="1">

					<option>
						<xsl:if test="not(descendant::dir:file or descendant::file)">
							<xsl:choose>
									<xsl:when test="contains( $actions, ',a,' )">
										<div class="pl-form-message">
											<i18n:text catalogue="module-eadeac-edit" key="ead-admin.no_doc_addfile.msg">
													il n'existe aucun document de cette catégorie. vous pouvez en ajouter.
											</i18n:text>
										</div>
										<xsl:call-template name="addfile-content" />
									</xsl:when>
									<xsl:otherwise>
										<div class="pl-form-message">
											<i18n:text catalogue="module-eadeac-edit" key="ead-admin.no_doc.msg">
												il n'existe aucun document de cette catégorie.
											</i18n:text>
										</div>
									</xsl:otherwise>
								</xsl:choose>
						</xsl:if>
					</option>

					<xsl:apply-templates select="*" mode="docs-selects-option">
						<xsl:with-param name="n" select="1" />
					</xsl:apply-templates>

				</select>

				<div class="pl-form-bttn-container">

					<xsl:if test="contains($actions, 'r')">
						<button type="button" id="get"
													title="module-eadeac-edit:ead-admin.get-file.title" i18n:attr="title"
													class="yui-button yui-push-button pl-form-bttn"
													onclick="javascript: adminManager.url2getFile($('doc-select'), '{netutils:encodeUrl(replace($curdir, '\\','/'),'UTF-8')}');"
													accesskey="g" tabindex="2">
							<i18n:text catalogue="module-eadeac-edit" key="ead-admin.get-file.text" >
								<xsl:text>récupérer le fichier</xsl:text>
							</i18n:text>
						</button>
					</xsl:if>
					<xsl:text>&#160;</xsl:text>
					<xsl:if test="contains($actions, 'b')">
						<button type="button" id="backups"
													title="module-eadeac-edit:ead-admin.backup.submit.title" i18n:attr="title"
													class="yui-button yui-push-button pl-form-bttn"
													accesskey="b" tabindex="3"
													onclick="javascript:var v=$($('doc-select')); var ret=adminManager.ctrlBckp(v); if(ret==true) ret=adminManager.form2work(this.form, 'backup', v, 0); if(ret==true) this.form.submit();">
							<i18n:text catalogue="module-eadeac-edit" key="ead-admin.backup.submit.text" >
								<xsl:text>revenir à la sauvegarde...</xsl:text>
							</i18n:text>
						</button>
					</xsl:if>
					<xsl:text>&#160;</xsl:text>
					<xsl:if test="contains($actions, 's')">
						<button type="button" id="delete"
													title="module-eadeac-edit:ead-admin.delete-file.title" i18n:attr="title"
													class="yui-button yui-push-button pl-form-bttn"
													accesskey="d" tabindex="4">
							<xsl:attribute name="onclick">
								<xsl:text>javacript: var ret= adminManager.deleteFile(this.form); if(ret==true) this.form.submit();</xsl:text>
							</xsl:attribute>
							<i18n:text catalogue="module-eadeac-edit" key="ead-admin.delete-file.text" >
								<xsl:text>supprimer le fichier</xsl:text>
							</i18n:text>
						</button>
					</xsl:if>

				</div>

        <xsl:if test="contains($actions, 'c')">
          <div>
            <label for="newfilenewfile">
              <i18n:text catalogue="module-eadeac-edit" key="ead-admin.replace-file.legend">
                <xsl:text>remplacer par un nouveau fichier</xsl:text>
              </i18n:text>
            </label>
            <p>
              <input id="newfilenewfile" class="pl-form-npt-file newfile" size="40" type="file"/>
            </p>
            <div class="pl-form-bttn-container">
              <button type="button" id="replace"
                            title="module-eadeac-edit:ead-admin.new-file.submit.title" i18n:attr="title"
                            class="yui-button yui-push-button pl-form-search-submit"
                            accesskey="r" tabindex="5">
                <xsl:attribute name="onclick">
                    <xsl:text>javacript:var ret=adminManager.form2work(this.form, 'newfile', 'newfile', 0); if(ret==true) this.form.submit();</xsl:text>
                  </xsl:attribute>
                <i18n:text catalogue="module-eadeac-edit" key="ead-admin.replace-file.submit.text" >
                  <xsl:text>valider l'envoi...</xsl:text>
                </i18n:text>
              </button>
            </div>
          </div>
        </xsl:if>

			</fieldset>

			<!-- Si on le doit : un bouton pour ajouter un fichier -->
			<xsl:if test="contains( $actions, ',a,' )">
			<xsl:call-template name="addfile-content">
					<xsl:with-param name="subdir" select="'true'" />
				</xsl:call-template>
			</xsl:if>

			<!-- Si on le doit : un bouton pour redémarrer l'application -->
			<xsl:if test="contains( $actions, ',d,' )">
        <xsl:call-template name="restart-content" />
			</xsl:if>

		</div>

	</xsl:template>
	<xsl:template match="dir:directory | directory" mode="docs-selects-option">

		<xsl:param name="n" select="0" />
		<xsl:param name="p" />

		<option class="pl-ead-publish-dir" disabled="disabled" label="{@name}">
			<xsl:call-template name="indent">
				<xsl:with-param name="n" select="$n" />
			</xsl:call-template>
			<xsl:value-of select="@name" />
		</option>
		<xsl:apply-templates select="*" mode="docs-selects-option">
			<xsl:with-param name="n" select="$n + 1" />
			<xsl:with-param name="p">
					<xsl:choose>
						<xsl:when test="$p!=''">
							<xsl:value-of select="concat( $p, '/', @name )" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@name" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
		</xsl:apply-templates>

	</xsl:template>
	<xsl:template match="dir:file | file" mode="docs-selects-option">

		<xsl:param name="n" select="0" />
		<xsl:param name="p" select="''" />

		<xsl:variable name="m" select="normalize-space(@name)" />

		<xsl:if test="not(ends-with($m, '.backup'))">
			<option class="pl-ead-publish-file">
				<xsl:attribute name="value">
					<xsl:if test="$p!=''"><xsl:value-of select="concat($p, '/')" /></xsl:if>
					<xsl:value-of select="@name" />
				</xsl:attribute>
				<xsl:call-template name="indent">
					<xsl:with-param name="n" select="$n" />
				</xsl:call-template>
				<xsl:value-of select="$m" />
			</option>
		</xsl:if>

	</xsl:template>

  <xsl:template match="dir:directory | directory" mode="dirs-selects-option">
    <xsl:param name="n" select="0" />
    <xsl:param name="p" />

    <xsl:param name="parents">
          <xsl:choose>
            <xsl:when test="$p!=''">
              <xsl:value-of select="concat( $p, '/', @name )" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@name" />
            </xsl:otherwise>
          </xsl:choose>
    </xsl:param>

    <option class="pl-ead-publish-dir" label="{@name}" value="{$parents}">
      <xsl:call-template name="indent">
        <xsl:with-param name="n" select="$n" />
      </xsl:call-template>
      <xsl:value-of select="@name" />
    </option>
    <xsl:apply-templates select="dir:directory|directory" mode="dirs-selects-option">
      <xsl:with-param name="n" select="$n + 1" />
      <xsl:with-param name="p">
        <xsl:value-of select="$parents"/>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

	<!-- Fonction d'indentation pour la liste des fichiers
				On pourrait utiliser la propriété CSS text-indent, mais elle ne
				fonctionne pas du tout sur IE 6. -->
	<xsl:template name="indent">
		<xsl:param name="n" select="1" />
		<xsl:param name="c" select="0" />
		<xsl:param name="sep" select="'- '" />

		<xsl:if test="$c &lt; $n">
			<xsl:value-of select="$sep" />
			<xsl:call-template name="indent">
				<xsl:with-param name="c" select="$c + 1" />
				<xsl:with-param name="n" select="$n" />
			</xsl:call-template>
		</xsl:if>

	</xsl:template>

	<!--+
	    | Construction des sections pour un fichier
	    +-->
	<xsl:template match="dir:file | file" mode="content">

		<xsl:param name="pos" select="number(0)" />

		<xsl:variable name="n" select="@name" />

		<!--<xsl:variable name="l" select="substring-before($n, $ext)" />-->
		<xsl:variable name="l" select="pleade:get-url-file-wo-extension($n)" />

		<xsl:variable name="p">
			<xsl:choose>
				<xsl:when test="@absolutePath!=''">
					<xsl:value-of select="@absolutePath" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$n" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

<!--<xsl:message>
n = <xsl:value-of select="$n" />
l = <xsl:value-of select="$l" />
p = <xsl:value-of select="$p" />
</xsl:message>-->

		<!-- Le fichier backup correspondant -->
		<xsl:variable name="bckp" select="$backups[@name=concat($n, '.backup')]" />

		<!--<xsl:if test="$cp_msg and ends-with($cp_cur, $p)">-->
		<xsl:if test="$cp_msg and ends-with($p, $cp_cur)">
			<fieldset>
				<legend>
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.result.legend">
						<xsl:text>message d'information</xsl:text>
					</i18n:text>
				</legend>
				<div class="pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.msg.{$cp_msg}">
						<xsl:value-of select="$cp_msg" />
					</i18n:text>
					<xsl:if test="$cp_msg = 'not-valid-file'">
						<xsl:variable name="validation-path" select="concat('cocoon:/ead-admin/isvalid.xml?filename=', $cp_new, '&amp;schema=', $cp_schema)"/>
						<xsl:if test="document($validation-path)">
							<xsl:call-template name="validation-messages">
								<xsl:with-param name="report" select="document($validation-path)//*[local-name() = 'error']"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:if>
				</div>
			</fieldset>
		</xsl:if>

		<div>
			<xsl:comment>u</xsl:comment>
			<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.{$l}.intro" />
		</div>
    <xsl:call-template name="build-help">
      <xsl:with-param name="v" select="concat($l, '.info')" />
      <xsl:with-param name="k" select="concat($i18n-root, '.info')" />
    </xsl:call-template>

		<fieldset>
			<legend>
				<xsl:comment>u</xsl:comment>
				<i18n:text catalogue="module-eadeac-edit" key="ead-admin.info.legend">
					<xsl:text>information sur le fichier actuel</xsl:text>
				</i18n:text>
			</legend>
			<p>
				<i18n:text catalogue="module-eadeac-edit" key="ead-admin.current.date-text">
					<xsl:text>date du fichier&#160;: </xsl:text>
				</i18n:text>
				<!-- TODO: formater la date avec @date (ISO 8601) ou @lastModified (millis)  -->
				<xsl:value-of select="@date" />
			</p>
			<xsl:if test="contains($actions, 'r')">
        <div class="pl-form-bttn-container">
          <button type="button" id="get{$pos}"
                        title="module-eadeac-edit:ead-admin.get-file.title" i18n:attr="title"
                        class="yui-button yui-push-button pl-form-bttn"
                        accesskey="get"
                        onclick="javascript: window.location.href='../download/{concat( netutils:encodeUrl(replace($curdir,'\\','/'), 'UTF-8'), if(not(ends-with($curdir,'/'))) then '/' else '')}{$n}';">
                        <!--onclick="javascript: window.location.href='../download/{$curdir}/{/dir:directory[@requested='true']/@name}/{$n}';">-->
            <i18n:text catalogue="module-eadeac-edit" key="ead-admin.get-file.text" >
              <xsl:text>récupérer le fichier</xsl:text>
            </i18n:text>
          </button>
        </div>
			</xsl:if>
			<xsl:text>&#160;</xsl:text>
			<!-- <xsl:if test="contains($actions, 's') and not($n='ead-default.xconf')"> -->
			<xsl:if test="contains($actions, 's')">
        <div class="pl-form-bttn-container">
          <button type="button" id="delete{$pos}"
                        title="module-eadeac-edit:ead-admin.delete-file.title" i18n:attr="title"
                        class="yui-button yui-push-button pl-form-bttn"
                        accesskey="d"
          >
            <xsl:attribute name="onclick">
              <xsl:text>javacript:if(confirm(_usersMessages.ead_admin_delete_file_confirm_text+'\'</xsl:text><xsl:value-of select="$n" /><xsl:text>\'')){ var ret= adminManager.form2work(this.form, 'deletefile', '</xsl:text><xsl:value-of select="$n" /><xsl:text>', </xsl:text><xsl:value-of select="$pos" /><xsl:text>); if(ret==true) this.form.submit();}</xsl:text>
            </xsl:attribute>
            <i18n:text catalogue="module-eadeac-edit" key="ead-admin.delete-file.text" >
              <xsl:text>supprimer le fichier</xsl:text>
            </i18n:text>
          </button>
        </div>
			</xsl:if>
		</fieldset>

		<xsl:if test="contains($actions, 'c')">
      <xsl:call-template name="build-help">
        <xsl:with-param name="v" select="$l" />
        <xsl:with-param name="k" select="concat($i18n-root, '.newfile')" />
      </xsl:call-template>
			<fieldset>
				<legend>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.new-file.legend">
						<xsl:text>envoyer un nouveau fichier</xsl:text>
					</i18n:text>
				</legend>
				<p>
					<input id="newfile{$n}" class="pl-form-npt-file newfile" size="40" type="file"/>
				</p>
        <div class="pl-form-bttn-container">
          <button type="button" id="replace{$pos}"
                        title="module-eadeac-edit:ead-admin.new-file.submit.title" i18n:attr="title"
                        class="yui-button yui-push-button pl-form-search-submit"
                        accesskey="r"
                        onclick="javascript:var ret=adminManager.form2work(this.form, 'newfile', '{$n}', {$pos}); if(ret==true) this.form.submit();">
            <i18n:text catalogue="module-eadeac-edit" key="ead-admin.new-file.submit.text" >
              <xsl:text>valider l'envoi...</xsl:text>
            </i18n:text>
          </button>
        </div>
			</fieldset>
		</xsl:if>

		<xsl:if test="contains($actions, 'b')">
      <xsl:call-template name="build-help">
        <xsl:with-param name="v" select="concat($l,'.backup')" />
        <xsl:with-param name="k" select="concat($i18n-root, '.backup')" />
      </xsl:call-template>
			<fieldset>

				<legend>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.backup.legend">
						<xsl:text>sauvegarde</xsl:text>
					</i18n:text>
				</legend>

				<xsl:choose>
					<!-- On a un backup -->
					<xsl:when test="$bckp">
						<p>
							<i18n:text catalogue="module-eadeac-edit" key="ead-admin.backup.date-text">
								<xsl:text>date de la dernière sauvegarde&#160;: </xsl:text>
							</i18n:text>
							<!-- TODO: formater la date avec @date (ISO 8601) ou @lastModified (millis)  -->
							<xsl:value-of select="$bckp/@date" />
							<!--<xsl:apply-templates select="$bckp/@date" />-->
							<!--<xsl:value-of select="du:format(du:parse($bckp/@date))"
														xmlns:du="java:org.pleade.utils.DateUtilities"
														xmlns:dfu="org.apache.commons.lang.time.DateFormatUtils"/>-->
						</p>
            <div class="pl-form-bttn-container">
              <button type="button" id="back{$pos}"
                            title="module-eadeac-edit:ead-admin.backup.submit.title" i18n:attr="title"
                            class="yui-button yui-push-button pl-form-search-submit"
                            accesskey="b"
                            onclick="javascript:var ret=adminManager.form2work(this.form, 'backup', '{$n}', {$pos}); if(ret==true) this.form.submit();">
                <i18n:text catalogue="module-eadeac-edit" key="ead-admin.backup.submit.text" >
                  <xsl:text>revenir à la sauvegarde...</xsl:text>
                </i18n:text>
              </button>
            </div>
					</xsl:when>

					<!-- Pas de backup -->
					<xsl:otherwise>
						<p>
							<i18n:text catalogue="module-eadeac-edit" key="ead-admin.backup.no-backup">
								<xsl:text>il n'existe aucune copie de sauvegarde.
								&#x9;
								si vous pensez que c'est une erreur, contactez l'administrateur système.</xsl:text>
							</i18n:text>
						</p>
					</xsl:otherwise>

				</xsl:choose>

			</fieldset>
		</xsl:if>

	</xsl:template>

	<xsl:template name="validation-messages">
		<xsl:param name="report"/>
		<ul>
		<xsl:for-each select="$report">
			<li>
				<i18n:translate>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.invalid.document.error">line {0}, column {1}: {2}</i18n:text>
					<i18n:param><xsl:value-of select="@line"/></i18n:param>
					<i18n:param><xsl:value-of select="@column"/></i18n:param>
					<i18n:param><xsl:value-of select="."/></i18n:param>
				</i18n:translate>
			</li>
		</xsl:for-each>
		</ul>
	</xsl:template>

	<!--+
	    | Ajouter un fichier
	    +-->
	<xsl:template name="addfile-content">

		<xsl:param name="pos" />
		<xsl:param name="subdir" select="''" />

		<xsl:if test="$cp_msg and ends-with($cp_cur, 'addfile')">
			<fieldset>
				<legend>
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.result.legend">
						<xsl:text>message d'information</xsl:text>
					</i18n:text>
				</legend>
				<div class="pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.msg.{$cp_msg}">
						<xsl:value-of select="$cp_msg" />
					</i18n:text>
				</div>
			</fieldset>
		</xsl:if>

    <xsl:call-template name="build-help">
      <xsl:with-param name="v" select="'addfile'" />
      <xsl:with-param name="k" select="concat($i18n-root, '.addfile')" />
    </xsl:call-template>
		<fieldset>
			<legend>
				<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.addfile.legend">
					<xsl:text>ajouter un fichier</xsl:text>
				</i18n:text>
				<xsl:if test="$subdir='true'">
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.pages.addfile.subdir.selector.legend">&#32;dans le dossier&#160;:</i18n:text>
					<select id="dir-select" class="pl-form-slct">
						<option></option>
						<xsl:apply-templates select="$dir/dir:directory | $dir/directory" mode="dirs-selects-option" />
					</select>
				</xsl:if>
			</legend>
			<div>
				<xsl:comment>u</xsl:comment>
				<i18n:text catalogue="module-eadeac-edit" key="{$i18n-root}.addfile.intro" />
			</div>
			<p>
				<input id="newfileaddfile" class="pl-form-npt-file addfile" size="40" type="file"/>
			</p>
      <div class="pl-form-bttn-container">
        <button type="button" id="addf{$pos}"
                      title="module-eadeac-edit:ead-admin.new-file.submit.title" i18n:attr="title"
                      class="yui-button yui-push-button pl-form-search-submit"
                      accesskey="a"
                      onclick="var ret=adminManager.form2work(this.form, 'addfile', 'addfile', 'last'); if(ret==true) this.form.submit();">
          <i18n:text catalogue="module-eadeac-edit" key="ead-admin.new-file.submit.text" >
            <xsl:text>valider l'envoi...</xsl:text>
          </i18n:text>
        </button>
      </div>
		</fieldset>
	</xsl:template>

	<!--+
	    | Redémarrer l'application
	    +-->
	<xsl:template name="restart-content">

		<xsl:param name="pos" />

		<xsl:if test="$cp_msg and ends-with($cp_cur, 'restart')">
			<fieldset>
				<legend>
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.result.legend">
						<xsl:text>message d'information</xsl:text>
					</i18n:text>
				</legend>
				<div class="pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="ead-admin.msg.{$cp_msg}">
						<xsl:value-of select="$cp_msg" />
					</i18n:text>
				</div>
			</fieldset>
		</xsl:if>

    <xsl:call-template name="build-help">
      <xsl:with-param name="v" select="'restart'" />
      <xsl:with-param name="k" select="concat($i18n-root, '.restart')" />
    </xsl:call-template>
		<fieldset>
			<legend>
				<i18n:text catalogue="module-eadeac-edit" key="ead-admin.restart.legend">
					<xsl:text>redémarrer l'application</xsl:text>
				</i18n:text>
			</legend>
			<div>
				<xsl:comment>u</xsl:comment>
				<i18n:text catalogue="module-eadeac-edit" key="ead-admin.restart.intro" />
			</div>
      <div class="pl-form-bttn-container">
        <button type="button" id="restart{$pos}"
                      title="module-eadeac-edit:ead-admin.restart.submit.title" i18n:attr="title"
                      class="yui-button yui-push-button pl-form-search-submit"
                      accesskey="d"
                      onclick="var ret=adminManager.form2work(this.form, 'restart', 'restart', 'last'); if(ret==true) this.form.submit();">
          <i18n:text catalogue="module-eadeac-edit" key="ead-admin.restart.submit.text" >
            <xsl:text>redémarrer</xsl:text>
          </i18n:text>
        </button>
      </div>
		</fieldset>
	</xsl:template>

	<!--+
	    | Un tableau Javascript contenant la liste des fichiers *.backup et leur
			|	date
	    +-->
	<xsl:template match="dir:directory | directory" mode="backups-array-list">

		<xsl:for-each select="descendant::dir:file[ends-with(@name, '.backup')] | descendant::file[ends-with(@name, '.backup')]">
			<xsl:if test="position()!=1">,</xsl:if>['<xsl:for-each select="ancestor::dir:directory[not(@requested='true')]|ancestor::dir[not(@requested='true')]"><xsl:if test="position()!=1">/</xsl:if><xsl:value-of select="@name" /><xsl:if test="position()=last()">/</xsl:if></xsl:for-each><xsl:value-of select="@name" />','<xsl:value-of select="@date" />']
		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>
