rem TODO-MAINTAIN: The jar names in the classpath below should be updated as jars change

rem this is an example url for connecting to the internal hsql database
rem use the url below or one similar when propmted by the hsql interface
rem the user name is "sa" and there is no password
rem url:
rem jdbc:hsqldb:../../sdxtest/conf/databases/_hsql/

rem Database Manager is an AWT Tool for manageing a JDBC database.
rem Usage: java DatabaseManager [-options]
rem where options include:
rem -driver   jdbc driver class
rem -url           jdbc url
rem -user          username used for connection
rem -password  password for this user
rem -dir           default directory
rem -script        reads from script file

java -cp ../lib/hsqldb-1.7.1.jar org.hsqldb.util.DatabaseManager -url jdbc:hsqldb:../../sdxtest/conf/databases/_hsql/