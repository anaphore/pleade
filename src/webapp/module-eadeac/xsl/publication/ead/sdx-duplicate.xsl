<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="xs fn xsl saxon"
>

	<!-- Un paramètre de publication permettant de désactiver la duplication des champs pour un IR donné par exemple.-->
	<xsl:param name="duplicated-fields" select="'_all'" />
	
	<!-- Un paramètre de publication donnant la liste des éléments EAD qui ne doivent pas être pris en compte lors de la duplication des champs d'indexation. -->
	<xsl:param name="duplicated-field-excluded-elements" select="'ead eadheader div archdesc'"/>
	
	<xsl:param name="debug" select="'false'"/>
	
	<!-- La liste des éléments EAD exclus de la duplication sous la forme d'une liste -->
	<xsl:variable name="excluded-elements-list" select="tokenize($duplicated-field-excluded-elements,' ')"/>
	
	<!-- XSLT qui permet de créer de nouveaux champs par héritage pour doublonner les champs
			destinés à la recherche multi-critères -->

	<!-- Un pointeur sur le fichier de propriétés pour les champs à dupliquer -->
	<xsl:variable name="url" select="'cocoon://module-eadeac/conf/ead-duplicated-fields.xconf'" />
	<xsl:variable name="duplicated-fields-list" select="
		if( $duplicated-fields = '_none' ) then ()
		else if (doc-available($url)) then document($url)/duplicated-fields
		else ()
	" />

	<xsl:template match="/">
		<xsl:message>	Traitement de la duplication des champs (etape 14 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{sdx:document/@id}/{sdx:document/@id}-14.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-onde="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:message>Fin de l'analyse : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
	</xsl:template>

	<!-- Traitement d'un document SDX -->
	<xsl:template match="sdx:document">
		<!-- On le copie -->
		<xsl:copy>
			<!-- On copie les attributs -->
			<xsl:apply-templates select="@*"/>
			<!-- Un pointeur sur le document en cours de traitement -->
			<xsl:variable name="doc" select="."/>
			<!-- On boucle sur les champs à dupliquer -->
			<xsl:for-each select="$duplicated-fields-list/duplicated-field[tokenize($duplicated-fields,',') = @original or $duplicated-fields = '_all']">
				<!-- Le nom champ à dupliquer -->
				<xsl:variable name="orig" select="@original"/>
				<!-- Le nom du nouveau champ à sortir -->
				<xsl:variable name="new" select="@duplicate"/>
				<!-- On traite tous les champs du document courant ou celui de ses ancêtres sans tenir des éléments de haut niveau (ead, eadheadder, frontmatter (ie, div) et archdesc -->
				<xsl:apply-templates select="$doc/ancestor-or-self::sdx:document[not( $excluded-elements-list = pleade:subdoc/c/@pleade:original-element)]/sdx:field[@name = $orig and .!='']" mode="duplicate">
					<xsl:with-param name="new" select="$new"/>
				</xsl:apply-templates>
			</xsl:for-each>
			<!-- On traite maintenant les sous-éléments -->
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- Traitement d'un champ pour la duplication -->
	<xsl:template match="sdx:field" mode="duplicate">
		<!-- Le nom du nouveau champ -->
		<xsl:param name="new" select="''"/>
		<!-- On sort un champ avec le même contenu -->
		<sdx:field name="{$new}">
			<xsl:choose>
				<xsl:when test="@name='fulltext' or @name='extfulltext'">
					<xsl:value-of select="normalize-space(.)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of saxon:read-once="yes" select="node()"/>
				</xsl:otherwise>
			</xsl:choose>
		</sdx:field>
	</xsl:template>

	<!-- De manière générale, on copie tout -->
	<xsl:template match="sdx:field">
		<xsl:if test=".!=''">
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
				<xsl:choose>
					<xsl:when test="@name='fulltext' or @name='extfulltext'">
						<xsl:value-of select="normalize-space(.)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of saxon:read-once="yes" select="node()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
