<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: skip-protected-content.xsl 12373 2008-10-15 14:41:34Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT pour supprimer les contenus protégés avant leur affichage
		La suppression est conditionnelle à :
		 - un rôle de l'utilisateur : admin ou eadeac-editor 
		FIXME : Pourrait l'être à une origine (internet vs. intranet)
		
		Utilisé dans 2 contextes :
		1) Fragment EAD
			/sdx:document
		2) Fragment de TOC
			/root/ead
			/root/properties
			
		On aura toujours le fragment EAD dans un élément "fragment".
		On aura toujours
		/root/fragment/* : contenant l'EAD ou la TOC
		/root/userinfos

	  Dans le contexte de la TOC, on a en plus :
		/root/properties
		
		On ne recopie jamais /root/userinfos et /root/fragment uniquement le contenenu de ce dernier.
-->
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xsl fn"
	version="2.0">
	
	<!-- Les rôles de l'utilisateur courant -->
	<!-- <xsl:variable name="userinfos" select="if(doc-available('cocoon://auth/userinfos.xml')) then document('cocoon://auth/userinfos.xml')/userinfos else ''"/> -->
	<xsl:variable name="userinfos" select="/root/userinfos"/>
	<xsl:variable name="roles" select="if($userinfos!='' and $userinfos/*) then replace($userinfos/authentication/roles,'\s','') else ''" />
	<!-- Rôles autorisés à voir les infos protégées -->
	<xsl:variable name="roles-auth" select="'admin,eadeac-editor'"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="@*|node()" />
	</xsl:template>
	
	<!-- On ne recopie pas userinfos, ni son contenu -->
	<xsl:template match="userinfos"/>
	
	<xsl:template match="root">
		<xsl:choose>
			<xsl:when test="properties">
				<xsl:copy>
					<xsl:apply-templates/>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- On ne recopie pas les éléments, seulement leur contenu -->
	<xsl:template match="fragment">
		<xsl:apply-templates/>
	</xsl:template>
	
	<!-- On ne recopie pas les contenus protégés par sauf si utilisateur a le droit de les voir.
			Ce traitement s'applique dans l'EAD, dans les liens vers les subdoc, dans la toc.
			Les contenus protégés sont ceux qui ont :
				 * @audience='internal'
	-->
	<xsl:template match="node()[@audience='internal']">
		<!-- Si l'utilisateur a un rôle autorisé, on conserve les éléments -->
		<xsl:if test="fn:tokenize($roles, ',') = fn:tokenize($roles-auth, ',')">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- On copie tout -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
