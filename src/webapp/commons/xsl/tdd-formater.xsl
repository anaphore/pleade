<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Une XSLT utilisée pour formater un fichier Texte délimité déterministe
    |
    | On travaille un XML :
    | <root>
    |  [plein texte]
    | </root>
    |
    | @param separator-record     Séparateur de notice, défaut : "//"
    | @param separator-field      Séparateur de champs, défaut : "|"
    | @param first-record-headers Indique si oui ou non (true|false) le premier
    |                             enregistrement représente les noms de champs, défaut : "true"
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="separator-record" />
  <xsl:param name="separator-field" />
  <xsl:param name="first-record-headers" />

  <xsl:variable name="_rec">
    <xsl:choose>
      <xsl:when test="$separator-record!=''">
        <xsl:value-of select="$separator-record" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>//</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="esc_rec">
    <xsl:analyze-string select="$_rec" regex="\||\[|\]|\$|\.|\^|\\">
      <xsl:matching-substring>
        <xsl:value-of select="concat('\',$_rec)" />
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:value-of select="$_rec" />
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:variable>

  <xsl:variable name="_fld">
    <xsl:choose>
      <xsl:when test="$separator-field!=''">
        <xsl:value-of select="$separator-field" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>|</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="esc_fld">
    <xsl:analyze-string select="$_fld" regex="\||\[|\]|\$|\.|\^|\\">
      <xsl:matching-substring>
        <xsl:value-of select="concat('\',$_fld)" />
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:value-of select="$_fld" />
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:variable>

  <xsl:variable name="_1">
    <xsl:choose>
      <xsl:when test="$first-record-headers!=''">
        <xsl:value-of select="$first-record-headers" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>true</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!--+
      | Formatage
      +-->
  <xsl:template match="/*">

    <ap-formater-report>

      <separator-record>
        <xsl:value-of select="$_rec" />
      </separator-record>
      <separator-field>
        <xsl:value-of select="$_fld" />
      </separator-field>
      <first-record-headers>
        <xsl:value-of select="$_1" />
      </first-record-headers>

      <xsl:for-each select="tokenize(., $esc_rec)">

        <xsl:variable name="el">
          <xsl:choose>
            <xsl:when test="position()!=1">
              <xsl:text>record</xsl:text>
            </xsl:when>
            <xsl:when test="$_1='true'">
              <xsl:text>header</xsl:text>
            </xsl:when>
          </xsl:choose>
        </xsl:variable>

        <xsl:element name="{$el}">
          <xsl:attribute name="no">
            <xsl:value-of select="position()" />
          </xsl:attribute>
          <xsl:for-each select="tokenize(., $esc_fld, 'm')">
            <column no="{position()}">
              <xsl:value-of select="." />
            </column>
          </xsl:for-each>
        </xsl:element>

      </xsl:for-each>

    </ap-formater-report>
  </xsl:template>

</xsl:stylesheet>
