//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer facilement un Yui Panel
 * @author Julien Huet
 */
/**
 * @class Classe permettant de créer facilement un Yui Panel
 * @author Julien Huet
 */
var YuiPanel = new Class(/** @lends YuiPanel.prototype */{
	/**
	* Le panel YUI
	* @private
	* @type YAHOO.widget.Panel
	*/
	panel : null,

	/**
	* <strong>Constructor</strong><br/> Constructeur du panel YuiPanel
	* @param {String}  id     Id du bouton sur lequel on souhaite placer le Panel
	* @param {Integer} width  Lageur du panneau
	* @param {Integer} height Hauteur du panneau
	*/
	initialize: function(id,width,height){
		var idButton = this.getRealId(id);
		this.panel = new YAHOO.widget.Panel(id, {
			underlay: "none",
			width: width,
			height: height,
			visible:false,
			draggable:true,
			constraintoviewport:false/*,
			context:[idButton, "tr", "bl"]*/
		});
		/** [JC] FIXME: le 'context' ici pose problème dans les boutons d'aide du formulaiure de publication, à cause je pense de l'initialisation des panneaux alors que les boutons sont planqués façon YUI. Il faudrait probablement utiliser visibility:hidden au lieu de display:none */
	},
	
	/**
	* Fonction permettant de contruire le véritable ID de l'élement sur lequel on va créer le panel YUI
	* @param {Object} id
	*/
	getRealId: function(id){
		return id + '-button';
	},

	/**
	* Pour savoir si le panel est visible ou non
	* @public
	*/
	isVisible: function(){
		return this.panel.cfg.getProperty('visible');
	},

	/**
	* Masque le panneau
	* @public
	*/
	hide: function(){
		this.panel.hide();
		//$(this.panel.element.id).setStyle('display', 'none');
	},

	/**
	* Affiche le panneau
	* @public
	*/
	show: function(){
		this.panel.render();
		this.panel.show();
		//$(this.panel.element.id).setStyle('display', 'block');
	},

	/**
	* @public
	*/
	render: function(){
		this.panel.render();
	}
});
