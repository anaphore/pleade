#! /bin/sh

# Fichier de commande qui permet de lancer Pleade dans Jetty et ensuite de
# lancer le navigateur Web sur la page par defaut.

# L'executable Java, on le suppose dans le PATH, sinon indiquer le chemin complet
export JAVA_CMD=java

# Le port d'ecoute de Jetty
export PORT=8090

# On se déplace dans le sous-dossier Jetty pour que les chemins
# relatifs soient OK.
cd jetty

# On demarre Jetty
$JAVA_CMD -Xmx500m -Djetty.port=$PORT -jar start.jar ./etc/jetty.xml

# On revient au repertoire precedent
cd ..
