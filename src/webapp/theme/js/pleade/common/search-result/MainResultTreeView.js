//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer l'arbre du CDC pour le résultat de recherche 
 * @author Julien Huet
 */
/**
 * @class Classe permettant de créer l'arbre du CDC pour le résultat de recherche 
 * @author Julien Huet
 * @augments TreeView
 * @augments AjaxRequestHandler
 */
var MainResultTreeView = new Class(/** @lends MainResultTreeView */{
	Extends: TreeView,
	Implements: [AjaxRequestHandler],
	
	/**
	* <strong>Constructor</strong><br/>  Initialisation de l'arbre
	* @param {String} id Id de l'element où l'on va insérer le treeView
	*/
	initialize: function(id){
		// recuperation des variable globale	
		this.qidcdcall = qidcdcall;
		this.qidcdc = qidcdc;
		this.qidrootid = qidrootid;
		this.oid = oid;
		this.ae = ae;
		
		var url = 'functions/ead/cdc-results.ajax-html?qcdcall=' + qidcdcall + '&qcdc=' + qidcdc + '&qrootid=' + qidrootid + '&oid=' + oid + '&_ae=' + ae;
		var urlStart = 'functions/ead/cdc-results.ajax-html?';
		var urlEnd = 'qcdcall=' + qidcdcall + '&qcdc=' + qidcdc + '&qrootid=' + qidrootid + '&oid=' + oid + '&_ae=' + ae;
		this.parent(url, $(id),urlStart,urlEnd);
		// surcharge de la methode getChildrenUrl afin de recuperer la bonne URL
		this.getTree().getChildrenUrl = function(node){
			if ( node.data.id ) {
				return this.urlStart + 'id=' + node.data.id + '&' + this.urlEnd;
			} else {
				return null;
			}
		}
		if ( this.ae == '_all' ) {
			//on active l'auto expand
			this.enableExpandAllOnLoad();
		} else {
			this.enableExpandAllOnLoad(this.ae.toInt());
		}
	},

	/**
	* Surcharge de la fonction OnSelect.
	* Action appelée lors de la selection d'un noeud
	* @private
	* @param {Mif.Tree.Node} node
	*/
	onSelect: function(node){
		var wrapper=node.getDOM('wrapper');
		wrapper.removeClass('mif-tree-node-selected');
	},

	/**
	* Affiche les résultats pour un document.
	* @public
	* @param {String} tdId L'identifiant du td où insérer le div
	* @param {String} nodeid	L'identifiant du noeud, pour avoir un id unique
	* @param {String} url L'URL pour appeler les résultats en AJAX
	*/
	displayDocumentResults: function(nodeid, tdId, url) {
		// L'identifiant du div où l'on a les résultats
		var divid = 'results-container-' + nodeid;
		// Il est possible que l'on ait déjà affiché les résultats
		var div = $(divid);
		if ( div ) {
			// On a les résultats, alors on l'affiche ou le ferme
			div.toggle();
		} else {
			// On n'a pas les résultats, on doit construire le div et le remplir
			var d = document.createElement('div');
			d.setAttribute('id', divid);
			var td = $(tdId);
			td.appendChild(d);
			// On va chercher les informations et on met à jour le conteneur
			this.loadAjaxContent(url,d,null,null,'message');
		}
		return false;
	}
});