<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="1.0"
	xmlns:xsp="http://apache.org/xsp"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsp-pleade="http://pleade.org/ns/xsp-pleade/1.0"
	xmlns:c-pleade="http://pleade.org/ns/c-pleade/1.0"
	xmlns:xsp-nav="http://ajlsm.com/sdx/xsp-navigation"
	xmlns:ui-strings="http://pleade.org/ns/ui-strings"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
>

	<!--About the page content-->
	<xsl:template match="xsp-pleade:page">
		<!-- Page type -->
		<xsl:variable name="type" select="@type"/>

		<!--Java variables-->
		<xsp:logic>
			<xsl:choose>
				<xsl:when test="$type='static'">String pleade_id_page = parameters.getParameter("<xsl:value-of select="@idParam"/>");</xsl:when>
				<xsl:otherwise>String pleade_id_page = "<xsl:value-of select="@id"/>";</xsl:otherwise>
			</xsl:choose>
			String pleade_uri_page = request.getRequestURI();
			int pleade_uri_nbparts = (new StringTokenizer(pleade_uri_page, "/")).countTokens() - 3;
			String pleade_url_index = ".";
			for (int pleade_uri_part=0; pleade_uri_part &lt; pleade_uri_nbparts; pleade_uri_part++) pleade_url_index += "/..";
			String pleade_lang = sdx_lang.substring(0, 2);
			String pleade_current_lang = "";
		</xsp:logic>

		<!--Page configuration-->
		<xsl:call-template name="page-configuration-output"/>

		<c-pleade:page>
			<xsp-nav:declare-variables/>
			<xsl:call-template name="general-navigation"/>
			<xsl:choose>
				<xsl:when test="$type='static'">
					<xsl:call-template name="xsp-pleade:static-content"/>
				</xsl:when>
				<xsl:otherwise>
					<c-pleade:page-header/>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Admin bar -->
			<!-- TODO: contextualize -->
			<sdx:userIsMember group="admins editeurs editors">
				<c-pleade:administration>
					<c-pleade:admin-tool code="add" link="upload-form.xsp" place="left"/>
					<c-pleade:admin-tool code="delete" link="delete-all.xsp" place="left"/>
					<c-pleade:admin-tool code="config-forms" link="config-forms.xsp" place="left"/>
					<c-pleade:admin-tool code="config-subsets" link="config-subsets.xsp" place="left"/>
					<c-pleade:admin-tool code="see-top" link="search-s.xsp?q=top:1&amp;base=ead" place="center"/>
					<c-pleade:admin-tool code="see-img" link="search-s.xsp?q=object:1&amp;base=ead" place="center"/>
					<c-pleade:admin-tool code="see-all" link="search-s.xsp?q=sdxall:1&amp;base=ead" place="center"/>
					<c-pleade:admin-tool code="login" link="login.xsp" place="right"/>
					<c-pleade:admin-tool code="logout" link="index.shtm?logout=true" place="right"/>
				</c-pleade:administration>
			</sdx:userIsMember>
			<!-- Output user information if authenticated -->
			<sdx:userIsMember group="admins editeurs editors">
				<c-pleade:user-information/>
			</sdx:userIsMember>
			<!-- debug information if isAdmin -->
			<sdx:userIsAdmin>
				<c-pleade:debug-info/>
			</sdx:userIsAdmin>
		</c-pleade:page>


	</xsl:template>

	<!-- Page configuration output -->
	<xsl:template name="page-configuration-output">
		<c-pleade:config>
			<c-pleade:param name="url-index"><xsp:expr>pleade_url_index</xsp:expr></c-pleade:param>
			<!-- Global UI messages -->
			<c-pleade:ui-strings>
				<xsl:call-template name="include-ui-strings">
					<xsl:with-param name="url" select="concat('../../../pleade-local/skin/ui-strings/', 'common.xml')"/>
					<xsl:with-param name="group" select="''"/>
				</xsl:call-template>
			</c-pleade:ui-strings>
			<xsl:if test="@id-ui-strings">
				<c-pleade:my-ui-strings>
					<xsl:call-template name="include-ui-strings">
						<xsl:with-param name="url" select="concat('../../../pleade-local/skin/ui-strings/', @id-ui-strings, '.xml')"/>
						<xsl:with-param name="group" select="@group-ui-strings"/>
					</xsl:call-template>
				</c-pleade:my-ui-strings>
			</xsl:if>
		</c-pleade:config>
	</xsl:template>

	<!-- Includes specific messages -->
	<xsl:template name="include-ui-strings">
		<xsl:param name="url" select="''"/>
		<xsl:param name="group" select="''"/>
		<xsl:choose>
			<xsl:when test="$group != ''">
				<xsl:apply-templates select="document($url)/*/ui-strings:group[@id=$group]/*" mode="ui"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="document($url)/*/ui-strings:group/*" mode="ui"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*" mode="ui">
		<!-- We add the element only if language matches or no language defined or no language filter -->
		<xsp:logic>
			pleade_current_lang = "<xsl:value-of select="ancestor-or-self::*[@xml:lang][1]/@xml:lang"/>";
			if ( pleade_lang.equals("") || pleade_current_lang.equals("") || pleade_lang.equals(pleade_current_lang) ) {
				<xsl:copy>
					<xsl:apply-templates select="node()|@*" mode="ui"/>
				</xsl:copy>
			}
		</xsp:logic>
	</xsl:template>

	<xsl:template match="text()|@*" mode="ui">
		<xsl:copy><xsl:apply-templates select="node()|@*" mode="ui"/></xsl:copy>
	</xsl:template>

	<!-- Java variables for navigation -->
	<xsl:template name="declaration-variables-navigation">
	</xsl:template>

	<!-- General menu-->
	<xsl:template name="general-navigation">
		<xsp-nav:include-menu id="general" file="../../../pleade-local/skin/menubars/main.xml" current-var="pleade_id_page"/>
	</xsl:template>

	<!-- static page content  -->
	<xsl:template name="xsp-pleade:static-content">
		<!-- XHTML static doc inclusion -->
		<ci:include xmlns:ci="http://apache.org/cocoon/include/1.0">
			<xsp:attribute name="src">../../pleade-local/skin/static/<xsp:expr>pleade_lang</xsp:expr>/<xsp:expr>pleade_id_page</xsp:expr>.xhtml</xsp:attribute>
		</ci:include>

	</xsl:template>

	<!-- An error -->
	<xsl:template match="xsp-pleade:error">
		<c-pleade:error code="{@code}"/>
	</xsl:template>

	<!-- keep the other tags-->
	<xsl:template match="@*|*|text()|processing-instruction()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|*|text()|processing-instruction()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
