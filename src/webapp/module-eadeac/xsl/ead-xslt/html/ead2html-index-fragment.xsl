<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		exclude-result-prefixes="xsl i18n pleade sdx">

	<!-- Extrait une partie d'un index, correspondant à un groupe.
			Le XML en entrée est un fichier d'index. -->
	
	<!-- L'identifiant du groupe à demander -->
	<xsl:param name="id" select="''"/>

	<!-- On intercepte la racine et on trouve le group à sortir -->
	<xsl:template match="/*">
		<xsl:variable name="ini" select="urld:decode(string($id), 'UTF-8')" xmlns:urld="java:java.net.URLDecoder"/>
		<xsl:apply-templates select=".//group[@id=$ini][1]"/>
	</xsl:template>
	
	<xsl:template match="group">
		<xsl:copy>
			<xsl:attribute name="array">true</xsl:attribute>
			<!-- On copie les attributs de la racine pour avoir le nom des champs -->
			<xsl:apply-templates select="/*/@*"/>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
