/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.parameters.ParameterException;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.transformation.Transformer;
import org.apache.excalibur.source.Source;
import org.pleade.utils.PleadeUtilities;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import fr.gouv.culture.sdx.pipeline.AbstractTransformation;

/**
 * Stocke les proprietes du document dans des fichiers. Le traitement
 * est entierement pilote par un fichier de configuration qui liste les
 * proprietes, leur nature (qui donne le fichier de destination) et leur
 * valeur par defaut.
 */
public class PropertiesWriter extends AbstractTransformation implements Transformer {

    /** Le nom du parametre qui fournit le chemin complet du fichier de configuration. */
    public final static String PARAM_CONFIGURATION_FILE = "properties-configuration-file";

    /** Le nom du parametre qui fournit le dossier de sortie */
    public final static String PARAM_OUTPUT_DIR = "data-output-dir";

    /** L'extension de fichier &aagrave; utiliser pour les jeux de propri&eacute;t&eacute;s */
    private final static String PROPERTIES_FILE_EXTENSION = ".xconf";

    /** Le dossier de sortie des fichiers */
    private File outputDirectory = null;

    /** La liste des jeux de propri&eacute;t&eacute; */
    private Vector propertySets = new Vector();

//    /** L'identifiant du document EAD */
//    private String eadid = null;

    /** L'identifiant du document*/
    private String docid = null;

    // Pour le log
    private Logger l;
    private boolean ldebug = false;

    /** Un factory pour obtenir des XSLT pour la sortie XML des documents. */
    SAXTransformerFactory stf = (SAXTransformerFactory)SAXTransformerFactory.newInstance();

    /**
     * Lecture du fichier de configuration des propri&eacute;t&eacute;s et configuration
     * des diff&eacute;rents jeux de propri&eacute;t&eacute;s.
     */
    private void readConfiguration() throws ParserConfigurationException, ParameterException, SAXException, IOException {

    		String f = this.parameters.getParameter(PARAM_CONFIGURATION_FILE);
    		f = f.replaceAll("::docid::", this.docid);
    		
    		ldebug = ( l!=null ) ? l.isDebugEnabled() : false;

    		if(ldebug){
    			l.debug("Lecture du fichier de configuration des proprietes \""+f+"\" pour le document \""+this.docid+"\"");
    		}

        // On reinitialise la liste des proprites
        this.propertySets = new Vector();

        // On parse le fichier en DOM
        /* On change le mecanisme de maniere a parser une source qui peut donc
         * etre un "file:" ou autre (eg, "cocoon:"). Ca rend dependant de
         * Excalibur mais bon... (MP) */
        // Document dom  = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(this.parameters.getParameter(PARAM_CONFIGURATION_FILE)));
        Document dom = null;
        if (this._resolver!=null) {
	        Source src = this._resolver.resolveURI( f );
	        InputStream is = null;
	        try {
	        	is = src.getInputStream();
	        	dom = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse( is );
	        } catch (Exception e) {
	        	if (l!=null && l.isErrorEnabled()) {
	        		l.error("Impossible de lire le fichier de propriétés de publication : " + f);
	        	}                  
	        } finally {
						if (is!=null) { is.close(); }
						if(src != null) { this._resolver.release( src ); }
					}
        }
        else {
        	if(ldebug){
      			l.debug("Pas de resolveur d'URI ; on lit le fichier suivant directement sur le disque si possible \""+f+"\"");
      		}
        	dom  = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File( f ));
        }

        // On parcours la liste des elements sous la racine. Chaque element definit un jeu de proprietes
        NodeList children = dom.getDocumentElement().getChildNodes();
        Node n = null; Element e = null; PropertySet props = null;
        for (int i=0; i<children.getLength(); i++) {
            n = children.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                e = (Element)n;
                // On a un nouveau jeu de proprietes, on le conserve
                props = new PropertySet(e.getAttribute("id"));
                this.propertySets.add(props);
                props.configure(e.getElementsByTagName("property"));
            }
            n = null; e = null; props = null;
        }

        f = null;
        dom = null;
    }


    /**
     * D&eacute;but du traitement du document. On r&eacute;initialise les variables globales
     * et on lit le fichier de configuration des propri&eacute;t&eacute;s.
     */
    public void startDocument() throws SAXException {
        // Les variables globales a reinitialiser
        this.outputDirectory = null;
        //[JC] 20090928: on n'apelle pus le readConfiguration ici, mais dans le startElement, pour avoir l'id du document que l'on veut
        // On passe la main sur l'evenement
        super.startDocument();
    }

    /**
     * M&eacute;thode appel&eacute;e par Cocoon lorsque cette transformation est utilis&eacute;e dans un pipeline.
     * Pour l'instant elle ne fait rien.
     */
    public void setup(SourceResolver resolver, Map map, String src, Parameters params) throws ProcessingException, SAXException, IOException {
	this.parameters = params;
	this._resolver = resolver;
    }

    /**
     * Le d&eacute;but d'un &eacute;lement. On s'int&eacute;resse uniquement &agrave; l'&eacute;l&eacute;ment racine (ead)
     * et on en profite pour conserver les param&eagrave;tres comme propri&eacute;t&eacute;s.
     */
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {

        //if (localName.equals("ead") || localName.equals("eac")) {
				if (PleadeUtilities.isRoot(localName)) {
            // L'identifiant du document
            //this.eadid = PleadeUtilities.getPleadeId(atts);
            this.docid = PleadeUtilities.getPleadeId(atts);
						// On va parser le fichier de configuration et le conserver
						// le temps de la transformation
						try {
								readConfiguration();
						}
						catch (ParserConfigurationException e) { throw new SAXException(e); }
						catch (ParameterException e) { throw new SAXException(e); }
						catch (IOException e) { throw new SAXException(e); }

            try {
                // On passe les proprietes aux differents jeux de proprietes qui les conservent ou pas
                String[] names = this.parameters.getNames();
                for (int i=0; i<names.length; i++) {
                    for (int j=0; j<propertySets.size(); j++) ((PropertySet)propertySets.get(j)).addProperty(names[i], this.parameters.getParameter(names[i]));
                }
                // On envoie les proprietes systeme issus des attributs avec namespace pleade
                for (int i=0; i<atts.getLength(); i++) {
                    if (atts.getURI(i) != null && atts.getURI(i).equals(PleadeUtilities.PLEADE_NAMESPACE_URI)) {
                        for (int j=0; j<propertySets.size(); j++) ((PropertySet)propertySets.get(j)).addProperty(atts.getLocalName(i), atts.getValue(i));
                    }
                }
                // On stocke les fichiers de proprietes
                //this.outputDirectory = new File(parameters.getParameter(PARAM_OUTPUT_DIR), eadid);
                this.outputDirectory = new File(parameters.getParameter(PARAM_OUTPUT_DIR), docid);
                PleadeUtilities.checkDir(this.outputDirectory, true);
                for (int i=0; i<propertySets.size(); i++) ((PropertySet)propertySets.get(i)).save(this.outputDirectory);
            }
            catch (ParameterException e) { throw new SAXException(e); }
            catch (TransformerConfigurationException e) { throw new SAXException(e); }
            catch (IOException e) { throw new SAXException(e); }
        }

        // Dans tous les cas, on passe la main pour le traitement
        super.startElement(namespaceURI, localName, qName, atts);
	}

    /**
     * Retourne l'identifiant du document EAD.
     * @deprecated Utiliser {@link #getDocid()}
     */
    public String getEadid() {
        //return this.eadid;
    	return this.docid;
    }
    public String getDocid() {
      return this.docid;
    }

    /**
     * Un jeu de propri&eacute;t&eacute;s, tel que sp&eacute;cifi&eacute; dans un fichier de configuration.
     */
    private class PropertySet {
        /** L'identifiant de ce jeu de propri&eacute;t&eacute;s. */
        private String id = null;
        /** La liste des propri&eacute;t&eacute;s. */
        private Properties properties = null;
				// La liste des proprietes read-only
				private Vector readonlys = null;
        /**
         * Cr&eacute;e un jeu de propri&eacute;t&eacute;s.
         * @param id        L'identifiant
         */
        public PropertySet(String id) {
            // On conserve l'identifiant
            this.id = id;
        }
        /**
         * Configuration du jeu de propri&eacute;t&eacute;s.
         * @param nl    Une liste d'&eacute;l&eacute;ments de d&eacute;finition des propri&eacute;t&eacute;s individuelles.
         */
        public void configure(NodeList nl) {
            // On initialise les proprietes a partir des valeurs par defaut
            this.properties = new Properties();
						this.readonlys = new Vector(nl.getLength());
						String readonly = "", name = "";
            for (int i=0; i<nl.getLength(); i++) {
                if ( nl.item(i).getNodeType() == Node.ELEMENT_NODE ) {
                    Element e = (Element)nl.item(i);
										 name = e.getAttribute("name");
                    this.properties.setProperty(name,e.getAttribute("default"));
									   // ajoute ou non la propriete courante a la liste des proprietes read-only
										 readonly = e.getAttribute("read-only");
										 if ( readonly!=null && readonly.equals("true")
												 && !readonlys.contains(name) ) readonlys.add(name);
                    //TODO: separateur
                }
            }
						readonlys.trimToSize();
        }
        /**
         * Retourne l'identifiant de ce jeu de propri&eacute;t&eacute;s.
         * @return  L'identifiant, tel que fourni au constructeur.
         */
        public String getId() {
            return this.id;
        }

        /**
         * Ajoute une propri&eacute;t&eacute;, mais seulement si elle doit faire partie de ce jeu de propri&eacute;t&eacute;.
         * @param name      Le nom (avec son pr&eacute;fixe &eacute;ventuel)
         * @param value     La valeur
         */
        public void addProperty(String name, String value) {
            if (name != null && !name.equals("")) {
                // On la conserve uniquement si elle fait partie de la configuration
                if ( this.properties.containsKey(name) )
											this.properties.setProperty(name, value);
            }
        }
        /**
         * Enregistre la liste de propri&eacute;t&eacute;s dans un fichier.
         * @param dir   Le dossier o&uactue; enregistrer le fichier.
         * @throws TransformerConfigurationException
         * @throws SAXException
         */
        public void save(File dir) throws TransformerConfigurationException, SAXException {
            TransformerHandler th = stf.newTransformerHandler();
            th.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
            th.setResult(new StreamResult(getOutputFile(dir)));
            AttributesImpl atts = new AttributesImpl();
            th.startDocument();
            atts.addAttribute("", "id", "id", "CDATA", this.id);
            th.startElement("", "properties", "properties", atts);
            Enumeration props = properties.propertyNames();
            while (props.hasMoreElements()) {
                String name = (String)props.nextElement();
                atts = new AttributesImpl();
                atts.addAttribute("", "name", "name", "CDATA", name);
								 if ( readonlys!=null && readonlys.contains(name) )
										atts.addAttribute("", "read-only", "read-only", "CDATA", "true");
                th.startElement("", "property", "property", atts);
                String value = properties.getProperty(name);
                th.characters(value.toCharArray(), 0, value.length());
                th.endElement("", "property", "property");
            }
            th.endElement("", "properties", "properties");
            th.endDocument();
        }
        /**
         * Retourne le fichier complet de sortie.
         * @param dir   Le dossier de sortie.
         */
        private File getOutputFile(File dir) {
            return new File(dir, this.getId() + PROPERTIES_FILE_EXTENSION);
        }
    }

}