<?xml version="1.0" encoding="UTF-8"?>
<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" version="1.0" sdx:copy="for namespace">
   <xsl:import href="skin.xsl"/>
<!-- parameter language to choose a message set -->
   <o:param name="lang-default" select="'fr'"/>

   <xsl:param name="messages-URI" select="concat('messages.', substring($lang, 1, 2))"/><!-- take set of language -->
   <o:variable name="messages" select="document($messages-URI)/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)] | document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
   <xsl:param name="id" select="/sdx:document/sdx:user/@id"/>
   <xsl:template name="title">
      <o:apply-templates select="($messages/xfm:caption[@id=&#34;title&#34;] )[1]"/>
   </xsl:template>
<!-- login-form -->

   <xsl:template match="login">
      <h2>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;server&#34;] )[1]"/>
      </h2>
      <table class="form" style="text-align:center" cellpadding="4" cellspacing="1" border="1">
         <form action="{/sdx:document/@uri}" method="post" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
            <tr>
               <td>
                  <div class="legend">
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;su&#34;] )[1]"/>
                  </div>
                  <label class="xfm_label">
                     <o:text> &#160; </o:text>
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;id&#34;] )[1]"/>
                     <span class="xfm:required" style="color:red">*</span>
                     <o:text>&#160;</o:text>
                  </label>
                  <input type="text" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_input" name="id"/>
                  <br/>
                  <label class="xfm_label">
                     <o:text> &#160; </o:text>
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;pass&#34;] )[1]"/>
                     <span class="xfm:required" style="color:red">*</span>
                     <o:text>&#160;</o:text>
                  </label>
                  <input type="password" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_secret" name="pass"/>
                  <div style="text-align:center">
                     <br/>
                     <input type="submit" onblur="if (window.xfm_blur) xfm_blur(this);" onfocus="if (window.xfm_focus) xfm_focus(this);" class="xfm_submit" name="enter" title="{$messages/xfm:hint[@id=&#34;enter&#34;]}" value="{$messages/xfm:caption[@id=&#34;enter&#34;]}"/>
                  </div>
               </td>
            </tr>
         </form>
      </table>
   </xsl:template>
<!-- is not super user -->

   <xsl:template match="refused">
      <div class="alert">
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;div3&#34;] )[1]"/>
      </div>
   </xsl:template>
<!-- is superuser -->

   <xsl:template match="enter">
      <p>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;p1&#34;] )[1]"/>
      </p>
      <ul>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;ul1&#34;] )[1]"/>
         <li>
            <o:apply-templates select="($messages/xfm:caption[@id=&#34;li1&#34;] )[1]"/>
         </li>
         <li>
            <o:apply-templates select="($messages/xfm:caption[@id=&#34;li2&#34;] )[1]"/>
         </li>
      </ul>
   </xsl:template>
<!-- ====== SCRIPT ====================== -->

   <o:template name="xfm:script">
      <script type="text/javascript" language="javascript">

    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, '');
        return true;
    }

    function xfm_focus(o)
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus';
        return true;
    }


            function xfm_load()
            {

                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }


            function xfm_submit(form)
            {
                var message="";
                var name="";

    if (name != "id")
    {
        name="id";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;id&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

    if (name != "pass")
    {
        name="pass";
        if (xfm_badControl(form, name)) message+='<o:call-template name="xfm:replace">
            <o:with-param name="string" select="normalize-space(($messages/xfm:alert[@id=&#34;pass&#34;]  | $messages/xfm:alert[@id=&#34;xfm:required&#34;] )[1])"/>
         </o:call-template>'+"\n";
    }

                if (!message) return true ;
                alert(message);
                return false;
            }

            function xfm_reset(form)
            {

            }

                </script>
   </o:template>
<!-- ======  COMMON TEMPLATES  ======================
        short templates to format messages
        known issues : no correct id for a repeated help button
        then hide/show message can't work
        but is it useful to repeat the same help ?

-->

   <o:template match="xfm:help">
      <o:param name="id" select="@id"/>
      <input type="button" class="xfm_help" tabindex="32767" value="?">
         <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
      </input>
      <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
         <o:attribute name="id">
            <o:value-of select="$id"/>_help</o:attribute>
         <tr>
            <td width="90%" class="xfm_bar">
               <o:value-of select="(caption | title | h1 )[1]"/>
            </td>
            <td align="right" class="xfm_bar">
               <input type="button" class="xfm_key" value="X">
                  <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td colspan="2" class="xfm_text">
               <o:apply-templates mode="copy"/>
            </td>
         </tr>
      </table>
   </o:template>
   <o:template match="xfm:caption" priority="-1">
      <o:apply-templates mode="copy"/>
   </o:template><!--
        Escape js string (or other search/replace)-->
   <o:template match="* | @* | text()" mode="replace" name="xfm:replace">
      <o:param name="string" select="normalize-space(.)"/>
      <o:param name="search">'</o:param>
      <o:param name="replace">\'</o:param>
      <o:choose>
         <o:when test="contains($string, $search)">
            <o:value-of select="concat(substring-before($string, $search), $replace)"/>
            <o:call-template name="xfm:replace">
               <o:with-param name="string" select="substring-after($string, $search)"/>
               <o:with-param name="search" select="$search"/>
               <o:with-param name="replace" select="$replace"/>
            </o:call-template>
         </o:when>
         <o:otherwise>
            <o:value-of select="$string"/>
         </o:otherwise>
      </o:choose>
   </o:template>
   <o:template match="*" priority="-1" mode="copy">
      <o:element name="{name()}">
         <o:apply-templates select="@*" mode="copy"/>
         <o:apply-templates mode="copy"/>
      </o:element>
   </o:template>
   <o:template match="@*" mode="copy">
      <o:copy/>
   </o:template>
<!-- ======  MESSAGES @xml:lang="fr"  ====================== -->

   <xfm:messages xml:lang="fr">
      <xfm:caption id="title"> SDX Administration - Serveur - Identification </xfm:caption>
      <xfm:caption id="div3">Refusé</xfm:caption>
      <xfm:caption id="p1">
                Vous êtes identifié au "super-utilisateur".
            Vous pouvez accéder au bureau d'administration de chaque application ouverte sur ce serveur.
        </xfm:caption>
      <xfm:caption id="ul1">Vous pouvez aussi accéder aux tâches qui vous concernent seul :</xfm:caption>
      <xfm:caption id="li1">Modifier les informations du "Super-utilisateur"</xfm:caption>
      <xfm:caption id="li2">Ajouter ou supprimer des applications sur ce serveur</xfm:caption>
   </xfm:messages>
<!-- ======  MESSAGES @xml:lang="en"  ====================== -->

   <xfm:messages xml:lang="en">
      <xfm:caption id="title"> SDX Administration - Server - Login </xfm:caption>
      <xfm:caption id="div3">Deny</xfm:caption>
      <xfm:caption id="p1">
                You are logged as a Super-User.
            You can access to the desk of each open application on this server.
        </xfm:caption>
      <xfm:caption id="ul1">Or acces to specific Super-user tasks</xfm:caption>
      <xfm:caption id="li1">Modify Super-user infos</xfm:caption>
      <xfm:caption id="li2">Add or delete applications on this server</xfm:caption>
   </xfm:messages>
   <o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>