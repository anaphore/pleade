// $Id: CQLNotNode.java 19706 2010-11-25 16:52:20Z jcwiklinski $

package org.sru.cql;


/**
 * Represents a NOT node in a CQL parse-tree.
 *
 * @version	$Id: CQLNotNode.java 19706 2010-11-25 16:52:20Z jcwiklinski $
 */
public class CQLNotNode extends CQLBooleanNode {
    /**
     * Creates a new NOT node with the specified left- and right-hand
     * sides and modifiers.
     */
    public CQLNotNode(CQLNode left, CQLNode right, ModifierSet ms) {
	super(left, right, ms);
    }

    byte[] opType1() {
	byte[] op = new byte[5];
	putTag(CONTEXT, 46, CONSTRUCTED, op, 0); // Operator
	putLen(2, op, 2);
	putTag(CONTEXT, 2, PRIMITIVE, op, 3); // and-not
	putLen(0, op, 4);
	return op;
    }
}
