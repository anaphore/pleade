// $Id: CQLParseException.java 19706 2010-11-25 16:52:20Z jcwiklinski $

package org.sru.cql;
import java.lang.Exception;


/**
 * Exception indicating that an error ocurred parsing CQL.
 *
 * @version	$Id: CQLParseException.java 19706 2010-11-25 16:52:20Z jcwiklinski $
 */
public class CQLParseException extends Exception {
    /**
     * Creates a new <TT>CQLParseException</TT>.
     * @param s
     *	An error message describing the problem with the query,
     *	usually a syntax error of some kind.
     */
    public CQLParseException(String s) {
	super(s);
    }
}

