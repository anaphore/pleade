<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Restitue les propriétés courantes d'un document
    | Le but du jeu est de resortir un xml de la même forme que le *-documents-property-sets.xconf à partir d'un aggregate
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl">

	<xsl:template match="/property-sets">
		<property-sets>
			<xsl:apply-templates select="properties"/>
		</property-sets>
	</xsl:template>

	<xsl:template match="properties">
		<property-set id="{@id}">
			<xsl:apply-templates select="property"/>
		</property-set>
	</xsl:template>

	<xsl:template match="property">
		<property name="{@name}">
			<xsl:attribute name="default">
				<xsl:choose>
					<xsl:when test="@name = 'docUrl'"/>
					<xsl:when test=". != ''"><xsl:value-of select="."/></xsl:when>
					<xsl:when test="@default"><xsl:value-of select="@default"/></xsl:when>
				</xsl:choose>
			</xsl:attribute>
		</property>
	</xsl:template>

</xsl:stylesheet>
