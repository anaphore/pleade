<?xml version="1.0" encoding="UTF-8"?>  
<!--
    extends but not fully implements W3C namespace "http://www.w3.org/2002/01/xforms"
-->  

<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" version="1.0">
   <xsl:output indent="yes" method="xml" encoding="UTF-8"/> 
<!-- parameter language to choose a message set -->
   <o:param name="lang-default" select="'fr'"/>
   <xsl:param name="lang" select="'fr'"/> 
<!-- take set of language -->
   <o:variable name="messages" select="document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
   <xsl:param name="binds-url" select="/htm:html/htm:head/xfm:model/xfm:bind/@href"/>
   <xsl:param name="binds" select="//*[local-name()='bind'] | document($binds-url)//*[local-name()='bind']"/>
   <xsl:param name="action" select="//xfm:model/xfm:submitInfo/@action"/>
   <xsl:param name="method" select="//xfm:model/xfm:submitInfo/@method"/>
   <xsl:namespace-alias stylesheet-prefix="o" result-prefix="xsl"/>  
<!--                                                                      -->  
  
<!-- ======================= Variables and parameters =================== -->  
  
<!--                                                                      -->  
  
<!--
     default language messages  -->  
  
<!-- "binds.xml" define constraints for the nodes 
        used for immediate javascript validation -->  

   <xsl:variable name="messages" select="(//xfm:message| //xfm:caption| //xfm:hint| //xfm:alert| //xfm:help | $binds//xfm:message| $binds//xfm:caption| $binds//xfm:hint| $binds//xfm:alert| $binds//xfm:help)[normalize-space(.)!= '']"/>
   <xsl:variable name="apos">'</xsl:variable>
   <xsl:variable name="quot">"</xsl:variable>  
<!--
-->  
  
<!--                                                                      -->  
  
<!-- ============================= ROOT ================================= -->  
  
<!--                                                                      -->  
  
<!--
-->  

   <xsl:template match="/*">  
<!--
        create an xsl from "xform.xhtml" to display the instance
         <xsl:result-document href="{$xslform}" format="UTF-8"/>-->  

      <o:stylesheet xmlns:o="output.xsl" version="1.0">  
<!-- hack to get namespaces from source and declared in root element -->  

         <xsl:copy-of select="@*[namespace-uri() != '' and namespace-uri() != 'http://www.w3.org/XML/1998/namespace']"/>
         <xsl:apply-templates select="xsl:import | xsl:include | xsl:output"/>
         <xsl:call-template name="xfm:params"/>
         <xsl:apply-templates select="*[name() != 'xsl:param' and name() != 'xsl:include' and name() != 'xsl:import' and name() != 'xsl:output'] | comment()"/>
         <xsl:apply-templates mode="template"/>
         <xsl:call-template name="script"/>
         <xsl:call-template name="templates"/>
         <xsl:call-template name="xfm:messages"/>
         <o:template match="node()" mode="onload" priority="-2"/>
      </o:stylesheet>
   </xsl:template>  
<!--
            <xsl:apply-templates select="//*" mode="template"/>
            <xsl:call-template name="comment">
                <xsl:with-param name="name">BODY</xsl:with-param>
            </xsl:call-template>
            <o:template name="body">
                <xsl:for-each select="/htm:html/htm:body[1]">
                    <xsl:apply-templates/>
                </xsl:for-each>
            </o:template>
            <xsl:call-template name="head"/>
            <xsl:call-template name="script"/>
            <xsl:call-template name="style"/>
-->  
  
<!--
                                                                
 =================== MESSAGES ===============================  
                                                                
-->  

   <xsl:template name="xfm:messages">  
<!-- write blocks of messages per lang  -->  
  
<!-- get the different langs from all messages -->  

      <xsl:variable name="langs-list" select="$messages[ancestor-or-self::*[@xml:lang]]/ancestor-or-self::*/@xml:lang[1]"/>
      <xsl:choose>
         <xsl:when test="function-available('xalan:distinct')">
            <xsl:call-template name="messages">
               <xsl:with-param name="langs" select="xalan:distinct($langs-list)"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="function-available('saxon:distinct')">
            <xsl:call-template name="messages">
               <xsl:with-param name="langs" select="saxon:distinct($langs-list)"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="langs-distinct">
               <xsl:with-param name="crowd" select="$langs-list"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!-- generic distinct nodes for multi-document nodeset, to avoid an extension node-set() function
    thanks to jeni@jenitennison.com 
    empty an indistinct "crowd" of nodes into "ones" -->  

   <xsl:template name="langs-distinct">
      <xsl:param name="crowd"/>
      <xsl:param name="ones" select="crowd[1]"/>
      <xsl:choose>
         <xsl:when test="$crowd">
            <xsl:call-template name="langs-distinct">
               <xsl:with-param name="ones" select="$ones | $crowd[1][not(. = $ones )]"/>
               <xsl:with-param name="crowd" select="$crowd[position() &gt; 1]"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="messages">
               <xsl:with-param name="langs" select="$ones"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!-- process messages -->  

   <xsl:template name="messages">
      <xsl:param name="langs"/>
      <xsl:for-each select="$langs">  
<!-- blocks of identified messages for each languages -->  

         <xsl:variable name="lang" select="."/>
         <xsl:call-template name="comment">
            <xsl:with-param name="name">
               <xsl:choose>
                  <xsl:when test="contains($lang, 'fr')"> MESSAGES @xml:lang="fr" </xsl:when>
                  <xsl:otherwise> MESSAGES @xml:lang="<xsl:value-of select="$lang"/>" </xsl:otherwise>
               </xsl:choose>
            </xsl:with-param>  
<!--
                <xsl:with-param name="text">
                    <xsl:choose>
                        <xsl:when test="contains($lang, 'fr')">
        Les messages en français de votre formulaire
        (&lt;caption/>, &lt;hint>, &lt;help/>, &lt;alert/>, &lt;message/>)
        2 blocs
        1) les messages se rapportant à des éléments non identifiés,
           (pas d'attributs @bind, ou @id)
           identifiant bâti sur le nom de l'élément parent
           dans l'ordre d'apparition de la source
        2) les messages se rapportant à des contrôles identifiés,
           (attributs @bind, ou @id)
           dans l'ordre alphabétique des identifiants
           et si plusieurs messages
             1) message spécifique au formulaire
             2) message commun à une contrainte (&lt;bind id="title"/&gt;)
             3) message par défaut pour certaines actions
                                </xsl:when>
                        <xsl:otherwise>
            (&lt;caption/>, &lt;hint>, &lt;help/>, &lt;alert/>, &lt;message/>)
        1) no user defined id (@id or @bind of parent) => id=name(parent)position(parent)
        2) user defined id, ordered by id, and origin (form-binds-default)
                                </xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
-->  

         </xsl:call-template>
         <xfm:messages xml:lang="{$lang}">  
<!-- lang() buggy in msxml3 -->  

            <xsl:apply-templates select="$messages[ ancestor-or-self::*[@xml:lang][1][@xml:lang=$lang]   ] | $messages[not(ancestor-or-self::*[@xml:lang])]" mode="message">
               <xsl:sort select="../@id|../@bind"/>
            </xsl:apply-templates>  
<!-- some default messages for controls -->  
  
<!--
                <xsl:if test="starts-with($lang, 'fr')">
                    <xfm:hint id="xfm:select-add">[Entrée] Ajouter l'item sélectionné à la liste 
[Ctrl+Click] ou [Maj+Haut/Bas] pour en sélectionner plusieurs</xfm:hint>
                    <xfm:hint id="xfm:select-del">[Suppr] Supprimer l'item sélectionné
[Ctrl+Click] ou [Maj+Haut/Bas] pour en sélectionner plusieurs</xfm:hint>
                    <xfm:hint id="xfm:select-up">[Ctrl+Haut] Monter l'item sélectionné d'un rang</xfm:hint>
                    <xfm:hint id="xfm:select-down">[Ctrl+Bas] Descendre l'item sélectionné d'un rang</xfm:hint>
                    <xfm:hint id="xfm:select-reset">Supprimer tous les items</xfm:hint>
                    <xfm:caption id="xfm:insert">+</xfm:caption>
                    <xfm:hint id="xfm:insert">Ajouter un item en fin de liste</xfm:hint>
                    <xfm:caption id="xfm:delete">-</xfm:caption>
                    <xfm:hint id="xfm:delete">Retirer le dernier item de la liste</xfm:hint>
                    <xfm:caption id="xfm:submit">Envoyer</xfm:caption>
                    <xfm:hint id="xfm:submit">Envoyer le formulaire au serveur</xfm:hint>
                    <xfm:caption id="xfm:reset">Restaurer</xfm:caption>
                    <xfm:hint id="xfm:reset">Restaurer le formulaire</xfm:hint>
                    <xfm:caption id="xfm:secret">Mot de passe</xfm:caption>
                    <xfm:hint id="xfm:secret">Taper un mot de passe dans le premier champ, confirmez dans le deuxième.</xfm:hint>
                    <xfm:alert id="xfm:secret">Avez-vous fait une erreur ? Les deux champs ne sont pas identiques.</xfm:alert>
                </xsl:if>
                <xsl:if test="starts-with($lang, 'en')">
                    <xfm:hint id="xfm:select-add">[Return] Add selected item(s) to the list 
[Ctrl+Click] or [Shift+Up/Down] to select more than one</xfm:hint>
                    <xfm:hint id="xfm:select-del">Delete selected item(s)
[Ctrl+Click] or [Shift+Up/Down] to select more than one</xfm:hint>
                    <xfm:hint id="xfm:select-up">[Ctrl+Up] Selected item go up</xfm:hint>
                    <xfm:hint id="xfm:select-down">[Ctrl+Down] Selected item go down</xfm:hint>
                    <xfm:hint id="xfm:select-reset">Delete all items</xfm:hint>
                    <xfm:caption id="xfm:insert">+</xfm:caption>
                    <xfm:hint id="xfm:insert">Add an item to the end of the list</xfm:hint>
                    <xfm:caption id="xfm:delete">-</xfm:caption>
                    <xfm:caption id="xfm:submit">Send</xfm:caption>
                    <xfm:hint id="xfm:submit">Send data to the server</xfm:hint>
                    <xfm:caption id="xfm:reset">Reset</xfm:caption>
                    <xfm:caption id="xfm:secret">Pass</xfm:caption>
                    <xfm:hint id="xfm:secret">Type your pass in first field, confirm in second one.</xfm:hint>
                    <xfm:alert id="xfm:secret">Is something wrong ? The two fields are not equal.</xfm:alert>
                </xsl:if>
-->  

         </xfm:messages>
      </xsl:for-each>  
<!-- messages without lang specification, add to list of each language -->  

      <xsl:if test="$messages[not(@xml:lang)][not(ancestor::*[@xml:lang])]">
         <xsl:call-template name="comment">
            <xsl:with-param name="name"> MESSAGES, no specified language </xsl:with-param>
            <xsl:with-param name="text">
            (&lt;caption/&gt;, &lt;hint&gt;, &lt;help/&gt;, &lt;alert/&gt;, &lt;message/&gt;)
        1) no user defined id (@id or @bind of parent) =&gt; id=name(parent)position(parent)
        2) user defined id, ordered by id, and origin (form-binds-default)
                        </xsl:with-param>
         </xsl:call-template>
         <xfm:messages>
            <xsl:apply-templates select="$messages[not(@xml:lang)][not(ancestor::*[@xml:lang])]" mode="message">
               <xsl:sort select="../@id|../@bind"/>
            </xsl:apply-templates>
         </xfm:messages>
      </xsl:if>
   </xsl:template>  
<!-- produce a message node inside xsl output for localisation, used on a node-set -->  

   <xsl:template match="*" mode="message">
      <xsl:element name="{name()}">
         <xsl:attribute name="id">
            <xsl:call-template name="xfm:getId"/>
         </xsl:attribute>
         <xsl:apply-templates/>
      </xsl:element>
   </xsl:template>
   <xsl:template match="xfm:alert" mode="message">
      <xsl:element name="{name()}">
         <xsl:attribute name="id">
            <xsl:call-template name="xfm:getId"/>
         </xsl:attribute>
         <xsl:call-template name="replace">
            <xsl:with-param name="string" select="normalize-space(.)"/>
         </xsl:call-template>
      </xsl:element>
   </xsl:template>
   <xsl:template match="xfm:caption[name(..)='xfm:item']" mode="message">
      <xsl:element name="xfm:caption">
         <xsl:attribute name="parent">
            <xsl:apply-templates select="ancestor::*[name()='xfm:choices' or name()='xfm:selectOne' or name()='xfm:selecMany'][1]" mode="getId"/>
         </xsl:attribute>
         <xsl:attribute name="id">
            <xsl:call-template name="xfm:getId"/>
         </xsl:attribute>
         <xsl:apply-templates/>
      </xsl:element>
   </xsl:template>  
<!--
                                                                
 =================== STRUCTURE ===============================  
                                                                
-->  
  
<!-- ============= xsl:PARAMS ===================== -->  

   <xsl:template name="xfm:params">
      <xsl:text/>
      <xsl:comment> parameter language to choose a message set </xsl:comment>
      <xsl:variable name="apos">'</xsl:variable>
      <o:param xmlns:o="output.xsl" name="lang-default">
         <xsl:attribute name="select">
            <xsl:choose>
               <xsl:when test="false()"/>  
<!--
                    <xsl:when test="$langs-list">
                        <xsl:value-of select="concat($apos, $langs-list[1], $apos)"/>
                    </xsl:when>
-->  

               <xsl:otherwise>
                  <xsl:value-of select="concat($apos, $lang, $apos)"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
      </o:param>
      <xsl:choose>
         <xsl:when test="not(xsl:param[@name='lang'])">
            <xsl:text/>
            <xsl:comment> lang param </xsl:comment>
            <o:param xmlns:o="output.xsl" name="lang">
               <o:choose>
                  <o:when test="//@xml:lang">
                     <o:value-of select="//@xml:lang"/>
                  </o:when>
                  <o:otherwise>
                     <o:value-of select="$lang-default"/>
                  </o:otherwise>
               </o:choose>
            </o:param>
         </xsl:when>
         <xsl:when test="xsl:param[@name='lang' and @select='false()']"/>
         <xsl:otherwise>
            <xsl:apply-templates select="xsl:param[@name='lang']"/>
         </xsl:otherwise>
      </xsl:choose>  
<!--
        <xsl:comment> lang of messages : $lang if available, otherwise : $lang-default </xsl:comment>
        <o:param name="messages-lang">
            <o:choose>
                <o:when test="document($messages-URI)/*/xfm:messages[starts-with(@xml:lang, $lang)]">
                    <o:value-of select="$lang"/>
                </o:when>
                <o:otherwise>
                    <o:value-of select="$lang-default"/>
                </o:otherwise>
            </o:choose>
        </o:param>
        -->  

      <xsl:text/>
      <xsl:apply-templates select="xsl:param[@name='messages-URI']"/>
      <xsl:comment> take set of language </xsl:comment>
      <xsl:choose>
         <xsl:when test="xsl:param[@name='messages-URI']">
            <o:variable xmlns:o="output.xsl" name="messages" select="document($messages-URI)/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)] | document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
         </xsl:when>
         <xsl:when test="xsl:param[@name='messages' and @select='false()']"/>
         <xsl:otherwise>
            <o:variable xmlns:o="output.xsl" name="messages" select="document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="xsl:param[not(@name='lang' or @name='messages-lang' or @name='messages-URI' or @name='lang-default' or @name='messages')]"/>
   </xsl:template>
   <xsl:template match="/htm:html">
      <xsl:call-template name="comment">
         <xsl:with-param name="name">ROOT</xsl:with-param>
      </xsl:call-template>
      <o:template xmlns:o="output.xsl" match="/">
         <html>
            <head>
               <o:call-template name="xfm:head"/>
            </head>
            <body>
               <xsl:apply-templates select="htm:html/htm:body/@*"/>
               <form style="margin:0" action="{//xfm:submitInfo/@action | //htm:form/@action}" method="{//xfm:submitInfo/@method | //htm:form/@method}" enctype="{//xfm:submitInfo/@encoding | //htm:form/@enctype}">
                  <o:call-template name="body"/>
               </form>
            </body>
         </html>
      </o:template>
   </xsl:template>  
<!-- ============= GROUP ===================== -->  

   <xsl:template match="xfm:group">
      <xsl:variable name="name">
         <xsl:call-template name="xfm:getId"/>
      </xsl:variable>
      <o:call-template xmlns:o="output.xsl" name="{$name}"/>
   </xsl:template>
   <xsl:template match="xfm:group" mode="template">
      <xsl:variable name="name">
         <xsl:call-template name="xfm:getId"/>
      </xsl:variable>
      <xsl:variable name="select">
         <xsl:call-template name="xfm:getSelect"/>
      </xsl:variable>
      <xsl:variable name="ref">
         <xsl:call-template name="xfm:getRef"/>
      </xsl:variable>
      <xsl:call-template name="comment">
         <xsl:with-param name="name" select="concat('GROUP ', $name)"/>
      </xsl:call-template>
      <o:template xmlns:o="output.xsl" name="{$name}">
         <xsl:choose>
            <xsl:when test="$ref != ''">
               <o:for-each select="{$select}[1]">
                  <div id="{$ref}" class="xfm_group">
                     <xsl:apply-templates/>
                  </div>
               </o:for-each>
            </xsl:when>
            <xsl:when test="$select != ''">
               <o:for-each select="{$select}">
                  <div id="{$select}" class="xfm_group">
                     <xsl:apply-templates/>
                  </div>
               </o:for-each>
            </xsl:when>
            <xsl:otherwise>
               <div id="{$name}" class="xfm_group">
                  <xsl:apply-templates/>
               </div>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:if test="@ref|@bind"/>
      </o:template>
   </xsl:template>  
<!-- ============= REPEAT ===================== -->  

   <xsl:template match="xfm:repeat">
      <xsl:variable name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:variable>
      <xsl:variable name="ref">
         <xsl:call-template name="xfm:getRef"/>
      </xsl:variable>
      <xsl:variable name="select">
         <xsl:call-template name="xfm:getSelect"/>
      </xsl:variable>
      <xsl:comment>block to repeat (<xsl:value-of select="$ref"/>) </xsl:comment>
      <xsl:choose>
         <xsl:when test="htm:tr">
            <tbody id="xfm_{$id}2repeat" class="xfm_2repeat" style="display: none">  
<!-- no class here to avoid init controls -->  

               <o:call-template xmlns:o="output.xsl" name="{$id}"/>
            </tbody>
            <tbody id="{$ref}" class="xfm_repeat">
               <o:apply-templates xmlns:o="output.xsl" select="{$select}"/>
            </tbody>
         </xsl:when>
         <xsl:otherwise>
            <div id="xfm_{$id}2repeat" class="xfm_2repeat" style="display:none">
               <o:call-template xmlns:o="output.xsl" name="{$id}"/>
            </div>
            <div id="{$ref}" class="xfm_repeat">
               <o:apply-templates xmlns:o="output.xsl" select="{$select}"/>
            </div>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!-- the template of what to repeat, 
    to easy count and refer, only the first node is copied (and repeatable) -->  

   <xsl:template match="xfm:repeat" mode="template">
      <xsl:call-template name="comment"/>
      <xsl:variable name="select">
         <xsl:call-template name="xfm:getSelect"/>
      </xsl:variable>
      <xsl:variable name="name">
         <xsl:call-template name="xfm:getId"/>
      </xsl:variable>
      <o:template xmlns:o="output.xsl" match="{$select}" name="{$name}">
         <xsl:apply-templates select="*[1]"/>
      </o:template>
   </xsl:template>  
<!--                                            -->  
  
<!-- ========= SCRIPT ========================= -->  
  
<!--                                            -->  

   <xsl:template match="xfm:instance[not(preceding::xfm:instance)][@href]">
      <textarea onchange="xfm_doc=string2doc(this.value); fillForm();">
         <xsl:copy-of select="@class | @row | @cols"/>
         <xsl:attribute name="style">
            <xsl:choose>
               <xsl:when test="@style">
                  <xsl:value-of select="@style"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:text>width:100%; heighxsl:10em; background:transparent; color:GrayText; border: groove 2;</xsl:text>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
         <xsl:attribute name="name">
            <xsl:choose>
               <xsl:when test="@name">
                  <xsl:value-of select="@name"/>
               </xsl:when>
               <xsl:otherwise>xml</xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
         <xsl:attribute name="id">
            <xsl:choose>
               <xsl:when test="@id">
                  <xsl:value-of select="@id"/>
               </xsl:when>
               <xsl:otherwise>xfm_xml</xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
      </textarea>
   </xsl:template>
   <xsl:template name="script">  
<!--
    function xfm_initEvents(node)
    {
        if (!document.getElementsByTagName) return;
        if (!node) node=document.documentElement;
        var x = node.getElementsByTagName('BUTTON');
        for (var i=0;i<x.length;i++)
        { 
            x[i].onmousedown=function() {this.className='down';}
            x[i].onmouseup=function() {this.className='up';}
            x[i].onmouseover=function() {this.classOld=this.className; this.className='over';}
            x[i].onmouseout=function() {this.className=this.classOld;}
        }
    }
-->  

      <xsl:call-template name="comment">
         <xsl:with-param name="name">SCRIPT</xsl:with-param>
      </xsl:call-template>
      <o:template xmlns:o="output.xsl" name="xfm:script">
         <script type="text/javascript" language="javascript">
    
    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, ''); 
        return true;
    }

    function xfm_focus(o) 
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus'; 
        return true;
    }
    

            function xfm_load()
            {
                <xsl:for-each select="//xfm:instance[@href][1]">
                    var xfm_formId='xfm_xml';
                    <xsl:if test="@id">xfm_formId="<xsl:value-of select="@id"/>";</xsl:if>
                    if (document.getElementById) xfm_form=document.getElementById(xfm_formId);
                    // if (!xfm_doc || !xfm_doc.documentElement) fillXml();
                    if (xfm_form) {REFRESHFORM=true; xfm_form.value=doc2string(xfm_doc); }
                    xfm_initControls();
                </xsl:for-each>
                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }

            
            function xfm_submit(form)
            {
                var message="";
                var name="";
                <xsl:apply-templates select="//* | $binds" mode="submit">
               <xsl:sort select="@htm:name | @name | @id | @bind | @ref | @xsl:select"/>
            </xsl:apply-templates>
                if (!message) return true ;
                alert(message);
                return false;
            }
            
            function xfm_reset(form)
            {
            
            }

                <xsl:for-each select="//xfm:instance[@href][1]">
               <xsl:value-of select="concat('href=&#34;', @href, '&#34;; ')"/>
               <xsl:text>xfm_http="xfm_doc"; </xsl:text>
               <xsl:if test="normalize-space(@name)!=''">xfm_http="<xsl:value-of select="@name"/>";</xsl:if>
            </xsl:for-each>
         </script>
      </o:template>
   </xsl:template>  
<!--
    <xsl:template match="*[boolean(@required)=true()]" mode="submit">
    if (name != "<xsl:call-template name="xfm:getName"/>") // controls sorted, no handle of multiple fields
    {
        set=false;
        name="<xsl:call-template name="xfm:getName"/>";
        control=form[name];
        if (!control) { set=true} // let it, the control is not displayed
        else if (control.type == 'hidden') set=true; // let it, user can't modify this control
        else if (control.style) // control is not displayed
            if (control.style.display=="none" || control.style.visibility=="hidden" ) set=true;
        else if (control.selectedIndex) set=true;
        else if (control.value) set=true;
        if (!set) message+='<xsl:call-template name="xfm:alert">
            <xsl:with-param name="alt-id" select="'xfm:required'"/>
        </xsl:call-template>'+"\n";
    }
    </xsl:template>
-->  

   <xsl:template match="*[boolean(@required)=true()]" mode="submit">
    if (name != "<xsl:call-template name="xfm:getName"/>")
    {
        name="<xsl:call-template name="xfm:getName"/>";
        if (xfm_badControl(form, name)) message+='<xsl:call-template name="xfm:alert">
         <xsl:with-param name="alt-id" select="'xfm:required'"/>
      </xsl:call-template>'+"\n";
    }
    </xsl:template>  
<!--                                                                      -->  
  
<!-- =========================== CONTROLS =============================== -->  
  
<!--                                                                      -->  
  
<!--
-->  
  
<!--  controls could be isolated in templates
    <xsl:template match=" xfm:input | xfm:textarea | xfm:output">
        <o:call-template>
            <xsl:attribute name="name">
                <xsl:call-template name="xfm:getId"/>
            </xsl:attribute>
        </o:call-template>
    </xsl:template>
    <xsl:template match="xfm:input | xfm:textarea | xfm:output " mode="template">
        <xsl:call-template name="comment"/>
        <o:template>
            <xsl:attribute name="name">
                <xsl:call-template name="xfm:getId"/>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="content"/>
        </o:template>
    </xsl:template>
    -->  
  
<!-- ========= BUTTON ======== -->  
  
<!-- // not for NS4
    <xsl:template match="xfm:button">
        <xsl:param name="id">
            <xsl:call-template name="xfm:getId"/>
        </xsl:param>
        <button>
            <xsl:call-template name="xfm:control-atts">
                <xsl:with-param name="onchange" select="''"/>
                <xsl:with-param name="value" select="''"/>
            </xsl:call-template>
            <xsl:attribute name="title">{$messages/xfm:hint[@id=&quot;<xsl:value-of select="$id"/>&quot;] | $messages/xfm:hint[@id=&quot;<xsl:value-of select="name()"/>&quot;]}</xsl:attribute>
            <xsl:choose>
                <xsl:when test="name()='xfm:button'">
                    <xsl:apply-templates select="xfm:reset | xfm:submit | xfm:insert | xfm:delete" mode="action"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="." mode="action"/>
                </xsl:otherwise>
            </xsl:choose>
            <o:copy-of>
                <xsl:attribute name="select">($messages/xfm:caption[@id=&quot;<xsl:value-of select="$id"/>&quot;] | $messages/xfm:caption[@id=&quot;<xsl:value-of select="name()"/>&quot;])[1]/node()</xsl:attribute>
            </o:copy-of>
            <xsl:apply-templates select="text()"/>
        </button>
        <xsl:apply-templates select="xfm:help"/>
    </xsl:template>
    -->  

   <xsl:template match="xfm:button">
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <input>
         <xsl:call-template name="xfm:control-atts">
            <xsl:with-param name="onchange" select="''"/>
            <xsl:with-param name="value" select="''"/>
         </xsl:call-template>
         <xsl:apply-templates mode="attribute"/>
         <xsl:attribute name="value">{<xsl:apply-templates select="xfm:caption[1]" mode="xpath"/>}</xsl:attribute>
         <xsl:apply-templates select="xfm:help"/>
      </input>
   </xsl:template>  
<!-- ======== INSERT ======== -->  

   <xsl:template match="xfm:insert" mode="action">
      <xsl:variable name="id">
         <xsl:choose>  
<!-- relative to repeat of same bind name -->  

            <xsl:when test="//xfm:repeat[@bind=current()/@bind]">
               <xsl:apply-templates select="//xfm:repeat[@bind=current()/@bind]" mode="getId"/>
            </xsl:when>  
<!-- or relative to the next one -->  

            <xsl:otherwise>
               <xsl:apply-templates select="following::xfm:repeat[1] | preceding::xfm:repeat[1]" mode="getId"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:attribute name="type">button</xsl:attribute>
      <xsl:attribute name="onclick">xfm_insert('<xsl:value-of select="$id"/>');</xsl:attribute>
   </xsl:template>  
<!-- ======== DELETE ======== -->  

   <xsl:template match="xfm:delete" mode="action">  
<!--
        <xsl:variable name="id">
            <xsl:choose>
                <xsl:when test="//xfm:repeat[@bind=current()/@bind]">
                    <xsl:apply-templates select="//xfm:repeat[@bind=current()/@bind]" mode="getId"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="following::xfm:repeat[1] | preceding::xfm:repeat[1]" mode="getId"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="path">
            <xsl:choose>
                <xsl:when test="//xfm:repeat[@bind=current()/@bind]">
                    <xsl:apply-templates select="//xfm:repeat[@bind=current()/@bind]" mode="getPathAbs"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="following::xfm:repeat[1] | preceding::xfm:repeat[1]" mode="getPathAbs"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        -->  

      <xsl:attribute name="type">button</xsl:attribute>
      <o:attribute xmlns:o="output.xsl" name="onclick">return xfm_delete();</o:attribute>
   </xsl:template>  
<!-- ======== RESET ======== -->  

   <xsl:template match="xfm:button[xfm:reset] | xfm:reset">
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <input type="reset">
         <xsl:call-template name="xfm:control-atts">
            <xsl:with-param name="onchange" select="''"/>
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="type" select="'reset'"/>
         </xsl:call-template>
         <xsl:attribute name="title">{$messages/xfm:hint[@id="<xsl:value-of select="$id"/>"] | $messages/xfm:hint[@id="<xsl:value-of select="name()"/>"]}</xsl:attribute>
         <xsl:attribute name="value">{$messages/xfm:caption[@id="<xsl:value-of select="$id"/>"] | $messages/xfm:caption[@id="<xsl:value-of select="name()"/>"]}</xsl:attribute>
      </input>
      <xsl:apply-templates select="xfm:help"/>
   </xsl:template>  
<!-- ======== SUBMIT ======== -->  

   <xsl:template match="xfm:button[xfm:submit] | xfm:submit">
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <input type="submit">
         <xsl:call-template name="xfm:control-atts">
            <xsl:with-param name="onchange" select="''"/>
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="type" select="'submit'"/>
         </xsl:call-template>
         <xsl:attribute name="title">{$messages/xfm:hint[@id="<xsl:value-of select="$id"/>"]}</xsl:attribute>
         <xsl:attribute name="value">{$messages/xfm:caption[@id="<xsl:value-of select="$id"/>"]}</xsl:attribute>
      </input>
      <xsl:apply-templates select="xfm:help"/>
   </xsl:template>  
<!--
============================================================
   sélections unique ou multiple
de nombreux perfectionnements peuvent être implémentés à cet endroit
selon le nombre d'items, choisir entre
 <input type="radio"/> ou  <select/> ou <select><input name="search"/></select>
 <input type="checkbox"/> ou <select multiple="multiple"/> ou <select name="edit"/><select name="list"/>
 
 réfléchir à un import externe de liste (langues, mime/type ..)
============================================================
-->  
  
<!-- ======== SELECT ONE ======== -->  

   <xsl:template match="xfm:selectOne">
      <xsl:call-template name="xfm:label"/>
      <select>  
<!-- set xml instance -->  

         <xsl:choose>
            <xsl:when test="//xfm:instance/@href and (@bind or @ref)">
               <xsl:attribute name="onchange">
                  <xsl:text>i=this.selectedIndex; if(i&gt;-1)setNode(this.ref, this.options[i].value); </xsl:text>
                  <xsl:value-of select="normalize-space(@onchange)"/>
               </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
               <xsl:copy-of select="@onchange"/>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:call-template name="xfm:control-atts">
            <xsl:with-param name="onchange" select="''"/>
            <xsl:with-param name="value" select="''"/>
         </xsl:call-template>
         <xsl:apply-templates select="xfm:itemset | xfm:item | xfm:choices | htm:option"/>
      </select>
   </xsl:template>  
<!-- ======== SELECT MANY ======== -->  

   <xsl:template match="xfm:selectMany">
      <xsl:call-template name="xfm:label"/>
      <select multiple="multiple">
         <xsl:apply-templates mode="attribute"/>  
<!-- set xml instance -->  

         <xsl:choose>
            <xsl:when test="//xfm:instance/@href and (@bind or @ref)">
               <xsl:attribute name="onchange">  
<!-- need a function to clear node, and append node -->  

                  <xsl:text>for (var i=0; i&lt; this.options.length; i++) if(this.options[i].selected) setNode(this.ref, this.options[i].value); </xsl:text>
                  <xsl:value-of select="normalize-space(@onchange)"/>
               </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
               <xsl:copy-of select="@onchange"/>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:call-template name="xfm:control-atts">
            <xsl:with-param name="onchange" select="''"/>
            <xsl:with-param name="value" select="''"/>
         </xsl:call-template>
         <xsl:apply-templates select="xfm:itemset | xfm:item | xfm:choices | htm:option"/>
      </select>
   </xsl:template>  
<!-- ======== SELECT MANY double ======== -->  

   <xsl:template match="xfm:selectMany[@ui='double']" mode="submit">
      <xsl:variable name="name">
         <xsl:call-template name="xfm:getName"/>
      </xsl:variable>
    if (form.<xsl:value-of select="$name"/>) selectAll(form.<xsl:value-of select="$name"/>);
    </xsl:template>
   <xsl:template match="xfm:selectMany[@ui='double']">
      <xsl:variable name="name">
         <xsl:call-template name="xfm:getName"/>
      </xsl:variable>
      <table cellpadding="0" cellspacing="0" border="0" class="xfm_control">
         <xsl:apply-templates mode="attribute"/>
         <tr>
            <xsl:call-template name="xfm:label"/>
            <td>
               <select multiple="multiple" size="5" onkeydown="return xfm_selectKeydown (this, this.form.{$name});">  
<!-- set xml instance -->  

                  <xsl:choose>
                     <xsl:when test="//xfm:instance/@href and (@bind or @ref)">
                        <xsl:attribute name="onchange">  
<!-- need a function to clear node, and append node -->  

                           <xsl:text>for (var i=0; i&lt; this.options.length; i++) if(this.options[i].selected) setNode(this.ref, this.options[i].value); </xsl:text>
                           <xsl:value-of select="normalize-space(@onchange)"/>
                        </xsl:attribute>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:copy-of select="@onchange"/>
                     </xsl:otherwise>
                  </xsl:choose>
                  <xsl:call-template name="xfm:control-atts">
                     <xsl:with-param name="onchange" select="''"/>
                     <xsl:with-param name="value" select="''"/>
                     <xsl:with-param name="name" select="''"/>
                  </xsl:call-template>
                  <xsl:apply-templates select="xfm:itemset | xfm:item | xfm:choices | htm:option"/>
               </select>
            </td>
            <td>
               <input class="xfm_key" type="button" value=" &gt; " onclick="for (var i=0; this.form.length; i++) if (this.form[i]==this) break; xfm_selectAdd(this.form[i-1],this.form.{$name}); ">
                  <xsl:call-template name="xfm:title">
                     <xsl:with-param name="id" select="'xfm:select-add'"/>
                  </xsl:call-template>
               </input>
            </td>
            <td>
               <select multiple="multiple" size="5" onkeydown="return xfm_selectKeydown(this)">
                  <xsl:call-template name="xfm:control-atts">
                     <xsl:with-param name="onchange" select="''"/>
                     <xsl:with-param name="value" select="''"/>
                  </xsl:call-template>
                  <xsl:apply-templates select="xfm:itemset | xfm:item | xfm:choices | htm:option">
                     <xsl:with-param name="selected" select="'only'"/>
                  </xsl:apply-templates>
               </select>
            </td>
            <td width="1%">
               <div>
                  <input class="xfm_key" type="button" value=" - " onclick="optionDel(this.form.{$name}); ">
                     <xsl:call-template name="xfm:title">
                        <xsl:with-param name="id" select="'xfm:select-del'"/>
                     </xsl:call-template>
                  </input>
               </div>
               <div>
                  <input class="xfm_key" type="button" value=" ^ " onclick="optionUp(this.form.{$name}); ">
                     <xsl:call-template name="xfm:title">
                        <xsl:with-param name="id" select="'xfm:select-up'"/>
                     </xsl:call-template>
                  </input>
               </div>
               <div>
                  <input class="xfm_key" type="button" value=" v " onclick="optionDown(this.form.{$name}); ">
                     <xsl:call-template name="xfm:title">
                        <xsl:with-param name="id" select="'xfm:select-down'"/>
                     </xsl:call-template>
                  </input>
               </div>
               <div>
                  <input class="xfm_key" type="button" value=" 0 " onclick="xfm_selectReset(this.form.{$name}); ">
                     <xsl:call-template name="xfm:title">
                        <xsl:with-param name="id" select="'xfm:select-reset'"/>
                     </xsl:call-template>
                  </input>
               </div>
            </td>
            <td width="100%"/>
         </tr>
      </table>
   </xsl:template>  
<!-- ============ SELECTONE open ========= -->  

   <xsl:template match="xfm:selectOne[@selection='open']">
      <xsl:call-template name="xfm:label"/>
      <select>
         <xsl:attribute name="onchange">
            <xsl:text>for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i+1]; </xsl:text>
            <xsl:text>; input.value=this.options[this.selectedIndex].value;</xsl:text>
            <xsl:value-of select="@onchange"/>
         </xsl:attribute>
         <xsl:call-template name="xfm:control-atts">
            <xsl:with-param name="onchange"/>
            <xsl:with-param name="value"/>
            <xsl:with-param name="name"/>
         </xsl:call-template>
         <xsl:apply-templates select="xfm:itemset | xfm:item | xfm:choices | htm:option"/>
      </select>
      <input type="text" value="">
         <xsl:variable name="nodeset" select=".//htm:option/@value | .//xfm:value"/>
         <xsl:call-template name="xfm:control-atts">  
<!-- find a width for the field -->  

            <xsl:with-param name="size">
               <xsl:choose>
                  <xsl:when test="@size">
                     <xsl:value-of select="@size"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:call-template name="max-length">
                        <xsl:with-param name="nodeset" select="$nodeset"/>
                     </xsl:call-template>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:with-param>
         </xsl:call-template>
      </input>
   </xsl:template>
   <xsl:template match="xfm:choices">
      <xsl:param name="selected"/>
      <xsl:apply-templates>
         <xsl:with-param name="selected"/>
      </xsl:apply-templates>
   </xsl:template>  
<!-- ======== CHOICES ======== 
    <xsl:template match="xfm:choices">
    <xsl:param name="path"/>
        <optgroup>
            <xsl:apply-templates select="xfm:hint" mode="title"/>
            <xsl:attribute name="label">
                <xsl:apply-templates select="xfm:caption" mode="value"/>
            </xsl:attribute>
            <xsl:apply-templates select="xfm:choices|xfm:item"/>
        </optgroup>
    </xsl:template>
    -->  
  
<!-- ======== ITEM ======== -->  

   <xsl:template match="htm:option">
      <xsl:param name="match">
         <xsl:apply-templates select="(ancestor::xfm:selectOne | ancestor::xfm:selectMany)[1]" mode="getSelect"/>
      </xsl:param>
      <xsl:param name="selected"/>
      <option>
         <xsl:apply-templates select="@*[local-name()!='selected' and local-name()!='value']"/>
         <xsl:choose>
            <xsl:when test="@value">
               <xsl:copy-of select="@value"/>
               <xsl:if test="$match">
                  <o:if xmlns:o="output.xsl" test="{$match}=&#34;{@value}&#34;">
                     <o:attribute name="selected">selected</o:attribute>
                  </o:if>
               </xsl:if>
            </xsl:when>
            <xsl:otherwise>
               <xsl:attribute name="value">
                  <xsl:value-of select="."/>
               </xsl:attribute>
               <xsl:if test="$match">
                  <o:if xmlns:o="output.xsl" test="{$match}=&#34;{.}&#34;">
                     <o:attribute name="selected">selected</o:attribute>
                  </o:if>
               </xsl:if>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:apply-templates/>
      </option>
   </xsl:template>  
<!-- itemset -->  

   <xsl:template match="xfm:itemset">
      <xsl:param name="select">
         <xsl:apply-templates select="(ancestor::xfm:selectOne | ancestor::xfm:selectMany)[1]" mode="getSelect"/>
      </xsl:param>
      <xsl:param name="selected"/>
      <xsl:choose>
         <xsl:when test="$selected='only'">
            <o:variable xmlns:o="output.xsl" name="xfm:b{generate-id()}" select="{$select}"/>
            <o:for-each xmlns:o="output.xsl" select="{@nodeset}">
               <o:sort select="{xfm:caption/@ref}"/>
               <o:if test="$xfm:b{generate-id()}=normalize-space({xfm:value/@ref})">
                  <option>
                     <xsl:attribute name="value">{<xsl:value-of select="xfm:value/@ref"/>}</xsl:attribute>
                     <o:value-of select="{xfm:caption/@ref}"/>
                  </option>
               </o:if>
            </o:for-each>
         </xsl:when>
         <xsl:otherwise>
            <o:variable xmlns:o="output.xsl" name="xfm:a{generate-id()}" select="{$select}"/>
            <o:for-each xmlns:o="output.xsl" select="{@nodeset}">
               <o:sort select="{xfm:caption/@ref}"/>
               <option>
                  <xsl:attribute name="value">{<xsl:value-of select="xfm:value/@ref"/>}</xsl:attribute>
                  <o:if test="$xfm:a{generate-id()}=normalize-space({xfm:value/@ref})">
                     <o:attribute name="selected">selected</o:attribute>
                  </o:if>
                  <o:value-of select="{xfm:caption/@ref}"/>
               </option>
            </o:for-each>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!--
    <xsl:template match="xfm:itemset[ancestor::xfm:selectMany]">
        <xsl:param name="select">
            <xsl:apply-templates select="(ancestor::xfm:selectOne | ancestor::xfm:selectMany)[1]" mode="getSelect"/>
        </xsl:param>
        <o:variable name="xfm:a{generate-id()}" select="{$select}"/>
        <o:for-each select="{@nodeset}">
        <o:sort select="{xfm:caption/@ref}"/>
        <o:variable name="xfm:b{generate-id()}" select="{xfm:caption/@ref}"/>
            <option>
                <xsl:attribute name="value">{<xsl:value-of select="xfm:value/@ref"/>}</xsl:attribute>
                <o:if test="$xfm:a{generate-id()}[.={xfm:value/@ref}]">
                    <o:attribute name="selected">selected</o:attribute>
                </o:if>
                <o:value-of select="{xfm:caption/@ref}"/>
            </option>
        </o:for-each>
    </xsl:template>
-->  
  
<!-- item -->  

   <xsl:template match="xfm:item[1]">
      <xsl:param name="select">
         <xsl:apply-templates select="(ancestor::xfm:selectOne | ancestor::xfm:selectMany)[1]" mode="getSelect"/>
      </xsl:param>
      <xsl:param name="selected"/>
      <xsl:choose>
         <xsl:when test="$selected='only'">
            <o:variable xmlns:o="output.xsl" name="xfm:b{generate-id()}" select="{$select}"/>
            <o:for-each xmlns:o="output.xsl">
               <xsl:attribute name="select">
                  <xsl:text>$messages/xfm:caption[@parent="</xsl:text>
                  <xsl:apply-templates select="(ancestor::xfm:choices | ancestor::xfm:selectOne | ancestor::xfm:selecMany)[1]" mode="getId"/>
                  <xsl:text>"]</xsl:text>
               </xsl:attribute>
               <o:sort/>
               <o:if test="$xfm:b{generate-id()}=@id">
                  <option>
                     <xsl:attribute name="value">{@id}</xsl:attribute>
                     <o:value-of select="."/>
                  </option>
               </o:if>
            </o:for-each>
         </xsl:when>
         <xsl:otherwise>
            <o:variable xmlns:o="output.xsl" name="xfm:a{generate-id()}" select="{$select}"/>
            <o:for-each xmlns:o="output.xsl">
               <xsl:attribute name="select">
                  <xsl:text>$messages/xfm:caption[@parent="</xsl:text>
                  <xsl:apply-templates select="(ancestor::xfm:choices | ancestor::xfm:selectOne | ancestor::xfm:selecMany)[1]" mode="getId"/>
                  <xsl:text>"]</xsl:text>
               </xsl:attribute>
               <o:sort/>
               <option>
                  <xsl:attribute name="value">{@id}</xsl:attribute>
                  <o:if test="$xfm:a{generate-id()}=@id">
                     <o:attribute name="selected">selected</o:attribute>
                  </o:if>
                  <o:value-of select="."/>
               </option>
            </o:for-each>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="xfm:item"/>
   <xsl:template match="xfm:value" mode="attribute">
      <xsl:attribute name="value">
         <xsl:value-of select="normalize-space(text())"/>
      </xsl:attribute>
   </xsl:template>  
<!-- ========== inline LABEL ================ -->  

   <xsl:template name="xfm:label">
      <xsl:param name="id"/>
      <xsl:if test="xfm:caption">
         <label class="xfm_label">
            <xsl:attribute name="class">
               <xsl:choose>
                  <xsl:when test="xfm:caption/@class">
                     <xsl:value-of select="xfm:caption/@class"/>
                  </xsl:when>
                  <xsl:otherwise>xfm_label</xsl:otherwise>
               </xsl:choose>
            </xsl:attribute>
            <o:text xmlns:o="output.xsl">
               <xsl:text disable-output-escaping="yes"> &amp;#160; </xsl:text>
            </o:text>
            <xsl:choose>
               <xsl:when test="$id">
                  <xsl:call-template name="xfm:caption">
                     <xsl:with-param name="id" select="$id"/>
                  </xsl:call-template>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:apply-templates select="xfm:caption"/>
               </xsl:otherwise>
            </xsl:choose>  
<!-- TODO get required from bind -->  

            <xsl:if test="@required">
               <span class="xfm:required" style="color:red">*</span>
            </xsl:if>
            <o:text xmlns:o="output.xsl">
               <xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text>
            </o:text>
         </label>
      </xsl:if>
   </xsl:template>  
<!--
   ======== CONTROL-ATTS ======== 
   -->  

   <xsl:template name="xfm:control-atts">
      <xsl:param name="type">
         <xsl:choose>
            <xsl:when test="name()='xfm:secret'">password</xsl:when>
            <xsl:when test="name()='xfm:upload'">file</xsl:when>
            <xsl:when test="name()='xfm:reset'">reset</xsl:when>
            <xsl:when test="name()='xfm:submit'">submit</xsl:when>
            <xsl:when test="name()='xfm:button'">button</xsl:when>
            <xsl:otherwise>text</xsl:otherwise>
         </xsl:choose>
      </xsl:param>
      <xsl:param name="style" select="@style"/>
      <xsl:param name="size">
         <xsl:choose>
            <xsl:when test="@size">
               <xsl:value-of select="@size"/>
            </xsl:when>
            <xsl:otherwise/>
         </xsl:choose>
      </xsl:param>
      <xsl:param name="class">
         <xsl:if test="@class">
            <xsl:value-of select="concat(@class, ' ')"/>
         </xsl:if>
         <xsl:value-of select="concat('xfm_', local-name())"/>
      </xsl:param>
      <xsl:param name="name">
         <xsl:call-template name="xfm:getName"/>
      </xsl:param>  
<!-- get a select from source -->  

      <xsl:param name="select">
         <xsl:call-template name="xfm:getSelect"/>
      </xsl:param>  
<!-- get a ref to generate -->  

      <xsl:param name="ref">
         <xsl:call-template name="xfm:getRef"/>
      </xsl:param>
      <xsl:param name="id">
         <xsl:choose>  
<!-- store relative path in the id attribute -->  

            <xsl:when test="//xfm:instance/@href and normalize-space($ref)!=''">
               <xsl:value-of select="$ref"/>
            </xsl:when>
            <xsl:when test="@id">
               <xsl:value-of select="@id"/>
            </xsl:when>
         </xsl:choose>
      </xsl:param>  
<!-- ======== events ========== -->  

      <xsl:param name="onblur" select="concat('if (window.xfm_blur) xfm_blur(this); ', @onblur)"/>
      <xsl:param name="onfocus" select="concat('if (window.xfm_focus) xfm_focus(this); ', @onfocus)"/>
      <xsl:param name="onchange">
         <xsl:value-of select="normalize-space(@onchange)"/>
         <xsl:if test="//xfm:instance/@href and normalize-space($ref)!=''">
            <xsl:text> ; setNode(this.ref, this.value)</xsl:text>
         </xsl:if>
      </xsl:param>
      <xsl:param name="onclick" select="@onclick"/>  
<!-- value if an xpath found -->  

      <xsl:param name="value">
         <xsl:choose>  
<!-- avoid copy all when this is used as a template to insert -->  

            <xsl:when test="$select='.'">
               <xsl:text>normalize-space(self::*[name() != ''])</xsl:text>
            </xsl:when>
            <xsl:when test="normalize-space($select)!=''">
               <xsl:text>normalize-space(</xsl:text>
               <xsl:value-of select="$select"/>
               <xsl:text>)</xsl:text>
            </xsl:when>
            <xsl:otherwise/>
         </xsl:choose>
      </xsl:param>  
<!--
-->  

      <xsl:apply-templates select="@tabindex  | @accesskey | @rows | @cols  | @wrap"/>  
<!-- type -->  

      <xsl:if test="$type !=''">
         <xsl:attribute name="type">
            <xsl:value-of select="normalize-space($type)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- onchange -->  

      <xsl:if test="$onchange!=''">
         <xsl:attribute name="onchange">
            <xsl:value-of select="normalize-space($onchange)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- onblur -->  

      <xsl:if test="$onblur!=''">
         <xsl:attribute name="onblur">
            <xsl:value-of select="normalize-space($onblur)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- onfocus -->  

      <xsl:if test="$onfocus!=''">
         <xsl:attribute name="onfocus">
            <xsl:value-of select="normalize-space($onfocus)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- onclick -->  

      <xsl:if test="$onclick!=''">
         <xsl:attribute name="onclick">
            <xsl:value-of select="normalize-space($onclick)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- class -->  

      <xsl:if test="$class != ''">
         <xsl:attribute name="class">
            <xsl:value-of select="normalize-space($class)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- size -->  

      <xsl:if test="$value != ''">
         <xsl:attribute name="size">
            <xsl:value-of select="normalize-space($size)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- style -->  

      <xsl:if test="$value != ''">
         <xsl:attribute name="style">
            <xsl:value-of select="normalize-space($style)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- name -->  

      <xsl:if test="$name != ''">
         <xsl:attribute name="name">
            <xsl:value-of select="normalize-space($name)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- id -->  

      <xsl:if test="$value != ''">
         <xsl:attribute name="id">
            <xsl:value-of select="normalize-space($id)"/>
         </xsl:attribute>
      </xsl:if>  
<!-- value -->  

      <xsl:if test="$value != ''">
         <o:attribute xmlns:o="output.xsl" name="value">
            <o:value-of select="{$value}"/>
         </o:attribute>
      </xsl:if>
   </xsl:template>  
<!-- ======== GETNAME ============ -->  

   <xsl:template name="xfm:getName">
      <xsl:choose>
         <xsl:when test="@htm:name">
            <xsl:value-of select="normalize-space(@htm:name)"/>
         </xsl:when>
         <xsl:when test="@name">
            <xsl:value-of select="normalize-space(@name)"/>
         </xsl:when>
         <xsl:when test="@bind">
            <xsl:value-of select="normalize-space(@bind)"/>
         </xsl:when>
         <xsl:when test="@ref">
            <xsl:value-of select="normalize-space(@ref)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="xfm:getId"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!-- ========= TEXTAREA ========= -->  

   <xsl:template match="xfm:textarea">
      <table border="0" cellpadding="0" cellspacing="0" class="xfm_control">
         <xsl:apply-templates mode="attribute"/>
         <tr>
            <td>
               <xsl:call-template name="xfm:label"/>
            </td>
            <td>
               <textarea>
                  <xsl:call-template name="xfm:control-atts">
                     <xsl:with-param name="value" select="''"/>
                  </xsl:call-template>  
<!-- get value from source -->  

                  <o:apply-templates xmlns:o="output.xsl">
                     <xsl:attribute name="select">
                        <xsl:call-template name="xfm:getSelect"/>
                     </xsl:attribute>
                  </o:apply-templates>
               </textarea>
            </td>
         </tr>
      </table>
   </xsl:template>  
<!-- ========== INPUT ========== -->  

   <xsl:template match="xfm:input">
      <xsl:call-template name="xfm:label"/>
      <input>
         <xsl:call-template name="xfm:control-atts"/>  
<!-- get the validation
                <xsl:apply-templates select="@bind" mode="javascript">
                    <xsl:with-param name="repeat" select="$repeat"/>
                </xsl:apply-templates>
                -->  

      </input>
      <xsl:apply-templates select="xfm:help"/>
   </xsl:template>  
<!-- ========== SECRET ========== -->  

   <xsl:template match="xfm:secret">
      <xsl:call-template name="xfm:label"/>
      <input>
         <xsl:call-template name="xfm:control-atts"/>
      </input>
   </xsl:template>  
<!-- ========== SECRET CONFIRM ======== -->  

   <xsl:template match="xfm:secret[@ui='confirm']">
      <xsl:param name="ref">
         <xsl:call-template name="xfm:getRef"/>
      </xsl:param>
      <span class="xfm_confirm">
         <xsl:call-template name="xfm:label"/>
         <input>
            <xsl:call-template name="xfm:control-atts">
               <xsl:with-param name="name" select="''"/>
               <xsl:with-param name="onchange">
                  <xsl:text>for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i+1]; input.focus();</xsl:text>
               </xsl:with-param>
               <xsl:with-param name="id" select="''"/>
            </xsl:call-template>
         </input>
         <xsl:call-template name="xfm:label">
            <xsl:with-param name="id" select="'confirm'"/>
         </xsl:call-template>
         <input>
            <xsl:call-template name="xfm:control-atts">
               <xsl:with-param name="onchange" select="''"/>
            </xsl:call-template>
            <o:attribute xmlns:o="output.xsl" name="onchange">
               <xsl:text>for (var i=0; this.form.length; i++) if (this.form[i]==this) break; input=this.form[i-1]; </xsl:text>
               <xsl:text>if (this.value != input.value) { alert('</xsl:text>
               <xsl:call-template name="xfm:alert">
                  <xsl:with-param name="id" select="'xfm:secret'"/>
               </xsl:call-template>
               <xsl:text>'); input.focus(); }</xsl:text>
               <xsl:if test="//xfm:instance/@href and (@bind or @ref)">
                  <xsl:text>else { setNode(this.ref, this.value); }</xsl:text>
               </xsl:if>
               <xsl:value-of select="@onchange"/>
               <xsl:if test="//xfm:instance/@href and normalize-space($ref)!=''">
                  <xsl:text> ; setNode(this.ref, this.value)</xsl:text>
               </xsl:if>
            </o:attribute>  
<!-- get value from source -->  
  
<!-- get the validation
                <xsl:apply-templates select="@bind" mode="javascript">
                    <xsl:with-param name="repeat" select="$repeat"/>
                </xsl:apply-templates>
                -->  

         </input>
         <xsl:apply-templates select="xfm:help"/>
      </span>
   </xsl:template>  
<!-- ======== OUTPUT ========= -->  

   <xsl:template match="xfm:output">
      <xsl:apply-templates select="xfm:caption"/>
      <xsl:variable name="select">
         <xsl:call-template name="xfm:getSelect"/>
      </xsl:variable>
      <o:value-of xmlns:o="output.xsl" select="{$select}"/>
   </xsl:template>  
<!--
        <xsl:template match="@required">
        <xsl:variable name="message">
        <xsl:choose>
                <xsl:when test="xfm:alert">
                <xsl:call-template name="escape-js">
                        <xsl:with-param name="string" select="xfm:alert"/>
                </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>Champ requis.</xsl:otherwise>
        </xsl:choose>
        </xsl:variable>
                <xsl:text>if (this.value=='') alert ('</xsl:text><xsl:value-of select="normalize-space($message)"/><xsl:text>'); this.focus(); </xsl:text>
        </xsl:template>
    <xsl:template match="@bind" mode="javascript">
        <xsl:param name="repeat"/>
        <xsl:variable name="bind" select="normalize-space($binds/xfm:bind[@id=current()]/@ref)"/>
        <xsl:variable name="path" select="concat($repeat, substring-after($bind, substring-before($repeat, '[')))"/>
        <xsl:attribute name="onchange">
            <xsl:text>setValueByPath ('</xsl:text>
            <xsl:value-of select="$path"/>
            <xsl:text>', this.value); </xsl:text>
            <xsl:value-of select="../@onchange"/>
        </xsl:attribute>
    </xsl:template>
-->  
  
<!--
                                                                           
     ==================== TOOL TEMPLATES for output ====================== 
                                                                           
-->  
  
<!-- embed templates to the output XSL, especially for displaying messages -->  

   <xsl:template name="templates">
      <xsl:call-template name="comment">
         <xsl:with-param name="name" select="' COMMON TEMPLATES '"/>
         <xsl:with-param name="text">
        short templates to format messages
        known issues : no correct id for a repeated help button 
        then hide/show message can't work
        but is it useful to repeat the same help ?
        </xsl:with-param>
      </xsl:call-template>  
<!-- help for a show/hide html button -->  

      <o:template xmlns:o="output.xsl" match="xfm:help">
         <o:param name="id" select="@id"/>
         <input type="button" class="xfm_help" tabindex="32767" value="?">
            <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
         </input>  
<!-- content of xfm:help considered for an hidden block -->  

         <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
            <o:attribute name="id">
               <o:value-of select="$id"/>_help</o:attribute>
            <tr>
               <td style="width:90%;" class="xfm_bar">
                  <o:value-of select="(caption | title | h1 )[1]"/>
               </td>
               <td style="text-align:right" class="xfm_bar">
                  <input type="button" class="xfm_key" value="X">
                     <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
                  </input>
               </td>
            </tr>
            <tr>
               <td colspan="2" class="xfm_text">
                  <o:apply-templates mode="copy"/>
               </td>
            </tr>
         </table>
      </o:template>
      <o:template xmlns:o="output.xsl" match="xfm:caption" priority="-1">
         <o:apply-templates mode="copy"/>
      </o:template>
      <xsl:comment>
        Escape js string (or other search/replace)</xsl:comment>
      <o:template xmlns:o="output.xsl" match="* | @* | text()" mode="replace" name="xfm:replace">
         <o:param name="string" select="normalize-space(.)"/>
         <o:param name="search">'</o:param>
         <o:param name="replace">\'</o:param>
         <o:choose>
            <o:when test="contains($string, $search)">
               <o:value-of select="concat(substring-before($string, $search), $replace)"/>
               <o:call-template name="xfm:replace">
                  <o:with-param name="string" select="substring-after($string, $search)"/>
                  <o:with-param name="search" select="$search"/>
                  <o:with-param name="replace" select="$replace"/>
               </o:call-template>
            </o:when>
            <o:otherwise>
               <o:value-of select="$string"/>
            </o:otherwise>
         </o:choose>
      </o:template>  
<!-- copy all nodes, avoiding xmlns declaration
        !!! some html tags like <META> will not be closed by saxon in html mode !!! -->  

      <o:template xmlns:o="output.xsl" match="*" priority="-1" mode="copy">
         <o:element>
            <xsl:attribute name="name">{name()}</xsl:attribute>
            <o:apply-templates select="@*" mode="copy"/>
            <o:apply-templates mode="copy"/>
         </o:element>
      </o:template>
      <o:template xmlns:o="output.xsl" match="@*" mode="copy">
         <o:copy/>
      </o:template>  
<!-- displaying messages need to be less templated for performances -->  

   </xsl:template>  
<!--
                                                                 
     ========== HTML HEAD styles and scripts =================== 
                                                                 
-->  
  
<!-- default matching (with breaks on modes) -->  

   <xsl:template name="head">
      <xsl:call-template name="comment">
         <xsl:with-param name="name"> HEAD </xsl:with-param>
      </xsl:call-template>
      <o:template xmlns:o="output.xsl" name="xfm:head">
         <o:call-template name="xfm:style"/>
         <o:call-template name="xfm:script"/>
         <xsl:apply-templates select="//*" mode="head"/>  
<!-- copy user head -->  

         <xsl:apply-templates select="/htm:html/htm:head"/>  
<!-- apply user templates -->  

         <o:apply-templates mode="head"/>
      </o:template>
   </xsl:template>  
<!-- =========== STYLE =================== -->  

   <xsl:template name="style">
      <xsl:call-template name="comment">
         <xsl:with-param name="name">STYLE</xsl:with-param>
      </xsl:call-template>
      <o:template xmlns:o="output.xsl" name="xfm:style">
         <xsl:if test="//htm:style[@type='noNS4']">  
<!-- dynamic CSS classes for critic NS4 behaviors -->  

            <script type="text/javascript">
                style  ='<style type="text/css"> \n';
                if (!document.layers) // styles really buggy for NS4
                {
                    style +='';
                    style +='<xsl:apply-templates select="//htm:style[@type='noNS4']" mode="replace"/> \n';
                }
                style +='</style> \n';
                document.write(style);
                </script>
         </xsl:if>
      </o:template>
   </xsl:template>  
<!--
                                                              
     ========== MESSAGES, apply-templates =================== 
                                                              
-->  
  
<!-- messages become apply-templates in the xsl output (to localise messages ) -->  

   <xsl:template match="xfm:hint"/>
   <xsl:template match="xfm:hint[1]" mode="attribute" name="xfm:title">
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <xsl:attribute name="title">{$messages/xfm:hint[@id="<xsl:value-of select="$id"/>"][1]}</xsl:attribute>
   </xsl:template>
   <xsl:template match="xfm:caption"/>
   <xsl:template match="xfm:caption[1]" name="xfm:caption">
      <xsl:param name="alt-id"/>
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <o:apply-templates xmlns:o="output.xsl">
         <xsl:attribute name="select">  
<!-- the path -->  

            <xsl:text>($messages/xfm:caption[@id="</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>"] </xsl:text>
            <xsl:if test="normalize-space($alt-id) != ''">
               <xsl:text>| $messages/xfm:caption[@id="</xsl:text>
               <xsl:value-of select="normalize-space($alt-id)"/>
               <xsl:text>"] </xsl:text>
            </xsl:if>
            <xsl:text>)[1]</xsl:text>
         </xsl:attribute>
      </o:apply-templates>
   </xsl:template>
   <xsl:template match="node()" mode="xpath" priority="-1"/>
   <xsl:template match="xfm:caption" mode="xpath">
      <xsl:param name="alt-id"/>
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <xsl:text>($messages/xfm:caption[@id="</xsl:text>
      <xsl:value-of select="$id"/>
      <xsl:text>"] </xsl:text>
      <xsl:if test="normalize-space($alt-id) != ''">
         <xsl:text>| $messages/xfm:caption[@id="</xsl:text>
         <xsl:value-of select="normalize-space($alt-id)"/>
         <xsl:text>"] </xsl:text>
      </xsl:if>
      <xsl:text>)[1]</xsl:text>
   </xsl:template>
   <xsl:template match="xfm:alert"/>  
<!-- imported messages are correctly escaped, but for others, call escape template -->  

   <xsl:template match="xfm:alert[1]" name="xfm:alert">
      <xsl:param name="alt-id"/>
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <o:call-template xmlns:o="output.xsl" name="xfm:replace">
         <o:with-param name="string">
            <xsl:attribute name="select">
               <xsl:text>normalize-space(($messages/xfm:alert[@id="</xsl:text>
               <xsl:value-of select="$id"/>
               <xsl:text>"] </xsl:text>
               <xsl:if test="normalize-space($alt-id) != ''">
                  <xsl:text> | $messages/xfm:alert[@id="</xsl:text>
                  <xsl:value-of select="normalize-space($alt-id)"/>
                  <xsl:text>"] </xsl:text>
               </xsl:if>
               <xsl:text>)[1])</xsl:text>
            </xsl:attribute>
         </o:with-param>
      </o:call-template>
   </xsl:template>
   <xsl:template match="xfm:help"/>
   <xsl:template match="xfm:help[1]" name="xfm:help">
      <xsl:param name="id">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <o:apply-templates xmlns:o="output.xsl">
         <xsl:attribute name="select">$messages/xfm:help[@id="<xsl:value-of select="$id"/>"][1]</xsl:attribute>
      </o:apply-templates>
   </xsl:template>  
<!--
                                          
     ========== TOOLS =================== 
                                          
-->  
  
<!--
        rules for a human id for input elements 
    used for 
    could be also used for external file of messages 
    in hope to be the more human and stable (if the source xform changes)
    for future separated files of messages 
    
        ordered rules 
     - @bind attribute
     - @id attribute
     - @name attribute
     - @ref attribute
     - xfm:item/xfm:caption, preceding-sibling xfm:value
     - xfm:message, id of parent xfm:control
     - name and number of element
     -->  

   <xsl:template match="*" name="xfm:getId" mode="getId">
      <xsl:param name="node" select="."/>
      <xsl:choose>  
<!-- identified control -->  

         <xsl:when test="$node/@id">
            <xsl:value-of select="$node/@id"/>
         </xsl:when>
         <xsl:when test="$node/@bind">
            <xsl:value-of select="$node/@bind"/>
         </xsl:when>
         <xsl:when test="$node/@name">
            <xsl:value-of select="$node/@name"/>
         </xsl:when>
         <xsl:when test="$node/@htm:name">
            <xsl:value-of select="$node/@htm:name"/>
         </xsl:when>  
<!--
            <xsl:when test="$node/@ref">
                <xsl:value-of select="$node/@ref"/>
            </xsl:when>
            -->  
  
<!-- if message from select  -->  

         <xsl:when test="name($node/..)='xfm:item'">
            <xsl:value-of select="$node/../xfm:value"/>
         </xsl:when>  
<!-- if message, identify by parent  -->  

         <xsl:when test="name($node)='xfm:caption' or name($node)='xfm:hint' or name($node)='xfm:help' or name($node)='xfm:alert' or name($node)='xfm:message'">
            <xsl:apply-templates select="$node/.." mode="getId"/>
         </xsl:when>  
<!-- otherwise, identify by name and order of element -->  

         <xsl:otherwise>
            <xsl:variable name="name" select="local-name($node)"/>
            <xsl:value-of select="$name"/>
            <xsl:number level="any" count="*[local-name() = $name]"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!--
    get a relative path from an xfm element, for select -->  

   <xsl:template match="*" name="xfm:getSelect" mode="getSelect">
      <xsl:param name="node" select="."/>
      <xsl:choose>
         <xsl:when test="$node/@xsl:select">
            <xsl:value-of select="normalize-space($node/@xsl:select)"/>
         </xsl:when>
         <xsl:when test="$node/@select">
            <xsl:value-of select="normalize-space($node/@select)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:apply-templates select="$node" mode="getRef"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="*" name="xfm:getRef" mode="getRef">
      <xsl:param name="node" select="."/>
      <xsl:choose>
         <xsl:when test="$node/@ref">
            <xsl:value-of select="normalize-space($node/@ref)"/>
         </xsl:when>
         <xsl:when test="$binds[@id=$node/@bind]">
            <xsl:value-of select="normalize-space($binds[@id=$node/@bind]/@ref)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="normalize-space($node/@bind)"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!--
    get an absolute path from an xfm element, especially use for select/match of xslt output -->  

   <xsl:template match="*" name="xfm:getPathAbs" mode="getPathAbs">
      <xsl:param name="node" select="."/>
      <xsl:for-each select="$node/ancestor::*[name()='xfm:repeat' or name()='xfm:group']">
         <xsl:apply-templates select="." mode="getMatch"/>
         <xsl:if test="name()='xfm:repeat'">
            <xsl:text>[</xsl:text>
            <o:number xmlns:o="output.xsl">
               <xsl:attribute name="count">
                  <xsl:for-each select="$node/ancestor-or-self::*[name()='xfm:repeat' or name()='xfm:group']">
                     <xsl:apply-templates select="." mode="getMatch"/>
                     <xsl:if test="position()!=last()">
                        <xsl:text>/</xsl:text>
                     </xsl:if>
                  </xsl:for-each>
               </xsl:attribute>
            </o:number>
            <xsl:text>]</xsl:text>
         </xsl:if>
         <xsl:if test="position()!=last()">
            <xsl:text>/</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:variable name="ref">
         <xsl:apply-templates select="$node" mode="getMatch"/>
      </xsl:variable>  
<!-- path separators need to be strict -->  

      <xsl:if test="$node/ancestor::*[name()='xfm:repeat' or name()='xfm:group'] and $ref != '.'">
         <xsl:text>/</xsl:text>
      </xsl:if>
      <xsl:if test="$ref != '.'">
         <xsl:value-of select="$ref"/>
      </xsl:if>
   </xsl:template>  
<!--
     your own comment format in xsl output -->  

   <xsl:template name="comment">
      <xsl:param name="name">
         <xsl:call-template name="xfm:getId"/>
      </xsl:param>
      <xsl:param name="text"/>
      <xsl:text/>
      <xsl:comment>
         <xsl:text> ====== </xsl:text>
         <xsl:value-of select="$name"/>
         <xsl:text> ====================== </xsl:text>
         <xsl:if test="$text">
            <xsl:text/>
            <xsl:value-of select="$text"/>
            <xsl:text/>
         </xsl:if>
      </xsl:comment>
      <xsl:text/>
   </xsl:template>  
<!-- ========== REPLACE ============= -->  

   <xsl:template match="* | @* | text()" mode="replace" name="replace">
      <xsl:param name="string" select="normalize-space(.)"/>
      <xsl:param name="search">'</xsl:param>
      <xsl:param name="replace">\'</xsl:param>
      <xsl:choose>
         <xsl:when test="contains($string, $search)">
            <xsl:value-of select="concat(substring-before($string, $search), $replace)"/>
            <xsl:call-template name="replace">
               <xsl:with-param name="string" select="substring-after($string, $search)"/>
               <xsl:with-param name="search" select="$search"/>
               <xsl:with-param name="replace" select="$replace"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$string"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!-- find the length of the longer string of a nodeset -->  

   <xsl:template name="max-length">
      <xsl:param name="nodeset" select="*"/>
      <xsl:param name="max-length" select="0"/>
      <xsl:choose>
         <xsl:when test="count($nodeset)=1">
            <xsl:value-of select="$max-length"/>
         </xsl:when>
         <xsl:when test="string-length($nodeset[1]) &gt; $max-length">
            <xsl:call-template name="max-length">
               <xsl:with-param name="nodeset" select="$nodeset[position()&gt;1]"/>
               <xsl:with-param name="max-length" select="string-length($nodeset[1])"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="max-length">
               <xsl:with-param name="nodeset" select="$nodeset[position()&gt;1]"/>
               <xsl:with-param name="max-length" select="$max-length"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>  
<!--
                                                       
     ========== MODES copy and cut =================== 
                                                       
-->  
  
<!--
    <xsl:template match="xsl:template" mode="template">
        <xsl:call-template name="comment">
            <xsl:with-param name="name" select="concat('xsl:TEMPLATE match=&quot;', @match, '&quot; name=&quot;', @name, '&quot; mode=&quot;', @mode, '&quot;'  )"/>
        </xsl:call-template>
        <o:template>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </o:template>
    </xsl:template>
-->  
  
<!--
    <xsl:template match="xfm:*" priority="-0.5"/>
    <xsl:template match="*[namespace-uri()='http://www.w3.org/2002/01/xforms']" priority="-1"/>
-->  
  
<!-- copy supposed to be xsl or html without to much name-space declarations -->  

   <xsl:template match="*" priority="-1">
      <xsl:element name="{name()}">
         <xsl:apply-templates select="@*"/>
         <xsl:apply-templates mode="attribute"/>
         <xsl:apply-templates select="comment()|text()| node()"/>
      </xsl:element>
   </xsl:template>
   <xsl:template match="text()">
      <xsl:if test="normalize-space(.) != ''">
         <xsl:value-of select="."/>
      </xsl:if>
   </xsl:template>
   <xsl:template match="@*">  
<!-- 
    <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
-->  

      <xsl:copy/>
   </xsl:template>
   <xsl:template match="comment()">
      <xsl:text/>
      <xsl:copy/>
      <xsl:text/>
   </xsl:template>
   <xsl:template match="@*[local-name()='schemaLocation']"/>
   <xsl:template match="@*[local-name()='xsi']"/>
   <xsl:template match="text()[normalize-space(.)!=''][name(..)='htm:style' or name(..)='htm:script']" priority="1">
      <o:text xmlns:o="output.xsl" disable-output-escaping="yes">
         <xsl:copy-of select="."/>
      </o:text>
   </xsl:template>  
<!-- mode="action" used to precise the action of a button -->  
  
<!-- TODO, strip it -->  

   <xsl:template match="node()" mode="action"/>  
<!-- mode="getRef", used to get a simple path for the output instance -->  

   <xsl:template match="*|text()" mode="getRef" priority="-1"/>  
<!-- mode="getSelect", used to get an xpath from the input instance -->  

   <xsl:template match="*|text()" mode="getSelect" priority="-1"/>  
<!-- mode="attribute", used to define attributes, especially <xfm:hint/> -->  

   <xsl:template match="*|text()" mode="attribute" priority="-1"/>  
<!-- mode="submit", to build a js xfm_submit() to test controls -->  

   <xsl:template match="*|text()" mode="submit"/>  
<!-- mode="head", used for javascript or other infos in the head of html -->  

   <xsl:template match="*|text()" mode="head"/>  
<!-- mode="template", to create a template in the xsl output -->  

   <xsl:template match="*|text()" mode="template"/>  
<!-- ====== SCRIPT ====================== -->  

   <o:template name="xfm:script">
      <script type="text/javascript" language="javascript">
    
    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, ''); 
        return true;
    }

    function xfm_focus(o) 
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus'; 
        return true;
    }
    

            function xfm_load()
            {
                
                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }

            
            function xfm_submit(form)
            {
                var message="";
                var name="";
                
                if (!message) return true ;
                alert(message);
                return false;
            }
            
            function xfm_reset(form)
            {
            
            }

                </script>
   </o:template>  
<!-- ======  COMMON TEMPLATES  ====================== 
        short templates to format messages
        known issues : no correct id for a repeated help button 
        then hide/show message can't work
        but is it useful to repeat the same help ?
        
-->  

   <o:template match="xfm:help">
      <o:param name="id" select="@id"/>
      <input type="button" class="xfm_help" tabindex="32767" value="?">
         <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
      </input>
      <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
         <o:attribute name="id">
            <o:value-of select="$id"/>_help</o:attribute>
         <tr>
            <td style="width:90%;" class="xfm_bar">
               <o:value-of select="(caption | title | h1 )[1]"/>
            </td>
            <td style="text-align:right" class="xfm_bar">
               <input type="button" class="xfm_key" value="X">
                  <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td colspan="2" class="xfm_text">
               <o:apply-templates mode="copy"/>
            </td>
         </tr>
      </table>
   </o:template>
   <o:template match="xfm:caption" priority="-1">
      <o:apply-templates mode="copy"/>
   </o:template><!--
        Escape js string (or other search/replace)-->
   <o:template match="* | @* | text()" mode="replace" name="xfm:replace">
      <o:param name="string" select="normalize-space(.)"/>
      <o:param name="search">'</o:param>
      <o:param name="replace">\'</o:param>
      <o:choose>
         <o:when test="contains($string, $search)">
            <o:value-of select="concat(substring-before($string, $search), $replace)"/>
            <o:call-template name="xfm:replace">
               <o:with-param name="string" select="substring-after($string, $search)"/>
               <o:with-param name="search" select="$search"/>
               <o:with-param name="replace" select="$replace"/>
            </o:call-template>
         </o:when>
         <o:otherwise>
            <o:value-of select="$string"/>
         </o:otherwise>
      </o:choose>
   </o:template>
   <o:template match="*" priority="-1" mode="copy">
      <o:element name="{name()}">
         <o:apply-templates select="@*" mode="copy"/>
         <o:apply-templates mode="copy"/>
      </o:element>
   </o:template>
   <o:template match="@*" mode="copy">
      <o:copy/>
   </o:template>
   <o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>