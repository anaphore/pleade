//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du formulaire de publication
 * @author Julien Huet
 */
/**
 * @class Gestion du formulaire de publication
 * @author Julien Huet
 * @augments EditFormValidator
 * @augments HelpPanels
 */
var PublicationForm = new Class(/** @lends PublicationForm.prototype */{
	Implements: [HelpPanels,EditFormValidator],
	
	/**
	 * Tabulation YUI du formulaire de publication
	 * @private
	 * @type YAHOO.widget.TabView
	 */
	tabs: null,
	
	/**
	 * Le formulaire de publication
	 * @private
	 * @type Element
	 */
	form: null,
	
	/**
	 * Les suggestions du formulaires
	 * @private
	 * @type AdminSuggest
	 */
	suggest: null,
	
	/**
	 * <strong>Constructor</strong><br/> Initialisation du formulaire de publication
	 * @param {Array} fieldList
	 * @param {Array} indexGlobal
	 * @param {Array} indexLocal
	 */
	initialize: function(fieldList,indexGlobal, indexLocal) {
		this.tabs = new YAHOO.widget.TabView('tabs');
		this.initHelpPanels();
		
		this.suggest = new AdminSuggest();
		this.suggest.initSuggests(fieldList, indexGlobal, indexLocal);
		
		this.form = $('pl-form-button-submit').getParent('form');
		$('pl-form-button-submit').addEvent('click', submitForm);
	},
	
	/**
	 * Validation du formulaire de publication
	 */
	submitForm: function() {
		//alert('PublicationForm.js submitForm');
		var _form = this.form;
		var ret = this.form2work(_form);
		if( ret == true ) {
			//startFragmentLoading( _usersMessages.edit_admin_publication_in_progress, '../' );
			//startPublicationInProgress(null,theme);
			$('pl-form-button-submit-button').disabled = true;
			$('pl-form-button-submit-button').getParent('span.yui-button').addClass('yui-button-disabled yui-push-button-disabled');
			$('pl-form-button-reset-button').disabled = true;
			$('pl-form-button-reset-button').getParent('span.yui-button').addClass('yui-button-disabled yui-push-button-disabled');
			_form.submit();
		}
		return false;
	}
});
