<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!-- Création de la fieldlist finale à partir de celles fournies dans l'élément root -->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
>

	<!-- L'élément racine : on sort un sdx:fieldList, même s'il est incomplet -->
	<xsl:template match="/root">
		<!-- On suppose que la première fieldlist contient les détails -->
		<xsl:apply-templates select="sdx:fieldList[1]">
			<xsl:with-param name="fields">
				<xsl:apply-templates select="sdx:fieldList/node()" mode="copy"/>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	
	<!-- Une fieldList, ce sera la racine, on recopie ses attributs -->
	<xsl:template match="sdx:fieldList">
		<xsl:param name="fields"/>
		<xsl:copy>	
			<xsl:apply-templates select="@*" mode="copy"/>
			<xsl:copy-of select="$fields"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- Un template de copie -->
	<xsl:template match="@*|node()" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" mode="copy"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>

