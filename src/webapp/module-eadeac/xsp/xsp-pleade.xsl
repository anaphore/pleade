<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsp="http://apache.org/xsp"
                xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx">

	<xsl:template match="sdx:page">
		<xsp:logic>
			/**
				Cette methode permet d'étendre une requête de mots à l'aide de nouveaux termes.
				@param q				La requête SDX à étendre
				@param originalTerms	Les termes originaux de la requête, les clés sont celles fournies par Lucene, les valeurs sont des String
				@param newTerms			Les nouveaux termes à ajouter, les clés sont les mêmes que originalTerms, les valeurs sont des String[]
			*/
			public Query expandSimpleQuery(Query q, Hashtable newTerms) throws IOException {
				// On s'assure d'avoir des objets valides
				if ( q == null || newTerms == null ) return q;
				org.apache.lucene.search.Query luceneQuery = q.getLuceneQuery();
				if ( luceneQuery == null ) return q;
				// Maintenant on étend la requête Lucene
				q.setLuceneQuery(expandLuceneQuery(q.getLuceneQuery(), newTerms));
				// On la retourne
				return q;
			}

			public org.apache.lucene.search.Query expandLuceneQuery(org.apache.lucene.search.Query q, Hashtable terms) {
				if ( q instanceof org.apache.lucene.search.BooleanQuery ) {
					// On doit parcourir les différentes clauses et appeler cette méthode de manière récursive
					org.apache.lucene.search.BooleanClause[] clauses = ((org.apache.lucene.search.BooleanQuery)q).getClauses();
					for (int i=0; i&lt;clauses.length; i++) {
						clauses[i].setQuery(expandLuceneQuery(clauses[i].getQuery(), terms));
					}
					return q;
				}
				else if (q instanceof org.apache.lucene.search.TermQuery) {
					// On doit étendre les termes
					String value = ((org.apache.lucene.search.TermQuery)q).getTerm().text();
					String[] values = (String[])terms.get(value);
					if ( values != null ) {
						// On créé une requête booléenne
						org.apache.lucene.search.BooleanQuery bq = new org.apache.lucene.search.BooleanQuery(true);
						// On ajoute un TermQuery sur le terme original
						String field = ((org.apache.lucene.search.TermQuery)q).getTerm().field();
						org.apache.lucene.search.TermQuery tq = new org.apache.lucene.search.TermQuery(new org.apache.lucene.index.Term(field, value));
						bq.add(tq, org.apache.lucene.search.BooleanClause.Occur.SHOULD);
						// On ajoute maintenant les nouveaux termes
						for (int i=0; i&lt;values.length; i++) {
							org.apache.lucene.search.TermQuery ntq = new org.apache.lucene.search.TermQuery(new org.apache.lucene.index.Term(field, values[i]));
							bq.add(ntq, org.apache.lucene.search.BooleanClause.Occur.SHOULD);
						}
						// On retourne la nouvelle requête booléenne
						return bq;
					}
					// Sinon on retourne la requête non modifiée
					else return q;
				}
				else {
					// Pour les autres types de requêtes, on n'étend pas
					return q;
				}
			}
		</xsp:logic>
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
  <!--+
      | Recherche multi-critères
      +-->
  <xsl:template match="sdx:multiCriteriaQuery">
    <xsp:logic>

			int nbMax = 5,	// nombre de resultats maxi autorise
					ksMax = 20;	// nombre de criteres de recherche maxi
			try{
				nbMax = Integer.parseInt( parameters.getParameter("results-max",
																	 request.getParameter("results-max") ) );
			} catch(NumberFormatException e){}
			try{
				ksMax = Integer.parseInt( request.getParameter("keys-max") );
			} catch(NumberFormatException e){
				try{
					ksMax = Integer.parseInt( request.getParameter("keys-max") );
				} catch( NumberFormatException ee ){;}
			}

			// DEBUG System.out.println("[PLEADE][RESULTS] nbMax:"+nbMax+" keysMax:"+ksMax);
			
			String contextWin = parameters.getParameter("context", "main");
			// DEBUG System.out.println("[PLEADE][RESULTS] context:"+contextWin);

			<xsl:call-template name="extendSDXLocations"/>

			<!--===================================================================-->
			<!--           Requete complexe                                        -->
			<!--===================================================================-->
			<!-- Creation de la CQ globale pour les criteres -->
			LinearComplexQueryBuilder sdx_cqGlobale = new LinearComplexQueryBuilder();
			sdx_cqGlobale.enableLogging( sdx_log );
			sdx_cqGlobale.setUp( sdx_locations, Query.OPERATOR_OR );

			String		_champ	= "",
						_champh = "",
								_query	= "",
								_date		= "",
								_from		= "",
								_to			= "",
								_cop		= "",
								_op			= "",
								_sep		=	" \u2022 ", // espace puce (\u2022) espace <!-- TODO: utiliser parameters.getParameter("list-sep", "NULL") [MP] -->
								_du			=	"",
								_db			=	"",
								_de			=	"",
								_thid		= "",
								_threl		= "";
								<!-- FIXME: Comment on récupère l'opérateur complexe du critère de recherche sur les intervalles ? [MP] -->
			String[] _queries;

			int			myOp		= -1,
							nbCrits = 0,
							z				= -1;

			<!-- Boucle sur les n critères de recherche, pour voir combien on en retient -->
			for( z = 1; z &lt;= ksMax; z++ )
			{
				_champ		= request.getParameter("champ"+z);
				_query		= request.getParameter("query"+z);
				_date			= request.getParameter("date"+z);
				_from			= request.getParameter("from"+z);
				_to				= request.getParameter("to"+z);
				_du				=	request.getParameter("du"+z);
				_db				=	request.getParameter("db"+z);
				_de				=	request.getParameter("de"+z);

				if(	Utilities.checkString( _champ )   &amp;&amp;
						(	Utilities.checkString( _query ) ||
						 	Utilities.checkString( _date )  ||
						 	Utilities.checkString( _from )  ||
							Utilities.checkString( _to )		 ||
							Utilities.checkString( _du )		 ||
							Utilities.checkString( _db ) || Utilities.checkString( _de )
						)
					)
					nbCrits++;
			}

			//DEBUG System.out.println("[PLEADE][RESULTS] Nombre de critères : " + nbCrits);

			<!-- Boucle sur les n criteres de recherche -->
			for( z = 1; z &lt;= ksMax; z++ )
			{

				<!-- Retient le champ, chaîne recherchee, l'operateur (de requete) complexe, l'operateur de requete pour la critere courant-->
				_champ		= request.getParameter("champ"+z);
				_champh		= request.getParameter("champh"+z);
				_query		= request.getParameter("query"+z);
				_queries	=	request.getParameterValues("query"+z);
				_date			= request.getParameter("date"+z);
				_from			= request.getParameter("from"+z);
				_to				= request.getParameter("to"+z);
				_du				=	request.getParameter("du"+z);
				_db				=	request.getParameter("db"+z);
				_de				=	request.getParameter("de"+z);
				_cop			= request.getParameter("cop"+z);
				_op				= request.getParameter("op"+z);
				_thid		= request.getParameter("thid"+z);
				_threl		= request.getParameter("threl"+z);


				// DEBUG System.out.println("\n[PLEADE][RESULTS] Traite le "+z+" e critere de requete avec _champ="+_champ+" _champh="+_champh+" _query="+_query+" _du="+_du+" _db="+_db+" _de="+_de+" _op="+_op+" _cop="+_cop);

				<!-- Si la chaîne recherchee n'est pas vide, cela signifie qu'on souhaite effectivement faire une recherche. -->
				if(	Utilities.checkString( _champ )   &amp;&amp;
						(	Utilities.checkString( _query ) ||
						 	Utilities.checkString( _date )  ||
						 	Utilities.checkString( _from )  ||
							Utilities.checkString( _to )		 ||
							Utilities.checkString( _du )		 ||
							Utilities.checkString( _db ) || Utilities.checkString( _de )
						)
					)
				{

					// S'il y a plusieurs criteres, on utilise, si disponible, le _champh
					// TODO: pour les dates?
					if ( nbCrits > 1 &amp;&amp; Utilities.checkString(_champh) ) _champ = _champh;

					sdx_query = null;

					if ( !Utilities.checkString(_du+_db+_de)
							 &amp;&amp; sdx_locations.getField(_champ) == null )
					{
							System.out.println(fr.gouv.culture.sdx.utils.Date.formatDate(new java.util.Date())
																	+" [PLEADE3][RECHERCHE] Le champ `"+_champ+"` n'existe pas pour le sdx_locations courant !");
							// TODO: Meilleure gestion d'erreur
							<xsp:content><message>Le champ <code><xsp:expr>_champ</xsp:expr></code> n'existe pas !</message></xsp:content>
					}

					else
					{

						<!--if ( _op==null || _op.trim().equals("") ) _op = "exact";	// S'il n'y a pas d'operateur intra-champ, il s'agit d'une recherche exacte.-->

						<!-- Calculer le type de la requete : fieldQuery, listQuery, simpleQuery, intervalle de dates à la Pleade -->
						if ( Utilities.checkString( _du ) ||
								 Utilities.checkString( _db ) ||
								   Utilities.checkString( _de )
							 )
						{
							<!-- On fait une requête sur un intervalle ou une date précise à la Pleade
										Pour la recherche de dates: du = date unique, db = date de début, de = date de fin -->
							Date uDate = null, bDate = null, eDate = null;	// Les dates telles que specifiees par l'utilisateur
							Date to1 = null, from1 = null, to2 = null, from2 = null;									// Les dates que l'on va chercher
							boolean withDate = false;												// On va faire une recherche seulement si true

							// DEBUG System.out.println("[PLEADE][RESULTS] Recherche dates a la Pleade...");

							if ( Utilities.checkString( _du ) )
							{
								// On recherche une date "exacte"
								uDate = org.pleade.utils.DateUtilities.parse( _du );
								if ( uDate != null )
								{
									withDate = true;
									from1 = org.pleade.utils.DateUtilities.getMinimumDate();
									to1 = org.pleade.utils.DateUtilities.endOfDatePeriod( uDate, _du );
									from2 = uDate;
									to2 = org.pleade.utils.DateUtilities.getMaximumDate();
								}
							}
							else
							{
								// On recherche un intervalle de dates
								if ( Utilities.checkString( _db ) || Utilities.checkString( _de ) )
								{
									if ( Utilities.checkString( _db ) )
									{
										bDate = org.pleade.utils.DateUtilities.parse( _db );
										if ( bDate != null ) withDate = true;
									}
									if ( Utilities.checkString( _de ) )
									{
										eDate = org.pleade.utils.DateUtilities.parse( _de );
										if ( eDate != null ) withDate = true;
									}
									// On a un intervalle, mais on doit ajuster s'il est ouvert
									if ( eDate == null )
									{
										// Intervalle ouvert vers la droite
										from1 = org.pleade.utils.DateUtilities.getMinimumDate();
										to1 = org.pleade.utils.DateUtilities.getMaximumDate();
										from2 = bDate;
										to2 = to1;
									}
									else if ( bDate == null )
									{
										// Intervalle ouvert vers la gauche
										from1 = org.pleade.utils.DateUtilities.getMinimumDate();
										to1 = org.pleade.utils.DateUtilities.endOfDatePeriod( eDate, _de );
										from2 = org.pleade.utils.DateUtilities.getMinimumDate();
										to2 = org.pleade.utils.DateUtilities.getMaximumDate();
									}
									else
									{
										// Intervale ferme
										from1 = org.pleade.utils.DateUtilities.getMinimumDate();
										to1 = org.pleade.utils.DateUtilities.endOfDatePeriod( eDate, _de );
										from2 = bDate;
										to2 = org.pleade.utils.DateUtilities.getMaximumDate();
									}
								}
							}
							<!-- Maintenant on peut lancer la recherche -->
							if ( withDate )
							{
								// On prepare la requete qui contient les deux dates
								sdx_query = new ComplexQuery();
								sdx_query.enableLogging( sdx_log );
								sdx_op = Query.OPERATOR_AND;	// pour ce critere, l'operateur est toujours un ET
								( (ComplexQuery) sdx_query ).setUp( sdx_locations, sdx_op );

								// Première date
								sdx_bool = true;
								sdx_query2 = new DateIntervalQuery();
								sdx_query2.enableLogging( sdx_log );
								( (DateIntervalQuery) sdx_query2 ).setUp( sdx_locations, "bdate", from1, to1, sdx_bool );
								( (ComplexQuery) sdx_query ).addComponent( sdx_query2 );

								// Deuxième date
								sdx_query2 = new DateIntervalQuery();
								sdx_query2.enableLogging( sdx_log );
								( (DateIntervalQuery) sdx_query2 ).setUp( sdx_locations, "edate", from2, to2, sdx_bool );
								( (ComplexQuery) sdx_query ).addComponent( sdx_query2 );

							}

							// DEBUG else System.out.println("[PLEADE][RESULTS] Pas de quoi faire une recherche de date a la Pleade :: "+withDate);

						}

						else
						{

							<!-- NOTE: Ce calcul se fait suivant le type du champ defini dans la fieldList [mp] -->
							int _queryType = -1;
							if (	sdx_locations!=null &amp;&amp;
										sdx_locations.getFieldType(_champ) > -1
							)	_queryType = sdx_locations.getFieldType(_champ);

							<!-- On traite une listQuery ou une fieldQuery si le champ interroge est de type 'field' -->
							if (	_queryType == fr.gouv.culture.sdx.search.lucene.Field.FIELD )
							{

								<!--	Si on a un separateur (cf. _sep) dans les valeurs,
											ou si on a plusieurs fois le paramètres contenant les
											valeurs à rechercher : on fait une listQuery avec un OU par défaut -->
								_queries = request.getParameterValues( "query"+z );
								String[] _values = null;
								if ( _query.indexOf(_sep) != -1 || _queries.length > 1 ) {
									// On a plusieurs valeurs
									if ( _query.indexOf(_sep) != -1 )
									{
										_values = new String[_query.split(_sep).length];
										_values = _query.split(_sep);
									}
									else _values = _queries;
								}
								else {
									// On a une seule valeur
									_values = new String[1];
									_values[0] = _query;
								}
								// On conserve le nombre de valeurs original
								int _nbValues = _values.length;
								// La requête est toujours complexe
								sdx_query = new ComplexQuery();
								sdx_query.enableLogging(sdx_log);
								// On déterminer l'opérateur, si une seule valeur originale, on prend OR car c'est plus efficace pour le thésaurus
								if ( !Utilities.checkString(_op) || _nbValues &lt; 2 )
										sdx_op = Query.OPERATOR_OR;
								else
										sdx_op = AbstractQuery.getOperator(_op);
								((ComplexQuery)sdx_query).setUp(sdx_locations, sdx_op);

								// On vérifie si on doit aller chercher des termes dans un thesaurus
								if (_thid != null) {
									// Un vecteur pour collecter les termes FIXME: ne pas avoir de doublons serait mieux
									Vector _vvalues = new Vector();
									// On boucle sur les termes recherchés
									for (int _iterm = 0; _iterm &lt; _values.length; _iterm++) {
										// L'URL d'accès à ThesX
										<!-- [JC] 20101118: pour une raison que je ne m'explique pas ; on doit absolument passer de l'ISO ici -->
										String _thQueryUrl = parameters.getParameter("thesx-url") + "functions/" + _thid + "/search.txt?exact=true&amp;format=term&amp;relations=TS&amp;query=" + java.net.URLEncoder.encode(_values[_iterm], "ISO-8859-1");
										// On va chercher les termes dans le thésaurus (un par ligne)
										org.apache.cocoon.environment.Source _q = this.resolver.resolve(_thQueryUrl);
										String _thcontent = org.apache.cocoon.components.language.markup.xsp.XSPUtil.getContents(_q.getInputStream(), "UTF-8");
										// On ajoute le terme courant
										_thcontent = _values[_iterm] + "\n" + _thcontent;
										// On scinde la réponse par lignes.
										String[] _newTerms = _thcontent.split("\n");
										// On stocke ces valeurs dans le vecteur
										_vvalues.add(_newTerms);
									}
									// On a terminé de boucler sur les termes recherchés puis les thésaurus ; on est complet!
									// On a donc un vecteur _vvalues, il contient des objets String[] qui sont des listes de valeurs à chercher
									// Selon l'opérateur sdx_op, on prend tout simplement la longue liste, sinon on doit faire des sous-requêtes
									if ( sdx_op == Query.OPERATOR_OR ) {
										// On doit construire une FieldQuery pour chaque terme
										for (int i=0; i&lt;_vvalues.size(); i++) {
											String[] _v = (String[])_vvalues.get(i);
											for (int j=0; j&lt;_v.length; j++) {
												FieldQuery _tQ = new FieldQuery();
												_tQ.enableLogging(sdx_log);
												_tQ.setUp(sdx_locations, _v[j], _champ);
												((ComplexQuery)sdx_query).addComponent(_tQ);
											}
										}
									}
									else {
										// Un ET, on doit construire une requête complexe par terme
										for (int i=0; i&lt;_vvalues.size(); i++) {
											ComplexQuery _tQ = new ComplexQuery();
											_tQ.enableLogging(sdx_log);
											_tQ.setUp(sdx_locations, Query.OPERATOR_OR);
											String[] _v = (String[])_vvalues.get(i);
											for (int j=0; j&lt;_v.length; j++) {
												FieldQuery _tQF = new FieldQuery();
												_tQF.enableLogging(sdx_log);
												_tQF.setUp(sdx_locations, _v[j], _champ);
												((ComplexQuery)_tQ).addComponent(_tQF);
											}
											// On peut ajouter la première CQ à la globale
											((ComplexQuery)sdx_query).addComponent(_tQ);
										}
									}
								}
								else {
									// Pas de thésaurus, on doit remplir le contenu normalement
									for(sdx_i=0; sdx_i &lt; _values.length; sdx_i++){
										if (sdx_check(_values[sdx_i])){
											sdx_query2=new FieldQuery();
											sdx_query2.enableLogging(sdx_log);
											((FieldQuery)sdx_query2).setUp(sdx_locations, _values[sdx_i], _champ);
											((ComplexQuery)sdx_query).addComponent(sdx_query2);
										}
									}
								}
								//((ComplexQuery)sdx_query).prepare();

								// DEBUG System.out.println("Creation d'une ListQuery");
							}

							// Une recherche de date
							else if (	_queryType == fr.gouv.culture.sdx.search.lucene.Field.DATE )
							{

								if( !Utilities.checkString(_from) &amp;&amp; !Utilities.checkString(_to) )
								{
                  sdx_from = fr.gouv.culture.sdx.utils.Date.parseDate(_date);
                  sdx_to = fr.gouv.culture.sdx.utils.Date.parseUtcISO8601Date(
                              org.pleade.utils.DateUtilities.format(
                                  org.pleade.utils.DateUtilities.endOfDatePeriod( org.pleade.utils.DateUtilities.parse(_date), _date )
                              )
                            );
								}
								else
								{
									// Avec from et/ou to, on fait une requete normale
									sdx_from = fr.gouv.culture.sdx.utils.Date.parseDate(_from);
									sdx_to = fr.gouv.culture.sdx.utils.Date.parseDate(_to);
								}
								sdx_bool = true;
								sdx_query = new DateIntervalQuery();
								sdx_query.enableLogging(sdx_log);
								((DateIntervalQuery)sdx_query).setUp(sdx_locations, _champ, sdx_from, sdx_to, sdx_bool);
								// DEBUG System.out.println("Creation d'une DateQuery: "+sdx_query.getLuceneQuery().toString());

							}

							<!-- Par defaut on cree une requete simple -->
							else
							{

								sdx_query = new SimpleQuery();
								sdx_query.enableLogging(sdx_log);
								sdx_op = AbstractQuery.getOperator(_op);
								if (sdx_op &gt; 0) ((SimpleQuery)sdx_query).setUp(sdx_locations, _champ, _query, sdx_op);
								else ((SimpleQuery)sdx_query).setUp(sdx_locations, _champ, _query);

                if ( _thid != null ) {

                  // DEBUG System.out.println("\n_thid:"+_thid+"\n\t _query:"+_query+"\n\tsdx_query:`"+sdx_query.getLuceneQuery()+"`");

                  // La liste des termes qui devront etendre la requete originale
                  Hashtable _terms = new Hashtable();
                  boolean isExps = false;

                  /* On a une ou des expressions dans la requete :
                   * On va creer une requete temporaire (ie, la requete
                   * originale dans laquelle on aura supprime les expressions
                   * On va recuperera les termes de cette requete temporaire que
                   * l'on jette tout de suite apres. Ces termes seront utilises
                   * pour recuperer les termes d'extension de la requete.*/
                  if ( _query.indexOf("\"") != -1 ) {

                    // DEBUG System.out.println("\tune ou des expressions dans la requete...");

                    isExps = true;
                    java.util.regex.Pattern p = java.util.regex.Pattern.compile("\"([^\"]+)\"");
                    java.util.regex.Matcher m = p.matcher( _query );
                    StringBuffer _nquery = new StringBuffer( _query.length() );
                    String _group = null;
                    int i=0;
                    while( m.find() ) {
                      _group = m.group(1);
                      if ( Utilities.checkString( _group ) ) {
                        i++;
                        m.appendReplacement( _nquery, "");
                      }
                    }
                    m.appendTail( _nquery );
                    // DEBUG System.out.println("\tRequete temporaire :"+_nquery);
                    if ( _nquery != null &amp;&amp; _nquery.length() > 0 ) {
											// on construit une requete temporaire que l'on va soumettre au moteur d'extension
											sdx_query2 = new SimpleQuery();
											sdx_query2.enableLogging(sdx_log);
											if ( sdx_op &gt; 0 ) {
												( (SimpleQuery) sdx_query2 ).setUp( sdx_locations, _champ, _nquery.toString(), sdx_op );
											}
											else {
												( (SimpleQuery) sdx_query2 ).setUp( sdx_locations, _champ, _nquery.toString() );
											}
											// DEBUG System.out.println("\tRequete temporaire: "+sdx_query2.getLuceneQuery());
	
											// on recupere la liste des termes qui etendront la requete originale
											fr.gouv.culture.sdx.utils.lucene.LuceneTools.getTerms( sdx_query2.getLuceneQuery(), _terms, true );
										}

                    sdx_query2 = null; p = null; m = null; _nquery = null; _group = null;

                  }

                  /* Pas d'expression dans la requete :
                   * On recupere la liste des termes recherches, qui est
                   * complete et reutilisable dans les parties de la requete */
                  if ( !isExps ) {
                    fr.gouv.culture.sdx.utils.lucene.LuceneTools.getTerms( sdx_query.getLuceneQuery(), _terms, true );
                  }

									// Pour chaque terme, on utilise le thesaurus pour en avoir d'autres
									Enumeration _keys = _terms.keys();
									Hashtable _newTerms = new Hashtable();
                  String k = null, _term = null, _thQueryUrl = null, _thcontent = null;
                  String[] _newTermsS = null;
                  while ( _keys.hasMoreElements() ) {
										k = (String) _keys.nextElement();
										_term = (String) _terms.get(k);
										// DEBUG System.out.println("\tk=`" + k + "`, t=`" + _term+"`");
										if ( _term != null &amp;&amp; !_term.trim().equals("") ) {
											if ( _threl == null ) _threl = "TS";
											// On appelle thesx
                      <!-- [JC] 20101118: pour une raison que je ne m'explique pas ; on doit absolument passer de l'ISO ici -->
											_thQueryUrl = parameters.getParameter("thesx-url") + "functions/" + _thid + "/search.txt?exact=false&amp;operator=or&amp;format=term&amp;relations=" + _threl + "&amp;query=" + java.net.URLEncoder.encode(_term, "ISO-8859-1");
											// On va chercher les termes dans le thesaurus (un par ligne)
											_thcontent = org.apache.cocoon.components.language.markup.xsp.XSPUtil.getSourceContents( _thQueryUrl, this.resolver );
											// On scinde la reponse par lignes.
											_newTermsS = _thcontent.split("\\n");
											// On conserve la liste
											_newTerms.put(_term, _newTermsS);
										}
									}
                  k = null; _term = null; _thQueryUrl = null; _thcontent = null;
									// On appelle une methode qui permet d'etendre la requete en cours
									sdx_query = expandSimpleQuery(sdx_query, _newTerms);
                  _newTerms = null;

								}

								// DEBUG System.out.println("Creation d'une SimpleQuery sur le champ `"+_champ+"` pour `"+_query+"` avec l'operateur `"+_op+"`");
								// DEBUG System.out.println("SimpleQuery apres analyseur=`"+sdx_query.getLuceneQuery().toString()+"`");

							}

						}

					}

					<!--	On determine l'operateur au sein de la requete complexe.
								Par défaut, c'est un ET. -->
					myOp = Query.OPERATOR_AND;
					if ( _cop==null || _cop.equals("") );
					else if (	_cop.equalsIgnoreCase("or") || _cop.equalsIgnoreCase("ou") )
							myOp = Query.OPERATOR_OR;
					else if ( _cop.equalsIgnoreCase("not") || _cop.equalsIgnoreCase("sauf") )
							myOp = Query.OPERATOR_NOT;

					<!-- On ajoute cette requete au builder -->
					sdx_cqGlobale.addQuery( sdx_query, myOp );

				}

			}


			<!-- Valider la CQ globale en tant que requete courante -->
			if ( sdx_cqGlobale.size() > 0 ){
				sdx_query = new ComplexQuery();
				sdx_query.enableLogging( sdx_log );
				( (ComplexQuery) sdx_query ).setUp( sdx_locations, Query.OPERATOR_AND );
				( (ComplexQuery) sdx_query ).addComponent(sdx_cqGlobale);
				// On ne tient pas compte du critere searchable pour le contexte doc-window
				if ( !contextWin.equals("doc-window") ) {
					sdx_query2 = new FieldQuery();
					sdx_query2.enableLogging(sdx_log);
					((FieldQuery)sdx_query2).setUp(sdx_locations, "1", "searchable");
					( (ComplexQuery) sdx_query ).addComponent(sdx_query2);
				}
			}

    </xsp:logic>
  </xsl:template>

	<xsl:template match="sdx:extendSDXLocations">
		<xsl:call-template name="extendSDXLocations"/>
	</xsl:template>
	<xsl:template name="extendSDXLocations">
		<xsp:logic>
			<!-- Dans quelle base de document, la recherche va-t-elle s'effectuer?
			La définition de la ou des base(s) de recherche s'effectue suivant les priorités suivantes :
				1- Paramètre d'URL (pour les recherches par formulaire)
				2- Paramètre base du sitemap
				3- Paramètre multi-base cdc-searchbase de sitemap
				4- Base de document par défaut, soit ead
			Les priorités 1 et 3 correspondent au fonctionnement par défaut
			-->
			SearchLocations sLocs = new SearchLocations();
			sLocs.enableLogging( sdx_log );
			String base = parameters.getParameter("base", null);
			String cdcBase = parameters.getParameter("cdc-searchbases", "");
			String[] baseParam = request.getParameterValues("base");
			Enumeration bList = sdx_application.getDocumentBasesIds(); <!-- Les bases du fichier application.xconf -->

			<!-- Cas du paramètre d'URL -->
			if ( baseParam!=null ) {
        for ( int i = 0, l = baseParam.length; i&lt;l; i++ ) {
					try{
					sLocs.addDocumentBase(sdx_application.getDocumentBase(baseParam[i]));
					} catch(Exception e){
						sdx_log.error("La base "+baseParam[i]+" n'existe pas.", e);
				}
        }
			}
			<!-- Cas du paramètre base de sitemap -->
      else if ( Utilities.checkString( base ) ) {
				try {
					sLocs.addDocumentBase(sdx_application.getDocumentBase( base ));
				} catch (SDXException sdxe) {
					sdx_base = null;
					base = null;
					sdx_log.error("La base "+base+" n'existe pas.", sdxe);
				}
			}
			<!-- Cas du paramètre cdc-searchbase de sitemap -->
			else if ( cdcBase != null &amp;&amp; !cdcBase.equals("") ) {
					<!-- On vérifie que les bases sont bien dans le application.xconf : si oui, on les ajoute -->
					while ( bList.hasMoreElements() ) {
						String baseItem = (String)bList.nextElement();
						if ( cdcBase.indexOf(baseItem) != -1) {
							sLocs.addDocumentBase(sdx_application.getDocumentBase(baseItem));
						}
					}
				}
			<!-- Maintenant on s'inquiète de construire un sdx_locations qui fonctionne -->
			if(sLocs!=null &amp;&amp; sLocs.size()!=0){
				sdx_locations = sLocs;
			}
			else{<!-- on prend la base par défaut -->
				sdx_locations = new SearchLocations();
				sdx_locations.enableLogging( sdx_log );
				try{
					sdx_locations.addDocumentBase(sdx_application.getDocumentBase(sdx_application.getDefaultDocumentBase().getId()));
				} catch(Exception e){
					sdx_log.error("Probleme lors de la recuperation de la base par defaut : "+e, e);
				}
			}
		</xsp:logic>
	</xsl:template>

	<!--+
      | Recopier les autres balises
      +-->
	<xsl:template match="@* | * | text() | processing-instruction()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@* | * | text() | processing-instruction()" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
