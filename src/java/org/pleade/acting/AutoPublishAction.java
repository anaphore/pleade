/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
 */
package org.pleade.acting;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.acting.ServiceableAction;
import org.apache.cocoon.environment.Redirector;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.excalibur.source.Source;
import org.apache.excalibur.source.SourceNotFoundException;
import org.pleade.utils.DateUtilities;

/**
 * Action qui permet de detecter si on est en mode d'auto-publication et si c'est le
 * cas elle va construire la liste des fichiers a publier.
 *
 * Elle attent les parametres suivants:
 */
public class AutoPublishAction extends ServiceableAction implements Configurable {

	/** Le parametre dont la presence declenche la publication automatique. */
	public static final String PARAMETER_AUTO = "auto";

	/** Le parametre qui indique de tout publier, pas seulement les nouveaux documents. */
	public static final String PARAMETER_ALL = "all";

	/** Le parametre qui indique l'action (ead-publish, eac-publish, ...) */
	public static final String PARAMETER_ACTION = "action";

	/** Le prefixe du nom de fichier qui sera utilise pour stocker la liste des documents a publier. */
	private static final String DOCUMENTS_FILENAME_PREFIX = "autopublish-";

	/** Le suffixe du nom de fichier qui sera utilise pour stocker la liste des documents a publier. */
	private static final String DOCUMENTS_FILENAME_SUFFIX = ".txt";

	/** Le nom du parametre sorti pour le fichier de documents à publier */
	public static final String PARAMETER_DOCUMENTS_FILENAME = "auto-publish-filename";

	/** Le nom du parametre sorti pour la liste des fichiers à publier */
	public static final String PARAMETER_FILE_LIST = "auto-publish-file-list";

	/* Le logger */
	private Logger logger = null;
	private boolean ldebug = false;
	private boolean linfo = false;


	/**
	 * Configuration de l'action, au moment du lancement de Cocoon.
	 * Les parametres de configuration acceptes sont:
	 */
	public void configure(Configuration conf) throws ConfigurationException {
		// Rien a faire pour l'instant
		logger = getLogger();
		ldebug = logger.isDebugEnabled();
		linfo = logger.isInfoEnabled();
	}

	/**
	 * Execution de l'action. Les parametres acceptes sont:
	 *  auto: si "auto", alors on tente une publication automatique, sinon cette action ne fait rien
	 *  auto-all : si true, alors tous les documents seront publies, peu importe leur date
	 *  auto-dir : le dossier qui contient les documents a publier
	 *
	 *  Cette action retourne toujours un resultat positif.
	 */
	public Map act(Redirector redirector, SourceResolver resolver, Map objectModel, String src, Parameters parameters) throws Exception {

		// Si on n'a pas un parametre "auto", alors on ne fait rien
		String auto = parameters.getParameter(PARAMETER_AUTO, null);
		if ( auto == null || auto.trim().equals("") ) return objectModel;

		// Doit-on tout publier ou uniquement les plus recents?
		boolean useDate = !parameters.getParameterAsBoolean(PARAMETER_ALL, false);

		// L'action effectuee
		String action = parameters.getParameter(PARAMETER_ACTION, "ead-publish");

		// La base concernee
		String base = "ead";
		if ( action.indexOf("-") > -1 ) base = action.substring(0, action.indexOf("-"));

		// Le dossier de publication fourni par un parametre "{base}-docs-dir"
		// L'action ne fait rien si on ne trouve pas ce parametre
		File publicationDir = null;
		String p = parameters.getParameter("docs-dir-param", "");
		if ( p!=null && !"".equals(p) ) {
			publicationDir = getPublicationDir(resolver, p);
			if ( publicationDir==null || !publicationDir.exists() || !publicationDir.isDirectory() ) {
				publicationDir = null;
				if(ldebug){
					logger.error("[AUTOPUBLISH] Le dossier "+p+" indique par le parametre d'URL \"docs-dir\" n'est pas un dossier valide.");
				}
			}
		}
		else {
			publicationDir = getPublicationDir(resolver, parameters.getParameter(base + "-docs-dir", ""));
		}
		if ( publicationDir == null ) return objectModel;

		if(linfo){
			logger.info("[AUTOPUBLISH] Publication automatique avec all: "+useDate+" ; base: "+base+" ; dossier a publier: "+publicationDir.toString());
		}

		// La date de debut du traitement
		Date startTime = new Date();

		// Le nom du fichier qui contiendra la liste des documents a publier
		File documentListFile = null;
		if ( publicationDir.canWrite() ) {
			documentListFile = new File(publicationDir, DOCUMENTS_FILENAME_PREFIX + startTime.getTime() + DOCUMENTS_FILENAME_SUFFIX);
		} else {
			documentListFile = new File(System.getProperty("java.io.tmpdir"), DOCUMENTS_FILENAME_PREFIX + startTime.getTime() + DOCUMENTS_FILENAME_SUFFIX);
		}

		// La liste des fichiers en cours de publication
		Hashtable currentDocs = getCurrentDocuments(publicationDir);

		// Les dates de publication des documents, si necessire
		Hashtable publishedDocuments = null;
		if ( useDate ) {
			Source s = resolver.resolveURI(src);
			publishedDocuments = getPublishedDocuments(s);
			resolver.release(s);
		}

		// La liste des fichiers à publier
		DocumentToPublishFilter filter = new DocumentToPublishFilter(currentDocs, publishedDocuments);
		Vector docsToPublish = new Vector();
		buildDocumentList(publicationDir, filter, docsToPublish);

		// On remplit le fichier
		if (ldebug) {
			logger.debug("[AUTOPUBLISH] Ecriture de la liste de(s) "+docsToPublish.size()+" fichier(s) a publier dans: "+documentListFile.getAbsolutePath());
		}
		BufferedWriter w = new BufferedWriter(new FileWriter(documentListFile));
		for (int i=0; i<docsToPublish.size(); i++) {
			w.write(((File)docsToPublish.get(i)).getAbsolutePath() + "\n");
			if(linfo){
				logger.info("[AUTOPUBLISH] Document a publier: "+((File)docsToPublish.get(i)).getAbsolutePath());
			}
		}
		if(linfo && docsToPublish.isEmpty()){
			logger.info("[AUTOPUBLISH] Aucun document a publier dans le dossier : "+publicationDir.getAbsolutePath());
		}
		w.close();

		// On ajoute les deux paramètres produits par cette action
		objectModel.put(PARAMETER_DOCUMENTS_FILENAME, documentListFile.getAbsolutePath());
		objectModel.put(PARAMETER_FILE_LIST, formatFileList(docsToPublish));

		// On retourne toujours quelque chose de positif
		return objectModel;
	}

	/**
	 * Trouve le dossier de publication depuis les parametres qui se terminent par -docs-dir
	 * @param res       Un resolveur Cocoon
	 * @param url       L'URL du dossier
	 * @return          Un File qui correspond au dossier, null si impossible de le trouver
	 */
	private File getPublicationDir(SourceResolver res, String url) {
		if ( url == null || url.trim().equals("") ) return null;
		try {
			File docsDirFile = new File(new URI(res.resolveURI(url).getURI()));
			if (docsDirFile.exists() && docsDirFile.canRead() && docsDirFile.isDirectory() ) return docsDirFile;
		}
		catch ( MalformedURLException e ) {}
		catch ( IOException e ) {}
		catch ( URISyntaxException e ) {}
		return null;
	}

	/**
	 * Retourne la liste des documents qui sont en cours de publication
	 * automatique par un processus tiers.
	 * @param rootDir   Le dossier où se trouvent les fichiers qui listent ces documents
	 * @return  Un tableau avec comme clés le nom complet des fichiers
	 */
	private Hashtable getCurrentDocuments(File rootDir) {
		Hashtable docs = new Hashtable();
		File[] autoFiles = rootDir.listFiles(new AutoPublishFileFilter());
		for (int i=0; i<autoFiles.length; i++) {
			try {
				if(linfo){
					logger.info("[AUTOPUBLISH] Lecture du fichier de publication automatique " + autoFiles[i].getAbsolutePath());
				}
				BufferedReader buf = new BufferedReader(new FileReader(autoFiles[i]));
				String line = null;
				while ((line = buf.readLine()) != null ) {
					docs.put(line, line);
					if(ldebug){
						logger.debug("[AUTOPUBLISH] Le document " + line + " est en cours de publication.");
					}
				}
			}
			catch (FileNotFoundException e ) {}
			catch (IOException e ) {}
		}
		return docs;
	}

	private Hashtable getPublishedDocuments(Source s) {
		Hashtable docs = new Hashtable();
		BufferedReader r = null;
		try {
			r = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String line = null;
			StringTokenizer st = null;
			while ((line = r.readLine()) != null) {
				if(ldebug){
					logger.debug("[AUTOPUBLISH] Date de publication du document " + line);
				}
				st = new StringTokenizer(line, "\t");
				if ( st.countTokens() == 2 ) {
					docs.put(st.nextToken(), DateUtilities.parse(st.nextToken()));
				}
				st = null;
			}
		}
		catch (SourceNotFoundException e ) {}   // TODO: quelque chose ?
		catch (IOException e) {}                // TODO: quelque chose ?
		finally {
			try {
				if ( r!=null) r.close();
			}
			catch (IOException e) {}
		}
		return docs;
	}

	private Vector buildDocumentList(File dir, FileFilter filter, Vector ret) {
		File[] f = dir.listFiles(filter);
		for (int i=0; i<f.length; i++) {
			if ( f[i].isDirectory() ) buildDocumentList(f[i], filter, ret);
			else ret.add(f[i]);
		}
		return ret;
	}

	private String formatFileList(Vector list) {
		StringBuffer ret = new StringBuffer();
		for (int i=0; i<list.size(); i++) {
			if ( i > 0 ) ret.append("\t");
			ret.append(((File)list.get(i)).getAbsolutePath());
		}
		return ret.toString();
	}

	/**
	 * Un filtre pour obtenir les fichiers qui contiennent des listes de documents en cours de publication.
	 */
	private class AutoPublishFileFilter implements FileFilter {
		/**
		 * Retourne vrai si le fichier debute par le prefixe et se termine par le suffixe.
		 */
		public boolean accept(File f) {
			String name = f.getName();
			if (name.startsWith(DOCUMENTS_FILENAME_PREFIX) && name.endsWith(DOCUMENTS_FILENAME_SUFFIX) ) return true;
			else return false;
		}
	}

	/**
	 * Filtre de fichiers pour retenir uniquement des documents a publier.
	 */
	private class DocumentToPublishFilter implements FileFilter {
		/** La liste des documents deja en cours de publication */
		private Hashtable current;

		/** La liste des documents publies, avec leur date, peut etre null */
		private Hashtable published;

		/**
		 * Construit le filtre
		 * @param c     La liste des documents en cours de publication, la cle est le path complet
		 * @param p     La liste des documents deja publies, la cle est l'identifiant, si null alors on ne tient pas compte de ce critere
		 */
		public DocumentToPublishFilter(Hashtable c, Hashtable p) {
			this.current = c;
			this.published = p;
		}

		/**
		 * Retourne vrai si le fichier f doit faire partie de la liste.
		 */
		public boolean accept(File f) {
			// On retient toujours les dossiers pour le faire recursivement
			if (f.isDirectory()) return true;

			// L'identifiant et le chemin du fichier
			String id = f.getName();
			String path = f.getAbsolutePath();

			// On ne retient que les .xml
			if ( id.toLowerCase().endsWith(".xml") ) {
				// On suppose le nom du fichier egal a l'identifiant
				id = id.substring(0, id.toLowerCase().indexOf(".xml"));
				
				// Si deja en cours de publication on ne doit pas le publier de nouveau
				if ( current.get(path) != null ) {
					if (ldebug) logger.debug("[AUTOPUBLISH][KO] Document "+id+" est en cours de publication.");
					return false;
				}

				// Si on ne tient pas compte de la date alors on le publie
				if ( published == null ) {
					if (ldebug) logger.debug("[AUTOPUBLISH][OK] Document "+id+" ; on ne tient pas compte de sa date de publication.");
					return true;
				}

				// S'il est nouveau alors on le publie
				if ( published.get(id) == null ) {
					if (ldebug) logger.debug("[AUTOPUBLISH][OK] Document "+id+" est nouveau.");
					return true;
				}

				// On compare les dates
				long fileTimestamp = f.lastModified();
				if ( published.get(id) != null && ((Date)published.get(id)).getTime() < fileTimestamp ) {
					if (ldebug) logger.debug("[AUTOPUBLISH][OK] Document "+id+" a été modifié depuis sa publication.");
					return true;
				}
				if (ldebug) logger.debug("[AUTOPUBLISH][KO] Document "+id+" n'a pas été modifié depuis sa publication.");
				return false;
			}
			else {
				if (ldebug) logger.debug("[AUTOPUBLISH][KO] Document "+id+" n'a pas d'extension XML.");
				return false;
			}
		}
	}

}
