<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT construisant le  formulaire de recherche personnalisé intégré à la fenêtre EAD, applique les template de search.xsl
		| sur certains éléments uniquement (key@type=text ou key@type=list et key@type=hidden pour l'identifiant de l'IR),
		|	mais pas sur ceux nécessitant du javascript
		| (ce formulaire HTML étant récupéré en ajax, les fonction javascript init() ne sont pas facile à implémenter).
		| catalogue i18n par défaut : module-eadeac
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:h="http://apache.org/cocoon/request/2.0"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml pleade fn">
		
	<!-- Inclusion de la XSL qui formate en HTML les fichiers de configuration des formulaires -->
	<xsl:import href="search.xsl"/>
	
	<!-- Identifiant du document EAD consulté -->
	<xsl:param name="eadid" select="''"/>
	<!-- Liste des IR non cherchables -->
	<xsl:param name="ead-docs-not-searchable" select="''"/>
	<!-- Paramètre qui active ou non l'affichage du formulaire spécifique dans le cas des documents non cherchables -->
	<xsl:param name="ead-docs-not-searchable-specific-form" select="'false'"/>

	<xsl:variable name="form-id" select="concat(/form/@name, '.docwin')"/>

	<xsl:variable name="n-start" select="number(0)"/>
	
	<!-- On affiche le formulaire spécifique que si le document est cherchable -->
	<xsl:template match="/form">
		<xsl:choose>
			<xsl:when test="fn:tokenize($ead-docs-not-searchable, '\s')[. = $eadid] and $ead-docs-not-searchable-specific-form != 'true'">
				<i18n:text key="docs-search.form.not-searchable">ce document n'est pas cherchable.</i18n:text>
			</xsl:when>
			<xsl:otherwise>
				<i18n:text key="docs-search.form.intro.search-form.{$form-id}">docs.search-form.<xsl:value-of select="$form-id"/></i18n:text>
				<!-- L'action par défaut du formulaire est préfixée de docsearch, de façon à séparer l'url de recherche du formulaire standard et l'url de recherche du formulaire EAD -->
				<form id="{$form-id}" class="custom-search-form" action="doc-window/{@action}" method="get" onsubmit="return eadWindow.getContentManager().sendEadForm(this);">
					<table class="custom-search-table"
								 summary="form.table-criteria@summary.search-form.{$form-id}" i18n:attr="summary">
						<xsl:apply-templates select="key[@type='suggest' or @type='date' or @type='text' or @type='hidden' or (@type='list' and (@variant='combo' or @variant='checkbox' or @variant='fixed'))]" mode="criteria">
							<xsl:with-param name="form-id" select="$form-id"/>
							<xsl:with-param name="n-start" select="$n-start"/>
              <xsl:with-param name="mode" select="'eadwindow'"/>
						</xsl:apply-templates>
					</table>
					<input type="submit" value="docs-search.form.submit.search-form.{$form-id}" i18n:attr="value" class="pl-custom-form-submit" id="pl-custom-form-submit"/>
				</form>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*"/>

</xsl:stylesheet>
