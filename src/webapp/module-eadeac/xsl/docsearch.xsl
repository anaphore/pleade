<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: docsearch.xsl 16045 2009-08-26 16:23:30Z jhuet $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT qui permet d'afficher les résultats de recherche pour un document.
		Ces résultats sont produits sous la forme d'un arbre YUI représentant le plan de classement du dit document.
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		exclude-result-prefixes="xsl sdx i18n pleade">

	<!-- Le code pour traiter la navigation d'une page à l'autre -->
	<xsl:import href="navigation.xsl"/>

	<!-- Les fonctions générales de Pleade -->
	<xsl:import href="../../commons/xsl/functions.xsl"/>

	<!-- Ce paramètre indique si on est en train de traiter des résultats pour un terme dans un document ou autre -->
	<xsl:param name="mode"/> <!--peut être vide ou =-term ou =-sdx-->

	<!-- Ce paramètre nous fournit l'eadid -->
	<xsl:param name="eadid" select="/sdx:document/sdx:results/sdx:query[@field='root-id']/@text"/>

	<!-- Activer ou non le panier -->
	<xsl:param name="active-basket" select="'false'" />

	<!-- Si $ead-viewer vaut 'ead-viewer' c'est qu'on est déjà dans la fenêtre EAD -->
	<xsl:param name="ead-viewer" select="''"/>

  <!-- La base de documents utilisées -->
  <xsl:param name="base" select="'ead'" />
  <xsl:variable name="docbase" select="if($base!='') then $base else 'ead'" />
  <xsl:variable name="seperator"><xsl:text disable-output-escaping="yes">&#8226;</xsl:text></xsl:variable>	
	<xsl:variable name="echap"><xsl:text>'</xsl:text></xsl:variable>

	<!-- L'identifiant du div qui contiendra les résultats. Ce div est celui où le TreeView s'affiche. -->
	<xsl:variable name="divid" select="concat('document-results-', $eadid)"/>

	<!-- Une variable pour indiquer si on est dans un résultat de type term -->
	<!-- FIXME: il pourra y avoir d'autres type, donc il faut généraliser -->
	<xsl:variable name="is-term" select="boolean($mode = '-term')"/>

	<xsl:variable name="is-query" select="boolean($mode = '-query')"/>

	<xsl:variable name="qid" select="/sdx:document/sdx:results/@qid"/>

	<!-- Une variable qui indique le type de lien en fonction du fait que ce soit un term ou pas -->
	<xsl:variable name="link-type">
		<xsl:choose>
			<xsl:when test="$is-term">internal</xsl:when>
			<xsl:when test="$is-query">internal</xsl:when>
			<xsl:when test="$ead-viewer = 'ead-viewer'">internal</xsl:when>
			<xsl:otherwise>newWin</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Le séparateur utilisé dans les champs cid et ctitle, entre le numéro et le contenu -->
	<xsl:variable name="sep" select="'-'"/>

	<!-- Un guillemet -->
	<xsl:variable name="q">"</xsl:variable>

  <xsl:variable name="publication-params-url" select="concat('cocoon://functions/ead/get-properties/', $eadid, '/publication.xml')"/>

  <xsl:variable name="show-illustrated">
    <xsl:choose>
      <xsl:when test="doc-available($publication-params-url)">
        <xsl:if test="document($publication-params-url)/properties/property[@name='show-illustrated'] = 'true'">true</xsl:if>
      </xsl:when>
      <xsl:otherwise>false</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

	<!-- On reçoit des résultats de recherche, donc un /sdx:document -->
	<xsl:template match="/sdx:document">
		<!-- On traite uniquement les résultats -->
		<html>
			
			<body>
				<div class="document-results">
					<!-- Pour fermer -->
					<xsl:if test="not($is-term) and not($is-query) and not($ead-viewer = 'ead-viewer')">
						<div class="document-results-close">
							<a i18n:attr="title" title="docsearch.hideresults.title" href="javascript:void(0);" onclick="return cdcResult.displayDocumentResults('{$eadid}');">&#160;</a>
						</div>
					</xsl:if>
					<xsl:apply-templates />
				</div>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="message[@type = 'with-dynamic-data' and @key]">
		<p class="pl-message">
			<xsl:variable name="ik" select="@key"/>
			<xsl:for-each select="tokenize(normalize-space(.), ';')">
				<i18n:text key="{concat($ik, '.', position())}"><xsl:value-of select="concat($ik, '.', position())"/></i18n:text>
				<xsl:value-of select="."/>
				<xsl:if test="position() = last()">
					<i18n:text key="{concat($ik, '.end')}"><xsl:value-of select="concat($ik, '.end')"/></i18n:text>
				</xsl:if>
			</xsl:for-each>
		</p>
	</xsl:template>

	<!-- Les résultats -->
	<xsl:template match="sdx:results">

        <!-- La clé i18n selon singulier / pluriel -->
        <xsl:variable name="key-suffix">
            <xsl:choose>
                <xsl:when test="@nb = 1">one</xsl:when>
                <xsl:otherwise>many</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

		<!-- Le panneau de navigation dans les résultats -->
		<div class="navigation-results">
			<p>
				<span class="nbresults">
                  <xsl:value-of select="@nb"/>
                  <xsl:text> </xsl:text>
                  <i18n:text key="docsearch.results.{$key-suffix}"/>
				</span>
				<xsl:if test="@nbPages > 1">
					<span class="nbpages">
						<i18n:text key="docsearch.page"/>
						<xsl:text> </xsl:text>
						<strong><xsl:value-of select="@page"/></strong>
						<xsl:text> </xsl:text><i18n:text key="docsearch.page.of"/><xsl:text> </xsl:text>
						<xsl:value-of select="@pages"/>
					</span>
					<span class="navpage">
						<xsl:variable name="loadUrl">
							<xsl:choose>
								<xsl:when test="$ead-viewer = 'ead-viewer'">
									<xsl:value-of select="concat('doc-window/', $mode, 'results.ajax')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="concat('docsearch', $mode, '.xsp')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="loadQuery">
							<xsl:choose>
								<xsl:when test="$ead-viewer = 'ead-viewer'">
									<xsl:value-of select="concat('&amp;r=', $eadid, '&amp;mode=ead-viewer')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="concat('&amp;r=', $eadid)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:apply-templates select="." mode="hpp">
              <xsl:with-param name="url" select="$loadUrl" />
              <xsl:with-param name="query" select="$loadQuery" />
              <xsl:with-param name="onclick">
                <xsl:text>windowManager.updateDocumentResults('</xsl:text>
								<xsl:choose>
									<xsl:when test="$ead-viewer = 'ead-viewer'">pl-pg-body-main-mcol</xsl:when>
									<xsl:when test="$is-term">pl-pg-body-main-mcol</xsl:when>
									<xsl:when test="$is-query">pl-pg-body-main-mcol</xsl:when>
									<xsl:otherwise>results-container-<xsl:value-of select="$eadid"/></xsl:otherwise>
								</xsl:choose>
								<xsl:text>', this.href); return false;</xsl:text>
              </xsl:with-param>
						</xsl:apply-templates>
					</span>
				</xsl:if>
                <xsl:if test="$active-basket='true'">
                  <xsl:variable name="docids">
                    <xsl:for-each select="sdx:result">
                      <xsl:value-of select="concat('id=', sdx:field[@name='sdxdocid']/@value)" />
                      <!-- INFO (MP) : On n'envoie pas l'identifiant "URL
                      encoded" car cela peut poser des problemes dans le cas ou
                      cet identifiant possede des espaces et autre caractere
                      interdit en URL.
                      <xsl:value-of select="concat('id=', sdx:field[@name='sdxdocid']/@escapedValue)" />-->
                      <xsl:if test="position()!=last()"><xsl:text>&#38;</xsl:text></xsl:if>
                    </xsl:for-each>
                  </xsl:variable>
                  <span class="bskt-bttn">
                    <button type="button" title="basket.button.add-to-basket.page.title"
                            i18n:attr="title" class="pl-bskt-bttn-page"
                            onclick="basket.basketManager.addPageToBasket('{$docids}', this);"
                            onkeypress="basket.basketManager.addPageToBasket('{$docids}', this);"
                    >
                      <span class="access"><i18n:text key="basket.button.add-to-basket.page.text">
                        ajouter cette page
                      </i18n:text></span>
                    </button>
                    <button type="button" title="basket.button.add-to-basket.results.title"
                            i18n:attr="title" class="pl-bskt-bttn-results"
                            onclick="basket.basketManager.addResultsToBasket('qid={@qid}', this);"
                            onkeypress="basket.basketManager.addResultsToBasket('qid={@qid}', this);"
                    >
                      <span class="access"><i18n:text key="basket.button.add-to-basket.results.text">
                        ajouter tous les résultats
                      </i18n:text></span>
                    </button>
                  </span>
                </xsl:if>
			</p>
		</div>
		
		<xsl:variable name="result">
			<allresult>
			<xsl:apply-templates select="sdx:result" mode="build-results"/>
			</allresult>
		</xsl:variable>
		<!-- Le div, vide mais il sera rempli par MIF -->
		<div id="{$divid}">&#xA0;</div>
		
		<div id="{concat($divid,'-treeRoot')}">
			<ul>
				<xsl:apply-templates select="$result"/>
			</ul>
		</div>
		
		<script type="text/javascript">
			<xsl:value-of select="concat(
				'window.addEvent(',
					$q,'domready',$q,
					',function(){',
					'var searchResult = new ResultTreeView(',$q,$divid,$q,',',$q,$divid,'-treeRoot',$q,');',
					'});'
				)
			"/>
		</script>
	</xsl:template>

	<!-- Traitement du nodeset afin de creer l'arboressence-->
	<xsl:template match="allresult">
		<xsl:param name="level" select="1"/>
		<xsl:param name="parentID"/>
		
		<!-- Dans le cas ou il n'y a pas d'ancetre on creer directement les resultats -->
		<xsl:if test="count(ancestor)=0">
			<xsl:apply-templates select="result[1]" mode="write-result-html"/>
		</xsl:if>
		
		<xsl:for-each-group select="ancestor [@pid = $parentID and @depth=$level]" group-by="@depth">				
			<xsl:for-each-group select="current-group()" group-by="@id">
				<xsl:variable name="currentID" select="@id"/>
				<li id="{current-grouping-key()}">
					<xsl:choose>
						<!-- On test si le pere est egalement un resultat -->
						<xsl:when test="@isResult = 'true'">
							<!-- Si l'ancetre est lui même un resultat alors on va chercher la balise result correspondante et on affiche l'ancetre en tant que resultat -->
							<xsl:apply-templates select="../result/sdx:field[@name='sdxdocid' and @value=$currentID]/.." mode="write-result"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@title"/>
						</xsl:otherwise>
					</xsl:choose>
					<ul>
						<xsl:choose>
							<xsl:when test="../ancestor [@pid = $currentID and @depth=$level+1]">
								<xsl:apply-templates select=".."><!-- /allresult -->
									<xsl:with-param name="level" select="$level+1"/>
									<xsl:with-param name="parentID" select="$currentID"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="../ancestor [@pid = $parentID and @id=$currentID]/following-sibling::result[1]" mode="write-result-html"/>
							</xsl:otherwise>
						</xsl:choose>
					</ul>
				</li>
			</xsl:for-each-group>
		</xsl:for-each-group>
	</xsl:template>
	
	<!--template permettant de gerer laffichage des feuilles des resultats-->
	<xsl:template match="result" mode="write-result-html">
		<li>
			<xsl:apply-templates select="." mode="write-result"/>
		</li>
	</xsl:template>
	
	<xsl:template match="result" mode="write-result">
		<xsl:variable name="eadid" select="$eadid"/>
		<xsl:variable name="id" select="sdx:field[@name='sdxdocid']/@value"/>
		<xsl:variable name='amp'><xsl:text>&amp;</xsl:text></xsl:variable>
		<xsl:variable name="_href" select="
			concat($docbase,'.html?id=',$eadid,$amp,'c=',$id,$amp,'qid=',$qid)
			"/>
		<xsl:variable name="_onclick">
			<xsl:choose>
			<xsl:when test="$link-type eq 'internal'">
				<xsl:value-of select="concat('eadWindow.getContentManager().loadFragment(',$q,$docbase,'-fragment.xsp?c=',$id,$amp,'qid=',$qid,$q,'); return false;')"/>
				</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('return windowManager.winFocus(',$q,$_href,$q,',',$q,'doc',$docbase,$q,')')"/>
			</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- ajout du boutton d'ajout dans le panier il faut tester ici si on a le panier d'activé-->
		<button class="pl-bskt-bttn-doc" title="docsearch.results.add-to-basket" i18n:attr="title" onclick="basket.basketManager.addDocToBasket({concat($q,'id=',$id,$q)}, this);" onkeypress="this.click();">
			<span class="access"><i18n:text key="docsearch.results.add-to-basket"/></span>
		</button>

		<a href="{$_href}" onclick="{$_onclick}">
		<!--	<xsl:apply-templates select="sdx:field[@name='fucomptitle']" mode="hilite"/>-->
			<xsl:apply-templates select="." mode="write-link" />
		</a>
		<xsl:apply-templates select="." mode="write-other"/>
	</xsl:template>

	<xsl:template match="result" mode="write-link">
      <xsl:apply-templates select="." mode="write-icon" />
		<xsl:apply-templates select="." mode="write-unittitle" />
	</xsl:template>
	<xsl:template match="result" mode="write-other">
		<xsl:apply-templates select="." mode="write-unitdate" />
		<xsl:apply-templates select="." mode="write-unitid" />
		<xsl:apply-templates select="." mode="write-pacomm" />
	</xsl:template>
	
    <xsl:template match="result" mode="write-icon">
      <xsl:if test="$show-illustrated">
        <xsl:variable name="objects-types">
          <xsl:for-each select="sdx:field[@name='object']">
            <xsl:value-of select="."/>
            <xsl:if test="position() != last()"><xsl:text>::</xsl:text></xsl:if>
          </xsl:for-each>     
        </xsl:variable>
        <xsl:variable name="arraytypes" select="distinct-values(tokenize($objects-types, '[0-9]+::'))"/>
        <xsl:variable name="types">
            <xsl:if test="count($arraytypes) &gt; 0">
              <xsl:for-each select="$arraytypes">
                <xsl:variable name="classname" select="."/>
                <span class="{concat($classname,'_illustrated')}"><xsl:comment><xsl:copy-of select="."/></xsl:comment></span>
              </xsl:for-each>
            </xsl:if>
        </xsl:variable>
        <xsl:copy-of select="$types"/>
      </xsl:if>
    </xsl:template>

	<xsl:template match="result" mode="write-unittitle">
		<xsl:variable name="title">
			<xsl:apply-templates select="sdx:field[@name='fucomptitle']" mode="hilite"/>
		</xsl:variable>
		<!-- Traitement des balises hilite -->
		<!--<xsl:variable name="titleOut" select="replace($title,'hilite_pretag(\w+)hilite_posttag','\<span\>; $1 \</span \>')"/>-->
		<xsl:copy-of select="$title"/>
	</xsl:template>
	
	<xsl:template match="result" mode="write-unitdate">
		<xsl:variable name="udate" select="normalize-space(sdx:field[@name='udate']/@value)"/>
		<xsl:if test="not($udate eq '')">
			<xsl:value-of select="$seperator"/>
			<span class='cdc-unitdate'>
				<xsl:value-of select="$udate"/>
			</span>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="result" mode="write-unitid">
		<xsl:variable name="uuunitid" select="normalize-space(sdx:field[@name='uunitid']/@value)"/>
			<xsl:if test="not($uuunitid eq '')">
			<xsl:value-of select="$seperator"/>
			<span class='cdc-unitid'>
				<xsl:value-of select="$uuunitid"/>
			</span>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="result" mode="write-pacomm">
		<xsl:variable name="pacomm" select="normalize-space(sdx:field[@name='pacomm']/@value)"/>
			<xsl:if test="not($pacomm eq '')">
			<xsl:value-of select="$seperator"/>
			<span class='cdc-pacomm'>
				<xsl:value-of select="concat('Première année communicable&#xA0;:',$pacomm)"/>
			</span>
		</xsl:if>
	</xsl:template>
	
<!--	<xsl:template match="sdx:hilite">
		<span class="pl-hilite"><xsl:apply-templates /></span>
	</xsl:template>-->
	

	<xsl:template match="text()" mode="hilite">
		<xsl:value-of select="."/>
	</xsl:template>
	<xsl:template match="sdx:hilite" mode="hilite">
		<xsl:choose>
			<xsl:when test="pleade:hilite-string(.)">
				<span class="pl-hilite">
					<xsl:value-of select="."/>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
		<!-- Traitement d'un résultat pour construire l'arbre des résultats temporaires -->

	<xsl:template match="sdx:field">
		<xsl:value-of select="@value"/>
	</xsl:template>
	
	<xsl:template match="sdx:field" mode="malo">
		<xsl:variable name="depth" select="substring-before(@value,$sep)"/>
		<xsl:variable name="next-depth" select="number($depth) + 1"/>
		<xsl:variable name="next-node" select="../sdx:field[@name=current()/@name and starts-with(@value,concat($next-depth, '-'))]"/>
		<anc id="{substring-after(@value,$sep)}" depth="{$depth}">
		<!-- Attention, il faudra bien prendre en compte le traitement des <hilite> pour pouvoir suppimer le niveau du titre -->
			<xsl:apply-templates select="../sdx:field[@name='ctitle' and starts-with(@value, concat($depth,'-'))]" mode="hilite"/>
			<xsl:choose>
				<xsl:when test="$next-node">
					<xsl:apply-templates select="$next-node" mode="malo"/>
				</xsl:when>
				<xsl:otherwise>
					<result>
						<xsl:copy-of select="parent::sdx:result/sdx:field[not(@name=current()/@name)]"/>
					</result>
				</xsl:otherwise>
			</xsl:choose>
		</anc>
	</xsl:template>
	
		<xsl:template match="sdx:result" mode="build-results">
		<!--<xsl:apply-templates select="sdx:field[@name='cid' and starts-with(@value, '1-')]" mode="malo"/>-->
		<!-- On boucle sur les ancêtres, pour voir si on doit en sortir un -->
		<xsl:for-each select="sdx:field[@name='cid']">
			<!-- On trie selon le niveau hiérarchique de l'ancêtre -->
			<xsl:sort select="substring-before(@value, $sep)" data-type="number"/>
			<!-- L'identifient de l'ancêtre -->
			<xsl:variable name="current-id" select="substring-after(@value, $sep)"/>
			<xsl:variable name="parent" select="preceding-sibling::sdx:field[@name='cid'][1]"/>
			<xsl:variable name="parent-id" select="
				if ($parent) then substring-after($parent/@value, $sep)
				else ''
			"/>
			<!-- On sort un élément ancestor s'il n'existe pas déjà dans cette page de résultat -->
			<!--<xsl:if test="not(preceding::sdx:field[@name='cid' and substring-after(@value, $sep) = $current-id] or preceding::sdx:field[@name='sdxdocid' and @value = $current-id])">-->
				<xsl:variable name="no" select="substring-before(@value, $sep)"/>
				<!--<ancestor title="{substring-after(../sdx:field[@name='ctitle' and substring-before(@value, $sep) = $no]/@value, $sep)}" id="{$current-id}" depth="{$no}"/>-->
				<xsl:variable name="title">
					<xsl:apply-templates select="../sdx:field[@name='ctitle' and substring-before(@value, $sep) = $no]" mode="hilite"/>
				</xsl:variable>
				<xsl:variable name="resultID" select="../../sdx:result/sdx:field[@name='sdxdocid' and @value=$current-id]/@value"/>
				<xsl:variable name="isResult" select="if($resultID) then 'true' else ''"/>
				<ancestor id="{$current-id}" pid="{$parent-id}" depth="{$no}" isResult="{$isResult}">
					<xsl:attribute name="title">
						<xsl:value-of select="substring-after($title, $sep)"/>
					</xsl:attribute>
				</ancestor>
				<!-- <xsl:value-of select="../sdx:field[@name='ctitle' and substring-before(@value, $sep) = $no]"/><strong><xsl:text> &gt; </xsl:text></strong> -->
			<!--</xsl:if>-->
		</xsl:for-each>
	
		<!-- Et on sort un résultat -->
		<result depth="{string-length(translate(sdx:field[@name='pos']/@value, '0123456789', '')) + 1}">
			<xsl:copy-of select="sdx:field"/>
		</result>
	</xsl:template>

</xsl:stylesheet>


