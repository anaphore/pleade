//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe implementant la verification du formulaire de recherche avancé
 * @author Julien Huet
 */
/**
 * @class Classe implementant la verification du formulaire de recherche avancé
 * @augments SuggestManager
 * @author Julien Huet
 */
var SearchFormValidator = new Class(/** @lends SearchFormValidator*/{
	Implements: [SuggestManager],
	
	/**
	* strong>Constructor</strong><br/>
	* @param {SearchForm} searchForm
	*/
	initialize: function(searchForm){
		this.searchForm = searchForm;
		// un tableau dans lequel on va consigner les erreurs
		this.arrErrors = new Array();
	},

	/**
	* Travail sur le formulaire de recherche avant de l'envoyer.
	* @param {Element} myForm Le formulaire HTML
	*/
	form2work: function( myForm ) {
		if ( myForm == null || myForm.elements == 0) {
			return false;
		}
		// travail sur les champs this.searchForm.suggests et navindex
		this.checkSuggest();
		this.checkMultipleCombo();
		this.checkTruncate();

		// Initialise le tableau des erreurs au cas ou il ne serait pas vide
		this.arrErrors.empty();

		this.checkFormat();

		this.checkDate();

		this.checkEmptyField(myForm);

		this.checkCdcSearch();

		this.checkAlternateField(myForm);

		// Résultat de la fonction : s'il y a des erreurs on les affiche sans valider, sinon on valide
		if ( this.arrErrors.length > 0 ) {
			this.displayErrors(myForm);
			return false;
		} else {
			return true;
		}
	},

	checkAlternateField: function(myForm){
		// Travail sur les critères "alternate field"
		var altinputs = $(myForm).getElements('input[type=checkbox]');
		altinputs.each(function(cb) {
			if (cb.name.substring(0,3) == 'alt' && cb.checked) {
				// On va seulement modifier le paramètre du champ à chercher
				var no = cb.name.substring(3);
				var fieldId = 'champ' + no;
				if ($(fieldId)) $(fieldId).value = cb.value;
				// Idem pour le champ "h" s'il existe
				fieldId = 'champh' + no;
				if ($(fieldId) && $('h' + cb.name)) {
					$(fieldId).value = $('h' + cb.name);
				}
			}
		});
	},

	/**
	* Verification du cdc du champs de recherche
	*/
	checkCdcSearch: function(){
		// Travail sur le champ "cdc"
		var cdcvalue = '';
		var cdcChecked = '';
		
		if ( this.searchForm.cdcSearch ) {
			cdcChecked = this.searchForm.cdcSearch.getTree().getChecked();

			cdcChecked.each(function(o){
				if ( o.uid != null ) {
					if ( cdcvalue != '' ) {
						cdcvalue += sep;
					}
					cdcvalue += o.uid;
				}
			});
			if ( cdcvalue != '' ) {
				$$('input.pl-cdc')[0].value = cdcvalue;
			}
		}
	},
	
	/**
	* Vérification des champs tronqué
	*/
	checkTruncate: function(){
		// travail sur les champ à tronquer automatiquement
		$$('input.pl-form-truncate-left').each(function(input){
			if ( !input.value.blank() ) {
				input.value = '*' + input.value;
			}
		});
		$$('input.pl-form-truncate-right').each(function(input){
			if ( !input.value.blank() ) {
				input.value = input.value + '*';
			}
		});
		$$('input.pl-form-truncate-both').each(function(input){
			if ( !input.value.blank() ) {
				input.value = '*' + input.value + '*';
			}
		});
	},

	/**
	* Verification des combos multiples 
	*/
	checkMultipleCombo: function(){
		// travail sur les combo multiple
		$$('select.pl-form-slct-multiple-combo').each(function(select){
			var n = select.name.replace(/\query/g, '');
			//var n = select.name.gsub('query','');
			var myValues = $('query'+n+'-values'); // le conteneur de toutes les values
			this.multipleCombo2work(select, myValues);
		}.bind(this));
	},

	/**
	* Verification des suggestions
	*/
	checkSuggest: function(){
		if ( this.searchForm.suggest.isSuggests
			&& this.searchForm.suggest.suggests != null
			&& this.searchForm.suggest.suggests.length > 0
		) {
			$A(this.searchForm.suggest.suggests).clean().each( function(o, i){
				var idi = o + '-input',
				    idv = o + '-values';
				if ( $(idi) && $(idv) ) {
	          			this.suggest2work( $(idi), $(idv) );
				}
			}.bind(this));
		}

		//champs textes qui peuvent obtenir leur valeur depuis un navindex
		$$('.pl-txt-navindexed').each( function(o, i) {
			var idi = o.name + '-input',
			    idv = o.name + '-values';
			if ( $(idi) && $(idv) ) {
				this.suggest2work( $(idi), $(idv) );
			}
		}.bind(this));
	},

	/**
	* Verification si au moins un champs du formulaire est rempli
	*/
	checkEmptyField: function(myForm){
		// Verification que tous les champs ne sont pas vides
		var countEmptyFields = 0;
		var countFields = 0;
		var errMessage = '';
		var errFieldID = '';
		this.getFormElements(myForm, ['input','select']).each(function(el,i){
			var elName = el.get('name');
			if (elName!=null) {
				if (elName.indexOf('query') != -1
					|| elName.indexOf('du') != -1
					|| elName.indexOf('db') != -1
					|| elName.indexOf('de') != -1
					|| elName.indexOf('date') != -1
					|| elName.indexOf('from') != -1
					|| elName.indexOf('to') != -1
				) {
					countFields += 1;
					// un checkbox non coché aura un get('value') = "null"
					if ( el.get('value')=='' || el.get('value') == null ) {
						countEmptyFields += 1;
					}
				}
			}
		});
		if ( countEmptyFields == countFields ) {
			errMessage = _usersMessages.search_form_msg_all_fields_empty;
			errFieldID = '';
			this.addErrorMessage(errMessage, errFieldID);
		}
	},

	/**
	* Recuperation des champs a verifier dans le formulaire
	* @param {Element} myForm Formulaire dans lequel on souhaite travailler
	* @param {Array} eltToGet Tableau de string qui sont les selectors dans le formulaire
	* @return {Array} Tableau contenant les Element du formulaire qu'on doit verifier
	*/
	getFormElements: function(myForm,eltToGet){
		var formElements = new Array();
		eltToGet.each(function(selector){
			formElements.combine($$(myForm).getElements(selector));
		})
		return formElements.flatten();
	},

	/**
	 * Verification du champs date du formulaire
	 */
	checkDate : function(){
		// Verifications sur les dates
		var errMessage = '';
		var errFieldID = '';
		var dbID = '';
		var deID = '';
		var duID = '';
		// on récupère les input de date (au passage on les mets à chaîne vide s'ils contiennent que des espaces)
		$$('input.pl-form-date').each(function(input){
			if ( input.name.match(/db\d/g) ) {
				dbID = input.id;
			}
			if ( input.name.match(/de\d/g) ) {
				deID = input.id;
			}
			if ( input.name.match(/du\d/g) ) {
				duID = input.id;
			}
			if ( input.value.blank() ) {
				input.value = '';
			}
		});

		// Verification db < de (uniquement quand ce sont des années)
		if ( dbID!='' && deID!='' ) {
			var db = $(dbID).get('value') ;
			var de = $(deID).get('value') ;
			// dans le cas ou "db" et "de" sont des années correctement remplies, on verifie l'ordre
			if ( db.match(/^(\d)+$/) && de.match(/^(\d)+$/) ) {
				if ( parseInt(db) > parseInt(de) ){
					errMessage = _usersMessages.search_form_msg_date_unordered;
					errFieldID = dbID + ',' + deID;
					this.addErrorMessage(errMessage, errFieldID);
				}
			}
		}
	},

	/**
	* Verification du format des champs
	*/
	checkFormat: function(){
		// verification du remplissage du formulaire
		var errMessage = '';
		var errFieldID = '';

		// verification des formats (si le champ n'est pas vide)
		$$('a.format').each( function(a,i){
			var formatRegexp =  '^' + a.getAttribute('title') + '$';
			var targetID = a.getAttribute('target');
			var target = $(targetID);
			if ( target!=null ) {
				var value = target.get('value');
				if ( !value.blank() && value.search(formatRegexp) != 0 ) {
					errMessage = '"' + value + '"' + _usersMessages.search_form_msg_format_not_valid + this.getLabel('label' + this.getN(target.name));
					errFieldID = targetID;
					this.addErrorMessage(errMessage, errFieldID);
				}
			}
		}.bind(this));
	},

	/**
	* Verification des comboBox
	* @param {Element} mySelect
	* @param {Object} myValues
	*/
	multipleCombo2work: function(mySelect, myValues) {
		if ( mySelect==null || myValues==null ) {
			return false;
		}
		// le select devient un input pour etre envoyé avec le concatenation des "value"
		var name = mySelect.name;
		/*var myInputString = '<input type="text" name="' + name + '" class="inputFromSelect" />';*/
		inputElt = new Element(
			'input', {
				'type': 'text',
				'name': mySelect.name,
				'class': 'inputFromSelect'
			}
		);
		inputElt.replaces(mySelect);
		var myInput = '';
		myInput = $(myValues).getParent().getElements('input.inputFromSelect');
		
		$(myValues).getElements('span.real-value').each( function(o, i) {
			if ( o.get('text').length > 0 ) {
				var v = o.innerHTML.trim();
				if ( myInput.get('value').indexOf( v ) != -1 ) {
					// la valeur y est deja, on continue
					return;
				} else if ( i!=0 || !myInput.get('value')[0].blank() ) {
					myInput.set('value',myInput.get('value')+sep+v);
				} else {
					myInput.set('value',myInput.get('value')+v);
				}
			}
		});
	},

	/**
	* Ajoute un message d'erreur dans le tableau des erreurs
	* @param {String} message Message d'erreur
	* @param {String} filedID ID du champs ou se trouve l'erreur
	*/
	addErrorMessage: function(message, fieldID){
		this.arrErrors.push( { 'message' : message , 'fieldID' : fieldID } );
	},

	/**
	* Affiche une message d'erreur
	* @param {Element} myForm
	*/
	displayErrors: function(myForm){
		myForm.getChildren().each(function(el){
			el.removeClass('pl-form-error');
		});
		// Pour chaque (nouvelle) erreur, on affiche le message et on mets en surbrillance l'élément
		var errBox = $$('div.pl-form-errors')[0];
		errBox.innerHTML = '';
		this.arrErrors.each(function(error){
			errBox.innerHTML += error.message + '<br/>';
			$A(error.fieldID.split(',')).each(function(fieldID){
				if ( $(fieldID) ) {
					$(fieldID).addClass('pl-form-error');
				}
			});
		});
		errBox.show();
		var p = errBox.getParent('div.pl-form-message');
		if ( p ) {
			p.show();
		}
	},

	/**
	* Recuperation d'un label depuis l'ID d'un element
	* @param {String} id ID de l'element
	* @returns {String}
	*/
	getLabel: function(id){
		if ( $(id) ) {
			var td = $(id);
			return Try.these(
				function(){
					var select = td.down('select');
					return select.options[select.selectedIndex].innerHTML
				},
				function(){
					return td.innerHTML
				}
			)
		}
	},

	getN: function(string){
		return string.match(/\d+/g);
	}
});
