<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="xsl i18n xhtml">

	<!-- On retient le code utilisateur et la page login / logout  -->
	<xsl:param name="code" select="''" />
	<xsl:param name="page" select="''" />

	<!-- Les infos utilisateurs si on les a -->
	<xsl:variable name="userinfos" select="/*/userinfos" />

	<!--	Matche la racine
				Commence l'affichage du formulaire en suivant le modèle -->
	<xsl:template match="login | logout">

<!--
<xsl:message>
[AUTH-FORM] INFOS :
 code = <xsl:value-of select="$code" />
 page = <xsl:value-of select="$page" />
   ID = <xsl:value-of select="$userinfos/authentication/ID" />
roles = <xsl:value-of select="$userinfos/authentication/roles" /></xsl:message>
-->
		<xsl:apply-templates select="template/xhtml:html" mode="template" />

	</xsl:template>
	
	<!-- On ne veut pas réafficher le formulaire d'identification quand l'identification a réussi -->
	<xsl:template match="xhtml:div[@class='pl-form-box' or @class='pl-form-intro']" mode="template">
		<xsl:choose>
			<xsl:when test="$code!='' and $userinfos/authentication/ID">
				<!-- On est connecté : pas besoin du formulaire -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@* | node()" mode="template" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--	Recopie les bouts de code du modèle -->
	<xsl:template match="@* | node()" mode="template" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" mode="template" />
		</xsl:copy>
	</xsl:template>

	<!--	Intercepte l'élément XHTML qui contiendra le message -->
	<xsl:template match="xhtml:*[@id='message']" mode="template">

		<xsl:choose>

			<!-- Pas de message : formulaire vierge (page d'identification et pas de code)  -->
			<xsl:when test="$page!='logout' and $code=''"></xsl:when>

			<xsl:otherwise>

				<xsl:copy>
					<xsl:apply-templates select="@*[local-name()!='id']" mode="template" />
					<xsl:apply-templates select="$userinfos" mode="message" />
				</xsl:copy>

			</xsl:otherwise>

		</xsl:choose>

	</xsl:template>

	<!--	Traitement du message -->
	<xsl:template match="userinfos" mode="message">
		<xsl:choose>
			<xsl:when test="$page='logout'">
				<xsl:choose>
					<xsl:when test="authentication/ID">
						<i18n:text key="logout.result.fail" catalogue="pleade">déconnection ratée</i18n:text>
					</xsl:when>
					<xsl:otherwise>
						<i18n:text key="logout.result.success" catalogue="pleade">déconnexion réussie</i18n:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$code!=''">
				<xsl:choose>
					<xsl:when test="authentication/ID">
						<i18n:text key="login.result.success" catalogue="pleade">identification réussie</i18n:text>
					</xsl:when>
					<xsl:otherwise>
						<i18n:text key="login.result.fail" catalogue="pleade">identification ratée</i18n:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
