<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx">
	<xsl:template match="sdx:user">
		<sdx:document id="{@id}">
			<sdx:field code="firstname">
				<xsl:value-of select="@firstname"/>
			</sdx:field>
			<sdx:field code="lastname">
				<xsl:value-of select="@lastname"/>
			</sdx:field>
			<sdx:field code="lang">
				<xsl:value-of select="@xml:lang"/>
			</sdx:field>
			<sdx:field code="variant">
				<xsl:value-of select="@variant"/>
			</sdx:field>
			<sdx:field code="email">
				<xsl:value-of select="@email"/>
			</sdx:field>
		</sdx:document>
	</xsl:template>
	<xsl:template match="sdx:group">
		<sdx:document id="{@id}">
			<sdx:field code="description">
				<xsl:apply-templates mode="fulltext"/>
			</sdx:field>
		</sdx:document>
	</xsl:template>
	<!-- a very simple template, puts a space between all elements
	and cut extra spaces and break-lines  -->
	<xsl:template match="*" mode="fulltext">
		<xsl:apply-templates select="*|text()" mode="fulltext"/>
	</xsl:template>
	<xsl:template match="text()" mode="fulltext">
		<xsl:value-of select="normalize-space(.)"/>
		<xsl:if test="normalize-space(.) !=''">
			<xsl:text>&#32;</xsl:text>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
