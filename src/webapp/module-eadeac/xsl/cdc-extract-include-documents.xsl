<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		Cette XSLT complète la sortie d'un cadre de classement avec extraction
		d'une entrée et de ses enfants, en ajoutant les résultats de recherche
		qui permettent d'avoir les documents associés à une entrée.

		Elle recopie tout sauf les sdx:results, qu'elle formate adéquatement.
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	exclude-result-prefixes="xsl sdx">


	<!-- On s'intéresse uniquement à chaque résultat -->
	<xsl:template match="sdx:results">
		<xsl:apply-templates select="sdx:result"/>
	</xsl:template>

	<!-- Un résultat est résumé comme une entrée du cadre de classement -->
	<xsl:template match="sdx:result">
		<!-- On sort un élément c -->
		<c pleade:id="{sdx:field[@name='sdxdocid']/@value}" pleade:type="document" hasChildren="false" nbdocs="0" nbresults="{@nbresults}">
			<xsl:attribute name="pleade:base">
				<xsl:value-of select="sdx:field[@name = 'sdxdbid']"/>
			</xsl:attribute>
      <!-- Indique si un enfant de l'élément est illustré -->
      <xsl:if test="sdx:field[@name='ancobject']">
        <xsl:attribute name="illustratedChild">true</xsl:attribute>
      </xsl:if>
			<!-- Des informations sur le document -->
			<did>
				<xsl:for-each select="sdx:field[@name='fatitle' and @value!='']"><!-- On peut avoir plusieurs fatitle -->
					<unittitle><xsl:value-of select="." /></unittitle>
				</xsl:for-each>
				<xsl:if test="sdx:field[@name = 'uunitid']"><unitid><xsl:value-of select="sdx:field[@name = 'uunitid']"/></unitid></xsl:if>
				<xsl:if test="sdx:field[@name = 'udate']"><unitdate><xsl:value-of select="sdx:field[@name = 'udate']"/></unitdate></xsl:if>
			</did>
		</c>
	</xsl:template>

	<!-- De manière générale, on recopie les éléments -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
