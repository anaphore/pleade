<?xml version="1.0"?>
<?xml-stylesheet href="../../../_config/docbook.css" type="text/css"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "../../../_config/docbookx.dtd">
<article lang="fr"> 
  <title>Descripteurs (termes
	 d'indexation)</title> 
  <titleabbrev>Descripteurs</titleabbrev> 
  <articleinfo> 
	 <author><firstname>Florence</firstname><surname>Clavaud</surname>
		
		<affiliation> 
		  <orgname>Anaphore</orgname> 
		</affiliation> 
		<email>florence.clavaud@arkheia.net</email>
		
	 </author> 
	 <abstract> 
		<para>Description des paramètres liés aux
		  descripteurs ou termes d'indexation, que ce soit
		  pour un usage interne au document ou dans
		  l'ensemble de l'installation.</para> 
	 </abstract><date
	 role="modification">2006-02-18</date> 
  </articleinfo> 
  <para>Les deux sections relatives aux
	 descripteurs proposent toutes les deux les mêmes
	 options. Elles ont cependant un objectif
	 différent. <emphasis role="bold">La première
	 section concerne l'indexation des descripteurs
	 pour leur usage interne au document
	 EAD</emphasis> (les catégories de descripteurs
	 retenues formeront des index propres à
	 l'instrument de recherche, interrogeables dans le
	 cadre de gauche de la page Web présentant cet
	 instrument de recherche et dans le corps de
	 l'instrument de recherche lui-même).
	 <emphasis role="bold">La deuxième section
	 concerne l'indexation des descripteurs dans les
	 index cumulatifs de l'application</emphasis>,
	 afin de rendre ces descripteurs utilisables dans
	 les formulaires de recherche avancée qui servent
	 à explorer l'ensemble (ou un sous-ensemble)
	 d'instruments de recherche.</para> 
  <para>On pourra donc retenir dans la deuxième
	 section des catégories de descripteurs
	 différentes de celles retenues dans la première.
	 De fait, certaines listes de descripteurs peuvent
	 n'avoir qu'un intérêt très limité au sein d'un
	 instrument de recherche (notamment si elles sont
	 courtes, que les occurrences d'un descripteur
	 donné sont rares, et/ou que les descripteurs ne
	 couvrent qu'une partie du document EAD) alors que
	 le contenu des mêmes listes viendra avec profit
	 se cumuler avec les valeurs fournies par d'autres
	 instruments de recherche pour former des index
	 globaux. Même partiels, ces index peuvent
	 s'avérer des outils de recherche
	 performants.</para> 
  <para>Pour sélectionner une catégorie de
	 descripteur dans la liste donnée par la section,
	 il suffit de cliquer sur la valeur choisie dans
	 la liste. Pour choisir une deuxième ou une énième
	 catégorie dans la même liste, appuyer sur la
	 touche majuscule du clavier, la garder enfoncée
	 et cliquer sur la deuxième ou enième valeur à
	 retenir.</para> 
  <para>Si on coche la case intitulée
	 <emphasis role="italic">Inclure les descripteurs
	 hérités des unités parent</emphasis> dans ces
	 deux sections, le logiciel va indexer, pour une
	 unité documentaire donnée, non seulement les
	 descripteurs qu'il rencontrera dans cette unité
	 même, mais les descripteurs situés dans les
	 éléments <sgmltag class="element">c</sgmltag>
	 père et ancêtres de l'unité documentaire. Cette
	 option peut être très utile lorsqu'on a pratiqué
	 une indexation "par niveaux de description", que
	 le document EAD est profond et que les éléments
	 d'indexation sont répartis sur l'ensemble des
	 niveaux hiérarchiques. Mais elle est inutile si
	 tous les éléments d'indexation se situent dans
	 les composants <sgmltag
	 class="element">c</sgmltag>d'un même niveau
	 hiérarchique : il n'y aura pas d'élément
	 d'indexation à faire hériter.</para> 
  <para>A la fin des deux listes de descripteurs
	 qui apparaissent, figurent trois lignes
	 concernant des descripteurs particuliers
	 susceptibles d'être présents dans les instruments
	 de recherche en XML/EAD produits avec le logiciel
	 Aide au classement : 
	 <orderedlist> 
		<listitem> 
		  <para>l'index des grandes catégories de
			 documents, qui sera produit par PLEADE à partir
			 de l'élément <sgmltag
			 class="element">&lt;genreform&gt;</sgmltag>
			 correspondant au champ Type de document d'Aide au
			 classement (et doté d'un attribut
			 <sgmltag class="attribute">source</sgmltag> de
			 valeur <sgmltag
			 class="attvalue">liste-typedocAC</sgmltag>)
			 ;</para> 
		</listitem> 
		<listitem> 
		  <para>l'index des bâtiments, qui sera
			 produit par PLEADE à partir de l'élément
			 <sgmltag
			 class="element">&lt;geogname&gt;</sgmltag>
			 correspondant au champ Bâtiments dans la grille
			 d'indexation d'Aide au classement (et doté d'un
			 attribut <sgmltag
			 class="attribute">role</sgmltag> de valeur
			 <sgmltag class="attvalue">batiment</sgmltag>)
			 ;</para> 
		</listitem> 
		<listitem> 
		  <para>l'index des auteurs, qui sera
			 produit par PLEADE à partir des éléments Nom de
			 personne <sgmltag
			 class="element">&lt;persname&gt;</sgmltag> et
			 Collectivité <sgmltag
			 class="element">&lt;corpname&gt;</sgmltag>
			 correspondant à l'indexation des auteurs dans les
			 grilles ISBD d'Aide au classement (et dotés d'un
			 attribut <sgmltag
			 class="attribute">role</sgmltag> de valeur
			 <sgmltag
			  class="attvalue">auteur</sgmltag>).</para> 
		</listitem> 
	 </orderedlist> </para> 
  <para>Il faut souligner ici que ces
	 descripteurs peuvent être supprimés des listes
	 présentées dans le formulaire de publication. Il
	 suffit pour cela de modifier le fichier
	 <filename>pleade-local\local.xconf</filename> du
	 logiciel (supprimer la déclaration de ces
	 descripteurs dans les sections index/specific et
	 index/global de ce fichier). </para> 
  <para>D'autre part, il est possible d'ajouter
	 de nouveaux descripteurs à ces listes. Attention,
	 leur simple déclaration dans le fichier<filename>
	 pleade-local\local.xconf</filename> ne suffit
	 pas. Il faudra aussi créer des champs
	 d'indexation et peut-être compléter les
	 programmes XSLT qui s'occupent de l'indexation
	 dans PLEADE. Actuellement, ces adaptations sont
	 très faciles à réaliser pour prendre en compte de
	 façon spécifique les éléments d'indexation dotés
	 d'un attribut <sgmltag
	 class="attribute">role</sgmltag> ou d'un attribut
	 <sgmltag class="attribute">source</sgmltag> (le
	 code XSLT existe déjà). Cet aspect sera documenté
	 plus précisément ultérieurement.</para> 
  <para>Enfin, rappelons ici que PLEADE indexe
	 automatiquement, sans que le responsable de la
	 publication n'ait à faire quoi que ce soit, la
	 plupart des éléments XML/EAD dans des champs
	 spécifiques, ce qui permet de concevoir des
	 logiques et des formulaires de recherche très
	 précis. Dans la version 2 de PLEADE, l'indexation
	 des éléments Origine <sgmltag
	 class="element">&lt;origination&gt;</sgmltag> et
	 Organisme responsable de l'accès intellectuel
	 <sgmltag
	 class="element">&lt;repository&gt;</sgmltag>,
	 qu'ils existent ou non dans l'unité documentaire
	 à indexer, se fait avec héritage des valeurs des
	 unités documentaires parentes. On peut aussi
	 choisir d'indexer les intitulés et dates en
	 héritant les informations trouvées dans les
	 unités parentes, ou non.</para> 
</article> 
