//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Retourne différentes dimensions d'élements<br/> Permet de définir sio l'on souhaite inclure les marges, ...
 * @author Julien Huet
 */
/**
* @class SuggestManager Gestion des suggestions dans le formulaire de recherche avancé et aussi dans la partie admin
* @author jhuet
*/
var SuggestManager = new Class(/** @lends SuggestManager.prototype */{
	
	/**
	* Inialisation du gestionnaire de suggestion
	*/
	initialize: function(){
	},
	
	/**
	* @param {Element} elt Element dans lequel on souhaite ajouter la suggestion
	* @param {String} value La valeur de la suggestion
	* @param {function} otherActionFunction Une autre action a exectuer lorsque l'on clique sur la suggestion. Par deaut on la supprime
	*/
	addSuggest: function(elt, text, value, otherActionFunction){
		if ( !otherActionFunction ) {
			otherActionFunction = $empty;
		}
		var divContainer = new Element('div');
		divContainer.addClass('pl-form-suggest-value');
		var link = new Element('a', {href: '#'});
		link.set('text',text);
		link.addEvent('click', this.removeSuggest);
		link.addEvent('click', otherActionFunction);

		var span = new Element('span');
		span.addClass('real-value');
		span.addClass('access');
		span.set('text',value);

		link.adopt(span);
		divContainer.adopt(link);

		elt.adopt(divContainer);
	},

	/**
	* Suppression d'une suggestion
	*/
	removeSuggest: function(){
		this.parentNode.dispose();
	},

	/**
	* Valider les selections de l'utilisateur dans les criteres de type "suggest".
	* On recoit l'INPUT et le bloc conteneur des valeurs a y inscrire.
	* Ce dernier bloc contient des SPAN. On boucle dessus et on ne conserve que le
	* le texte
	* @param {Element} myInput Input dans lequel sera inscrit les valeurs des suggest
	* @param {Element} myValues Element contenant les span qui correspondent aux valeurs des suggestions
	*/
	suggest2work: function( myInput, myValues ) {
		if( myInput==null || myValues==null ) {
			return false;
		}
		$(myValues).getElements('a').each( function(o, i) {
				if ( o.getElement('span.real-value') ) {
					o = o.getElement('span.real-value');
				}
				if( o.get('text') ){
				//var v = o.innerHTML.stripTags().trim(); 
				var v = o.get('text').trim(); // Test avec un get('text') pour ne pas incluure String.Extras de mootools
				if ( myInput.value.indexOf( v ) != -1 ) {
					// la valeur y est deja, on continue
					return;
				}  else if ( i!=0 || !myInput.get('value').blank() ) {
					myInput.value += sep + v;
				} else {
					myInput.value += v;
				}
			}
		});
	}
});
