//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer l'arbre du cadre de classement
 * @author Julien Huet
 */
/**
 * @class Classe permettant de créer l'arbre du cadre de classement
 * @augments TreeView
 * @augments AjaxRequestHandler
 * @author Julien Huet
 */
var CdcTreeView = new Class(/** @lends CdcTreeView */{
	Extends: TreeView,
	Implements: [AjaxRequestHandler],
	
	/**
	* <strong>Constructor</strong><br/> Constructeur de l'arbre CDC
	* @param {Mixed} autoExpand Definit la profondeur du extend au chargement de l'arbre.
	*/
	initialize: function(autoExpand){
		// Le cdcPartId est facultatif; s'il est fourni, et si sa valeur
		// est non null et non vide, alors on débute l'affichage sur l'entrée du
		// cadre de classement qui correspond à cet id. Il est normalement en
		// variable globale de la page HTML.
		var query = '';
		if ( cdcPartId && cdcPartId != '' ) query = '?id=' + cdcPartId;

		var url = 'functions/ead/cdc.ajax-html' + query;
		this.parent(url, $('_cdc_'), null, null);

		this.noTreeMessage = 'Le cadre de classement est vide'; //TODO: i18n - check

		// On redefini la fonction permettant de recuperer le lien pour charger les enfants
		this.getTree().getChildrenUrl = function(node){
			return node.getDataHref();
		}

		if ( autoExpand == '_all' ) {
			//on active l'auto expand
			this.enableExpandAllOnLoad();
		} else {
			this.enableExpandAllOnLoad(autoExpand.toInt());
		}
	},

	/**
	* Surcharge de la fonction de selection du noeud
	* @param {Mif.Tree.Node} node
	*/
	onSelect: function(node){
		var wrapper=node.getDOM('wrapper');
		wrapper.removeClass('mif-tree-node-selected');
	},
	
	/**
	* Affiche une rangée de tableau pour donner plus d'information sur
	* une entrée de cadre de classement.
	* @param {String} tableId L'identifiant du tableau qui contient l'entrée
	* @param {String} cdcId   L'identifiant de l'entrée du cadre de classement
	*/
	displayMoreinfo: function(tableId, cdcId) {
		// Le tableau
		var t = $(tableId).getParent();
		// L'identifiant de la rangée pour pouvoir la fermer ensuite
		var rowId = tableId + '-moreinfo';
		var newR = $(rowId);
		// Le span et son lien
		var sp = $(tableId + '-moreinfo-span');
		var a = sp.getFirst('a');
	
		if ( newR ) {
			// On a déjà construit la rangée, on la cache ou rend visible
			newR.toggle();
			if ( newR.getStyle('display') != 'none' ) {
				// Ajustement du lien
				t.addClass('cdc-exit');
				t.removeClass('cdc-entry');
				sp.addClass('lessinfo');
				sp.removeClass('moreinfo');
				a.setAttribute('title', _usersMessages.cdc_moreinfo_close_title);
				a.innerHTML = _usersMessages.cdc_moreinfo_close_text;
			} else {
				// Ajustement du lien
				t.addClass('cdc-entry');
				t.removeClass('cdc-exit');
				sp.addClass('moreinfo');
				sp.removeClass('lessinfo');
				a.setAttribute('title', _usersMessages.cdc_moreinfo_title);
				a.innerHTML = _usersMessages.cdc_moreinfo_text;
			}
		} else {
			// On n'a pas la rangée, on la construit
			var div = new Element('div', {id: rowId});
			t.getParent().adopt(div);
			// On va chercher le contenu
			cdcTreeView.loadAjaxContent('functions/ead/cdc-moreinfo.html?id=' + cdcId,div);
			// On ajuste le lien
			t.addClass('cdc-exit');
			t.removeClass('cdc-entry');
			sp.addClass('lessinfo');
			sp.removeClass('moreinfo');
			a.setAttribute('title', _usersMessages.cdc_moreinfo_close_title);
			a.innerHTML = _usersMessages.cdc_moreinfo_close_text;
		}
		return false;
	}
});