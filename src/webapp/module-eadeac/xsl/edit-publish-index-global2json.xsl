<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!--+
		|	Dresse la liste des index globaux à partir du fichier module-eadeac/conf/ead-index.xconf
		| Prépare la liste pour être convenablement transformée en JSON
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		exclude-result-prefixes="xsl i18n">

	<xsl:param name="query" select="''" />

	<xsl:template match="/index-list">
		<root_root>
		<root array="true">
			<xsl:choose>
				<xsl:when test="$query!=''">
					<xsl:for-each select="index[@global-sdxfield and starts-with(@id, $query)]">
						<xsl:apply-templates select="@id" mode="out" />
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<o>
						<v>_all</v>
						<!--<t>
							<i18n:text key="edit.publish.index-global._all">
								<xsl:text>tous</xsl:text>
							</i18n:text>
						</t>-->
					</o>
					<xsl:for-each select="index[@global-sdxfield and @id!='']">
						<xsl:apply-templates select="@id" mode="out" />
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</root>
		</root_root>
	</xsl:template>

	<xsl:template match="@*|*" mode="out">
		<xsl:variable name="v" select="normalize-space(.)" />
		<o>
			<v><xsl:value-of select="$v" /></v>
			<!--<t>
				<i18n:text key="edit.publish.index-global.{$v}">
					<xsl:value-of select="$v" />
				</i18n:text>
			</t>-->
		</o>
	</xsl:template>

</xsl:stylesheet>
