<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSL construisant l'XHTML du Pleade embarqué
    |
    | Il s'agit de servir uniquement le coeur de page de Pleade dans un DIV en y
    | ajoutant les déclarations CSS et Script que l'on pourrait trouver dans
    | l'HEAD du Pleade complet.
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:xhtml="http://www.w3.org/1999/xhtml"
								exclude-result-prefixes="xhtml">

	<xsl:template match="/xhtml:html | /html">

    &#13;<xsl:comment>Pleade embed</xsl:comment>&#13;
		<div>

      <!-- Recopier les attributs du body -->
      <xsl:apply-templates select="xhtml:body/@*|body/@*" />

      <xsl:variable name="class" select="xhtml:body/@classe | body/@class" />

      <!-- On ajoute la classe CSS "yui-skin-pleade' si elle n'y est pas -->
      <xsl:attribute name="class">
        <xsl:value-of select="$class" />
        <xsl:if test="not(contains($class, 'yui-skin-pleade'))">
          <xsl:text> yui-skin-pleade</xsl:text>
        </xsl:if>
      </xsl:attribute>

      <!-- Recopier les déclarations de scripts -->
			<xsl:apply-templates select="xhtml:head/xhtml:script|head/script" />
      <!-- Recopier les déclarations de CSS -->
      <xsl:apply-templates select="xhtml:head/xhtml:link|head/link" />
      <xsl:apply-templates select="xhtml:head/comment()|head/comment()" />

      <!-- Recopier le contenu du body -->
      <xsl:apply-templates select="xhtml:body//xhtml:div[@id='pl-pg-body-main']
                                  | body/div[@id='pl-pg-body-main']" />

    </div>

	</xsl:template>

	<xsl:template match="*">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="@* | node()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="@xml:*">
		<xsl:copy-of select="." copy-namespaces="no" />
	</xsl:template>

	<xsl:template match="@*">
		<xsl:attribute name="{local-name()}">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>

	<xsl:template match="comment()|processing-instruction()">
		<xsl:copy-of select="." copy-namespaces="no" />
	</xsl:template>

</xsl:stylesheet>
