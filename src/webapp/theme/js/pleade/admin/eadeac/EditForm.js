//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du formulaire d'edition dans la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion du formulaire d'edition dans la partie admin
 * @author Julien Huet
 * @augments EditFormValidator
 * @augments HelpPanels
 */
var EditForm = new Class(/** @lends EditForm.prototype */{
	Implements: [EditFormValidator, HelpPanels],
	
	/**
	 * <strong>Constructor</strong><br/> Initialisation du formulaire
	 */
	initialize: function(){
		this.initHelpPanels();
	}
});