<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT construisant l'XHTML de la partie dynamique de la recherche
		+-->
<xsl:stylesheet
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:h="http://apache.org/cocoon/request/2.0"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		version="2.0" exclude-result-prefixes="xsl xsp sdx i18n xhtml pleade fn xs">

	<xsl:import href="../../commons/xsl/functions.xsl"/>

	<!-- le nom du formulaire -->
	<xsl:param name="name" select="''" />
	<xsl:param name="search-help-mode"/>
	<xsl:param name="navindex-from-input" select="'false'"/>
	<!-- Contexte d'affichage du formulaire -->
	<xsl:param name="destination"/>
  <xsl:param name="suggest-starts-with" select="'false'"/>

	<!--separateur de Liste pour javascript-->
	<xsl:param name="list-sep" select="''"/>

	<xsl:variable name="quot">
		<xsl:text>'</xsl:text>
	</xsl:variable>

	<xsl:variable name="form-id">
		<xsl:choose>
			<xsl:when test="$name!=''">
				<xsl:value-of select="$name" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>ead-default</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="form-parameters">
		<xsl:choose>
			<xsl:when test="/aggregate/h:request/h:requestParameters/h:parameter[starts-with(@name, 'champ')]">
				<xsl:text>true</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="hidden" select="'hidden'"/>
	<!--DEBUG-->
	<!--<xsl:variable name="hidden" select="'text'"/>-->

  <!--+
      | Construction de la page XHTML
      +-->
	<xsl:template match="/aggregate">
		<!--<xsl:message>[search.xsl]name=<xsl:value-of select="$name"/></xsl:message>
		<xsl:message>[search.xsl]form-parameters=<xsl:value-of select="$form-parameters"/></xsl:message>-->
		<html xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="html-head" />
      <xsl:apply-templates select="." mode="html-body" />
		</html>

	</xsl:template>

  <!--+
      | Construction de l'en-tête HTML
      +-->
  <xsl:template match="aggregate" mode="html-head">
    <head>
      <title>
        <i18n:text key="form.title.search-form.{$form-id}">form.title.search-form.<xsl:value-of select="$form-id"/></i18n:text>
      </title>
				<i18n:text key="form.meta.tags.search-form.{$form-id}"><meta name="copyright" content="Copyright AJLSM - Anaphore"/></i18n:text>
				<link type="text/css" rel="stylesheet" href="yui/assets/skins/pleade/container.css"/>
				<link type="text/css" rel="stylesheet" href="yui/assets/skins/pleade/autocomplete.css" />
				<link type="text/css" rel="stylesheet" href="yui/assets/skins/pleade/treeview.css"  />
        <script type="text/javascript" src="yui/treeview/treeview-min.js">//</script>
        <script type="text/javascript" src="yui/datasource/datasource-min.js">//</script>
        <script type="text/javascript" src="yui/autocomplete/autocomplete-min.js">//</script>
        <script type="text/javascript" src="i18n/module-eadeac-js.js">//</script>
        <script type="text/javascript" src="js/pleade/common/manager/SuggestManager.js">//</script>	
        <script type="text/javascript" src="js/pleade/module/search-form/SearchCdc.js">//</script>
        	<script type="text/javascript" src="js/pleade/module/search-form/Operator.js">//</script>
        	<script type="text/javascript" src="js/pleade/module/search-form/SearchForm.js">//</script>
        	<script type="text/javascript" src="js/pleade/module/search-form/SearchFormSuggest.js">//</script>
        	<script type="text/javascript" src="js/pleade/module/search-form/IndexPanel.js">//</script>
        	<script type="text/javascript" src="js/pleade/module/search-form/SearchFormValidator.js">//</script>
        <xsl:apply-templates select="form" mode="javascript-head-sup"/>
        <xsl:apply-templates select="form" mode="javascript-init"/>
      </head>
  </xsl:template>

  <!--+
      | Construction du BODY HTML
      +-->
  <xsl:template match="aggregate" mode="html-body">
    <body>
      <!-- Un titre pour le contenu de la page -->
      <h1 class="pl-title">
        <i18n:text key="form.header.search-form.{$form-id}">form.header.search-form.<xsl:value-of select="$form-id"/></i18n:text>
      </h1>
      <!-- Une introduction -->
      <div class="pl-form-intro">
				<xsl:comment>texte introductif</xsl:comment>
        <i18n:text key="form.intro.search-form.{$form-id}">form.intro.search-form.<xsl:value-of select="$form-id"/></i18n:text>
      </div>
      <!-- Le formulaire de recherche -->
      <div class="pl-form-box" id="search-form">
        <xsl:apply-templates select="form" mode="search-form"/>
      </div>
      <!-- Une conclusion -->
      <p class="pl-form-outro">
        <i18n:text key="form.conclusion.search-form.{$form-id}">form.conclusion.search-form.<xsl:value-of select="$form-id"/></i18n:text>
      </p>
    </body>
  </xsl:template>

  <!--+
      | Déclaration des bouts de Javascript dans l'en-tête XHTML
      +-->
	<xsl:template match="form" mode="javascript-init">
		<xsl:param name="provenance" select="''"/>
		<!--mis a part dans un template car sera ce template sera appelé depuis subset.xsl-->
		<script type="text/javascript">
		<xsl:comment>
			var sep="<xsl:value-of select="$list-sep"/>";
			var cdc_2check_ids = "";
			var search_help_mode = "<xsl:value-of select="$search-help-mode"/>";
			var navindex_from_input = "<xsl:value-of select="$navindex-from-input"/>";
			<xsl:if test="normalize-space(@documents)!=''">
				<!-- FIXME: utilisée ? -->
				var test = "<xsl:value-of select="$provenance" />";
				var dbid = "<xsl:value-of select="normalize-space(@documents)" />";
				var searchForm = "";
				var b_initSuggest, b_initSimpleSuggests, b_initHelp, b_initCdcSearch, b_initIndexLinks, b_update_all_display_op_suggest = false;
				var b_init_dyn_display_op_checkbox, b_init_dyn_display_op_select, b_initMultipleComboSelect, b_update_all_display_op_multiple_combo = false;
			</xsl:if>
			<xsl:if test="key[@type = 'suggest' and not(@multiple = 'false')] or keygroup/key[@type = 'suggest' and not(@multiple = 'false')]">
				/* Lance l'initialisation des criteres de type "suggest". */
				b_initSuggest = true;
			</xsl:if>
			<xsl:if test="key[@type = 'suggest' and @multiple = 'false'] or keygroup/key[@type = 'suggest' and @multiple = 'false']">
				/* Lance l'initialisation des criteres de type "suggest" simple */
				b_initSimpleSuggests = true;
			</xsl:if>
			<xsl:if test="key[@help = 'true'] or keygroup/key[@help = 'true']">
				/* Lance l'initialisation des panels d'aide */
				b_initHelp = true;
			</xsl:if>
			<xsl:if test="key[@type = 'cdc'] or keygroup/key[@type = 'cdc']">
				/* Lance l'initialisation des cadre de classement */
				b_initCdcSearch = true;
			</xsl:if>
			<xsl:if test="key[(@thesx-id and @thesx-browse) or @terms-browse] or keygroup/key[(@thesx-id and @thesx-browse) or @terms-browse]">
				/* Lance l'initialisation des liens vers dex index ou thesaurus */
				b_initIndexLinks = true;
			</xsl:if>
			<xsl:if test="key[@type='suggest' and @op-display='true'] or keygroup/key[@type='suggest' and @op-display='true']">
				/* Lance l'initialisation de l'affichage des operateurs intra-champs pour les suggests */
				b_update_all_display_op_suggest = true;
			</xsl:if>
			<xsl:if test="key[@type='list' and @variant='checkbox' and @op-display='true'] or keygroup/key[@type='list' and @variant='checkbox' and @op-display='true']">
				/* Lance l'initialisation de l'affichage des operateurs intra-champs pour les listes checkbox avec @op-display='true'*/
				b_init_dyn_display_op_checkbox = true;
			</xsl:if>
			<xsl:if test="key[@type='list' and @variant='fixed' and @selection='multiple' and @op-display='true'] or keygroup/key[@type='list' and @variant='fixed' and @selection='multiple' and @op-display='true']">
				/* Lance l'initialisation des evenements necessaires à la gestion de l'affichage des operateurs intra-champs en cas de selection multiple */
				b_init_dyn_display_op_select = true;
				<!--FIXME : a revoir car l'evenement survient trop tot (onclick => avant la reinitialisation reelle du formulaire-->
				<!--//reinitialisation lorsqu'on clique sur le bouton reset
					window.addEvent("load", function(){
					//Event.observe($('<xsl:value-of select="$form-id"/>'), 'reset', update_all_display_op_select)
					$('<xsl:value-of select="$form-id"/>').addEvent('reset', update_all_display_op_select);
				});-->
			</xsl:if>
			<xsl:if test="key[@type='list' and @variant='combo' and @selection='multiple'] or keygroup/key[@type='list' and @variant='combo' and @selection='multiple']">
				/* Lance l'initialisation des list combo a plusieurs entrée (type suggest) */
				b_initMultipleComboSelect = true;
			</xsl:if>
			<xsl:if test="key[@type='list' and @variant='combo' and @selection='multiple' and @op-display='true'] or keygroup/key[@type='list' and @variant='combo' and @selection='multiple' and @op-display='true']">
				/* Lance l'initialisation des evenements necessaires à la gestion de l'affichage des operateurs intra-champs en cas de selection multiple depuis un "combo" */
				b_update_all_display_op_multiple_combo = true;
			</xsl:if>

			<xsl:choose>
				<xsl:when test="$provenance = 'ead'">
					searchForm.init(b_initSuggest, b_initSimpleSuggests, b_initHelp, b_initCdcSearch, b_initIndexLinks, b_update_all_display_op_suggest,b_init_dyn_display_op_checkbox, b_init_dyn_display_op_select, b_initMultipleComboSelect, b_update_all_display_op_multiple_combo);
				</xsl:when>
				<xsl:otherwise>
					window.addEvent("load", init);
					function init(){
						searchForm = new SearchForm();
						searchForm.init(b_initSuggest, b_initSimpleSuggests, b_initHelp, b_initCdcSearch, b_initIndexLinks, b_update_all_display_op_suggest,b_init_dyn_display_op_checkbox, b_init_dyn_display_op_select, b_initMultipleComboSelect, b_update_all_display_op_multiple_combo);
					}
				</xsl:otherwise>
			</xsl:choose>

			window.addEvent('domready', function() {
				<!-- we *have to* initialize submit button here, or the onclick code won't work -->
				oButton = new YAHOO.widget.Button( 'pl-form-button1', {type: "submit"} );
				<!-- let's use 'click' event on submit button instead of 'submit' on the form, which will fails under IE used on the top of YUI buttons -->
				$('pl-form-button1-button').addEvent('click', function(e) {
					try{
						if ( !searchForm.searchFormValidator.form2work($('<xsl:value-of select="$form-id" />')) ) {
							e.stop();
						}
					} catch (er) {
						alert(_usersMessages.form_submit_error);
						e.stop();
					}
				});
				<!-- Cancel button -->
				oButton = new YAHOO.widget.Button( 'pl-form-button2', {type: "reset"} );
				$('pl-form-button2-button').addEvent('click', function(e) {
					e.stop();
					searchForm.reloadNoParam('<xsl:value-of select="$form-id" />');
				});
			});

      var suggest_starts_with = "<xsl:value-of select="$suggest-starts-with"/>";
		//</xsl:comment>
		</script>
		<!-- A la fin un template pour permettre d'ajouter des scripts locaux au niveau du formulaire -->
		<xsl:apply-templates select="." mode="local-javascript-init"/>
	</xsl:template>

	<!-- Template pour permettre d'ajouter des scripts locaux. Ne fait rien par défaut. -->
	<xsl:template match="form" mode="local-javascript-init">
	</xsl:template>

  <!--+
      | Traitement du formulaire de recherche
      +-->
	<xsl:template match="form" mode="search-form">
		<!--form-id est une variable globale, mais on peut la surcharger ici en l'envoyant en paramètre de template (c'est le cas depuis subset.xsl)
		si rien n'est envoyé c'est bien la valeur de la variable globale qui est reprise-->
		<xsl:param name="form-id" select="$form-id"/>

		<!--Il est possible d'envoyer en paramètre une surcharge de critères à ajouter en hidden, par défaut ce sont les parametres d'url mais ils peuvent aussi être envoyé en paramètre de template
				Isls sont préfixé avec h_ (pour hidden) => _hchampN,_hqueryN, _hcopN et _hopN
				ATTENTION ! ils doivent être numérotés dans l'ordre :  1, 2, 3,...	-->
		<xsl:param name="str-paramsCriteria">
			<!--valeur par défaut de str-paramsCriteria (quand il n'est pas passé au template)-->
			<xsl:choose>
				<xsl:when test=" $form-parameters = 'true' and /aggregate/h:request/h:requestParameters/h:parameter[@name = 'n-start'] ">
					<!--cas : on travail sur les parametres d'url pour remplir le formulaire et n-start est envoyé en parametre d'url :  on cache les (n-start) premiers critères -->
					<!--<xsl:message>[search.xsl]on travail sur les parametres d'url pour remplir le formulaire et n-start est envoyé en parametre d'url :  on cache les (n-start) premiers critères</xsl:message>-->
					<xsl:for-each select="/aggregate/h:request/h:requestParameters/h:parameter[ starts-with(@name, 'champ') and number(substring-after( @name, 'champ' )) &lt;= number(/aggregate/h:request/h:requestParameters/h:parameter[@name = 'n-start']/h:value) ]">
						<xsl:sort select="number(substring-after( @name, 'champ' ))"/>
						<xsl:variable name="i" select="number(substring-after( @name, 'champ' ))"/>
						<xsl:variable name="query" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('query', $i)]"/>
						<xsl:variable name="op" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('op', $i)]"/>
						<xsl:variable name="cop" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('cop', $i)]"/>
						<xsl:value-of select="@name"/>
						<xsl:text>=</xsl:text>
						<xsl:value-of select="h:value"/>
						<xsl:if test="$query">
							<xsl:text>&amp;</xsl:text>
							<xsl:value-of select="$query/@name"/>
							<xsl:text>=</xsl:text>
							<xsl:value-of select="$query/h:value"/>
						</xsl:if>
						<xsl:if test="$cop">
							<xsl:text>&amp;</xsl:text>
							<xsl:value-of select="$cop/@name"/>
							<xsl:text>=</xsl:text>
							<xsl:value-of select="$cop/h:value"/>
						</xsl:if>
						<xsl:if test="$op">
							<xsl:text>&amp;</xsl:text>
							<xsl:value-of select="$op/@name"/>
							<xsl:text>=</xsl:text>
							<xsl:value-of select="$op/h:value"/>
						</xsl:if>
						<xsl:if test="not(position()=last())">
						<xsl:text>&amp;</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<!--cas : n-start n'est pas envoyé en paramètre on va le calculer à partir des paramètre d'url préfixés "_h"-->
					<!--<xsl:message>[search.xsl]n-start n'est pas envoyé en paramètre on va le calculer à partir des paramètre d'url préfixés "_h"</xsl:message>-->
					<xsl:for-each select="/aggregate/h:request/h:requestParameters/h:parameter[starts-with(@name, 'h_')]">
						<xsl:sort select="substring(@name, string-length(@name), 1 )"/>
						<!--FIXME : tri sur le numero du champs (doit être [0-9] dans le cas présent)-->
						<xsl:value-of select="substring-after( @name, 'h_'  )"/>
						<xsl:text>=</xsl:text>
						<xsl:value-of select="h:value"/>
						<xsl:if test="not(position()=last())">
						<xsl:text>&amp;</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:param>

		<xsl:variable name="ns-paramsCriteria">
			<xsl:for-each select="tokenize($str-paramsCriteria, '&amp;')">
				<xsl:variable name="param-name" select="substring-before( ., '='  )"/>
				<xsl:variable name="param-value" select="substring-after( ., '='  )"/>
				<!--<xsl:message>[search.xsl]param-name = <xsl:value-of select="$param-name"/> et param-value = <xsl:value-of select="$param-value"/></xsl:message>-->
				<pleade:param name="{$param-name}" value="{$param-value}"/>
			</xsl:for-each>
		</xsl:variable>

		<!--nombre de paramètres cachés à ajouter en début de formulaire -->
		<xsl:variable name="n-start">
					<xsl:value-of select="count($ns-paramsCriteria/pleade:param[starts-with(@name, 'champ')])"/>
		</xsl:variable>

		<!--DEBUG-->
		<!--<xsl:message>[search.xsl]str-paramsCriteria : <xsl:value-of select="$str-paramsCriteria"/></xsl:message>-->
		<!--<xsl:message>[search.xsl]ns-paramsCriteria : <xsl:copy-of select="$ns-paramsCriteria" /></xsl:message>-->
		<!--<xsl:message>[search.xsl]n-start : <xsl:value-of select="$n-start"/></xsl:message>-->

    <xsl:apply-templates select="." mode="build-form">
      <xsl:with-param name="form-id" select="$form-id" />
      <xsl:with-param name="hidden" select="$hidden" />
      <xsl:with-param name="n-start" select="$n-start" />
      <xsl:with-param name="ns-paramsCriteria" select="$ns-paramsCriteria" />
    </xsl:apply-templates>

	</xsl:template>

  <!--+
      | Construction du formulaire de recherche
      | Le fait d'avoir une règle spécifique à la construction du formulaire
      | permet de surchager cette construction
      +-->
  <xsl:template match="form" mode="build-form">

    <xsl:param name="form-id" />
    <xsl:param name="hidden" />
    <xsl:param name="n-start" />
    <xsl:param name="ns-paramsCriteria" />

    <!--Le formulaire-->
		<form action="{@action}" id="{$form-id}" name="{$form-id}" method="get" class="pl-form-advanced-srch">

			<!-- La base recherchee -->
			<xsl:if test="@documents != ''">
				<xsl:for-each select="tokenize(@documents, ' ')">
					<input type="hidden" class="pl-form-srch-base" name="base" value="{.}"/>
				</xsl:for-each>
			</xsl:if>

      <!-- Appel la règle de construction du @onsubmit -->
      <xsl:apply-templates select="." mode="build-form-onsubmit" />

			<table class="form-table" cellspacing="0" cellpadding="0"
            summary="form.table-form@summary.search-form.{$form-id}" i18n:attr="summary">

				<tr>

					<td style="vertical-align:top">

						<div class="pl-form">

							<div class="pl-form-brdr-t">
								<div class="pl-form-brdr-tl"><xsl:comment>u</xsl:comment></div>
								<div class="pl-form-brdr-tr"><xsl:comment>u</xsl:comment></div>
							</div>

							<div class="pl-form-brdr-r">

								<div class="pl-form-brdr-l">

									<div class="pl-form-main">

                    <!--champs cachés qui ne sont pas des critères de recherche -->
                    <!--form-id : sert à la récupération des bonnes clés i18n pour l'affichage des critères-->
                    <input type="{$hidden}" name="name" value="{$form-id}"/>
                    <!--champ caché linkBack (affichage du bouton "modifier votre requete" ou non)-->
                    <input type="{$hidden}" name="linkBack" value="true"/>
                    <!--n-start : servira à récupérer le nombre de champs cachés de surcharge pour les reconstruire correctement
                    après clic sur "modifier votre recherche" (il n'y a plus le prefixe _h à ce moment là)-->
                    <input type="{$hidden}" name="n-start" value="{$n-start}"/>
					<!--  _ae permet de contrôler l'ouverture automatique des entrées du cdc en résultat -->
					<xsl:if test="@auto-expand != ''"><input type="{$hidden}" name="_ae" value="{@auto-expand}"/></xsl:if>

										<!--critères cachés en surcharge envoyés en paramètres-->
										<xsl:apply-templates select="$ns-paramsCriteria/pleade:param" mode="criteria"/>

										<!--critères cachés du fichier de configuration-->
										<xsl:apply-templates select="key[@type = 'cdc' or @type = 'hidden']" mode="criteria">
											<xsl:with-param name="form-id" select="$form-id"/>
											<xsl:with-param name="n-start" select="$n-start"/>
										</xsl:apply-templates>

										<!--les autres critères sont affichés-->
										<table border="0" cellpadding="0" cellspacing="5"
                           summary="form.table-criteria@summary.search-form.{$form-id}" i18n:attr="summary" class="form-criteria">
											<xsl:apply-templates select="keygroup | key[@type != 'cdc' and @type != 'hidden']" mode="criteria">
												<xsl:with-param name="form-id" select="$form-id"/>
												<xsl:with-param name="n-start" select="$n-start"/>
											</xsl:apply-templates>
										</table>

									</div>

                  <!-- Appel la règle de construction des boutons submit et reset -->
                  <xsl:apply-templates select="." mode="build-submit-buttons" />

								</div>

							</div>

							<div class="pl-form-brdr-b">
								<div class="pl-form-brdr-bl"><xsl:comment>u</xsl:comment></div>
								<div class="pl-form-brdr-br"><xsl:comment>u</xsl:comment></div>
							</div>

						</div>

					</td>

          <!-- Pour le filtre avec le cadre de classement. Ce dernier sera chargé en Ajax -->
					<xsl:if test="key[ @type = 'cdc' ]">
						<td style="vertical-align:top">
							<div style="margin-left: 20px;">
								<p class="pl-form-cdcall-intro">
									<i18n:text key="form.intro.cdcall.search-form.{$form-id}">form.intro.cdcall.search-form.<xsl:value-of select="$form-id"/></i18n:text>
								</p>
								<div id="_cdc_"><xsl:comment>u</xsl:comment></div>
							</div>
						</td>
					</xsl:if>

				</tr>

			</table>

		</form>


  </xsl:template>

  <!--+
      | Construction de la balise "onbsubmit" du formulaire de recherche
      | INFO (MP): Pour que ce contrôle fonctionne sous IE, il faut utiliser YUI
      | et son contrôleur d'évènements : YAHOO.util.Event.on({$form-id}, "submit", onSearchFormSubmit);
      | lorsque le contrôle est mauvais, on renvoie YAHOO.util.Event.preventDefault(_event);
      | pour désactiver la soumission.
      +-->
  <xsl:template match="form" mode="build-form-onsubmit">

    <!--<xsl:attribute name="onsubmit">
      <xsl:text>return form2work(this);</xsl:text>
    </xsl:attribute>-->

  </xsl:template>

  <!--+
      | Construction des boutons de soumissions / ré-initialisation du formulaire
      +-->
  <xsl:template match="form" mode="build-submit-buttons">

    <xsl:param name="form-id" select="$form-id" />


    <div class="pl-form-submit">
      <!-- Un bouton pour la recherche avec une image en css -->
			<input type="submit" i18n:attr="value title" value="form.submit.text.search-form.{$form-id}" id="pl-form-button1" title="form.submit.title.search-form.{$form-id}" class="yui-submit-button pl-form-search-submit"/>
      &#160;
      <!-- Un bouton pour remettre le formulaire a ses valeurs par défaut -->
     <!--  <xsl:choose> -->
       <!--  <xsl:when test="$form-parameters = 'true'"> -->
        <!--si on a pré-rempli le formulaire, on appele le formulaire sans parametres d'url pour l'afficher sans pré-remplissage
					TODO: possibilité de réinitialiser le formulaire dynamiquement en javascript, mais attention aux valeurs par défaut -->
           <!--<button type="button" id="pl-form-button2" class="yui-button yui-push-button"
                 title="form.reset.title.search-form.{$form-id}"  i18n:attr="title"
                  onclick="searchForm.reloadNoParam('{$form-id}');">
            <i18n:text key="form.reset.text.search-form.{$form-id}">form.reset.text.search-form.<xsl:value-of select="$form-id"/></i18n:text>
          </button>-->
          <input type="button" id="pl-form-button2" class="yui-button yui-push-button"
                 title="form.reset.title.search-form.{$form-id}"  i18n:attr="title value"
                 value="form.reset.text.search-form.{$form-id}"
                  onclick="reloadNoParam('{$form-id}');">
          </input>
       <!--  </xsl:when> -->
        <!-- <xsl:otherwise> --> <!--bouton reset par défaut-->
          <!--<button type="reset" id="pl-form-button2"
                 title="form.reset.title.search-form.{$form-id}" i18n:attr="title"
                 class="yui-button yui-reset-button">
            <i18n:text key="form.reset.text.search-form.{$form-id}">form.reset.text.search-form.<xsl:value-of select="$form-id"/></i18n:text>
          </button>-->
          <!--<input type="reset" id="pl-form-button2"
                 title="form.reset.title.search-form.{$form-id}" i18n:attr="title value"
                 value="form.reset.text.search-form.{$form-id}"
                 class="yui-button yui-reset-button">
          </input>-->
        <!-- </xsl:otherwise>
      </xsl:choose> -->

      <div class="pl-form-message" style="display:none;">
        <div class="pl-form-errors">
          <xsl:comment>u</xsl:comment>
        </div>
        <div class="pl-form-infos">
          <xsl:comment>u</xsl:comment>
        </div>
      </div>

    </div>

  </xsl:template>

	<xsl:template match="pleade:param" mode="criteria"> <!--$ns-paramsCriteria/-->
		<!-- exemple : <param name="champ1" value="v1" />-->
		<input type="{$hidden}" name="{@name}" value="{@value}"/>
	</xsl:template>

  <!--+
      | Critère : cadre de classement (@type='cdc')
      +-->
	<xsl:template match="key[@type = 'cdc']" mode="criteria">

		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n-start" select="number(0)"/>
		<xsl:variable name="n" select="count(preceding::key) + 1 + $n-start"/>

		<xsl:if test="position() = 1"> <!--il ne peut y avoir qu'un seul key@type=cdc (on ne prend pas en compte les éventuels autres)-->

			<input type="{$hidden}" name="champ{$n}" value="cdcall" />
			<input type="{$hidden}" name="query{$n}" id="query{$n}" class="pl-cdc"/>

			<!--<input type="{$hidden}" name="op{$n}" value="OR" />-->
			<!--<input type="{$hidden}" name="cop{$n}" value="AND" id="query{$n}-cop" />-->

			<xsl:apply-templates select="." mode="criteria-op">
				<xsl:with-param name="form-id" select="$form-id"/>
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="op-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('op', $n)]"/>
			</xsl:apply-templates>

			<xsl:apply-templates select="." mode="criteria-cop">
				<xsl:with-param name="form-id" select="$form-id"/>
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="cop-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('cop', $n)]"/>
			</xsl:apply-templates>

			<!--Pre-remplissage du cdc-->
			<xsl:if test="$form-parameters = 'true' and /aggregate/h:request/h:requestParameters/h:parameter[@name = concat('query', $n)]/h:value != '' ">
				<script type="text/javascript">
					cdc_2check_ids = "<xsl:value-of select="replace(/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('query', $n)]/h:value, $list-sep, ' ')"/>";
					<!--TODO : ouverture de l'arbre sur items pré-cochés-->
				</script>
			</xsl:if>

		</xsl:if>

	</xsl:template>

  <!--+
      | Critère : hidden
      +-->
	<xsl:template match="key[@type = 'hidden']" mode="criteria">

		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n-start" select="number(0)"/>
		<xsl:variable name="n" select="count(preceding::key) + 1 + $n-start"/>

		<input type="{$hidden}" name="champ{$n}" value="{@field}" id="query{$n}-field" />
		<xsl:if test="@fieldh">
			<input type="{$hidden}" name="champh{$n}" value="{@fieldh}" id="query{$n}-fieldh" />
		</xsl:if>

		<input type="{$hidden}" name="query{$n}" tabindex="{$n}">
			<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="$form-parameters = 'true'">
						<xsl:value-of select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('query', $n)]/h:value"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@value"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</input>

		<xsl:apply-templates select="." mode="criteria-op">
			<xsl:with-param name="form-id" select="$form-id"/>
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="op-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('op', $n)]"/>
		</xsl:apply-templates>

		<xsl:apply-templates select="." mode="criteria-cop">
			<xsl:with-param name="form-id" select="$form-id"/>
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="cop-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('cop', $n)]"/>
		</xsl:apply-templates>

		<input type="{$hidden}" name="hidden{$n}" value="true"/>

	</xsl:template>

  <!--+
      | Conteneur de critères
      +-->
	<xsl:template match="key[@type != 'cdc' and @type != 'hidden']" mode="criteria">

		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n-start" select="number(0)"/>
		<xsl:variable name="n" select="count(preceding::key) + 1 + $n-start"/>

		<tr>
			<xsl:if test="@id">
				<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
			</xsl:if>

      <!-- Les opérateurs complexes -->
			<td class="pl-form-opcomplex">
				<xsl:apply-templates select="." mode="criteria-cop">
					<xsl:with-param name="form-id" select="$form-id"/>
					<xsl:with-param name="n" select="$n"/>
					<xsl:with-param name="cop-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('cop', $n)]"/>
				</xsl:apply-templates>
			</td>

			<xsl:choose>
				<xsl:when test="$destination = 'eadwindow'">
					<td>
						<!-- Le label -->
						<div class="pl-form-label">
							<!--DEBUG : onclick="alert(getLabel('label{$n}'))"-->
							<xsl:apply-templates select="." mode="criteria-label">
								<xsl:with-param name="form-id" select="$form-id"/>
								<xsl:with-param name="n" select="$n"/>
								<xsl:with-param name="field-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('champ', $n)]"/>
							</xsl:apply-templates>
						</div>

						<!-- Le champ de recherche + operateur intra-champ-->
						<div class="pl-form-searchfield">
							<xsl:call-template name="draw-search-field">
								<xsl:with-param name="n" select="$n"/>
								<xsl:with-param name="form-id" select="$form-id"/>
							</xsl:call-template>
						</div>
					</td>
				</xsl:when>
				<xsl:otherwise>
					<!-- Le label -->
					<td class="pl-form-label">
						<!--DEBUG : onclick="alert(getLabel('label{$n}'))"-->
						<xsl:apply-templates select="." mode="criteria-label">
							<xsl:with-param name="form-id" select="$form-id"/>
							<xsl:with-param name="n" select="$n"/>
							<xsl:with-param name="field-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('champ', $n)]"/>
						</xsl:apply-templates>
					</td>

					<!-- Le champ de recherche + operateur intra-champ-->
					<td class="pl-form-searchfield">
						<xsl:call-template name="draw-search-field">
							<xsl:with-param name="n" select="$n"/>
							<xsl:with-param name="form-id" select="$form-id"/>
						</xsl:call-template>
					</td>
				</xsl:otherwise>
			</xsl:choose>

    <xsl:if test="$destination != 'eadwindow'">
			<!-- Les liens vers index / thésaurus -->
			<td class="pl-form-index">
				<xsl:choose>
					<xsl:when test="@thesx-browse = 'true'">
						<xsl:apply-templates select="." mode="output-index-link">
							<xsl:with-param name="form-id" select="$form-id"/>
							<xsl:with-param name="n" select="$n"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:when test="contains(@variant,'interval') or not(@terms-browse!='') or (@terms-browse castable as xs:boolean and not(xs:boolean(@terms-browse)))">
						<!-- Pour un critère de recherche sur un intervalle (de date), on n'active pas le navindex.
								 De la même manière, si on n'a pas de @terms-browse ou un @terms-browse vide. -->
						<xsl:text>&#xA0;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="." mode="output-index-link">
							<xsl:with-param name="form-id" select="$form-id"/>
							<xsl:with-param name="n" select="$n"/>
						</xsl:apply-templates>
					</xsl:otherwise>
				</xsl:choose>
			</td>

      <!-- Le bouton d'aide -->
			<td class="pl-form-help">
				<xsl:choose>
					<xsl:when test="@help = 'true'">
						<xsl:apply-templates select="." mode="output-help">
							<xsl:with-param name="form-id" select="$form-id"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						&#160;
					</xsl:otherwise>
				</xsl:choose>
			</td>
    </xsl:if>

		</tr>

	</xsl:template>

  <!--+
      | keygroup
      +-->
  <xsl:template match="keygroup" mode="criteria">
    <xsl:variable name="keygroupId" select="@id"/>
    <!-- En-tête du groupe -->
    <tbody class="keygroup">
      <tr class="keygroup">
        <td class="keygroup-title" colspan="5">
          <a href="#" title="Voir ou masquer les critères de recherche de ce groupe" onclick="javascript:searchForm.toggleGroup('{@id}', this);" class="tbody_hidden">
            <xsl:attribute name="id"><xsl:value-of select="@id"/><xsl:text>-open-image</xsl:text></xsl:attribute>
            <xsl:text> </xsl:text>
            <xsl:choose>
              <xsl:when test="title[@i18n-key != '']">
                <i18n:text key="{title[@i18n-key != ''][1]/@i18n-key}"><xsl:value-of select="title[@i18n-key != ''][1]/@i18n-key"/></i18n:text>
              </xsl:when>
              <xsl:otherwise><xsl:value-of select="title"/></xsl:otherwise>
            </xsl:choose>
          </a>
        </td>
      </tr>
    </tbody>
    <!-- Corps du groupe : repliable -->
    <tbody>
      <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
      <xsl:attribute name="class">form-keygroup <xsl:value-of select="@id"/></xsl:attribute>
      <xsl:attribute name="style"><xsl:text>display: none</xsl:text></xsl:attribute>
      <xsl:apply-templates select="key" mode="criteria"/>
    </tbody>
  </xsl:template>

	<xsl:template name="draw-search-field">
		<xsl:param name="form-id"/>
		<xsl:param name="n"/>
		<xsl:apply-templates select="." mode="criteria-field">
			<xsl:with-param name="form-id" select="$form-id"/>
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="query-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('query', $n)]"/>
			<xsl:with-param name="query-param-alt" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('alt', $n)]"/>
			<xsl:with-param name="query-param-date" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('date', $n)]"/>
			<xsl:with-param name="query-param-from" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('from', $n)]"/>
			<xsl:with-param name="query-param-to" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('to', $n)]"/>
			<!--pour les intervalles de dates query-param est constitué de 3 champs "du{$n}", "db{$n}", "de{$n}"-->
			<xsl:with-param name="query-param-du" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('du', $n) ]"/>
			<xsl:with-param name="query-param-db" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('db', $n) ]"/>
			<xsl:with-param name="query-param-de" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('de', $n) ]"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="." mode="criteria-op">
			<xsl:with-param name="form-id" select="$form-id"/>
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="op-param" select="/aggregate/h:request/h:requestParameters/h:parameter[@name = concat('op', $n)]"/>
		</xsl:apply-templates>
	</xsl:template>

	<!--+
      | CRITERIA LABEL
      +-->

  <!--+
      | Label de critère : suggest multi-champs : Non pris en compte !
      +-->
	<xsl:template match="key[@type = 'suggest' and not(@field)] " mode="criteria-label">
		<xsl:param name="form-id" select="$form-id"/>
		<!--on ne prend pas en compte les suggest multi-champs-->
		<!--<xsl:param name="n" select="number(0)"/>
		<select name="champ{$n}" id="query{$n}-field" class="pl-form-slct" onChange="initSuggest( 'query{$n}' );">
			<xsl:copy-of select="@style"/>
			<xsl:apply-templates select="field" mode="criteria-field">
				<xsl:with-param name="form-id" select="$form-id"/>
			</xsl:apply-templates>
		</select>-->
	</xsl:template>

  <!--+
      | Label de critère : text
      +-->
	<xsl:template match="key[@type = 'text' and not(@field)] " mode="criteria-label">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="field-param" select="/"/>
		<select name="champ{$n}" id="query{$n}-field" class="pl-form-slct">
			<xsl:copy-of select="@style"/>
			<xsl:apply-templates select="field" mode="criteria-label">
				<xsl:with-param name="form-id" select="$form-id"/>
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="field-param" select="$field-param"/>
			</xsl:apply-templates>
		</select>
	</xsl:template>

  <!--+
      | Label de critère : field
      +-->
	<xsl:template match="field" mode="criteria-label">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="field-param" select="/"/>
		<option value="{@name}">
			<xsl:choose>
				<!--<xsl:when test="$field-param">-->
				<xsl:when test="$form-parameters = 'true'">
					<xsl:if test="@name = $field-param/h:value">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="@default = 'true'">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<i18n:text key="form.field.{@name}.search-form.{$form-id}">form.field.<xsl:value-of select="@name"/>.search-form.<xsl:value-of select="$form-id"/></i18n:text>
		</option>
	</xsl:template>

  <!--+
      | Label de critère : date
      +-->
	<xsl:template match="key[@type = 'date']" mode="criteria-label">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n"/>
    <xsl:variable name="k">
      <xsl:text>form.date-label.id</xsl:text>
      <xsl:if test="@id!=''">
        <xsl:value-of select="concat('.', @id)" />
      </xsl:if>
      <xsl:text>.search-form.</xsl:text>
      <xsl:value-of select="$form-id" />
    </xsl:variable>
    <xsl:variable name="fieldname">
    	<xsl:choose>
    		<xsl:when test="key[@type='date' and @field!='']">date<xsl:value-of select="$n"/></xsl:when>
    		<xsl:otherwise>query<xsl:value-of select="$n"/>_du</xsl:otherwise>
    	</xsl:choose>
    </xsl:variable>
    <label for="{$fieldname}">
			<i18n:text key="{$k}"><xsl:value-of select="$k" /></i18n:text>
		</label>
	</xsl:template>

  <!--+
      | Label de critère : autre
      +-->
	<xsl:template match="key" mode="criteria-label">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:variable name="fieldname">
			<xsl:choose>
				<xsl:when test="@terms-browse"><xsl:text>query</xsl:text><xsl:value-of select="$n"/><xsl:text>-input</xsl:text></xsl:when>
		<xsl:when test="@type = 'list' and (@variant = 'combo' or @variant = 'fixed')"><xsl:value-of select="concat(../@documents, '-', @field, '-slct')"/></xsl:when>
				<xsl:when test="@field"><xsl:text>query</xsl:text><xsl:value-of select="if(@id) then @id else $n"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="@id"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<label for="{$fieldname}">
			<i18n:text key="form.field.{@field}.search-form.{$form-id}">form.field.<xsl:value-of select="@field"/>.search-form.<xsl:value-of select="$form-id"/></i18n:text>
		</label>
	</xsl:template>

	<!--+
      | CRITERIA FIELD
      +-->
  <!--+
      | Champ de critère : text
	  | TODO: @fieldh
      +-->
	<xsl:template match="key[@type = 'text' and not(@field)]" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:apply-templates select="." mode="make-text-input">
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="query-param" select="$query-param"/>
		</xsl:apply-templates>
		<!-- TODO: quoi faire pour les fieldh ici? -->
		<xsl:apply-templates select="." mode="format">
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="target-id" select="@id"/>
		</xsl:apply-templates>

		<xsl:if test="@terms-browse = 'true'">
			<xsl:variable name="opdisplay" select="@op-display"/>
			<div class="pl-form-suggest-values" id="query{$n}-values"><xsl:comment>u</xsl:comment>
			<!--<xsl:if test="$query-param">-->
			<xsl:if test="$form-parameters = 'true' and not(@multiple = 'false')">
				<xsl:for-each select="tokenize($query-param/h:value, ' &#x2022; ')">
					<div class="pl-form-suggest-value">
						<a href="#" onclick="javascript: $(this.parentNode).dispose();return false;">
							<xsl:attribute name="onclick">
								<xsl:text>javascript: $(this.parentNode).dispose();return false;</xsl:text>
								<xsl:if test="$opdisplay='true'">
									<xsl:text>searchForm.update_display_op_suggest('</xsl:text>
									<xsl:value-of select="$n"/>
									<xsl:text>')</xsl:text>
								</xsl:if>
							</xsl:attribute>
							<xsl:attribute name="title">
								<i18n:text key="form.list.item.selected.remove">retirer ce terme de la recherche</i18n:text>
							</xsl:attribute>
							<xsl:value-of select="." />
						</a>
					</div>
				</xsl:for-each>
			</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>

  <!--+
      | Champ de critère : field
      +-->
	<xsl:template match="key[@type = 'text' and @field]" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:param name="query-param-alt" select="/*[2]"/>
		<!--<input type="{$hidden}" name="champ{$n}" value="{@field}" id="query{$n}-field" />-->
		<input type="{$hidden}" name="champ{$n}" value="{@field}" id="champ{$n}"/>
		<xsl:if test="@fieldh">
			<input type="{$hidden}" name="champh{$n}" id="champh{$n}" value="{@fieldh}"/>
		</xsl:if>
		<xsl:apply-templates select="." mode="make-text-input">
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="query-param" select="$query-param"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="." mode="format">
			<xsl:with-param name="n" select="$n"/>
		</xsl:apply-templates>
		<!-- Gestion de la recherche dans un champ alternatif -->
		<xsl:if test="@alternate-field">
			<xsl:variable name="control-id" select="concat('alt', $n)"/>
			<br/>
			<xsl:if test="@alternate-fieldh"><input type="hidden" name="h{$control-id}" id="h{$control-id}" value="{@alternate-fieldh}"/></xsl:if>
			<input type="checkbox" name="{$control-id}" id="{$control-id}" value="{@alternate-field}" class="pl-field-alternate-{@alternate-field}">
				<!-- Coché ou non? -->
				<xsl:if test="$query-param-alt/h:value or ($form-parameters = 'false' and @alternate-field-checked = 'true')">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
			<label for="{$control-id}" class="pl-form-label pl-field-alternate-{@alternate-field}">
				<xsl:variable name="k" select="concat('form.field.alt.', @alternate-field, '.search-form.', $form-id)"/>
				<i18n:text key="{$k}"><xsl:value-of select="$k"/></i18n:text>
			</label>
		</xsl:if>
		<xsl:if test="@terms-browse = 'true'">
			<xsl:variable name="opdisplay" select="@op-display"/>
			<div class="pl-form-suggest-values" id="query{$n}-values"><xsl:comment>u</xsl:comment>
			<!--<xsl:if test="$query-param">-->
			<xsl:if test="$form-parameters = 'true' and not(@multiple = 'false')">
				<xsl:for-each select="tokenize($query-param/h:value, ' &#x2022; ')">
					<div class="pl-form-suggest-value">
						<a href="#" onclick="javascript: $(this.parentNode).dispose();return false;">
							<xsl:attribute name="onclick">
								<xsl:text>javascript:$(this.parentNode).dispose();return false;</xsl:text>
								<xsl:if test="$opdisplay='true'">
									<xsl:text>searchForm.update_display_op_suggest('</xsl:text>
									<xsl:value-of select="$n"/>
									<xsl:text>')</xsl:text>
								</xsl:if>
							</xsl:attribute>
							<xsl:attribute name="title">
								<i18n:text key="form.list.item.selected.remove">retirer ce terme de la recherche</i18n:text>
							</xsl:attribute>
							<xsl:value-of select="." />
						</a>
					</div>
				</xsl:for-each>
			</xsl:if>
			</div>
		</xsl:if>

	</xsl:template>

	<!-- Construit l'input des key@type='text' en incorporant eventuellement la troncature et le pré-remplissage -->
	<xsl:template match="key[@type='text']" mode="make-text-input">
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<!-- Utilisation d'un thésaurus -->
		<xsl:if test="@thesx-id">
			<input type="{$hidden}" name="thid{$n}" value="{@thesx-id}"/>
			<xsl:if test="@thesx-relations">
				<input type="{$hidden}" name="threl{$n}" value="{@thesx-relations}"/>
			</xsl:if>
		</xsl:if>
		<input type="text" name="query{$n}" tabindex="{$n}">
			<xsl:attribute name="id">
				<xsl:choose>
					<xsl:when test="@terms-browse"><xsl:text>query</xsl:text><xsl:value-of select="$n"/><xsl:text>-input</xsl:text></xsl:when>
					<xsl:when test="@field"><xsl:text>query</xsl:text><xsl:value-of select="if(@id) then @id else $n"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="@id"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:text>pl-form-txt</xsl:text>
				<xsl:if test="@terms-browse = 'true'">
					<xsl:text> pl-txt-navindexed</xsl:text>
				</xsl:if>
				<!--ajout d'une class pour la troncature automatique-->
				<xsl:if test="@truncate = 'left' or @truncate='right' or @truncate='both' ">
					<xsl:text> pl-form-truncate-</xsl:text>
					<xsl:value-of select="@truncate"/>
				</xsl:if>
			</xsl:attribute>
			<xsl:copy-of select="@style"/>
			<xsl:if test="$form-parameters = 'true'">
				<xsl:variable name="value" select="normalize-space($query-param/h:value)"/>
				<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test="@terms-browse = 'true'"></xsl:when>
						<xsl:when test="@truncate = 'left' and substring( $value, 1, 1 ) = '*' "> <!--double verification avant de supprimer le * au debut de la valeur-->
							<xsl:value-of select="substring( $value, 2 )"/>
						</xsl:when>
						<xsl:when test="@truncate = 'right' and substring( $value, string-length($value) ) = '*' "> <!--double verification avant de supprimer le * au debut de la valeur-->
							<xsl:value-of select="substring( $value, 1, string-length($value) -1 )"/>
						</xsl:when>
						<xsl:when test="@truncate = 'both' and substring( $value, 1, 1 ) = '*' and substring( $value, string-length($value) ) = '*'">
							<xsl:variable name="value_untruncated_on_right" select="substring( $value, 1, string-length($value) -1 )"/>
							<xsl:value-of select="substring( $value_untruncated_on_right ,  2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$value"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
		</input>
	</xsl:template>

  <!--+
      | Champ de critère : suggest multi-champs : Non pris en compte !
      +-->
	<xsl:template match="key[@type = 'suggest' and not(@field)]" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<!--suggest multi-champ n'est pas pris en compte-->
	</xsl:template>

  <!--+
      | Champ de critère : suggest
      +-->
	<xsl:template match="key[@type = 'suggest' and @field]" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:variable name="opdisplay">
			<xsl:choose>
				<xsl:when test="(not(@multiple) or @multiple='true') and @op-display='true'">
					<xsl:text>true</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div id="query{$n}-autocomplete">
			<input type="{$hidden}" name="champ{$n}" value="{@field}" id="query{$n}-field" />
      <!-- On sort les infos pour les filtres éventuels. -->
			<xsl:apply-templates select="filters">
        <xsl:with-param name="n" select="$n"/>
			</xsl:apply-templates>
			<xsl:if test="@fieldh">
				<input type="{$hidden}" name="champh{$n}" value="{@fieldh}"/>
			</xsl:if>
			<!-- Utilisation d'un thésaurus -->
			<xsl:if test="@thesx-id">
				<input type="{$hidden}" name="thid{$n}" value="{@thesx-id}"/>
				<xsl:if test="@thesx-relations">
					<input type="{$hidden}" name="threl{$n}" value="{@thesx-relations}"/>
				</xsl:if>
			</xsl:if>
			<input type="text" name="query{$n}" id="query{$n}-input" tabindex="{$n}">
				<xsl:copy-of select="@style"/>
				<!--une class spécifique ajoutée dans le cas où il faut gérer l'affichage des opérateurs intra-champs (selection multiple) : class intercepté en javascript pour ajout d'evenenements-->
				<xsl:attribute name="class">
					<xsl:text>pl-form-txt</xsl:text>
					<xsl:choose>
						<xsl:when test="@multiple = 'false'">
							<xsl:text> pl-simple-suggest</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text> pl-suggest</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$opdisplay='true'">
						<xsl:text> dyn_display_op_suggest</xsl:text>
					</xsl:if>
					<!-- class avec le nom du champ sdx -->
					<xsl:text> pl-form-input-</xsl:text>
					<xsl:value-of select="@field"/>
				</xsl:attribute>
				<!--pre-remplissage du champs dans le cas d'un suggest simple-->
				<xsl:if test="$form-parameters = 'true' and @multiple = 'false'">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>
			<div id="query{$n}-container"><xsl:comment>u</xsl:comment></div>
		</div>
		<div class="pl-form-suggest-values" id="query{$n}-values"><xsl:comment>u</xsl:comment>
		<xsl:if test="$form-parameters = 'true' and not(@multiple = 'false')">
			<xsl:for-each select="tokenize($query-param/h:value, ' &#x2022; ')">
				<div class="pl-form-suggest-value">
					<a href="#" onclick="javascript: $(this.parentNode).dispose();return false;">
						<xsl:attribute name="onclick">
							<xsl:text>javascript:$(this.parentNode).dispose();return false;</xsl:text>
							<xsl:if test="$opdisplay='true'">
								<xsl:text>searchForm.update_display_op_suggest('</xsl:text>
								<xsl:value-of select="$n"/>
								<xsl:text>')</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="title">
							<i18n:text key="form.list.item.selected.remove">retirer ce terme de la recherche</i18n:text>
						</xsl:attribute>
						<xsl:value-of select="." />
					</a>
				</div>
			</xsl:for-each>
		</xsl:if>
		</div>
		<xsl:apply-templates select="." mode="format">
			<xsl:with-param name="n" select="$n"/>
			<xsl:with-param name="target-id" select="concat('query', $n, '-input')"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<!--+
      | Les filtres pour un suggest
      | 
      | Possiblement plusieurs couples champ / valeur (ff / fv) ;
      | obligatoirement le même nombre de champs et de valeurs.
      | Un seul opérateur possible (fo).
      +-->
	<xsl:template match="filters">
    <xsl:param name="n"/>
    <xsl:apply-templates select="@operator">
      <xsl:with-param name="n" select="$n"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="filter">
      <xsl:with-param name="n" select="$n"/>
    </xsl:apply-templates>
  </xsl:template>
  <xsl:template match="filters/@operator">
    <xsl:param name="n"/>
    <input type="{$hidden}" class="pl-fo-query{$n}" name="fo" value="{.}"/>
  </xsl:template>
  <xsl:template match="filter">
    <xsl:param name="n"/>
    <input type="{$hidden}" class="pl-ff-query{$n}" name="ff" value="{@field}"/>
    <input type="{$hidden}" class="pl-fv-query{$n}" name="fv" value="{@value}"/>
  </xsl:template>

  <!--+
      | Champ de critère : liste
      +-->
	<xsl:template match="key[@type = 'list'][@variant = 'combo' or @variant = 'fixed']" mode="criteria-field">

		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:variable name="fieldSource">
			<xsl:choose>
				<xsl:when test="../key[@updateSelect=current()/@field]">
					<xsl:value-of select="../key[@updateSelect=current()/@field]/@field"/>
				</xsl:when>
				<xsl:when test="../key[updates/updateSelect=current()/@field]">
					<xsl:value-of select="../key[updates/updateSelect=current()/@field]/@field"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<!-- Les filtres -->
		<xsl:variable name="filter">
			<xsl:for-each select="filters/filter">
				<xsl:value-of select="concat('&amp;field=',@field)"/>
				<xsl:value-of select="concat('&amp;value=',@value)"/>
			</xsl:for-each>
			<xsl:for-each select="../key[@type='hidden' and not(current()/filters/filter/@*[name() = 'field' or name() = 'value'] = @*[name() = 'field' or name() = 'value'])]">
				<xsl:value-of select="concat('&amp;field=',@field)"/>
				<xsl:value-of select="concat('&amp;value=',@value)"/>
			</xsl:for-each>
		</xsl:variable>

		<input type="{$hidden}" name="champ{$n}" value="{@field}" id="query{$n}-field" />
		<xsl:if test="@fieldh">
			<input type="{$hidden}" name="champh{$n}" value="{@fieldh}"/>
		</xsl:if>
		<!--permettra d'afficher correctement, dans les critères de recherche de la page de resultats, le champ qui a un @display-field-->
		<xsl:if test="normalize-space(@display-field) != ''">
			<input type="{$hidden}" name="displayfield_{$n}" value="{@display-field}"/>
		</xsl:if>

		<div class="pl-slct-cnt">

			<select id="{../@documents}-{@field}-slct" name="query{$n}" tabindex="{$n}"> <!--id="query{$n}-input" seulement pour les suggest-->
				<!-- [MR] TODO : revoir @i18n-prefix n'est pas conforme à la convention de nommage (soit i18n-key "entiere", soit @id sur lequel on contruit la clé)  => remplacer par @id-->
				<!-- [MR] FIXME : i18n-catalogue est-il utile ? n'est pas toujours module-eadeac.xml ici ?-->

				<!--une class spécifique ajoutée dans le cas où il faut gérer l'affichage des opérateurs intra-champs (selection multiple) : class intercepté en javascript pour ajout d'evenenements-->
				<xsl:attribute name="class">
					<xsl:text>pl-form-slct</xsl:text>
					<xsl:if test="@variant='fixed' and @selection='multiple' and @op-display='true'">
						<xsl:text> dyn_display_op_select</xsl:text>
					</xsl:if>
					<xsl:if test="@variant='combo' and @selection='multiple'">
						<xsl:text> pl-form-slct-multiple-combo</xsl:text>
					</xsl:if>
					<xsl:if test="@variant='combo' and @selection='multiple' and @op-display='true'">
						<xsl:text> dyn_display_op_multiple_combo</xsl:text>
					</xsl:if>
				</xsl:attribute>

				<!-- Liste cible de mise à jour -->
				<xsl:if test="@updateSelect">
					<xsl:variable name="target" select="../key[@field=current()/@updateSelect]" />
					<xsl:variable name="no-choice">
						<xsl:apply-templates select="$target" mode="no-choice-updateSelect">
							<xsl:with-param name="f" select="@field" />
						</xsl:apply-templates>
					</xsl:variable>
					<xsl:variable name="onchange">
						<xsl:value-of select="if ($target/no-choice) then 'if (v!=&quot;&quot;)' else ''"/> searchForm.updateSelect(this, '<xsl:value-of select="concat(../@documents, '-', @updateSelect, '-slct')"/>', '<xsl:value-of select="$no-choice" />', '<xsl:value-of select="$target/@i18n-prefixe" />', '<xsl:value-of select="$target/@i18n-catalogue" />', '<xsl:value-of select="$filter"/>');
					</xsl:variable>
					<xsl:attribute name="onchange"><xsl:value-of select="normalize-space($onchange)"/></xsl:attribute>
				</xsl:if>

				<xsl:if test="updates">
					<xsl:variable name="targets">
						<xsl:apply-templates select="updates" mode="linkedLists"/>
					</xsl:variable>
					<xsl:variable name="onchange">
						searchForm.updateSelects(this, '<xsl:value-of select="../@documents"/>', '<xsl:value-of select="$targets"/>', '<xsl:value-of select="$filter"/>');
					</xsl:variable>
					<xsl:attribute name="onchange"><xsl:value-of select="normalize-space($onchange)"/></xsl:attribute>
				</xsl:if>

				<xsl:copy-of select="@style"/>

				<!--la différence entre @variant=combo et @variant=fixed se situe dans
				les attributs @rows et @selection on ignore ces attributs dans le cas
				d'une liste "combo", on le sinterprète dans le cas d'une liste "fixed"	-->
				<xsl:if test="@variant = 'fixed' ">
					<xsl:attribute name="size">
						<xsl:choose>
							<xsl:when test="@rows">
								<xsl:value-of select="@rows"/>
							</xsl:when>
							<xsl:otherwise><!--valeur par défaut-->
								<xsl:text>10</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:if test="@selection = 'multiple'">
						<xsl:attribute name="multiple">multiple</xsl:attribute>
					</xsl:if>
				</xsl:if>

				<!-- Liste dépendante, vide au départ => on ajoute un attribut disabled=disabled si pas de valeur dans les paramètres -->
				<xsl:if test="$fieldSource!='' and items/item">
					<xsl:variable name="val" select="/aggregate/h:request/h:requestParameters[1]/h:parameter[@name=(concat('query',substring-after(/aggregate/h:request/h:requestParameters[1]/h:parameter[h:value=current()/@field and @name != ''][1]/@name, 'champ') ))]/h:value"/>
					<xsl:if test="string($val) = ''">
						<xsl:attribute name="disabled">disabled</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="items/item" mode="criteria-field">
						<xsl:with-param name="form-id" select="$form-id"/>
						<xsl:with-param name="updateSelect" select="$fieldSource"/>
					</xsl:apply-templates>
				</xsl:if>

				<!-- Premier item appelant à choisir -->
				<xsl:apply-templates select="no-choice" mode="criteria-field">
					<xsl:with-param name="form-id" select="$form-id"/>
				</xsl:apply-templates>

				<xsl:choose>
					<!-- Des items -->
					<xsl:when test="items and not($fieldSource!='')">
						<!-- On traite les items seulement s'il ne s'agit pas d'une liste dépendante ou si on affiche un formulaire vierge -->
						<xsl:apply-templates select="items/item" mode="criteria-field">
							<xsl:with-param name="form-id" select="$form-id"/>
							<xsl:with-param name="query-param" select="$query-param"/>
						</xsl:apply-templates>
					</xsl:when>
					<!-- Des filtres -->
					<xsl:when test="not(items) or $form-parameters='true'">
						<xsl:variable name="base">
							<xsl:if test="../@documents!=''">
								<xsl:for-each select="tokenize(../@documents, ' ')">
									<xsl:value-of select="concat('&amp;base=', .)" />
								</xsl:for-each>
							</xsl:if>
						</xsl:variable>
						<!-- Récupérer un sdx:terms qu'on ne sauvegardera pas dans la session (cache=false) mais que Cocoon cachera (hpp=-1). cf. sdx-terms.xsp -->
						<xsl:variable name="filterSource">
							<xsl:if test="$fieldSource!=''">
								<xsl:for-each select="/aggregate/h:request/h:requestParameters[1]">
									<xsl:variable name="id" select="substring-after(h:parameter[h:value=$fieldSource]/@name, 'champ')" />
									<xsl:variable name="v" select="h:parameter[@name=concat('query', $id)]/h:value" />
									<xsl:if test="$v!=''">
										<xsl:value-of select="concat('&amp;field=', $fieldSource, '&amp;value=', $v)" />
									</xsl:if>
								</xsl:for-each>
							</xsl:if>
						</xsl:variable>

						<xsl:variable name="terms-url" select="concat('cocoon:/functions/sdx-terms.xml?hpp=-1&amp;cache-cocoon=false', $filter, $base, $filterSource, '&amp;field=', @field)"/>
						<xsl:choose>
							<xsl:when test="doc-available($terms-url)">
								<xsl:variable name="terms" select="document($terms-url)/sdx:document/sdx:terms"/>
								<!--<xsl:message>[search.xsl]filter : <xsl:value-of select="$filter"/></xsl:message>-->
								<!--<xsl:message>[search.xsl]filterSource : <xsl:value-of select="$filterSource"/></xsl:message>-->
								<!--<xsl:message>[search.xsl]terms-url : <xsl:value-of select="$terms-url"/></xsl:message>-->
								<!--<xsl:message>[search.xsl]terms: <xsl:copy-of select="document($terms-url)" /></xsl:message>-->
								<xsl:apply-templates select="$terms/sdx:term" mode="comboOrFixed">
									<xsl:with-param name="form-id" select="$form-id"/>
									<xsl:with-param name="query-param" select="$query-param"/>
									<xsl:with-param name="field" select="@field"/>
									<xsl:with-param name="display-field" select="@display-field"/>
									<xsl:with-param name="i18n-prefixe" select="@i18n-prefixe"/>
									<xsl:with-param name="i18n-catalogue" select="@i18n-catalogue"/>
									<xsl:with-param name="truncate_width">
										<xsl:choose>
											<xsl:when test="@max-chars != ''">
												<xsl:value-of select="@max-chars"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="/aggregate/form/@max-chars"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:otherwise>
								<xsl:message>[search.xsl] le document <xsl:value-of select="$terms-url"/> n'est pas accessible</xsl:message>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</select>

			<xsl:if test="@variant='combo' and @selection='multiple'">
				<div class="pl-form-select-values" id="query{$n}-values"><xsl:comment>u</xsl:comment>
					<xsl:if test="$form-parameters = 'true'">
					<xsl:variable name="display-field" select="@display-field"/>
					<xsl:variable name="field" select="@field"/>
					<xsl:variable name="opdisplay" select="@op-display"/>
						<xsl:for-each select="tokenize($query-param/h:value, ' &#x2022; ')">
							<div class="pl-form-select-value">
								<a href="#">
									<xsl:attribute name="onclick">
										<xsl:text>javascript:$(this.parentNode).dispose();return false;</xsl:text>
										<xsl:if test="$opdisplay='true'">
											<xsl:text>searchForm.update_display_op_multiple_combo('</xsl:text>
											<xsl:value-of select="$n"/>
											<xsl:text>')</xsl:text>
										</xsl:if>
									</xsl:attribute>
									<xsl:attribute name="title">
										<i18n:text key="form.list.item.selected.remove">retirer ce terme de la recherche</i18n:text>
									</xsl:attribute>
									<xsl:value-of select="' '"/>
									<xsl:choose>
										<!--si @display-value alors on affiche la valeur "correspondante" au champ-->
										<xsl:when test="$display-field!=''">
											<xsl:variable name="fieldQuery-url" select="concat('cocoon:/functions/sdx-fieldQuery.xml?field=', $field , '&amp;value=', ., '&amp;hpp=1&amp;cache-cocoon=true')"/>
											<!--on active la cache cocoon sur ce résultat pour alleger le traitement-->
											<!--DEBUG -->
											<!--<xsl:message>fieldQuery-url = <xsl:value-of select="$fieldQuery-url"/></xsl:message>-->
											<xsl:choose>
												<xsl:when test="doc-available($fieldQuery-url)">
													<xsl:value-of select="document($fieldQuery-url)/sdx:document/sdx:results/sdx:result[1]/sdx:field[@name=$display-field]/@value"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:message>[pleade3][search.xsl] erreur document('<xsl:value-of select="$fieldQuery-url"/>')</xsl:message>
													<xsl:value-of select="." />
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<!--sinon on affiche la valeur telle qu'envoyée dans l'url-->
										<xsl:otherwise>
											<xsl:value-of select="."/>
										</xsl:otherwise>
									</xsl:choose>
									<!--dans tous les cas on affiche la valeur "reelle" dans un span caché (pour simplifier le traitement javascript)-->
									<span style="display:none;" class="real-value">
										<xsl:value-of select="."/>
									</span>
								</a>
							</div>
						</xsl:for-each>
					</xsl:if>
				</div>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="updates" mode="linkedLists">
		<xsl:text>[</xsl:text>
		<xsl:apply-templates select="updateSelect" mode="linkedLists"/>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template match="updateSelect" mode="linkedLists">
		<xsl:variable name="name" select="normalize-space(.)"/>
		<xsl:variable name="target" select="../../../key[@field=$name]" />
		<xsl:text>{</xsl:text>
		<xsl:value-of select="concat('&quot;value&quot;: &quot;', ., '&quot;, ')"/>
		<xsl:value-of select="concat('&quot;18nPrefix&quot;: &quot;', $target/@i18n-prefixe, '&quot;, ')"/>
		<xsl:value-of select="concat('&quot;i18nCalatolg&quot;: &quot;', $target/@i18n-catalogue, '&quot;, ')"/>
		<xsl:variable name="nochoice">
			<xsl:apply-templates select="$target" mode="no-choice-updateSelect">
				<xsl:with-param name="f" select="$name" />
			</xsl:apply-templates>
		</xsl:variable>
		<xsl:value-of select="concat('&quot;i18nKey&quot;: &quot;', $nochoice, '&quot;')"/>
		<xsl:text>}</xsl:text>
		<xsl:if test="position() != last()"><xsl:text>, </xsl:text></xsl:if>
	</xsl:template>

	<xsl:template match="key[@type = 'list'][@variant = 'combo' or @variant = 'fixed']/no-choice" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<option value="">
			<i18n:text key="{@i18n-key}.search-form.{$form-id}"><xsl:value-of select="concat(@i18n-key, 'search-form.', $form-id)"/></i18n:text>
		</option>
	</xsl:template>

	<!--+
	    | Les items d'une liste de choix
	    +-->
	<xsl:template match="key[@type = 'list'][@variant = 'combo' or @variant = 'fixed']/items/item" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:param name="updateSelect" select="''"/>
		<xsl:variable name="k">
			<xsl:choose>
				<xsl:when test="$updateSelect!='' and $form-parameters='true'">
					<xsl:apply-templates select="parent::items/parent::key" mode="no-choice-updateSelect">
						<xsl:with-param name="f" select="$updateSelect" />
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@i18n-key" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<option value="{@value}">
			<xsl:choose>
				<xsl:when test="$form-parameters = 'true'">
					<xsl:if test="@value = $query-param/h:value">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<!--Dans un combo si plusieurs default = true, on laisse comme ça, le navigateur ne selectionnera que le dernier-->
					<xsl:if test="@default = 'true'">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			 <i18n:text key="{$k}.search-form.{$form-id}"><xsl:value-of select="concat($k, '.search-form.', $form-id)"/></i18n:text>
		</option>
	</xsl:template>

  <!--+
      | Un sdx:terms pour une liste de choix
      +-->
	<xsl:template match="sdx:term" mode="comboOrFixed">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:param name="field" select="''"/>
		<xsl:param name="display-field" select="''"/>
		<xsl:param name="i18n-prefixe" select="''"/>
		<xsl:param name="i18n-catalogue" select="''"/>
		<xsl:param name="truncate_width" select="''"/>

		<xsl:variable name="displayed-value">
			<xsl:choose>
				<xsl:when test="normalize-space($display-field)!=''">
					<!--on doit aller chercher la valeur sur champs correpondan via une fieldQuery-->
					<xsl:variable name="fieldQuery-url" select="concat('cocoon:/functions/sdx-fieldQuery.xml?field=', $field , '&amp;value=', @value, '&amp;hpp=1&amp;cache-cocoon=true')"/>
					<!--on active la cache cocoon sur ce résultat pour alleger le traitement-->
					<!--<xsl:message><xsl:text>fieldQuery-url=</xsl:text><xsl:value-of select="$fieldQuery-url"/></xsl:message>-->
					<xsl:choose>
						<xsl:when test="doc-available($fieldQuery-url)">
							<xsl:value-of select="document($fieldQuery-url)/sdx:document/sdx:results/sdx:result[1]/sdx:field[@name=$display-field]/@value"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:message>[pleade3][search.xsl] erreur document('<xsl:value-of select="$fieldQuery-url"/>')</xsl:message>
							<xsl:value-of select="@value" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@value" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<option value="{@value}">
			<xsl:if test="$form-parameters = 'true' and $query-param/h:value = @value">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$i18n-prefixe!=''">
					<i18n:text key="{$i18n-prefixe}.{@value}.search-form.{$form-id}">
						<xsl:if test="$i18n-catalogue!=''">
							<xsl:attribute name="catalogue">
								<xsl:value-of select="$i18n-catalogue" />
							</xsl:attribute>
						</xsl:if>
						<!--<xsl:value-of select="concat(@value, '.search-form.', $form-id)" />-->
						<xsl:value-of select="@value" />
					</i18n:text>
				</xsl:when>
				<xsl:when test="number($truncate_width)">
					<xsl:value-of select="pleade:truncate-string($displayed-value, number($truncate_width))"/>
				</xsl:when>
				<xsl:otherwise>
					<!--<xsl:value-of select="@value"/>-->
					<xsl:value-of select="$displayed-value"/>
				</xsl:otherwise>
			</xsl:choose>
		</option>
	</xsl:template>

  <!--+
      | Champ de critère radios
      +-->
	<xsl:template match="key[@type = 'list'][@variant = 'radio' or @variant = 'checkbox']" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<input type="{$hidden}" name="champ{$n}" value="{@field}"/>
		<xsl:if test="@fieldh">
			<input type="{$hidden}" name="champh{$n}" value="{@fieldh}"/>
		</xsl:if>
		<span id="checkbox_container{$n}">
		<xsl:attribute name="class">
			<xsl:text>pl-form-checkbox-container</xsl:text>
			<xsl:if test="@variant='checkbox' and @op-display='true'">
				<xsl:text> dyn_display_op_checkbox</xsl:text>
			</xsl:if>
		</xsl:attribute>


		<xsl:choose>
			<xsl:when test="items">
				<xsl:apply-templates select="items/item" mode="criteria-field">
					<xsl:with-param name="form-id" select="$form-id"/>
					<xsl:with-param name="n" select="$n"/>
					<xsl:with-param name="query-param" select="$query-param"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="filter">
					<xsl:for-each select="filters/filter">
						<xsl:value-of select="concat('&amp;field=',@field)"/>
						<xsl:value-of select="concat('&amp;value=',@value)"/>
					</xsl:for-each>
					<xsl:for-each select="../key[@type='hidden' and not(current()/filters/filter/@*[name() = 'field' or name() = 'value'] = @*[name() = 'field' or name() = 'value'])]">
						<xsl:value-of select="concat('&amp;field=',@field)"/>
						<xsl:value-of select="concat('&amp;value=',@value)"/>
					</xsl:for-each>
				</xsl:variable>
        <xsl:variable name="base">
          <xsl:if test="../@documents!=''">
						<xsl:for-each select="tokenize(../@documents, ' ')">
							<xsl:value-of select="concat('&amp;base=', .)" />
						</xsl:for-each>
          </xsl:if>
        </xsl:variable>
				<!-- Récupérer un sdx:terms qu'on ne sauvegardera pas dans la session (cache=false) mais que Cocoon cachera (hpp=-1). cf. sdx-terms.xsp -->
				<xsl:variable name="terms-url" select="concat('cocoon:/functions/sdx-terms.xml?hpp=-1&amp;cache-cocoon=false', $filter, $base, '&amp;field=', @field)"/>
				<xsl:choose>
					<xsl:when test="doc-available($terms-url)">
						<xsl:variable name="terms" select="document($terms-url)/sdx:document/sdx:terms"/>
						<!--<xsl:apply-templates select="$terms/*[local-name() = 'term']" mode="radioOrCheckBox">-->
						<xsl:apply-templates select="$terms/sdx:term" mode="radioOrCheckBox">
							<xsl:with-param name="form-id" select="$form-id"/>
							<xsl:with-param name="n" select="$n"/>
							<xsl:with-param name="variant" select="@variant"/>
							<xsl:with-param name="display" select="@display"/>
							<xsl:with-param name="query-param" select="$query-param"/>
							<xsl:with-param name="truncate_width">
								<xsl:choose>
									<xsl:when test="@max-chars != ''">
										<xsl:value-of select="@max-chars"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="/aggregate/form/@max-chars"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>[search.xsl] le document <xsl:value-of select="$terms-url"/> n'est pas accessible</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		</span>
	</xsl:template>

  <!--+
      | Items d'un critère : radio ou checkbox
      +-->
	<xsl:template match="key[@type = 'list'][@variant = 'radio' or @variant = 'checkbox']/items/item" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<input type="{ancestor::key[1]/@variant}" name="query{$n}" id="query{$n}-input-{position()}" tabindex="{$n}" value="{@value}" class="pl-form-chckbx">
			<xsl:choose>
				<xsl:when test="$form-parameters = 'true'">
					<xsl:if test="@value = $query-param/h:value">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="@default = 'true'">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:copy-of select="@style"/>
		</input>
		&#160;
		<label for="query{$n}-input-{position()}" class="pl-form-label">
			<xsl:choose>
				<xsl:when test="@i18n-key"><i18n:text key="{@i18n-key}.search-form.{$form-id}"><xsl:value-of select="concat(@i18n-key, '.search-form.', $form-id)"/></i18n:text></xsl:when>
				<xsl:when test=". != ''"><xsl:value-of select="."/></xsl:when>
				<xsl:otherwise><xsl:value-of select="@value"/></xsl:otherwise>
			</xsl:choose>
		</label>
		<xsl:choose>
			<xsl:when test="ancestor::key[1]/@display = 'horizontal'">
				&#160;
			</xsl:when>
			<xsl:otherwise>
				<br/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <!--+
      | Un sdx:terms pour une liste de boutons radio ou checkbox
      +-->
	<xsl:template match="sdx:term" mode="radioOrCheckBox">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="variant" select="''"/>
		<xsl:param name="display" select="'vertical'"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:param name="truncate_width" select="''"/>
		<span class="pl-input-label">
			<input type="{$variant}" name="query{$n}" id="query{$n}-input-{position()}" tabindex="{$n}" value="{@value}" class="pl-form-chckbx">
				<xsl:copy-of select="@style"/>
				<xsl:if test=" $form-parameters = 'true' and  $query-param/h:value = @value">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
			<label for="query{$n}-input-{position()}" class="pl-form-label">
				<xsl:choose>
					<xsl:when test="number($truncate_width)">
						<xsl:value-of select="pleade:truncate-string(@value, number($truncate_width))"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@value"/>
					</xsl:otherwise>
				</xsl:choose>
			</label>
		</span>
		<xsl:choose>
			<xsl:when test="$display = 'horizontal'">
				&#160;
			</xsl:when>
			<xsl:otherwise>
				<br/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <!--+
      | Champ de critère : boolean
      +-->
	<xsl:template match="key[@type = 'boolean']" mode="criteria-field">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param" select="/"/>
		<xsl:param name="field-param" select="/"/>
		<input type="{$hidden}" name="champ{$n}" value="{@field}"/>
		<xsl:if test="@fieldh">
			<input type="{$hidden}" name="champh{$n}" value="{@fieldh}"/>
		</xsl:if>
		<input type="checkbox" name="query{$n}" tabindex="{$n}" value="{@value-on}">
			<xsl:copy-of select="@style"/>
			<xsl:choose>
				<xsl:when test="$form-parameters = 'true'">
					<xsl:if test="$query-param/h:value = @value-on">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="@default = 'on'">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</input>
		&#160;
		<!--<xsl:value-of select="@value-on"/>-->
		<xsl:choose>
			<xsl:when test="@i18n-key">
				<i18n:text key="{@i18n-key}"><xsl:value-of select="@i18n-key"/></i18n:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@value-on"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <!--+
      | Champ de critère : date "classique"
      +-->
	<xsl:template match="key[@type='date' and @field!='']" mode="criteria-field" priority="+1">
    <xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param-date" select="/"/>
		<xsl:param name="query-param-from" select="/"/>
		<xsl:param name="query-param-to" select="/"/>

		<xsl:variable name="variant-ns">
			<xsl:for-each select="tokenize(@variant, ' ')">
				<pleade:variant value="{.}"/>
			</xsl:for-each>
		</xsl:variable>

		<input type="{$hidden}" name="champ{$n}" value="{@field}"/>

		<!--on s'assure que le format ne contiendra pas de ","-->
		<xsl:variable name="format">
			<xsl:choose>
				<xsl:when test="contains(@format, ',')">
					<xsl:value-of select="substring-before(@format,',')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@format"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="intervalAndExact">
			<xsl:choose>
				<xsl:when test="$variant-ns/pleade:variant[@value = 'exact'] and $variant-ns/pleade:variant[@value = 'interval']">
					<xsl:text>true</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- @variant contient "exact" -->
		<xsl:if test="$variant-ns/pleade:variant[@value = 'exact']">

			<!--<label for="date{$n}">-->
				<i18n:text key="form.date-label.du.search-form.{$form-id}">
					form.date-label.du.search-form.<xsl:value-of select="$form-id"/>
				</i18n:text>
			<!--</label>-->

			<input class="pl-form-date" type="text" name="date{$n}" tabindex="{$n}" id="date{$n}">
				<xsl:if test="$intervalAndExact = 'true'">
					<xsl:attribute name="onfocus">
            <xsl:value-of select="concat('searchForm.clearInput(',$quot,'from',$n,$quot,'); searchForm.clearInput(',$quot,'to',$n,$quot,');')" />
					</xsl:attribute>
				</xsl:if>
				<xsl:copy-of select="@style"/>
				<xsl:apply-templates select="." mode="maxlength-size-attributes"/>
				<xsl:if test="$form-parameters = 'true' and $query-param-date">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param-date/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>

       <!-- Contrôle de format -->
			<xsl:apply-templates select="." mode="format">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="format" select="$format"/>
				<xsl:with-param name="target-id" select="concat('date', $n , '_du')"/>
			</xsl:apply-templates>

		</xsl:if>

		<!--opérateur "ou" si @variant contient "exact" ET "interval"-->
		<xsl:if test="$intervalAndExact = 'true'">
			<i18n:text key="form.date-label.or.search-form.{$form-id}">
				form.date-label.or.search-form.<xsl:value-of select="$form-id"/>
			</i18n:text>
		</xsl:if>

		<!-- @variant contient "interval"-->
		<xsl:if test="$variant-ns/pleade:variant[@value = 'interval']">

			<label for="from{$n}">
				<i18n:text key="form.date-label.db.search-form.{$form-id}">
					form.date-label.db.search-form.<xsl:value-of select="$form-id"/>
				</i18n:text>
			</label>

			<input class="pl-form-date" type="text" name="from{$n}" tabindex="{$n}" id="from{$n}">
				<xsl:if test="$intervalAndExact = 'true'">
					<xsl:attribute name="onfocus">
						<xsl:value-of select="concat('searchForm.clearInput(', $quot, 'date', $n, $quot, ');')"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:copy-of select="@style"/>
				<xsl:apply-templates select="." mode="maxlength-size-attributes"/>
				<xsl:if test="$form-parameters = 'true' and $query-param-from">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param-from/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>

      <!-- Contrôle de format -->
			<xsl:apply-templates select="." mode="format">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="format" select="$format"/>
				<xsl:with-param name="target-id" select="concat('from', $n)"/>
			</xsl:apply-templates>

			<!--Fin-->
			<label for="to{$n}">
				<i18n:text key="form.date-label.de.search-form.{$form-id}">
					form.date-label.de.search-form.<xsl:value-of select="$form-id"/>
				</i18n:text>
			</label>

			<input class="pl-form-date" type="text" name="to{$n}" tabindex="{$n}" id="to{$n}">
				<xsl:if test="$intervalAndExact = 'true'">
					<xsl:attribute name="onfocus">
						<xsl:value-of select="concat('searchForm.clearInput(', $quot, 'date', $n, $quot, ');')"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:copy-of select="@style"/>
				<xsl:apply-templates select="." mode="maxlength-size-attributes"/>
				<xsl:if test="$form-parameters = 'true' and $query-param-to">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param-to/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>

      <!-- Contrôle de format -->
			<xsl:apply-templates select="." mode="format">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="format" select="$format"/>
				<xsl:with-param name="target-id" select="concat('to', $n)"/>
			</xsl:apply-templates>

		</xsl:if>

	</xsl:template>

  <!--+
      | Champ de critère : date "à la Pleade"
      |
      | Ce critère de recherche sur un intervalle de date s'execute sur les
      | champs du, db, de. Il n'y a pas de choix.
      | Pour un  critère date "classique", il faut déclarer un @field
      +-->
  <xsl:template match="key[@type='date']" mode="criteria-field">
    <xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="query-param-du" select="/"/>
		<xsl:param name="query-param-db" select="/"/>
		<xsl:param name="query-param-de" select="/"/>

		<xsl:variable name="variant-ns">
			<xsl:for-each select="tokenize(@variant, ' ')">
				<pleade:variant value="{.}"/>
			</xsl:for-each>
		</xsl:variable>

		<!-- champ{$n} sert a récupérer un id pour le traitement i18n de l'affichage des critères de recherche dans la page de résultats,
		 il n'y a pas de query{$n} donc ce champ ne perturbe pas la recherche (seuls les champs du, db et de seront utilisés)-->
		<input type="{$hidden}" name="champ{$n}" value="id_{@id}"/>

		<!--on s'assure que le format ne contiendra pas de ","-->
		<xsl:variable name="format">
			<xsl:choose>
				<xsl:when test="contains(@format, ',')">
					<xsl:value-of select="substring-before(@format,',')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@format"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="intervalAndExact">
			<xsl:choose>
				<xsl:when test="$variant-ns/pleade:variant[@value = 'exact'] and $variant-ns/pleade:variant[@value = 'interval']">
					<xsl:text>true</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!--@variant contient "exact"-->
		<xsl:if test="$variant-ns/pleade:variant[@value = 'exact']">
			<!--<label for="query{$n}_du">-->
				<i18n:text key="form.date-label.du.search-form.{$form-id}">
					form.date-label.du.search-form.<xsl:value-of select="$form-id"/>
				</i18n:text>
			<!--</label>-->
			<input class="pl-form-date" type="text" name="du{$n}" tabindex="{$n}" id="query{$n}_du">
				<xsl:if test="$intervalAndExact = 'true'">
					<xsl:attribute name="onfocus">
						<xsl:value-of select="concat('searchForm.clearInput(', $quot, 'query', $n, '_db', $quot, ');searchForm.clearInput(', $quot, 'query', $n, '_de', $quot, ');')"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:copy-of select="@style"/>
				<xsl:apply-templates select="." mode="maxlength-size-attributes"/>
				<xsl:if test="$form-parameters = 'true' and $query-param-du">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param-du/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>
			<xsl:apply-templates select="." mode="format">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="format" select="$format"/>
				<xsl:with-param name="target-id" select="concat('query',$n , '_du')"/>
			</xsl:apply-templates>
		</xsl:if>

		<!--opérateur "ou" si @variant contient "exact" ET "interval"-->
		<xsl:if test="$intervalAndExact = 'true'">
			<i18n:text key="form.date-label.or.search-form.{$form-id}">
				form.date-label.or.search-form.<xsl:value-of select="$form-id"/>
			</i18n:text>
		</xsl:if>

		<!--@variant contient "interval"-->
		<xsl:if test="$variant-ns/pleade:variant[@value = 'interval']">
			<!--date begin-->
			<label for="query{$n}_db">
				<i18n:text key="form.date-label.db.search-form.{$form-id}">
					form.date-label.db.search-form.<xsl:value-of select="$form-id"/>
				</i18n:text>
			</label>
			<input class="pl-form-date" type="text" name="db{$n}" tabindex="{$n}" id="query{$n}_db">
				<xsl:if test="$intervalAndExact = 'true'">
					<xsl:attribute name="onfocus">
						<xsl:value-of select="concat('searchForm.clearInput(', $quot, 'query', $n, '_du', $quot, ');')"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:copy-of select="@style"/>
				<xsl:apply-templates select="." mode="maxlength-size-attributes"/>
				<xsl:if test="$form-parameters = 'true' and $query-param-db">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param-db/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>
			<xsl:apply-templates select="." mode="format">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="format" select="$format"/>
				<xsl:with-param name="target-id" select="concat('query', $n, '_db')"/>
			</xsl:apply-templates>

			<!--date end-->
			<label for="query{$n}_de">
				<i18n:text key="form.date-label.de.search-form.{$form-id}">
					form.date-label.de.search-form.<xsl:value-of select="$form-id"/>
				</i18n:text>
			</label>
			<input class="pl-form-date" type="text" name="de{$n}" tabindex="{$n}" id="query{$n}_de">
				<xsl:if test="$intervalAndExact = 'true'">
					<xsl:attribute name="onfocus">
						<xsl:value-of select="concat('searchForm.clearInput(', $quot, 'query', $n, '_du', $quot, ');')"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:copy-of select="@style"/>
				<xsl:apply-templates select="." mode="maxlength-size-attributes"/>
				<xsl:if test="$form-parameters = 'true' and $query-param-de">
					<xsl:attribute name="value">
						<xsl:value-of select="$query-param-de/h:value"/>
					</xsl:attribute>
				</xsl:if>
			</input>
			<xsl:apply-templates select="." mode="format">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="format" select="$format"/>
				<xsl:with-param name="target-id" select="concat('query', $n, '_de')"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>


	 <!--[MR] : FIXME : n'est plus vraiment utilisé puisqu'on passe par le parametre "forcé" size avec la valeur 4
	 	calculer la taille max d'une chaine qui matche une expression régulière donnée est un exercice difficile :-0
	 -->
	<xsl:template match="key[@type = 'date']" mode="maxlength-size-attributes">
		<xsl:param name="format" select="''"/>
		<xsl:param name="size" select="number(4)"/>

		<xsl:variable name="input_size">
			<xsl:choose>
				<xsl:when test="@max-chars castable as xs:integer and number(@max-chars) &gt; 0">
					<xsl:value-of select="string(@max-chars)" />
				</xsl:when>
				<xsl:when test="$size castable as xs:integer and number($size) &gt; 0">
					<xsl:value-of select="string($size)"/>
				</xsl:when>
				<xsl:when test="contains($format,'-')">
					<xsl:value-of select="(string-length($format) div 2) + 1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="string-length($format) div 2"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$input_size!='' and $input_size!='0'">
			<xsl:attribute name="maxlength">
				<xsl:value-of select="$input_size"/>
			</xsl:attribute>
			<xsl:attribute name="size">
				<xsl:value-of select="$input_size"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>

	<!--TRAITEMENT DES OPERATEURS-->

		<!--criteria-op = operateur "intra-champ"-->

		<!--operateur "et" par défaut pour tout ce qui n'est pas list ou suggest-->
		<xsl:template match="key[not(@type = 'list' or @type='suggest')]" mode="criteria-op">
			<xsl:param name="n" select="''"/>
			<xsl:param name="op-param" select="/"/>
			<xsl:apply-templates select="." mode="ops-html">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="op-param" select="$op-param"/>
				<xsl:with-param name="default-value" select="'AND'"/>
			</xsl:apply-templates>
		</xsl:template>

		<!--operateur "ou" par défaut pour les listes-->
		<xsl:template match="key[@type = 'list'][@variant='combo' or @variant='checkbox' or @variant='radio']" mode="criteria-op">
			<xsl:param name="n" select="''"/>
			<xsl:param name="op-param" select="/"/>
			<xsl:apply-templates select="." mode="ops-html">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="op-param" select="$op-param"/>
				<xsl:with-param name="default-value" select="'OR'"/>
			</xsl:apply-templates>
		</xsl:template>
		<!--TODO : sur les checkbox, afficher le choix du champ intra-champ à la 2eme cochée-->

		<xsl:template match="key[@type = 'list'][@variant='fixed']" mode="criteria-op">
			<xsl:param name="n" select="''"/>
			<xsl:param name="op-param" select="/"/>
			<xsl:apply-templates select="." mode="ops-html">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="op-param" select="$op-param"/>
				<xsl:with-param name="css-default-display" select="'none'"/>
				<xsl:with-param name="default-value" select="'OR'"/>
			</xsl:apply-templates>
		</xsl:template>



		<!--operateur "ou" par défaut pour les suggest-->
		<xsl:template match="key[@type = 'suggest']" mode="criteria-op">
			<xsl:param name="n" select="''"/>
			<xsl:param name="op-param" select="/"/>
			<xsl:apply-templates select="." mode="ops-html">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="op-param" select="$op-param"/>
				<xsl:with-param name="css-default-display" select="'none'"/>
				<xsl:with-param name="default-value" select="'OR'"/>
			</xsl:apply-templates>
		</xsl:template>

		<!--traitement générique des opérateurs intra-champ-->
		<xsl:template match="key" mode="ops-html">
			<xsl:param name="n" select="''"/>
			<xsl:param name="op-param" select="/"/>
			<xsl:param name="default-value" select="'AND'"/>
			<xsl:param name="css-default-display">
				<xsl:choose>
					<xsl:when test="@op-display='true'">
						<xsl:text>block</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>none</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:param>
			<xsl:variable name="value">
				<xsl:choose>
					<xsl:when test="$form-parameters = 'true'">
						<xsl:value-of select="$op-param/h:value"/>
					</xsl:when>
					<xsl:when test="@op!=''">
						<xsl:value-of select="@op"/>
					</xsl:when>
					<xsl:otherwise> <!-- valeur par défaut -->
						<xsl:value-of select="$default-value"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!--DEBUG-->
			<!--<xsl:message>
			css-default-display = '<xsl:value-of select="$css-default-display"/>'
			value = '<xsl:value-of select="$value"/>'
			</xsl:message>-->
			<!-- [CB] ce code était créé même quand on avait pas @op-display. Aucune raison à cela. -->
			<xsl:if test="@op-display">
				<div class="pl-form-ops" style="display:{$css-default-display}" id="ops{$n}">
					<!--TODO : revoir clé i18n pour pouvoir surcharger sur chaque element du formulaire-->
					<input type="radio" name="op{$n}" id="op-and{$n}" value="AND">
						<xsl:if test="$value='AND'">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<xsl:text>&#160;</xsl:text>
					<label for="op-and{$n}">
						<i18n:text key="form.op.and.search-form.{$form-id}">form.op.and.search-form.<xsl:value-of select="$form-id"/></i18n:text>
					</label>
					<xsl:text>&#160;&#160;</xsl:text>
					<input type="radio" name="op{$n}" id="op-or{$n}" value="OR">
						<xsl:if test="$value='OR'">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<xsl:text>&#160;</xsl:text>
					<label for="op-or{$n}">
						<i18n:text key="form.op.or.search-form.{$form-id}">form.op.or.search-form.<xsl:value-of select="$form-id"/></i18n:text>
					</label>
				</div>
			</xsl:if>
		</xsl:template>

		<!--criteria-cop = operateur complexe "extra-champ"-->

		<xsl:template match="key[@connector != '3' or not(@connector)]" mode="criteria-cop">
			<xsl:param name="form-id" select="$form-id"/>
			<xsl:param name="n" select="number(0)"/>
			<xsl:param name="cop-param" select="/"/>
			<input type="{$hidden}" name="cop{$n}">
				<xsl:attribute name="value">
				<xsl:choose>
					<!--<xsl:when test="$cop-param">-->
					<xsl:when test="$form-parameters = 'true'">
						<xsl:value-of select="$cop-param/h:value"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="@connector = '1'">
								<xsl:text>AND</xsl:text>
							</xsl:when>
							<xsl:when test="@connector = '2'">
								<xsl:text>OR</xsl:text>
							</xsl:when>
							<xsl:when test="@connector = '4'">
								<xsl:text>NOT</xsl:text>
							</xsl:when>
							<xsl:otherwise> <!--valeur par défaut-->
								<xsl:text>AND</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				</xsl:attribute>
			</input>
		</xsl:template>
		<xsl:template match="key[@connector = '3'][not( @type='hidden' )]" mode="criteria-cop">
			<xsl:param name="form-id" select="$form-id"/>
			<xsl:param name="n" select="number(0)"/>
			<xsl:param name="cop-param" select="/"/>
			<span class="pl-form-operators">
				<select name="cop{$n}">
					<option value="AND">
						<xsl:if test="$form-parameters = 'true' and $cop-param/h:value = 'AND'">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>
						<i18n:text key="form.connector.and.search-form.{$form-id}">form.connector.and.search-form.<xsl:value-of select="$form-id"/></i18n:text>
					</option>
					<option value="OR">
						<xsl:if test="$form-parameters = 'true' and $cop-param/h:value = 'OR'">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>
						<i18n:text key="form.connector.or.search-form.{$form-id}">form.connector.or.search-form.<xsl:value-of select="$form-id"/></i18n:text>
					</option>
				</select>
				<!--<input type="radio" name="cop{$n}" value="AND" checked="checked" id="query{$n}-cop-or"/>
				<label for="query{$n}-cop-or" class="pl-form-label"><i18n:text key="form.connector.and.search-form.{$form-id}">form.connector.and.search-form.<xsl:value-of select="$form-id"/></i18n:text></label>
				<input type="radio" name="cop{$n}" value="OR" id="query{$n}-cop-and"/>
				<label for="query{$n}-cop-and" class="pl-form-label"><i18n:text key="form.connector.or.search-form.{$form-id}">form.connector.or.search-form.<xsl:value-of select="$form-id"/></i18n:text></label>-->
			</span>
		</xsl:template>
		<xsl:template match="key[@connector = '3'][@type='hidden']" mode="criteria-cop">
			<xsl:param name="form-id" select="$form-id"/>
			<xsl:param name="n" select="''"/>
			<xsl:param name="cop-param" select="/"/>
			<input type="{$hidden}" name="cop{$n}" id="query{$n}-cop-or">
				<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test="$form-parameters = 'true' and $cop-param">
							<xsl:value-of select="$cop-param/h:value"/>
						</xsl:when>
						<xsl:otherwise> <!-- valeur par défaut -->
							<xsl:text>AND</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</input>
		</xsl:template>

	<xsl:template match="key" mode="format">
		<xsl:param name="n" select="number(0)"/>
		<xsl:param name="format" select="@format"/>
		<xsl:param name="target-id" select="concat('query', $n)"/>
		<xsl:if test="$format != ''">
			<a href="javascript:void(0)" class="format" style="display:none;" title="{$format}" target="{$target-id}">
				<xsl:comment>u</xsl:comment>
			</a>
		</xsl:if>
	</xsl:template>

	<!-- Sortie d'un lien vers un panneau permettant de naviguer un index ou un thésaurus -->
	<xsl:template match="key" mode="output-index-link">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:param name="n" select="number(0)"/>
		<xsl:variable name="current" select="."/>
		<xsl:variable name="filter">
			<xsl:for-each select="filters/filter">
				<xsl:value-of select="concat('&amp;field=',@field)"/>
				<xsl:value-of select="concat('&amp;value=',@value)"/>
			</xsl:for-each>
			<!-- FIXME : mieux exprimer la condition, on ne prend en compte les key[@type = 'hidden'] que s'ils sont différents des filtres  -->
			<xsl:for-each select="../key[@type='hidden' and not($current/filters/filter/@*[name() = 'field' or name() = 'value'] = @*[name() = 'field' or name() = 'value'])]">
				<xsl:value-of select="concat('&amp;field=',@field)"/>
				<xsl:value-of select="concat('&amp;value=',@value)"/>
			</xsl:for-each>
		</xsl:variable>
		<!-- L'identifiant de l'index ou du thésaurus -->
		<xsl:variable name="index-id">
			<xsl:choose>
				<xsl:when test="@thesx-id"><xsl:value-of select="@thesx-id"/></xsl:when>
				<xsl:when test="not(@terms-browse)"/>
				<xsl:when test="@terms-browse!='' and not(@terms-browse castable as xs:boolean)"><!-- L'index sur lequel on doit construire le navindex est indiqué dans l'attribut @terms-browse -->
					<xsl:value-of select="@terms-browse" />
				</xsl:when>
				<xsl:when test="@field!=''"><xsl:value-of select="@field"/></xsl:when><!-- L'index sur lequel on doit construire le navindex est dans l'attribut @field -->
				<xsl:when test="@type='date'"><xsl:text>edate</xsl:text></xsl:when><!-- Pour une recherche de type "date"  -->
				<xsl:otherwise>TODO</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Une partie de la clé i18n, légèrement différente pour les dates -->
		<xsl:variable name="fieldOrId">
			<xsl:choose>
				<xsl:when test="@field and not(@type = 'date' and contains(@field, ','))">
					<xsl:text>field.</xsl:text><xsl:value-of select="@field"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>id.</xsl:text><xsl:value-of select="@id"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- S'il s'agit d'un thésaurus, d'un index, ... -->
		<xsl:variable name="type">
			<xsl:choose>
				<xsl:when test="@thesx-id">thesx</xsl:when>
				<xsl:when test="@terms-browse">terms</xsl:when>
				<xsl:otherwise>TODO</xsl:otherwise>	<!-- FIXME: à préciser, mais devrait jamais arriver ici -->
			</xsl:choose>
		</xsl:variable>
		<!-- Le bouton de lien -->
		<div>
			<a class="pl-bttn-index" id="form.panel-index.{$fieldOrId}.{$type}.{$index-id}.{@terms-browse-hpp}.{$filter}-button" href="javascript:void(0);" title="form.{$type}-link.button.title.{$fieldOrId}.search-form.{$form-id}" i18n:attr="title">
				<span class="access"><i18n:text key="form.{$type}-link.button.title.{$fieldOrId}.search-form.{$form-id}">voir l'index</i18n:text></span>
			</a>
		</div>
		<div id="form.panel-index.{$fieldOrId}.{$type}.{$index-id}.{@terms-browse-hpp}.{$filter}" class="panel-index panel-index-{$type}" style="display:none;">
			<div class="hd">
				<i18n:text key="form.{$type}.panel.hd.{$fieldOrId}.search-form.{$form-id}">form.<xsl:value-of select="$type"/>.panel.hd.<xsl:value-of select="$fieldOrId"/>.search-form.<xsl:value-of select="$form-id"/></i18n:text>
			</div>
			<div class="bd" id="form.panel-index.bd.{$fieldOrId}.{$type}.{$index-id}.{$n}">
				<!-- Une intro -->
				<i18n:text key="form.{$type}.panel.intro.{$fieldOrId}.search-form.{$form-id}">form.<xsl:value-of select="$type"/>.panel.intro.<xsl:value-of select="$fieldOrId"/>.search-form.<xsl:value-of select="$form-id"/></i18n:text>
				<!-- Un div pour contenir le contenu comme tel -->
				<div id="index.content.{$index-id}" class="pl-pgd-toc pl-pg-index-{$type}">
					<!-- Ici le contenu dynamique -->
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- Sortie d'une bulle et d'un panneau d'aide -->
	<xsl:template match="key" mode="output-help">
		<xsl:param name="form-id" select="$form-id"/>
		<xsl:variable name="fieldOrId">
			<xsl:choose>
				<xsl:when test="@field and not(@type = 'date' and contains(@field, ','))">
					<xsl:text>field.</xsl:text><xsl:value-of select="@field"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>id.</xsl:text><xsl:value-of select="@id"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div id="form.panel-help-{$fieldOrId}-button">
			<!--<a id="form.tooltip-help-{$fieldOrId}-button" href="javascript:void(0);">-->
				<!-- <img src="images/pl-form-help.png"/> -->
				<div class="pl-help-img"><xsl:comment>u</xsl:comment></div>
			<!--</a>-->
		</div>
		<div id="form.panel-help-{$fieldOrId}" class="panel-help" style="display:none;">
			<div class="hd">
				<i18n:text key="form.help.panel.hd.{$fieldOrId}.search-form.{$form-id}">form.help.panel.hd.<xsl:value-of select="$fieldOrId"/>.search-form.<xsl:value-of select="$form-id"/>.</i18n:text>
			</div>
			<div class="bd">
				<i18n:text key="form.help.panel.bd.{$fieldOrId}.search-form.{$form-id}">form.help.panel.bd.<xsl:value-of select="$fieldOrId"/>.search-form.<xsl:value-of select="$form-id"/></i18n:text>
			</div>
		</div>
		<!--ANCIEN CODE AVEC PANEL YUI-->
		<!--<div>
			<a id="form.panel-help-{$fieldOrId}-button" href="javascript:void(0);" title="form.help.button.title.{$fieldOrId}.search-form.{$form-id}" i18n:attr="title">
				<img src="images/pl-form-help.png" alt="form.help.button.alt.{$fieldOrId}.search-form.{$form-id}" i18n:attr="alt"/>
			</a>
		</div>
		<div id="form.panel-help-{$fieldOrId}" class="panel-help" style="display:none;">
			<div class="hd">
				<i18n:text key="form.help.panel.hd.{$fieldOrId}.search-form.{$form-id}">form.help.panel.hd.<xsl:value-of select="$fieldOrId"/>.search-form.<xsl:value-of select="$form-id"/>.</i18n:text>
			</div>
			<div class="bd">
				<i18n:text key="form.help.panel.bd.{$fieldOrId}.search-form.{$form-id}">form.help.panel.bd.<xsl:value-of select="$fieldOrId"/>.search-form.<xsl:value-of select="$form-id"/></i18n:text>
			</div>
		</div>-->
	</xsl:template>

  <!--+
      | Calcul de la clé i18n pour un critère dépendant d'un autre
      +-->
  <xsl:template match="key" mode="no-choice-updateSelect">

    <xsl:param name="f" select="''" />

    <xsl:variable name="k">
      <xsl:choose>
				<xsl:when test="@i18n-key">
					<xsl:value-of select="@i18n-key" />
				</xsl:when>
        <xsl:when test="no-choice[@i18n-key]">
          <xsl:value-of select="no-choice/@i18n-key" />
        </xsl:when>
        <xsl:when test="items/item[@i18n-key][1]">
          <xsl:value-of select="items/item[@i18n-key][1]/@i18n-key" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>form-no-choice-select</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="k1" select="concat('since-', $f)" />

    <xsl:variable name="k-final">
      <xsl:choose>
        <xsl:when test="contains($k, $k1)">
          <xsl:value-of select="fn:replace(concat(substring-before($k, $k1), substring-after($k, $k1)), '--', '-')" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$k" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$k-final" />

  </xsl:template>

</xsl:stylesheet>
