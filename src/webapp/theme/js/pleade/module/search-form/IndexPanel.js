//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permetant de créer un 'navindex'. L'index contenu du formulaire de recherche
 * @author Julien Huet
 */
/**
 * @class Classe permetant de créer un 'navindex'. L'index contenu du formulaire de recherche
 * @author Julien Huet
 * @augments AjaxRequestHandler
 * @augments YuiPanel
 */
var IndexPanel = new Class(/** @lends IndexPanel */{
	Implements: [AjaxRequestHandler],
	Extends : YuiPanel,

	/**
	* div sur lequel on se base pour creer le panneau
	* @private
	* @type Element
	*/
	div : '',

	/**
	* <strong>Constructor</strong><br/>
	* @param {Element} div div sur lequel on se base pour creer le panneau
	*/
	initialize: function(div){
		// L'attribut et celui du bouton correspondant, qui partagent une même forme
		var id = div.getAttribute('id');
		var idButton = id + '-button';
		this.div = div;
		// La taille du panneau dépend du type
		var width = '250px';
		var height = '200px';
		if ( div.hasClass('panel-index-terms') ) {
			width = '400px';
			height = '600px';
		}
		// On crée le panel YUI
		this.parent(id,width,height);

		// On le prépare
		this.panel.render();
	},

	/**
	* Chargement du contenu du panel
	*/
	loadContent: function(){
		// Des informations importantes proviennent de l'identifiant qui est de la forme:
		// form.panel-index.{champ}.{type}.{index-id}.{hpp}.{filter}
		/* `.{hpp}.{filter}` => Pleade Plus ? */
		// 1					2				3				4				5				6				7
		var id = this.div.getAttribute('id');
		var idParts = id.split('\.');
		var indexId = idParts[5];
		var indexType = idParts[4];
		var hpp = idParts[6];
		var filter = idParts[7];
		var value = '';
		var values = $$('.pl-form-input-' + indexId);
		if ( values!=null && values.lenghth != 0 && value[0]!=null ) {
			value = $(value[0]);
		}

		var bdid = this.div.getChildren('div.bd').getProperty('id') + '';
		var bdidParts = bdid.split('\.');
		var indexNum = bdidParts[7];

		// On doit trouver le div qui contiendra le contenu dans le panel
		var contentDiv = $('index.content.' + indexId);
		// Maintenant on appelle la bonne méthode, selon le type d'index
		if ( indexType == 'thesx' ) this.loadIndexContentThesx(indexId, contentDiv);
		else if (indexType == 'terms' ) this.loadIndexContentTerms(indexId, contentDiv, indexNum, hpp, filter, value);
	},
	
	/**
	* Charge le contenu d'une liste de termes dans un panneau.
	* @param {String}  termsId
	* @param {Element} div
	* @param {Integer} indexNum
	* @param {Object}  hpp
	* @param {Object}  filter
	* @param {Object}  value
	*/
	loadIndexContentTerms: function(termsId, div, indexNum, hpp, filter, value) {
		var filterParam = '';
		if(filter!=null && !filter.blank()){
			filterParam = filter;
		}
		var hppParam = '';
		if ( hpp!='' ) {
			hppParam = '&hpp=' + hpp;
		}
		// La ou les base(s) de documents
		var baseParam = '';
		$$('input.pl-form-srch-base').each( function(o){
			bs = o.value;
			if ( bs!=null ) baseParam += '&base='+ bs;
		});
		// L'URL à appeler au départ
		var url = 'navindex.xsp?context=search-form-panel' + filterParam + '&field=' + termsId + '&indexnum=' + indexNum + hppParam  + baseParam;
		if( navindex_from_input == 'true' && value != '') url += '&value=' + value + '*';

		// Une fonction générique pour charger du contenu dans un div depuis Ajax
		windowManager.loadAjaxContent(url, div);
	},

	/**
	* Charge le contenu d'un thésaurus dans un panneau
	* @param {String}  thesId
	* @param {Element} div
	*/
	loadIndexContentThesx: function(thesId, div) {
		var thesxThree = new ThesXTreeView(thesId, div);
		thesxThree.load();
	}
});
