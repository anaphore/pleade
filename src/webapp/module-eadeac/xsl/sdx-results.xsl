<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSLT qui affiche le résultat d'une recherche sous forme de tableau
    +-->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:xsp="http://apache.org/xsp"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  exclude-result-prefixes="xsl xsp sdx i18n pleade">

    <!-- Pour l'affichage des résultats de recherche -->
    <xsl:import href="sdx-results-data.xsl"/>

    <xsl:import href="cocoon://cdc-results.xsl"/>

  <!--+
      | Construction de la racine XHTML
      +-->
  <xsl:template match="/sdx:document">
  <html>

      <head>

        <title>
          <i18n:text key="results-title.page.{$form-id}">results-title.page.<xsl:value-of select="$form-id" /></i18n:text>
        </title>

        <link type="text/css" href="yui/assets/skins/pleade/treeview.css" rel="stylesheet"/>

        <script type="text/javascript" src="yui/treeview/treeview-min.js">//</script>
        <xsl:if test="$active-basket='true'">
          <script type="text/javascript" src="i18n/module-eadeac-basket-js.js">//</script>
        </xsl:if>
        <script type="text/javascript" src="i18n/module-eadeac-js.js">//</script>
        <script type="text/javascript">
          <xsl:comment>
            window.addEvent("load", pageLoaded);
            // Function to bind Events on pagination links
            function pageLoaded(){
              $$('#<xsl:value-of select="$real-div-id"/>_pagination a').each(function(n){
                $(n).addEvent('click', changePage);
              });
            }

            // Function that change page, according to current link's href
            function changePage(e){
              new Request.HTML({
                method: 'get',
                url: this.href.replace(/\.html/g, '.ajax-html'),
                update: $('<xsl:value-of select="$real-div-id"/>'),
                onSuccess: pageLoaded //Pagination links has been reloaded, we have to rebind them
              }).send();
              Event.stop(e);
            }
            <xsl:if test="sdx:parameters/sdx:parameter[@name = 'linkBack']/@value = 'true'">
              <xsl:text>
                function back2form(){
                  var re = new RegExp("name=([^&amp;]+)");
                  var m = re.exec(location.href);
                  if (m == null || m[1] == null || m[1] == '' ) {
                    //pas de nom ici, on remplace simplement 'results.html' par 'search-form.html'
                    location.href = location.href.replace("results.","search-form.");
                  } else { // on a un nom, on l'utilise !
                    var reurl = new RegExp("[^/]*results\.html", "g");
                    location.href = location.href.replace(reurl, m[1] + '-search-form.html');
                  }
                }
              </xsl:text>
            </xsl:if>
          //</xsl:comment>
        </script>

      </head>

      <body>

          <xsl:apply-templates select="." mode="pl-title" />

          <i18n:text key="results-intro.query"/>

          <!--affichage des critères de recherche-->
          <xsl:apply-templates select="sdx:parameters" mode="criteria"/>

          <!--affichage des résultats-->
          <xsl:apply-templates select="sdx:results | message" />

          <i18n:text key="results-intro.results"/>

          <div id="{$real-div-id}">
            <xsl:call-template name="display-results"/>
          </div>

        <!-- On prépare l'appel qui laisse une trace dans les logs de statistiques -->
        <xsl:variable name="bases">
          <xsl:for-each select="/sdx:document/sdx:parameters/sdx:parameter[@name='base' and @value!='']">
            <xsl:value-of select="concat( if(position()=1) then '?' else '&amp;','base=', @value )" />
          </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="stats-url" select="concat('cocoon:/stats.xml?base=', $bases, '&amp;name=', /sdx:document/sdx:parameters/sdx:parameter[@name='name']/@value, '&amp;nb-crit=', /sdx:document/criteres/@nb, '&amp;nb-docs=', /sdx:document/sdx:terms[@name = 'root-id']/@nb, '&amp;nb-units=', /sdx:document/sdx:results/@nb, '&amp;query=')"/>
        <div id="pl-results-stats-info">
          <xsl:value-of select="$stats-url"/>
        </div>
      </body>

    </html>
  </xsl:template>

  <!--+
      | Les scripts nécessaires à la construction de la liste des résultats
      +-->
  <xsl:template match="sdx:document" mode="cdc-results-script">
    <!-- JC À priori, on a pas besoin de JS particulier pour ce cas -->
  </xsl:template>

  <xsl:template match="sdx:results">
    <p class="pl-results-count">
      <i18n:translate>
        <i18n:text key="results.nbresults"/>
        <i18n:param><xsl:value-of select="@nb"/></i18n:param>
      </i18n:translate>
    </p>
  </xsl:template>

</xsl:stylesheet>
