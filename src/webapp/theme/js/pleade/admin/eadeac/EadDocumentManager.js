//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion des documents EAD dans la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion des documents EAD dans la partie admin
 * @author Julien Huet
 */
var EadDocumentManager = new Class(/** @lends EadDocumentManager.prototype */{
	
	/**
	 * <strong>Constructor</strong><br/> Initialisation du formulaire de gestion de document EAD
	 * @param {String} base
	 */
	initialize: function(base){
		this.eadManageButtons(base);
	},
	
	/**
	 * Fonction permettant d'inialiser les boutons du formulaire de gestion de document EAD
	 * @param {String} _current_base
	 */
	eadManageButtons: function(_current_base){
		//alert("EditForm.js eadManageButtons()");
		var _reg = new RegExp('pl-form-button[1-3]([0-9]+)');
		$$('.pl-form-button').each(function(bt){
			bt.addEvent('click', function(e){
				var _id = $('did' + _reg.exec(this.id)[1]).get('value');
				if ( this.id.startsWith('pl-form-button1') ) { // DELETE case
					if( confirm(_usersMessages.ead_admin_delete_file_confirm_text) ){
						window.location.href= _current_base + '-delete.html?delete=true&id=' + _id;
					}
				} else if (this.id.startsWith('pl-form-button2') ) { // REPUBLISH case
					window.location.href= _current_base + '-publish-form.html?id=' + _id;
				} else if (this.id.startsWith('pl-form-button3') ) { // DISPLAY PROPERTIES case
					window.location.href='display-params.html?id=' + _id;
				}
				Event.stop(e);
			});
		});
	}

});
