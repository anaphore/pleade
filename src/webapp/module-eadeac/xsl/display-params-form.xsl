<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Construction du formulaire de gestion des paramètres d'affichage
		+-->
<!-- FIXME: Bloquer l'accès aux paramètres généraux en forçant l'utilisation
d'un paramètre "id" indiquant l'identifiant du doc EAD/EAC ? [MP] -->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		exclude-result-prefixes="xsl xsp i18n dir">

	<!-- Imports -->
	<xsl:include href="form2xhtml-commons.xsl"/>

	<xsl:param name="id" select="''" />

	<!-- Séparateur pour les listes -->
	<xsl:variable name="sep" select="string(' ')" />

	<!-- Les propriétés d'affichage du document. Contient juste nom et valeur -->
	<xsl:variable name="doc-prop" select="concat('cocoon:/edit/functions/edit-publish/get-properties-display.xml?id=', $id)" />
	<xsl:variable name="current-params" select="
			if(doc-available($doc-prop)) then document($doc-prop)
			else ''
		"/>
	<!-- Les propriétes d'affichage par défaut. Contient nom, valeur et état -->
	<xsl:variable name="default-prop" select="'cocoon://functions/ead/get-properties/display.xml'" />
	<xsl:variable name="def-params" select="
			if(doc-available($default-prop)) then document($default-prop)
			else ''
		"/>
	
	<xsl:variable name="search-forms" select="/root/dir:directory | /root/directory" />

	<!--+
	    | Construction de la balise <html>
	    +-->
	<xsl:template match="/root">

		<html>

			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<title>
					<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.html.title">
						<xsl:text>modifier les paramètres d'affichage</xsl:text>
					</i18n:text>
				</title>
				<!--yui-->
				<script type="text/javascript" src="yui/datasource/datasource-min.js">//</script>
				<script type="text/javascript" src="yui/autocomplete/autocomplete-min.js">//</script>
				<link type="text/css" href="yui/assets/skins/pleade/autocomplete.css" rel="stylesheet" />
				<!--pleade-->
				<!--<script type="text/javascript" src="i18n/module-eadeac-edit-js.js">//</script>-->
				<!--<script type="text/javascript" src="js/module-eadeac.js">//</script>-->
				<script type="text/javascript" src="js/module-eadeac-edit.js">//</script>
				<!-- Test Migration -->
					<script type="text/javascript" src="js/pleade/common/manager/SuggestManager.js">//</script>	
					<script type="text/javascript" src="js/pleade/admin/eadeac/AdminSuggest.js">//</script>
					<script type="text/javascript" src="js/pleade/admin/eadeac/EditFormValidator.js">//</script>
					<script type="text/javascript" src="js/pleade/admin/eadeac/DisplayParamsForm.js">//</script>
	
				
				<script type="text/javascript">
					var sep="<xsl:value-of select="$sep" />";
					var isSuggests=false;
					var suggests;
					var displayParamForm = '';

					<!-- Lance l'initialisation des éléments -->
					window.addEvent("load", init);
					//window.addEvent("load", initSuggests);

					function init(){
						displayParamForm = new DisplayParamsForm([<xsl:apply-templates select="index-list[@id='index-local']" mode="JSArray" />]);
					}
					
					<!-- Fonction d'initialisation des suggests -->
					function initSuggests() {
						alert('display-params-form.xsl initSuggests()');
						<!--sélection des suggest : on prend le div/@class=suggest
						qui contient tout ; il a l'@id qui covient-->
						suggests = $$('div.suggest');

						if( suggests==null || suggests.length==0 ) return;

						var _part; var mid;
						$A(suggests).clean().each( function(o,i) {
							mid = o.id;
							if ( mid==null || mid.blank() );
							else if ( mid=="index-local" ) {
								_part = [<xsl:apply-templates select="index-list[@id='index-local']" mode="JSArray" />];
							}
							var _src = new YAHOO.widget.DS_JSArray( _part );
							var idi = mid + "-input",
									idc = mid + "-container",
									idv = mid + "-values";
							if( $(idi)==null || $(idc)==null || $(idv)==null ) {
									suggests.splice(1,1);
							}
							else {
								var autoComp = new YAHOO.widget.AutoComplete(idi,idc, _src);
								autoComp.queryDelay = 0.1;
								autoComp.prehighlightClassName = "yui-ac-prehighlight";
								autoComp.useShadow = true;
								autoComp.animHoriz = true;
								autoComp.allowBrowserAutocomplete = false;
								autoComp.delimChar = "<xsl:value-of select="$sep" />";
								autoComp.doBeforeExpandContainer = function(oTextbox, oContainer, sQuery, aResults) {
									var pos = YAHOO.util.Dom.getXY(oTextbox);
									pos[1] += YAHOO.util.Dom.get(oTextbox).offsetHeight + 2;
									YAHOO.util.Dom.setXY(oContainer,pos);
									return true;
								};
								autoComp.itemSelectEvent.subscribe(function(event, obj) {
										var value = obj[2][0];	// l'objet est un resultat format JSON suivant le schema definit plus haut
										$(idv).update($(idv).innerHTML + '<div class="pl-form-suggest-value"><img onclick="javascript: this.parentNode.dispose();" src="images/pl-form-delete.png" alt="Supprimer" /><span> ' + value + '</span></div>');
										// $(idi).value=$(idi).value.replace(value,"").trim();
										$(idi).value = "";
								});
								isSuggests = true;
							}
						});
					}
					
				</script>
			</head>

			<body>

				<h1 class="pl-title">
					<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.pl-title">
						<xsl:text>modifier les paramètres d'affichage</xsl:text>
					</i18n:text>
					<!--xsl:apply-templates select="properties/property[@name='titleproper']" mode="pl-title" /-->
					<xsl:apply-templates select="$current-params/properties/property[@name='titleproper']" mode="pl-title" />
				</h1>

				<div class="pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.result.{sourceResult/execution}">
						<xsl:value-of select="sourceResult/execution" />
					</i18n:text>
				</div>

				<div class="pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.intro" />
				</div>

				<form action="display-params.html" method="get"
							id="display-params-manage-form" class="pl-form-display-params">

					<input type="hidden" name="id" value="{$id}" />
					<input type="hidden" name="modify" value="false" />

					<xsl:choose>
						<!-- [CB] les current-params n'ont pas de @read-only -->
						<xsl:when test="$current-params/properties/property[@name = $def-params/properties/property[not(@read-only='true')]/@name]">
							<!-- On n'affiche que les propriétés modifiables -->
							<xsl:apply-templates select="$current-params/properties/property[@name = $def-params/properties/property[not(@read-only='true')]/@name]" />
						</xsl:when>
						<xsl:otherwise>
							<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.no-params">
								<p>Il n'existe pas de paramètre d'affichage modifiable.</p>
							</i18n:text>
						</xsl:otherwise>
					</xsl:choose>

					<div class="pl-form-submit">
						<button type="button" id="pl-form-button1" title="module-eadeac-edit:edit.display-params-manage.submit.title"
								class="yui-button yui-push-button pl-form-search-submit" i18n:attr="title"
								onclick="javascript: var ret=displayParamForm.submitForm(this.form); if(ret==true) this.form.submit();">
							<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.submit.text">valider...</i18n:text>
						</button>
						&#160;
						<button type="reset" id="pl-form-button1"
									class="yui-button yui-reset-button"
									title="module-eadeac-edit:edit.display-params-manage.submit.reset.title" i18n:attr="title"
									onclick="javascript:window.location.href='?id={$id}';return false;">
									<!--onclick="javascript:this.form.reset();resetSuggests();var mod=this.form.elements['modify']; if(mod!=null) if(mod.value!=null) mod.value='false';"-->
							<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.submit.reset.text">effacer...</i18n:text>
						</button>
					</div>

				</form>

				<div class="pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.conclusion" />
					<xsl:if test="sourceResult[execution='failure' and message]">
						<textarea cols="80" rows="2" style="white-space:preserve;">
							<xsl:value-of select="sourceResult/message/text()" />
						</textarea>
					</xsl:if>
					<!--textarea cols="80" rows="10" nowrap="nowrap" style="white-space:preserve;">
						<xsl:copy-of select="document(concat('cocoon:/edit/functions/edit-publish/get-properties-display.xml?id=', $id))" />
					</textarea>
					<xsl:if test="sourceResult">
						<textarea cols="80" rows="5" nowrap="nowrap" style="white-space:preserve;">
							<xsl:copy-of select="sourceResult" />
						</textarea>
					</xsl:if-->
				</div>

			</body>

		</html>

	</xsl:template>

	<!--+
	    | Le titre du doc EAD/EAC
	    +-->
	<xsl:template match="property" mode="pl-title">

		<xsl:if test=".!=''">
			<xsl:text>&#160;: &quot;</xsl:text>
			<xsl:apply-templates />
			<xsl:text>&quot;</xsl:text>
		</xsl:if>

	</xsl:template>

	<!--+
	    |	Présentation des propriétés
	    +-->
	<xsl:template match="property">

		<xsl:variable name="n" select="@name" />
		<xsl:variable name="p" select="position()" />
		<xsl:variable name="d">
			<xsl:choose>
				<xsl:when test="normalize-space(.)!=''">
					<xsl:value-of select="normalize-space(.)" />
				</xsl:when>
				<xsl:when test="@default!=''">
					<xsl:value-of select="@default" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<!-- Construction des outils d'aide -->
		<xsl:call-template name="build-help">
			<xsl:with-param name="v" select="$n" />
			<xsl:with-param name="k" select="concat('edit.display-params-manage.field.', $n)"/>
		</xsl:call-template>

		<fieldset id="fieldset_{$n}">

			<legend>
				<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.field.{$n}.legend">
					<xsl:value-of select="$n" />
				</i18n:text>
			</legend>

			<table border="0" cellspacing="0" cellpadding="0" summary="{$n}">

				<tbody>

					<xsl:choose>

						<xsl:when test="$n='search-form'">
							<xsl:apply-templates select="." mode="search-form">
								<xsl:with-param name="n" select="$n" />
								<xsl:with-param name="d" select="$d" />
								<xsl:with-param name="p" select="$p" />
							</xsl:apply-templates>
						</xsl:when>

						<xsl:when test="$n='index-local'">
							<xsl:apply-templates select="." mode="index-local">
								<xsl:with-param name="n" select="$n" />
								<xsl:with-param name="d" select="$d" />
								<xsl:with-param name="p" select="$p" />
							</xsl:apply-templates>
						</xsl:when>

						<xsl:when test="$n='display-navigation'">
							<xsl:apply-templates select="." mode="display-navigation">
								<xsl:with-param name="n" select="$n" />
								<xsl:with-param name="d" select="$d" />
								<xsl:with-param name="p" select="$p" />
							</xsl:apply-templates>
						</xsl:when>

						<xsl:otherwise>
							<xsl:apply-templates select="." mode="default">
								<xsl:with-param name="n" select="$n" />
								<xsl:with-param name="d" select="$d" />
								<xsl:with-param name="p" select="$p" />
							</xsl:apply-templates>
						</xsl:otherwise>

					</xsl:choose>

				</tbody>

			</table>

		</fieldset>

	</xsl:template>

	<!--+
	    |	Propriété "search-form"
	    +-->
	<xsl:template match="property" mode="search-form">

		<xsl:param name="n" />
		<xsl:param name="p" />
		<xsl:param name="d" />


		<tr>
			<td class="pl-form-label"> </td>
			<td>
				<select name="{$n}" tabindex="{$p}" class="pl-form-slct">
					<option value="{$d}">
						<xsl:if test="not($search-forms/*)">
							<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.field.search-form.empty">
								<xsl:text>pas de formulaire disponible</xsl:text>
							</i18n:text>
						</xsl:if>
					</option>
					<xsl:for-each select="$search-forms/*[self::dir:file or self::file]">
						<xsl:variable name="nm" select="@name"/>
						<xsl:variable name="v" select="substring-before($nm, '.xconf')"/>
<!--<xsl:message>
nm=<xsl:value-of select="$nm" />
v=<xsl:value-of select="$v" />
d=<xsl:value-of select="$d" />
</xsl:message>-->
						<option value="{$v}">
							<xsl:if test="$v=$d">
								<xsl:attribute name="selected">
									<xsl:text>selected</xsl:text>
								</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="$v" />
						</option>
					</xsl:for-each>
				</select>
			</td>
		</tr>

	</xsl:template>

	<!--+
	    |	Propriété "display-navigation"
	    +-->
	<xsl:template match="property" mode="display-navigation">

		<xsl:param name="n" />
		<xsl:param name="p" />
		<xsl:param name="d" />

		<xsl:variable name="vs" select="'true,false'" />
		<xsl:for-each select="tokenize($vs, ',')">
			<xsl:variable name="v" select="." />
			<xsl:if test="$v!=''">
				<tr>
					<td class="pl-form-label">
						<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.field.{$n}.label.{$v}">
							<xsl:value-of select="$v" />
						</i18n:text>
					</td>
					<td>
						<input type="radio" name="{$n}"
									id="{$n}{$v}" class="pl-form-npt-rd"
									value="{$v}"  tabindex="{$p}">
							<xsl:if test="$d=$v">
								<xsl:attribute name="checked">
									<xsl:text>true</xsl:text>
								</xsl:attribute>
							</xsl:if>
						</input>
					</td>
				</tr>
			</xsl:if>
		</xsl:for-each>

	</xsl:template>

	<!--+
	    |	Propriété "index-local"
	    +-->
	<xsl:template match="property" mode="index-local">

		<xsl:param name="n" />
		<xsl:param name="p" />
		<xsl:param name="d" />

		<tr>
			<td class="pl-form-label"> </td>
			<td>
				<div class="suggest" id="{$n}">
					<input type="text" name="{$n}" tabindex="{$p}"
							id="{$n}-input" class="pl-form-txt pl-suggest" />
					<div id="{$n}-container">
						<xsl:comment>u</xsl:comment>
					</div>
				</div>
				<div class="pl-form-suggest-values"
							id="{$n}-values">
					<xsl:call-template name="build-div-suggest-values">
						<xsl:with-param name="i" select="$n" />
						<xsl:with-param name="d" select="$d" />
						<xsl:with-param name="sep" select="$sep" />
					</xsl:call-template>
					<xsl:comment>u</xsl:comment>
				</div>
			</td>
		</tr>

	</xsl:template>

	<!--+
	    |	Gestion d'une propriété. Construction générique
	    +-->
	<xsl:template match="property" mode="default">

		<xsl:param name="n" />
		<xsl:param name="p" />
		<xsl:param name="d" />

		<tr>
			<td class="pl-form-label">
				<i18n:text catalogue="module-eadeac-edit" key="edit.display-params-manage.field.{$n}.label">
					<xsl:text> </xsl:text>
				</i18n:text>
			</td>
			<td>
				<input type="text" name="{$n}" tabindex="{$p}" id="{$n}"
							class="pl-form-txt" value="{$d}" />
			</td>
		</tr>

	</xsl:template>

</xsl:stylesheet>
