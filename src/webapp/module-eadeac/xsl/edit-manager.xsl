<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Gestion de base de documents simple.
    | La base de document n'est pas pré-déterminée.
    | La gestion est simple :
    | 1- tout supprimer
    | 2- ajouter un document XML
    | 3- ajouter un ZIP
    |
    | Le formulaire est construit sur la base d'un SDXTerms "sdxall:1".
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:xsp-session="http://apache.org/xsp/session/2.0"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml xsp-session pleade">

	<!-- Imports -->
  <xsl:import href="../../commons/xsl/functions.xsl"/>
	<xsl:include href="form2xhtml-commons.xsl"/>

  <xsl:param name="base" select="'bib'" />
  <xsl:param name="encoding" select="'UTF-8'" />

	<!-- Définition de paramètres qui vont permettre d'ajuster les clés i18n et le catalogue -->
  <xsl:param name="i18n-root" select="concat('edit.',$base,'-manager.form')" />
  <xsl:param name="i18n-cat" select="'module-eadeac-edit'" />

	<!--+
	    | Matche la racine et envoie le traitement du template XHTML
	    +-->
	<xsl:template match="/sdx:document">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <xsl:apply-templates select="." mode="head-title" />
        <xsl:apply-templates select="." mode="head-script-links" />
        <xsl:apply-templates select="." mode="head-script" />
      </head>
      <xsl:apply-templates select="." mode="body" />
    </html>
	</xsl:template>

  <!--+
      | Le titre de la page HTML
      +-->
  <xsl:template match="sdx:document" mode="head-title">

    <title>
      <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.title">gestion de la base <xsl:value-of select="$base" /></i18n:text>
    </title>

  </xsl:template>

  <!--+
      | Le titre de niveau 1
      +-->
  <xsl:template match="sdx:document" mode="pl-title">

    <div class="pl-title">
      <xsl:call-template name="build-help">
        <xsl:with-param name="v" select="'title'" />
        <xsl:with-param name="k" select="concat($i18n-root, '.title')" />
				<xsl:with-param name="c" select="$i18n-cat"/>
      </xsl:call-template>
      <h1>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.pl-title">gestion de la base <code><xsl:value-of select="$base" /></code></i18n:text>
      </h1>
    </div>

  </xsl:template>

  <!--+
      | Les liens vers les scripts dans l'en-tête
      +-->
  <xsl:template match="sdx:document" mode="head-script-links">
    <script type="text/javascript" src="yui/datasource/datasource-min.js">//</script>
    <script type="text/javascript" src="yui/tabview/tabview-min.js">//</script>
    <link type="text/css" href="yui/assets/skins/pleade/tabview.css" rel="stylesheet" />
    <script type="text/javascript" src="i18n/module-eadeac-edit-js.js">//</script>
    <script type="text/javascript" src="js/pleade/common/manager/SuggestManager.js">//</script> 
    <script type="text/javascript" src="js/pleade/admin/eadeac/AdminSuggest.js">//</script>
    <script type="text/javascript" src="js/pleade/admin/eadeac/EditFormValidator.js">//</script>
    <script type="text/javascript" src="js/pleade/admin/eadeac/EditForm.js">//</script>
    <xsl:call-template name="head-script-links-more"/>
  </xsl:template>

  <!-- Pour les surcharges -->
  <xsl:template name="head-script-links-more"/>

  <!--+
      | Les JavaScript dans l'en-tête
      +-->
  <xsl:template match="sdx:document" mode="head-script">
    <script type="text/javascript">
      <xsl:comment>
      var editForm = '';
      window.addEvent("load", init);
      function init() {
        editForm = new EditForm();
      }
      var lfc="";
      //</xsl:comment>
    </script>
  </xsl:template>

  <!--+
      | Le BODY
      +-->
  <xsl:template match="sdx:document" mode="body">

    <body>
      <xsl:apply-templates select="." mode="pl-title" />
      <xsl:apply-templates select="." mode="intro" />
      <div class="pl-form-container">
        <div class="pl-form-ead-admin">
					<xsl:call-template name="display-form"/>
        </div>
      </div>
    </body>

  </xsl:template>
	
	<!-- Affiche le formulaire et différentes parties. Ce template permet de surcharger l'ordre de ces composants. -->
	<xsl:template name="display-form">
		<xsl:apply-templates select="." mode="form-first" />
		<xsl:apply-templates select="." mode="form-delete-all" />
		<xsl:apply-templates select="." mode="form-upload" />
		<xsl:apply-templates select="." mode="form-last" />
	</xsl:template>

  <!--+
      | L'introduction
      +-->
  <xsl:template match="sdx:document" mode="intro">

    <div class="pl-form-message pl-form-intro">
      <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.intro">
        Cette page vous permet de gérer les documents de la base courante. Cette
        gestion est des plus simples : supprimer tous les documents, publier des
        documents. La publication peut se faire via un fichier XML (repésentant un
        unique document) ou une archive ZIP contenant un ou plusieurs fichiers XML.
        <br/>Ce fichier ou cette archive ZIP doivent être présents sur votre poste
        de travail.
      </i18n:text>
      <xsl:comment>u</xsl:comment>
    </div>

    <div id="message" class="pl-form-message">
      <xsl:comment>u</xsl:comment>
    </div>

  </xsl:template>

  <!--+
      | Contrôle : supprimer tous les documents de cette base
      +-->
  <xsl:template match="sdx:document" mode="form-delete-all">

      <xsl:apply-templates select="." mode="form-delete-all-hidden-params" />
      <xsl:apply-templates select="." mode="form-delete-all-fieldset" />

  </xsl:template>

  <!--+
      | Contrôle : supprimer tous les documents de cette base
      | Construction du FieldSet
      +-->
  <xsl:template match="sdx:document" mode="form-delete-all-fieldset">

    <xsl:variable name="docs">
      <xsl:choose>
        <xsl:when test="sdx:terms/sdx:term[1]/@docs">
          <xsl:value-of select="number(sdx:terms/sdx:term[1]/@docs)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="number(0)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="n">
      <xsl:choose>
        <xsl:when test="function-available('pleade:format-number')">
          <xsl:value-of select="pleade:format-number($docs)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$docs" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:call-template name="build-help">
      <xsl:with-param name="v" select="'delete-all'" />
      <xsl:with-param name="k" select="concat($i18n-root, '.delete-all')" />
			<xsl:with-param name="c" select="$i18n-cat"/>
    </xsl:call-template>
    <fieldset id="fieldset_delete-all">
      <legend>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.delete-all.legend">
          supprimer tous les documents
        </i18n:text>
      </legend>
      <div class="pl-form-message pl-form-intro">
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.delete-all.intro">
          Ici, vous pouvez supprimer l'ensemble des documents de cette base en une
          seule action. Attention, cette action de suppression est relativement
          lente, et plus il y a de documents à supprimer, plus la suppression prend
          de temps.
        </i18n:text>
        <xsl:comment>u</xsl:comment>
      </div>
      <button id="pl-dlt-all" class="yui-button yui-push-button pl-dlt-bttn"
          title="{$i18n-cat}:{$i18n-root}.delete-all.button.title"
          onclick="return !(deleteAll('{$base}', 'pl-dlt-all'));"
          onkeypress="return !(deleteAll('{$base}', 'pl-dlt-all'));">
        <xsl:if test="$docs &lt;= 0">
          <xsl:attribute name="disabled">
            <xsl:text>disabled</xsl:text>
          </xsl:attribute>
        </xsl:if>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.delete-all.button.text">
          supprimer
        </i18n:text>
        <xsl:value-of select="concat(' ', $n, ' ')" />
        <xsl:choose>
          <xsl:when test="$docs &gt; 1">
            <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.documents">documents</i18n:text>
          </xsl:when>
          <xsl:otherwise>
            <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.document">document</i18n:text>
          </xsl:otherwise>
        </xsl:choose>
      </button>
    </fieldset>

  </xsl:template>

	<!--+
			|	Contrôle "Envoyer un fichier ou un dossier à publier sur le poste de travail"
			+-->
	<xsl:template match="sdx:document" mode="form-upload">

    <form action="ead-publish.html" enctype="multipart/form-data" method="POST"
              id="edit-publish-form" class="pl-form-edit-publish">

        <xsl:apply-templates select="." mode="form-upload-hidden-params" />
				<xsl:apply-templates select="." mode="form-upload-modes"/>

      </form>

	</xsl:template>

  <!--+
      | Les paramètres cachés pour le contrôle "Sélectionner un fichier ou un
      | dossier à publier"
      +-->
  <xsl:template match="sdx:document" mode="form-upload-hidden-params">

    <input type="hidden" name="base" value="{$base}"/>
    <input type="hidden" name="encoding" value="{$encoding}"/>

  </xsl:template>
	
	<!-- Pour permettre d'avoir plusieurs modes d'upload -->
	<xsl:template match="sdx:document" mode="form-upload-modes">
    <xsl:apply-templates select="." mode="form-upload-fieldset" />
	</xsl:template>

  <!--+
      | Le FieldSet affichant le contrôle "Sélectionner un fichier ou un dossier
      | à publier"
      +-->
  <xsl:template match="sdx:document" mode="form-upload-fieldset">

    <xsl:call-template name="build-help">
      <xsl:with-param name="v" select="'docs-zip'" />
      <xsl:with-param name="k" select="concat($i18n-root, '.docs-zip')" />
			<xsl:with-param name="c" select="$i18n-cat"/>
    </xsl:call-template>
    <fieldset id="fieldset_docs-zip">
      <legend>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs-zip.legend">
          envoyer un fichier à publier
        </i18n:text>
      </legend>
      <div class="pl-form-message pl-form-intro">
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs-zip.intro">
          <xsl:comment>u</xsl:comment>
          Ce contrôle vous permet de publier un document (fichier XML) ou un
          ensemble de documents contenus dans une archive ZIP depuis votre poste de travail.
        </i18n:text>
      </div>
      <input id="docs-zip" class="pl-form-npt-file" size="40" name="zipz" type="file" onchange="javascript: editForm.valideLfc(this);" />
      <div class="pl-form-submit">
        <button type="button" id="pl-form-button1"
            title="{$i18n-cat}:{$i18n-root}.docs-zip.submit.title"
            class="yui-button yui-push-button pl-form-search-submit" i18n:attr="title"
            onclick="javascript: var ret=editForm.simpleForm2work(this.form); if(ret==true) this.form.submit();">
          <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs-zip.submit.text">publier...</i18n:text>
        </button>
        &#160;
        <button type="reset" id="pl-form-button1"
              class="yui-button yui-reset-button"
              title="{$i18n-cat}:{$i18n-root}.docs-zip.reset.title" i18n:attr="title"
              onclick="javascript:this.form.reset();">
          <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.docs-zip.reset.text">effacer...</i18n:text>
        </button>
      </div>
    </fieldset>

  </xsl:template>

  <!--+
      | Contrôles supplémentaires
      +-->
  <xsl:template match="sdx:document" mode="form-first">
    <!-- Rien ici. Ca permet de surcharger en locale si on le souhaite -->
  </xsl:template>
  <xsl:template match="sdx:document" mode="form-last">
    <!-- Rien ici. Ca permet de surcharger en locale si on le souhaite -->
  </xsl:template>

</xsl:stylesheet>
