<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
								xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:h="http://apache.org/cocoon/request/2.0"
								xmlns:e="http://apache.org/cocoon/error/2.1"
								exclude-result-prefixes="xsl h e">

	<xsl:param name="result" select="'NULL'" />

	<xsl:template match="/h:request | /request">

		<xsl:variable name="src" select="h:*[self::h:requestParameters or self::h:attributes]/h:parameter[@name='src']/h:value" />
		<xsl:variable name="dst" select="h:*[self::h:requestParameters or self::h:attributes]/h:parameter[@name='dest']/h:value" />

		<copysource>

			<src>
				<xsl:choose>
					<xsl:when test="$src!=''">
						<xsl:value-of select="$src" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>NULL</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</src>
			<dest>
				<xsl:choose>
					<xsl:when test="$dst!=''">
						<xsl:value-of select="$dst" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>NULL</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</dest>

			<result>
				<xsl:value-of select="$result" />
			</result>

		</copysource>

	</xsl:template>

	<xsl:template match="/e:notify | /notify">
		<copysource>
			<error>
				<message><xsl:apply-templates select="e:message/text() | message/text()" /></message>
				<description><xsl:apply-templates select="e:description/text() | description/text()" /></description>
			</error>
		</copysource>
	</xsl:template>

</xsl:stylesheet>
