<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:error="http://apache.org/cocoon/error/2.1">
    <xsl:template match="error:notify">
        <html>
            <head>
                <title>
                    <xsl:value-of select="@error:type"/>:<xsl:value-of select="error:title"/>
                </title>
                <style type="text/css">
/* typo */

		html, body, p, td, ol, ul, dl
	{ font-family: Verdana, sans-serif;  font-weight: normal;}
		small, strong, .alert, .bar a
	{ font-family: Frutiger, Trebuchet, Arial, sans-serif;  }
		h1, big
	{ font-family: Impact, sans-serif; font-weight: bold;  }
		em, q, blockquote
	{ font-family: Garamond, Georgia, serif; font-weight: 100; }
		notag
	{ font-family: Script, cursive; }
		notag
	{ font-family: "Comic Sans MS", fantasy; }
		strong
	{ font-family: Arial, sans-serif ; font-weight: bold; }
		code, kbd, button, .button
	{font-size: 90%; font-family: "Lucida Console", monospace; }
		notag
	{ font-family: Courier, "MS Courier New", monospace; }

		.highlight strong
	{ font-size: small; display: block;  text-align: left;
	 border-bottom: 1pt solid; text-decoration: underline; }
		.highlight em
	{text-align: right; display: block;  font-size: large; }

/* 1er niveau */

		h1
	{ font-size: xx-large; text-align: right; }
		.highlight  div.text
	{  font-weight: normal; padding: 1ex; padding-left: 2em; border: inset 2px; }

	/* colors-system */

		body, .highlight div.text, div.section .highlight strong, div.section .highlight em
	{ color: MidnightBlue; background-color: WhiteSmoke; }



@media all {
		a 
	{ color: Background; }
		a:visited 
	{ color: GrayText}
		a:hover 
	{ color: green }

		kbd, menu, .menu, table.bar
	{ background-color: menu; color: WindowText; }
		.table.bar a
	{ border-color: menu }
		menu a, menu a:visited, .menu a, .menu a:visited, table.bar a, table.bar a:visited
	{ color: MenuText}
		menu a, .menu a
	{ text-decoration: none; background-color: transparent}
		menu a:hover, .menu a:hover
	{ background-color: Highlight; color: HighlightText; }
		menu a:active, .menu a:active
	{ border-top: 1pt solid; border-bottom: 1pt solid; color: red; font-weight: 900; }
	
	
		strong 
	{ color: Highlight; }
		.highlight 
	{ background-color: Highlight; color: HighlightText; }
		.highlight a
	{ color: InfoBackground; font-weight: bold; }
		.highlight a:hover
	{ color: HighlightText; font-weight: bold; }

}

</style>
                <script language="JavaScript1.2"><![CDATA[
    <!--
      var head="display:''"
      function expand(whatToExpand)
      {
        var head=whatToExpand.style
        if (head.display=="none"){
          head.display=""
        }
        else{
          head.display="none"
        }
      }
     //-->]]></script>
            </head>
            <body>
                <table cellpadding="2" cellspacing="5" border="0"style="width:100%;">
                    <tr>
                        <td class="highlight" colspan="2">
                            <h1> SDX message &#160;</h1>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right">
                            <strong> Source </strong>>
                        </td>
                        <td>
                            <xsl:value-of select="@error:sender"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right">
                            <strong> Exception </strong>>
                        </td>
                        <td>
                            <xsl:value-of select="error:source"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right">
                            <strong> Message </strong>>
                        </td>
                        <td>
                            <xsl:call-template name="returns2br">
                                <xsl:with-param name="string" select="error:message"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;vertical-align:top">
                            <strong> Details </strong>>
                        </td>
                        <td>
                            <small>
                                <xsl:apply-templates select="error:description" mode="returns2br"/>
                            </small>
                        </td>
                    </tr>
                    <tr>
                        <td class="highlight" style="vertical-align:top" colspan="2">
                            <strong>extra info</strong>
                        </td>
                    </tr>
                    <xsl:apply-templates select="error:extra"/>
                </table>
            </strong>ody>
        </html>
    </xsl:template>
    <xsl:template match="error:description">
        <tr>
            <td class="highlight" style="vertical-align:top">
                description
            </td>
            <td style="background:#fff">
                <xsl:apply-templates mode="returns2br"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="error:message">
        <xsl:apply-templates mode="returns2br"/>
    </xsl:template>
    <xsl:template match="error:extra">
        <tr>
            <td colspan="2">
                <xsl:choose>
                    <xsl:when test="contains(@error:description,'stacktrace')">
                        <!-- wrap="OFF" if you want simple lines -->
                        <textarea rows="7" cols="80" wrap="OFF" style="width:100% ;font-size:8pt" readonly="readonly">
                            <xsl:apply-templates/>
                        </textarea>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates mode="returns2br"/>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="*|text()" name="text" mode="text">
        <xsl:param name="string" select="."/>
        <xsl:variable name="return" select="'&#xa;'"/>
        <xsl:choose>
            <xsl:when test="contains($string,$return)">
                <xsl:value-of select="substring-before($string,$return)"/>
                <xsl:call-template name="text">
                    <xsl:with-param name="string" select="substring-after($string,$return)"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$string"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="returns2br" match="*" mode="returns2br">
        <xsl:param name="string" select="."/>
        <xsl:variable name="return" select="'&#xa;'"/>
        <xsl:choose>
            <xsl:when test="contains($string,$return)">
                <xsl:value-of select="substring-before($string,$return)"/>
                <br/>
                <xsl:call-template name="returns2br">
                    <xsl:with-param name="string" select="substring-after($string,$return)"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$string"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
