<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
>

	<!--
		Méthodes liées à l'identification des titres dans un document EAD
	-->



	<xsl:import href="../../../../commons/xsl/functions.xsl" />

	<!-- Retourne le titre d'un composant -->
	<xsl:template name="get-title">
		<!-- La méthode de fabrication du titre -->
		<xsl:param name="method" select="''"/>
		<!-- La méthode par défaut si la méthode choisie retourne aucun titre -->
		<xsl:param name="default-method" select="'did'"/>
		<!-- Le séparateur entre les éléments -->
		<xsl:param name="sep" select="''"/>
		<!-- La taille maximale du titre -->
		<xsl:param name="limit" select="0"/>
		<!-- L'élément dont il faut trouver le titre, par défaut l'élément courant. -->
		<xsl:param name="element" select="."/>

		<!-- On trouve un titre en fonction de la méthode choisie -->
		<xsl:variable name="title">
			<xsl:call-template name="select-method">
				<xsl:with-param name="method" select="$method"/>
				<xsl:with-param name="sep" select="$sep"/>
				<xsl:with-param name="element" select="$element"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="tmptitle">
		<xsl:choose>
			<xsl:when test="normalize-space($title) = '' and $method != $default-method">
				<xsl:variable name="tmptitle">
					<!-- Méthode par défaut : tout le did -->
					<xsl:call-template name="select-method">
						<xsl:with-param name="method" select="$default-method"/>
						<xsl:with-param name="sep" select="$sep"/>
						<xsl:with-param name="element" select="$element"/>
					</xsl:call-template>
				</xsl:variable>
					<xsl:value-of select="normalize-space($tmptitle)"/>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="normalize-space($title)"/></xsl:otherwise>
		</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="pleade:truncate-string($tmptitle, $limit)"/>
	</xsl:template>

	<!-- Fabrication du titre, en choisissant la bonne méthode -->
	<xsl:template name="select-method">
		<xsl:param name="method" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:param name="element" select="."/>
		<xsl:choose>
			<!-- origination -->
			<xsl:when test="$method='origination'">
				<xsl:call-template name="get-title-origination">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- unittitle -->
			<xsl:when test="$method='unittitle'">
				<xsl:call-template name="get-title-unittitle">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- did -->
			<xsl:when test="$method='did'">
				<xsl:call-template name="get-title-did">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- unittitle et unitdate -->
			<xsl:when test="$method='unittitle-unitdate'">
				<xsl:call-template name="get-title-unittitle-unitdate">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- unittitle et unitid-->
			<xsl:when test="$method='unittitle-unitid'">
				<xsl:call-template name="get-title-unittitle-unitid">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method='unittitle-unitid_article'">
				<xsl:call-template name="get-title-unittitle-unitid_article">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- unitid et unittitle -->
			<xsl:when test="$method='unitid-unittitle'">
				<xsl:call-template name="get-title-unitid-unittitle">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method='unitid_article-unittitle'">
				<xsl:call-template name="get-title-unitid_article-unittitle">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- unittitle, unitdate et unitid -->
			<xsl:when test="$method='unittitle-unitdate-unitid'">
				<xsl:call-template name="get-title-unittitle-unitdate-unitid">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method='unittitle-unitdate-unitid_article'">
				<xsl:call-template name="get-title-unittitle-unitdate-unitid_article">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!--unitid (contributed by François Lemoine) -->
			<xsl:when test="$method='unitid'">
				<xsl:call-template name="get-title-unitid">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<!-- unitid, unittitle et unitdate -->
			<xsl:when test="$method='unitid-unittitle-unitdate'">
				<xsl:call-template name="get-title-unitid-unittitle-unitdate">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method='unitid_article-unittitle-unitdate'">
				<xsl:call-template name="get-title-unitid_article-unittitle-unitdate">
					<xsl:with-param name="element" select="$element"/>
					<xsl:with-param name="sep" select="$sep"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Les méthodes comme tells sont implémentées ci-dessous -->

	<!-- origination -->
	<xsl:template name="get-title-origination">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:for-each select="$element/did/origination[normalize-space(.)!='']">
			<xsl:value-of select="normalize-space(.)"/>
			<xsl:if test="position()!=last()"><xsl:value-of select="$sep"/></xsl:if>
		</xsl:for-each>
	</xsl:template>


	<!-- unittitle -->
	<xsl:template name="get-title-unittitle">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
			<xsl:apply-templates select="." mode="filter"/>
			<xsl:if test="position()!=last()"><xsl:value-of select="$sep"/></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- did -->
	<xsl:template name="get-title-did">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:for-each select="$element/did/*[normalize-space(.)!='']">
			<xsl:value-of select="normalize-space(.)"/>
			<xsl:if test="position()!=last()"><xsl:value-of select="$sep"/></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- unitittle et unitid -->
	<xsl:template name="get-title-unittitle-unitid">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
        <xsl:apply-templates select="." mode="filter">
          <xsl:with-param name="sep" select="$sep"/>
        </xsl:apply-templates>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
				<xsl:value-of select="pleade:format-unitid(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and $part-2 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
	</xsl:template>
	<xsl:template name="get-title-unittitle-unitid_article">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:if test="not($element/self::archdesc) and not(starts-with($element/@id, 'tt'))">
				<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
					<xsl:value-of select="pleade:format-unitid(.)"/>
					<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and $part-2 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
	</xsl:template>

	<!-- unitid et unittitle -->
	<xsl:template name="get-title-unitid-unittitle">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
				<xsl:value-of select="pleade:format-unitid(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and $part-2 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
	</xsl:template>
	<xsl:template name="get-title-unitid_article-unittitle">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:if test="not($element/self::archdesc) and not(starts-with($element/@id, 'tt'))">
				<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
					<xsl:value-of select="pleade:format-unitid(.)"/>
					<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:if test="not($element/self::archdesc) and not(starts-with($element/@id, 'tt'))">
				<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
					<xsl:apply-templates select="." mode="filter"/>
					<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and $part-2 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
	</xsl:template>

	<!-- unitittle et unitdate -->
	<xsl:template name="get-title-unittitle-unitdate">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unitdate[normalize-space(.)!='']">
				<xsl:value-of select="normalize-space(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and $part-2 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
	</xsl:template>

	<!-- unitid, unittitle et unitdate -->
	<xsl:template name="get-title-unitid-unittitle-unitdate">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
				<xsl:value-of select="pleade:format-unitid(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-3">
			<xsl:for-each select="$element/did/unitdate[normalize-space(.)!='']">
				<xsl:value-of select="normalize-space(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and ($part-2 != '' or $part-3 != '')">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
		<xsl:if test="($part-1 != '' or $part-2 != '') and $part-3 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-3"/>
	</xsl:template>
	<xsl:template name="get-title-unitid_article-unittitle-unitdate">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:if test="not($element/self::archdesc) and not(starts-with($element/@id, 'tt'))">
				<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
					<xsl:value-of select="pleade:format-unitid(.)"/>
					<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-3">
			<xsl:for-each select="$element/did/unitdate[normalize-space(.)!='']">
				<xsl:value-of select="normalize-space(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and ($part-2 != '' or $part-3 != '')">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
		<xsl:if test="($part-1 != '' or $part-2 != '') and $part-3 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-3"/>
	</xsl:template>

	<!-- unitittle, unitdate et unitid -->
	<xsl:template name="get-title-unittitle-unitdate-unitid">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unitdate[normalize-space(.)!='']">
				<xsl:value-of select="normalize-space(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-3">
			<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
				<xsl:value-of select="pleade:format-unitid(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and ($part-2 != '' or $part-3 != '')">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
		<xsl:if test="$part-2 != '' and $part-3 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-3"/>
	</xsl:template>
	<xsl:template name="get-title-unittitle-unitdate-unitid_article">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:variable name="part-1">
			<xsl:for-each select="$element/did/unittitle[normalize-space(.)!='']">
				<xsl:apply-templates select="." mode="filter"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-2">
			<xsl:for-each select="$element/did/unitdate[normalize-space(.)!='']">
				<xsl:value-of select="normalize-space(.)"/>
				<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="part-3">
			<xsl:if test="not($element/self::archdesc) and not(starts-with($element/@id, 'tt'))">
				<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
					<xsl:value-of select="pleade:format-unitid(.)"/>
					<xsl:if test="position() != last()"><xsl:value-of select="$sep"/></xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="$part-1"/>
		<xsl:if test="$part-1 != '' and ($part-2 != '' or $part-3 != '')">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-2"/>
		<xsl:if test="($part-1 != '' or $part-2 != '') and $part-3 != ''">
			<xsl:value-of select="$sep"/>
		</xsl:if>
		<xsl:value-of select="$part-3"/>
	</xsl:template>

	<!-- unitid -->
	<xsl:template name="get-title-unitid">
		<xsl:param name="element" select="''"/>
		<xsl:param name="sep" select="''"/>
		<xsl:for-each select="$element/did/unitid[normalize-space(.)!='']">
			<xsl:value-of select="pleade:format-unitid(.)"/>
			<xsl:if test="position()!=last()"><xsl:value-of select="$sep"/></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- Ajuste un élément pour supprimer les @pleade:hidden -->
	<xsl:template match="*[@audience = 'internal']" mode="filter">
	</xsl:template>

  <!-- Un unittile qui contient un imprint ; on s'assure d'avoir un séparateur entre le contenu du titre et le contenu du imprint -->
  <xsl:template match="unittitle[imprint]" mode="filter">
    <xsl:param name="sep" select="''"/>
    <xsl:choose>
      <xsl:when test="@pleade:hidden = 'true'"/>
      <xsl:otherwise><xsl:value-of select="concat(./text(), $sep, ./imprint/text())"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>
	<xsl:template match="*" mode="filter">
		<xsl:choose>
			<xsl:when test="@pleade:hidden = 'true'"/>
			<xsl:otherwise><xsl:apply-templates mode="filter"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="text()" mode="filter">
		<xsl:value-of select="."/>
	</xsl:template>

</xsl:stylesheet>
