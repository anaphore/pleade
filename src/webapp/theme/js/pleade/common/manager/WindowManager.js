//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion de fenêtres
 * @author Julien Huet
 */
/**
 * @class Gestion de fenêtres
 * @author Julien Huet
 * @augments AjaxRequestHandler
 */
var WindowManager = new Class(/** @lends WindowManager.prototype */{
	Implements: [AjaxRequestHandler],
	
	/**
	* Liste des popups deja affichées.
	* @private
	* @type Array
	*/
	popups: null,
	
	/**
	* Création de nouvelles fenêtres
	*/
	initialize: function(){
		//Permet d'avoir une collection de popup 
		this.popups = new Array();
	},
	
	/**
	* Ouvre une nouvelle fenêtre, nommée ou non
	* @param {String} href Lien qui sera ouvert dans la nouvelle popup.
	* @param {String} name Nom de la popup (utiliser comme indice dans la collection de popup
	* @param {String} width Largeur souhaitée pour la popup
	* @param {String} height Hauteur voulu pour la popup
	* @returns {boolean} retourne false si la création de la popup a fonctionné  ??
	* @type boolean
	*/
	newWin: function (href, name, width, height) {
		/** FIXME [JC] : n'est visiblement pas utilisée... */
		if( !href ) return true;
		var options = '';
		var opts = '';
		if ( width!='' && width != undefined ) {
			opts = 'width=' + width;
		}
		if ( height!='' && height != undefined ) {
			if( opts != '' ) {
				opts += ',';
			}
			opts += 'height=' + height;
		}
		if( opts != '' ) {
			opts += ';';
		}
		opts += options;
		var newWin = window.open(href, name, opts);
		newWin.focus();
		return false;
	},
	
	/**
	* Ouvre une nouvelle fenêtre, nommée ou non, avec le focus. De plus la fonction permet d'ajouter la popup à la collection de popup
	* @param {String} href Lien qui sera ouvert dans la nouvelle popup.
	* @param {String} name Nom de la popup (utiliser comme indice dans la collection de popup
	* @param {String} width Largeur souhaitée pour la popup
	* @param {String} height Hauteur voulu pour la popup
	* @param {boolean} full Si a true permet de faire apparaitre la popup en plein ecran
	* @returns {boolean} retourne false si la création de la popup a fonctionné pour annuler l'évènement original
	* @type boolean
	 */
	winFocus: function(href, name, width, height, full) {
		if (!href) {
			return true;
		}
		if ( this.popups && this.popups[name] && !this.popups[name].closed && this.popups[name].location ) {
			if(this.popups[name].location.href == href);
			else {
				this.popups[name].location.href = href;
			}
		} else {
			var options = 'directory=yes,location=yes,menubar=no,resizable=yes,scrollbars=yes,status=yes,toolbar=yes';
			var opts = '';
	    		if( full === true ) {
				options = 'directory=yes,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,fullscreen=yes';
			} else {
				if ( width!='' && width != undefined ) {
					opts = 'width=' + width;
				}
				if( height!='' && height != undefined ) {
					if ( opts != '' ) {
						opts += ',';
					}
					opts += 'height=' + height;
				}
				if ( opts != '' ) {
					opts += ';';
				}
			}
			opts += options;
			this.popups[name]=window.open(href, name, opts);
		}
		if (this.popups && this.popups[name] && this.popups[name].focus) {
			try {
				this.popups[name].focus();
			} catch (e) {}
		}
		return false;
	},

	/**
	* Ouvre la visionneuse d'images.
	* @param {String} url URL a passer a la visionneuse
	* @returns {boolean} retourne false si la création de la popup a fonctionné  ??
	* @type boolean
	*/
	openImgViewer: function(url) {
		return this.winFocus(url, 'imgviewer', null, null, 'true');
	},

	/**
	* Ouvre une fenêtre pour un document attaché.
	* @param {String} url URL a passer a la visionneuse
	* @returns {boolean} retourne false si la création de la popup a fonctionné  ??
	* @type boolean
	*/
	openAttachedDocument: function(url) {
		return this.winFocus(url, 'attdoc');
	},

	/**
	* Fonction permettant d'afficher les documents appartenant au resultat de recherche
	* @param div TO DOCUMENT
	* @param {String} url TO DOCUMENT
	*/
	updateDocumentResults: function(div, url) {
		var loading = new Loading(div);
		loading.start();
		var req = new Request.HTML({
			method: 'get',
			url: url,
			update: div,
			evalScripts: true,
			evalResponse: true,
			onComplete: function(){
				loading.stop();
			}
		});
		req.send();
	},

	/**
	* Initialisation du "menu box"<br/>
	* On va initialiser les boutons :
	* <ul>
	* <li>de l'aide,</li>
	* <li>de la fermeture de la fenêtre,</li>
	* <li>du retour à la fenêtre précédente ou du retour à la page d'accueil</li>
	* </ul>
	*/
	initMenuBox: function(){
		//bouton aide
		this.btnHelp = $('btn-help');
		this.btnHelp.addEvent('click', this.showHelp.bind(this));

		//bouton fermer la fenêtre
		this.btnClose = $('btn-close-window');

		//bouton retour à la fenêtre précédente ou retour à l'accueil
		if ( window.opener ) {
			$('btn-home').getParent().destroy();
			this.btnOpener = $('btn-goto-opener');
		} else {
			$('btn-goto-opener').getParent().destroy();
			this.btnHome = $('btn-home');
			this.btnHome.addEvent('click', this.gotoHome.bind(this));
		}
	},

	showHelp: function(event){
		var _url = this.btnHelp.get('href');
		this.winFocus(_url, 'pleadehelp', 600, 400, false);
		event.stop();
	},

	/** Retour à la page d'accueil */
	gotoHome: function(event) {
		var _url = this.btnHome.get('href');
		self.location.href = _url;
	}
});
