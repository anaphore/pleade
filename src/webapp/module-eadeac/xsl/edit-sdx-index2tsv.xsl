<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Export XML vers TSV
    | On travaille sur un sdx:terms issu de functions/sdx-terms.xml?hpp=-1
    | On veut :
    | 1) le terme
    | 2) la fréquence
    | 3) l'identification du doc s'il n'y en a qu'un
    |
    | FIXME (MP) : i18n pour les en-têtes ?
    +-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		exclude-result-prefixes="sdx xsl">

  <xsl:variable name="lf"  select="'&#10;'" /> <!-- Retour ligne -->
  <xsl:variable name="tab" select="'&#9;'" />  <!-- Tabulation -->

  <!--+
      | Traitement de la racine qui compte pour du beurre
      +-->
	<xsl:template match="sdx:terms">

		<xsl:if test="$lf!='' and $tab!=''">

			<root>
        <xsl:value-of select="concat('Term',  $tab)" />
        <xsl:value-of select="concat('Freq',  $tab)" />
        <xsl:value-of select="concat('Id', $tab)" />
        <xsl:apply-templates select="sdx:term" />
			</root>

		</xsl:if>

	</xsl:template>

  <!--+
      | Formatage d'un terme
      +-->
  <xsl:template match="sdx:term">

    <xsl:value-of select="$lf" />

    <xsl:value-of select="concat( normalize-space(@value), $tab )" />

    <xsl:value-of select="concat( normalize-space(@docFreq),  $tab )" />

    <xsl:if test="number(@docFreq) = 1">
      <xsl:value-of select="normalize-space(@docId)" />
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>
