// $Id: MissingParameterException.java 19706 2010-11-25 16:52:20Z jcwiklinski $

package org.sru.cql;
import java.lang.Exception;


/**
 * Exception indicating that a required property was not specified.
 *
 * @version	$Id: MissingParameterException.java 19706 2010-11-25 16:52:20Z jcwiklinski $
 */
public class MissingParameterException extends Exception {
    /**
     * Creates a new <TT>MissingParameterException</TT>.
     * @param s
     *	The name of the property whose value was required but not supplied.
     */
    public MissingParameterException(String s) {
	super(s);
    }
}
