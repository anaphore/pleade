<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: ead2html-toolskit.xsl 14362 2009-04-10 13:00:34Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		exclude-result-prefixes="xsl i18n pleade sdx fn dir xs">

	<!--		CETTE TRANSFORMATION GERE LES OUTILS de la fenêtre EAD
					 - barre d'outils : boutons pour ajouter au panier, pour exporter au format PDF
	-->
	
	<!-- Identifiant du document -->
	<xsl:variable name="docid" select="/sdx:document/pleade:subdoc/pleade:ancestors/@pleade:eadid"/>

  <!--+
      | Construction de la boite à outils
      +-->
  <xsl:template name="tool-box-builder">

    <xsl:if test="$active-basket='true' or $active-pdf='true'">

      <div id="pl-pg-body-tool-box">
        <ul>
          <xsl:call-template name="basket-button-builder">
            <xsl:with-param name="id2add" select="/sdx:document/pleade:subdoc/@pleade:id"/>
          </xsl:call-template>
          <xsl:call-template name="pdf-button-builder">
            <xsl:with-param name="id2add" select="/sdx:document/pleade:subdoc/@pleade:id"/>
          </xsl:call-template>
        </ul>
      </div>

    </xsl:if>

  </xsl:template>

  <!--+
        | Construction du bouton d'export PDF du document
        +-->
	<xsl:template name="pdf-button-builder">
		<xsl:param name="id2add" select="''" />
		<xsl:if test="$active-pdf='true'">
			<!-- FIXME: <a> au lieu de onclick? -->
			<!-- bouton PDF pour l'unité -->
			<li>
				<button class="pl-pdf-bttn-doc" title="pdf.button.doc.title" i18n:attr="title" id="pdf-doc-{$id2add}" onclick="window.open('ead.pdf?id={$docid}&amp;c={$id2add}'); return false;">
					<span class="access"><i18n:text key="pdf.button.doc.text">pdf de cette unité</i18n:text></span>
				</button>
			</li>
          <!-- bouton PDF pour l'unité et ses sous-unités -->
			<li>
				<button class="pl-pdf-bttn-doch" title="pdf.button.doch.title" i18n:attr="title" id="pdf-doch-{$id2add}" onclick="window.open('ead.pdf?id={$docid}&amp;c={$id2add}&amp;children=true'); return false;">
					<span class="access"><i18n:text key="pdf.button.doch.text">pdf de cette unité  et des sous-unité</i18n:text></span>
				</button>
			</li>
      </xsl:if>
    </xsl:template>

    <!--+
        | Construction du bouton d'ajout du document au porte-documents
        +-->
    <xsl:template name="basket-button-builder">
      <xsl:param name="id2add" select="''" />
      <xsl:if test="$active-basket='true'">
        <li class="pl-bskt-bttn-cnt">
          <!-- bouton d'ajout au porte-docs -->
          <button class="pl-bskt-bttn-doc"
                  title="module-eadeac:basket.button.add-to-basket.doc.title"
                  i18n:attr="title" id="bskt-add-{$id2add}"
                  onclick="basket.basketManager.addDocToBasket('id={$id2add}', this);"
                  onkeypress="basket.basketManager.addDocToBasket('id={$id2add}', this);">
            <xsl:if test="not( $id2add!='' )">
              <xsl:attribute name="disabled"><xsl:text>disabled</xsl:text></xsl:attribute>
              <xsl:attribute name="class">
                <xsl:text>pl-bskt-bttn-doc disabled</xsl:text>
              </xsl:attribute>
            </xsl:if>
            <span class="access"><i18n:text i18n:catalogue="module-eadeac" key="basket.button.add-to-basket.doc.text">ajouter au porte-documents</i18n:text></span>
          </button>
        </li>
      </xsl:if>
    </xsl:template>

</xsl:stylesheet>
