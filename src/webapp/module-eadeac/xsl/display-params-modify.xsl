<?xml version="1.0"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:source="http://apache.org/cocoon/source/1.0"
		xmlns:h="http://apache.org/cocoon/request/2.0"
		exclude-result-prefixes="xsl source h">

	<xsl:param name="target" select="''" />

	<xsl:variable name="param" select="/root/h:request/h:requestParameters/h:parameter" />
	<xsl:variable name="props" select="/root/properties/property" />

	<!--+
	    |
	    +-->
	<xsl:template match="/root | /response">
		<xsl:choose>
			<xsl:when test="self::response">
				<xsl:apply-templates select="." mode="copy" />
			</xsl:when>
			<xsl:when test="$target=''">
				<error code="no-target">Pas de fichier cible.</error>
			</xsl:when>
			<xsl:when test="not(properties/@id)">
				<error code="no-properties-src">Pas de propriété source.</error>
			</xsl:when>
			<xsl:when test="not(h:request/h:requestParameters/h:parameter[h:value!=''])">
				<error code="nothing-to-do">Pas de paramètre à modifier.</error>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="properties" mode="modify" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--<xsl:template match="properties">

		<xsl:variable name="do">
			<xsl:for-each select="$param">
				<xsl:message><xsl:value-of select="@name" /> = <xsl:value-of select="h:value" /></xsl:message>
				<xsl:if test="$props[@name=current()/@name]">
					<xsl:choose>
						<xsl:when test="not(@default) and normalize-space(.)!=normalize-space(current()/h:value)">
							<xsl:text>1</xsl:text>
						</xsl:when>
						<xsl:when test="normalize-space(@default)!=normalize-space(current()/h:value)">
							<xsl:text>1</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
<xsl:message>do=<xsl:value-of select="$do" /></xsl:message>
		<xsl:choose>
			<xsl:when test="contains($do, '1')">
				<xsl:apply-templates select="." mode="modify" />
			</xsl:when>
			<xsl:otherwise>
				<error code="no-change">Aucune valeur à modifier.</error>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>-->

	<xsl:template match="properties" mode="modify">
		<source:insert>
			<source:source>
				<xsl:value-of select="$target" />
			</source:source>
			<source:path>/</source:path>
			<source:replace>properties</source:replace>
			<source:fragment>
				<properties>
					<xsl:apply-templates select="@* | node()" mode="copy" />
				</properties>
			</source:fragment>
		</source:insert>
	</xsl:template>

	<xsl:template match="property[@default]" mode="copy">
		<property>
			<xsl:apply-templates select="@*[not(name()='default')]" mode="copy" />
			<xsl:attribute name="default">
				<xsl:choose>
					<xsl:when test="$param[@name=current()/@name]">
						<xsl:value-of select="normalize-space( $param[@name=current()/@name]/h:value )" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="normalize-space(@defautl)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</property>
	</xsl:template>

	<xsl:template match="property" mode="copy">
		<property>
			<xsl:apply-templates select="@*" mode="copy" />
			<xsl:choose>
				<xsl:when test="$param[@name=current()/@name]">
					<xsl:value-of select="normalize-space( $param[@name=current()/@name]/h:value )" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="normalize-space(.)" />
				</xsl:otherwise>
			</xsl:choose>
		</property>
	</xsl:template>

	<xsl:template match="@* | node()" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" mode="copy" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
