<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2000-2011  Ministere de la culture et de la communication (France), AJLSM
See LICENCE file.
-->
<!--+
    | Actualisation de la date de réponse (responsDate).
    | On le fait ici pour pouvoir cacher la réponse XML construite par SDX.
    | Seule la date doit changer.
    +-->
<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:oai2="http://www.openarchives.org/OAI/2.0/"
xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
xmlns:sdxoai="http://www.culture.gouv.fr/ns/oai/"
xmlns:xsp="http://apache.org/xsp"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:fn="http://www.w3.org/2005/xpath-functions"
exclude-result-prefixes="sdx sdxoai xsp xs fn">

  <xsl:variable name="nss">http://www.openarchives.org/OAI/2.0/oai_dc/,http://purl.org/dc/elements/1.1/,http://pleade.org/oai/qdc,http://purl.org/dc/terms/,http://patrimoines.aquitaine.fr/ap/format-pivot/1.1,http://www.openarchives.org/OAI/2.0/,http://www.w3.org/2001/XMLSchema-instance</xsl:variable>

  <xsl:template match="oai2:responseDate">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <!-- Mise à jour de la date de la réponse -->
      <xsl:value-of select="format-dateTime(adjust-dateTime-to-timezone(current-dateTime(), xs:dayTimeDuration('PT0H')), '[Y1,4]-[M1,2]-[D1,2]T[H1,2]:[m1,2]:[s1,2]Z', (), 'ISO', ())"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*[local-name()='dummyRoot']" priority="+5">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="*">
    <xsl:choose>
      <xsl:when test="namespace-uri-from-QName(node-name(.)) = tokenize($nss, ',')">
        <!--<xsl:message>On a un namespace connu pour <xsl:value-of select="name()"/></xsl:message>-->
        <xsl:copy>
          <xsl:apply-templates select="@* | node()" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
          <xsl:message>Namespace inconnu pour <xsl:value-of select="concat('{', namespace-uri-from-QName(node-name(.)), '}', name())"/></xsl:message>
        <xsl:element name="{local-name()}">
          <xsl:apply-templates select="@* | node()" />
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@xml:*">
    <xsl:copy-of select="." copy-namespaces="no" />
  </xsl:template>

  <xsl:template match="@*">
    <xsl:choose>
      <xsl:when test="namespace-uri-from-QName(node-name(.)) = tokenize($nss, ',')">
        <!--<xsl:message>On a un namespace connu pour <xsl:value-of select="name()"/></xsl:message>-->
        <xsl:attribute name="{name()}">
          <xsl:value-of select="." />
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>Namespace inconnu pour <xsl:value-of select="concat('{', namespace-uri-from-QName(node-name(.)), '}', name())"/></xsl:message>
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="." />
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:copy/>
  </xsl:template>

</xsl:stylesheet>
