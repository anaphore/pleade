/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.generation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Stack;

import org.apache.excalibur.source.Source;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.ResourceNotFoundException;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.generation.DirectoryGenerator;
import org.xml.sax.SAXException;

/**
 * Ce generateur surcharge le DirectoryGenerator de Cocoon pour :
 * <ul>
 * <li>Prendre le dossier parent si la source est un fichier</li>
 * <li>Ajouter un attribut <code>absolutePath</code> a toutes les entrees</li>
 * </ul>
 * @see DirectoryGenerator
 */
public class CompletePathDirectoryGenerator extends DirectoryGenerator {

    /** Le nom de l'attribut. */
    protected static String COMPLETE_PATH_ATTR_NAME = "absolutePath";
    
    /** Constant for the file protocol. */
    // on recopie celle de DirectoryGenerator car elle n'est pas visible :-(
    private static final String FILE = "file:";

    public void generate() throws SAXException, ProcessingException {
    	
    	Logger logger = getLogger();
    	
    	try {
    	
    		// Controle s'il s'agit bien d'une source de type file:/
	    	String systemId = this.directorySource.getURI();
	      if (!systemId.startsWith(this.FILE)) {
	          throw new ResourceNotFoundException(systemId + " does not denote a valid file or directory.");
	      }
	      
	      // On controle s'il s'agit bien d'un dossier, sinon on prend le dossier parent
	      File directoryFile = new File(new URL(systemId).getFile());
	      if (!directoryFile.isDirectory()) {
	          File tmp = directoryFile.getParentFile();
	          this.directorySource = this.resolver.resolveURI(tmp.getAbsolutePath());
	          if(logger!=null && logger.isDebugEnabled()){ logger.debug("[CompletePathDirectoryGenerator] Pour path:"+systemId+", on renvoie "+this.directorySource.getURI()); }
	          tmp = null;
	      }

    	} catch (IOException ioe) {
         throw new ResourceNotFoundException("Could not read directory " + super.source, ioe);
    	}
    	// on renvoie le traitement a la super classes (ie, DirectoryGenerator)
    	super.generate();
  }
    
    /**
     * Ajoute un attribut absolutePath.
     * @param path L'entree en cours de traitement.
     */
    protected void setNodeAttributes(File path) throws SAXException {
        super.setNodeAttributes(path);
        attributes.addAttribute("", COMPLETE_PATH_ATTR_NAME, COMPLETE_PATH_ATTR_NAME, "CDATA", path.getAbsolutePath());
    }

}
