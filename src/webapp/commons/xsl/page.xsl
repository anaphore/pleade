<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:c-pleade="http://pleade.org/ns/c-pleade/1.0"
	xmlns:nav="http://ajlsm.com/sdx/navigation/cbe/1.0"
	xmlns:local-conf="http://pleade.org/ns/local-conf/1.0"
>

	<!-- We drive the page output from the top PLEADE element -->
	<xsl:template match="c-pleade:page">
		<html>
			<xsl:call-template name="output-html-head"/>
			<body>
				<xsl:call-template name="output-page-header"/>
				<xsl:call-template name="output-page-content"/>
				<xsl:call-template name="output-page-footer"/>
				<xsl:call-template name="output-band"/>
			</body>
		</html>
	</xsl:template>



	<!-- Output page footer -->
	<xsl:template name="output-page-footer">
		<div class="page-footer">
			<div class="administration">
				<xsl:apply-templates select="c-pleade:administration|c-pleade:user-information|c-pleade:debug-info"/>
			</div>
		</div>
	</xsl:template>

	<!-- Output page content -->
	<xsl:template name="output-page-content">
		<div class="page-content">
			<xsl:apply-templates select="*[not(self::c-pleade:administration or self::c-pleade:user-information or self::c-pleade:debug-info)]"/>
		</div>
	</xsl:template>

	<!-- Output page header -->
	<xsl:template name="output-page-header">
		<div class="page-header">
			<table cellpadding="0" cellspacing="0" style="border:none;width:100%">
				<tr style="vertical-align:top" id="background">
					<td><div id="logo">&#160;</div></td>
					<!--<xsl:call-template name="output-header-left"/>-->
					<!--<td width="200" align="right" valign="middle" background="{$g-url-index}/@pleade.webapp.system.subfolder@/web/pictures/degrade.gif">
						<img src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/pictures/degrade.gif"/>
					</td>-->
					<td style="text-align:right;vertical-align::middle" class="common-search-form">
						<!-- the search bar -->
						<xsl:call-template name="common-search-form"/>
					</td>
				</tr>
			</table>
			<xsl:call-template name="output-header-right"/>
		</div>
	</xsl:template>

	<!-- Output HTML head element -->
	<xsl:template name="output-html-head">
		<head>
			<xsl:call-template name="html-title-output"/>
			<xsl:call-template name="css-link-output"/>
			<xsl:call-template name="script-output"/>
		</head>
	</xsl:template>

	<!-- Output HTML title element -->
	<xsl:template name="html-title-output">
		<title>
			<xsl:call-template name="title-start-output"/>
			<xsl:call-template name="title-output"/>
		</title>
	</xsl:template>

	<!-- Left part of page headers -->
	<xsl:template name="output-header-left">
		<xsl:call-template name="output-site-logo"/>
	</xsl:template>

	<!-- Site logo -->
	<!-- FIXME: correct ALT -->
	<xsl:template name="output-site-logo">
		<xsl:call-template name="find-logo"/>
	</xsl:template>

	<!-- Right part of page headers -->
	<xsl:template name="output-header-right">
		<!-- First the menubar -->
		<xsl:apply-templates select="menu-bar[@id='general']" mode="output"/>
	</xsl:template>

	<!-- Page header -->
	<xsl:template match="c-pleade:page-header">
		<h1>
			<xsl:call-template name="page-header"/>
		</h1>
	</xsl:template>
	<xsl:template name="page-header"/>


	<!--user information-->
	<xsl:template match="c-pleade:user-information">
		<div class="user">
			<xsl:value-of select="$g-ui-strings/user/label"/>
			<xsl:apply-templates select="/sdx:document/sdx:user" mode="output"/>
			<xsl:text> ; </xsl:text>
			<xsl:value-of select="$g-ui-strings/group/label"/>
			<xsl:apply-templates select="/sdx:document/sdx:user/sdx:group" mode="output"/>
			<xsl:text> ; </xsl:text>
			<xsl:value-of select="$g-ui-strings/version/label"/>
			<xsl:value-of select="/sdx:document/@version"/>
			<xsl:text> // </xsl:text>
			<xsl:value-of select="/sdx:document/@build"/>
		</div>
	</xsl:template>

	<xsl:template match="sdx:group" mode="output">
		<xsl:if test="position() > 1"><xsl:text>, </xsl:text></xsl:if>
		<xsl:value-of select="@id"/>
	</xsl:template>

	<xsl:template match="sdx:user" mode="output">
		<xsl:value-of select="@id"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="@firstname"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="@lastname"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="@host"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<!--administration-->
	<xsl:template match="c-pleade:administration">
		<div class="admin">
			<table style="width:100%;border:none" cellpadding="0" cellspacing="0">
				<tr style="vertical-align:top">
					<td>
						<img src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/pictures/px.png" width="36" height="1"/>
					</td>
					<td>
						<table style="border:none" cellspacing="0" cellpadding="1">
							<xsl:apply-templates select="c-pleade:admin-tool[@place='left']"/>
						</table>
					</td>
					<td>
						<img src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/pictures/px.png" width="20" height="1"/>
					</td>
					<td style="text-align:center">
						<table border="0" cellspacing="0" cellpadding="1">
							<xsl:apply-templates select="c-pleade:admin-tool[@place='center']"/>
						</table>
					</td>
					<td>
						<img src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/pictures/px.png" width="20" height="1"/>
					</td>
					<td style="text-align:right">
						<table style="border:none" cellspacing="0" cellpadding="1">
							<xsl:apply-templates select="c-pleade:admin-tool[@place='right']"/>
						</table>
					</td>
					<td>
						<img src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/pictures/px.png" width="36" height="1"/>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>

	<!--administration tools-->
	<xsl:template match="c-pleade:admin-tool">
		<xsl:variable name="code" select="@code"/>
		<tr style="text-align:left;vertical-align:middle">
			<td>
			 <input type="submit" title= "{$g-ui-strings/admin/title[@code=$code]}" >
				<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test="$code='logout' or $code='delete'  or $code='delete-af'">
							<xsl:value-of select="' X '"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="' > '"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="onclick">
					<xsl:variable name="ap">'</xsl:variable>
					<xsl:choose>
						<xsl:when test="$code='delete' or $code='delete-af'">
							<xsl:value-of select="concat('if(confirm(',$ap,$g-ui-strings/admin/confirm[@code=$code],$ap,'))','window.location.href=',$ap,$g-url-index,'/',@link,$ap)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat('window.location.href=',$ap,$g-url-index,'/',@link,$ap)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="$code='delete' or $code='delete-af'">
						<xsl:attribute name="class">
							<xsl:value-of select="'supprimer'"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="$code='add' or $code='add-af'">
						<xsl:attribute name="class">
							<xsl:value-of select="'indexer'"/>
						</xsl:attribute>
					</xsl:when>
				</xsl:choose>
			 </input>
			</td>
			<td>
				<xsl:value-of select="$g-ui-strings/admin/label[@code=$code]"/>
				<xsl:text>&#xA0;</xsl:text>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="c-pleade:debug-info">
		<div class="debug-info">
			<xsl:text>Debug: </xsl:text>
			<a href="{/sdx:document/@uri}2xsp{/sdx:document/@query}">XSP</a>
			<xsl:text> --> </xsl:text>
			<a href="{/sdx:document/@uri}2java{/sdx:document/@query}">Java</a>
			<xsl:text> --> </xsl:text>
			<a href="{/sdx:document/@uri}2sdx{/sdx:document/@query}">SDX</a>
		</div>
	</xsl:template>

	<!--javascript for the menu-->
	<xsl:template name="script-output">
		<!--<script src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/js/cbe_core.js" type="text/javascript"/>
		<script src="{$g-url-index}/@pleade.webapp.system.subfolder@/web/js/cbe_event.js" type="text/javascript"/>-->
		<xsl:copy-of select="menu-bar[@id='general']/config/scripts/*"/>
	</xsl:template>

	<xsl:template match="c-pleade:*"/>

	<!-- css links output-->
	<xsl:template name="css-link-output">
		<!-- We will output standard PLEADE css here -->
		<link rel="stylesheet" type="text/css" href="theme/css/pleade.css"/>
		<!-- personnal css output -->
		<xsl:call-template name="self-css-link-output"/>
	</xsl:template>

	<!-- personnal css links -->
	<xsl:template name="self-css-link-output"/>

	<!-- The first part of the page title -->
	<xsl:template name="title-start-output">
		<xsl:value-of select="$g-ui-strings/site/page-title-start"/>
	</xsl:template>

	<!--title output-->
	<xsl:template name="title-output"/>

	<!-- The list of user interface languages, from the configuration file -->
	<xsl:template match="local-conf:ui-languages">
		<xsl:variable name="l-url">
			<xsl:value-of select="concat($g-sdx-doc/@uri,'?')"/>
			<xsl:for-each select="$g-sdx-doc/sdx:parameters/sdx:parameter[@type='get' and @name !='l']">
				<xsl:choose>
					<xsl:when test="preceding-sibling::sdx:parameter[@type='get' and @name !='l']">
						<xsl:value-of select="'&amp;'"/>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="@name"/>
				<xsl:text>=</xsl:text>
				<xsl:value-of select="@value"/>
			</xsl:for-each>
			<xsl:choose>
				<xsl:when test="$g-sdx-doc/sdx:parameters/sdx:parameter[@type='get' and @name !='l']">
					<xsl:value-of select="'&amp;l='"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'l='"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<select onChange="window.location.href='{$l-url}' + this.options[this.selectedIndex].value;">
			<xsl:apply-templates/>
		</select>
	</xsl:template>

	<!-- One user interface language -->
	<xsl:template match="local-conf:ui-language">
		<option value="{@code}">
			<xsl:if test="@code = $g-lang"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
			<xsl:value-of select="local-conf:name[lang($g-lang)]"/>
		</option>
	</xsl:template>

	<!-- Error handling -->
	<xsl:template match="c-pleade:error">
		<xsl:variable name="code" select="@code"/>
		<h2 class="error">
			<xsl:value-of select="$g-ui-strings/errors/header[@code=$code]"/>
		</h2>
		<xsl:copy-of select="$g-ui-strings/errors/explanation[@code=$code]/node()"/>
	</xsl:template>

	<!--the logo-->
	<xsl:template name="find-logo"/>
	<xsl:template name="output-band"/>

</xsl:stylesheet>
