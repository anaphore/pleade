<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!-- XSLT qui pilote la conversion d'un document ou fragment EAD en format Dublin Core qualifié. -->
<xsl:stylesheet version="1.1"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:dcterms="http://purl.org/dc/terms/"
	xmlns:qdc="http://pleade.org/oai/qdc"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:map="http://apache.org/cocoon/sitemap/1.0"
	exclude-result-prefixes="pleade map">

  <!-- Code qui intercepte les métadonnées -->
  <xsl:import href="extractmetadata.xsl"/>

	<xsl:variable name="imu" select="'cocoon://functions/commons/get-input-module.xml?name='"/>
	<xsl:param name="public-url" select="if(doc-available(concat($imu,'pleade.public-url'))) then document(concat($imu,'pleade.public-url'))/get-input-module/input:attribute-values[@name='pleade.public-url']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/> 

  <!--choosen title-->
	<xsl:param name="pleade-title" select="if(doc-available(concat($imu,'ead-oai-pleade-title'))) then document(concat($imu,'ead-oai-pleade-title'))/get-input-module/input:attribute-values[@name='ead-oai-pleade-title']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
  <!--title separator -->
	<xsl:param name="pleade-title-sep" select="if(doc-available(concat($imu,'ead-oai-pleade-title-sep'))) then document(concat($imu,'ead-oai-pleade-title-sep'))/get-input-module/input:attribute-values[@name='ead-oai-pleade-title-sep']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
	<!--language of the title -->
	<xsl:param name="language" select="if(doc-available(concat($imu,'ead-oai-pleade-title-lang'))) then document(concat($imu,'ead-oai-pleade-title-lang'))/get-input-module/input:attribute-values[@name='ead-oai-pleade-title-lang']/input:value[.!=''][1] else 'fr'" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
	<!-- output sub-resources -->                                
	<xsl:param name="includehasPart" select="if(doc-available(concat($imu,'ead-oai-includehasPart'))) then document(concat($imu,'ead-oai-includehasPart'))/get-input-module/input:attribute-values[@name='ead-oai-includehasPart']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
	<!-- descriptors inheritance -->
	<xsl:param name="inheritDesc" select="if(doc-available(concat($imu,'ead-oai-inheritDesc'))) then document(concat($imu,'ead-oai-inheritDesc'))/get-input-module/input:attribute-values[@name='ead-oai-inheritDesc']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
	<!-- how write relation element content -->
	<xsl:param name="relationMode" select="if(doc-available(concat($imu,'ead-oai-relationMode'))) then document(concat($imu,'ead-oai-relationMode'))/get-input-module/input:attribute-values[@name='ead-oai-relationMode']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
	<!-- base URL to build URL in case relationMode='url' -->
	<xsl:param name="baseURL" select="concat( $public-url, if(ends-with('/',$public-url)) then '' else '/')"/>
  <!-- URL interne du serveur d'image -->
  <xsl:param name="internal-img-server-base" select="'cocoon://img-server/'"/>
  <!-- URL relative du serveur d'images -->
  <xsl:param name="public-img-server-base" select="'img-server/'"/>
  <!-- URL relative de la visionneuse -->
  <xsl:param name="img-viewer-base" select="'img-viewer/'"/>

  <!-- Un flag pour le débogage -->
  <xsl:param name="isDebug" select="'false'"/>

	<!-- L'identifiant de l'IR -->
	<xsl:variable name="eadid" select="string(/*/@pleade:eadid)"/>

	<!-- Les propriétés OAI liées à ce document -->
	<xsl:variable name="oai-properties" select="document(concat('cocoon:/functions/ead/get-properties/', $eadid, '/oai.xml'))"/>

  <!-- les données de publication; permet notament de récupérer le title-limit -->
  <xsl:variable name="pub-properties">
    <xsl:variable name="url" select="concat('cocoon://functions/ead/get-properties/', $eadid, '/publication.xml')"/>
    <xsl:choose>
      <xsl:when test="doc-available($url)">
        <xsl:copy-of select="doc($url)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>Problem url : <xsl:value-of select="$url"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

	<!-- Début du traitement TODO -->
	<xsl:template match="/*">
		<qdc:metadata-record>
			<!-- Deux cas de figure : on a un eadheader ou encore un component -->
			<xsl:choose>
				<xsl:when test="c/@pleade:original-element = 'eadheader' or eadheader">
					<!-- Quelques constantes -->
					<xsl:apply-templates select="c | eadheader" mode="meta-oai"/>
					<dc:identifier>
						<xsl:value-of select="
								concat( $public-url, if(ends-with('/',$public-url)) then '' else '/', 'ead.html?id=', @pleade:eadid )
						" />
					</dc:identifier>
					<dc:type>dataset</dc:type>
					<dc:type>text</dc:type>
					<dc:format>text/xml</dc:format>
					<dcterms:conformsTo>EAD DTD</dcterms:conformsTo>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="oai"/>
				</xsl:otherwise>
			</xsl:choose>
		</qdc:metadata-record>
	</xsl:template>

<!-- *************** BEGIN general template **************** -->
	<!-- We intercept the top element -->
	<!-- <xsl:template match="/">
		<qdc:metadata-record>
			<xsl:apply-templates select="//c[@pleade:break = 'true' and not(parent::pleade:subdoc-link)]" mode="oai"/>
			<xsl:apply-templates select="//eadheader[parent::header[@pleade:break='true']]" mode="oai"/>
			<xsl:copy-of select="$g-oai-context"/>
		</qdc:metadata-record>
	</xsl:template> -->
<!-- ***************** END general template ******************* -->

	<!-- Archival component resource -->
	<xsl:template match="c[not(@pleade:original-element = 'eadheader')]" mode="oai" >
		<xsl:call-template name="extractmetadata">
			<xsl:with-param name="id" select="@pleade:id"/>
		</xsl:call-template>
		<!--<xsl:variable name="url">
			<xsl:call-template name="get-link">
				<xsl:with-param name="id" select="@pleade:id"/>
				<xsl:with-param name="mode" select="$relationMode"/>
				<xsl:with-param name="url-base" select="$basicurl"/>
			</xsl:call-template>
		</xsl:variable>-->
		<dc:identifier><xsl:value-of select="concat( $public-url, if(ends-with('/',$public-url)) then '' else '/', 'ead.html?',if(@pleade:eadid != '') then concat('id=', @pleade:eadid, '&amp;') else '', 'c=', @pleade:id )"/></dc:identifier>

		<!--<dc:identifier><xsl:value-of select="$url"/></dc:identifier>-->
	</xsl:template>

	<!-- Process eadheader resource -->
	<!-- <xsl:template match="eadheader" mode="oai">
		<xsl:variable name="url">
			<xsl:call-template name="get-link">
				<xsl:with-param name="id" select="eadid"/>
				<xsl:with-param name="mode" select="$relationMode"/>
				<xsl:with-param name="url-base" select="$basicurl"/>
			</xsl:call-template>
		</xsl:variable>
		<dc:identifier><xsl:value-of select="$url"/></dc:identifier>
		<xsl:call-template name="ead"/>
		<xsl:apply-templates select="." mode="meta-oai"/>
	</xsl:template> -->

</xsl:stylesheet>
