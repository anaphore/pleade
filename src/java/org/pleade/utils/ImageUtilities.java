/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Map;

import org.apache.avalon.framework.logger.Logger;
import org.devlib.schmidt.imageinfo.ImageInfo;

import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.TIFFDirectory;

public class ImageUtilities {
    
    public static ImageInformation getImageInformation(Map objectModel, Logger logger, String systemId, InputStream is) {
        if ( systemId == null || is == null ) return null;

        // On essaie de trouver le type MIME
        String mimeType = NetUtilities.getMimeType(objectModel, systemId);
        
        // On initialise les autres valeurs
        int width = -1;
        int height = -1;
        int dpi = -1;
        
        // On cherche largeur, hauteur et resolution

        // On commence avec ImageInfo (PNG, GIF, JPEG, ... pas mal de choses sauf TIFF)
        boolean informationsTrouvees = false;
        ImageInfo ii = new ImageInfo();
        RandomAccessFile RAF = null;
        try {
            ii.setInput(is);
            if ( ii.check() ) {
                // Un format supporte, on sort les informations
                width = ii.getWidth();
                height = ii.getHeight();
                if ( ii.getPhysicalWidthDpi() > 0 ) dpi = ii.getPhysicalWidthDpi();
                informationsTrouvees = true;
            }
        } 
        finally {
            try {
                if (RAF != null) RAF.close();
            }
            catch (IOException e) {
                if (logger != null ) logger.error("Probleme lors de l'ouverture du fichier", e);
            }
        }
        
        // On verifie si c'est du TIFF
        if ( !informationsTrouvees && mimeType != null && mimeType.equals("image/tiff") ) {
            RandomAccessFileOrArray RAFA = null;
            try {
                RAFA = new RandomAccessFileOrArray(is);
                TIFFDirectory td = new TIFFDirectory(RAFA, 0);
                width = (int)td.getFieldAsLong(256);
                height = (int)td.getFieldAsLong(257);
                // Pour le DPI on suppose des pouces (peut etre indique dans le tag 296)
                // On prend egalement la resolution en X (le Y est donne par le tag 283)
                dpi = Math.round(td.getFieldAsFloat(282));
            }
            catch (IOException e) {
                if (logger != null ) logger.error("Probleme lors de la lecture des champs TIFF", e);
            }
            finally {
                try {
                    RAFA.close();
                } 
                catch (IOException e) {
                    if (logger != null ) logger.error("Probleme lors de la lecture des champs TIFF", e);
                }
            }
        }
        
        // Dans tous les cas on retourne un objet
        return new ImageInformation(width, height, dpi, mimeType);
    }
    
}
