<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: ead2html-index.xsl 17976 2010-02-22 09:02:41Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:h="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="xsl i18n pleade h">

	<!--		CETTE TRANSFORMATION GENERE LA PAGE D'INDEX ET LES FRAGMENTS HTML. ELLE EST UTILISEE EN STATIQUE (uniqument quand $full = "false") ET EN DYNAMIQUE

					** EN STATIQUE $mode = "static" **
						- s'applique sur le modele.html
						- créer index.html
						- créer aussi tous les fragments html (import de ead2html.xsl et appliqué sur document($eadDoc.path) )

					** EN DYNAMIQUE $mode = "dyn" **
						- s'applique sur cocoon:/functions/ead/get-properties/{request-param:id}/display.xml qui contient les propriétés nécessaires à l'affichage de cette page (titre, archdesc, eadid)
						- créer dynamiquement la page d'index de la visionneuse html.
	-->

	<xsl:import href="common.xsl"/>
	<xsl:import href="ead2html.xsl"/>
	<!--pour construire le bandeau du titre toujours de la même manière on importe pleade-aggregate.xsl-->
	<!--<xsl:import href="../../../../theme/pleade-aggregate.xsl"/>-->

	<xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"  />
	<!-- FIXME : 	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>-->

	<xsl:param name="mode" select="'static'"/>
	<xsl:param name="build.dir" select="''"/>
	<xsl:param name="docid" select="''"/>
	<xsl:param name="eadDoc.url" select="''"/>
  <xsl:param name="active-basket" select="'true'" />
  <xsl:param name="active-eac" select="'true'" />
  <xsl:param name="active-pdf" select="'true'" />
  <xsl:param name="qid" select="''" />

	<!--FIXME : pas la peine de mettre dans une variable, faire l'appel directe document(...), ce n'est pas plus gourmand en xslt-->
	<xsl:variable name="eadDoc.path" select="concat( '../../../', $build.dir, '/_tmp/', $docid, '/', $docid, '.xml')"/>
	<!--FIXME : dossier _tmp peut être surchargé avec le build : il faut que ce path soit envoyé depuis le build pour être bien générique-->
	<!--<xsl:variable name="eadDoc" select="document( $eadDoc.url )"/>--> <!--FIXME : ne créer pas les fragments... !!!-->

	<!--<xsl:variable name="theme-relPath" select="'../_theme'"/>-->
	<xsl:variable name="theme-relPath">
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:text>../_theme/</xsl:text>
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn' -->
			<!--<xsl:text>theme</xsl:text>-->
				<!--<xsl:text>../../../../theme</xsl:text>-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="eadid"><!--FIXME : cette variable doit absoluement correpondre à : ds/_temp/{docid}/toc/{docid}.xml/ead/@id -->
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<!-- FIXME: non, il faut lire les propriétés aussi -->
				<xsl:value-of select="normalize-space(document($eadDoc.path)/ead/eadheader/eadid)"/>
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn' -->
				<!--<xsl:value-of select="/root/properties/property[@name='eadid']"/>-->
				<xsl:value-of select="/properties/property[@name='eadid']"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="archdescid">
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<!-- FIXME: non, il faut lire les proprités aussi -->
				<xsl:value-of select="document($eadDoc.path)/ead/archdesc/@pleade:id"/>
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn' -->
				<!--<xsl:value-of select="/root/properties/property[@name='archdesc-id']"/>
				<xsl:value-of select="/properties/property[@name='archdesc-id']"/>-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- FIXME: le titre pourrait être plus compliqué... -->
	<xsl:variable name="title">
		<xsl:call-template name="title-builder" />
	</xsl:variable>

	<xsl:template name="title-builder">
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:value-of select="document($eadDoc.path)/ead/eadheader/filedesc/titlestmt/titleproper"/>
				<!--FIXME : et/ou eadheader/filedesc/seriesstmt/titleproper-->
				<!--et/ou frontmatter/titlepage/titleproper-->
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn' -->
				<!--<xsl:value-of select="/root/properties/property[@name='titleproper']"/>-->
				<xsl:value-of select="/properties/property[@name='titleproper']"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="/">
		<!--DEBUG-->
		<!--<xsl:message>$eadDoc.url : <xsl:value-of select="$eadDoc.url"/></xsl:message>-->
		<!--<xsl:message>name($eadDoc/*) : <xsl:value-of select="name($eadDoc/*)"/></xsl:message>-->
		<!--<xsl:message>mode : <xsl:value-of select="$mode"/></xsl:message>-->
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:result-document href="_tmp-make-fragment-out.html">
							<xsl:apply-templates select="document($eadDoc.path)/ead"/>
				</xsl:result-document>
				<xsl:apply-templates select="xhtml:html" mode="index"/>
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn'-->
				<html>
					<!--<xsl:apply-templates select="/root/properties" mode="index"/>-->
					<xsl:apply-templates select="/properties" mode="index"/>
				</html>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@* | node()" mode="index" >
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" mode="index"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="xhtml:img/@src" mode="index">
		<xsl:attribute name="src"><xsl:value-of select="$theme-relPath"/>/<xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

	<!-- Ce template est appelé en mode dynamique et il définit le head, augmentant ainsi le modèle. -->
	<xsl:template match="properties" mode="index">
		<head>
			<!-- TODO: titre correct -->
			<title><xsl:value-of select="$title"/> (<xsl:value-of select="$eadid"/>)</title> <!--Pleade 3 - Consultation d'un document EAD-->
			<i18n:text key="ead.meta.tags"><meta name="copyright" content="Copyright AJLSM - Anaphore"/></i18n:text>
			<script type="text/javascript">
				var qid = "<xsl:value-of select="$qid"/>";
			</script>
			<script type="text/javascript" src="{$theme-relPath}js/pleade/module/eadeac/EadContentManager.js">// </script>
			<script type="text/javascript" src="{$theme-relPath}js/pleade/module/eadeac/EadLayoutManager.js">// </script>
			<script type="text/javascript" src="{$theme-relPath}js/pleade/module/eadeac/IndexTreeView.js">// </script>
			<script type="text/javascript" src="{$theme-relPath}js/pleade/module/eadeac/TocTreeView.js">// </script>
			<script type="text/javascript" src="{$theme-relPath}js/pleade/module/eadeac/EadWindow.js">// </script>
			<!-- Les Javascript de YUI -->
			<script type="text/javascript" src="{$theme-relPath}yui/container/container-min.js">// </script>
			<script type="text/javascript" src="{$theme-relPath}yui/resize/resize-min.js">//</script>
			<script type="text/javascript" src="{$theme-relPath}yui/layout/layout-min.js">//</script>
			<xsl:if test="$active-basket='true'">
				<script type="text/javascript" src="{$theme-relPath}i18n/module-eadeac-basket-js.js">//</script>
			</xsl:if>

			<!-- Yui layout -->
			<link type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/container.css" rel="stylesheet" />
			<link rel="stylesheet" type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/resize.css" />
			<link rel="stylesheet" type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/layout.css" />
			<link href="{$theme-relPath}css/module-ead-xslt.css" type="text/css" rel="stylesheet" />

		<!-- <xsl:call-template name="add-headers"/>
			<xsl:call-template name="define-variable-script"/>
			 -->	
		</head>
		<body>
			<div class="pl-ignore-content-main">
				<h1 class="pl-title"><xsl:value-of select="$title"/></h1>
			</div>
		</body>
	</xsl:template>

	<xsl:template match="xhtml:head" mode="index">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
			<!-- TODO: mettre le titre de l'IR (template à faire, utiliser titlestmt... voire subtitle, à décider)-->
			<title><xsl:value-of select="$title"/> (<xsl:value-of select="$eadid"/>)</title> <!--Pleade 3 - Consultation d'un document EAD-->
			<i18n:text key="ead.meta.tags"><meta name="copyright" content="Copyright AJLSM - Anaphore"/></i18n:text>
			<!-- Décider de l'organisation des fichiers ressources -->
			<script type="text/javascript">
				var qid = "<xsl:value-of select="$qid"/>"; //
			</script>
			<!--mootools--><xsl:comment>Mootools</xsl:comment>
      <script type="text/javascript" src="{$theme-relPath}js/mootools/mootools-core-1.3.1-nc.js">// </script>
      <script type="text/javascript" src="{$theme-relPath}js/mootools/mootools-more-1.3.1.1-nc.js">// </script>
			<script type="text/javascript" src="{$theme-relPath}js/pleade/common/util/mootools-common.js">// </script>

			<!--YUI--><xsl:comment>YUI</xsl:comment>
			<script type="text/javascript" src="{$theme-relPath}yui/utilities/utilities.js">
				//
			</script>
			<script type="text/javascript" src="{$theme-relPath}yui/tabview/tabview-min.js">
				//
			</script>
			<script type="text/javascript" src="{$theme-relPath}yui/treeview/treeview-min.js">
				//
			</script>
			<script type="text/javascript" src="{$theme-relPath}yui/container/container-min.js">
				//
			</script>
			<link type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/tabview.css" rel="stylesheet" />
			<link type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/treeview.css" rel="stylesheet" />
			<link rel="stylesheet" type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/container.css" />
			<!--pleade--><xsl:comment>pleade</xsl:comment>
			<link href="{$theme-relPath}css/pleade.css" type="text/css" rel="stylesheet" />
			<link href="{$theme-relPath}css/module-eadeac.css" type="text/css" rel="stylesheet" />
			<link href="{$theme-relPath}css/module-ead-xslt.css" type="text/css" rel="stylesheet" />

			<!-- Yui layout -->
			<link rel="stylesheet" type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/resize.css" />
			<link rel="stylesheet" type="text/css" href="{$theme-relPath}yui/assets/skins/pleade/layout.css" />
			<script type="text/javascript" src="{$theme-relPath}yui/resize/resize-min.js">//</script>
			<script type="text/javascript" src="{$theme-relPath}yui/layout/layout-min.js">//</script>

			<xsl:call-template name="define-variable-script"/>
		</head>
	</xsl:template>

    <!--+
        | Permet d'ajouter "simplement" des headers au HTML
        +-->
		<xsl:template name="add-headers"/>

    <!--+
        | Variables Javascript pour la visionneuse dynamique
        +-->
    <xsl:template name="define-variable-script">
    <script type="text/javascript">
	//alert('ead2html-index.xsl declaration variable golbale');
      // variables globales pour module-ead-xslt.js
      <xsl:text>&#10;var activeBasket = "</xsl:text><xsl:value-of select="$active-basket" /><xsl:text>";</xsl:text>
      <!-- FIXME (MP) : Deux variables Javascript qui n'ont plus d'utilité apparemment
			<xsl:text>&#10;var activeEAC = "</xsl:text><xsl:value-of select="$active-eac" /><xsl:text>";</xsl:text>
      <xsl:text>&#10;var activePDF = "</xsl:text><xsl:value-of select="$active-pdf" /><xsl:text>";</xsl:text>-->
      // mode dyn ou static
      var mode = "<xsl:value-of select="$mode"/>";
      // pour charger le contenu par défaut
      var archdescID = "<xsl:value-of select="$archdescid"/>";
      // pour creer la TDM à partir des bons fichiers json
      var eadid = "<xsl:value-of select="$eadid"/>";
	  var base = "ead";
	  // variable permettant de gerer la fenetre ead
	  var eadWindow = '';
	  window.addEvent('domready', init);
	  window.addEvent('resize', resize);
		
		 function init(){
	  		// declaration des variables globale pour la fenetre ead
			eadWindow = new EadWindow();
			windowManager.mode = mode;
			windowManager.base = base;
		  }
	  
	  
		function resize(){
			if(eadWindow){
				eadWindow.adjustHeight();
			}
		}
	  
	  
			function initNotes() {<!-- Initialisation des boutons pour les notes EAD -->
				alert('ead2html-index.xsl initNotes');
				function initNote(p){
					p.style.display="block";
					var id = p.getAttribute("id");
					var idButton = "bttn_"+id;
					var panel = new YAHOO.widget.Panel(id,{underlay:"true",width:"350px",visible:false,draggable:true,constraintoviewport:true,context:[idButton, "tr", "bl"]});
					panel.setHeader("");
					panel.setFooter("");
					panel.render("container");
					$(idButton).addEvent( "click", function() {
						try {
							if( panel.cfg.getProperty("visible") ) panel.hide();
							else {panel.render();panel.show();}
						} catch(e) {panel.render();panel.show();}
					});
				}
				$$(".pl-ead-nt").each(function(p) { initNote(p); });
			}
    </script>
    </xsl:template>

    <xsl:template match="xhtml:div[@id = 'pl-pg-body-main-mcol']" mode="index">
      <xsl:text>Julien</xsl:text>
      <div id="pl-pg-body-main-mcol">
        <xsl:comment/>
      </div>
    </xsl:template>
    

	<xsl:template match="xhtml:*[@id = 'pl-title-ead']" mode="index"><!-- FIXME : /div/div ?-->
		<!--FIXME import de pleade-aggregate.xsl : l'import ne peut pas être conditionnel, trouver une solution :
		=> créer 2 xsl : ead2html-static.xsl, ead2html-dyn.xsl qui importent les bonnes xsl avec les bons param définis dedans-->
		<!--<xsl:apply-templates select="." mode="template"/>-->
		<!--todo : a supprimer quand on aura fait les différentes xsl-->
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="template"/>
			<div class="pl-title">
				<div class="pl-title-brdr-t">
					<div class="pl-title-tr"><xsl:comment /></div>
					<div class="pl-title-tl"><xsl:comment /></div>
				</div>
				<div class="pl-title-main">
					<div class="pl-title-brdr-r">
						<div class="pl-title-brdr-l">
							<h1>
								<!--<xsl:if test="$class!=''">
									<xsl:attribute name="class">
										<xsl:value-of select="$class" />
									</xsl:attribute>
								</xsl:if>
								<xsl:apply-templates select="@*[local-name()!='class'] | node()" mode="main" />-->
								<xsl:value-of select="$title"/>
								<span class="pl-ead-eadid">
									<i18n:text>ead.eadid</i18n:text>
									<xsl:text> : </xsl:text>
									<xsl:value-of select="$eadid"/>
								</span>
							</h1>
						</div>
					</div>
				</div>
				<div class="pl-title-brdr-b">
					<div class="pl-title-br"><xsl:comment /></div>
					<div class="pl-title-bl"><xsl:comment /></div>
				</div>
			</div>
		</xsl:copy>
	</xsl:template>


	<xsl:template match="node()|@*" mode="template" priority="-1">
		<!--todo : a supprimer quand on aura fait les différentes xsl-->
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" mode="template" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="pleade:title" mode="index">
		<div style="display:none;">
			<h1 class="pl-title">
				<xsl:value-of select="$title"/><!--FIXME copy-of  ? -->
				<span class="pl-ead-eadid">
					<i18n:text>ead.eadid</i18n:text>
					<xsl:text> : </xsl:text>
					<xsl:value-of select="$eadid"/>
				</span>
			</h1>
		</div>
	</xsl:template>


</xsl:stylesheet>

