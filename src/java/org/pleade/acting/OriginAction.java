/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.acting;

import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.avalon.framework.thread.ThreadSafe;
import org.apache.cocoon.acting.ServiceableAction;
import org.apache.cocoon.environment.Cookie;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.Redirector;
import org.apache.cocoon.environment.Request;
import org.apache.cocoon.environment.Response;
import org.apache.cocoon.environment.Session;
import org.apache.cocoon.environment.SourceResolver;

/**
 * Une action qui permet d'identifier l'origine d'une requete, en fonction
 * d'une adresse IP, d'un header HTTP, ...
 * Pour l'instant, seul le controle de l'IP est implemente.
 *
 *
 * TODO (MP) : Comme on renvoit la premiere zone selectionnee, on pourrait
 * stopper le controle des qu'on a quelque chose.
 *
 * @author msevigny
 * @author mpichot
 *
 */
public class OriginAction extends ServiceableAction implements ThreadSafe, Configurable {

    /** Le nom de l'objet de session qui contient la zone d'origine */
    private final static String SESSION_OBJECT_ZONE = "pleade-zone";

    /** Le nom de l'objet de session qui contient les zones d'origine (separees par ";") */
    private final static String SESSION_OBJECT_ZONES = "pleade-zones";

    /** Le nom du parametre de sitemap qui contient la zone d'origine */
    private final static String SITEMAP_PARAM_ZONE = "pleade-zone";

    /** Le nom du parametre de sitemap qui contient les zones d'origine (separees par ";") */
    private final static String SITEMAP_PARAM_ZONES = "pleade-zones";
    
    private final static String REQUEST_PARAM_ZONE = "pleade-zone";

    private final static String REQUEST_PARAM_ZONES = "pleade-zones";
    
    private final static String COOKIE_NAME_ZONE = "pleade-zone";

    private final static String COOKIE_NAME_ZONES = "pleade-zones";

    /** Le prefixe des parametres de configuration qui identifient des zones par adresse IP */
    private final static String PREFIX_ZONE_IP = "zone-ip-";

    /** Le parametre passe a l'action qui contient les definitions de zones */
    private final static String PARAM_ZONE_DEFINITIONS = "zone-definitions";
    
    private final static String PARAM_ZONES_SEPARATOR = "zones-separator";
    
    public static final String STORE_REQUEST = "store-in-request";
    
    public static final String CREATE_SESSION = "create-session";
    
    public static final String STORE_SESSION = "store-in-session";
    
    public static final String STORE_COOKIE = "store-in-cookie";
    
    public static final String DEFAULT_ZONES_SEPARATOR = ",";
    
    private boolean storeInRequest;
    
    private boolean storeInSession;
    
    private boolean createSession;
    
    private boolean storeInCookie;
    
    private String zonesSeparator;


    /**
     * Configuration de l'action. On recupere les parametres et on interprete ceux
     * qui sont pertinents.
     */
    public void configure(Configuration config) throws ConfigurationException {
    	this.storeInRequest = config.getChild(STORE_REQUEST).getValueAsBoolean(false);
      this.createSession = config.getChild(CREATE_SESSION).getValueAsBoolean(false);
      this.storeInSession = config.getChild(STORE_SESSION).getValueAsBoolean(false);
      this.storeInCookie = config.getChild(STORE_COOKIE).getValueAsBoolean(false);
      if (getLogger().isDebugEnabled()) {
          getLogger().debug((this.storeInRequest ? "will" : "won't") + " set values in request");
          getLogger().debug((this.createSession ? "will" : "won't") + " create session");
          getLogger().debug((this.storeInSession ? "will" : "won't") + " set values in session");
          getLogger().debug((this.storeInCookie ? "will" : "won't") + " set values in cookies");
      }
    	this.zonesSeparator = config.getChild(PARAM_ZONES_SEPARATOR).getValue(this.DEFAULT_ZONES_SEPARATOR);
    }

    /**
     * L'exécution de l'action elle-même.
     */
    public Map act(Redirector redirector, SourceResolver resolver, Map objectModel, String src, Parameters params) throws Exception {
        // Le logger
        Logger l = getLogger();
        boolean debug = l.isDebugEnabled();
        // La requete
        Request req = ObjectModelHelper.getRequest(objectModel);
        // La liste des zones a retouner dans les variables de sitemap
        Vector zones = new Vector();
        // La valeur du parametre
        String pValue = params.getParameter(PARAM_ZONE_DEFINITIONS, null);
        if (debug) l.debug("Definitions des zones : " + pValue + "\n\t\tIp courante : " + req.getRemoteAddr());
        // On boucle sur les parties de cette valeur
        // Exemple: zone-ip-intranet:192\.168\.1\.123;zone-ip-internet:_default
        if ( pValue != null ) {
            // Chaque partie est separee par un ";"
            StringTokenizer st = new StringTokenizer(pValue, ";");
            String tok, tName, tValue, zone;
            while (st.hasMoreTokens()) {
                tok = st.nextToken().trim();
                if (debug) l.debug("Definition d'une zone : " + tok);
                // Dans une partie, on a nom:valeur
                // Exemple: zone-ip-intranet:192\.168\.1\.123
                if ( tok != null ) {
                    int colonPos = tok.indexOf(":");
                    if ( colonPos > 0 && colonPos < tok.length()-1 ) {
                        tName = tok.substring(0, colonPos);
                        tValue = tok.substring(colonPos + 1);
                        if (debug) l.debug("Zone " + tName + " ; regexp = " + tValue);
                        // On peut maintenant verifier
                        zone = checkZones(req, tName, tValue);
                        if ( zone != null ) {
													zones.add(zone);
												}
                    }
                 }
                tok = null; tName = null; tValue = null; zone = null;
            }
        }
        pValue = null;
        // On construit la chaine de sortie
        String sZones = "";     // Toutes les zones separees par un ";"
        String sZone = "";      // La premiere seulement
        for (int i=0; i<zones.size(); i++) {
            if (debug) l.debug("Requete fait partie de " + (String)zones.get(i));
            if (i>0) sZones += this.zonesSeparator;
            else sZone = (String)zones.get(i);  // La premiere zone trouvee
            sZones += (String)zones.get(i);
        }
        zones = null;
        // On veut peut-etre stocker le resultat
        store(sZones,sZone, objectModel);
        // On retourne deux parametres
        objectModel.put(SITEMAP_PARAM_ZONES, sZones);
        objectModel.put(SITEMAP_PARAM_ZONE, sZone);
        return objectModel;
    }
    
    /** Stocke la ou les zones auxquelles la requete courante appartient.
     * Le resultat est stocke dans la requete, la session et ou un cookie suivant
     * la configuration de l'action.
     * 
     * @param sZones
     * @param sZone
     * @param objectModel
     */
    private void store(String sZones, String sZone, Map objectModel){
    	// store in a request if so configured
      if (this.storeInRequest) {
          Request request = ObjectModelHelper.getRequest(objectModel);
          request.setAttribute(REQUEST_PARAM_ZONES, sZones);
          request.setAttribute(REQUEST_PARAM_ZONE, sZone);
      }
      // store in session if so configured
      if (this.storeInSession) {
          Request request = ObjectModelHelper.getRequest(objectModel);
          Session session = request.getSession(this.createSession);
          if (session != null) {
							if ( !sZones.equals( (String) session.getAttribute(SESSION_OBJECT_ZONES)) )
		              session.setAttribute(SESSION_OBJECT_ZONES, sZones);
							if ( !sZone.equals( (String) session.getAttribute(SESSION_OBJECT_ZONE)) )
		              session.setAttribute(SESSION_OBJECT_ZONE, sZone);
          }
      }
      // store in a cookie if so configured
      if (this.storeInCookie) {
          Response response = ObjectModelHelper.getResponse(objectModel);
					Map cookies = ( ObjectModelHelper.getRequest(objectModel) ).getCookieMap();
					if ( cookies==null || !cookies.containsKey(COOKIE_NAME_ZONES) || !sZones.equals( ( (Cookie) cookies.get(COOKIE_NAME_ZONES) ).getValue() ) )
		          response.addCookie(response.createCookie(COOKIE_NAME_ZONES, sZones));
					if ( cookies==null || !cookies.containsKey(COOKIE_NAME_ZONE) || !sZones.equals( ( (Cookie) cookies.get(COOKIE_NAME_ZONE) ).getValue() ) )
          		response.addCookie(response.createCookie(COOKIE_NAME_ZONE, sZone));
      }
    }

    /**
     * Verifie si une requete provient d'une zone. Retourne le nom de la zone
     * ou null si la condition n'est pas satisfaite.
     */
    private String checkZones(Request req, String name, String value) {
        // On traite uniquement si on connait le parametre
        if ( name != null && name.startsWith(PREFIX_ZONE_IP) && !name.equals(PREFIX_ZONE_IP) ) {
            if (Pattern.matches(value, req.getRemoteAddr())) {
                return name.substring(PREFIX_ZONE_IP.length());  // Apres le "-"
            }
        }
        // FIXME: autre chose que zone-ip?
        // Si on n'a rien trouve, on retourne null
        return null;
    }

}
