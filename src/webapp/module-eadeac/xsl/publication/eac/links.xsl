<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Première étape de transformation : traitement des liens essentiellement. -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
  xmlns:saxon="http://saxonf.sf.net"
  exclude-result-prefixes="saxon"
>

	<xsl:include href="../../../../commons/xsl/functions.xsl"/>

	<!-- Ce paramètre est envoyé par SDX (peut ne pas exister, si on indexe un DOM par exemple) -->
	<xsl:param name="docUrl" select="''"/>	<!-- TODO: statique -->

  <xsl:param name="debug" select="'false'"/>

  <!-- Template racine -->
  <xsl:template match="/">
    <xsl:message>	Traitement des liens (etape 4 / 6) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
    <xsl:choose>
      <xsl:when test="$debug='true'">
        <xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
        <xsl:copy-of saxon:read-once="yes" select="$result"/>
        <xsl:if test="$debug='true'">
          <xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{//*[local-name() = 'control']/*[local-name() = 'recordId']}/{//*[local-name() = 'control']/*[local-name() = 'recordId']}-4.xml"
            xmlns:sys="java:java.lang.System">
            <xsl:copy-of saxon:read-once="yes" select="$result"/>
          </xsl:result-document>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

	<!-- On intercepte les éléments de liens -->
	<!-- TODO : code repris de Pleade pour les docs EAD sans adaptation ; il faut le modifier pour les documents EAC -->
	<xsl:template match="ref|ptr|archref|extref|extptr|eacrel|resourcerel">
		<!-- La cible -->
		<xsl:variable name="ref">
			<xsl:choose>
				<xsl:when test="@syskey"><xsl:value-of select="@syskey"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="@href"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- On copie, mais en ajoutant des attributs -->
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<!-- On traite le cas où il y a un target -->
				<xsl:when test="@target">
					<!-- On ajoute ce qu'il faut pour identifier la cible et son fragment -->
					<xsl:variable name="target" select="@target"/>
					<xsl:attribute name="pleade:link-type">internal</xsl:attribute>
					<xsl:attribute name="pleade:target-id">
						<xsl:value-of select="//*[@id = $target]/@pleade:id"/>
					</xsl:attribute>
				</xsl:when>
				<!-- Les URLs absolues -->
				<xsl:when test="fn:matches($ref, '^[a-z]+://')">
					<xsl:attribute name="pleade:link-type">absolute</xsl:attribute>
				</xsl:when>
				<!-- Les URLs relatives -->
				<xsl:when test="normalize-space($ref) != ''">
					<xsl:variable name="abs" select="fn:resolve-uri($ref, $docUrl)"/>
					<xsl:attribute name="pleade:link-type">relative</xsl:attribute>
					<xsl:attribute name="pleade:href"><xsl:value-of select="$abs"/></xsl:attribute>
					<xsl:attribute name="pleade:doc"><xsl:value-of select="pleade:get-url-file-wo-extension($ref)"/></xsl:attribute>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- De manière générale on copie le contenu -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
