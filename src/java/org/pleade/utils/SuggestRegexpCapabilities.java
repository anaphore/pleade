/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils;

import org.apache.lucene.search.regex.RegexCapabilities;
import org.apache.lucene.search.regex.RegexTermEnum;
import org.apache.regexp.RE;
import org.apache.regexp.RESyntaxException;
import org.apache.regexp.RegexpTunnel;
/**
 * Le n&eacute;cessaire pour faire un {@link RegexTermEnum} avec une expression
 * reguli&egrave;re insensible &agrave; la casse.
 * 
 * @author Malo Pichot <malo.pichot@ajlsm.com>
 */
public class SuggestRegexpCapabilities
		implements RegexCapabilities
{

	private RE regexp;

	/** Compiles the regular expression
	 * 
	 * @param pattern The regular expression (as a {@link String}) to compile.
	 * @see org.apache.lucene.search.regex.RegexCapabilities#compile(java.lang.String)
	 * @throws {@link RESyntaxException}
	 */
	public void compile(String pattern) throws RESyntaxException {
		regexp = new RE(pattern, RE.MATCH_CASEINDEPENDENT);
	}

	public boolean match(String string) {
		return regexp.match(string);
	}

	public String prefix() {
		char[] prefix = RegexpTunnel.getPrefix(regexp);
		return prefix == null ? null : new String(prefix);
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final SuggestRegexpCapabilities that = (SuggestRegexpCapabilities) o;

		if (regexp != null ? !regexp.equals(that.regexp) : that.regexp != null) return false;

		return true;
	}

	public int hashCode() {
		return (regexp != null ? regexp.hashCode() : 0);
	}

}
