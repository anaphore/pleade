<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT qui affiche le résultat d'une recherche
		| Ici, on ne traite que sdx:result, la XSL est ainsi utiliisée à la fois 
		| par l'affichage de la page HTML standard et ppar l'affichage AJAX.
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		exclude-result-prefixes="xsl xsp sdx i18n pleade fn">

	<!-- Le code pour traiter la navigation d'une page à l'autre -->
	<xsl:import href="navigation.xsl"/>

	<xsl:param name="active-basket" select="'true'" />

	<!-- Langue d'interface -->
	<xsl:param name="locale" select="'fr'"/>

	<!--separateur de Liste pour javascript-->
	<xsl:param name="list-sep" select="''" />

	<!--id du div dans lequel doivent apparaître les résultats-->
	<xsl:param name="divID" select="'_cdc_'"/>

	<xsl:param name="ead-viewer" select="''"/>

	<xsl:param name="qidParamName" select="'qid'" />

	<xsl:param name="base" select="/sdx:document/sdx:parameters/sdx:parameter[@name='base']/@value"/>

	<xsl:variable name="real-div-id">
		<xsl:choose>
			<xsl:when test="$divID = ''">_cdc_</xsl:when>
			<xsl:otherwise><xsl:value-of select="$divID"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Action par défaut pour l'appel direct -->
	<xsl:template match="/">
		<xsl:call-template name="display-results"/>
	</xsl:template>

  <!--+
      | Mise en page des résultats de recherche
      | Ce template est appellé par sdx-results.xsl
      | ou via ajax (dans la cas d'un changement de page pex).
      +-->
	<xsl:template name="display-results">
		<div>
		<!-- On n'affiche pas la pagination si on ne vient pas de la fenêtre EAD et que l'on a pas de résultats -->
		<xsl:if test="$ead-viewer = 'ead-viewer' or ( $ead-viewer = '' and /sdx:document/sdx:results/@nb &gt; 0)">
			<xsl:call-template name="pagination"/>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="/sdx:document/sdx:results/@nb &gt; 0">
				<ul>
					<xsl:apply-templates select="/sdx:document/sdx:results/sdx:result" mode="show-results"/>
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$ead-viewer = 'ead-viewer'">
					<xsl:apply-templates select="/sdx:document/message" mode="ajax"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
		</div>
	</xsl:template>

	<!-- Affichage des résultats eux-mêmes -->
	<xsl:template match="sdx:result" mode="show-results">
		<li>
			<xsl:if test="$active-basket='true'">
				<!-- FIXME: pas de base passée lors de l'ajout au panier ? -->
				<button class="pl-bskt-bttn-doc" title="basket.button.add-to-basket.doc.title" i18n:attr="title" onclick="basket.basketManager.addDocToBasket('id={sdx:field[@name='sdxdocid']}', this);" onkeypress="this.click();"><span class="access"><i18n:text key="basket.button.add-to-basket.doc.text">ajouter ce document</i18n:text></span></button>
			</xsl:if>
			<xsl:variable name="onclick">
				<xsl:choose>
					<xsl:when test="$ead-viewer = 'ead-viewer'">
						<xsl:text>eadWindow.getContentManager().loadFragment('ead-fragment.xsp?c=</xsl:text>
						<xsl:value-of select="sdx:field[@name='sdxdocid']"/>
						<xsl:text>&amp;qid=</xsl:text>
						<xsl:value-of select="/sdx:document/sdx:results/@id"/>
						<xsl:text>'); return false;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>return windowManager.winFocus(this.href, 'docead');</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<span class="cdc-unittitle">
				<a href="ead.html?id={sdx:field[@name='root-id']}&amp;c={sdx:field[@name='sdxdocid']}&amp;qid={/sdx:document/sdx:results/@id}" onclick="{$onclick}">
				<xsl:value-of select="sdx:field[@name='fucomptitle']"/>
				</a>
			</span>
			<xsl:if test="sdx:field[@name='uunitdate'] != ''"><xsl:value-of select="$list-sep"/><span class="cdc-unitdate"><xsl:value-of select="sdx:field[@name='uunitdate']"/></span></xsl:if>
			<xsl:if test="sdx:field[@name='uunitid'] != ''"><xsl:value-of select="$list-sep"/><span class="cdc-unitid"><xsl:value-of select="sdx:field[@name='uunitid']"/></span></xsl:if>
			<xsl:if test="sdx:field[@name='pacomm'] != ''"><xsl:value-of select="$list-sep"/><span class="cdc-pacomm"><xsl:value-of select="sdx:field[@name='pacomm']"/></span></xsl:if>
		</li>
	</xsl:template>

	<!-- Affichage de la pagination -->
	<xsl:template name="pagination">
		<div class="navigation-results">
			<p>
        <!-- La clé i18n selon singulier / pluriel -->
        <xsl:variable name="key-suffix">
            <xsl:choose>
                <xsl:when test="/sdx:document/sdx:results/@nb = 1">one</xsl:when>
                <xsl:otherwise>many</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
				<span class="nbresults">
                  <xsl:value-of select="/sdx:document/sdx:results/@nb"/>
                  <xsl:text> </xsl:text>
                  <i18n:text key="docsearch.results.{$key-suffix}"/>
				</span>
				<span class="nbpages"><xsl:text>Page </xsl:text><strong><xsl:value-of select="/sdx:document/sdx:results/@page"/></strong><xsl:text> de </xsl:text><xsl:value-of select="/sdx:document/sdx:results/@pages"/></span>
				<span id="{$real-div-id}_pagination" class="navpage">
					<xsl:apply-templates select="." mode="hpp">
						<xsl:with-param name="url" select="''" />
						<xsl:with-param name="query" select="concat('&amp;divID=', $divID)"/>
						<xsl:with-param name="qidParamName" select="$qidParamName"/>
					</xsl:apply-templates>
				</span>
				<xsl:if test="$active-basket='true'">
					<xsl:variable name="docids">
						<xsl:for-each select="/sdx:document/sdx:results/sdx:result">
							<xsl:value-of select="concat('id=', sdx:field[@name='sdxdocid']/@value)" />
							<!-- INFO (MP) : On n'envoie pas l'identifiant "URL
							encoded" car cela peut poser des problemes dans le cas ou
							cet identifiant possede des espaces et autre caractere
							interdit en URL.
							<xsl:value-of select="concat('id=', sdx:field[@name='sdxdocid']/@escapedValue)" />-->
							<xsl:if test="position()!=last()"><xsl:text>&#38;</xsl:text></xsl:if>
						</xsl:for-each>
					</xsl:variable>
					<span class="bskt-bttn">
						<button type="button" title="basket.button.add-to-basket.page.title"
										i18n:attr="title" class="pl-bskt-bttn-page"
										onclick="basket.basketManager.addPageToBasket('{$docids}', this, '{$base}');"
										onkeypress="basket.basketManager.addPageToBasket('{$docids}', this, '{$base}');"
						>
							<span class="access"><i18n:text key="basket.button.add-to-basket.page.text">
								ajouter cette page
							</i18n:text></span>
						</button>
						<button type="button" title="basket.button.add-to-basket.results.title"
										i18n:attr="title" class="pl-bskt-bttn-results"
										onclick="basket.basketManager.addResultsToBasket('qid={/sdx:document/sdx:results/@qid}', this, '{$base}');"
										onkeypress="basket.basketManager.addResultsToBasket('qid={/sdx:document/sdx:results/@qid}', this, '{$base}');"
						>
							<span class="access"><i18n:text key="basket.button.add-to-basket.results.text">
								ajouter tous les résultats
							</i18n:text></span>
						</button>
					</span>
				</xsl:if>
			</p>
		</div>
	</xsl:template>

	<xsl:template match="message[@type = 'with-dynamic-data' and @key]" mode="ajax">
		<p class="pl-message">
			<xsl:variable name="ik" select="@key"/>
			<xsl:for-each select="fn:tokenize(normalize-space(.), ';')">
				<i18n:text key="{concat($ik, '.', position())}"><xsl:value-of select="concat($ik, '.', position())"/></i18n:text>
				<xsl:value-of select="."/>
				<xsl:if test="position() = last()">
					<i18n:text key="{concat($ik, '.end')}"><xsl:value-of select="concat($ik, '.end')"/></i18n:text>
				</xsl:if>
			</xsl:for-each>
		</p>
	</xsl:template>

	<xsl:template match="message" mode="ajax">
		<p class="pl-message">
			<strong><i18n:text key="{@key}"><xsl:value-of select="."/></i18n:text></strong>
			<!--<xsl:apply-templates />-->
		</p>
	</xsl:template>

</xsl:stylesheet>
