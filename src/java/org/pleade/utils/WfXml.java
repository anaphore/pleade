package org.pleade.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

public class WfXml {
	public static final int PARSE_ERROR = 1;
	
	private File xmlFile;
	private static boolean _debug = false;
	private String errMessage = null;
	private int errCode = -1;
	private int errCol, errLine;
	private XMLReader xmlr;
	private long meminit, memout;

	public WfXml(String path){
		this(path, _debug, null);
	}

	public WfXml(String path, boolean debug){
		this(path, debug, null);
	}

	public WfXml(String path, XMLReader sp){
		this(path, _debug, sp);
	}

	public WfXml(String path, boolean debug, XMLReader xmlr){
		_debug = debug;
		this.xmlFile = new File(path);
		if ( xmlr == null ) {
			try {
				SAXParserFactory spfactory = SAXParserFactory.newInstance();
				SAXParser sp = spfactory.newSAXParser();
				this.xmlr = sp.getXMLReader();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
		} else {
			this.xmlr = xmlr;
		}
	}

	public boolean isWellFormed(){
		if( _debug ) {
			this.meminit = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();			
		}
		
		try {
			//we set an empty EntityResolver to avoid FileNotFoundException
			this.xmlr.setEntityResolver(new EntityResolver()
	        {
	            public InputSource resolveEntity(String publicId, String systemId)
	                throws SAXException, IOException
	            {
            		System.out.println("resolving entity");
	                return new InputSource(new StringReader(""));
	            }
	        });

			this.xmlr.parse(new InputSource(new FileInputStream(this.xmlFile)));
			if ( _debug ) {
				System.out.println("Document is well-formed.");
			}
			return true;
		} catch (SAXParseException se) {
			if ( _debug ) {
				System.err.println("Document is not well-formed.");
			}
			this.errCode = PARSE_ERROR;
			this.errLine = se.getLineNumber();
			this.errCol = se.getColumnNumber();
			this.errMessage = se.getMessage();
		} catch(FileNotFoundException fnfe){
			if ( _debug ) {
				System.err.println("File not found... Maybe a DTD declaration that cannot be resolved?");
			}
			this.errMessage = fnfe.getMessage();
			if( fnfe.getCause() != null && _debug ) {
				System.err.println(fnfe.getCause());
			}
		} catch(IOException ioe) {
			if ( _debug ) {
				System.err.println("Cannot read file.");
			}
			this.errMessage = ioe.getMessage();
			ioe.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if( _debug ) {
				memout = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Memory consumed (approx.): " + (memout -meminit) / 1024 + "Ko");
			}
		}
		return false;
	}
	
	public int getErrorCode(){
		return this.errCode;
	}
	
	public int getErrorLine(){
		return this.errLine;
	}
	
	public int getErrorColumn(){
		return this.errCol;
	}

	public String getErrorMessage(){
		return this.errMessage;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (String s: args) {
			WfXml wf = new WfXml(s, true);
			if(!wf.isWellFormed()){
				System.err.println(wf.getErrorMessage() + "|Line: " + wf.getErrorLine() + " ; column: " + wf.getErrorColumn());
			}
		}
	}
}
