<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
	Transformation d'un fichier i18n en tableau associatif Javascript.
	Le nom du tableau est difini par l'attribut "javascript-var-name" de l'élément "catalogue".
	Le tableau est initialisé sans le mot clé "var". Cela permet d'utiliser un nom de tableau contenant un point,
	c'est a dire une propriété d'un tableau existant. Exemple : piv.messages (cf visionneuse).
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/catalogue">
		<root>
			<xsl:value-of select="(@javascript-var-name, 'pleade_EmptyMessages')[1], '= {',
					string-join(for $m in message return concat($m/@key, ':', '&quot;', replace($m, '&quot;', '\\&quot;'), '&quot;'), ','),
				'};'"/>
		</root>
	</xsl:template>

</xsl:stylesheet>
