<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		| Cette XSLT permet d'extraire une partie du cadre de classement
		| selon l'identifiant fourni. Si aucun identifiant n'est fourni,
		| elle envoit le premier niveau (archdesc).
		|
		| En entrée, elle reçoit:
		|	- le cadre de classement en EAD enrichi
		|	- un sdx:terms sur le champ cdcroot pour savoir quelles entrées du
		|	  cadre de classement ont au moins un IR attaché
		|
		| On l'utilse dans les 2 contextes suivants :
		| #1 : sommaire des fonds
		| #2 : résultats de recherche
		|
		| Si on a des termes "cdcroot", alors on navigue le cadre de classement.
		| Si on a des termes "cdcall", alors on a des résultats de recherche.
		|
		| Si une entrée possède :
		| # @altrender = 'hidden' : elle n'est pas traité, de même que ses enfants
		| # @altrender = 'hidden-in-{$context}' : l'entrée n'est pas affichée,
		|                                         seulement ses enfants
		|   Par exemple, @altrender = 'hidden-in-cdc', si le contexte courant est "
    +-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xsl sdx fn xs">

	<!-- Le paramètre id, qui peut être vide -->
	<xsl:param name="id" select="''"/>
	<!-- Clé de tri des instruments de recherche d'une rubrique du cdc -->
	<xsl:param name="sort-keys" select="'fatitle'"/>

	<!-- Indique si on doit explorer -->
	<xsl:param name="explore" select="''"/>

	<!-- Des indications par défaut pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand-default" select="''"/>

	<!-- Des indications dynamiques pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand" select="''"/>

	<xsl:param name="_cdcids" select="''"/>
	
	<!-- Un paramètre pour déclencher l'affichage d'informations de débogage -->
	<xsl:param name="debug" select="'false'"/>

	<!-- Un pointeur sur la liste des termes cdcroot -->
	<xsl:variable name="terms-cdcroot" select="/root/sdx:document/sdx:terms[@name = 'cdcroot']"/>
	<!-- Un pointeur sur la liste des termes cdcall -->
	<xsl:variable name="terms-cdcall" select="/root/sdx:document/sdx:terms[@name = 'cdcall']"/>
	<!-- Un pointeur sur la liste des termes cdc -->
	<xsl:variable name="terms-cdc" select="/root/sdx:document/sdx:terms[@name = 'cdc']"/>
	<!-- Un pointeur sur la liste des termes root-id -->
	<xsl:variable name="terms-root-id" select="/root/sdx:document/sdx:terms[@name = 'root-id']"/>

	<!-- Une variable pour savoir si on est en mode exploration -->
	<xsl:variable name="mode-explore" select="boolean($explore = 'true' or $terms-cdcroot)"/>

	<!-- Définir le context : 'cdc' ou 'results' ou rien -->
	<xsl:variable name="context" select="
			if ($terms-cdcroot) then 'cdc'
			else if ($terms-cdc) then 'results'
			else ''
	" />
	
	<!-- Définir le contexte de débogage -->
	<xsl:variable name="is-debug" as="xs:boolean" select="
		if($debug castable as xs:boolean) then xs:boolean($debug)
		else false()"/>

	<!-- L'élément racine du aggregate : on traite le document EAD uniquement -->
	<xsl:template match="/root">
		<xsl:if test="$is-debug">
			<xsl:message>&#10;===================================================================</xsl:message>
			<xsl:message>context=<xsl:value-of select="$context"/> ; id=<xsl:value-of select="$id"/></xsl:message>
		</xsl:if>
		<xsl:apply-templates select="ead"/>
		<xsl:if test="$is-debug"><xsl:message>===================================================================&#10;</xsl:message></xsl:if>
	</xsl:template>

	<!-- Le document EAD : on lance la sortie des informations -->
	<xsl:template match="ead">
		<xsl:if test="$is-debug"><xsl:message>&#10;traite ead</xsl:message></xsl:if>
		<!-- On doit d'abord identifier le noeud à traiter -->
		<xsl:choose>
			<xsl:when test="$id != ''">
				<!-- Un identifiant, alors on essaie de le traiter -->
				<xsl:variable name="component" select=".//*[@pleade:component = 'true' and @pleade:id = $id][1]"/>
				<xsl:if test="$is-debug"><xsl:message>	on a un id (<xsl:value-of select="$id"/>) appel son traitement mode root</xsl:message></xsl:if>
				<xsl:choose>
					<xsl:when test="$component">
						<xsl:apply-templates select="$component" mode="root"/>
					</xsl:when>
					<xsl:otherwise>
						<error>
							<message>Composant inconnu : <xsl:value-of select="$id"/></message>	<!-- TODO: i18n des messages d'erreur système ? -->
						</error>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Pas d'identifiant, alors on traite le archdesc (premier niveau) -->
				<xsl:if test="$is-debug"><xsl:message>	appel traitement archdesc mode root</xsl:message></xsl:if>
				<xsl:apply-templates select="archdesc" mode="root"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Piloter le traitement de l'entrée principale. -->
	<xsl:template match="*" mode="root">
		<xsl:if test="$is-debug"><xsl:message>&#10;traite <xsl:value-of select="name()"/><xsl:if test="@pleade:id"> ; id=<xsl:value-of select="@pleade:id"/></xsl:if><xsl:if test="@altrender"> / @altrender = <xsl:value-of select="@altrender"/> (context: <xsl:value-of select="$context"/>)</xsl:if> mode root</xsl:message></xsl:if>
		<xsl:choose>
		  <xsl:when test="@altrender = 'hidden'"/>
			<xsl:when test="substring-after(@altrender,'hidden-in-')!='' and $context!=''">
				<xsl:choose>
				  <xsl:when test="substring-after(@altrender,'hidden-in-') = $context"/>
				  <xsl:when test="substring-after(@altrender,'hidden-in-') != $context">
						<xsl:if test="$is-debug"><xsl:message>	on va traiter ce composant car il n'est pas a cacher dans ce contexte</xsl:message></xsl:if>
						<xsl:apply-templates select="." mode="do-childs" />
					</xsl:when>
				  <xsl:otherwise>
						<xsl:apply-templates select="." mode="do-root" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		  <xsl:otherwise>
				<xsl:apply-templates select="." mode="do-root" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- On ne recopie par le composant demandé, on veut seulement ses enfants -->
	<xsl:template match="*" mode="do-childs">
		<xsl:if test="$is-debug"><xsl:message>&#10;traite <xsl:value-of select="name()"/> <xsl:if test="@pleade:id">id=<xsl:value-of select="@pleade:id"/></xsl:if> mode do-childs</xsl:message></xsl:if>
		<c><!-- Un niveau car c'est obligatoire pour la suite (cf. cdc2json.xsl) -->
			<xsl:if test="$is-debug"><xsl:message>	appel le traitement des inclusions s'il y en a</xsl:message></xsl:if>
			<xsl:apply-templates select="." mode="do-includes" />
			<!-- On traite ensuite les sous-unités -->
			<xsl:if test="$is-debug"><xsl:message>	appel le traitement des autres composants en mode copy-component</xsl:message></xsl:if>
			<xsl:apply-templates select="*[self::dsc or @pleade:component = 'true']" mode="copy-component"/>
		</c>
	</xsl:template>

	<!-- On recopie le composant demandé -->
	<xsl:template match="*" mode="do-root">

		<xsl:if test="$is-debug"><xsl:message>&#10;traite <xsl:value-of select="name()"/><xsl:if test="@pleade:id"> ; id=<xsl:value-of select="@pleade:id"/></xsl:if> mode do-root</xsl:message></xsl:if>
		<!-- Toujours un élément c -->
		<c>
			<!-- On copie d'abord les attributs -->
			<xsl:apply-templates select="@*" mode="copy-component"/>

			<!-- Ensuite le contenu sauf les sous-composants -->
			<xsl:if test="$is-debug"><xsl:message>	appel le traitement de son contenu sauf ses sous-composants</xsl:message></xsl:if>
			<xsl:apply-templates select="*[not(self::dsc or @pleade:component = 'true')]" mode="copy-component"/>

			<!-- Ensuite prépare les inclusions de résultats s'il y en a -->
			<xsl:if test="$is-debug"><xsl:message>	appel le traitement des inclusions s'il y en a</xsl:message></xsl:if>
			<xsl:apply-templates select="." mode="do-includes">
				<xsl:with-param name="is-root" select="true()"/><!-- Un paramètre indiquant qu'on traite la racine -->
			</xsl:apply-templates>

			<!-- On traite ensuite les sous-unités -->
			<xsl:if test="$is-debug"><xsl:message>	appel le traitement des autres composants en mode copy-component</xsl:message></xsl:if>
			<xsl:apply-templates select="*[self::dsc or @pleade:component = 'true']" mode="copy-component"/>
		</c>

	</xsl:template>

	<!-- Préparer les inclusions de documents -->
	<xsl:template match="*" mode="do-includes">
	
		<xsl:param name="is-root" select="false()"/>

		<!-- L'identifiant du composant -->
		<xsl:variable name="component-id" select="@pleade:id"/>
		
		<xsl:if test="$is-debug"><xsl:message>&#10;traite <xsl:value-of select="name()"/><xsl:if test="@pleade:id"> ; id=<xsl:value-of select="@pleade:id"/></xsl:if> (is-root=<xsl:value-of select="$is-root"/>) mode do-includes</xsl:message></xsl:if>

		<!-- Clés de tri -->
		<xsl:variable name="param-sort-keys">
			<xsl:for-each select="tokenize(normalize-space($sort-keys), ';')">
				<xsl:text>&amp;sf=</xsl:text>
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:choose>

			<!-- Le cas de la navigation : on inclut les documents -->
			<xsl:when test="$context = 'cdc'">
				<xsl:variable name="docs" select="$terms-cdcroot/sdx:term[@value = $component-id]"/>
				<xsl:if test="$docs">
					<xi:include href="cocoon:/functions/ead/cdc-documents.xml?cdc={$component-id}{$param-sort-keys}"/>
				</xsl:if>
			</xsl:when>

			<!-- Cas des résultats : on inclut des sous-documents mais uniquement s'ils ont des résultats directement -->
			<xsl:when test="$context = 'results'">
				<xsl:variable name="docs" select="$terms-cdc/sdx:term[@value = $component-id]"/>
				<xsl:if test="$is-debug"><xsl:message>	do-includes en context=<xsl:value-of select="$context"/> ; component-id=<xsl:value-of select="$component-id"/> ; docs=<xsl:value-of select="if($docs) then true() else false()"/></xsl:message></xsl:if>

				<!-- Ce niveau possède des résultats, on les inclus -->
				<xsl:if test="$docs">
					<xi:include href="cocoon:/functions/ead/cdc-documents-results.xml?rqid={$terms-root-id/@id}&amp;cdc={$component-id}{$param-sort-keys}"/>
				</xsl:if>

				<xsl:choose>
					<!-- On traite la racine ; il ne faut pas appeler le traitement des sous-composants, car on l'a déjà fait plus haut. -->
					<xsl:when test="$is-root">
						<xsl:if test="$is-debug"><xsl:message>	on traite le "root" : on ne va donc pas appeler le traitement des autres composants en mode copy-component</xsl:message></xsl:if>
					</xsl:when>
					<!-- On ne traite pas la racine ; on appelle donc le traitement de ses sous-composants -->
					<xsl:otherwise>
						<xsl:if test="$is-debug"><xsl:message>	on ne traite pas le "root" : appel le traitement des autres composants en mode copy-component</xsl:message></xsl:if>
						<xsl:apply-templates select="*[self::dsc or @pleade:component = 'true']" mode="copy-component"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

		</xsl:choose>

	</xsl:template>

	<!-- Lorsqu'on intercepte un composant, on sort des informations mais on arrête le traitement -->
	<xsl:template match="*[@pleade:component = 'true']" mode="copy-component">
		<xsl:if test="$is-debug"><xsl:message>&#10;traite <xsl:value-of select="name()"/><xsl:if test="@pleade:id"> ; id=<xsl:value-of select="@pleade:id"/></xsl:if><xsl:if test="@altrender"> / @altrender = <xsl:value-of select="@altrender"/> (context: <xsl:value-of select="$context"/>)</xsl:if> mode copy-component</xsl:message></xsl:if>
		<xsl:choose>
		  <xsl:when test="@altrender = 'hidden'"/>
			<xsl:when test="substring-after(@altrender,'hidden-in-')!='' and $context!=''">
				<xsl:choose>
				  <xsl:when test="substring-after(@altrender,'hidden-in-') = $context"/>
				  <xsl:when test="substring-after(@altrender,'hidden-in-') != $context">
						<xsl:if test="$is-debug"><xsl:message>	on va traiter ce composant car il n'est pas a cacher dans ce contexte</xsl:message></xsl:if>
						<xsl:apply-templates select="." mode="do-copy-child-component" />
					</xsl:when>
				  <xsl:otherwise>
						<xsl:apply-templates select="." mode="do-copy-component" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		  <xsl:otherwise>
				<xsl:apply-templates select="." mode="do-copy-component" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement d'une entrée du cadre de classement en mode "enfant seulement"
	On ne veut pas afficher l'entrée courante, seuls ses enfants nous intéressent.-->
	<xsl:template match="*[@pleade:component = 'true']" mode="do-copy-child-component">
		<xsl:if test="$is-debug"><xsl:message>&#10;traite <xsl:value-of select="name()"/><xsl:if test="@pleade:id"> ; id=<xsl:value-of select="@pleade:id"/></xsl:if> mode do-copy-child-component</xsl:message></xsl:if>
		<xsl:apply-templates select="." mode="do-includes" />
	</xsl:template>

	<!-- Traitement d'une entrée du cadre de classement. -->
	<xsl:template match="*[@pleade:component = 'true']" mode="do-copy-component">

			<!-- L'identifiant du composant -->
			<xsl:variable name="component-id" select="@pleade:id"/>

			<!-- On ne sort pas toujours l'information : on le fait uniquement si on est
				 en mode navigation ou si on a des résultats -->
			<xsl:if test="$mode-explore or $terms-cdcall/sdx:term[@value = $component-id]">

				<!-- On vérifie si des documents sont attachés à ce composant -->
				<xsl:variable name="docs" select="$terms-cdcroot/sdx:term[@value = $component-id]"/>
				<xsl:variable name="nbdocs">
					<xsl:choose>
						<xsl:when test="$docs"><xsl:value-of select="$docs/@docs"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<!-- On vérifie si des résultats sont attachés à ce composant -->
				<xsl:variable name="results" select="$terms-cdcall/sdx:term[@value = $component-id]"/>
				<xsl:variable name="nbresults">
					<xsl:choose>
						<xsl:when test="$results"><xsl:value-of select="$results/@docs"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<!-- On copie l'élément en y mettant uniquement les informations utiles -->
				<c>

					<xsl:apply-templates select="@*" mode="copy-component"/>	<!-- On conserve les attributs, peuvent être utiles -->

					<!-- Un attribut pour indiquer qu'il s'agit d'une entrée de cdc -->
					<xsl:attribute name="pleade:type">entry</xsl:attribute>

					<xsl:apply-templates select="." mode="att-has-children">
						<xsl:with-param name="nbdocs" select="$nbdocs" />
						<xsl:with-param name="component-id" select="$component-id" />
					</xsl:apply-templates>

					<xsl:apply-templates select="." mode="att-auto-expand">
						<xsl:with-param name="nbdocs" select="$nbdocs" />
						<xsl:with-param name="nbresults" select="$nbresults" />
					</xsl:apply-templates>

					<!-- On sort des informations sur le nombre de documents attachés -->
					<xsl:attribute name="nbdocs"><xsl:value-of select="$nbdocs"/></xsl:attribute>
					<!-- On sort des informations sur le nombre de documents attachés -->
					<xsl:attribute name="nbentries"><xsl:value-of select="count(child::*[@pleade:component = 'true'])"/></xsl:attribute>

					<!-- On sort des informations sur le nombre de résultats de recherche -->
					<xsl:attribute name="nbresults"><xsl:value-of select="$nbresults"/></xsl:attribute>

					<xsl:apply-templates select="." mode="att-moreinfo" />

					<!-- On copie le did uniquement -->
					<xsl:apply-templates select="did" mode="copy-component"/>

					<!-- FIXME [JC] : le code qui suit permet de renvoyer les enfants sélectionnés du noeud courant afin d'éviter les appels JSON
				 ; mais le treeview n'est actuellement pas capable de gérer cela correctement -->
					<!--<xsl:if test="$_cdcids != '' and tokenize(normalize-space($_cdcids), ';') = descendant::c/@pleade:id">
						<xsl:message>on a un enfant pour <xsl:value-of select="@pleade:id"/></xsl:message>
						<xsl:apply-templates select="dsc|*[@pleade:component = 'true']" mode="copy-component"/>
					</xsl:if>-->

				</c>

			</xsl:if>

	</xsl:template>

	<!-- Traitement de l'attribut "has-children" -->
	<xsl:template match="*" mode="att-has-children">
		<xsl:param name="nbdocs" />
		<xsl:param name="component-id" />
		<!-- On calcule si on a des enfants -->
		<xsl:variable name="hasChildren">
			<xsl:choose>
				<xsl:when test="*[@pleade:component='true'] or dsc//*[@pleade:component='true']">true</xsl:when>
				<xsl:when test="$nbdocs > 0">true</xsl:when>
				<xsl:when test="$terms-cdcall/sdx:term[@value = $component-id]">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Indique si on a des enfants (dans le cdc) -->
		<xsl:attribute name="hasChildren"><xsl:value-of select="$hasChildren"/></xsl:attribute>
	</xsl:template>

	<!-- Traitement de l'attribut "auto-expand" -->
	<xsl:template match="*" mode="att-auto-expand">
		<xsl:param name="nbdocs" />
		<xsl:param name="nbresults" />
		<!-- Un atribut pour indiquer si on doit auto-ouvrir cette entrée -->
		<xsl:attribute name="autoExpand">
			<!-- On va chercher la vraie valeur du paramètre: URL ou par défaut -->
			<xsl:variable name="ae">
				<xsl:choose>
					<xsl:when test="$auto-expand != ''"><xsl:value-of select="$auto-expand"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$auto-expand-default"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- En fonction du paramètre... -->
			<xsl:choose>
				<!-- Toujours -->
				<xsl:when test="$ae = '_all'">true</xsl:when>
				<!-- En fonction du niveau -->
				<xsl:when test="number($ae) &gt;= 0">
					<xsl:variable name="hlevel" select="count(ancestor::*[@pleade:component = 'true'])"/>
					<xsl:choose>
						<xsl:when test="$hlevel &lt;= $ae">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<!-- En fonction du nombre de documents ou d'unités -->
				<xsl:when test="$ae != ''">
					<!-- Une variable pour avoir le résultat -->
					<xsl:variable name="res">
						<!-- On a la forme "docs:n;units:n" où les deux parties sont optionnelles -->
						<xsl:for-each select="fn:tokenize($ae, ';')">
							<!-- Seulement si on a un : -->
							<xsl:if test="contains(., ':')">
								<xsl:variable name="parts" select="fn:tokenize(., ':')"/>
								<!-- Seulement si la deuxième partie est un nombre -->
								<xsl:if test="number($parts[2])">
									<xsl:variable name="nb" select="number($parts[2])"/>
									<xsl:choose>
										<xsl:when test="$parts[1] = 'docs' and $nbdocs &lt;= $nb">true</xsl:when>
										<xsl:when test="$parts[1] = 'units' and $nbresults &lt;= $nb">true</xsl:when>
										<!-- Tous les autres cas on n'ouvre pas -->
									</xsl:choose>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</xsl:variable>
					<!-- On peut sortir le résultat final -->
					<xsl:choose>
						<xsl:when test="$res != ''">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$_cdcids != '' and tokenize(normalize-space($_cdcids), ';') = descendant::c/@pleade:id">true</xsl:when>
				<!-- Sinon non -->
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>

	<!-- Traitement de l'attribut "moreinfo" -->
	<xsl:template match="*" mode="att-moreinfo">
		<!-- On indique si l'entrée possède des informations complémentaires -->
		<xsl:attribute name="moreinfo">
			<xsl:choose>
				<xsl:when test="*[not(self::did or @pleade:component or self::dsc or self::otherfindaid[normalize-space(.) = ''])]">true</xsl:when>
				<xsl:when test="did/*[not(self::unittitle or self::unitdate or self::unitid)]">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>

	<!-- Pour les dsc, on passe tout droit -->
	<xsl:template match="dsc" mode="copy-component">
		<xsl:apply-templates select="dsc|*[@pleade:component = 'true']" mode="copy-component"/>
	</xsl:template>

	<!-- Pour les autres, on recopie -->
	<xsl:template match="node()|@*" mode="copy-component">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" mode="copy-component"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
