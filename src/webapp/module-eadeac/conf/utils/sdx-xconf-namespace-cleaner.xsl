<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: sdx-xconf.xsl 12373 2008-10-15 14:41:34Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Construction de l'application.xconf
		| Nettoyage des espaces de noms surnuméraires
		+-->
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:map="http://apache.org/cocoon/sitemap/1.0"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	 xmlns="http://www.culture.gouv.fr/ns/sdx/sdx"
	 xmlns:jdate="java:java.util.Date"
	version="2.0"
	exclude-result-prefixes="sdx map jdate xi">

	<xsl:template match="/application | /sdx:application">
		<application date="{jdate:to-string(jdate:new())}">
			<xsl:apply-templates select="@* | node()" />
		</application>
	</xsl:template>

	<xsl:template match="sdx:*">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="@* | node()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="*">
		<xsl:element name="{name()}">
			<xsl:apply-templates select="@* | node()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="@xml:*">
		<xsl:copy-of select="." copy-namespaces="no" />
	</xsl:template>

	<xsl:template match="@*">
		<xsl:attribute name="{name()}">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>

	<xsl:template match="processing-instruction()">
		<xsl:copy-of select="." copy-namespaces="no" />
	</xsl:template>

	<xsl:template match="comment()"/>

</xsl:stylesheet>
