//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion d'index et de champs dans la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion d'index et de champs dans la partie admin
 * @author Julien Huet
 * @augments HelpPanels
 */
/** FIXME: ce fichier est-il vraiment utile */
var IndexAndFieldManager = new Class(/** @lends IndexAndFieldManager.prototype */{
	Implements: [HelpPanels],
	
	/**
	 * <strong>Constructor</strong><br/> Inialise le formulaire de gestion d'index et de champs
	 */
	initialize: function(){
		this.initHelpPanels();
	}
});