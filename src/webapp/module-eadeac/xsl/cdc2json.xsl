<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Cette XSLT permet de transformer une partie d'un cadre de classement
		| en format JSON.
		|
		| On l'utilse dans les 3 contextes suivants :
		| #1 : sommaire des fonds (cdc)
		| #2 : résultats de recherche (results)
		| #3 : formulaire de recherche (searchform)
		|
		| Il est possible de cacher un noeud en utilisant l'attribut @altrender d'un
		| composant "c". Les valeurs traitées ici sont :
		| # 'hidden' : le composant est caché dans tous les cas (ie, toutes les
		|              valeurs de contexte décrites plus haut).
		| # 'hidden-in-{$context}' : le composant est caché dans le contexte précis
    +-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xsl sdx pleade xs fn">


	<!-- Le contexte : cdc ou results ou searchform... -->
	<xsl:param name="context"/>

	<!-- Des indications par défaut pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand-default" select="''"/>

	<!-- Des indications dynamiques pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand" select="''"/>

	<!-- Les paramètres suivant sont utiles pour le contexte de résultats -->
	<xsl:param name="qcdcall"/>
	<xsl:param name="qcdc"/>
	<xsl:param name="qrootid"/>
	<xsl:param name="oid"/>

	<xsl:variable name="cdc-news-path" select="'cocoon://search-new-docs.xml'"/>

	<xsl:variable name="cdc-news">
		<xsl:if test="doc-available($cdc-news-path)">
			<xsl:copy-of select="document($cdc-news-path)/sdx:document/news"/>
		</xsl:if>
	</xsl:variable>

	<!-- S'il y a eu une erreur -->
	<xsl:template match="/error">
		<xsl:copy-of select="."/>
	</xsl:template>

	<!-- Match les "c" -->
	<xsl:template match="c">
		<xsl:choose>
			<xsl:when test="position()=1"><xsl:apply-templates select="." mode="first" /></xsl:when>
			<xsl:otherwise><xsl:apply-templates select="." mode="not-first" /></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement du premier "c" -->
	<xsl:template match="c" mode="first">
		<root array="true">	<!-- Pour forcer le résult JSON à être une array, même s'il y a un seul élément -->
			<xsl:apply-templates select="c" mode="not-first"/>
		</root>
	</xsl:template>

	<!-- Une entrée, qui peut être un document ou une partie du cadre de classement -->
	<xsl:template match="c" mode="not-first">
		<xsl:variable name="do"><xsl:apply-templates select="." mode="is-do" /></xsl:variable><!-- Marqueur indiquant si l'entrée courante sera traitée -->
		<xsl:if test="not($do castable as xs:boolean) or xs:boolean($do)">
			<c>
				<xsl:apply-templates select="." mode="atts-inf-gen" /><!-- Informations générales -->
				<xsl:apply-templates select="." mode="att-isnew" /><!-- Le document est-il nouveau ? -->
				<xsl:apply-templates select="." mode="att-hidden" /><!-- Les attributs hidden -->
				<xsl:apply-templates select="." mode="att-class-css" /><!-- Les classes CSS qu'on veut voir ajouter au noeud -->
        <xsl:apply-templates select="." mode="atts-illustrated" /><!-- Gestion des éléments illustrés -->
				<xsl:apply-templates select="." mode="atts-others" /><!-- D'autres attributs éventuellement -->
				<xsl:apply-templates select="." mode="att-href" /><!-- L'attribut @href -->
				<xsl:apply-templates select="." mode="content" /><!-- Intitulé, cote, ... -->
			</c>
		</xsl:if>
	</xsl:template>

	<!-- Une fonction pour savoir si on traite le noeud courant.
			 # Si @altrender ne contient pas la chaîne 'hidden', on traite
			 # Si @altrender = 'hidden', on ne traite pas (ie, on cache dans tous les cas)
			 # Si la sous-chaîne de 'hidden-in-' trouvée dans @altrender égale le
			   marqueur du contexte (cf. variable XSL "context"), on ne traite pas
			 -->
	<xsl:template match="c" mode="is-do">
		<xsl:value-of select="
			if ( not(contains(@altrender, 'hidden')) ) then true()
			else if ( @altrender='hidden' ) then false()
			else if ( substring-after(@altrender,'hidden-in-') = $context ) then false()
			else true()
		" />
	</xsl:template>

	<!-- Informations générales -->
	<xsl:template match="c" mode="atts-inf-gen">
		<id><xsl:value-of select="@pleade:id"/></id>
		<type><xsl:value-of select="@pleade:type"/></type>
		<base><xsl:value-of select="@pleade:base"/></base>
		<hasChildren><xsl:value-of select="@hasChildren"/></hasChildren>
		<autoExpand><xsl:value-of select="@autoExpand"/></autoExpand>
		<nbDocs><xsl:value-of select="@nbdocs"/></nbDocs>
		<nbEntries><xsl:value-of select="@nbentries"/></nbEntries>
		<nbResults><xsl:value-of select="@nbresults"/></nbResults>
		<moreInfo><xsl:value-of select="@moreinfo"/></moreInfo>
		<context><xsl:value-of select="$context"/></context>
	</xsl:template>

	<!-- Le document est-il nouveau ? -->
	<xsl:template match="c" mode="att-isnew">
		<xsl:variable name="myid" select="@pleade:id"/>
		<xsl:if test="$cdc-news/news/new[@sdxdocid=$myid]">
			<isnew>1</isnew>
		</xsl:if>
	</xsl:template>

	<!-- Les informations pour cacher le noeud -->
	<xsl:template match="c" mode="att-hidden">
		<xsl:if test="starts-with(@altrender,'hidden-')">
			<hidden><xsl:value-of select="substring-after(@altrender,'hidden-')" /></hidden>
		</xsl:if>
	</xsl:template>

	<!-- Les classes CSS qu'on veut voir ajouter au noeud -->
	<xsl:template match="c" mode="att-class-css">
		<xsl:variable name="styles"><xsl:apply-templates select="." mode="styles-css" /></xsl:variable>
		<xsl:if test="$styles!=''">
			<styles><xsl:value-of select="$styles" /></styles>
		</xsl:if>
	</xsl:template>
	<xsl:template match="c" mode="styles-css">
		<xsl:if test="@altrender!=''"><xsl:value-of select="@altrender" /></xsl:if>
		<!--<xsl:if test="starts-with(@altrender,'hidden')"><xsl:value-of select="@altrender" /></xsl:if>-->
	</xsl:template>

  <xsl:template match="c" mode="atts-illustrated">
    <illustratedChild><xsl:value-of select="@illustratedChild"/></illustratedChild>
  </xsl:template>

	<!-- Pour surcharges -->
	<xsl:template match="c" mode="atts-others"/>

	<!-- L'attribut @href -->
	<xsl:template match="c" mode="att-href">
		<!-- L'adresse dépend du fait qu'il s'agit d'un document ou d'une entrée, mais aussi qu'on soit en contexte de résultat ou de navigation -->
			<href>
				<xsl:choose>
					<xsl:when test="$context = 'cdc'">
						<xsl:choose>
							<xsl:when test="@pleade:type = 'document'">ead.html?id=<xsl:value-of select="@pleade:id"/></xsl:when>
							<xsl:otherwise>functions/ead/cdc.json?id=<xsl:value-of select="@pleade:id"/></xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$context = 'results'">
						<xsl:choose>
							<!-- <xsl:when test="@pleade:type = 'document'">docsearch.xsp?r=<xsl:value-of select="@pleade:id"/>&amp;bq=<xsl:value-of select="$oid"/></xsl:when> -->
							<xsl:when test="@pleade:type = 'document' and @pleade:base = 'ead'">docsearch.xsp?r=<xsl:value-of select="@pleade:id"/>&amp;bq=<xsl:value-of select="$oid"/></xsl:when>
							<xsl:when test="@pleade:type = 'document' and @pleade:base != 'ead'">module-<xsl:value-of select="@pleade:base"/>/docsearch.xsp?r=<xsl:value-of select="@pleade:id"/>&amp;bq=<xsl:value-of select="$oid"/></xsl:when>
							<xsl:otherwise>
								<!-- On va chercher la vraie valeur du paramètre: URL ou par défaut -->
								<xsl:variable name="ae">
									<xsl:choose>
										<xsl:when test="$auto-expand != ''"><xsl:value-of select="$auto-expand"/></xsl:when>
										<xsl:otherwise><xsl:value-of select="$auto-expand-default"/></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:value-of select="concat('functions/ead/cdc-results.json?id=', @pleade:id, '&amp;qcdcall=', $qcdcall, '&amp;qcdc=', $qcdc, '&amp;qrootid=', $qrootid, '&amp;oid=', $oid, '&amp;_ae=', $ae)"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$context = 'searchform'">functions/ead/cdc-searchform.json?id=<xsl:value-of select="@pleade:id"/></xsl:when>
				</xsl:choose>
			</href>
	</xsl:template>

	<!-- Intitulé, cote, ... -->
	<xsl:template match="c" mode="content">
		<xsl:apply-templates select="did/unittitle[1]"/>
		<xsl:apply-templates select="did/unitid[1]"/>
		<xsl:apply-templates select="did/unitdate[1]"/>
		<xsl:apply-templates select="c"/>
	</xsl:template>

	<xsl:template match="unittitle|unitdate|unitid">
		<xsl:element name="{local-name()}">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
