<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!-- XSLT qui produit du XSL-FO à partir d'un document HTML, pour des instruments
		de recherche EAD ou des parties d'instrument de recherche. -->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/XSL/Format" xmlns:i18n="http://apache.org/cocoon/i18n/2.1" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:pleade="http://pleade.org/ns/pleade/1.0" exclude-result-prefixes="xsl xhtml fn pleade">

	<!-- La définition des styles est dans un fichier à part -->
	<xsl:import href="html2fo-style.xsl"/>

	<!-- La sortie du pied de page est dans un fichier à part -->
	<xsl:import href="footer.xconf"/>

	<!-- La sortie de l'en-tête est dans un fichier à part -->
	<xsl:import href="header.xconf"/>

	<!-- Cette déclaration est utilisée en mode statique -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>

	<!-- Le mode : dyn ou static -->
	<xsl:param name="mode" select="'dyn'"/>

	<!-- Si on veut les unités enfant ou non -->
	<xsl:param name="children" select="'false'"/>
	<!-- La même chose mais dans une variable de type booléen -->
	<xsl:variable name="include-children" select="boolean($children = 'true' or number($children) > 0)"/>

	<!-- L'identifiant du document EAD -->
	<xsl:param name="eadid"/>

	<!-- L'identifiant de l'unité à afficher -->
	<xsl:param name="cid"/>

	<!-- L'ajout des images : valeurs possibles none | all | unit-only | {nombre}-limited -->
	<xsl:param name="images-in-pdf"/>

	<!-- URL interne de l'application qui permet de reconstituer l'URL absolue d'accès aux images -->
	<xsl:param name="url-interne"/>

	<!-- Taille des miniatures -->
	<xsl:param name="img-thumbnail-size" select="'150'"/>


	<!-- Le titre du composant principal à mettre en page -->
	<xsl:variable name="root-component-title">
		<xsl:choose>
			<xsl:when test="$mode='dyn'">
				<xsl:value-of select="/xhtml:html/xhtml:body/xhtml:div[@class='pl-ignore-content-main']/xhtml:h1[@class='pl-title']"/>
			</xsl:when>
		</xsl:choose>
	</xsl:variable>

	<!-- L'élément racine, qui est toujours un document HTML complet car on part
			de l'URL ead.xsp -->
	<xsl:template match="/xhtml:html">
		<root>
			<!-- La définition des pages -->
			<layout-master-set>
				<!-- Une page "normale" TODO: dans les styles-->
				<simple-page-master master-name="normal" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1.5cm">
					<!-- Le corps de la page -->
					<region-body margin-top="1.3cm" margin-bottom="1.3cm"/>
					<!-- L'espace pour l'en-tête -->
					<region-before extent="1cm"/>
					<!-- L'espace pour le pied de page -->
					<region-after extent="1cm"/>
				</simple-page-master>
			</layout-master-set>
			<!-- On démarre une séquence de pages pour le contenu principal -->
			<page-sequence master-reference="normal">
				<!-- L'en-tête -->
				<static-content flow-name="xsl-region-before">
					<!-- On appelle un template qui est dans une XSLT à part -->
					<xsl:call-template name="output-header">
						<xsl:with-param name="root-component-title" select="$root-component-title"/>
					</xsl:call-template>
				</static-content>
				<!-- Le pied de page -->
				<static-content flow-name="xsl-region-after">
					<!-- On appelle un template qui est dans une XSLT à part -->
					<xsl:call-template name="output-footer">
						<xsl:with-param name="root-component-title" select="$root-component-title"/>
					</xsl:call-template>
				</static-content>
				<!-- Le corps du document -->
				<flow flow-name="xsl-region-body">
					<!-- Un bloc pour avoir les attributs génériques -->
					<block xsl:use-attribute-sets="general">
						<xsl:apply-templates select="." mode="pdf-content"/>
					</block>
					<!-- On insère ici un marqueur afin de connaître le nombre total de pages -->
					<block id="last-page"/>
				</flow>
			</page-sequence>
		</root>
	</xsl:template>
	
	<!-- Ce template permet de surcharger le contenu du PDF. Ainsi, on peut réutiliser cette XSL pour d'autres exports PDF que celui d'un doc EAD ou d'un fragment -->
	<xsl:template match="xhtml:html" mode="pdf-content">
		<!-- Maintenant on traite l'unité à afficher -->
		<xsl:variable name="murl" select="concat('cocoon:/ead-fragment.xsp?pdf=true&amp;id=', $eadid, '&amp;c=', $cid, '&amp;children=', $children)" />
			<xsl:choose>
				<xsl:when test="doc-available($murl)">
					<xsl:apply-templates select="document($murl)"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- un message qui sort dans la console de Tomcat -->
					<xsl:message>[<xsl:value-of select="d:to-string(d:new())" xmlns:d="java:java.util.Date"/>][PLEADE][html2fo:line 158] Erreur lors de l'export PDF du document "<xsl:value-of select="$eadid" />" ; fragment:"<xsl:value-of select="$cid" />" ; children:"<xsl:value-of select="$children" />".</xsl:message>
				</xsl:otherwise>
			</xsl:choose>
	</xsl:template>

	<xsl:template match="xhtml:head" />

	<xsl:template match="xhtml:body">
		<xsl:apply-templates />
		<!--<xsl:message><xsl:value-of select=" fn:tokenize('abracadabra', '(ab)|(a)')"/></xsl:message>-->
		<!--<xsl:message><xsl:copy-of select="/*//*[contains(@class, 'pl-ead-att-level-fonds')]"/></xsl:message>-->
	</xsl:template>

	<!-- Un div -->
	<xsl:template match="xhtml:div | xhtml:p">
		<!-- Un paramètre pour un niveau par rapport au premier composant traité -->
		<xsl:param name="clevel" select="1" tunnel="yes"/>
		<!-- Les classes qui lui sont attribuées -->
		<xsl:variable name="classes" select="fn:tokenize(@class, '\s')"/>
		<!-- Le traitement peut varier en fonction des classes -->
		<xsl:choose>
			<!-- On ne sort jamais les ancêtres ici -->
			<xsl:when test="$classes[. = 'pl-pgd-ancestors']"/>
			<xsl:when test="$classes[. = 'pl-pgd-children']">
				<!-- Le div qui contient la liste des enfants -->
				<xsl:choose>
					<xsl:when test="$mode='dyn'">
						<!-- On doit soit sortir la liste des sous-unités, soit les inclure -->
						<xsl:choose>
							<!-- On doit traiter les enfants -->
							<xsl:when test="$include-children">
								<xsl:variable name="q">'</xsl:variable>
								<xsl:for-each select="xhtml:ul/xhtml:li/xhtml:a">
									<block xsl:use-attribute-sets="component-content">
										<xsl:variable name="murl" select="concat('cocoon:/ead-fragment.xsp?pdf=true&amp;c=', fn:tokenize(@onclick, $q)[2])" />
										<xsl:choose>
										  <xsl:when test="doc-available($murl)">
												<xsl:apply-templates select="document($murl)/xhtml:div">
													<xsl:with-param name="clevel" select="$clevel + 1" tunnel="yes"/>
													<xsl:with-param name="children" select="$children" tunnel="yes"/>
												</xsl:apply-templates>
											</xsl:when>
										  <xsl:otherwise>
												<!-- un message qui sort dans la console de Tomcat -->
												<xsl:message>[<xsl:value-of select="d:to-string(d:new())" xmlns:d="java:java.util.Date"/>][PLEADE][html2fo:line 158] Erreur lors de l'export PDF du fragment <xsl:value-of select="fn:tokenize(@onclick, $q)[2]" />.</xsl:message>
											</xsl:otherwise>
										</xsl:choose>
									</block>
								</xsl:for-each>
							</xsl:when>
							<!-- On doit uniquement sortir la liste -->
							<xsl:otherwise>
								<xsl:apply-templates/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise><!-- TODO: mode statique --></xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Le div qui contient un document -->
			<xsl:when test="$classes[. = 'pl-pgd-subdoc']">
				<xsl:choose>
					<xsl:when test="$clevel = 1">
						<block xsl:use-attribute-sets="component-title-1">
							<!-- On doit aussi sortir les ancêtres -->
							<xsl:apply-templates select="xhtml:div/xhtml:div/xhtml:div[@class = 'pl-pgd-ancestors']" mode="output"/>
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 2">
						<block xsl:use-attribute-sets="component-title-2">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 3">
						<block xsl:use-attribute-sets="component-title-3">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 4">
						<block xsl:use-attribute-sets="component-title-4">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 5">
						<block xsl:use-attribute-sets="component-title-5">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 6">
						<block xsl:use-attribute-sets="component-title-6">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 7">
						<block xsl:use-attribute-sets="component-title-7">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 8">
						<block xsl:use-attribute-sets="component-title-8">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 9">
						<block xsl:use-attribute-sets="component-title-9">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
					<xsl:when test="$clevel = 10">
						<block xsl:use-attribute-sets="component-title-10">
							<xsl:call-template name="process-subdoc-title"/>
						</block>
						<xsl:call-template name="process-subdoc"/>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<!-- TODO: poursuivre ici -->
			<xsl:when test="$classes[. = 'pl-pgd-c']">
				<xsl:element name="block" use-attribute-sets="pl-pgd-c">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:when test="$classes[. = 'pl-pgd-sect']">
				<xsl:element name="block" use-attribute-sets="pl-pgd-sect">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:when test="$classes[. = 'pl-pgd-titlepage-block']">
				<xsl:element name="block" use-attribute-sets="pl-pgd-titlepage-block">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:when test="$classes[. = 'pl-pgd-head']">
				<xsl:element name="block" use-attribute-sets="pl-pgd-head">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:when test="$classes[. = 'pl-pgd-list-head']">
				<xsl:element name="block" use-attribute-sets="pl-pgd-list-head">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="block" use-attribute-sets="block">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="xhtml:acronym">
		<xsl:value-of select="concat(normalize-space(.), ' (', @title, ')')"/>
	</xsl:template>

	<!-- Un span -->
	<xsl:template match="xhtml:span">
		<!-- Les classes qui lui sont attribuées -->
		<xsl:variable name="classes" select="fn:tokenize(@class, '\s')"/>
		<xsl:choose>
			<xsl:when test="$classes[. = 'pl-ead-controlaccess-group-header']">
				<inline xsl:use-attribute-sets="controlaccess-group-header">
					<xsl:apply-templates/>
				</inline>
			</xsl:when>
			<xsl:when test="$classes[. = 'pl-ead-att-render-italic']">
				<inline font-style="italic">
					<xsl:apply-templates/>
				</inline>
			</xsl:when>
			<xsl:when test="$classes[. = 'pl-tbl-th-sp']">
				<inline xsl:use-attribute-sets="did-label">
					<xsl:apply-templates/>
				</inline>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Les sauts de ligne -->
	<xsl:template match="xhtml:br">
		<!-- Le truc, c'est de faire un bloc vide -->
		<block/>
	</xsl:template>

	<xsl:template match="xhtml:script"/>

	<xsl:template match="xhtml:div[contains(@class, 'pl-ead-dao')]">
		<xsl:if test="not($images-in-pdf = 'none')">
			<xsl:apply-templates/>
		</xsl:if>
	</xsl:template>

	<!-- Une image dao -->
	<xsl:template match="xhtml:img[parent::xhtml:a[contains(@class, 'pl-pgd-medias-thumbnail')] and @src != '']">
		<xsl:variable name="img-url"
				select="concat($url-interne,
											( if(ends-with($url-interne, '/')) then '' else '/' ),
											( if(contains(@src,'?')) then substring-before(@src,'?') else @src ))" />
			<xsl:choose>
			<xsl:when test="$images-in-pdf = 'all'">
				<external-graphic src="{$img-url}" content-width="{$img-thumbnail-size}" content-height="{$img-thumbnail-size}" width="100%"/>
			</xsl:when>
			<xsl:when test="$images-in-pdf = 'unit-only'">
				<xsl:if test="not($include-children)">
					<external-graphic src="{$img-url}" content-width="{$img-thumbnail-size}" content-height="{$img-thumbnail-size}" width="100%"/>
				</xsl:if>
			</xsl:when>
			<!-- TODO : faire en sorte que le nombre des images puisse être limité donc il faut les compter  -->
			<xsl:when test="ends-with($images-in-pdf, '-limited')"></xsl:when>
			<!-- cas par défaut = $images-in-pdf = 'none' -->
		</xsl:choose>

	</xsl:template>

	<!-- Une fonction pour traiter le contenu d'un sous-document -->
	<xsl:template name="process-subdoc">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- Une fonction pour traiter le titre d'un sous-document -->
	<xsl:template name="process-subdoc-title">
		<!-- On doit sortir un marqueur -->
		<block>
			<marker marker-class-name="current-component-title">
				<xsl:value-of select="@pleade:title"/>
			</marker>
		</block>
		<!-- On va aussi sortir le titre pour bien le marquer -->
		<block xsl:use-attribute-sets="component-title">
			<xsl:value-of select="@pleade:title"/>
		</block>
	</xsl:template>

	<!-- Traitement des tableaux -->
	<xsl:template match="xhtml:table">
		<!-- Les classes qui lui sont attribuées -->
		<xsl:variable name="classes" select="fn:tokenize(@class, '\s')"/>
		<!-- On traite la légende à l'extérieur du tableau -->
		<xsl:apply-templates select="xhtml:caption"/>
		<xsl:if test="xhtml:tbody/xhtml:tr | xhtml:tr | xhtml:thead/xhtml:tr">	<!-- Il semble y avoir des tableaux vides parfois -->
			<xsl:choose>
				<!-- Les tableaux pour les champs / valeurs -->
				<xsl:when test="$classes[. = 'pl-pgd-fv']">
					<table xsl:use-attribute-sets="table-fv">
						<!-- La colonne de gauche peut être limitée à 4cm -->
						<table-column column-width="4cm"/>
						<!-- On ne définit pas la largeur de la seconde colonne : FOP lui donne l'espace qui reste -->
						<table-column/>
						<xsl:call-template name="process-table-content"/>
					</table>
				</xsl:when>
				<xsl:when test="contains(@class, 'pl-pgd-table')">
					<xsl:element name="table" use-attribute-sets="pl-pgd-table">
						<xsl:call-template name="process-table-content"/>
					</xsl:element>
				</xsl:when>
				<xsl:when test="contains(@class, 'pl-pgd-fv')">
					<xsl:element name="table" use-attribute-sets="pl-pgd-fv">
						<xsl:call-template name="process-table-content"/>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="table" use-attribute-sets="table">
						<xsl:call-template name="process-table-content"/>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

	</xsl:template>

	<!-- Traitement du contenu d'un tableau (mais sans les définitions de colonnes) -->
	<xsl:template name="process-table-content">
		<!-- On doit générer un table body s'il n'existe pas dans le HTML -->
		<xsl:choose>
			<xsl:when test="xhtml:thead">
				<xsl:apply-templates select="*[not(self::xhtml:caption)]"/>
			</xsl:when>
			<xsl:when test="xhtml:tbody">
				<xsl:apply-templates select="*[not(self::xhtml:caption)]"/>
			</xsl:when>
			<xsl:when test="xhtml:tr">
				<table-body>
					<xsl:apply-templates select="*[not(self::xhtml:caption)]"/>
				</table-body>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>Erreur tableau vide:</xsl:message>
				<xsl:message><xsl:copy-of select="."/></xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Corps d'un tableau -->
	<xsl:template match="xhtml:tbody">
		<table-body>
			<xsl:apply-templates />
		</table-body>
	</xsl:template>
	<!-- Entête d'un tableau  -->
	<xsl:template match="xhtml:thead">
		<table-header>
			<xsl:apply-templates />
		</table-header>
	</xsl:template>

	<!-- Légende de tableau : bloc normal TODO -->
	<xsl:template match="xhtml:caption">
		<xsl:if test="normalize-space(.) != ''">
			<xsl:choose>
				<xsl:when test="contains(@class, 'pl-pgd-head')">
					<xsl:element name="block" use-attribute-sets="caption pl-pgd-head">
						<xsl:apply-templates />
					</xsl:element>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!-- Rangée de tableau -->
	<xsl:template match="xhtml:tr">
		<xsl:variable name="classes" select="fn:tokenize(@class, '\s')"/>
		<xsl:choose>
			<xsl:when test="$classes[. = 'pl-pgd-eadheader-subtitle']">
				<table-row xsl:use-attribute-sets="pl-pgd-eadheader-subtitle">
					<xsl:apply-templates/>
				</table-row>
			</xsl:when>
			<xsl:otherwise>
				<table-row xsl:use-attribute-sets="table-row">
					<xsl:apply-templates />
				</table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Une cellule de tableau -->
	<xsl:template match="xhtml:td">
		<xsl:element name="table-cell" use-attribute-sets="td-as-table-cell">
			<xsl:call-template name="table-cell-innerfo"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="xhtml:th">
		<xsl:element name="table-cell" use-attribute-sets="th-as-table-cell">
			<xsl:call-template name="table-cell-innerfo"/>
		</xsl:element>
	</xsl:template>

	<!-- TODO -->
	<xsl:template name="table-cell-innerfo">
		<xsl:if test="@colspan != '1'">
			<xsl:attribute name="number-columns-spanned">
				<xsl:value-of select="@colspan"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="@row != '1'">
			<xsl:attribute name="number-rows-spanned">
				<xsl:value-of select="@rowspan"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:element name="block">
			<xsl:call-template name="extra-attributes"/>
			<xsl:apply-templates />
			<xsl:if test=". = ''">
				<xsl:text>&#160;</xsl:text>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<!-- Une liste -->
	<xsl:template match="xhtml:ul">
		<!-- Si on a ul/ul, alors il ne faut pas faire pareil en FO, c'est interdit.
				En XHTML aussi, mais Pleade en sort semble-t-il -->
		<xsl:choose>
			<xsl:when test="parent::xhtml:ul">
				<list-item>
					<list-item-label>
						<block/>
					</list-item-label>
					<list-item-body>
						<xsl:element name="list-block" use-attribute-sets="list-block">
							<xsl:call-template name="extra-attributes"/>
							<xsl:apply-templates />
						</xsl:element>
					</list-item-body>
				</list-item>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="list-block" use-attribute-sets="list-block">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates />
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="xhtml:li">
		<xsl:element name="list-item" use-attribute-sets="list-item">
			<xsl:element name="list-item-label" use-attribute-sets="list-item-label">
				<xsl:element name="block">
					&#x2022; <!--todo : différentes puces selon le type de liste--></xsl:element>
			</xsl:element>
			<xsl:element name="list-item-body" use-attribute-sets="list-item-body">
				<xsl:element name="block">
					<xsl:call-template name="extra-attributes"/>
					<xsl:apply-templates />
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Les ancêtre à sortir -->
	<xsl:template match="xhtml:div[@class = 'pl-pgd-ancestors']" mode="output">
		<block xsl:use-attribute-sets="ancestors">
			<xsl:for-each select=".//xhtml:li">
				<xsl:value-of select="."/>
				<inline font-weight="bold">&#xA0;&gt; </inline>
			</xsl:for-each>
		</block>
	</xsl:template>

	<!-- Des éléments à passer sans sortir quoi que ce soit -->
	<xsl:template match="xhtml:a[@name and not(@href)]">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- Des contenus à supprimer -->
	<xsl:template match="xhtml:div[@id='pl-pg-body-tool-box']"/>
	<xsl:template match="xhtml:span[@style='display:none;']"/>
	<xsl:template match="xhtml:div[@class='pl-pgd-medias-content pl-pgd-medias-content-info']"/>
	<!-- Ne pas sortir le bouton de synchronisation dans le PDF -->
  <xsl:template match="xhtml:*[@class='pl-bttn-sync']"/>
  <xsl:template match="xhtml:*[@id='flashError']"/>

</xsl:stylesheet>
