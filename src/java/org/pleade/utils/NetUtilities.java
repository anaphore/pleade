/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.cocoon.environment.Context;
import org.apache.cocoon.environment.ObjectModelHelper;

/**
 * Methodes utilitaires liees au reseau, aux fichiers, etc.
 */
public class NetUtilities {

    /** L'encodage des URL */
    public final static String URL_ENCODING = "UTF-8";

    /**
     * Retourne une URL absolue depuis une base et une URL relative.
     * @param base  L'adresse de base (doit etre absolue).
     * @param rel   L'adresse relative.
     * @return      Une URL absolue, ou rel si jamais on ne peut fabriquer une
     *              URL absolue.
     */
    public static String resolveUrl(String base, String rel) {
        // Pas de base, on retourne l'URL relative
        if ( base == null ) return rel;
        try {
            // Une URL de base
            URL baseUrl = new URL(base);
            // On construit l'URL absolue
            URL relativeUrl = new URL(baseUrl, rel);
            // Et on la retourne
            return relativeUrl.toExternalForm();
        }
        catch (MalformedURLException e ) {}
        return rel;
    }

    /**
     * Indique si un fichier existe et peut etre lu.
     * @param path  Le chemin complet du fichier ou encore une URL de type file:/
     * @return      Vrai si le fichier peut etre lu, faux sinon.
     */
    public static boolean fileExists(String path) {
        // On se protege un peu
        if (path == null) return false;
        // On verifie d'abord s'il s'agit d'une URL
        try {
            URL u = new URL(path);
            if ( u.getProtocol().equals("file") ) {
                // C'est un fichier, on prend son chemin et on verifie s'il existe
                String p = u.getPath();
                try {
                    p = URLDecoder.decode(p, URL_ENCODING);
                }
                catch (UnsupportedEncodingException e) {}
                File f = new File(p);
                if ( f.exists() && f.canRead() ) return true;
            }
        }
        catch (MalformedURLException e) {
            // Ce n'est pas une URL, alors on considere que c'est un fichier
            File f = new File(path);
            if ( f.exists() && f.canRead() ) return true;
        }
        return false;
    }

    /**
     * Retourne une URL relative de base vers ref. Pas completement testee.
     * @param base  L'URL de depart
     * @param ref   L'URL d'arrivee
     * @return  Une URL relative
     */
    public static String relativize(String base, String ref) {
        // Si pas de depart, alors on retourne l'arrivee
        if (base == null) return ref;
        // Si pas d'arrivee, alors on retourne une URI vide
        if (ref == null) return "";
        // Maintenant on peut travailler
        try {
            URI depart = new URI(base);
            URI arrivee = new URI(ref);
            return depart.relativize(arrivee).normalize().toString();
        }
        catch (URISyntaxException e) {
            // Une des deux URI est mauvaise, donc on retourne ref
            return ref;
        }
    }
    
    /**
     * Retourne le type MIME d'une ressource, a condition d'avoir un objectModel Cocoon.
     * @return
     */
    public static String getMimeType(Map objectModel, String systemId) {
        if ( objectModel == null || systemId == null ) return null;
        // On utilise les outils Cocoon
        Context ctx = ObjectModelHelper.getContext(objectModel);
        if ( ctx == null ) return null;
        // Il semble que .TIFF ne soit pas reconnu...
        final String mimeType = ctx.getMimeType(systemId.toLowerCase());
        if (mimeType != null) {
            return mimeType;
        }
        return null;
    }
    
    /**
     * Retourne la partie "file" d'un URL.
     */
    public static String getFilePart(String url) {
        if ( url == null ) return null;
        if ( url.indexOf("/") < 0) return url;
        StringTokenizer st = new StringTokenizer(url, "/");
        String last = "";
        while (st.hasMoreTokens() ) {
            last = st.nextToken();
        }
        return last;
    }
    
    /**
     * Retourne une version encodee d'une URL absolue.
     * @param u L'URL a encoder
     * @param e L'encodage 
     * @return
     */
    public static String encodeUrl(String u, String e) {
        if ( u == null || e == null ) return null;
        // On scinde l'URL en parties, sur le ":" et le "/", et on demande
        // d'avoir les délimiteurs pour tout reconstruire proprement
        StringTokenizer st = new StringTokenizer(u, ":/", true);
        // Le buffer pour la sortie
        StringBuffer ret = new StringBuffer();
        while (st.hasMoreTokens()) {
            String t = st.nextToken();
            try {
                // Si c'est un séparateur, on le sort tel quel, sinon on encode la partie
                // A noter que sur une URL http://www.eglise.org, ça ne marchera pas terrible...
                if ( t.equals(":") || t.equals("/") ) ret.append(t);
                else ret.append(URLEncoder.encode(t, e));
            }
            catch (UnsupportedEncodingException ex) { ret.append(t); }
        }
        return ret.toString();
    }

}
