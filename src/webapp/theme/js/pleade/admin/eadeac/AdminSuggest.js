//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion des suggestions dans la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion des suggestions dans la partie admin
 * @author Julien Huet
 * @augments SuggestManager
 */
var AdminSuggest = new Class( /** @lends  AdminSuggest.prototype */ {
	Implements: [SuggestManager],
	
	/**
	 * <strong>Constructor</strong><br/>Initialisation des suggestions pour l'administration
	 */
	initialize: function(){},
	
	/**
	 * Fonction permettant d'initialiser les suggestions.
	 * @param {Array} fieldList
	 * @param {Array} indexGlobal
	 * @param {Array} indexLocal
	 */
	initSuggests: function(fieldList,indexGlobal, indexLocal ) {
		/* sélection des suggest : on prend le div/@class=suggest
				qui contient tout ; il a l'@id qui covient */
		this.suggests = $$('div.suggest');

		if ( this.suggests==null || this.suggests.length == 0 ) {
			return;
		}
		var _this = this;
		var _part; var mid;
		$A(this.suggests).clean().each(function(o, i) {
			mid = o.id;
			if ( mid==null || mid.blank() );
			else if ( mid.startsWith('inheritance-') ) {
				_part = fieldList;
			} else if ( mid == 'index-global' ) {
				_part = indexGlobal;
			} else if ( mid == 'index-local' ) {
				//alert('ndex-local');
				_part = indexLocal;
			}
			var _src = new YAHOO.widget.DS_JSArray( _part );
			var idi = mid + '-input',
					idc = mid + '-container',
					idv = mid + '-values';
			if ( $(idi)==null || $(idc)==null || $(idv)==null ) {
					this.suggests.splice(1,1);
			} else {
				var autoComp = new YAHOO.widget.AutoComplete(idi, idc, _src);
				autoComp.queryDelay = 0.1;
				autoComp.prehighlightClassName = 'yui-ac-prehighlight';
				autoComp.useShadow = true;
				autoComp.animHoriz = true;
				autoComp.allowBrowserAutocomplete = false;
				autoComp.doBeforeExpandContainer = function(oTextbox, oContainer, sQuery, aResults) {
					var pos = YAHOO.util.Dom.getXY(oTextbox);
					pos[1] += YAHOO.util.Dom.get(oTextbox).offsetHeight + 2;
					YAHOO.util.Dom.setXY(oContainer,pos);
					return true;
				};
				autoComp.itemSelectEvent.subscribe(function(event, obj) {
					var value = obj[2][0];	// l'objet est un resultat format JSON suivant le schema definit plus haut
					_this.addSuggest($(idv),value,value);
					$(idi).value = '';
				});
				this.isSuggests = true;
			}
		});
	}
});
