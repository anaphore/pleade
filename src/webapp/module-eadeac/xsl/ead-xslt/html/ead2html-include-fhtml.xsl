<?xml version="1.0" encoding="UTF-8"?>
	<!--$Id: get-search-form.xsl 17423 2010-01-07 09:54:12Z jcwiklinski $-->
	<!--
		Pleade: Outil de publication pour instruments de recherche, notices
		d'autorités et corpus d'images numérisés. Copyright (C) 2003-2010
		AJLSM, Anaphore AJLSM 17, rue Vital Carles 33000 Bordeaux, France
		info@ajlsm.com Anaphore SARL 3 ter chemin de la fontaine 13570
		Barbentane, France info@anaphore.eu This program is free software; you
		can redistribute it and/or modify it under the terms of the GNU
		General Public License as published by the Free Software Foundation;
		either version 2 of the License, or (at your option) any later
		version. This program is distributed in the hope that it will be
		useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
		General Public License for more details. You should have received a
		copy of the GNU General Public License along with this program; if
		not, write to the Free Software Foundation, Inc. 59 Temple Place -
		Suite 330, Boston, MA 02111-1307, USA or connect to:
		http://www.fsf.org/copyleft/gpl.html
	-->
	<!--
		+ | Récupère le nom du formulaire à afficher depuis les paramètres
		d'affichage +
	-->
<xsl:stylesheet version="2.0"

	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:xhtml="http://www.w3.org/1999/xhtml" 
	exclude-result-prefixes="xsl xi xhtml">

	<xsl:param name="eadid" select="''" />
	<xsl:param name="fragid" select="''" />
	<!-- liste des id des nœuds que l'on souhaite voir déplié (id séparé par des : ). Liste contenu dans le paramètre 'o' de l'URL -->
	<xsl:param name="nodes2show" select="''"/>
	
	<!-- Les propriétés d'affichage -->
	<xsl:variable name="fragmentid">
		<xsl:choose>
			<xsl:when test="$fragid = ''">
				<xsl:variable name="prop-url" select="concat('cocoon:/functions/ead/get-properties/', $eadid, '/display.xml')" />
				<xsl:variable name="display-properties" select="
						if(doc-available($prop-url)) then document($prop-url)/properties
						else ''
					"/>
				<xsl:value-of select="$display-properties/property[@name='archdesc-id']"/>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$fragid"/>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:variable>	
	 
	 <xsl:template match="/">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template name="toc-match" match="xhtml:div[@id = 'pl-pgd-toc']">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xi:include href="cocoon://functions/ead/get-toc-fragment/{$eadid}/{$eadid}.full-html"/>
		</xsl:copy>
		<!-- http://localhost:8080/pleade-libre-trunk/ead-fragment.xsp?c=frajlsm-pleade-test-01-fr_e0000012 -->
		
	</xsl:template>
	
	<xsl:template name="fragment-match" match="xhtml:div[@id = 'pl-pg-body-main-mcol']">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			
			<xi:include href="cocoon://{$fragmentid}.fragment"/>
		</xsl:copy>
	</xsl:template>
	

	<!-- On copie tout -->
	<xsl:template match="node()|@*">
		<xsl:choose>
			<xsl:when test="namespace-uri() != ''">
				<xsl:copy>
					<xsl:apply-templates select="node()|@*" />
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy copy-namespaces="no">
					<xsl:apply-templates select="node()|@*" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
