<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
>

	<!-- XSLT qui implémente les différentes méthodes pour retrouver un document dans
		 un cadre de classement. -->

	<!-- Méthode générale appelée -->
	<xsl:template name="get-cdc-entry-id">
		<xsl:param name="cdc"/>		<!-- Pointe sur l'élément racine du cadre de classement -->
		<xsl:param name="eadid"/>	<!-- L'identifiant du document en cours d'indexation -->
		<xsl:param name="url"/>		<!-- L'URL du document en cours d'indexation -->
		<xsl:param name="doc"/>		<!-- Le document EAD en cours de publication -->
		<xsl:variable name="method-01">
			<!-- Des liens explicites dans le cadre de classement -->
			<xsl:call-template name="get-cdc-entry-id-by-archref">
				<xsl:with-param name="cdc" select="$cdc"/>
				<xsl:with-param name="eadid" select="$eadid"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$method-01 != ''">
				<xsl:value-of select="$method-01"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="method-02">
					<!-- Le nom du dossier où se trouven le document -->
					<xsl:call-template name="get-cdc-entry-by-url">
						<xsl:with-param name="cdc" select="$cdc"/>
						<xsl:with-param name="url" select="$url"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="normalize-space($method-02) != ''">
						<xsl:value-of select="$method-02"/>
					</xsl:when>
					<xsl:otherwise>
						<!-- On retourne la racine -->
						<xsl:value-of select="$cdc/archdesc/@pleade:id"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Recherche une entrée de cadre de classement avec un lien archref
			pointant sur le eadid en cours -->
	<xsl:template name="get-cdc-entry-id-by-archref">
		<xsl:param name="cdc"/>
		<xsl:param name="eadid"/>
		<xsl:variable name="archref" select="$cdc//archref[@href = concat($eadid, '.xml')][1]"/>
		<xsl:value-of select="
			if ($archref) then $archref[1]/ancestor::*[@pleade:component='true'][1]/@pleade:id
			else ''
		" />
	</xsl:template>

	<!-- Recherche une entrée de cadre de classement en fonction du nom du
			dossier où se trouve le document -->
	<xsl:template name="get-cdc-entry-by-url">
		<xsl:param name="cdc"/>
		<xsl:param name="url"/>

		<!-- On trouve d'abord l'identifiant en fonction du dossier où se trouve le fichier -->
		<xsl:variable name="code" select="fn:tokenize($url, '/')[last() - 1]"/>

		<!-- L'identifiant du document EAD du cadre de classement -->
		<xsl:variable name="cdcid" select="$cdc/@pleade:id"/>

		<!-- On concatène les deux identifiants pour avoir l'entrée de CDC -->
		<xsl:variable name="cdc-entry-id" select="concat($cdcid, '_', $code)"/>

		<!-- On retourne l'identifiant s'il existe -->
		<xsl:if test="$cdc//*[@pleade:component = 'true' and @pleade:id = $cdc-entry-id]"><xsl:value-of select="$cdc-entry-id"/></xsl:if>

	</xsl:template>

</xsl:stylesheet>
