<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" exclude-result-prefixes="xsl sdx">
    <xsl:template match="/*">
        <xsl:choose>
            <xsl:when test="sdx:results">
                <xsl:apply-templates select="sdx:results[1]" mode="copy"/>
            </xsl:when>
            <xsl:when test="sdx:terms">
                <xsl:apply-templates select="sdx:terms[1]" mode="copy"/>
            </xsl:when>
            <xsl:when test="sdx:exception">
                <xsl:apply-templates select="sdx:exception[1]" mode="copy"/>
            </xsl:when>
            <xsl:when test="sdx:message">
                <xsl:apply-templates select="sdx:message[1]" mode="copy"/>
            </xsl:when>
            <!-- should be correct for include document -->
            <xsl:otherwise>
                <xsl:apply-templates select="*[1]" mode="copy"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="@*|*|text()|processing-instruction()" priority="-1" mode="copy">
        <xsl:copy>
            <xsl:apply-templates select="@*|*|text()|processing-instruction()" mode="copy"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
