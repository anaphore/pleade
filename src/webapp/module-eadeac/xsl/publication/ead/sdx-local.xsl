<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
>
	<!-- XSLT qui complète sdx.xsl et qui permet de plus facilement créer des champs
			locaux -->
			
	<!-- Ce template est appelé sur chaque élément d'un composant
			contexte = un c et son contenu (variable $content, pas de composant ancêtre ou enfant) -->
	<xsl:template name="local-content-fields">
		<xsl:param name="ComponentHasAncestor"/>
		<xsl:param name="ComponentHasSubcomponent"/>
	</xsl:template>

	<!-- Ce template est appelé sur chaque fragment
			contexte = contexte: c, archdesc, ... mais avec autour tout le document EAD -->
	<xsl:template name="local-process-fragment"/>

</xsl:stylesheet>
