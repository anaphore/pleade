<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT qui affiche la page avec le cadre de classement. Le cadre
		de classement peut être complet ou partiel, selon le paramètre id.
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		exclude-result-prefixes="xsl i18n">


	<!-- Le paramètre qui nous indique quelle partie du cadre de classement utiliser. -->
	<xsl:param name="id" select="''"/>
	
	<!-- Des indications par défaut pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand-default" select="''"/>

	<!-- La base de la clé i18n -->
	<xsl:variable name="i18n-base">
		<xsl:value-of select="'cdc'"/>
		<xsl:if test="$id != ''"><xsl:value-of select="concat('.', $id)"/></xsl:if>
	</xsl:variable>

	<xsl:template match="/root">

		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:i18n="http://apache.org/cocoon/i18n/2.1">
			<head>
				<title>
					<i18n:text key="{$i18n-base}.title">cadre de classement</i18n:text>
				</title>
				<i18n:text key="cdc.meta.tags"><meta name="copyright" content="Copyright AJLSM - Anaphore"/></i18n:text>
				<!-- Les .js nécessaires -->
				<script type="text/javascript" src="i18n/module-eadeac-js.js">//</script>
				<!-- Les CSS nécessaires -->
				<link type="text/css" href="css/module-ead-xslt.css" rel="stylesheet" />
				<link type="text/css" href="css/local/module-ead-xslt.css" rel="stylesheet" />
				<!-- Lorsque la page est prête, on déclenche le chargement du CDC -->
				<script type="text/javascript">
					var cdcPartId = '<xsl:value-of select="$id"/>';
					var ae = '<xsl:value-of select="$auto-expand-default"/>';
					var cdcTreeView = "";
					function initCdc(){
						cdcTreeView = new CdcTreeView(ae);
						cdcTreeView.load();
					}
					window.addEvent("load", initCdc);
				</script>
			</head>
			<body class="yui-skin-pleade">
				<!-- Un titre pour le contenu de la page -->
				<h1 class="pl-title">
					<i18n:text key="{$i18n-base}.header">cadre de classement</i18n:text>
				</h1>
				<!-- Une introduction -->
				<i18n:text key="{$i18n-base}.intro"/>
				<!-- Le plan de classement lui-même, qui sera chargé en AJAX -->
				<div id="_cdc_"><xsl:comment>u</xsl:comment></div>
				<!-- Une conclusion -->
				<i18n:text key="{$i18n-base}.conclusion"/>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
