<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="xsl saxon"
>

	<!-- Ajoute des informations sur le document, en particulier des attributs dans le
		namespace pleade. -->
		
	<xsl:param name="publication" select="'true'"/>
	<xsl:param name="debug" select="'false'"/>

	<!-- On conserve l'identifiant du document car on le réutilise souvent. -->
	<xsl:variable name="eadid">
		<xsl:call-template name="get-eadid">
			<xsl:with-param name="eadid" select="/ead/eadheader/eadid"/>
		</xsl:call-template>
	</xsl:variable>

	<!-- Une fonction pour dire si c'est un composant -->
	<xsl:function name="pleade:is-component" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:choose><!-- TODO (MP) : Pourquoi ne pas utiliser la méthode statique org.pleade.utils.EADUtilities.elementIsComponent(String localName) ?  -->
			<xsl:when test="$el/self::archdesc or $el/self::c or $el/self::c01 or $el/self::c02 or $el/self::c03 or $el/self::c04 or $el/self::c05 or $el/self::c06 or $el/self::c07 or $el/self::c08 or $el/self::c09 or $el/self::c10 or $el/self::c11 or $el/self::c12"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="$publication='true'">
				<xsl:message>	Traitement des attributs Pleade (etape 4 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
				<xsl:choose>
					<xsl:when test="$debug='true'">
						<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
						<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{$eadid}/{$eadid}-4.xml"
							xmlns:sys="java:java.lang.System">
							<xsl:copy-of saxon:read-once="yes" select="$result"/>
						</xsl:result-document>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
		</xsl:template>

	<!-- On intercepte tous les éléments pour ajouter leur identifiant et éventuellement d'autres informations. -->
	<xsl:template match="*">
		<xsl:copy>
			<!-- Les attributs déjà présents -->
			<xsl:apply-templates select="@*"/>
			<!-- L'identifiant -->
			<xsl:attribute name="pleade:id">
				<xsl:call-template name="get-id"/>
			</xsl:attribute>
			<!-- Les composants -->
			<xsl:if test="pleade:is-component(.)">
				<!-- On l'indique -->
				<xsl:attribute name="pleade:component">true</xsl:attribute>
			</xsl:if>
			<!-- Le cas particulier de l'élément racine -->
			<xsl:if test="self::ead">
				<xsl:attribute name="pleade:eadid">
					<xsl:value-of select="$eadid"/>
				</xsl:attribute>
				<xsl:attribute name="pleade:titleproper">
					<xsl:call-template name="get-titleproper"/>
				</xsl:attribute>
				<xsl:attribute name="pleade:subtitle">
					<xsl:call-template name="get-subtitle"/>
				</xsl:attribute>
				<xsl:attribute name="pleade:author">
					<xsl:call-template name="get-author"/>
				</xsl:attribute>
				<xsl:attribute name="pleade:sponsor">
					<xsl:call-template name="get-sponsor"/>
				</xsl:attribute>
				<xsl:attribute name="pleade:archdesc-id">
					<xsl:call-template name="get-id">
						<xsl:with-param name="element" select="archdesc"/>
					</xsl:call-template>
				</xsl:attribute>
        <xsl:attribute name="pleade:eadheader-id">
          <xsl:call-template name="get-id">
            <xsl:with-param name="element" select="eadheader"/>
          </xsl:call-template>
        </xsl:attribute>
			</xsl:if>
			<!-- Ensuite le contenu -->
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- On supprime les commentaires -->
	<xsl:template match="comment()"/>

	<!-- Les autres contenus (texte, instructions de traitement, attributs) sont copiés -->
	<xsl:template match="text()|processing-instruction()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- Retourne l'identifiant unique d'un élément -->
	<xsl:template name="get-id">

		<!-- L'élément en question, par défaut l'élément courant -->
		<xsl:param name="element" select="."/>

		<xsl:choose>
			<xsl:when test="$element/self::ead">
				<!-- Pour le document EAD, on a déjà calculé l'information -->
				<xsl:value-of select="$eadid"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- D'abord l'identifiant du document -->
				<xsl:value-of select="concat($eadid, '_')"/>
				<!-- Puis celui du document -->
				<xsl:choose>
					<xsl:when test="$element/@id"><xsl:value-of select="$element/@id"/></xsl:when>
					<xsl:when test="$element/@pleade:no">e<xsl:value-of select="format-number($element/@pleade:no, '0000000')"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="generate-id($element)"/></xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Retourne le eadid, en utilisant notamment ses attributs. -->
	<xsl:template name="get-eadid">
		<xsl:param name="eadid" select="."/>
		<xsl:variable name="value" select="normalize-space($eadid)"/>
		<!-- On commente le code de la méthode avec attributs, mais elle existe
				si nécessaire. -->
		<!-- <xsl:variable name="sep" select="'-'"/>
		<xsl:if test="$eadid/@countrycode"><xsl:value-of select="concat($eadid/@countrycode, $sep)"/></xsl:if>
		<xsl:if test="$eadid/@mainagencycode"><xsl:value-of select="concat($eadid/@mainagencycode, $sep)"/></xsl:if>
		<xsl:if test="$eadid/@identifier">
			<xsl:value-of select="$eadid/@identifier"/>
			<xsl:if test="$value != ''"><xsl:value-of select="$sep"/></xsl:if>
		</xsl:if> -->
		<xsl:value-of select="$value"/>
	</xsl:template>

	<!-- Retourne le titre propre de l'instrument de recherche. -->
	<xsl:template name="get-titleproper">
		<xsl:param name="ead" select="."/>
		<xsl:for-each select="$ead/eadheader/filedesc/titlestmt/titleproper">
			<xsl:if test="position() > 1"><xsl:text> – </xsl:text></xsl:if>
			<!-- <xsl:value-of select="normalize-space(.)"/> -->
			<xsl:apply-templates mode="titleproper"/>
		</xsl:for-each>
	</xsl:template>
	<!-- On remplace les sauts de ligne par un tiret cadratin pour les fatitle et pleade:titleproper -->
	<xsl:template match="lb" mode="titleproper">
		<xsl:text> &#8212; </xsl:text>
	</xsl:template>

	<!-- Retourne le sous-titre de l'instrument de recherche -->
	<xsl:template name="get-subtitle">
		<xsl:param name="ead" select="."/>
		<xsl:for-each select="$ead/eadheader/filedesc/titlestmt/subtitle">
			<xsl:if test="position() > 1"><xsl:text> – </xsl:text></xsl:if>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:for-each>
	</xsl:template>

	<!-- Retourne l'auteur de l'instrument de recherche -->
	<xsl:template name="get-author">
		<xsl:param name="ead" select="."/>
		<xsl:value-of select="normalize-space($ead/eadheader/filedesc/titlestmt/author)"/>
	</xsl:template>

	<!-- Retourne le commanditaire de l'instrument de recherche -->
	<xsl:template name="get-sponsor">
		<xsl:param name="ead" select="."/>
		<xsl:value-of select="normalize-space($ead/eadheader/filedesc/titlestmt/sponsor)"/>
	</xsl:template>
</xsl:stylesheet>


