<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Global variables -->
<xsl:stylesheet version="1.0"
								xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
								xmlns:pleade="http://pleade.org/ns/pleade/1.0"
								xmlns:local-conf="http://pleade.org/ns/local-conf/1.0"
								xmlns:c-pleade="http://pleade.org/ns/c-pleade/1.0"
								xmlns:oai-conf="http://pleade.org/ns/oai-conf/1.0"
								xmlns:dir="http://apache.org/cocoon/directory/2.0"
								exclude-result-prefixes="xsl sdx pleade local-conf c-pleade oai-conf dir">

	<!-- FIXME: check to see if everything is still useful here -->

	<!-- A pointer to the SDX document -->
	<xsl:variable name="g-sdx-doc" select="/sdx:document"/>

	<!--current interface lang -->
	<xsl:variable name="g-lang" select="substring(/sdx:document/@xml:lang, 1, 2)"/>

	<!-- common interface ui-strings -->
	<xsl:variable name="g-ui-strings" select="/sdx:document/c-pleade:config/c-pleade:ui-strings"/>

	<!--url index-->
	<xsl:variable name="g-url-index" select="/sdx:document/c-pleade:config/c-pleade:param[@name='url-index']"/>

	<!--personnal ui-strings-->
	<xsl:variable name="g-my-ui-strings" select="/sdx:document/c-pleade:config/c-pleade:my-ui-strings"/>

	<!-- Local configuration file -->
	<xsl:variable name="g-local-config" select="document('../../module-eadeac/conf/local.xconf')/local-conf:configuration"/>
	<!--<xsl:variable name="g-local-oai-config" select="document('../../module-eadeac/conf/oai.xconf')/oai-conf:oai-informations"/>-->
	<!-- labels file -->
	<xsl:variable name="g-labels" select="document('../../theme/msg/labels.xml')/labels"/>
	<!--formats-->
	<!--<xsl:variable name="g-formats" select="document('../../../@pleade.webapp.local.subfolder@/ead-display-formats/ead-display-formats.xml')/ead-display-formats"/>-->

	<!--forms lists-->
	<xsl:variable name="the_list" select="document(concat(/sdx:document/@server,'/',/sdx:document/@appbypath,'/','/form-list'))/dir:directory"/>

	<!--<xsl:variable name="formulaires">
		<xsl:variable name="my-lang" select="$g-lang"/>
		<xsl:for-each select="$the_list/dir:file">
			<xsl:variable name="the_form" select="document(concat('../../../@pleade.webapp.local.subfolder@/search-forms/',@name))/form"/>
			<name>
				<xsl:attribute name="n">
					<xsl:value-of select="$the_form/@name"/>
				</xsl:attribute>
				<xsl:attribute name="general">
					<xsl:value-of select="$the_form/@general"/>
				</xsl:attribute>
				<xsl:attribute name="default">
					<xsl:value-of select="$the_form/@default"/>
				</xsl:attribute>
				<xsl:attribute name="documents">
					<xsl:value-of select="$the_form/@documents"/>
				</xsl:attribute>
				<xsl:value-of select="$the_form/short-title[lang($my-lang)]"/>
			</name>
		</xsl:for-each>

	</xsl:variable>-->

</xsl:stylesheet>
