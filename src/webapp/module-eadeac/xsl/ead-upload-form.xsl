<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Gestion du contenu. Formulaire de chargement de documents
		|
		| TODO (MP) : deprecated ! A supprimer...
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:xsp-session="http://apache.org/xsp/session/2.0"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml xsp-session dir">

	<xsl:variable name="i18n-root" select="'edit.ead-upload.form'" />

	<!-- Imports -->
	<xsl:include href="form2xhtml-commons.xsl"/>

	<!--	Quelques éléments nécessaires à la construction du formulaire :
				dirs: les où l'on peut placer des documents
	-->
	<xsl:variable name="dirs" select="/aggregate/dir:directory | /aggregate/directory" />

	<!--+
	    | Matche la racine et envoie le traitement du template XHTML
	    +-->
	<xsl:template match="/">
		<xsl:apply-templates select="/aggregate/template/xhtml:html | /aggregate/template/html" mode="template" />
	</xsl:template>

  <!--+
      | Construction du titre
      | On récupère le titre et on ajoute un bouton d'aide
      +-->
  <xsl:template match="xhtml:h1[@class='pl-title']" mode="template">
    <div class="pl-title">
      <xsl:call-template name="build-help">
        <xsl:with-param name="v" select="'title'" />
        <xsl:with-param name="k" select="concat($i18n-root, '.title')" />
      </xsl:call-template>
      <h1>
        <xsl:apply-templates select="@*[not(local-name()='class')]" mode="template" />
        <xsl:attribute name="class">
          <xsl:value-of select="replace( @class, 'pl-title','' )" />
        </xsl:attribute>
        <xsl:apply-templates select="node()" mode="template" />
      </h1>
    </div>
  </xsl:template>

	<!--+
			|	Onglet "Documents",
			|	Contrôle "Sélectionner un fichier ou un dossier à publier"
			+-->
	<xsl:template match="xhtml:div[@id='docs' and parent::*/@id='ead-publish-box']
												| div[@id='docs' and parent::*/@id='ead-publish-box']"
			mode="template">

		<div>

			<xsl:apply-templates select="@*" mode="template" />

      <div class="pl-form-message pl-form-intro">
        <i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.tabs.docs.intro">
          <xsl:comment>u</xsl:comment>
        </i18n:text>
      </div>

			<xsl:apply-templates select="$props/*[@name='docUrl' and .!='']" mode="docUrl" />

			<xsl:apply-templates select="$docs[@requested='true']" mode="main" />

      <!-- Envoyer un fichier ou une archive ZIP -->
      <xsl:call-template name="build-help">
        <xsl:with-param name="v" select="'docs-zip'" />
        <xsl:with-param name="k" select="concat($i18n-root, '.docs-zip')" />
      </xsl:call-template>
      <fieldset id="fieldset_docs-zip">
        <legend>
          <i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.docs-zip.legend">
            envoyer un fichier à publier
          </i18n:text>
        </legend>
        <input id="docs-zip" class="pl-form-npt-file" size="40" name="zipz" type="file" onchange="javascript: publicationForm.valideLfc(this);" />
      </fieldset>

		</div>

	</xsl:template>

	<!--+
			|	Contruire la zone de texte indiquant l'URL du document à re-publier
			+-->
	<xsl:template match="*" mode="docUrl">

		<!-- On n'envoie pas file:/... mais /... -->
		<xsl:variable name="o" select="normalize-space(.)" />
		<!-- On supprime le protocol car la taglib de SDX 2.4 n'aime pas (encore !)
		les URL pour construire ses sdx:document. Ca viendra ! [MP] -->
		<xsl:variable name="v">
			<xsl:choose>
				<xsl:when test="starts-with($o, 'file:')">
					<xsl:value-of select="substring-after($o, 'file:')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$o" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<fieldset>
			<legend>
				<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.docurl.legend">url du document à re-publier</i18n:text>
			</legend>
			<input id="docurl" readonly="true" type="text" value="{$v}" class="pl-form-txt" />
		</fieldset>
	</xsl:template>

	<!--+
			|	Contruire la liste des documents disponibles sur le serveur
			+-->
	<xsl:template match="dir:directory[@requested='true'] | directory[@requested='true']" mode="main">
    <!-- Construction des outils d'aide -->
    <xsl:call-template name="build-help">
      <xsl:with-param name="v" select="'docs-list'" />
      <xsl:with-param name="k" select="concat($i18n-root, '.docs-list')" />
    </xsl:call-template>
		<fieldset>
			<legend>
				<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.docs-list.legend">
					<xsl:text>sélectionner un fichier ou un dossier à publier</xsl:text>
				</i18n:text>
			</legend>
			<select id="docs-select" name="dird" class="pl-form-slct"
							onchange="javascript: publicationForm.valideLfc(this);">
				<option>
					<xsl:if test="not(dir:directory or directory or dir:file or file)">
						<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.docs-list.empty">
							<xsl:text>aucun document à publier n'est disponible</xsl:text>
						</i18n:text>
					</xsl:if>
				</option>
				<option class="pl-ead-publish-dir" value="{@absolutePath}">
					<xsl:value-of select="normalize-space(@name)" />
				</option>
				<xsl:apply-templates select="*" mode="docs-selects-option">
					<xsl:with-param name="n" select="1" />
				</xsl:apply-templates>
			</select>
		</fieldset>
	</xsl:template>
	<xsl:template match="dir:directory | directory" mode="docs-selects-option">

		<xsl:param name="n" select="0" />
		<xsl:param name="p" />

		<xsl:variable name="m" select="normalize-space(@name)" />

		<option class="pl-ead-publish-dir" value="{@absolutePath}">
			<xsl:call-template name="indent">
				<xsl:with-param name="n" select="$n" />
			</xsl:call-template>
			<xsl:value-of select="$m" />
		</option>

		<xsl:apply-templates select="*" mode="docs-selects-option">
			<xsl:with-param name="n" select="$n + 1" />
		</xsl:apply-templates>

	</xsl:template>
	<xsl:template match="dir:file | file" mode="docs-selects-option">

		<xsl:param name="n" select="0" />

		<xsl:variable name="m" select="normalize-space(@name)" />

		<option class="pl-ead-publish-file" value="{@absolutePath}">
			<xsl:call-template name="indent">
				<xsl:with-param name="n" select="$n" />
			</xsl:call-template>
			<xsl:value-of select="$m" />
		</option>

	</xsl:template>
	<!-- Fonction d'indentation pour la liste des fichiers
				On pourrait utiliser la propriété CSS text-indent, mais elle ne
				fonctionne pas du tout sur IE 6. -->
	<xsl:template name="indent">
		<xsl:param name="n" select="1" />
		<xsl:param name="c" select="0" />
		<xsl:param name="sep" select="'- '" />

		<xsl:if test="$c &lt; $n">
			<xsl:value-of select="$sep" />
			<xsl:call-template name="indent">
				<xsl:with-param name="c" select="$c + 1" />
				<xsl:with-param name="n" select="$n" />
			</xsl:call-template>
		</xsl:if>

	</xsl:template>

	<!--+
			|	Onglet "Paramètres d'affichage",
			+-->
	<xsl:template match="xhtml:div[@id='params-dis' and parent::*/@id='ead-publish-box']
											| div[@id='params-dis' and parent::*/@id='ead-publish-box']" mode="template">

		<div>
			<xsl:apply-templates select="@*" mode="template" />

      <div class="pl-form-message">
        <i18n:text catalogue="module-eadeac-edit"
                    key="edit.ead-publish.form.tabs.parameters-display.intro">
          <!--u-->
        </i18n:text>
      </div>

			<xsl:variable name="props-dis" select="$props[@id='display']/*[not(@read-only='true')]" />
			<xsl:variable name="search-forms" select="/aggregate/search-forms/dir:directory | /aggregate/search-forms/directory" />

			<xsl:choose>

				<xsl:when test="count($props-dis) &gt; 0">

					<xsl:for-each select="$props-dis">

						<xsl:variable name="n" select="@name" />
						<xsl:variable name="d">
							<xsl:choose>
								<xsl:when test="normalize-space(.)!=''">
									<xsl:value-of select="normalize-space(.)" />
								</xsl:when>
								<xsl:when test="@default!=''">
									<xsl:value-of select="@default" />
								</xsl:when>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="p" select="position()" />

						<!-- Construction des outils d'aide -->
						<xsl:call-template name="build-help">
						  <xsl:with-param name="v" select="$n" />
              <xsl:with-param name="k" select="concat($i18n-root, '.field.', $n)" />
						</xsl:call-template>

						<fieldset id="fieldset_{$n}">

							<legend>
								<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.field.{$n}.legend">
									<xsl:value-of select="$n" />
								</i18n:text>
							</legend>

							<table border="0" cellspacing="0" cellpadding="0" summary="{$n}">
								<tbody>

									<xsl:choose>

										<!-- Paramètre "search-form" -->
										<xsl:when test="$n='search-form'">
											<tr>
												<td class="pl-form-label"> </td>
												<td>
													<select name="{$n}" tabindex="{$p}" class="pl-form-slct">
														<option>
															<xsl:if test="not($search-forms/*)">
																<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.field.search-form.empty">
																	<xsl:text>pas de formulaire disponible</xsl:text>
																</i18n:text>
															</xsl:if>
														</option>
														<xsl:for-each select="$search-forms/*[self::dir:file or self::file]">
															<xsl:variable name="nm" select="@name"/>
															<xsl:variable name="v"
															select="substring-before($nm, '.xconf')"/>
<!--<xsl:message>
nm=<xsl:value-of select="$nm" />
v=<xsl:value-of select="$v" />
d=<xsl:value-of select="$d" />
</xsl:message>-->
															<option value="{$v}">
																<xsl:if test="$v=$d">
																	<xsl:attribute name="selected">
																		<xsl:text>selected</xsl:text>
																	</xsl:attribute>
																</xsl:if>
																<xsl:value-of select="$v" />
															</option>
														</xsl:for-each>
													</select>
												</td>
											</tr>
										</xsl:when>

										<!-- Paramètre "display-navivation" -->
										<xsl:when test="$n='display-navigation'">
											<xsl:variable name="vs" select="'true,false'" />
											<xsl:for-each select="tokenize($vs, ',')">
												<xsl:variable name="v" select="." />
												<xsl:if test="$v!=''">
													<tr>
														<td class="pl-form-label">
															<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.field.{$n}.label.{$v}">
																<xsl:value-of select="$v" />
															</i18n:text>
														</td>
														<td>
															<input type="radio" name="{$n}"
																		id="{$n}{$v}" class="pl-form-npt-rd"
																		value="{$v}"  tabindex="{$p}">
																<xsl:if test="$d=$v">
																	<xsl:attribute name="checked">
																		<xsl:text>true</xsl:text>
																	</xsl:attribute>
																</xsl:if>
															</input>
														</td>
													</tr>
												</xsl:if>
											</xsl:for-each>
										</xsl:when>

										<!-- Paramètre "index-local -->
										<xsl:when test="$n='index-local'">
											<tr>
												<td class="pl-form-label"> </td>
												<td>
													<div class="suggest" id="{$n}">
														<input type="text" name="{$n}" tabindex="{$p}"
																id="{$n}-input" class="pl-form-txt pl-suggest" />
														<div id="{$n}-container">
															<xsl:comment>u</xsl:comment>
														</div>
													</div>
													<div class="pl-form-suggest-values"
																id="{$n}-values">
														<xsl:call-template name="build-div-suggest-values">
															<xsl:with-param name="i" select="$n" />
															<xsl:with-param name="d" select="$d" />
															<xsl:with-param name="sep" select="$sep" />
														</xsl:call-template>
														<xsl:comment>u</xsl:comment>
													</div>
												</td>
											</tr>
										</xsl:when>

										<xsl:otherwise>
											<tr>
												<td class="pl-form-label">
													<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.field.{$n}.label">
														<xsl:text> </xsl:text>
													</i18n:text>
												</td>
												<td>
													<input type="text" name="{$n}" tabindex="{$p}" id="{$n}"
																class="pl-form-txt" value="{$d}" />
												</td>
											</tr>
										</xsl:otherwise>

									</xsl:choose>

								</tbody>
							</table>

						</fieldset>

					</xsl:for-each>

				</xsl:when>

				<xsl:otherwise>
					<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.form.tabs.parameters-display.no-params">
						<p>Il n'existe pas de paramètre d'affichage modifiable.</p>
					</i18n:text>
				</xsl:otherwise>

			</xsl:choose>

		</div>

	</xsl:template>

	<!--+
			|	Onglet "Paramètres de publication",
			+-->
	<!-- Les listes de méthodes : fragmentation, titre, table des matières -->
	<xsl:template match="xhtml:select[@name!=''] | select[@name!='']" mode="template">

		<!-- Quelle est la valeur par défaut -->
		<xsl:variable name="de" select="$props/*[@name=current()/@name]" />
		<xsl:variable name="d">
			<xsl:choose>
				<xsl:when test="normalize-space($de)!=''">
					<xsl:value-of select="normalize-space($de)" />
				</xsl:when>
				<xsl:when test="$de/@default!=''">
					<xsl:value-of select="$de/@default" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<select>
			<xsl:apply-templates select="@* |node()" mode="template"/>
			<xsl:call-template name="build-select-options">
				<xsl:with-param name="n" select="@name" />
				<xsl:with-param name="ls" select="$lists[@name=current()/@name]" />
				<xsl:with-param name="d" select="$d" />
			</xsl:call-template>
		</select>
	</xsl:template>

	<!--+
			|	Traiter les input du formulaire
			+-->
	<xsl:template match="xhtml:input | input" mode="template">

		<xsl:variable name="i">
			<xsl:choose>
				<xsl:when test="starts-with(@id, 'inheritance-index-')">
					<xsl:text>inheritance-index</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@name" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Quelle est la valeur par défaut -->
		<xsl:variable name="de" select="$props/*[@name=$i]" />
		<xsl:variable name="d">
			<xsl:choose>
				<xsl:when test="normalize-space($de)!=''">
					<xsl:value-of select="normalize-space($de)" />
				</xsl:when>
				<xsl:when test="$de/@default!=''">
					<xsl:value-of select="$de/@default" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<input>
			<xsl:apply-templates select="@*" mode="template" />
			<xsl:if test="not(contains(@class, 'pl-suggest'))
										and ( @name!='' or starts-with(@id, 'inheritance-index-') )">
				<xsl:call-template name="input-default-value">
					<xsl:with-param name="i" select="$i" />
					<xsl:with-param name="d" select="$d" />
					<xsl:with-param name="sep" select="$sep" />
				</xsl:call-template>
			</xsl:if>
			<xsl:apply-templates select="node()" mode="template" />
		</input>

	</xsl:template>

	<!--+
	    | Traiter les conteneurs des suggest du formulaire
	    +-->
	<xsl:template match="xhtml:div[contains(@class, 'pl-form-suggest-values')]
												| div[contains(@class, 'pl-form-suggest-values')]"
								mode="template">

		<xsl:variable name="i" select="substring-before(@id, '-values')" />
		<!-- Quelle est la valeur par défaut -->
		<xsl:variable name="de" select="$props/*[@name=$i]" />
		<xsl:variable name="d">
			<xsl:choose>
				<xsl:when test="normalize-space($de)!=''">
					<xsl:value-of select="normalize-space($de)" />
				</xsl:when>
				<xsl:when test="$de/@default!=''">
					<xsl:value-of select="$de/@default" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<div>
			<xsl:apply-templates select="@*" mode="template" />
			<xsl:call-template name="build-div-suggest-values">
				<xsl:with-param name="i" select="$i" />
				<xsl:with-param name="d" select="$d" />
				<xsl:with-param name="sep" select="$sep" />
			</xsl:call-template>
			<xsl:apply-templates select="node()" mode="template"  />
		</div>

	</xsl:template>

	<!--+
			|	Traitement des fieldsets
			+-->
	<xsl:template match="xhtml:fieldset[starts-with(@id, 'fieldset_')]
												| fieldset[starts-with(@id, 'fieldset_')]" mode="template">

		<xsl:variable name="v" select="substring-after( @id, 'fieldset_' )" />

		<xsl:call-template name="build-help">
		  <xsl:with-param name="v" select="$v" />
      <xsl:with-param name="k" select="concat($i18n-root, '.field.', $v)" />
		</xsl:call-template>

		<!-- On recopie le fieldset maintenant -->
		<fieldset>
			<xsl:apply-templates select="@* | node()" mode="template" />
		</fieldset>

	</xsl:template>

	<!-- Balises i18n -->
	<xsl:template match="i18n:*|@i18n:*" mode="template">
		<xsl:apply-templates select="." />
	</xsl:template>
	<xsl:template match="i18n:*|@i18n:*" mode="main">
		<xsl:apply-templates select="." />
	</xsl:template>
	<xsl:template match="i18n:*">
		<xsl:element name="{name()}">
			<xsl:apply-templates select="@*|node()" />
		</xsl:element>
	</xsl:template>
	<xsl:template match="@i18n:*|i18n:*/@*">
		<xsl:attribute name="{name()}">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>

	<!-- Copie générique -->
	<xsl:template match="*" priority="-1" mode="template">
		<xsl:element name="{name()}">
			<xsl:apply-templates select="@*|node()" mode="template" />
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*" priority="-1" mode="template">
		<xsl:attribute name="{name()}">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
	<xsl:template match="*" priority="-1" mode="main">
		<xsl:element name="{name()}">
			<xsl:apply-templates select="@*|node()" mode="main" />
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*" priority="-1" mode="main">
		<xsl:attribute name="{name()}">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>

</xsl:stylesheet>
