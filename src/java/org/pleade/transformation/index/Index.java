/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation.index;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Collator;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Locale;
import java.util.TreeMap;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;
import org.apache.avalon.framework.logger.AbstractLogEnabled;
import org.apache.avalon.framework.logger.LogEnabled;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.logger.NullLogger;
import org.apache.cocoon.serialization.XMLSerializer;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;


/**
 * An index of terms.
 *
 * An index is defined by an element name, and may be by one or two more
 * parameters: the role and the source. This models follows EAD
 */
public class Index extends AbstractLogEnabled implements LogEnabled {
    /** L'identifiant de l'index */
    private String id;
	// TODO: correct collator...
	private TreeMap terms = new TreeMap(Collator.getInstance(Locale.FRENCH));
	// TODO: parameter!
	private int nbTermsForGrouping = 20;

    /**
     * Creation d'un index a partir de son identifiant
     * @param id    L'identifiant de l'index
     */
    public Index(String id) {
        this.id = id;
    }

	/**
	 * Adds a value to this index
	 *
	 * @param value		La valeur
	 * @param id		The component id
	 */
	public void addValue(String value, String id) {

	    // Check if it exists
	    Term currentTerm = (Term)terms.get(value);
	    if ( currentTerm == null ) {
	        Term newTerm = new Term(value);
	        terms.put(value, newTerm);
	        currentTerm = newTerm;
	    }
	    // Add the docId
	    currentTerm.addDocId(id);
	}

	/**
	 * Returns a filename suitable for this index.
	 *
	 * The filename includes the name of the index, then if role or source
	 * is specified the hash code of the corresponding String object will be used
	 * as a suffix.
	 */
	public String getPreferredFilename() {
        return this.id + IndexBuilder.FILENAME_EXTENSION;
	}

	/**
	 * Outputs this index in a specific file
	 * @param outputFile	The file where to write the index
	 */
	public void output(File outputFile) throws SAXException, IOException, ConfigurationException {

	    // We first need to create a serializer to write to the output file
	    XMLSerializer ser = new XMLSerializer();
	    Logger logger = new NullLogger();
	    ser.enableLogging(logger);
	    String dummyConf = "<dummy></dummy>";
	    Configuration conf = new DefaultConfigurationBuilder().build(new ByteArrayInputStream(dummyConf.getBytes()));
	    ser.configure(conf);
	    ser.setOutputStream(new FileOutputStream(outputFile));

	    // Then we output XML to the serializer
	    this.toSAX(ser);
	}

	public void outputInformation(ContentHandler hdl) throws SAXException {
	    AttributesImpl atts = new AttributesImpl();
        atts.addAttribute(IndexBuilder.NAMESPACE, "index-id", "index-id", "CDATA", this.id);
	    atts.addAttribute(IndexBuilder.NAMESPACE, "file", "file", "CDATA", getPreferredFilename());
	    atts.addAttribute(IndexBuilder.NAMESPACE, "nb", "nb", "CDATA", "" + terms.size());
	    hdl.startElement(IndexBuilder.NAMESPACE, "index", "index", atts);
	    hdl.endElement(IndexBuilder.NAMESPACE, "index", "index");
	}

	/**
	 * Generates SAX events for the XML representation of this index
	 * @param hdl	The SAX content handler that will receive events
	 */
	public void toSAX(ContentHandler hdl) throws SAXException {

	    // We need a start document here, since it is a new file
	    hdl.startDocument();

	    // Then the root element
	    AttributesImpl atts = new AttributesImpl();
        atts.addAttribute(IndexBuilder.NAMESPACE, "index-id", "index-id", "CDATA", this.id);
	    hdl.startElement(IndexBuilder.NAMESPACE, "index", "index", atts);

	    // Create and initialize a grouper
	    if ( terms.size() > nbTermsForGrouping ) {
		    Grouper g = new TargetLetterGrouper(1);
		    g.init(hdl);

			// Loop over terms and generate XML for each
			Iterator it = terms.keySet().iterator();
			while (it.hasNext()) {
			    String n = (String)it.next();
			    Term t = (Term)terms.get(n);
			    g.group(t, hdl);
			    //t.toSAX(hdl);
			}

			// Finalize grouper
			g.finalize(hdl);
	    }
	    else {
			// Loop over terms and generate XML for each
			Iterator it = terms.keySet().iterator();
			while (it.hasNext()) {
			    String n = (String)it.next();
			    Term t = (Term)terms.get(n);
			    t.toSAX(hdl);
			}
	    }

		// Output the end element
	    hdl.endElement(IndexBuilder.NAMESPACE, "index", "index");

	    // Output the end document
	    hdl.endDocument();
	}

	/**
	 * Normalize a term as in XSLT.
	 * @param s		The term to normalize
	 */
	public static String normalizeTerm(String s) {
	    StringBuffer sb = new StringBuffer();
        StringTokenizer st = new StringTokenizer(s);
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            if (st.hasMoreTokens()) sb.append(" ");
        }
        return sb.toString();
	}
}
