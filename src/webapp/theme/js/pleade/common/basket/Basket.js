//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion de l'affichage du porte document.
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion de l'affichage du porte document.
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var Basket = new Class(/** @lends Basket.prototype */{

  /**
  * Gestionnaire du panier
  * @private
  * @type BasketManager
  */
  basketManager: null,

  /**
  * Identifiant utilisé pour l'initialisation du tableau contenant les documents du panier
  * @private
  * @type String
  */
  _tblId: null,

  /**
  * Nom pour le fichier PDF lors de l'export du panier
  */
  basketPdfName: 'basket.pdf',

  /**
  * Nom pour le fichier ZIP lors de l'export du panier
  */
  basketZipName: 'basket.zip',

  /**
  * <strong>Constructor</strong><br/> Initialisation du panier
  */
  initialize: function(){
    this.basketManager = new BasketManager(this);
  },

  /**
  * Fonction d'initialisation de la TabView YUI
  * @param {String} tabSelector La classe CSS utilisee pour selectionner les onglets
  * @param {String} tabsSelector  L'identifiant ou l'objet HTML contenant la TabView
  * @param {String} isSortable Chaine indiquant que la DataTable est triable ou non
  * @param {String} sort_field Nom du champ sur lequel le tri doit être fait au chargement de la page
  * @param {String} sort_dir Ordre du tri au chargement de la page
  */
  initBasketTabs: function(tabSelector, tabsSelector, isSortable, sort_field, sort_dir) {
    this.isSortable = isSortable;
    this.sort_field = sort_field;
    this.sort_dir = sort_dir;
    if ( tabSelector==null || tabSelector.blank() ) {
      this.tabSelector = 'div.tab';
    }
    if ( tabsSelector==null || tabsSelector.blank() ) {
      this.tabsSelector = 'pl-pg-tabs';
    }
    var mTabs = new YAHOO.widget.TabView(this.tabsSelector);
    this.initBasketTab();
    mTabs.on('activeTabChange', function(ev) {
      return basket.initBasketTab();
    });
  },

  /**
   * Fonction d'initialisation d'un onglet du porte-documents
   * @param {String} tabSelector La classe CSS utilisee pour selectionner les onglets
   */
  initBasketTab: function(tabSelector){
    if ( tabSelector==null || tabSelector.blank() ) {
      this.tabSelector = 'div.tab';
    }
    $$(this.tabSelector).each(function(o,i){
      if(o!=null && o.id!=null ){
        var _base = o.id;
        basket.initBasketButtons(_base);
        basket.bindBasketPagination();
      }
    });
  },

  /**
  * Fonction permettant d'initialiser les boutons du panier (Vider et supprimer un element du panier)
  * @param {String} _base
  */
  initBasketButtons : function(_base){
    //boutons d'action
    var _btn_flush = new YAHOO.widget.Button('btn_flush-' + _base, {type: 'push'});
    var _btn_update = new YAHOO.widget.Button('btn_update-' + _base, {type: 'push'});
    $('btn_flush-' + _base + '-button').removeEvent('click');
    $('btn_update-' + _base + '-button').removeEvent('click');
    $('btn_flush-' + _base + '-button').addEvent(
      'click',
      function(event){
        if ( confirm(_usersMessagesBasket.basket_flush_basket_confirm) ) {
          basket.basketManager.deleteFromBasket( 'all', this, _base );
        }
        Event.stop(event);
      }
    );
    $('btn_update-' + _base + '-button').addEvent(
      'click',
      function(event){
        var _selecteds = $$('input:checked');
        if ( _selecteds == null || _selecteds.length == 0 ) {
          alert(_usersMessagesBasket.basket_delete_from_basket_no_selected);
        } else {
          basket.basketManager.deleteFromBasket(_selecteds, this, _base);
        }
        Event.stop(event);
      }
    );
    /** Export PDF du panier */
    if ( $('btn_pdf-' + _base) ) { //dans le "panier" slideshow, il n'y a pas de bouton PDF
      var _btn_pdf = new YAHOO.widget.Button('btn_pdf-' + _base, {type: 'push'});
      $('btn_pdf-' + _base + '-button').removeEvent('click');
      $('btn_pdf-' + _base + '-button').addEvent(
        'click',
        function(event){
          window.open(basket.basketPdfName);
          Event.stop(event);
        }
      );
    }

    /** Export ZIP du panier */
    if ( $('btn_zip-' + _base) ) { //dans le "panier" slideshow, il n'y a pas de bouton ZIP
      var _btn_pdf = new YAHOO.widget.Button('btn_zip-' + _base, {type: 'push'});
      $('btn_zip-' + _base + '-button').removeEvent('click');
      $('btn_zip-' + _base + '-button').addEvent(
        'click',
        function(event){
          window.open(basket.basketZipName);
          Event.stop(event);
        }
      );
    }

    /** Sauvegarde du panier */
    if ( $('btn_save-' + _base) ) {
      var _btn_pdf = new YAHOO.widget.Button('btn_save-' + _base, {type: 'push'});
      $('btn_save-' + _base + '-button').removeEvent('click');
      $('btn_save-' + _base + '-button').addEvent(
        'click',
        function(event){
          _res = prompt('Nom du panier', 'Entrez un nom pour votre panier');
          basket.basketManager.saveBasket(_res, _base);
          Event.stop(event);
        }
      );
    }

  },

  /** Gestion de la pagination */
  bindBasketPagination: function(){
    //pagination en ajax
    if( $('basket_pagination') ) {
      $$('#basket_pagination a').each(function(n){
        $(n).addEvent('click', basket.ajaxBasketPagination);
      });
    }
  },

  /**
  * Intercepte les liens de la pagination pour les envoyer en Ajax plutôt que
  * de recharger toute la page
  * @param {Event} e
  */
  ajaxBasketPagination: function(e){
    var _myBase = this.getParent('div.tab').get('id');
    var _url = this.href.replace('basket\.html', 'functions/'+ _myBase +'/getBasket.ajax');
    basket.ajaxBasketLoad(_url, _myBase);
    Event.stop(e);
  },

  /**
  * Fonction appelée pour le chargement du panier
  * @param {String} _url
  * @param {String} _currentBase
  */
  ajaxBasketLoad: function(_url, _currentBase){
    // appel au window manager global
    windowManager.loadAjaxContent(_url, $(_currentBase + '_basket_content'), null, function(){
      basket.basketLoaded();
    });
  },

  /**
  * Évènements qui surviennent une fois le panier rechargé via ajax
  * Fonction appelée après le chargement du panier
  */
  basketLoaded: function(){
    basket.bindBasketPagination();
  },

  initStoredBaskets: function(){
    var _sel = $('stored_baskets');
    _sel.addEvent('change', function(){
      window.location = String(window.location).replace(/basket\.html.*/, 'basket.html?stored=' + this.value);
    })
  }
});
