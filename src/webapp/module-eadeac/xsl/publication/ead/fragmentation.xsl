<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:saxon="http://saxonf.sf.net"
		exclude-result-prefixes="xsl fn">

	<!-- Fragmentation du document, pour l'indexation et la table des matières.
		 En réalité, le document n'est pas fragmenté, mais des attributs @pleade:break="true"
		 sont ajoutés là où il faut. Idem pour la table des matières. Idem pour les searchable. -->

	<!-- Méthodes de fragmentation -->
	<xsl:import href="fragmentation-methods.xsl"/>

	<!-- Méthodes d'identification des titres -->
	<xsl:import href="title-methods.xsl"/>

	<!-- Des utilitaires pour la publication -->
	<xsl:import href="publication-helpers.xsl"/>

	<!-- Paramètre qui indique si on est en mode static ou dynamique -->
	<xsl:param name="mode" select="'dyn'"/>
	
	<xsl:param name="debug" select="'false'"/>

	<!-- L'URL où se trouvent les fichiers produits (utile en mode statique). -->
	<!-- <xsl:param name="data-output-dir" select="''" /> -->

	<!-- Les informations sur les méthode à utilier -->
	<xsl:variable name="frag-method" select="normalize-space($pub-properties/property[@name='frag-method'])"/>
	<xsl:variable name="frag-method-info" select="normalize-space($pub-properties/property[@name='frag-method-info'])"/>
	<xsl:variable name="toc-method" select="normalize-space($pub-properties/property[@name='toc-method'])"/>
	<xsl:variable name="toc-method-info" select="normalize-space($pub-properties/property[@name='toc-method-info'])"/>
	<xsl:variable name="notsearchable-method" select="normalize-space($pub-properties/property[@name='notsearchable-method'])"/>
	<xsl:variable name="notsearchable-method-info" select="normalize-space($pub-properties/property[@name='notsearchable-method-info'])"/>


	<xsl:template match="/">
		<xsl:message>	Traitement de la fragmentation (etape 7 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result"><xsl:apply-templates/></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{ead/@pleade:id}/{ead/@pleade:id}-7.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement des composants -->
	<xsl:template match="archdesc|c|c01|c02|c03|c04|c05|c06|c07|c08|c09|c10|c11|c12">
		<!-- On vérifie s'il s'agit d'un fragment -->
		<xsl:variable name="fragment">
			<xsl:call-template name="is-fragment">
				<xsl:with-param name="method" select="$frag-method"/>
				<xsl:with-param name="info" select="$frag-method-info"/>
				<xsl:with-param name="checking-toc" select="false()"/>
				<xsl:with-param name="frag-value" select="''"/>
			</xsl:call-template>
		</xsl:variable>
		<!--On vérifie s'il doit être dans la table des matières -->
		<xsl:variable name="toc">
			<xsl:call-template name="is-fragment">
				<xsl:with-param name="method" select="$toc-method"/>
				<xsl:with-param name="info" select="$toc-method-info"/>
				<xsl:with-param name="checking-toc" select="true()"/>
				<xsl:with-param name="frag-value" select="$fragment"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- On vérifie s'il est cherchable selon la méthode pour identifier les unités non cherchables du formulaire de publication  -->
		<xsl:variable name="notsearchable">
			<xsl:call-template name="is-fragment">
				<xsl:with-param name="method" select="$notsearchable-method"/>
				<xsl:with-param name="info" select="$notsearchable-method-info"/>
				<xsl:with-param name="checking-toc" select="false()"/>
				<xsl:with-param name="checking-notsearchable" select="true()"/>
				<xsl:with-param name="frag-value" select="$fragment"/>
			</xsl:call-template>
		</xsl:variable>

		<!-- On recopie maintenant l'élément -->
		<xsl:copy>
			<!-- Les attributs existants -->
			<xsl:apply-templates select="@*" />
			<!-- Le titre -->
			<xsl:attribute name="pleade:title">
				<xsl:variable name="get-title">
				<xsl:call-template name="get-title">
					<xsl:with-param name="method" select="normalize-space($pub-properties/property[@name='title-method'])"/>
					<xsl:with-param name="sep" select="$pub-properties/property[@name='title-separator']"/>
						<xsl:with-param name="limit" select="$pub-properties/property[@name='title-limit']"/><!-- FIXME (MP) : Meilleure à mettre dans les propriétés d'affichage. -->
				</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="normalize-space($get-title)!=''">
						<xsl:value-of select="$get-title"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Unité sans titre</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<!-- L'attribut indiquant que c'est un fragment -->
			<xsl:if test="$fragment = 'true'">
				<xsl:attribute name="pleade:break">true</xsl:attribute>
			</xsl:if>
			<!-- Si c'est une entrée de table des matières -->
			<xsl:if test="$toc = 'true'">
				<xsl:attribute name="pleade:toc">true</xsl:attribute>
			</xsl:if>
			<!-- Si ce n'est pas cherchable -->
			<xsl:if test="$notsearchable = 'true' or not(pleade:is-external(.))">
				<xsl:attribute name="pleade:notsearchable">true</xsl:attribute>
			</xsl:if>
			<!-- Et le contenu -->
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- Traitement des div dans le frontmatter, qui sont des unités -->
	<xsl:template match="div">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:variable name="head">
				<xsl:apply-templates select="head" mode="filter"/>
			</xsl:variable>
			<xsl:if test="$head != ''">
				<xsl:attribute name="pleade:title"><xsl:value-of select="$head"/></xsl:attribute>
			</xsl:if>
			<xsl:attribute name="pleade:break"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- Traitement du frontmatter qui dépend de la présence de div ou pas. -->
	<xsl:template match="frontmatter">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="count(*) > 1">
				<xsl:attribute name="pleade:break"><xsl:text>true</xsl:text></xsl:attribute>
				<xsl:attribute name="pleade:notsearchable">true</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- FIXME (MP) : Est-ce correct pour la sortie du titlepage ?
	<xsl:template match="titlepage">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:variable name="head">
				<xsl:apply-templates select="head" mode="filter"/>
			</xsl:variable>
			<xsl:if test="$head != ''">
				<xsl:attribute name="pleade:title"><xsl:value-of select="$head"/></xsl:attribute>
			</xsl:if>
			<xsl:attribute name="pleade:notsearchable"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>-->

	<!-- On n'utilise pas les commentaires -->
	<xsl:template match="comment()"/>

	<!-- Les autres contenus -->
	<xsl:template match="*|text()|processing-instruction()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
