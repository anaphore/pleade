<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSL qui permet d'afficher une liste d'utilisateurs et
		qui peut aussi permettre de les modifier, si $edit = true.
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="xsl i18n xhtml">

	<!-- La source d'information sur les utilisateurs -->
	<xsl:param name="source" select="'file'"/>

	<!-- Si on doit éditer ou pas -->
	<xsl:param name="edit" select="'false'"/>

	<!-- Un booléen pour le mode d'édition -->
	<xsl:param name="editable" select="boolean($edit)"/>

	<!-- On reçoit en entrée une liste d'utilisateurs -->
	<xsl:template match="/users">
		<html>
			<head>
				<title>
					<i18n:text key="admin.users.home.title"/>
				</title>
				<link href="yui/assets/skins/pleade/datatable.css" type="text/css" rel="stylesheet"/>
				<!-- FIXME: certains devraient être dans le modèle -->
				<script type="text/javascript" src="yui/utilities/utilities.js">&#160;</script>
        <script type="text/javascript" src="js/mootools/mootools-core-1.3.1-nc.js">&#160; </script>
        <script type="text/javascript" src="js/mootools/mootools-more-1.3.1.1-nc.js">&#160; </script>
				<script type="text/javascript" src="js/pleade/common/util/mootools-common.js">&#160;  </script>
				
				<script type="text/javascript" src="yui/datasource/datasource-min.js">&#160;</script>
				<script type="text/javascript" src="yui/button/button-min.js">&#160;</script>
				<script type="text/javascript" src="yui/datatable/datatable-min.js">&#160;</script>
				<!-- La librairie de gestion des utilisateurs -->
				<script type="text/javascript" src="i18n/pleade-admin-js.js">&#160;</script>
				<script type="text/javascript" src="js/pleade-admin.js">&#160;</script>
				<script type="text/javascript" src="js/pleade/admin/user/UserManager.js">&#160;</script>
				
				<script type="text/javascript">
					<!-- On définit une variable indiquant si la source est modifiable ou non -->
					<xsl:choose>
						<xsl:when test="$editable">var sourceEditable = true;</xsl:when>
						<xsl:otherwise>var sourceEditable = false;</xsl:otherwise>
					</xsl:choose>
					<!-- On définit une variable qui indique la source de données -->
					var sourceCode = '<xsl:value-of select="$source"/>';
					<!-- On définit un tableau qui contient les codes utilisateurs originaux -->
					var originalUsernames = [];
					<!-- On appelle l'initialisation du tableau sur le onload -->
					window.addEvent("load", init);
					var userManager = "";
					function init(){
						userManager = new UserManager();
					}
				</script>
			</head>
			<body>
				<!-- Un titre -->
				<h1 class="pl-title">
					<i18n:text key="admin.users.source.{$source}"/>
				</h1>
				<!-- Une intro -->
				<div>
					<xsl:variable name="key">
						<xsl:choose>
							<xsl:when test="$editable">admin.users.page.intro.edit</xsl:when>
							<xsl:otherwise>admin.users.page.intro.list</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<i18n:text key="{$key}"/>
				</div>
				<!-- Un bouton pour ajouter un utilisateur -->
				<xsl:if test="$editable">
					<p>
						<button type="button" id="btn-user-add" title="admin.users.page.add.title"
							class="yui-button yui-push-button" i18n:attr="title">
						<i18n:text key="admin.users.page.add.text">ajouter un utilisateur...</i18n:text>
					</button></p>
				</xsl:if>
				<!-- Et une liste -->
				<div id="userslist">
					<table id="users-table">
						<xsl:apply-templates select="user"/>
					</table>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- Une rangée dans le tableau -->
	<xsl:template match="user">
		<tr>
			<!-- Les informations sur l'utilisateur -->
			<td>
				<xsl:value-of select="@code"/>
			</td>
			<td>
				<xsl:value-of select="@password"/>
			</td>
			<td>
				<xsl:value-of select="@roles"/>
			</td>
			<td>
				<xsl:value-of select="@name"/>
			</td>
			<td>
				<xsl:value-of select="@email"/>
			</td>
			<!-- Les deux dernières colonnes sont pour les actions d'édition -->
			<xsl:if test="$editable">
				<td>
					<xsl:call-template name="output-button">
						<xsl:with-param name="role" select="'save'"/>
						<xsl:with-param name="code" select="@code"/>
						<xsl:with-param name="no" select="position()"/>
					</xsl:call-template>
				</td>
				<td>
					<xsl:call-template name="output-button">
						<xsl:with-param name="role" select="'delete'"/>
						<xsl:with-param name="code" select="@code"/>
						<xsl:with-param name="no" select="position()"/>
					</xsl:call-template>
				</td>
			</xsl:if>
		</tr>
	</xsl:template>

	<!-- Sortie d'un bouton pour sauevagrder ou supprimer -->
	<xsl:template name="output-button">
		<xsl:param name="role" select="'save'"/>
		<xsl:param name="code" select="''"/>
		<xsl:param name="no" />
		<xsl:variable name="id">
			<xsl:value-of select="concat('btn-', $role)" />
			<xsl:if test="number($no)">
				<xsl:value-of select="concat('-', $no)" />
			</xsl:if>
		</xsl:variable>
		<i18n:text i18n:key="admin.user.list.action.{$role}"/>
		<!-- <span id="{$id}-span" class="yui-button yui-push-button">
			<span class="first-child">
				<button type="button" name="{$id}" id="{$id}" i18n:attr="title" title="admin.user.list.action.{$role}">
					<i18n:text i18n:key="admin.user.list.action.{$role}"/>
				</button>
			</span>
		</span> -->
	</xsl:template>


</xsl:stylesheet>

