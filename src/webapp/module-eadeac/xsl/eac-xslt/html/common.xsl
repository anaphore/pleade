<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: common.xsl 18200 2010-03-08 09:02:43Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		exclude-result-prefixes="xsl i18n pleade sdx xs">
		<!-- 
		
			/!\/!\ CE FICHIER NE PEUT PAS ÊTRE SURCHARGÉ DIRECTEMENT, /!\/!\
			/!\/!\     IL FAUT SURCHARGER CEUX QUI L'IMPORTENT        /!\/!\
		
		-->
		
	<!-- Fonction pour identifier les éléments avec des contenus gérés par des listes -->
	<xsl:function name="pleade:eac-listElement" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:value-of select="boolean($el/self::*[local-name() = 'maintenanceStatus'] or $el/self::*[local-name() = 'eventType'] or $el/self::*[local-name() = 'agentType'] or $el/self::*[local-name() = 'publicationStatus'])
												or $el/self::*[local-name() = 'entityType']"/>
	</xsl:function>
	
	<!-- Fonction pour afficher des dates -->
	<xsl:function name="pleade:eacDisplayDate" as="xs:string">
		<xsl:param name="el"/>
		<xsl:choose>
			<xsl:when test="$el != ''"><xsl:value-of select="$el"/></xsl:when>
			<xsl:when test="$el/@standardDate"><xsl:value-of select="$el/@standardDate"/></xsl:when>
			<xsl:when test="$el/@standardDateTime"><xsl:value-of select="$el/@standardDateTime"/></xsl:when>
		</xsl:choose>
	</xsl:function>
	
	<!--+
      | Affichage des titres de sections
      +-->
  <xsl:template match="*" mode="sect-title">
    <thead>
      <tr>
        <th colspan="2">
          <i18n:text key="eac.{local-name()}">
            <xsl:value-of select="local-name()" />
          </i18n:text>
        </th>
      </tr>
    </thead>
  </xsl:template>
	
	<!--+
      | Affichage des étiquettes
      +-->
	<xsl:template match="*" mode="label">
		<xsl:param name="eln" select="local-name()"/>
		
		<xsl:if test="not(preceding-sibling::*[local-name() = current()/local-name()])">
			<i18n:text key="eac.{$eln}.{@localType}">
				<xsl:value-of select="$eln" />
			</i18n:text>
		</xsl:if>
	</xsl:template>
	
	<!-- Affichage du surlignage -->
	<xsl:template match="sdx:hilite">
		<span class="pl-hilite"><xsl:value-of select="."/></span>
	</xsl:template>


</xsl:stylesheet>
