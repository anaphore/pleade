<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: get-default-properties.xsl 12373 2008-10-15 14:41:34Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Construction de la liste des propriétés d'un document
    | On fusionne les propriétés spécifiques et les propriétés générales.
    | On recopie l'ensemble des propriétés spécifiques. Lorsque la propriétés
    | générales n'existe pas dans le fichier de propriétés spécifiques, on la prend.
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl">

    <xsl:param name="set" select="''" />

    <xsl:variable name="doc" select="/root/doc" /><!-- Propriétés spécifiques -->
    <xsl:variable name="gen" select="/root/gen" /><!-- Propriétés générales -->

    <!-- Match la racine -->
    <xsl:template match="/root">
			<!-- On recopie les propriétés spécifiques -->
			<xsl:choose>
				<xsl:when test="$doc/empty">
					<xsl:apply-templates select="$gen/properties" mode="copy-gen" />
				</xsl:when>
				<xsl:otherwise>
			<xsl:apply-templates select="$doc/properties" mode="copy" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:template>

		<!-- Match la racine des propriétés spécifiques -->
		<xsl:template match="properties[parent::doc]" mode="copy">
			<xsl:variable name="id" select="@id" />
			<xsl:copy>
				<xsl:apply-templates select="@* | node()" mode="copy" />
				<!-- Maintenant, on va chercher les propriétés générales qui n'existent pas dans les propriétés spécifiques -->
				<xsl:apply-templates select="$gen/properties[@id = $id]/property" mode="copy-gen"/>
			</xsl:copy>
		</xsl:template>

		<!-- Match la racine des propriétés génériques (uniquement si les spécifiques sont vides) -->
		<xsl:template match="properties[parent::gen]" mode="copy-gen">
			<xsl:copy>
				<xsl:apply-templates select="@* | node()" mode="copy-gen" />
			</xsl:copy>
		</xsl:template>

		<!-- Match les propriétés générales
				 Elle n'est recopiée que s'il n'existe pas de propriété spécifique de ce nom.-->
		<xsl:template match="property" mode="copy-gen">
			<xsl:if test="not( $doc/properties/property[@name = current()/@name] )">
				<xsl:copy>
					<!-- On ajoute un attribut "orig" pour indiquer la propriété vient de
					la liste des propriétés générales. Ça peut être utile pour le débogage. -->
					<xsl:attribute name="orig"><xsl:text>gen</xsl:text></xsl:attribute>
					<xsl:apply-templates select="@*[not(local-name()='default')]" mode="copy-gen" />
					<xsl:value-of select="@default" />
				</xsl:copy>
			</xsl:if>
		</xsl:template>

		<!-- Match les propriétés (spécifiques ou génériques) -->
		<xsl:template match="property" mode="copy">
			<xsl:copy>
				<xsl:apply-templates select="@* | node()" mode="copy" />
			</xsl:copy>
		</xsl:template>

		<!-- Règle générique pour recopier un un noeud-->
		<xsl:template match="@* | node()" mode="copy">
			<xsl:copy>
				<xsl:apply-templates select="@* | node()" mode="copy" />
			</xsl:copy>
		</xsl:template>
		<xsl:template match="@* | node()" mode="copy-gen">
			<xsl:copy>
				<xsl:apply-templates select="@* | node()" mode="copy-gen" />
			</xsl:copy>
		</xsl:template>

</xsl:stylesheet>
