<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT construisant l'XHTML de la partie dynamique de la recherche
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		exclude-result-prefixes="xsl xsp sdx i18n pleade fn">

	<!-- Fonctions d'utilités générales -->
	<xsl:import href="../../commons/xsl/functions.xsl"/>

  <xsl:param name="base" select="''" />
  <xsl:variable name="b">
    <xsl:choose>
      <xsl:when test="$base!=''"><xsl:value-of select="$base" /></xsl:when>
      <xsl:otherwise><xsl:text>ead</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


	<xsl:template match="/sdx:document | /root/*/sdx:document">

		<xsl:variable name="a">
			<xsl:choose>
				<xsl:when test="fn:sum(number(sdx:uploadDocuments/sdx:summary/@additions))">
					<xsl:value-of select="fn:sum(number(sdx:uploadDocuments/sdx:summary/@additions))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="f">
			<xsl:choose>
				<xsl:when test="fn:sum(number(sdx:uploadDocuments/sdx:summary/@failures)) and //wf-exception">
					<xsl:value-of select="fn:sum(number(sdx:uploadDocuments/sdx:summary/@failures)) + count(//wf-exception)" />
				</xsl:when>
				<xsl:when test="fn:sum(number(sdx:uploadDocuments/sdx:summary/@failures))">
					<xsl:value-of select="fn:sum(number(sdx:uploadDocuments/sdx:summary/@failures))" />
				</xsl:when>
				<xsl:when test="//wf-exception">
					<xsl:value-of select="number(count(//wf-exception))"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="d">
			<xsl:choose>
				<xsl:when test="fn:sum(number(sdx:uploadDocuments/sdx:summary/@duration))">
					<xsl:value-of select="fn:sum(number(sdx:uploadDocuments/sdx:summary/@duration))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<html>

			<head>
				<title>
					<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.html.title">Publier des documents</i18n:text>
				</title>
			</head>

			<body>

				<h1 class="pl-title">
					<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.pl.title">Publier des documents</i18n:text>
				</h1>

				<div><i18n:text key="edit.ead-publish.result.intro" /></div>

				<xsl:if test="//wf-exception">
					<div id="wf-errors" class="pl-form-message pl-exception-msg msg-fail">
						<xsl:apply-templates select="wf-exception"/>
					</div>
				</xsl:if>

				<table border="0" cellspacing="0" cellpadding="0" class="pl-ead-publish-result pl-ead-publish-result-summary"
						summary="Resultat de publication de documents...">

					<tbody>

						<tr>

							<td class="label">
								<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.additions">ajouts</i18n:text>
							</td>
							<td class="value">
								<xsl:value-of select="pleade:format-number($a)" />
							</td>

						</tr>

						<tr>

							<td class="label">
								<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.failures">échecs</i18n:text>
							</td>
							<td class="value">
								<xsl:value-of select="pleade:format-number($f)" />
							</td>

						</tr>

						<tr>

							<td class="label">
								<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.duration">durée</i18n:text>
							</td>
							<td class="value">
								<xsl:value-of select="concat(pleade:format-number($d, '', '2', ':'), ' sec.')" />
							</td>

						</tr>

					</tbody>

				</table>

				<div>
					<xsl:comment />
					<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.conclusion" />
				</div>

        <xsl:apply-templates select="sdx:loggingIndexation" />
				<xsl:if test="not(sdx:loggingIndexation)">
					<xsl:apply-templates select=".//sdx:exception" />
				</xsl:if>

			</body>

		</html>

	</xsl:template>

	<xsl:template match="wf-exception">
		<p>
			<xsl:choose>
				<xsl:when test="@line or @column">
					<!-- We got a line or a column: it's a parse error -->
					<i18n:translate>
						<i18n:text catalogue="module-eadeac-edit" key="ead-admin.file.notwf">xml not well-formed</i18n:text>
						<i18n:param><xsl:value-of select="@file"/></i18n:param>
						<i18n:param><xsl:value-of select="."/></i18n:param>
						<i18n:param><xsl:value-of select="@line"/></i18n:param>
						<i18n:param><xsl:value-of select="@column"/></i18n:param>
					</i18n:translate>
				</xsl:when>
				<xsl:otherwise>
					<!-- Another error occured while checking if XML is well-formed -->
					<i18n:translate>
						<i18n:text catalogue="module-eadeac-edit" key="ead-admin.file.error">an error occured</i18n:text>
						<i18n:param><xsl:value-of select="@file"/></i18n:param>
						<i18n:param><xsl:value-of select="."/></i18n:param>
					</i18n:translate>
				</xsl:otherwise>
			</xsl:choose>
		</p>
	</xsl:template>

	<!-- Affichage des informations complémentaires -->
	<xsl:template match="sdx:loggingIndexation">
		<xsl:variable name="id" select="normalize-space(sdx:IndexationID)" />
		<xsl:variable name="script">
			<xsl:text>javascript:var c=document.getElementById('</xsl:text><xsl:value-of select="$id" /><xsl:text>');</xsl:text>
			<xsl:text>if(c &amp;&amp; c.style &amp;&amp; c.style.display) { (c.style.display=='block') ? c.style.display='none' : c.style.display='block'; return false; } else return true;</xsl:text>
		</xsl:variable>
		<a href="void(0);" onclick="{$script}" title="module-eadeac-edit:edit.ead-publish.result.more-infos.show.title" i18n:attr="title">
			<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.show">afficher le complément d'information</i18n:text>
		</a>
		<div class="pl-form-message pl-exception-stack" id="{$id}" style="display:none;">
			<table style="border:1px solid" class="pl-ead-publish-result sdx-lui"
				id="indexationlog{if(sdx:IndexationID!='') then concat('-',sdx:IndexationID) else ''}" summary="Complements d'informations de publication">
				<caption><i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.caption">compléments d'information de publication</i18n:text></caption>
				<tbody>
				<xsl:apply-templates select="sdx:start" />
				<xsl:apply-templates select="sdx:end" />
				<xsl:apply-templates select="sdx:sdxdbid" />
				<xsl:apply-templates select="sdx:nb-docs-to-index" />
				<xsl:apply-templates select="sdx:nb-doc-added" />
				<xsl:apply-templates select="sdx:nb-docs-error" />
				<xsl:apply-templates select="sdx:added-docs" />
				<xsl:apply-templates select="sdx:*[starts-with(local-name(), 'doc-error-info')][1]" />
				</tbody>
			</table>
		</div>
	</xsl:template>

	<!-- Affichage générique des informations complémentaires -->
	<xsl:template match="sdx:loggingIndexation/sdx:*">
			<tr class="pl-lui pl-lui-{local-name()}">
				<th class="label"><i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.{local-name()}"><xsl:value-of select="local-name()" /></i18n:text></th>
				<td class="value"><xsl:apply-templates select="." mode="value" /></td>
			</tr>
		</xsl:template>
		<xsl:template match="sdx:loggingIndexation/sdx:*[starts-with(local-name(),'doc-error-info')]" priority="+2">
			<tr class="pl-lui pl-lui-docs-error-info">
				<th class="label"><i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.doc-error-info">liste des erreurs</i18n:text></th>
				<td class="value"><xsl:apply-templates select="." mode="value" /></td>
			</tr>
		</xsl:template>

		<!-- Affichage générique des informations complémentaires -->
		<xsl:template match="sdx:loggingIndexation/sdx:*" mode="value">
			<xsl:choose>
			  <xsl:when test="self::sdx:added-docs"><xsl:call-template name="get-added-docs-list" /></xsl:when>
				<xsl:when test="self::*[starts-with(local-name(), 'doc-error-info-')]"><xsl:call-template name="get-error-info-list" /></xsl:when>
			  <xsl:otherwise><xsl:apply-templates /></xsl:otherwise>
			</xsl:choose>
		</xsl:template>

		<!-- La liste des documents ajoutés -->
		<xsl:template name="get-added-docs-list">

		<!-- Match sdx:added-docs -->

			<xsl:variable name="is-doc-more-infos" select="parent::sdx:loggingIndexation/sdx:*[starts-with(local-name(), 'doc-more-infos-')] !=  ''" />

			<xsl:choose>
			  <xsl:when test="$is-doc-more-infos">
					<xsl:apply-templates select="." mode="table" />
				</xsl:when>
			  <xsl:otherwise>
					<xsl:apply-templates select="." mode="ul" />
				</xsl:otherwise>
			</xsl:choose>

		</xsl:template>

		<xsl:template match="sdx:added-docs" mode="table">
			<xsl:variable name="root" select="parent::sdx:loggingIndexation" />
			<table style="border:none" class="pl-lui-doc-added" summary="Complements d'informations sur les documents publiés">
				<thead>
					<tr>
						<th class="pl-lui-doc-added-rank"><xsl:comment>rank</xsl:comment></th>
						<xsl:apply-templates select="$root/sdx:*[starts-with(local-name(), 'doc-more-infos-')][1]" mode="doc-more-infos-th" />
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="tokenize(normalize-space(.), ';')">
						<xsl:sort select="substring-before(.,'=')"/>
						<xsl:variable name="rank" select="substring-before(.,'=')" />
						<tr>
							<td>
								<xsl:value-of select="$rank" />
							</td>
							<xsl:apply-templates select="$root/sdx:*[local-name() = concat('doc-more-infos-',$rank)]" mode="doc-more-infos" />
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</xsl:template>
		<xsl:template match="sdx:*" mode="ul">
			<ul class="pl-lui-doc-added">
				<xsl:for-each select="tokenize(normalize-space(.), ';')">
					<xsl:sort select="substring-before(.,'=')"/>
					<xsl:variable name="rank" select="substring-before(.,'=')" />
					<li>
						<span class="pl-rk">
							<i18n:translate>
								<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.doc-rank">document n°</i18n:text>
								<i18n:param><xsl:value-of select="$rank" /></i18n:param>
							</i18n:translate>
						</span>
						<xsl:value-of select="substring-after(.,'=')" />
					</li>
				</xsl:for-each>
			</ul>
		</xsl:template>

		<xsl:template match="sdx:*" mode="doc-more-infos">
			<!-- {start=2009-07-01T15:24:19.389 CEST, nbsubdocs=1, rank=9, end=2009-07-01T15:24:18.453 CEST, nbattached=2, id=FRAD064_IR0122} -->
			<xsl:for-each select="tokenize( substring(., 2, (string-length(.) - 2)), ', ' )">
				<xsl:variable name="label" select="substring-before(.,'=')" />
				<xsl:if test="$label!='rank'">
					<td>
            <xsl:choose>
              <xsl:when test="$label = 'id'">
                <a href="../ead.html?id={substring-after(.,'=')}"><xsl:value-of select="substring-after(.,'=')" /></a>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="substring-after(.,'=')" />
              </xsl:otherwise>
            </xsl:choose>
					</td>
				</xsl:if>
			</xsl:for-each>
		</xsl:template>

		<xsl:template match="sdx:*" mode="doc-more-infos-th">
			<!-- {start=2009-07-01T15:24:19.389 CEST, nbsubdocs=1, rank=9, end=2009-07-01T15:24:18.453 CEST, nbattached=2, id=FRAD064_IR0122} -->
			<xsl:for-each select="tokenize( substring(., 2, (string-length(.) - 2)), ', ' )">
				<xsl:variable name="label" select="substring-before(.,'=')" />
				<xsl:if test="$label!='rank'"> <!-- le rank est traité dans le template appelant celui-là -->
					<th>
						<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.doc-infos.{$label}"><xsl:value-of select="substring-after(.,'=')" /></i18n:text>
					</th>
				</xsl:if>
			</xsl:for-each>
		</xsl:template>

		<!-- La liste des erreurs -->
		<xsl:template name="get-error-info-list">
			<dl>
			<xsl:for-each select="tokenize(normalize-space(.), ';')">
				<xsl:sort select="substring-before(.,'=')" />
				<xsl:variable name="rank" select="substring-before(.,'=')" />
				<dt class="pl-rk">
					<i18n:translate>
						<i18n:text catalogue="module-eadeac-edit" key="edit.ead-publish.result.more-infos.doc-rank">document n°</i18n:text>
						<i18n:param><xsl:value-of select="$rank" /></i18n:param>
					</i18n:translate>
				</dt>
				<dd class="msg-fail"><xsl:value-of select="substring-after(.,'=')" /></dd>
			</xsl:for-each>
			</dl>
		</xsl:template>

  <!--+
      | Les erreurs d'indexation
      +-->
  <xsl:template match="sdx:exception">

    <xsl:variable name="id" select="generate-id(sdx:originalException)" />

    <div class="pl-form-message">
      <xsl:apply-templates select="parent::sdx:uploadDocument[@status='failure']" />
      <div class="pl-form-message pl-exception-msg msg-fail">
				<xsl:choose>
				  <xsl:when test="sdx:originalException">
        <a href="void(0);" onclick="javascript:$('{$id}').toggle(); return false;">
          <xsl:apply-templates select="sdx:message" />
        </a>
        <div id="{$id}" class="pl-form-message pl-exception-stack" style="display:none;">
          <xsl:apply-templates select="sdx:originalException" />
        </div>
				  </xsl:when>
				  <xsl:otherwise>
						<xsl:apply-templates select="sdx:message" />
				  </xsl:otherwise>
				</xsl:choose>
      </div>
    </div>

  </xsl:template>

  <xsl:template match="sdx:originalException">
    <pre>
      <xsl:apply-templates />
    </pre>
  </xsl:template>

  <xsl:template match="sdx:*[@status='failure']">
    <div class="pl-form-message pl-exception-docid">
      <!-- FIXME (MP) : Faut-il plus d'informations ? -->
      <i18n:text key="edit.ead-publish.result.document" catalogue="module-eadeac-edit" />
      <xsl:value-of select="@id" />
    </div>
  </xsl:template>

</xsl:stylesheet>
