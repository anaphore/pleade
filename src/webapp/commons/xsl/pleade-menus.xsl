<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSLT gérant la dynamisation des menus
    |
    | Cette XSLT dynamise certaines parties du modèle identifiant les menus.
    | Menu horizontal "haut de page"	:	*/@id='pl-pg-body-tmenu'
    | Menu horizontal "onglet"				:	*/@id='pl-pg-body-hmenu'
    | Menu vertical "gauche"					:	*/@id='left-menu'
    |
    | TODO (MP) : implémenter les entrées de type "subset"
		|
		| Les identifiants des entrées de menu sont préfixées pour éviter de faire doublon avec d'autres identifiants sur une page (par exemple, l'identifiant d'un formulaire)
    +-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:xsp="http://apache.org/xsp"
	xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsp-session="http://apache.org/xsp/session/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xsl xsp sdx i18n xhtml xsp-session">

	<!-- Les rôles de l'utilisateur -->
  <xsl:variable name="roles" select="replace($userinfos/authentication/roles[1],'\s','')" />
  <!-- FIXME (MP) : On n'utilise plus le système de JXPathMetaModule qui ne
                    fontionne pas ou plutôt n'est pas mis à jour immédiatement.
                    On utilise la variable "userinfos" déclarée dans
                    "pleade-userinfos.xsl", XSL importée par "pleade-aggregate.xsl",
                    comme cette présente XSL. -->
	<!-- <xsl:param name="roles" select="''" />
	<xsl:param name="rolesXML" select="$userinfos/authentication/roles" />
	<xsl:variable name="_roles">
		<xsl:choose>
			<xsl:when test="$rolesXML!='' or $userinfos/authentication/roles!=''">
				<xsl:value-of select="$rolesXML" />
			</xsl:when>
			<xsl:when test="$roles!=''">
				<xsl:value-of select="$roles" />
			</xsl:when>
		</xsl:choose>
	</xsl:variable> -->

	<!-- Menus dans le fichier de configuration -->
	<xsl:variable name="menus" select="/aggregate/menus" />
	<!-- Menu horizontal "haut de page" -->
	<xsl:variable name="htop" select="$menus/menu[@type='htop']" />
	<!-- Menu horizontal "onglets" -->
	<xsl:variable name="htabs" select="$menus/menu[@type='htabs']" />
	<!-- Menus verticaux "gauche" -->
	<xsl:variable name="vleft" select="$menus/menu[@type='vleft']" />
	
	<!-- Préfixe des identifiants des entrées de menu -->
	<xsl:variable name="menu-id-prefix" select="'mnu_'"/>
	
	<!--+
			|	Construction du menu horizontal "haut de page"
			|	Ce menu peut-être codé en dur dans le modèle XHTML
			+-->
	<xsl:template match="xhtml:*[@id='pl-pg-body-tmenu']" mode="template">

<!--<xsl:message>
[MENUS] construction menu horizontal "haut de page"...</xsl:message>-->

			<xsl:apply-templates select="." mode="no-depends">
				<xsl:with-param name="type" select="'htop'" />
			</xsl:apply-templates>

	</xsl:template>

	<!--+
			|	Construction du menu horizontal "onglets"
			+-->
	<xsl:template match="xhtml:*[@id='pl-pg-body-hmenu']" mode="template">


<!--<xsl:message>
[MENUS] construction menu horizontal "onglets"...
   _roles = <xsl:value-of select="$roles" />
userinfos =
<xsl:copy-of select="$userinfos" />
</xsl:message>-->

			<xsl:apply-templates select="." mode="no-depends">
				<xsl:with-param name="type" select="'htabs'" />
			</xsl:apply-templates>

	</xsl:template>

	<!--+
			|	Construction du menu vertical "gauche"
			|	La variable vleft-menu-id donne l'identifiant du menu gauche. Elle est
			| initialisée dans pleade-aggregate.xsl qui importe cette XSL.
			+-->
	<xsl:template match="xhtml:*[@id='pl-pg-body-main-left-menu-container']" mode="template">

<!--<xsl:message>
[MENUS] construction menu vertical "gauche"...
vlef-menu-id=<xsl:value-of select="$vleft-menu-id" /></xsl:message>-->

			<xsl:apply-templates select="." mode="depends">
				<xsl:with-param name="type" select="'vleft'" />
				<xsl:with-param name="menu-id" select="$vleft-menu-id" />
			</xsl:apply-templates>

	</xsl:template>

	<!--+
			| Traitement d'un menu avec ou sans dépendance
			+-->
	<xsl:template match="xhtml:*" mode="depends">

		<xsl:param name="type" select="''" />
		<xsl:param name="menu-id" select="'NULL'" />
		<xsl:variable name="menu" select="$menus/menu[@type=$type]" />

<!--<xsl:message>
[MENUS] match <xsl:value-of select="count($menu)" /> menu(s) de type <xsl:value-of select="$type" /> en mode "depends"</xsl:message>-->

		<xsl:variable name="menu2work">

			<xsl:choose>
				<xsl:when test="$menu-id='NULL'">
					<xsl:call-template name="menu2work">
						<xsl:with-param name="type" select="$type" />
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$menu-id" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

<!--<xsl:message>[MENUS] menu2work=<xsl:value-of select="$menu2work" /></xsl:message>-->

		<xsl:choose>

			<xsl:when test="$menu2work='' or not($menu2work)">
				<xsl:apply-templates select="." mode="no-depends">
					<xsl:with-param name="type" select="$type" />
				</xsl:apply-templates>
			</xsl:when>

			<xsl:otherwise>
				<xsl:variable name="_menu" select="$menus/menu[@type=$type and @id=$menu2work]" />
				<xsl:choose>
					<xsl:when test="$type='vleft'">
						<xsl:apply-templates select="." mode="template-vleft">
							<xsl:with-param name="_menu" select="$_menu" />
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy>
							<xsl:apply-templates select="@*" mode="template" />
							<xsl:apply-templates select="$_menu" />
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>

		</xsl:choose>

	</xsl:template>

	<!--+
			| Résolution de la dépendance d'un menu par rapport à un autre.
			|
			| Fonction retournant l'identifiant du menu à travailler pour le type courant
			| Si ce menu existe et qu'il possède une dépendance par rapport à un autre
			| menu, on boucle sur les items de cet autre menu pour savoir s'il y en a
			| un actif. Si oui, alors le menu courant est à afficher. Dans le cas
			| contraire on le cache.
			+-->
	<xsl:template name="menu2work">

		<xsl:param name="type" select="''" />
		<xsl:param name="menu" select="$menus/menu[@type=$type]" />

		<xsl:variable name="menu-depends-id">

			<xsl:if test="$menu/@depends!=''">

					<xsl:for-each select="$menu[@depends]">

						<xsl:variable name="_d" select="normalize-space(@depends)" />
						<xsl:variable name="_t" select="substring-before($_d, ':')" />
						<xsl:variable name="_i" select="substring-after($_d, ':')" />

<!--<xsl:message>
@id = <xsl:value-of select="@id"/>
@type = <xsl:value-of select="@type" />
@depends = <xsl:value-of select="$_d" />
_t=<xsl:value-of select="$_t" />
_i=<xsl:value-of select="$_i" />
</xsl:message>-->
<!--<xsl:message>
[MENU2WORK <xsl:value-of select="@id"/> @class=<xsl:value-of select="@class"/>]</xsl:message>-->

							<xsl:if test="$_t!='' and $_i!='' and
															contains( ',htop,htabs,vleft,', concat(',',$_t,',') )">

								<xsl:variable name="isActive">
									<xsl:apply-templates select="$menus/menu[@type=$_t]/item[@id=$_i]" mode="is-active" />
								</xsl:variable>

								<xsl:if test="$isActive='true'">
									<xsl:value-of select="@id" /><!-- L'identifiant du menu à activer -->
<!--<xsl:message>
	isActive = <xsl:value-of select="$isActive"/>
</xsl:message>-->
									<!-- Pour l'accordéon, on veut l'identifiant de l'item actif dans le menu -->
									<xsl:for-each select="descendant-or-self::menu[contains(@class,'pl-accordion')]">
										<xsl:variable name="mi" select="@id"/>

										<xsl:for-each select="descendant::item">
											<xsl:variable name="isItemActive">
												<xsl:apply-templates select="." mode="is-active" />
											</xsl:variable>
<!--<xsl:message>
[MENU ACCORDEON  <xsl:value-of select="$mi"/>][ITEM  <xsl:value-of select="@id"/>] :: <xsl:value-of select="$isItemActive"/>
</xsl:message>-->
											<xsl:if test="$isItemActive='true'">
												<xsl:value-of select="concat('::::',@id)"/>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:if>

							</xsl:if>

					</xsl:for-each>

				</xsl:if>

			</xsl:variable>

			<xsl:choose>
				<xsl:when test="$menu-depends-id!=''">
					<xsl:value-of select="$menu-depends-id" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$menu[@type=$type and @default='true']/@id" />
<!--<xsl:message>
Renvoie l'id du menu par defaut</xsl:message>-->
				</xsl:otherwise>
			</xsl:choose>

	</xsl:template>

	<!--+
			| Traitement particulier pour le menu vertical
			| On souhaite le cacher s'il doit être vide
			+-->
	<xsl:template match="xhtml:*" mode="template-vleft">
		<xsl:param name="_menu" select="''" />
		<xsl:if test="$_menu">
			<xsl:choose>
				<xsl:when test="@id='left-menu'">
          <!-- On a le bon element XHTML contenant le menu gauche, on peut lancer la sortie -->
					<xsl:element name="{local-name()}">
						<xsl:apply-templates select="@*" mode="template" />
						<xsl:apply-templates select="$_menu" />
            <xsl:apply-templates select="node()" mode="template" />
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
          <!-- Tant qu'on n'a pas le bon element XHTML contenant le menu gauche, on recopie la structure statique -->
					<xsl:element name="{local-name()}">
						<xsl:apply-templates select="@*" mode="template" />
						<xsl:apply-templates select="node()" mode="template-vleft">
							<xsl:with-param name="_menu" select="$_menu" />
						</xsl:apply-templates>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!--+
			| Traitement de l'affichage d'un menu sans règle de dépendance
			+-->
	<xsl:template match="xhtml:*" mode="no-depends">

		<xsl:param name="type" select="''" />

		<xsl:variable name="menu" select="$menus/menu[@type=$type]" />

<!--<xsl:message>
[MENUS] match <xsl:value-of select="count($menu)" /> menu de type <xsl:value-of select="$type" /> en mode no-depends</xsl:message>-->

		<!-- Pas de calcul de dépendance, on traite le menu par défaut ou le premier de la liste -->
		<xsl:choose>
			<xsl:when test="$menu">
				<xsl:choose>
					<xsl:when test="$type='vleft'">
						<xsl:apply-templates select="." mode="template-vleft">
							<xsl:with-param name="_menu" select="$menu[@default='true' or position()=1]" />
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy>
							<xsl:apply-templates select="@*" mode="template" />
							<xsl:apply-templates select="$menu[@default='true' or position()=1]" />
              <xsl:apply-templates select="node()" mode="template" />
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!--	Traitement particulier pour le menu de gauche : on le cache s'il doit
						être vide -->
			<xsl:when test="$type='vleft'" />
			<xsl:otherwise>
				<!--	Pas de menus correspondant dans le fichier de configuration des
							menus : on recopie le code du modèle -->
				<xsl:apply-templates select="node()" mode="template" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<!--+
			|	Construction XHTML d'un menu
			+-->
	<xsl:template match="menu">
		<xsl:choose>
			<xsl:when test="parent::menu">
				<li>
					<xsl:apply-templates select="." mode="menu-as-item"/>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="." mode="menu-as-item"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="menu" mode="menu-as-item">
		<ul>
			<xsl:if test="@id!=''">
				<xsl:attribute name="id"><xsl:value-of select="concat($menu-id-prefix, @id)"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@class!=''">
				<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates />
		</ul>
	</xsl:template>

	<!--+
			|	Traitement générique d'une entrée de menu
			|	1) on active l'entrée si nécessaire
			|	2) on applique un traitement spécifique au type d'entrée
			|	3) on traite éventuellement un regroupement (ie, une entrée contenant
			|			d'autres entrées)
			+-->
	<xsl:template match="item">

		<!--	Définir si l'entrée est restreinte à un ou plusieurs rôles.
					Si l'item courant est restreint à un ou plusieurs rôles
							Si l'utilisateur ne possède pas de rôle, l'item est caché
							Si l'utilisateur possède l'un des rôles requis, l'item est affiché
							Si l'utilisateur ne possède pas le rôle requis, l'item est caché
					Sinon, il est affiché -->
		<!-- On ajoute à cela la possibilité de restreindre un menu uniquement
				pour un patron d'URL, depuis l'attribut @reserve-uri -->
        <xsl:variable name="r" select="replace(@reserve,'\s','')" />
        <xsl:variable name="ru" select="replace(@reserve-uri,'\s','')"/>

        <xsl:variable name="test">
            <xsl:choose>
                <xsl:when test="$r!=''"><!-- Une restriction sur rôle -->
                    <xsl:choose>
                        <xsl:when test="$roles=''"/>
                        <!-- L'utilisateur n'a pas de rôle : on cache -->
                        <xsl:when test="tokenize($roles, ',') = tokenize($r, ',')"><!-- L'utilisateur a plusieurs rôles et en possède un qui convient : on ne cache pas -->
                            <xsl:text>false</xsl:text>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$ru != ''"><!-- Une restriction sur URL -->
                    <xsl:analyze-string select="$sitemapURI" regex="{$ru}">
                        <xsl:matching-substring>
                            <xsl:text>false</xsl:text>
                        </xsl:matching-substring>
                        <xsl:non-matching-substring/>
                        <xsl:fallback/>
                    </xsl:analyze-string>
                </xsl:when>
                <xsl:otherwise><!-- Pas de restriction -->
                    <xsl:text>false</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="hidde">
            <xsl:choose>
                <xsl:when test="contains($test, 'false')">
                    <xsl:text>false</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>true</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

<!--<xsl:message>
Restriction item :
            id = <xsl:value-of select="@id" />
       reserve = <xsl:value-of select="$r" />
   reserve-uri = <xsl:value-of select="$ru" />
    user roles = <xsl:value-of select="$roles" />
          test = <xsl:value-of select="$test" />
         hidde = <xsl:value-of select="$hidde" />
</xsl:message>-->

    <xsl:if test="$hidde='false'">

        <li>
            <xsl:apply-templates select="." mode="doActivate" />
            <xsl:apply-templates select="." mode="specific" />
            <xsl:if test="item">
                <ul>
										<xsl:variable name="iid" select="if (@id != '') then concat(@id, '-subs') 
																										else if (item[1]/@id!='') then concat(item[1]/@id, '-prt-subs')
																										else ''"/>
										
										<xsl:if test="$iid!=''">
											<xsl:attribute name="id"><xsl:value-of select="concat($menu-id-prefix, $iid)"/></xsl:attribute>
										</xsl:if>
                    <xsl:apply-templates select="item" />
                </ul>
            </xsl:if>
        </li>

    </xsl:if>

    </xsl:template>

	<!--+
			|	Traitement spécifique d'une entrée de menu de type 'grp'
			|	On traite les sous-items.
			+-->
	<xsl:template match="item[@type='grp']">
		<xsl:if test="item">
			<ul class="pl-menu-grp-item">
				<xsl:if test="@id != ''">
					<xsl:attribute name="id"><xsl:value-of select="concat($menu-id-prefix, @id)"/></xsl:attribute>
				</xsl:if>
				<xsl:apply-templates select="item" />
			</ul>
		</xsl:if>
	</xsl:template>

	<!--+
			|	Traitement spécifique d'une entrée de menu de type 'txt'
			|	On appel simplement le contenu, sauf d'éventuels éléments item.
			+-->
	<xsl:template match="item[@type='txt']" mode="specific">
		<span class="pl-menu-item-txt">
			<xsl:apply-templates select="node()[not(self::item)]" />
		</span>
	</xsl:template>

	<!--+
			|	Traitement spécifique d'une entrée de menu de type
			|	'link-normal', 'link-window', 'link-ead'
			+-->
	<xsl:template match="item[starts-with(@type, 'link-')]" mode="specific">

		<!-- Types d'entrée 'link' : 'normal' ; 'window' ; 'ead' -->
		<xsl:variable name="_t" select="substring-after(@type, 'link-')" />
		<xsl:variable name="type">
			<xsl:choose>
				<xsl:when test="$_t='normal'"><xsl:text>normal</xsl:text></xsl:when>
				<xsl:when test="$_t = 'ead'"><xsl:text>ead</xsl:text></xsl:when>
				<xsl:when test="starts-with($_t, 'window')"><xsl:text>window</xsl:text></xsl:when>
				<xsl:otherwise><xsl:text>normal</xsl:text></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="href">
			<xsl:choose>
				<xsl:when test="$type='normal' or $type='window'">
					<xsl:choose>
						<!-- FIXME: Faut-il supporter d'autres protocols ? [MP] -->
						<xsl:when test="starts-with( @href, 'http:' ) or starts-with(@href, 'mailto:')">
							<xsl:value-of select="@href" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat($baselink,@href)" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$type='ead'">
					<xsl:value-of select="concat($baselink,'ead.html?id=', @href)"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<!-- <span class="pl-menu-item pl-menu-item-link"> -->
			<a href="{$href}" class="pl-menu-item">

				<xsl:if test="$type='window'">

					<xsl:variable name="nom" select="substring-after($_t, ':')" />
					<xsl:variable name="target">
						<xsl:choose>
							<xsl:when test="$nom!=''">
								<xsl:value-of select="$nom" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>_new</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<!--<xsl:attribute name="target">
						<xsl:value-of select="$target" />
					</xsl:attribute>-->

					<xsl:attribute name="onclick">
						<xsl:text>javascript: return windowManager.winFocus( this.href, '</xsl:text>
						<xsl:value-of select="$target" />
						<xsl:text>' );</xsl:text>
					</xsl:attribute>

				</xsl:if>

				<xsl:if test="$type='ead'">
					<xsl:attribute name="onclick">return windowManager.winFocus(this.href, 'docead')</xsl:attribute>
				</xsl:if>

				<!-- on appelle le traitement du contenu, sauf d'éventuels éléments item -->
				<xsl:apply-templates select="@title|@onclick|@i18n:*|node()[not(self::item)]" />

			</a>
		<!-- </span> -->

	</xsl:template>

	<!--+
			|	Construction d'une entrée de type recherche simple
			|	(item/@type='simple-search')
			+-->
	<xsl:template match="item[@type='simple-search']" mode="specific">

    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="@action!=''">
          <xsl:value-of select="@action" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>results.html</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="method">
      <xsl:choose>
        <xsl:when test="@method!=''">
          <xsl:value-of select="@method" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>get</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
		<xsl:variable name="field" select="if (@field != '') then @field else 'fulltext'"/>

		<div class="pl-menu-frm-search">
			<form action="{$baselink}{$action}" method="{$method}">
        <xsl:if test="@base!=''">
					<xsl:for-each select="tokenize(@base, ' ')">
						<input type="hidden" name="base" value="{.}" />
					</xsl:for-each>
        </xsl:if>
				<input type="hidden" name="champ1" value="{$field}"/>
				<input type="hidden" name="op1" value="AND"/>
				<input type="hidden" name="search_type" value="simple"/>
				<input	size="18" name="query1" type="text" class="pl-npt-txt"
								title="module-eadeac:simple-search.input.title" i18n:attr="title"/>
				<input	type="image" name="ssearch-submit-npt" id="ssearch-submit-npt"
								title="module-eadeac:simple-search.submit.title" class="pl-npt-img"
								src="{$themepath}images/input-search.png" i18n:attr="title"/>
				<label for="ssearch-submit-npt" class="access">
					<i18n:text catalogue="module-eadeac" key="simple-search.submit.label">
						Lancer la recherche
					</i18n:text>
				</label>
			</form>
		</div>

	</xsl:template>

	<!--+
			|	Contextualise le menu actif si nécessaire
			|	Suivant le modèle courant, il s'agit de définir une classe CSS 'current'
			+-->
	<xsl:template match="item" mode="doActivate">

		<xsl:param name="class" select="'current'" />

		<xsl:if test="@context!=''">

			<xsl:variable name="activate">
				<xsl:apply-templates select="." mode="is-active" />
			</xsl:variable>

			<xsl:if test="$activate='true'">
				<xsl:attribute name="class">
					<xsl:value-of select="$class" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$activate='todo'">
				<xsl:attribute name="onclick">
					<xsl:text>javascript: alert('TODO: </xsl:text><xsl:value-of select="normalize-space(@context)" /><xsl:text>');</xsl:text>
				</xsl:attribute>
			</xsl:if>

		</xsl:if>

	</xsl:template>

	<!--+
			|	Retourne 'true' si le menu courant est actif
			+-->
	<xsl:template match="item" mode="is-active">

		<xsl:variable name="ctxt" select="@context" />

		<!-- Règle déterminant la contextualisation ou non -->
		<xsl:variable name="activate">

			<xsl:if test="$ctxt!=''">

				<!-- Sans information de contextualisation, on ne peut rien faire -->

          <xsl:choose>
            <xsl:when test="$ctxt='@href'">
						<xsl:choose>
						  <xsl:when test="contains(@href, '?') and @href=concat($sitemapURI, '?', $queryString)"><xsl:text>true</xsl:text></xsl:when>
						  <xsl:when test="@href=$sitemapURI"><xsl:text>true</xsl:text></xsl:when>
							<xsl:otherwise><xsl:text>false</xsl:text></xsl:otherwise>
          </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
						<xsl:variable name="m" select="if(contains($ctxt, '?')) then concat($sitemapURI,'?',$queryString) else $sitemapURI" />
						<xsl:analyze-string select="$m" regex="{$ctxt}">
							<xsl:matching-substring><xsl:text>true</xsl:text></xsl:matching-substring>
							<xsl:non-matching-substring><xsl:text>false</xsl:text></xsl:non-matching-substring>
							<xsl:fallback><xsl:text>false</xsl:text></xsl:fallback>
						</xsl:analyze-string>
            </xsl:otherwise>
          </xsl:choose>

			</xsl:if>

		</xsl:variable>

		<xsl:value-of select="$activate" />

<!--<xsl:if test="$ctxt!='' and ancestor::menu/@type='vleft'">
<xsl:message>
[MENUS] isActive = <xsl:value-of select="$activate" />
item/@id  = <xsl:value-of select="@id" />
@context  = <xsl:value-of select="@context" />
<xsl:if test="@context='@href'">
@href     = <xsl:value-of select="@href" />
</xsl:if>
sitemapURI= <xsl:value-of select="$sitemapURI" />
</xsl:message>
</xsl:if>-->

	</xsl:template>

	<!--+
			|	Traitement du @title d'une entrée de menu
			+-->
	<xsl:template match="item/@title">

		<xsl:if test=".!=''">
			<xsl:attribute name="title">
				<xsl:value-of select="." />
			</xsl:attribute>
		</xsl:if>

	</xsl:template>

	<xsl:template match="item/@onclick">
		<xsl:if test=".!=''">
			<xsl:attribute name="onclick">
				<xsl:value-of select="." />
			</xsl:attribute>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
