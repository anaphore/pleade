/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.StringTokenizer;

import org.xml.sax.Attributes;

/**
 * Utility methods for PLEADE-related information and processing.
 */
public class PleadeUtilities {

    /** The PLEADE namespace URI */
    public static String PLEADE_NAMESPACE_URI = "http://pleade.org/ns/pleade/1.0";


    /** The normal PLEADE namespace prefix */
    public static String PLEADE_NAMESPACE_PREFIX = "pleade";

    /**
     * Returns the PLEADE identifier (pleade:id) for an element.
     *
     * @param	atts		The attributs of the element
     *
     * @return	The identifier, or an empty string if the identifier is not defined
     */
    public static String getPleadeId(Attributes atts) {
        if ( atts == null ) return "";
        String att = atts.getValue(PLEADE_NAMESPACE_URI, "id");
        if ( att == null ) return "";
        else return att;
    }

    /**
     * Indique si l'element courant est une racine documentaire pour Pleade.
     *
     * @param String localName Le nom de l'element sur lequel porte le test.
     * Ce nom doit etre un nom local (pas de prefixe d'espace de nom).
     * @return boolean Retourne <code>true</code> si l'element courant est <code>ead</code>,
     * <code>tei</code> ou <code>tei.2</code>.
     * <br/>SInon retourne faux.
     */
    public static boolean isRoot(String localName) {
    	if (localName==null || "".equals(localName)) return false;
    	if ("ead".equals(localName)) return true;
    	if ("eac".equals(localName)) return true;
			if ("eac-cpf".equals(localName)) return true;
    	if ("TEI".equals(localName)) return true;
    	if ("TEI.2".equals(localName)) return true;
    	if ("teiCorpus".equals(localName)) return true;
    	return false;
    }

    /**
     * Returns the type of the document.
     * <p>The element to test must be root.</p>
     * @param localName The local name of the element as a String.
     * @return The document type as a String depending of the local name:
     *         <ul>
     *         <li><code>ead</code> if the local name is <code>ead</code></li>
     *         <li><code>eac</code> if the local name is <code>eac</code></li>
		 *         <li><code>eac</code> if the local name is <code>eac-cpf</code></li>
     *         <li><code>tei</code> if the local name is <code>TEI</code></li>
     *         <li><code>tei</code> if the local name is <code>TEI.2</code></li>
     *         <li><code>tei</code> if the local name is <code>teiCorpus</code></li>
     *         <li>the local name itself in others cases</li>
     *         </ul>
     */
    public static String getDocumentType(String localName) {
      if ( localName==null || "".equals(localName) ) return "";
      if ( "ead".equals(localName) ) return "ead";
      if ( "eac".equals(localName) ) return "eac";
			if ( "eac-cpf".equals(localName) ) return "eac";
      if ("TEI".equals(localName)) return "tei";
      if ("TEI.2".equals(localName)) return "tei";
      if ("teiCorpus".equals(localName)) return "tei";
      return localName;
    }


    /**
     * Returns true if the element have a pleade:break attribute equals to <code>true</code>
     * or if it's a forced pleade:break element (eg, <code>eadheader</code>., <code>frontmatter</code>, etc.)
     *
     * @param	localName		The local name of the element
     * @param	atts		The attributs of the element
     *
     * @return	boolean
     * @see PleadeUtilities#isPleadeBreak(String,String,Attributes)
     */
    public static boolean isPleadeBreak(String localName, Attributes atts) {
    	return PleadeUtilities.isPleadeBreak("ead",localName,atts);
    }
    /**
     * Returns true if the element have a pleade:break attribute equals to <code>true</code>
     * or if it's a forced pleade:break element
     *
     * @param	documentType		The type of the document containing the element
     * @param	localName		The local name of the element
     * @param	atts		The attributs of the element
     *
     * @return	boolean
     * @see PleadeUtilities#isForcedPleadeBreak(String,String,Attributes)
     */
    public static boolean isPleadeBreak(String documentType, String localName, Attributes atts) {
    	if ( isForcedPleadeBreak(documentType, localName) ) return true;
    	if ( atts == null ) return false;
    	// Java 1.6 else return Boolean.parseBoolean( atts.getValue(PLEADE_NAMESPACE_URI, "break") );
    	else return Boolean.valueOf( atts.getValue(PLEADE_NAMESPACE_URI, "break") ).booleanValue();
    }

    /**
     * Returns true if the element is a forced pleade:break element.
     * @param	localName		The local name of the element
     *
     * @return	boolean
     * @see #isForcedPleadeBreak(String,String)
     */
    public static boolean isForcedPleadeBreak(String localName){
      return PleadeUtilities.isForcedPleadeBreak("ead",localName);
    }

    /**
     * Returns true if the element is a forced pleade:break element.
     *
     * @param	documentType		The type of the document containing the element
     * @param	localName		The local name of the element
     *
     * @return	boolean
     * @see #isForcedPleadeBreak(String,String)
     * @see EadUtilities#isForcedPleadeBreak(String)
     * @see TeiUtilities#isForcedPleadeBreak(String)
     */
    public static boolean isForcedPleadeBreak(String documentType, String localName) {
      if ( documentType==null || "".equals(documentType) ) return false;
      if ( "ead".equals(documentType) ) return EadUtilities.isForcedPleadeBreak(localName);
      if ( "tei".equals(documentType) ) return TeiUtilities.isForcedPleadeBreak(localName);
    	if ( localName==null || "".equals(localName) ) return false;
    	else return false;
    }

    /**
     * Verifie un dossier. S'il n'existe pas, il va tenter de le creer.
     * @param writeable Si vrai, on doit verifier qu'on peut y ecrire egalement.
     * @throws IOException Si le dossier ne peut pas etre cree et/ou le dossier ne peut etre ecrit.
     */
    public static void checkDir(File dir, boolean writeable) throws IOException {
        if ( dir == null ) throw new IOException("Impossible de lire le dossier " + dir);
        if ( !dir.exists() )
            if (!dir.mkdirs()) throw new IOException("Impossible de creer le dossier " + dir.getAbsolutePath());
        if ( writeable && !dir.canWrite()) throw new IOException("Impossible d'ecrire dans le dossier " + dir.getAbsolutePath());
    }



    /**
     * Retoure la liste des dossiers racines.
     * Les dossiers ne sont pas verifies pour leur existance, etc.
     * @param racines   Les racines sous la forme d'une chaine de caracteres, separees par FILE.PATH_SEPARATOR.
     * @return  La liste des racines, ou null s'il y a un probleme.
     */
    public File[] getRootDirectories(String racines) {

        // On recupere les parties
        String parties[] = splitRoots(racines);

        // S'il y a eu erreur on retourne null
        if (parties == null) return null;

        // On fabrique le tableau de sortie
        File ret[] = new File[parties.length];
        for (int i = 0; i < parties.length; i++) ret[i] = new File(parties[i]);

        // Que l'on retourne
        return ret;
    }

    /**
     * Separe la liste des racines en fonction du File.pathSeparator.
     * @param racines   La liste des racines.
     * @return          La liste des racines sous la forme d'un tableau de String
     */
    public String[] splitRoots(String racines) {

        // On s'assure d'avoir un contenu valide.
        if (racines == null) return null;

        // On separe la chaine de caracteres
        StringTokenizer stringtokenizer = new StringTokenizer(racines, File.pathSeparator);

        // On fabrique le tableau de sortie
        String ret[] = new String[stringtokenizer.countTokens()];
        for (int i = 0; stringtokenizer.hasMoreTokens(); i++) ret[i] = stringtokenizer.nextToken();

        // Que l'on retourne...
        return ret;
    }

}
