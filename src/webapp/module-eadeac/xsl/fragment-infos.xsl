<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: infosimage.xsl 19547 2010-10-14 11:42:52Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Affichage d'informations sur le fragment (depuis la visionneuse).
    | Par défaut, on va aller chercher le titre de l'IR ; mais il est ausis possible,
    | via le mode "extended" de récupérer plus di'nformations sous forme de tableau.
    +-->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
    xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    exclude-result-prefixes="xsl sdx xhtml">

  <xsl:include href="ead-xslt/html/ead2html.xsl"/>

  <xsl:param name="image_path" select="''"/>
  <xsl:param name="from" select="''"/>
  <xsl:param name="baselink" select="''"/>

  <xsl:variable name="mode" select="'dyn'"/>

  <!--+
      | Traitement de la racine
      +-->
  <xsl:template match="/" priority="+2">
    <xsl:apply-templates select="root/fragment"/>
  </xsl:template>

  <xsl:template match="fragment">
    <xsl:apply-templates select="sdx:document/pleade:subdoc/*"/>
  </xsl:template>

  <xsl:template match="archdesc | c | c01 | c02 | c03 | c04 | c05 | c06 | c07 | c08 | c09 | c10 | c11 | c12" priority="+2">
    <xsl:call-template name="make-component-block"/>
  </xsl:template>

  <!-- On en sort rien pour les DAO -->
  <xsl:template match="archdesc/daogrp | c/daogrp | c01/daogrp | c02/daogrp | c03/daogrp | c04/daogrp | c05/daogrp | c06/daogrp | c07/daogrp | c08/daogrp | c09/daogrp | c10/daogrp | c11/daogrp | c12/daogrp" priority="+2"/>
  <xsl:template match="bioghist/daogrp | odd/daogrp | scopecontent/daogrp" priority="+2"/>
  <xsl:template match="archref/daogrp" priority="+2"/>
  <xsl:template match="daodesc" mode="inline" priority="+2"/>
  <xsl:template match="dao" name="process-dao" priority="+2"/>

<!--    <xsl:template match="fragment">
      <fragment>ok</fragment>
    </xsl:template>-->

    <xsl:template match="sdx:result">
      <table>
        <caption><i18n:text key="">Informations sur le document</i18n:text></caption>
        <tr>
          <th>cote</th>
          <td><xsl:value-of select="sdx:field[@name='uunitid']"/></td>
        </tr>
        <tr>
          <th>titre</th>
          <td><xsl:value-of select="sdx:field[@name='fucomptitle']"/></td>
        </tr>
        <xsl:if test="sdx:field[@name='unitdate']">
          <tr>
            <th>date</th>
            <td><xsl:value-of select="sdx:field[@name='udate']"/></td>
          </tr>
        </xsl:if>
        <xsl:apply-templates select="sdx:result" mode="more-extended-results"/>
      </table>
    </xsl:template>

    <!-- Template vide destiné à ajouter facilement d'autres informations. On attent ici une succession de lignes de tableau HTML -->
    <xsl:template match="sdx:result" mode="more-extended-results"/>

</xsl:stylesheet>
