//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du contenu de la fenetre EAD
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion du contenu de la fenetre EAD
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var EadContentManager = new Class(/** @lends EadContentManager.prototype */{
	/**
	* @private
	* @type String
	*/
	base: '',

	/**
	* @private
	* @type String
	*/
	mode: '',

	/**
	* @private
	* @type String
	*/
	qid: '',

	/**
	* @private
	* @type String
	*/
	archdescID: '',

	/**
	* @private
	* @type String
	*/
	_base: '',

	/**
	* <strong>Constructor</strong><br/> 
	* @param {EadWindow} eadWindow La fenêtre ead que l'on gère
	*/
	initialize: function(eadWindow){
		this.eadWindow = eadWindow;
		
		// recuperation des variables globale
		this.eadid = eadid;
		this.mode = mode;
		this.qid = qid;
		this.archdescID = archdescID;
		this._base = 'ead';
		this.firstCall = true;
		this.currentDocumentId = null;
		this.lastDocumentId = null;
		/* code inutile ??? */
		 try {
		      if (base && base !== '') 
		            this._base = base;
		    } 
		    catch (e) {
		    }
		/* ???? code inutile ??? */
		
		this.loadingManager = new Loading($('pl-pg-body-main-mcol'));
		this.loadingManager.autoDestroy = true;
	},

	/**
	* Retourne la variable base 
	* @returns {String} valeur de la base
	*/
	getBase: function(){
		return this._base;
	},

	/**
	* Renvoie le mode :soit dyn (pour dynamic) ou static (variable globale)
	* @returns {String} mode courant
	*/
	getMode: function(){
		return this.mode;
	},

	/**
	* Retourne le qid (variable globale)
	* @returns {String} qid de la requête
	*/
	getQid: function(){
		return this.qid;
	},

	/**
	* Retourne l'EAD ID (variable globale)
	* @returns {String} identifiant du document EAD
	*/
	getEadId: function(){
		return this.eadid;
	},

	/**
	* Retounr le archdescID (variable globale)
	* @returns {String} retourne le archdescID
	*/
	getArchdescID: function(){
		return this.archdescID;
	},

	/**
	* Fonction permettant de charger le contenu de la fenetre ead.
	* @param {String} defaultFragmentID Id du document par defaut
	* @param {Boolean} checkC Si il est a true on parse l'url de la fenetre pour recuperer la variable C et on affichera le document concerné
	* @param {Object} qid
	*/
	loadContent: function(defaultFragmentID, checkC, qid){
		this.loadHContent(defaultFragmentID, checkC, qid);
	},
	
	/**
	* Fonction permettant de charger le contenu de la fenetre ead.
	* @param {String} defaultFragmentID Id du document par defaut
	* @param {Boolean} checkC Si il est a true on parse l'url de la fenetre pour recuperer la variable C et on affichera le document concerné
	* @param {Object} qid 
	*/
	loadHContent: function(defaultFragmentID, checkC, qid, onCompleteFn){
		// une variable qui contient la partie de querystring sur le qid (est vide si qid vide)
		var qidParam = '';
		if (qid != null && !qid.blank())
			qidParam = '&qid=' + qid;

		this.currentDocumentId = this.getfragmentID(defaultFragmentID, checkC);
		if(this.lastDocumentId != this.currentDocumentId){
			this.lastDocumentId = this.currentDocumentId;
			// L'URL à appeler diffère selon qu'on est en statique ou dynamique
			var url = '';
			if (this.getMode() == 'static') {
				url = 'fragments/' + this.getfragmentID(defaultFragmentID, checkC) + '.html' + qidParam;
			}else{
				url= this.getBase() + '-fragment.xsp?c=' + this.getfragmentID(defaultFragmentID, checkC) + qidParam;
			}
		 
			this.updateAjaxContent(url,$('pl-pg-body-main-mcol'),onCompleteFn);
		}
	},

	/**
	* Fonction permettant de charger le contenu de la fenetre EAD.
	* Fonction appelée depuis l'index ou depuis un résultat de recherche contenu dans la fenêtre EAD
	* @param {Object} url
	*/
	loadFragment: function(url){
		this.loadHFragment(url);
	},

	/**
	* Fonction permettant de charger le contenu de la fenetre EAD.
	* Fonction appelée depuis l'index ou depuis un résultat de recherche contenu dans la fenêtre EAD
	* @param {Object} url
	*/
	loadHFragment: function(url){
		this.currentDocumentId = this.getFragmentIdFromUrl(url);
		this.updateAjaxContent(url,$('pl-pg-body-main-mcol'));
	},

  /**
  * Récupération de l'id du document qui va être affiché depuis une URL
  * @param {String} url URL que l'on va parser
  */
  getFragmentIdFromUrl: function(url){
    //une URL complète ne fonctionne pas ; on ne veut que la queryString
    var _url = url.substring(url.indexOf('?'), url.length)
    return this.getfragmentID(null, true, _url);
  },

	/**
	* Récupération de l'id du document à afficher
	* @param {Object} defaultFragmentID
	* @param {Boolean} checkC
	* @param {String} queryString queryString (uniquement !) a parser. Si la valeur est null ou n'est pas définie on prendra celle renvoiyée par le navigateur
	*/
	getfragmentID: function(defaultFragmentID, checkC, queryString){
		if ( !checkC ) {
	        	return defaultFragmentID;
		}
		var _queryString = (queryString) ? queryString : location.search.substring(1);
		var query = _queryString.parseQueryString();
		if ( query['c'] == undefined ) {
			return defaultFragmentID
		} else {
			return query['c']
		}
	},

	/** Action realisée au debut du chargement d'un fragment*/
	startLoading: function(){
		this.loadingManager.start();
	},

	/** Action realisée à la fin du chargement d'un fragment*/
	stopLoading: function(){
		try {
			this.loadingManager.stop();
		} catch (e) {}
		this.initAcronyms();
		try {
			this.pageLoaded(); //Pagination links has been reloaded, we have to rebind them
		} catch (e) {}
		this.eadWindow.adjustHeight(false);
	},
	
	/** Initialisation des tooltips pour les éléments html acronym */
	initAcronyms: function(){
		$$('acronym').each(function(item){
			var tooltip = new YAHOO.widget.Tooltip('myTooltip', { context:item, width:250} );
		});
	},

	/**
	* Applications des évènements sur les liens de pagination
	* @param {MooTools.Event} e
	*/
	pageLoaded: function(e){
		//chargement en Ajax des pages des résultats de recherche
		if ($(this.getEadId() + '_pagination')) {
			$$('#' + this.getEadId() + '_pagination a').each(function(n){
				$(n).addEvent('click', eadWindow.contentManager.ajaxPagination);
			});
		}
		//chargement en ajax de liens pour naviguer dans les résultats de recherche
		if ($(this.getEadId() + '_browse')) {
			$$('#' + this.getEadId() + '_browse a').each(function(n){
				$(n).addEvent('click', eadWindow.contentManager.ajaxBrowse);
			});
		}
	},

	/**
	* Chargement en Ajax de la pagination des résultats de recherche
	* @param {MooTools.Event} e
	*/
	ajaxPagination: function(e){
		this.startLoading();
		var _url = 'doc-window/sdx-results.ajax' + this.href.substr(this.href.indexOf('?'), this.href.length) + '&mode=ead-viewer';
		this.ajaxHPagination(_url);
		Event.stop(e);
	},

	/**
	* Chargement en Ajax de la pagination des résultats de recherche
	* @param {String} url Url utilisée pour la pagination
	*/
	ajaxHPagination: function(url){
		this.updateAjaxContent(url,$('pl-pg-body-main-mcol'));
	},

	/**
	* Chargement en ajax de liens pour naviguer dans les résultats de recherche
	* @param {MooTools.Event} e
	*/
	ajaxBrowse: function(e){
		eadWindow.getContentManager().startLoading();
		var req = new Request.HTML({
			method: 'get',
			url: this.href.replace(/\.html/g, eadWindow.getContentManager().getBase() + '-fragment.xsp'),
			update: $('pl-pg-body-main-mcol'),
			evalResponse: true,
			onSuccess: function(){
				eadWindow.getContentManager().stopLoading();
			}
		});
		req.send();
		Event.stop(e);
	},

	/**
	* Fonction qui permet la recherche depuis la fenêtre EAD
	* À la place d'envoyer le formulaire, on rafaîchit simplement
	* par ajax le 'pl-pg-body-main-mcol' avec les résultats simples
	* @param {Element} objForm Le formulaire
	*/
	sendEadForm: function(objForm){
		var url = objForm.action.replace(/\.html/g, '.ajax') + '?r=' + eadid + '&mode=ead-viewer';
		url += '&'+$(objForm).toQueryString();
		return this.sendHSearch(url);
	},

	/**
	* Fonction qui permet la recherche depuis la fenêtre EAD
	* À la place d'envoyer le formulaire, on rafaîchit simplement
	* par ajax le 'pl-pg-body-main-mcol' avec les résultats simples
	* @param {String} url L'url employée pour la recherche
	*/
	sendHSearch: function(url){
		this.currentDocumentId = this.getFragmentIdFromUrl(url);
		this.updateAjaxContent(url,$('pl-pg-body-main-mcol'));
		return false; // ne pas valider le formulaire
	},
	
	/**
	* Appel ajax permettant de mettre a jour le div contenant le document
	* @param {String} url URL du document
	* @param {Element} div div contenant le document
	* @param {Function} fnOnComplete Fonction executée après le chargement du fragment
	*/
	updateAjaxContent: function(url, div, fnOnComplete){
		if ( !fnOnComplete ) {
			fnOnComplete = $empty;
		}
		this.startLoading();
		div.fade('out');
		var eadContentManager = this;
		var req = new Request.HTML({
			method: 'get',
			url: url,
			update: div,
			evalResponse: true,
			onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){ //on ne peut pas appeler la fonction stopLoading directement sinon il y a un problème de calcul de du contenu
					eadContentManager.stopLoading();
					div.fade('in');
					fnOnComplete();
			},
			onFailure: function(xhr){
				alert('Erreur de chargement: '+xhr.statusText); //TODO: i18n
				div.fade('in');
				eadContentManager.stopLoading();
			}
		});
		req.send();
	},

	/**
	* Création d'ancres nommées
	* @param {String} _id L'id de l'ancre de destination
	*/
	processAnchor: function(_id){
		var _top = 0;
		if ( _id ) {
			_top = $(_id).offsetTop;
		}
		if( $('pl-pg-body-tool-box') ) {
			_top -= $('pl-pg-body-tool-box').getHeight();
		}
		var attributes = { 
			scroll: { to: [0, _top] } 
		}; 
		var anime = new YAHOO.util.Scroll( 'pl-pgd-cnt', attributes, 0 );
		anime.animate();
	},

  /**
  * Remplace le message d'erreur de base de FlowPlayer
  * @param {String} type le type (audio ou video)
  */
  isFlashError: function(type){
    $$('a.pl-flow-' + type).each(function(e){
      var clone = $('flashError').clone(true, true).replaces(e);
    });
  }
});