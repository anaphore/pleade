/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.transformation.AbstractTransformer;
import org.xml.sax.SAXException;

/**
 * Supprime des prefixes de namespace.
 * TODO: rendre configurable pour eviter de toujours calculer les parametres.
 * TODO: rendre cachable?
 */
public class NamespacePrefixStripper extends AbstractTransformer   {
    
    public static final String PARAMETER_PREFIXES = "prefixes";
    
    private Hashtable prefixes;
    
    
    /**
     * Mise en place lors de l'appel : conserve le parametre indiquant quels prefixes supprimer.
     */
    public void setup(SourceResolver resolver, Map map, String src, Parameters params) throws ProcessingException, SAXException, IOException {
        String p = params.getParameter(PARAMETER_PREFIXES, null);
        prefixes = new Hashtable();
        if (p != null) {
            StringTokenizer st = new StringTokenizer(p, " ");
            while (st.hasMoreTokens()) {
                String pp = st.nextToken();
                prefixes.put(pp, pp);
            }
        }
    }


    public void endPrefixMapping(String prefix) throws SAXException {
        if ( prefixes.get(prefix) == null ) super.endPrefixMapping(prefix);
    }


    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        if ( prefixes.get(prefix) == null ) super.startPrefixMapping(prefix, uri);
    }
    
    

}
