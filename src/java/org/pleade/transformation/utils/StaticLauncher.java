/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
/**
 * Classe utilisee pour lancer les transformateurs de maniere
 * statique, c'est-a-dire hors du contexte Pleade.
 */
package org.pleade.transformation.utils;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.avalon.framework.parameters.Parameters;
import org.pleade.transformation.PositionTransformer;
import org.pleade.transformation.PropertiesWriter;
import org.pleade.transformation.index.IndexBuilder;

public class StaticLauncher {
    /** Le nom du parametre des XSLT pour indiquer un traitement statique. */
    private final static String PARAM_NAME_MODE = "mode";
    /** Le nom du parametre des XSLT pour indiquer le dossier de sortie. */
    private final static String PARAM_NAME_DATA_OUTPUT_DIR = "data-output-dir";
    /** La valeur du parametre des XSLT pour indiquer un traitement statique. */
    private final static String PARAM_VALUE_MODE_STATIC = "static";
    /**
     * Lancement des transformations de maniere statique.
     * @param args  Arguments qui parametrent la transformation (voir ci-dessous)
     *
     * Les argument sont, dans l'ordre:
     *
     *      Fichier EAD a traiter
     *      Dossier de sortie
     *      Fichier de parametres
     *      Dossier ou se trouvent les transformations
     */
    public static void main(String[] args) throws Exception {
        /**
         * Les etapes de transformations sont celles de Pleade 3:
         *
         *  1) org.pleade.transformation.PositionTransformer
         *  2) attributes.xsl
         *  3) org.pleade.transformation.PropertiesWriter
         *  4) fragmentation.xsl
         *  5) links.xsl
         *  6) org.pleade.transformation.index.IndexBuilder
         *  7) toc.xsl
         *
         */

        // On verifie s'il y a le bon nombre d'arguments
        int nbArguments = 4;
        if (args.length != nbArguments) fail();

        // On interprete chaque argument de la ligne de commande

        // Le fichier EAD a traiter
        File eadFile = getFile(args[0]);

        // Le dossier de sortie
        File baseOutputDir = getDir(args[1], true);

        // Le fichier de parametres, que l'on interprete tout de suite
        File paramFile = getFile(args[2]);

        // Le dossier ou se trouvent les XSLT
        File xsltDir = getDir(args[3], false);

        // On en deduit les differentes XSLT utiles
        File xsltStep2 = getFile(xsltDir, "attributes.xsl");
        File xsltStep4 = getFile(xsltDir, "fragmentation.xsl");
        File xsltStep5 = getFile(xsltDir, "links.xsl");
        File xsltStep7 = getFile(xsltDir, "toc.xsl");

        // Etape 1 : PositionTransformer (SAX)
        PositionTransformer step1 = new PositionTransformer();

        // Etape 2 : attributes.xsl (XSLT)
        SAXTransformerFactory stf = (SAXTransformerFactory)SAXTransformerFactory.newInstance();
        TransformerHandler step2 = stf.newTransformerHandler(new StreamSource(xsltStep2));
        step1.setContentHandler(step2);

        // Etape 3 : PropertiesWriter (SAX)
        PropertiesWriter step3 = new PropertiesWriter();
        Parameters tempParams = new Parameters();
        tempParams.setParameter(PropertiesWriter.PARAM_OUTPUT_DIR, baseOutputDir.getAbsolutePath());
        tempParams.setParameter(PropertiesWriter.PARAM_CONFIGURATION_FILE, paramFile.getAbsolutePath());
        step3.setParameters(tempParams);
        step2.setResult(new SAXResult(step3));

        // Etape 4 : fragmentation.xsl (XSLT)
        TransformerHandler step4 = stf.newTransformerHandler(new StreamSource(xsltStep4));
        step4.getTransformer().setParameter(PARAM_NAME_MODE, PARAM_VALUE_MODE_STATIC);
        step4.getTransformer().setParameter(PARAM_NAME_DATA_OUTPUT_DIR, baseOutputDir.toURL().toExternalForm());
        step3.setContentHandler(step4);

        // Etape 5 : links.xsl (XSLT)
        TransformerHandler step5 = stf.newTransformerHandler(new StreamSource(xsltStep5));
        step5.getTransformer().setParameter(PARAM_NAME_MODE, PARAM_VALUE_MODE_STATIC);
        step5.getTransformer().setParameter(PARAM_NAME_DATA_OUTPUT_DIR, baseOutputDir.toURL().toExternalForm());
        step4.setResult(new SAXResult(step5));

        // Etape 6 : creation des index (SAX)
        IndexBuilder step6 = new IndexBuilder();
        tempParams = new Parameters();
        tempParams.setParameter(IndexBuilder.OUTPUTDIR_PARAMETER_NAME, baseOutputDir.getAbsolutePath());
        step6.setParameters(tempParams);
        step5.setResult(new SAXResult(step6));

        // Etape 7 : toc.xsl (XSLT)
        TransformerHandler step7 = stf.newTransformerHandler(new StreamSource(xsltStep7));
        step7.getTransformer().setParameter(PARAM_NAME_MODE, PARAM_VALUE_MODE_STATIC);
        step7.getTransformer().setParameter(PARAM_NAME_DATA_OUTPUT_DIR, baseOutputDir.toURL().toExternalForm());
        step6.setContentHandler(step7);

        // A la fin on ecrit la sortie dans un fichier temporaire
        TransformerHandler finalStep = stf.newTransformerHandler();
        File tmpOutputFile = File.createTempFile("doc-", ".xml", baseOutputDir);
        // On doit passer par un FileOutputStream car sinon le fichier ne peut pas etre detruit ulterieurement
        FileOutputStream fos = new FileOutputStream(tmpOutputFile);
        finalStep.setResult(new StreamResult(fos));
        step7.setResult(new SAXResult(finalStep));

        // On declenche le traitement en demarrant le parsing du fichier de depart
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = spf.newSAXParser();
        sp.getXMLReader().setFeature("http://xml.org/sax/features/namespaces", true);
        sp.getXMLReader().setFeature("http://xml.org/sax/features/namespace-prefixes", true);
        sp.parse(eadFile, new Handler2TransformationBridge(step1));

        // Puis on renomme le fichier de sortie
        String eadid = step3.getEadid();
        fos.close(); // Impossible de renommer si on ne ferme pas d'abord...
        tmpOutputFile.renameTo(new File(getDir(new File(baseOutputDir, eadid), true), eadid + ".xml"));
    }

    /**
     * Fin du programme, le message d'erreur sera des informations sur l'utilisation de la classe.
     */
    private static void fail() {
        fail("Utilisation : java org.pleade.transformers.StaticLauncher [fichier EAD a traiter] [dossier de sortie] [fichier de parametres] [dossier ou se trouvent les XSLT]");
    }

    /**
     * Fin du programme avec message d'erreur
     * @param message Le message a sortir sur la console
     */
    private static void fail(String message) {
        System.out.println("Erreur d'execution");
        System.out.println(message);
        System.exit(1);
    }

    /**
     * Retourne un objet File depuis un dossier et un nom ou un echec si le fichier ne peut etre lu
     * @param dir        Le dossier ou doit se trouver le fichier
     * @param name       Le nom du fichier
     */
    private static File getFile(File dir, String name) {
        dir = getDir(dir, false);
        File f = new File(dir, name);
        return getFile(f);
    }


    /**
     * Retourne le meme objet File mais seulement si on peut le lire
     * @param f L'objet File a verifier
     * @return
     */
    private static File getFile(File f) {
        if ( f == null ) fail("Impossible de lire le fichier " + f);
        if (!f.isFile() || !f.canRead()) fail("Impossible de lire le fichier " + f.getAbsolutePath());
        return f;
    }

    /**
     * Retourne un objet File depuis une chaine de caracteres ou un echec si le fichier ne peut etre lu
     * @param arg       Le nom du fichier
     */
    private static File getFile(String arg) {
        if ( arg == null ) fail("Impossible de lire le fichier " + arg);
        return getFile(new File(arg));
    }

    /**
     * Retourne un objet File depuis une chaine de caracteres ou un echec si le dossier ne peut etre lu ou ecrit
     * @param arg           Le nom du dossier
     * @param checkWrite    Si vrai, on verifie la possibilite d'ecrire
     */
    private static File getDir(String arg, boolean checkWrite) {
        if ( arg == null ) fail("Impossible de lire le dossier " + arg);
        return getDir(new File(arg), checkWrite);
    }

    /**
     * Retourne l'objet File passe en parametre mais seulement s'il existe,
     * s'il s'agit d'un dossier et eventuellement si on peut y ecrire.
     * @param dir   Le dossier a verifier
     * @param checkWrite    Si true, on doit pouvoir y ecrire
     * @return  Le meme objet File, ou un arret du traitement
     */
    private static File getDir(File dir, boolean checkWrite) {
        if ( dir == null ) fail("Impossible de lire le dossier " + dir);
        if (!dir.exists() && !dir.mkdirs() ) fail("Impossible de creer le dossier " + dir.getAbsolutePath());
        if (!dir.canRead()) fail("Impossible de lire le dossier " + dir.getAbsolutePath());
        if (checkWrite && !dir.canWrite()) fail("Impossible d'ecrire dans le dossier " + dir.getAbsolutePath());
        return dir;
    }

}
