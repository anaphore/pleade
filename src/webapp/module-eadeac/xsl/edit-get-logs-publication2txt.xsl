<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: edit-get-logs2xhtml.xsl 15636 2009-07-08 14:29:23Z mpichot $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		| Administration du module EAD-EAC
    | Exporter les logs de publication en format texte
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		xmlns:csv="http://apache.org/cocoon/csv/1.0"
		exclude-result-prefixes="xsl i18n xs csv">

  <xsl:import href="edit-get-logs-publication-cols-numbers.xsl"/>

  <!--+
      | Construction de la racine textuelle
      | Dans Cocoon, tout est XML, on doit avoir une racine XML temporaire que
      | le sérialiseur textuel supprimera.
      +-->
	<xsl:template match="/">

		<root-temp>
			<xsl:apply-templates select="csv:document" />
		</root-temp>

	</xsl:template>

	<!--+
	    | Match un csv:document, racine du document
	    +-->
	<xsl:template match="csv:document">
		<!-- La Liste des publications-->
		<xsl:variable name="publications" select="csv:record[csv:field = 'INDEXATION_INIT']" />
		<xsl:value-of select="concat('Nombre de Publication: ', count($publications))" />
		<!-- On s'appuie sur les "INDEXATION_INIT" uniquement car ce sont les seuls à être toujours présents, même si la publication n'a pas fonctionnée -->
		<xsl:for-each select="csv:record[csv:field = 'INDEXATION_INIT']">
			<xsl:apply-templates select="." mode="indexation-init">
				<xsl:sort select="csv:field[@number=$colID]"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>

	<!--+
	    | Match un csv:record : INDEXATION_INIT
	    +-->
	<xsl:template match="csv:record" mode="indexation-init">
		<!-- La clé de regroupement (ie, l'identifiant de la publication) -->
		<xsl:variable name="k" select="csv:field[@number=$colID]" />
		<!-- La liste des informations de cette publication -->
		<xsl:variable name="groupe" select="../csv:record[csv:field[@number=$colID] = $k]"/>

		<xsl:variable name="debut" select="csv:field[@number=$colStarts]"/>
		<xsl:variable name="fin" select="$groupe[csv:field[@number=$colStep]='INDEXATION_END']/csv:field[@number=$colEnds]"/>
		<xsl:variable name="dbid" select="csv:field[@number=$colDBID]"/>
		<xsl:variable name="pub-ok" select="if($fin!='') then true() else false()"/>
		<xsl:variable name="nbdocs" select="csv:field[@number=$colNbDocs]"/>
		<xsl:variable name="nbdocs-added" select="$groupe[csv:field[@number=$colStep]='DOCS_IDS']/csv:field[@number=$colNbDocsAdded]"/>
		<xsl:variable name="nbdocs-error" select="$groupe[csv:field[@number=$colStep]='DOCS_IDS']/csv:field[@number=$colNbDocsError]"/>
		<xsl:variable name="docs-added-ids" select="$groupe[csv:field[@number=$colStep]='DOCS_IDS']/csv:field[@number=$colDocsAddedID]"/>
		<xsl:variable name="docs-error-ids" select="$groupe[csv:field[@number=$colStep]='DOCS_IDS']/csv:field[@number=$colDocsAddedID]"/>
		<xsl:variable name="error-messages" select="$groupe[csv:field[@number=$colStep]='ERROR_MESSAGES']/csv:field[@number=$colErrorMessages]"/>

		<xsl:text>&#10;&#10;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-number">publication n° </i18n:text><xsl:value-of select="$k" />
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-is-ok">publication réussie: </i18n:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publication-is-ok.{string($pub-ok)}"><xsl:value-of select="string($pub-ok)" /></i18n:text>
		<xsl:if test="not(xs:boolean($pub-ok))">
			<xsl:variable name="s" select="$groupe[not(csv:field[@number=$colStep]='DOCS_IDS') and not(csv:field[@number=$colStep]='ERROR_MESSAGES')][position()=last()]/csv:field[@number=$colStep]" />
			<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-last-step">dernière étape de la publication: </i18n:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publication-last-step.{$s}"><xsl:value-of select="$s" /></i18n:text>
		</xsl:if>
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-date.start">date de début: </i18n:text><xsl:value-of select="$debut" />
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-date.end">date de fin: </i18n:text><xsl:value-of select="$fin" />
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-base">base de documents: </i18n:text><xsl:value-of select="$dbid" />
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-nbdocs">nombre de documents à publier: </i18n:text><xsl:value-of select="$nbdocs" />
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-nbdocs.added">nombre de documents publiés avec succès: </i18n:text><xsl:value-of select="$nbdocs-added" />
		<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-nbdocs.error">nombre de documents non publié: s</i18n:text><xsl:value-of select="$nbdocs-error" />

		<!-- Les identifiants des documents publiés -->
		<xsl:if test="$docs-added-ids!=''">
			<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.documents-added-ids">identifiants des documents publiés avec succès: </i18n:text>
			<xsl:variable name="l" select="replace(substring($docs-added-ids,2,(string-length($docs-added-ids) - 2)),', ',',')"/>
			<xsl:for-each select="tokenize($l,',')">
				<xsl:sort select="format-number(number(substring-before(.,'=')),'00000')"/>
				<xsl:value-of select="concat('&#10;&#9;&#9;',substring-before(.,'='),'- ',substring-after(.,'='))"/>
			</xsl:for-each>
		</xsl:if>

		<!-- Les messages d'erreurs -->
		<xsl:if test="$error-messages!='' and $error-messages!='{}'">
			<xsl:text>&#10;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.error-messages">messages d'erreur: </i18n:text>
			<xsl:variable name="e" select="replace(substring($error-messages,2,(string-length($error-messages) - 2)),', ',',')"/>
			<xsl:for-each select="tokenize($e,',')">
				<xsl:sort select="format-number(number(substring-before(.,'=')),'00000')"/>
				<xsl:text>&#10;&#9;&#9;</xsl:text><i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.error-document.rank">document n° </i18n:text><xsl:value-of select="substring-before(.,'='),'&#9;',substring-after(.,'=')" />
			</xsl:for-each>
		</xsl:if>

	</xsl:template>

</xsl:stylesheet>
