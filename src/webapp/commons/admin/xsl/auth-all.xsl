<?xml version="1.0"?>
<!-- $Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--

		XSLT chargée de sélectionner la première source d'authentification positive

		Elle renvoie un XML convenant au module d'authentification de Cocoon :
		<authentication>
			<ID>{identifiant de l'utilisateur}</ID>
			<roles>{un ou plusieurs rôles séparés par une virgule}</roles>
			<data>
				{information supplémentaire}
			</data>
		</authentication>

		Elle travaille un XML de la forme :

		<auth-all>
			<auth-file>[...]</auth-file>
			<auth-sql>[...]</auth-sql>
			<auth-ldap>[...]</auth-ldap>
			<auth-arkheia>[...]</auth-arkheia>
		</auth-all>

		Cf. http://cocoon.apache.org/2.1/developing/webapps/authentication.html
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<authentication>
			<xsl:apply-templates select="auth-all/*[authentication/ID!=''][1]" />
		</authentication>
	</xsl:template>

	<xsl:template match="auth-all/*">

		<xsl:if test="normalize-space(authentication/ID)!=''">
			<xsl:apply-templates select="authentication/*" mode="copy" />
		</xsl:if>

	</xsl:template>

	<xsl:template match="@* | node()" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" mode="copy" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
