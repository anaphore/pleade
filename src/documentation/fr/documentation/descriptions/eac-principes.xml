<?xml version="1.0"?>
<?xml-stylesheet href="../../_config/docbook.css" type="text/css"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "../../../_config/docbookx.dtd">
<article lang="fr"> 
  <title>Publication et consultation de documents
	 EAC : principes</title> 
  <titleabbrev>Documents EAC</titleabbrev> 
  <articleinfo> 
	 <author><firstname>Florence</firstname><surname>Clavaud</surname>
		
		<affiliation> 
		  <orgname>Anaphore</orgname> 
		</affiliation> 
		<email>florence.clavaud@arkheia.net</email>
		
	 </author> 
	 <abstract> 
		<para>Une présentation rapide des
		  fonctionnalités qui seront bientôt disponibles
		  dans PLEADE pour publier et consulter des notices
		  d'autorité au format XML/EAC.</para> 
	 </abstract><date
	 role="modification">2006-09-11</date> 
  </articleinfo> 
  <para>PLEADE a pour principal mandat de
	 permettre la publication et la consultation en
	 environnement Web de documents EAD, décrivant des
	 fonds ou corpus d'archives. Cependant PLEADE est
	 une application 
	 <ulink
	  url="http://adnx.org/sdx/">SDX</ulink>, et cette
	 plate-forme a été conçue de manière tout à fait
	 générique, pour indexer n'importe quel type de
	 documents XML, et développer des sites adaptés à
	 ces documents, aux besoins des utilisateurs et
	 aux logiques éditoriales. Cela rend donc possible
	 une <emphasis role="italic">extension du
	 périmètre de PLEADE à d'autres documents que des
	 documents EAD</emphasis>. Or depuis 2004, il
	 existe un modèle XML (la <emphasis
	 role="bold">DTD Encoded Archival Context,
	 EAC</emphasis>) pour décrire, sous la forme de
	 véritables notices d'autorité, les personnes
	 associées à ces corpus, et notamment leurs
	 producteurs. Un développement est donc en cours
	 et sera intégré à la version 2 rc de PLEADE, pour
	 permettre aux utilisateurs de PLEADE de publier
	 des documents EAC dans leur application. Nous
	 présentons rapidement ici les choix qui orientent
	 ce développement. </para> 
  <section> 
	 <title>Principes</title> 
	 <para>Un document XML/EAC peut décrire
		n'importe quelle entité agissant dans le domaine
		des archives. Mais, dans les structures
		responsables de fonds d'archives, si EAC est
		utilisée, ce sera très probablement pour décrire
		des producteurs de corpus documentaires. On
		supposera donc que les documents XML/EAC
		concernés ici décrivent bien des producteurs de
		documents d'archives. Notons cependant que,
		techniquement, lorsque PLEADE supportera le
		modèle EAC, n'importe quelle notice d'autorité en
		EAC pourra y être publiée, et les liens vers des
		documents EAD y seront fonctionnels, même si la
		relation n'est pas une relation de
		provenance.</para> 
	 <para>Les personnes décrites pourront être
		des personnes morales, des personnes physiques ou
		des familles. Les documents dont il s'agit seront
		conformes à la DTD XML EAC, version beta d'août
		2004, téléchargeable à : 
		<ulink
		url="http://jefferson.village.virginia.edu/eac/">http://jefferson.village.virginia.edu/eac/</ulink>.
		</para> 
	 <para><emphasis role="italic">L'intérêt de
		tels documents EAC est d'isoler et de structurer
		les informations relatives à une
		personne</emphasis>, disons à un producteur,
		celles-ci étant habituellement saisies sans
		structuration fine dans les éléments EAD Origine
		<sgmltag
		class="element">&lt;origination&gt;</sgmltag> et
		Biographie ou histoire <sgmltag
		class="element">&lt;bioghist&gt;</sgmltag>. La
		notice décrivant le producteur peut en effet être
		mise à jour selon d'autres rythmes et en fonction
		d'autres événements que l'instrument de
		recherche. De plus un producteur peut avoir
		produit n fonds ou ensembles documentaires,
		eux-mêmes décrits dans n documents EAD. Ceci fait
		d'ailleurs des notices d'autorité, bien plus que
		des documents EAD, des documents susceptibles
		d'être échangés ou partagés entre diverses
		institutions responsables de fonds d'archives.
		Enfin une notice d'autorité en tant que telle
		peut être intéressante pour l'utilisateur final,
		qu'il soit un professionnel des archives ou qu'il
		ait un autre profil. </para> 
	 <para>Comme EAD, le modèle EAC intéresse tous
		les responsables de fonds d'archives, donc, en
		particulier, aussi bien les archivistes que les
		bibliothécaires. La direction des Archives de
		France recommande de l'utiliser (<ulink
		url=" http://www.archivesdefrance.culture.gouv.fr/fr/circAD/DITN.2004.002.pdf">circulaire
		  de fin 2004</ulink>) ; il commence à en être
		question pendant certains stages de formation
		animés par diverses instances. Le groupe
		d'experts ad hoc de l'AFNOR vient de publier la 
		<ulink
		 url="http://www.archivesdefrance.culture.gouv.fr/fr/archivistique/EAC_trad_texte_integral.pdf">traduction
		  en français de la Tag Library de la DTD
		  EAC</ulink>. Certains projets spécifiques, comme 
		<ulink
		 url="http://chan.archivesnationales.culture.gouv.fr/sdx/etanot/index.xsp">Etanot</ulink>,
		utilisent ce format. Des logiciels permettront
		bientôt de produire facilement de tels documents.
		Malgré tout, la mise en oeuvre de la DTD EAC en
		est à ses débuts. D'autre part le modèle lui-même
		n'est pas totalement achevé ni documenté. C'est
		pourquoi la première version du volet EAC de
		PLEADE ne cherchera pas à atteindre, en
		particulier, un niveau de finesse très important
		dans l'indexation.</para> 
	 <para>Le propos, dans PLEADE, n'est pas de
		fournir un environnement de publication complet
		et dédié aux documents EAC, mais de permettre aux
		responsables d'une application PLEADE d'associer
		les documents EAD aux documents EAC qu'ils
		auraient pu produire. Les documents EAD restent
		bien le coeur d'une installation PLEADE.
		Cependant il ne faut pas exclure le scénario
		selon lequel des documents EAC seraient à publier
		alors que les documents EAD associés ne le sont
		pas ou n'existent pas. </para> 
	 <para>Une attention importante sera accordée
		aux <emphasis role="italic">relations entre les
		documents EAC et les unités documentaires issues,
		dans PLEADE, de la fragmentation des documents
		EAD</emphasis>. Ces relations sont bien prises en
		compte par le modèle EAC, dont c'est une des
		particularités. Il existe en effet dans la DTD
		EAC un élément Relation avec une ressource
		<sgmltag
		class="element">&lt;resourcerel&gt;</sgmltag>
		répétable, doté d'un attribut
		<sgmltag class="attribute">syskey</sgmltag> qui
		contient l'identifiant de l'unité documentaire
		EAD cible. Dans un document EAD, les éléments
		<sgmltag
		 class="element">&lt;origination&gt;</sgmltag> et
		<sgmltag
		 class="element">&lt;bioghist&gt;</sgmltag>
		disponibles à n'importe quel niveau de
		description peuvent aussi contenir les
		informations nécessaires (par le biais de
		l'attribut <sgmltag
		class="attribute">authfilenumber</sgmltag> de
		leurs sous-éléments Nom de personne
		<sgmltag
		 class="element">&lt;persname&gt;</sgmltag>,
		Collectivité <sgmltag
		class="element">&lt;corpname&gt;</sgmltag> et Nom
		de famille <sgmltag
		class="element">&lt;famname&gt;</sgmltag>) pour
		qu'un programme établisse un lien vers un
		document EAC.</para> 
	 <para>Une attention importante sera aussi
		accordée aux <emphasis role="italic">relations
		existant entre notices EAC</emphasis>. Ces liens
		(qui peuvent être établis au moyen de l'élément
		répétable Relation avec un document EAC
		<sgmltag
		 class="element">&lt;eacrel&gt;</sgmltag>, doté
		d'un attribut <sgmltag
		class="attribute">syskey</sgmltag>) sont
		potentiellement fréquents, notamment dans le cas
		de la description de familles (liens vers les
		notices décrivant les branches de la famille ou
		ses membres, et liens inverses) ou de personnes
		morales (liens depuis une notice d'autorité
		"principale" décrivant une structure complexe,
		vers les subdivisions de cette structure, et
		liens inverses ; ou liens depuis une notice EAC
		vers la notice d'une structure qui a une relation
		d'association ou d'antériorité et liens inverses,
		par ex.).</para> 
	 <para>Les éléments de lien concernés seront
		supposés renseignés avant la publication par tout
		moyen disponible, selon la syntaxe définie par
		les DTD EAD et EAC et, si nécessaire et sans
		mettre en question cette syntaxe, conformément à
		des consignes particulières de mise en oeuvre de
		cette syntaxe pour PLEADE.</para> 
	 <para>Il s'agit d'intégrer à PLEADE un
		nouveau groupe de fonctionnalités, ce qui
		implique : 
		<itemizedlist> 
		  <listitem> 
			 <para>qu'il se situe au
				<emphasis role="bold">même niveau de
				généricité</emphasis> pour EAC que l'actuel
				PLEADE pour EAD (support de tous documents EAC,
				multilinguisme, paramétrage et configuration
				possibles...) ; </para> 
		  </listitem> 
		  <listitem> 
			 <para>qu'il offre pour EAC
				<emphasis role="bold">les mêmes services
				</emphasis> que pour EAD, en tout cas ceux
				pertinents pour le propos (organisation en
				ensembles de notices, publication avec une
				indexation pertinente, recherche directe et par
				formulaires de recherche, consultation,
				possibilité de générer des enregistrements
				OAI-PMH, interface d'administration) ; </para> 
		  </listitem> 
		  <listitem> 
			 <para>que sur le plan technique il
				s'appuie sur les <emphasis role="bold">mêmes
				mécanismes</emphasis> que ceux utilisés par
				l'actuel PLEADE (par exemple sur l'ajout éventuel
				d'informations lors de la publication, sur
				l'utilisation de fichiers de configuration XML).
				</para> 
		  </listitem> 
		</itemizedlist></para> 
	 <para>En même temps ce volet EAC de PLEADE
		pourra être utilisé ou pas ; il conviendra donc
		que son accès soit offert en fonction d'une
		<emphasis role="bold">option de
		configuration</emphasis> du fichier de propriétés
		initial, comme c'est le cas pour la création d'un
		entrepôt OAI-PMH. Aucune des fonctionnalités
		actuelles de PLEADE ne devra se trouver perturbée
		par son utilisation. L'architecture technique
		choisie devra donc permettre, le mieux possible,
		de distinguer ce qui concerne EAD et ce qui
		concerne EAC. </para> 
  </section> 
  <section> 
	 <title>Fonctionnalités prévues</title> 
	 <para>Les développements en cours permettront
		de disposer dans PLEADE des fonctionnalités
		suivantes : 
		<itemizedlist> 
		  <listitem> 
			 <para>une <emphasis
				role="bold">interface de publication de documents
				EAC</emphasis>, à l'unité ou par dossiers,
				permettant de choisir des options, notamment en
				ce qui concerne l'indexation des notices (par
				ex., indexation plus ou moins fine des
				patronymes, indexation des fonctions), leur
				regroupement en sous-ensembles préalablement
				définis (par ex., par type de personne ; à plus
				long terme des dispositifs permettant d'exploiter
				plus à plein certains types de regroupements
				pourraient être étudiés, en fonction des liens
				existant entre diverses notices EAC), les liens
				avec des documents EAD, les modes d'affichage
				(plein écran ou juxtaposé au document EAD, par
				ex.) ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>des <emphasis role="bold">boutons
				d'administration des documents EAC</emphasis>,
				permettant, comme pour EAD, de supprimer,
				republier un document, de vider la base ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>au moins <emphasis role="bold">un
				formulaire de recherche</emphasis> dédié à
				l'interrogation des notices d'autorité, avec,
				parmi les critères de recherche possibles,
				l'existence de documents EAD associés ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>des <emphasis
				role="bold">parcours possibles dans le corpus de
				notices</emphasis>, par regroupements ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>éventuellement, des
				<emphasis role="bold">parcours possibles par le
				biais de certaines listes d'index</emphasis> (par
				ex. : par le biais de l'index alphabétique des
				noms des entités décrites ou de leurs fonctions)
				;</para> 
		  </listitem> 
		  <listitem> 
			 <para>des listes de résultats de
				recherche fournissant l'essentiel des
				informations sur le producteur (nom, dates
				d'activité, catégorie, fonction), éventuellement
				triables ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>une présentation HTML de chaque
				notice, permettant d'atteindre les fragments EAD
				associés (et vice-versa) et les notices EAC
				reliées à la notice affichée ; éventuellement, le
				téléchargement de la notice affichée au format
				PDF ;</para> 
		  </listitem> 
		  <listitem> 
			 <para>un <emphasis role="bold">entrepôt
				de métadonnées OAI-PMH</emphasis> permettant
				d'exposer des enregistrements Dublin Core et
				Dublin Core qualifié.</para> 
		  </listitem> 
		</itemizedlist>Ces fonctionnalités seront
		disponibles dans la version 2.0beta de PLEADE. Un
		appel sera lancé aux utilisateurs de PLEADE pour
		trouver des personnes qui accepteraient de tester
		les programmes avant la publication officielle de
		la version 2.0 finale.</para> 
  </section> 
  <section> 
	 <title>Architecture technique</title> 
	 <para>Sans chercher ici à être précis, voici
		ce qui est en cours d'écriture dans les sources
		de PLEADE pour implémenter ce volet EAC : 
		<itemizedlist> 
		  <listitem> 
			 <para>une troisième base de documents
				sera créée et configurée, avec ses propres
				champs, ses processus de transformation et de
				publication, et un entrepôt OAI-PMH spécifique
				;</para> 
		  </listitem> 
		  <listitem> 
			 <para>des XSP, XSLT et fichiers de
				configuration XML spécifiques seront écrits, et
				déposés soit dans les répertoires qui existent
				déjà dans PLEADE, soit dans de nouveaux
				répertoires dont le nom contiendra le préfixe
				"eac", selon la même logique qui a présidé à la
				conception de l'architecture initiale et en ne
				touchant pas aux noms de répertoires et de
				fichiers existants. Les programmes XSLT chargés
				de générer une version HTML des documents EAD
				seront modifiés pour inclure des liens hypertexte
				valides vers les documents EAC publiés dans
				l'application ; </para> 
		  </listitem> 
		  <listitem> 
			 <para>une ou plusieurs feuilles de
				style CSS spécifiques seront créées pour la mise
				en forme de la version HTML des documents
				EAC.</para> 
		  </listitem> 
		</itemizedlist></para> 
  </section> 
</article> 
