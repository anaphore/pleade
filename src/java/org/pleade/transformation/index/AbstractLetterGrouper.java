/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation.index;

import java.util.Vector;

import org.apache.avalon.framework.logger.AbstractLogEnabled;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

/**
 * An abstract grouper based on letters from the terms.
 */
public abstract class AbstractLetterGrouper extends AbstractLogEnabled implements Grouper {

    public static final char DEFAULT_CHARACTER = '_';

    /** The last letter used for grouping */
    protected String lastTargetLetter = null;

    /** Whether a group is open or not */
    protected boolean inGroup = false;

    /** The list of terms to send */
    protected Vector terms = new Vector();

    /** The maximum number of terms in a group */
    protected int maxNbTerms = 100;

    /** The target letter to check */
    protected int targetLetterNumber;

    /**
     * Groups terms and outputs them.
     */
    public void group(Term t, ContentHandler hdl) throws SAXException {

        // We need to check if we send a group

        // The target letter of the current term, uppercased
        String targetLetter = getTargetLetter(t.getValue());

        // We start a group if one of these conditions is met:
        //
        //  - target term (lastTargetLetter = null
        //  - target letter different from last one
        if ( lastTargetLetter == null || !targetLetter.equals(lastTargetLetter) ) {

            // Check if a group is already open
            if ( inGroup ) finalizeGroup(hdl);

            AttributesImpl atts = new AttributesImpl();
            atts.addAttribute("", "id", "id", "CDATA", getGroupId(t.getValue()));
            hdl.startElement("", "group", "group", atts);
            inGroup = true;
        }
        // In any case we keep track of the target letter
        lastTargetLetter = targetLetter;
        // and the term...
        terms.add(t);
    }

    public String getGroupId(String val) {
        String v = setComparable(val);
        if ( v.length() > targetLetterNumber ) {
            return normalizeString(v.substring(0, targetLetterNumber));
        }
        else {
            // We pad with "_"
            String ret = v;
            for (int i=v.length(); i<targetLetterNumber; i++) ret += DEFAULT_CHARACTER;
            return ret;
        }
    }

    public void finalizeGroup(ContentHandler hdl) throws SAXException {
        // Check if we need to group again
        int nbTerms = terms.size();
        if ( nbTerms > maxNbTerms ) {
            // Send the terms to a new grouper
            Grouper g = new TargetLetterGrouper(targetLetterNumber + 1);
            g.init(hdl);
            for (int i=0; i<terms.size(); i++) g.group((Term)terms.get(i), hdl);
            g.finalize(hdl);
        }
        else {
	        // Send the terms to the handler
	        for (int i=0; i<nbTerms; i++) ((Term)terms.get(i)).toSAX(hdl);
        }
        // Check if a group is already open
        if ( inGroup ) hdl.endElement("", "group", "group");
        // Reinit the terms vector
        terms = new Vector();
    }

    public void finalize(ContentHandler hdl) throws SAXException {
        finalizeGroup(hdl);
    }

    public void init(ContentHandler hdl) throws SAXException {
    }

    protected String normalizeChar(String letter) {
        char c = letter.charAt(0);
        String norm = "";
        switch (c) {
				case '\u00C0':
				case '\u00C1':
				case '\u00C2':
				case '\u00C3':
				case '\u00C4':
				case '\u00C5':
				case '\u0100':
				case '\u0102':
				case '\u0104':
				case '\u01CD':
				case '\u01FA':
							norm = "A";
							break;
				case '\u00C7':
				case '\u0106':
				case '\u0108':
				case '\u010A':
				case '\u010C':
							norm = "C";
							break;
				case '\u010E':
							norm = "D";
							break;
	    	case '\u00C8':
				case '\u00C9':
				case '\u00CA':
				case '\u00CB':
				case '\u0112':
				case '\u0114':
				case '\u0116':
				case '\u0118':
				case '\u011A':
							norm = "E";
							break;
				case '\u011C':
				case '\u011E':
				case '\u0120':
				case '\u0122':
					    norm = "G";
							break;
				case '\u0124':
					    norm = "H";
							break;
	    	case '\u00CC':
				case '\u00CD':
				case '\u00CE':
				case '\u00CF':
				case '\u0128':
				case '\u012A':
				case '\u012C':
				case '\u012E':
				case '\u0130':
				case '\u01CF':
							norm = "I";
							break;
				case '\u0134':
					    norm = "J";
							break;
				case '\u0136':
				case '\u0138':
					    norm = "K";
							break;
				case '\u0139':
				case '\u013B':
				case '\u013D':
					    norm = "L";
							break;
				case '\u00D1':
				case '\u0143':
				case '\u0145':
				case '\u0147':
					    norm = "N";
							break;
	    	case '\u00D2':
				case '\u00D3':
				case '\u00D4':
				case '\u00D5':
				case '\u00D6':
				case '\u014C':
				case '\u014E':
				case '\u0150':
				case '\u01A0':
				case '\u01D1':
				case '\u01FE':
							norm = "O";
							break;
				case '\u0154':
				case '\u0156':
				case '\u0158':
					    norm = "R";
							break;
				case '\u015A':
				case '\u015C':
				case '\u015E':
				case '\u0160':
					    norm = "S";
							break;
				case '\u0162':
				case '\u0164':
					    norm = "T";
							break;
	    	case '\u00D9':
				case '\u00DA':
				case '\u00DB':
				case '\u00DC':
				case '\u0168':
				case '\u016A':
				case '\u016C':
				case '\u016E':
				case '\u0170':
				case '\u0172':
				case '\u01AF':
				case '\u01D3':
				case '\u01D5':
				case '\u01D7':
				case '\u01D9':
				case '\u01DB':
	    	    norm = "U";
	    	    break;
				case '\u0174':
					  norm = "W";
						break;
				case '\u00DD':
				case '\u0176':
				case '\u0178':
					  norm = "Y";
						break;
				case '\u0179':
				case '\u017B':
				case '\u017D':
					  norm = "Z";
						break;
				case '\u00C6': // Æ
	    	case 'A':
	    	case 'B':
	    	case 'C':
	    	case 'D':
				case '\u00D0': // DE barre
	    	case 'E':
	    	case 'F':
	    	case 'G':
	    	case 'H':
	    	case 'I':
	    	case 'J':
	    	case 'K':
	    	case 'L':
	    	case 'M':
	    	case 'N':
				case '\u0152': // Œ
	    	case 'O':
	    	case 'P':
	    	case 'Q':
	    	case 'R':
	    	case 'S':
	    	case 'T':
	    	case 'U':
	    	case 'V':
	    	case 'W':
	    	case 'X':
	    	case 'Y':
	    	case 'Z':
				case '\u00D8': // Ø
				case '\u0110': // D barre
				case '\u0126': // Ħ
				case '\u0141': // L barre
				case '\u013F': // Ŀ
				case '\u0166': // Ŧ
	    	case '0':
	    	case '1':
	    	case '2':
	    	case '3':
	    	case '4':
	    	case '5':
	    	case '6':
	    	case '7':
	    	case '8':
	    	case '9':
	    	case '-':
	    	    norm = "" + c;
	    	    break;
	    	default:
	    	    // Non letter character : return a default one
	    	    norm = "" + DEFAULT_CHARACTER;
	    		break;
        }
        return norm;
    }

    protected String setComparable(String s) {
        StringBuffer ret = new StringBuffer();
        for (int i=0; i<s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
            	case ' ':
            	    break;	// Do nothing, we remove these characters
            	default:
            	    ret.append(c);
            		break;
            }
        }
        return ret.toString();
    }

    protected String normalizeString(String s) {
        StringBuffer sb = new StringBuffer();
        for ( int i=0; i<s.length(); i++) {
            sb.append(normalizeChar(s.substring(i, i+1).toUpperCase()));
        }
        return sb.toString();
    }

    public abstract String getTargetLetter(String value);

    protected void debug(String msg) {
        getLogger().debug(msg);
    }
}
