<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: eac2html.xsl 17423 2010-01-07 09:54:12Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Coeur de page : affichage XHTML d'une notice EAC
    | Travaille un XML au format :
      <eac-cpf> 
        <control>
          <recordId>[..]</recordId>
          <maintenanceAgency>[...]</maintenanceAgency>
          <maintenanceStatus>[...]</maintenanceStatus>
          <maintenanceHistory>[...]</maintenanceHistory>
          <publicationStatus>[...]</publicationStatus>
          <languageDeclaration>[...]</languageDeclaration>
          <sources>[...]</sources>
          <conventionDeclaration>[...]</conventionDeclaration>
          <otherRecordId>[...]</otherRecordId>
          <localControl>[...]</localControl>
          <localTypeDeclaration>[...]</localTypeDeclaration>
        </control>
        <cpfDescription>
          <identity>[...]</identity>
          <description>
            <existDates></existDates>
            <place></place> ou <places></places>
            <localDescription></localDescription> ou <localDescriptions></localDescriptions>
            <legalStatus></legalStatus> ou <legalStatuses></legalStatuses>
            <function></function> ou <functions></functions>
            <occupation></occupation> ou <occupations></occupations>
            <mandate></mandate> ou <mandates></mandates>
            <structureOrGenealogy></structureOrGenealogy>
            <generalContext></generalContext>
            <biogHist></biogHist>
          </description>
          <relations>
            <cpfRelation></cpfRelation>
            <functionRelation></functionRelation>
            <resourceRelation></resourceRelation>
          </relations>
          <alternativeSet>[...]</alternativeSet>
        </cpfDescription>
    | </eac-cpf>
    | <eac-cpf> 
    |   <control>[...]</control>
    |   <multipleIdentities> 
    |     <cpfDescription>[...]</cpfDescription>
    |     <cpfDescription>[...]</cpfDescription>
    |   </multipleIdentities>
    | </eac-cpf>
    +-->
<!-- ISAAR
        1. Identity area
        2. Description area
        3. Relationships area
        4. Control area
        5. Relating corporate bodies, persons, and families to archival materials and other resources
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                xmlns:xsp="http://apache.org/xsp"
                xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                xmlns:pleade="http://pleade.org/ns/pleade/1.0"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                exclude-result-prefixes="xsl xsp sdx i18n pleade xlink">

  <!-- Import de la XSL pour formater le XML -->
  <xsl:import href="xml2html.xsl"/>
  <!-- XSL contenant des templates/fonctions génériques -->
  <xsl:import href="common.xsl"/>

  <!-- Extension demandée par l'URL -->
  <xsl:param name="ext" select="''" />

  <!-- Inclusion de la XSL commune d'affichage des documents EAD -->
  <!-- <xsl:include href="../../ead-xslt/html/common.xsl"/> -->

  <!--+
      | Match la racine
      +-->
  <xsl:template match="/eac-doc">

    <xsl:choose>
      <!-- Seulement l'affichage d'un bloc HTML -->
      <xsl:when test="$ext='ajax'">
        <div>
          <xsl:apply-templates select="." mode="script-head" />
          <xsl:apply-templates select="sdx:document" mode="doc" />
        </div>
      </xsl:when>
      <!-- Affichage d'une page complète -->
      <xsl:otherwise>
        <html>
          <xsl:apply-templates select="." mode="html-head" />
          <xsl:apply-templates select="." mode="html-body" />
        </html>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!--+
      | Construction de l'en-tête XHTML
      +-->
  <xsl:template match="eac-doc" mode="html-head">
    <head>
      <title>
        <xsl:apply-templates select="." mode="page-title" />
      </title>
      <xsl:apply-templates select="." mode="script-head" />
    </head>
  </xsl:template>

  <!--+
      | Construction du corps de page XHTML
      +-->
  <xsl:template match="eac-doc" mode="html-body">
    <body>
      <xsl:apply-templates select="sdx:document" mode="doc" />
    </body>
  </xsl:template>

  <!--+
      | Le titre de la page HTML
      +-->
  <xsl:template match="eac-doc" mode="page-title">

    <xsl:choose>
      <xsl:when test="sdx:document/*[local-name() = 'eac-cpf']/@pleade:title">
        <xsl:value-of select="sdx:document/*[local-name() = 'eac-cpf']/@pleade:title" />
      </xsl:when>
      <xsl:when test="sdx:document/*[local-name() = 'eac-cpf']/*[local-name() = 'cpfDescription']/*[local-name() = 'identity']/*[local-name() = 'nameEntry'][1]/*[local-name() = 'part'][.!='']">
        <xsl:apply-templates select="sdx:document/*[local-name() = 'eac-cpf']/*[local-name() = 'cpfDescription']/*[local-name() = 'identity']/*[local-name() = 'nameEntry'][1]/*[local-name() = 'part'][.!=''][1]" />
      </xsl:when>
      <xsl:when test="properties[@id='display']/property[@name='title' and .!='']">
        <xsl:apply-templates select="properties[@id='display']/property[@name='title' and .!=''][1]" />
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <!--+
      | Scripts supplémentaires dans l'en-tête XHTML
      | Valable pour extension ajax et autre
      +-->
  <xsl:template match="eac-doc" mode="script-head">
    <!-- <xsl:if test="$ext!='ajax'"/> -->
  </xsl:template>

  <!--+
      | Présentation du document
      +-->
  <xsl:template match="sdx:document" mode="doc">

    <!-- Affichage des résultats. FIXME : est-ce utilisé? -->
    <xsl:choose>
      <xsl:when test="message">
        <xsl:text>&#32;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates />
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!--+
      | Affichage du document EAC
      +-->
  <!-- On traite les sections selon l'ordre de l'ISAAR -->    
  <xsl:template match="*[local-name() = 'eac-cpf']">
    <div class="pl-pgd-doc" id="eac-doc">
      <!-- FIXME: CSS -->
      <table summary="eac.doc.table.summary" i18n:attr="summary">
        <caption>
          <h1 class="pl-title">
            <xsl:apply-templates select="/eac-doc" mode="page-title" />
          </h1>
        </caption>
        <xsl:apply-templates select="
          *[local-name() = 'cpfDescription']/*[local-name() = 'identity'],
          *[local-name() = 'cpfDescription']/*[local-name() = 'description'],
          *[local-name() = 'cpfDescription']/*[local-name() = 'relations'],
          *[local-name() = 'control'],
          *[local-name() = 'cpfDescription']/*[local-name() = 'alternativeSet']
        " />
      </table>
    </div>
  </xsl:template>

  <!--+
      | Traitement des sections
      | 5 sections ont été définies :
      |   - control,
      |   - cpfDescripion/identity,
      |   - cpfDescription/description,
      |   - cpfDescription/relations,
      |   - cpfDescription/alternativeSet
      +-->
  <xsl:template match="*[local-name() = 'control']|*[local-name() = 'cpfDescription']/*">
    <xsl:apply-templates select="." mode="sect-title"/>
    <tbody>
      <xsl:for-each-group select="*[. != '']" group-by="local-name()">
        <xsl:apply-templates select="current-group()" mode="build-row">
          <xsl:with-param name="elname" select="local-name()" />
          <xsl:with-param name="pos" select="position()" />
        </xsl:apply-templates>
      </xsl:for-each-group>
    </tbody>
  </xsl:template>

  <!--+
      | Construction de lignes
      +-->
  <!-- Thso ones should be displayed on one line -->
  <xsl:template match="*[local-name()='nameEntry'][not(position()=1)]" mode="build-row"/>
  <xsl:template match="*[local-name()='resourceRelation'][not(position()=1)]" mode="build-row"/>
  <!-- Build new line without conditions for all others -->
  <xsl:template match="*" mode="build-row">
    <xsl:param name="elname" select="''" />
    <xsl:param name="pos" select="1" />
    <xsl:param name="first-last" select="''" /> 

    <!-- DEBUG <xsl:message>build-row :: elname = <xsl:value-of select="$elname"/> // local-name = <xsl:value-of select="local-name()"/> // pos = <xsl:value-of select="$pos"/> // nb-ds-grp = <xsl:value-of select="count(current-group())"/></xsl:message> -->

    <xsl:if test="$elname!=''">

      <xsl:variable name="odd-even">
        <xsl:choose>
          <xsl:when test="($pos mod 2) = 0">
            <xsl:text>odd</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>even</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <tr>
        <xsl:attribute name="class">
          <xsl:value-of select="concat('pl-tbl-tr-',$odd-even)" />
          <xsl:if test="$first-last!=''">
            <xsl:value-of select="concat(' pl-tbl-tr-',$first-last)" />
          </xsl:if>
        </xsl:attribute>

        <th class="label">
          <xsl:apply-templates select="." mode="label">
            <xsl:with-param name="eln" select="$elname"/>
          </xsl:apply-templates>
        </th>

        <td class="value">
          <xsl:apply-templates select="." mode="value">
            <xsl:with-param name="elname" select="$elname"/>
            <xsl:with-param name="parent" select="local-name(parent::*)"/>
            <xsl:with-param name="pos" select="if (count(current-group()) > 1) then position() else 0"/>
          </xsl:apply-templates>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <!--+
      | Gestion des valeurs de champs
      +-->
  <xsl:template match="*[local-name()='nameEntry'][1]" mode="value" priority="+3">
    <xsl:choose>
      <xsl:when test="following-sibling::*[local-name()='nameEntry']">
        <ul>
          <xsl:apply-templates select="parent::*/*[local-name()='nameEntry']" mode="list"/>
        </ul>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates mode="value">
          <xsl:with-param name="elname" select="local-name()"/>
          <xsl:with-param name="parent" select="local-name(parent::*)"/>
          <xsl:with-param name="pos" select="if (count(current-group()) > 1) then position() else 0"/>
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="*[local-name()='nameEntry'][not(position()=1)]" mode="value" priority="+3"/>

  <xsl:template match="*" mode="list" priority="+2">
    <li>
      <xsl:apply-templates mode="value">
        <xsl:with-param name="elname" select="local-name()"/>
        <xsl:with-param name="parent" select="local-name(parent::*)"/>
        <xsl:with-param name="pos" select="if (count(current-group()) > 1) then position() else 0"/>
      </xsl:apply-templates>
    </li>
  </xsl:template>

  <xsl:template match="*[local-name()='nameEntry']" mode="data" priority="+2">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="*[local-name()='resourceRelation'][1]" mode="value" priority="+3">
    <xsl:choose>
      <xsl:when test="following-sibling::*[local-name()='resourceRelation']">
        <ul>
          <xsl:apply-templates select="parent::*/*[local-name()='resourceRelation']" mode="list"/>
        </ul>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="value">
          <xsl:with-param name="elname" select="local-name()"/>
          <xsl:with-param name="parent" select="local-name(parent::*)"/>
          <xsl:with-param name="pos" select="if (count(current-group()) > 1) then position() else 0"/>
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*[local-name()='dateRange']" mode="value" priority="+2">
    <xsl:apply-templates mode="value">
      <xsl:with-param name="elname" select="local-name()"/>
      <xsl:with-param name="parent" select="local-name(parent::*)"/>
      <xsl:with-param name="pos" select="if (count(current-group()) > 1) then position() else 0"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="*[*]" mode="value">
    <xsl:param name="elname" select="local-name()"/>
    <xsl:param name="parent" select="''"/>
    <xsl:param name="pos"/>

    <!-- DEBUG <xsl:message>value avec ss :: elname = <xsl:value-of select="$elname"/> // local-name() = <xsl:value-of select="local-name()"/> // parent = <xsl:value-of select="$parent"/> // pos = <xsl:value-of select="$pos"/> // nb ss = <xsl:value-of select="count(*)"/> // text() = <xsl:value-of select="count(text())"/></xsl:message> -->

    <div class="pl-doc-grp eac-{$elname}-grp">
      <span class="pl-eac-grp-count">
        <xsl:comment>u</xsl:comment> <!-- Pour éviter d'avoir un span vide -->
        <xsl:value-of select="if (number($pos) > 0) then concat($pos, ') ') else ''"/>
      </span>
      <!-- TODO : pb de traitement des attributs <xsl:apply-templates select="@*"/> -->
      <xsl:for-each select="*">
        <xsl:variable name="eln" select="if (*) then 'div' else 'p'"/>
        <xsl:element name="{$eln}">
          <xsl:attribute name="class">
            <xsl:text>pl-eac-</xsl:text><xsl:value-of select="local-name()"/>
            <xsl:text> </xsl:text><xsl:value-of select="local-name()"/>-<xsl:value-of select="$eln"/>
          </xsl:attribute>
          <span class="pl-spn-lbl">
            <xsl:comment>u</xsl:comment> <!-- Pour éviter d'avoir un span vide -->
            <i18n:text key="eac.{local-name()}.{@localType}.{$elname}">
              <xsl:value-of select="concat('eac.', local-name(), '.', @localType, '.', $elname)" />
            </i18n:text>
          </span>
          <xsl:if test=".[not(*)]"> <!-- On ne met pas le séparateur si pas de noeud texte, que des sous-éléments. PB : ne fonctionne pas avec les contenus mixtes -->
            <i18n:text key="eac.sep.label-value.{local-name()}.{@localType}.{$elname}">&#160;: </i18n:text>
          </xsl:if>
          <xsl:apply-templates select="." mode="value">
            <xsl:with-param name="elname" select="local-name()"/>
            <xsl:with-param name="parent" select="$elname"/>
          </xsl:apply-templates>
        </xsl:element>
      </xsl:for-each>
    </div>
  </xsl:template>

  <!-- Noms -->
  <xsl:template match="*[local-name()='part']" mode="value" priority="+2">
    <!-- TODO: Multiple parts: made a list -->
    <!--<xsl:choose>
      <xsl:when test="following-sibling::*[local-name()='part'] or preceding-sibling::*[local-name()='part']">
        <li class="pl-spn-value">
          <xsl:apply-templates mode="value">
            <xsl:with-param name="parent" select="local-name()"/>
          </xsl:apply-templates>
        </li>
      </xsl:when>
      <xsl:otherwise>-->
        <span class="pl-spn-value">
          <xsl:apply-templates mode="value">
            <xsl:with-param name="parent" select="local-name()"/>
          </xsl:apply-templates>
        </span>
      <!--</xsl:otherwise>
    </xsl:choose>-->
  </xsl:template>
  <!-- Dates -->
  <xsl:template match="*[local-name()='fromDate']" mode="value" priority="+10">
    <xsl:value-of select="."/> - <xsl:value-of select="following-sibling::*[local-name()='toDate']"/>
  </xsl:template>
  <xsl:template match="*[local-name()='toDate']" mode="value" priority="+10"/>

  <xsl:template match="*" mode="value">
    <xsl:param name="elname" select="local-name()"/>
    <xsl:param name="parent" select="local-name(parent::*)"/>
    <xsl:param name="pos"/>

    <!-- DEBUG <xsl:message>value :: elname = <xsl:value-of select="$elname"/> // parent = <xsl:value-of select="$parent"/> // position = <xsl:value-of select="position()"/></xsl:message> -->

    <xsl:if test=".!='' and not(*)">
      <span class="pl-spn-value">
        <xsl:choose>
          <xsl:when test="pleade:eac-listElement(.)">
            <i18n:text key="eac.list.{$elname}.{.}"><xsl:value-of select="concat('eac.list.', $elname, '.', ., '.', @localType)" /></i18n:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates mode="value">
              <xsl:with-param name="parent" select="$elname"/>
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </xsl:if>
  </xsl:template>

  <!-- +
       |    Traitements des attributs
       +-->
  <xsl:template match="@xlink:href">
    <xsl:message>On matche le @href</xsl:message>
    <a href="{.}" class="pl-eac-xlinkRef" title="eac.xlinkRef.title" i18n:attr="title"><i18n:text key="eac.xlinkRef.text">[lien]</i18n:text></a>
  </xsl:template>
  <xsl:template match="@*">
  </xsl:template>

  <!-- +
       |    Traitements particuliers d'éléments EAC-CPF
       +-->
  <!-- Eléments inline span -->
  <xsl:template match="span">
    <xsl:if test=".!=''">
      <span class="pl-spn-value"><xsl:apply-templates /></span>
    </xsl:if>
  </xsl:template>
  <!-- Eléments de date : date, fromDate, toDate | eventDateTime  -->
  <xsl:template match="*[@standardDate or @standardDateTime]" mode="value" priority="9">
    <xsl:param name="elname" select="local-name()"/>
    <xsl:param name="parent" select="''"/>

    <xsl:variable name="val" select="pleade:eacDisplayDate(.)"/>

    <span class="pl-spn-value"><xsl:value-of select="$val"/></span>
  </xsl:template>
  <!-- Eléments avec code : language, script -->
  <xsl:template match="*[local-name() = 'language'] | *[local-name() = 'script']" mode="value" priority="9">
    <xsl:param name="elname" select="local-name()"/>
    <xsl:param name="parent" select="''"/>

    <span class="pl-spn-value">
      <xsl:choose>
        <xsl:when test=". != ''"><xsl:value-of select="."/></xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="@*[local-name() = 'scriptCode' or local-name() = 'languageCode']" mode="value"/>
        </xsl:otherwise>
      </xsl:choose>
    </span>
  </xsl:template>
  <!-- Eléménts de contenu mixte (PCDATA | span) -->
  <xsl:template match="*[local-name() ='abstract'] | *[local-name() ='citation'] | *[local-name() ='item'] | *[local-name() ='p']" mode="value" priority="9">
    <xsl:param name="elname" select="local-name()"/>
    <xsl:param name="parent" select="''"/>

    <xsl:if test="span">
      <i18n:text key="eac.sep.label-value.{local-name()}.{@localType}.{$elname}">&#160;: </i18n:text>
    </xsl:if>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:template>
  <xsl:template match="p" mode="value">
    <xsl:if test=".!=''">
      <xsl:if test="position()!=1">
        <br/>
      </xsl:if>

      <span class="pl-spn-value">
        <xsl:apply-templates />
      </span>
    </xsl:if>
  </xsl:template>
  <!-- outline, list, chronList -->
  <xsl:template match="*[local-name() ='outline']|*[local-name() ='list']|*[local-name() ='chronList']" mode="value" priority="9">
    <ul class="pl-eac-{local-name()}">
      <xsl:for-each select="*[local-name() ='level']|*[local-name() ='item']|*[local-name() ='chronItem']">
        <xsl:choose>
          <xsl:when test="self::*[local-name() ='level']">
            <xsl:apply-templates select="."/>
          </xsl:when>
          <xsl:when test="self::*[local-name() ='chronItem']">
            <li class="pl-eac-{local-name()}"><xsl:apply-templates select="." mode="value"/></li>
          </xsl:when>
          <xsl:otherwise>
            <li class="pl-eac-{local-name()}"><xsl:apply-templates select="."/></li>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </ul>
  </xsl:template>
  <!-- level -->
  <xsl:template match="*[local-name() ='level']">
    <li class="pl-eac-level">
      <xsl:apply-templates select="*[local-name() ='item']"/>
      <xsl:if test="local-name() ='level'">
        <ul class="pl-list-level">
          <xsl:for-each select="*[local-name() ='level']">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>
  <!-- Afficher sous forme de code XML -->
  <xsl:template match="*[local-name() ='objectXMLWrap']" mode="value" priority="9">
    <xsl:apply-templates select="node()" mode="xml-html"/>
  </xsl:template>

  <!-- Peut-être vide et avoir un attribut @standardDateTime ou rien. -->
  <xsl:template match="*[local-name() ='eventDateTime']" mode="value">
    <xsl:param name="elname"/>
    <xsl:param name="pos"/>

    <span class="pl-spn-value">
      <xsl:choose>
        <xsl:when test=". != ''"><xsl:value-of select="."/></xsl:when>
        <xsl:when test=". = '' and @standardDateTime != ''"><xsl:value-of select="@standardDateTime"/>
        </xsl:when>
        <xsl:otherwise>
          <i18n:text key="eac.eventDateTime.noDate">nr</i18n:text>
        </xsl:otherwise>
      </xsl:choose>
    </span>
  </xsl:template>

</xsl:stylesheet>
