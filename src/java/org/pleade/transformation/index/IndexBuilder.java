/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
 */
package org.pleade.transformation.index;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.logger.NullLogger;
import org.apache.avalon.framework.parameters.ParameterException;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.serialization.XMLSerializer;
import org.apache.cocoon.transformation.Transformer;
import org.pleade.utils.PleadeUtilities;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import fr.gouv.culture.sdx.pipeline.AbstractTransformation;

/**
 * Construction des listes de termes des indes depuis un document EAD.
 */
public class IndexBuilder extends AbstractTransformation implements Transformer {

	/** Le namespace utilise pour les sorties d'information. */
	public final static String NAMESPACE = "";

	/** L'extension des fichiers de sortie. */
	public final static String FILENAME_EXTENSION = ".xml";

	/** Le nom du fichier qui liste les index disponibles. */
	public final static String OVERVIEW_FILENAME = "pleade-index.xml";

	/** Le nom du sous-dossier qui contient les informations sur les index. */
	public final static String INDEX_SUBDIRECTORY = "index";

	/** Le dossier de sortie. */
	private File outputDirectory = null;

	/** Le parametre qui indique le dossier ou sortir les index. */
	public final static String OUTPUTDIR_PARAMETER_NAME = "data-output-dir";

	/** L'identifiant du document courant*/
	private String documentId = "";

	/** Le type du document courant */
	private String documentType = "";

	/** La liste des index par identifiant. */
	private TreeMap indicesById = new TreeMap();

	// Indique si l'on est dans un element d'indexation
	private boolean inIndexElement = false;

	// Un buffer pour contenir les caracteres
	private StringBuffer contentBuffer;

	// Une pile des identifiants des elements ouverts
	private Stack currentIds = new Stack();

	// Une pile des attributs des elements ouverts
	private Stack currentAttributes = new Stack();


	/**
	 * Au debut du document, reinitialise les variables globales.
	 */
	public void startDocument() throws SAXException {
		this.currentIds = new Stack();
		this.currentAttributes = new Stack();
		this.outputDirectory = null;
		this.documentId = "";
    this.documentType = "";
		this.indicesById = new TreeMap();
		super.startDocument();
	}

	/**
	 * Methode appelee par Cocoon lorsque cette transformation est utilisee dans un pipeline.
	 * Pour l'instant elle ne fait rien.
	 */
	public void setup(SourceResolver resolver, Map map, String src, Parameters params) throws ProcessingException, SAXException, IOException {
		this.parameters = params;
	}

	/**
	 * Conserve les caracteres dans le buffer si on est dans un element d'indexation.
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		if ( inIndexElement ) contentBuffer.append(ch, start, length);
		// On passe la main pour l'etape suivante
		super.characters(ch, start, length);
	}

	/**
	 * Debut d'un element. S'il s'agit d'un element d'indexation, on initalise les informations
	 * de traitement.
	 */
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		// Cette manipulation est necessaire pour des parseurs qui ne gerent pas les namespace
		if ( localName == null || localName.equals("") ) localName = qName;
		// On conserve toujours les attributs
		currentAttributes.push(new AttributesImpl(atts));
		// S'il s'agit d'un element d'indexation, alors on initialise les informations
		if ( isIndexElement(namespaceURI, localName, atts)) {
			inIndexElement = true;
			contentBuffer = new StringBuffer();
		}

		// On conserve l'identifiant du document EAD
		if ( PleadeUtilities.isRoot(localName) ) {
      this.documentId = PleadeUtilities.getPleadeId(atts);
      this.documentType = PleadeUtilities.getDocumentType(localName);
    }

		// Conserve l'identifiant de l'element, s'il s'agit d'un composant ou element avec identifiant obligatoire
		if ( PleadeUtilities.isPleadeBreak(this.documentType,localName,atts) ) {
			currentIds.push(PleadeUtilities.getPleadeId(atts));
		}

		// On passe la main pour la prochaine etape
		super.startElement(namespaceURI, localName, qName, atts);
	}

	/**
	 * Fermeture d'un element : on gere les elements d'index mais aussi la fin du document.
	 */
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {

		// Pour les parseurs qui supportent mal les namespace
		if ( localName == null || localName.equals("") ) localName = qName;

		// On prend les attributs de la stack
		Attributes currentAtts = (Attributes)currentAttributes.pop();

		// Traitement des elements d'indexation
		if ( isIndexElement(namespaceURI, localName, currentAtts) ) {

			// Le contenu
			String value = currentAtts.getValue(PleadeUtilities.PLEADE_NAMESPACE_URI, "value");
			// Les index dont fait partie cet element
			//            String indexIds = currentAtts.getValue(PleadeUtilities.PLEADE_NAMESPACE_URI, "index-list");
			//            StringTokenizer st = new StringTokenizer(indexIds, ",");
			// On boucle sur ces index
			//            while ( st.hasMoreTokens() ) {
			//                String indexId = st.nextToken();
			String indexId = currentAtts.getValue(PleadeUtilities.PLEADE_NAMESPACE_URI, "index-id");
			Index idx = (Index)indicesById.get(indexId);
			if (idx == null) {
				Index newIndex = new Index(indexId);
				indicesById.put(indexId, newIndex);
				idx = newIndex;
			}
			idx.addValue(value, (String)currentIds.peek());
			//            }

			// A la fin, on indique qu'on n'est plus dans un element d'indexation
			inIndexElement = false;
		}
		//else if ( localName.equals("ead") ) {
		else if ( PleadeUtilities.isRoot(localName) ) {
			// Fin du document : on sort les fichiers
			try {
				// Boucle sur les index
				Iterator it = indicesById.keySet().iterator();
				while (it.hasNext()) {
					// Index courant
					Index idx = (Index)indicesById.get((String)it.next());
					// Fichier de sortie
					File outFile = new File(getOutputDirectory(), idx.getPreferredFilename());
					// Sortie de l'index
					idx.output(outFile);
				}
				// Sortie de la liste des index disponibles
				XMLSerializer ser = new XMLSerializer();
				if ( this.getLogger() == null ) {
					Logger logger = new NullLogger();
					ser.enableLogging(logger);
				}
				else ser.enableLogging(this.getLogger());

				String dummyConf = "<dummy/>";				// TODO : better configuration here?
				Configuration conf = new DefaultConfigurationBuilder().build(new ByteArrayInputStream(dummyConf.getBytes()));
				ser.configure(conf);
				ser.setOutputStream(new FileOutputStream(new File(getOutputDirectory(), IndexBuilder.OVERVIEW_FILENAME)));
				ser.startDocument();
				AttributesImpl atts = new AttributesImpl();
				atts.addAttribute(IndexBuilder.NAMESPACE, "id", "id", "CDATA", this.documentId);
				atts.addAttribute(IndexBuilder.NAMESPACE, "nb", "nb", "CDATA", "" + indicesById.size());
				ser.startElement(IndexBuilder.NAMESPACE, "indices", "indices", atts);
				it = indicesById.keySet().iterator();
				while (it.hasNext()) {
					// Get the current index
					Index idx = (Index)indicesById.get((String)it.next());
					// Then output the index information
					idx.outputInformation(ser);
				}
				ser.endElement(IndexBuilder.NAMESPACE, "indices", "indices");
				ser.endDocument();
			}
			catch (IOException e) { throw new SAXException(e); }
			catch (ConfigurationException e) { throw new SAXException(e); }
			catch (ParameterException e) { throw new SAXException(e); }
		}
		// We remove the current id in the stack
		if ( PleadeUtilities.isPleadeBreak(this.documentType,localName, currentAtts) ) {
			currentIds.pop();
		}
		super.endElement(namespaceURI, localName, qName);
	}

	/**
	 * Definit le dossier de sortie.
	 * @throws IOException  Si le dossier ne peut etre cree ou ecrit
	 */
	private void setOutputDirectory() throws IOException, ParameterException {
		this.outputDirectory = new File(parameters.getParameter(OUTPUTDIR_PARAMETER_NAME), this.documentId);
		this.outputDirectory = new File(this.outputDirectory, INDEX_SUBDIRECTORY);
		PleadeUtilities.checkDir(this.outputDirectory, true);
	}

	/**
	 * Returns the output directory for this list of indices.
	 * @return
	 */
	private File getOutputDirectory() throws IOException, ParameterException {
		if ( this.outputDirectory == null ) setOutputDirectory();
		return this.outputDirectory;
	}

	/**
	 * Retoure vrai si l'element est un element d'indexation.
	 * @param uri       Le namespace de l'element
	 * @param name      Le nom de l'element
	 * @param atts      La liste des atributs de l'element
	 * @return
	 */
	private boolean isIndexElement(String uri, String name, Attributes atts) {
		if (atts.getValue(PleadeUtilities.PLEADE_NAMESPACE_URI, "index-id") != null && atts.getValue(PleadeUtilities.PLEADE_NAMESPACE_URI, "index-local").equals("true")) return true;
		return false;
	}

}