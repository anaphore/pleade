<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSLT transformant un sdx:terms en SELECT et OPTION XHTML
    |
    | Les paramètres i18n-prefixe et i18n-catalogue permettent d'activer les
    | fonctions i18n, le second étant optionnel par rapport au premier.
    +-->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                exclude-result-prefixes="sdx i18n">

  <xsl:param name="ext" select="''" />
  <xsl:param name="i18n-prefixe" select="''" />
  <xsl:param name="i18n-catalogue" select="''" />
  <xsl:param name="first-option" select="''" />

	<xsl:template match="/sdx:document">
    <xsl:choose>
      <xsl:when test="$ext='txt'">
        <xsl:apply-templates select="sdx:terms/sdx:term"/>
				<xsl:if test="not(sdx:terms/sdx:term)">
					<option value=""><i18n:text key="form-no-entry">aucune entrée</i18n:text></option>
				</xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <select>
          <xsl:apply-templates select="sdx:terms/sdx:term"/>
					<xsl:if test="not(sdx:terms/sdx:term)">
						<option value=""><i18n:text key="form-no-entry">aucune entrée</i18n:text></option>
					</xsl:if>
        </select>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:template>
	<xsl:template match="sdx:term">
    <xsl:if test="position()=1 and $first-option!=''">
      <option value="">
        <i18n:text key="{$first-option}"><xsl:value-of select="$first-option" /></i18n:text>
      </option>
    </xsl:if>
		<option value="{@value}">
      <xsl:choose>
        <xsl:when test="$i18n-prefixe!=''">
          <i18n:text key="{$i18n-prefixe}.{@value}">
            <xsl:if test="$i18n-catalogue!=''">
              <xsl:attribute name="catalogue">
                <xsl:value-of select="$i18n-catalogue" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="concat($i18n-prefixe, '.', @value)"/>
          </i18n:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@value"/>
        </xsl:otherwise>
      </xsl:choose>
    </option>
	</xsl:template>

</xsl:stylesheet>
