#!/bin/sh
# TODO-MAINTAIN: The jar names in the classpath below should be updated as jars change

# -----------------------------------------------------------------------------
# run.sh - Unix Run Script for SDX (not tested)
#
# $Id$
# -----------------------------------------------------------------------------

# ----- Verify and Set Required Environment Variables -------------------------

if [ "$JAVA_HOME" = "" ] ; then
  echo You must set JAVA_HOME to point at your Java Development Kit installation
  exit 1
fi


# ----- Run HSQL Database Manager -----------------------------------------------

# this is an example url for connecting to the internal hsql database
# use the url below or one similar when propmted by the hsql interface
# the user name is "sa" and there is no password
# url:
# jdbc:hsqldb:../../sdxtest/conf/databases/_hsql/

#Database Manager is an AWT Tool for manageing a JDBC database.
#Usage: java DatabaseManager [-options]
#where options include:
#-driver   jdbc driver class
#-url           jdbc url
#-user          username used for connection
#-password  password for this user
#-dir           default directory
#-script        reads from script file


$JAVA_HOME/bin/java -cp ../lib/hsqldb-1.7.1.jar org.hsqldb.util.DatabaseManager -url jdbc:hsqldb:../../sdxtest/conf/databases/_hsql/


