#!/bin/sh
# TODO-MAINTAIN: The jar names in the classpath below should be updated as jars change


# -----------------------------------------------------------------------------
# run.sh - Unix Run Script for Luke - Lucene Index Browser (not tested)
#
# $Id$
# -----------------------------------------------------------------------------

# ----- Verify and Set Required Environment Variables -------------------------

if [ "$JAVA_HOME" = "" ] ; then
  echo You must set JAVA_HOME to point at your Java Development Kit installation
  exit 1
fi

# ----- Luke - Lucene Index Browser -----------------------------------------------

$JAVA_HOME/bin/java -cp ../lib/lucene-patched-20040110.jar:../lib/luke-0.4.jar luke.Luke

