<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT construisant l'XHTML de la partie dynamique d'une page Rubrique
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml pleade">

	<xsl:import href="search.xsl"/>
	<!--<xsl:import href="../../commons/xsl/functions.xsl"/> importé via search.xsl-->

	<xsl:param name="active-basket" select="'true'"/>
	<!-- le nom du fichier de subset  -->
	<xsl:param name="name" select="''"/>
	<!--le subset courant-->
	<xsl:param name="id" select="''"/>
	<!--separateur de Liste pour javascript-->
	<xsl:param name="list-sep" select="''"/>
	<!-- La clé de tri par défaut -->
	<xsl:param name="default-sort-field" select="''" />

	<xsl:variable name="explicit-id">
		<xsl:choose>
			<xsl:when test="$id != ''">
				<xsl:value-of select="$id"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/subset/@id"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="explicit-name">
		<xsl:choose>
				<xsl:when test="$name != ''">
					<xsl:value-of select="$name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>subsets-default</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:apply-templates select=".//subset[@id = $explicit-id]" mode="currentLevel"/>
	</xsl:template>

	<xsl:template match="subset" mode="currentLevel">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<title>
					<i18n:text key="subset.name.id.{@id}.name.{$explicit-name}">subset.name.id.<xsl:value-of select="@id"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
				</title>
					<!--FIXME : xmlns de i18n est ajouté (deux fois!) sur le 1er element sous title-->
					<!--CSS ajoutés par le modele-->
					<!--CSS spécifiques à cette page-->
				<link type="text/css" rel="stylesheet" href="yui/assets/skins/pleade/container.css"/>
						<link type="text/css" rel="stylesheet" href="yui/assets/skins/pleade/autocomplete.css"/>
						<link type="text/css" rel="stylesheet" href="yui/assets/skins/pleade/treeview.css"/>

					<!-- JS ajoutés par le modele (module-eadeac-template-search.html)-->
						<!-- Les JS spécifiques à cette page-->
						<!--<script type="text/javascript" src="yui/treeview/treeview-min.js">//</script>-->
						 <xsl:if test="$active-basket='true'">
							<script type="text/javascript" src="i18n/module-eadeac-basket-js.js">//</script>
							<!--<script type="text/javascript" src="js/module-eadeac-basket.js">//</script>-->
						</xsl:if>
						<script type="text/javascript" src="yui/datasource/datasource-min.js">//</script>
						<script type="text/javascript" src="yui/autocomplete/autocomplete-min.js">//</script>
						<script type="text/javascript" src="i18n/module-eadeac-js.js">//</script>
						<!--<script type="text/javascript" src="js/module-eadeac.js">//</script>-->
						<!--<script type="text/javascript" src="js/module-eadeac-search-form.js">//</script>-->
						
					</head>
			<body>

				<div class="yui-skin-pleade">
					<!-- Un titre pour le contenu de la page -->
					<h1 class="pl-title">
						<i18n:text key="subset.header.id.{@id}.name.{$explicit-name}">subset.header.id.<xsl:value-of select="@id"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
					</h1>
					<!-- Une description -->
					<i18n:text key="subset.description.id.{@id}.name.{$explicit-name}">subset.description.id.<xsl:value-of select="@id"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
					<div class="pl-subset">
						<div class="pl-subset-links-subsets">
							<!--zone pour les sous catégories-->
							<xsl:choose>
								<xsl:when test="zones/zone/@role='subsets'">
									<xsl:apply-templates select="zones/zone[@role = 'subsets'][1]" mode="zone"/>
									<!-- il ne peut y avoir qu'une seule zone de catégories-->
								</xsl:when>
								<xsl:when test="subset" >
									<xsl:call-template name="make-zone">
										<xsl:with-param name="role" select="'subsets'"/>
										<xsl:with-param name="subset_el" select="self::subset"/>
									</xsl:call-template>
								</xsl:when>
							</xsl:choose>
						</div>
						<div class="pl-subset-links-links">
							<xsl:comment>u</xsl:comment>
							<!--zone de liens-->
							<xsl:choose>
								<xsl:when test="zones/zone/@role='links'">
									<xsl:apply-templates select="zones/zone[@role = 'links'][1]" mode="zone"/>
									<!-- il ne peut y avoir qu'une seule zone de liens-->
								</xsl:when>
								<xsl:when test="*[@display = 'link']" >
									<xsl:call-template name="make-zone">
										<xsl:with-param name="role" select="'links'"/>
										<xsl:with-param name="subset_el" select="self::subset"/>
									</xsl:call-template>
								</xsl:when>
							</xsl:choose>
						</div>
						<div class="pl-subset-in-page">
							<!--zone pour les méthode d'exploration "in-page"-->
							<xsl:apply-templates select="*" mode="in-page"/>
						</div>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="zones"/>

	<xsl:template match="zone[@role = 'subsets']" mode="zone">
		<xsl:call-template name="make-zone">
			<xsl:with-param name="role" select="'subsets'"/>
			<xsl:with-param name="subset_el" select="ancestor::subset[1]"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="zone[@role = 'links']" mode="zone">
		<xsl:call-template name="make-zone">
			<xsl:with-param name="role" select="'links'"/>
			<xsl:with-param name="subset_el" select="ancestor::subset[1]"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="make-zone">
		<xsl:param name="role" select="''"/>
		<xsl:param name="subset_el" select="/"/>
		<xsl:if test="$role != ''">
			<div class="pl-subset-zone">
				<h2>
					<i18n:text key="subset.header-zone.role.{$role}.id.{$subset_el/@id}.name.{$explicit-name}">
						subset.header-zone.role.<xsl:value-of select="$role"/>.id.<xsl:value-of select="$subset_el/@id"/>.name.<xsl:value-of select="$explicit-name"/>
					</i18n:text>
				</h2>
				<xsl:choose>
					<xsl:when test="$role = 'subsets'">
						<xsl:apply-templates select="$subset_el/subset" mode="otherLevel"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="$subset_el/*" mode="link"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</xsl:if>
	</xsl:template>


	<xsl:template match="subset" mode="otherLevel">
		<div class="pl-subset-otherlevel">
			<a href="subset.html?name={$explicit-name}&amp;id={@id}">
				<i18n:text key="subset.header.id.{@id}.name.{$explicit-name}">subset.header.id.<xsl:value-of select="@id"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
			</a>
			<div class="subset_summary">
				<i18n:text key="subset.summary.id.{@id}.name.{$explicit-name}">subset.summary.id.<xsl:value-of select="@id"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="*" mode="link"/>
	<xsl:template match="*" mode="in-page"/>


	<!--FORMS-->

	<xsl:template match="forms" mode="link">
		<xsl:apply-templates mode="link" />
	</xsl:template>

	<xsl:template match="form[@display = 'link']" mode="link">
		<div class="pl-subset-form-link">
			<!-- FIXME : critère de recherche pré-rempli : pleade2 => req=subset:collections/* -->
			<a href="search-form.html?name={@ref}&amp;h_champ1=subsetall&amp;h_query1={ancestor::subset[1]/@id}&amp;h_cop1=AND">
				<xsl:apply-templates select="." mode="i18n-header"/>
			</a>
			<!--<a href="search-form.html?name={@ref}&amp;champ1=subsetall&amp;query1={ancestor::subset[1]/@id}&amp;cop1=AND&amp;n-start=1"/>
			ne fonctionnerait pas car on serait dans un mode ou le formulaire se remplirait uniquement selon les valeurs d'url (plutôt que les valeurs par défaut) et ce n'est pas ce qu'on veut ici	-->
			<xsl:if test="@display-information = 'true' or @display-information = '' or not(@display-information)">
				<div class="subset_summary">
					<xsl:apply-templates select="." mode="i18n-intro"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="forms" mode="in-page">
		<xsl:apply-templates mode="in-page"/>
	</xsl:template>

	<xsl:template match="form[@display = 'in-page']" mode="in-page">
		<xsl:if test="@display-information = 'true' or @display-information = '' or not(@display-information)">
			<div class="header-list-documents">
				<xsl:apply-templates select="." mode="i18n-header"/>
			</div>
			<xsl:apply-templates select="." mode="i18n-intro"/>
		</xsl:if>
		<xsl:variable name="form-url" select="concat('cocoon:/functions/get-search-form.xml?name=',@ref)"/>
		<xsl:choose>
			<xsl:when test="doc-available($form-url)">
				<xsl:variable name="form-doc" select="document($form-url)"/>
				<xsl:choose>
					<xsl:when test="$form-doc/form">
						<xsl:variable name="form-id" select="@ref"/>
						<!--Affichage du formulaire-->
						<xsl:apply-templates select="$form-doc/form" mode="javascript-init"/>
						<!--FIXME : liste des champs cachés à ajouter comme dans pleade2 ?
						<input type="hidden" value="collection-cat" name="id"/>
						<input type="hidden" value="fa" name="base"/>
						<input type="hidden" value="subset:collections/*" name="req"/>-->
						<xsl:apply-templates select="$form-doc/form" mode="search-form">
							<xsl:with-param name="form-id" select="@ref"/>
							<xsl:with-param name="str-paramsCriteria" select="concat('champ1=subsetall&amp;query1=', ancestor::subset[1]/@id,'&amp;cop1=AND')"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<!--FIXME : même si le formulaire n'existe pas, la fonction get-search-form renvoie le formulaire par défaut, donc on ne passe jamais ici...
						mettre un document-available() plutôt				-->
						erreur accès formulaire <xsl:value-of select="@ref"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>[subset.xsl] document <xsl:value-of select="$form-url"/> n'est pas accessible </xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="form" mode="i18n-header">
		<xsl:variable name="header-key" select="string(header/@i18n-key)"/>
		<xsl:choose>
			<xsl:when test="$header-key != ''">
				<i18n:text key="{$header-key}"><xsl:value-of select="$header-key"/></i18n:text>
			</xsl:when>
			<xsl:otherwise>
				<i18n:text key="form.header.search-form.{@ref}.name.{$explicit-name}">header.search-form.<xsl:value-of select="@ref"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="form" mode="i18n-intro">
		<xsl:variable name="intro-key" select="string(intro/@i18n-key)"/>
		<xsl:choose>
			<xsl:when test="$intro-key != ''">
				<i18n:text key="{$intro-key}"><xsl:value-of select="$intro-key"/></i18n:text>
			</xsl:when>
			<xsl:otherwise>
				<i18n:text key="form.intro.search-form.{@ref}.name.{$explicit-name}">intro.search-form.<xsl:value-of select="@ref"/>.name.<xsl:value-of select="$explicit-name"/></i18n:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!--LIST-DOCUMENTS-->
	<xsl:template match="list-documents[@display = 'link']" mode="link">
		<div class="pl-subset-documents-link">
			<a href="list-results.html?mode=subset&amp;champ1=subsetall&amp;query1={parent::subset/@id}&amp;cop1=AND">
				<i18n:text key="subset.header-list-documents.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
					subset.header-list-documents.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
				</i18n:text>
			</a>
			<xsl:if test="@display-information = 'true' or @display-information = '' or not(@display-information)">
				<div class="subset_summary">
					<i18n:text key="subset.intro-list-documents.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
						subset.intro-list-documents.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
					</i18n:text>
				</div>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="list-documents[@display = 'in-page']" mode="in-page">
		<xsl:if test="@display-information = 'true' or @display-information = '' or not(@display-information)">
			<div class="list-documents-title">
				<h2>
				<i18n:text key="subset.header-list-documents.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
						subset.header-list-documents.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
					</i18n:text>
				</h2>
			</div>
			<i18n:text key="subset.intro-list-documents.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
				subset.intro-list-documents.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
			</i18n:text>
		</xsl:if>
		<!-- affichage de la liste des documents -->
		<xsl:variable name="results-url">
			<xsl:text>cocoon://search-top-docs.xsp?</xsl:text>
			<xsl:text>champ1=subsetall&amp;query1=</xsl:text>
			<xsl:value-of select="ancestor::subset[1]/@id"/>
			<xsl:if test="@hpp"><!-- FIXME (MP) : Sert a rien ? car on ne recupere que le javascript dans ce pipeline -->
				<xsl:text>&amp;hpp=</xsl:text>
				<xsl:value-of select="@hpp"/>
			</xsl:if>
			<xsl:value-of select="concat( '&amp;sf=', (if(@sort-field!='') then @sort-field else $default-sort-field),
																		if(@sort-order!='') then concat('&amp;so=', @sort-order) else '')" />
		</xsl:variable>
		<!--DEBUG-->
		<!-- <xsl:message>[subset.xsl]results-url = <xsl:value-of select="$results-url"/></xsl:message> -->

		<div class="list-documents">
			<div id="list-documents_subset_{ancestor::subset[1]/@id}">
				<xsl:comment>unclosed</xsl:comment>
				<xsl:choose>
					<xsl:when test="doc-available($results-url)">
						<xsl:copy-of select="document($results-url)/*[1]" copy-namespaces="no" />
						<!-- <xsl:message>[subset.xsl]<xsl:choose><xsl:when test="doc-available(replace($results-url, '.xsp', '.debug'))"><xsl:copy-of select="document(replace($results-url, '.xsp', '.debug'))" copy-namespaces="no" /></xsl:when><xsl:otherwise>Doc non valable</xsl:otherwise></xsl:choose></xsl:message> -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>[subset.xsl] document <xsl:value-of select="$results-url"/> n'est pas accessible</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>

	<!--LIST-UNITS-->

	<xsl:template match="list-units[@display = 'link']" mode="link">
		<div class="pl-subset-units-link">
			<a href="results.html?champ1=subsetall&amp;query1={parent::subset/@id}&amp;cop1=AND">
			<!--FIXME : différence avec le lien list-documents ?-->
				<i18n:text key="subset.header-list-units.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
					subset.header-list-units.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
				</i18n:text>
			</a>
			<div class="subset_summary">
				<i18n:text key="subset.intro-list-units.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
					subset.intro-list-units.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
				</i18n:text>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="list-units[@display = 'in-page']" mode="in-page">
		<xsl:if test="@display-information = 'true' or @display-information = '' or not(@display-information)">
			<div class="list-units-title">
				<h2>
					<i18n:text key="subset.header-list-units.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
						subset.header-list-units.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
					</i18n:text>
				</h2>
			</div>
			<i18n:text key="subset.intro-list-units.display.{@display}.id.{ancestor::subset[1]/@id}.name.{$explicit-name}">
				subset.intro-list-units.display.<xsl:value-of select="@display"/>.id.<xsl:value-of select="ancestor::subset[1]/@id"/>.name.<xsl:value-of select="$explicit-name"/>
			</i18n:text>
		</xsl:if>
		<!--affichage des résultats de recherche-->
		<xsl:variable name="results-url">
			<xsl:text>cocoon:/functions/ead/get-cdc-results-script.xml?divID=list-documents_subset_</xsl:text>
			<xsl:value-of select="ancestor::subset[1]/@id"/>
			<xsl:text>&amp;champ1=subsetall&amp;query1=</xsl:text>
			<xsl:value-of select="ancestor::subset[1]/@id"/>
			<xsl:text>&amp;op1=AND&amp;cop1=AND</xsl:text>
			<xsl:if test="@hpp"><!-- FIXME (MP) : Sert a rien ? car on ne recupere que le javascript dans ce pipeline -->
				<xsl:text>&amp;hpp=</xsl:text>
				<xsl:value-of select="@hpp"/>
			</xsl:if>
		</xsl:variable>
		<!--DEBUG-->
		<!--<xsl:message>[subset.xsl]results-url = <xsl:value-of select="$results-url"/></xsl:message>-->

		<!--FIXME : les script seraient plutôt a mettre une seule fois dans le html/head-->
		<xsl:choose>
			<xsl:when test="doc-available($results-url)">
				<xsl:copy-of select="document($results-url)/xhtml:div/*" copy-namespaces="no" />
				<!--<xsl:message>[subset.xsl]<xsl:choose><xsl:when test="doc-available(replace($results-url, '.xml', '.debug'))"><xsl:copy-of select="document(replace($results-url, '.xml', '.debug'))" copy-namespaces="no" /></xsl:when><xsl:otherwise>Doc non valable</xsl:otherwise></xsl:choose></xsl:message>-->
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>[subset.xsl] document <xsl:value-of select="$results-url"/> n'est pas accessible</xsl:message>
			</xsl:otherwise>
		</xsl:choose>

		<div id="list-units_subset_{ancestor::subset[1]/@id}">
			<xsl:comment>unclosed</xsl:comment>
		</div>
	</xsl:template>



</xsl:stylesheet>
