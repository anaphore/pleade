<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  xmlns:dir="http://apache.org/cocoon/directory/2.0"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsl i18n pleade sdx fn dir xs">

  <!--
    CETTE TRANSFORMATION GENERE LE HTML A PARTIR DU EAD. ELLE EST UTILISEE STATIQUE ET EN DYNAMIQUE

    ** EN STATIQUE $mode = "static" **
        $full = "false"
          - est appelée depuis ead2html-index.xsl et s'applique sur document($eadDoc.path)
          - créer tous les fragments html
        $full = "true"
          - s'applique directement à eadDoc.xml (préindexé par pleade : preprocessing)
          - créer le doc complet.html

    ** EN DYNAMIQUE $mode = "dyn" **
      - s'applique sur cocoon:/functions/ead/get-fragment/{request-param:c}.xml qui contient le fragment désiré sous <pleade:subdoc>
      - créer dynamiquement un fragment.html (appel ajax de la page cocoon:/functions/ead/get-toc-fragment/*/*.json)
  -->

  <xsl:import href="common.xsl"/>
  <xsl:import href="../../../../commons/xsl/functions.xsl"/>
  <xsl:import href="../../link-helpers.xsl"/>
  <xsl:include href="ead-isbd2html.xsl"/>
  <xsl:include href="ead2html-toolskit.xsl"/>

  <xsl:param name="full" select="'false'"/><!-- ('true' | 'false') -->
  <xsl:param name="mode" select="'static'"/><!-- ('static' | 'dyn') -->

  <xsl:param name="active-basket" select="'false'"/><!-- Activer le panier -->
  <xsl:param name="active-pdf" select="'false'"/><!-- Activer l'export PDF -->
  <xsl:param name="active-eac" select="'false'"/><!-- Activer la recherche dans la base EAC -->
  <xsl:param name="img-viewer-base" select="'img-viewer/'"/><!-- URL relative de la visionneuse -->
  <xsl:param name="internal-img-server-base" select="'cocoon://img-server/'"/><!-- URL interne du serveur d'image -->
  <xsl:param name="public-img-server-base" select="'img-server/'"/><!-- URL relative du serveur d'images -->
  <xsl:param name="img-thumbnail-size" select="'150'"/><!-- Taille des vignettes. Ce paramètre est utilisé pour la largeur *et* la hauteur.  -->
  <xsl:param name="ead-archdesc-toc" select="'true'"/><!-- Afficher le sommaire -->
  <xsl:param name="active-synch-toc" select="'true'"/><!-- Synchronisation du sommaire -->
  <xsl:param name="public-url" select="''"/><!-- URL publique de l'application -->
  <xsl:param name="supported-video-formats"/> <!-- Formats vidéo supportés (séparés par une virgule) -->
  <xsl:param name="supported-audio-formats"/> <!-- Formats audio supportés (séparés par une virgule) -->

  <!-- Un flag pour le débogage -->
  <xsl:param name="debug" select="'false'"/>

  <!-- Indique si la sortie est pour le PDF ou pas (doit être 'true' pour que ce le soit)-->
  <xsl:param name="for-pdf"/>

  <!--L'identifiant de la recherche le cas échéant-->
  <xsl:param name="qid" select="''"/>

  <!--output par défaut pour générer le document complet  -->
  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"
      doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <!--output pour générer les documents fragment en statique avec xsl:result-document-->
  <xsl:output name="fragment" method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes" />

  <xsl:variable name="theme-relPath" select="'../_theme'"/>
  <!--cet url relative est valable uniquement en statique, mais comme $theme-relPath n'est pas appelé en dynamique
  alors il n'est pas nécessaire de décliner la définition de $theme-relPath-->

  <!-- Définit la variable pour les dimensions finales des vignettes.
       Si le paramètre reçu n'est pas numérique, on utilise la valeur par défaut : 150.-->
  <xsl:variable name="active-img-thumbnail-size"
                select="if($img-thumbnail-size castable as xs:integer)
                            then number($img-thumbnail-size)
                        else number(150)" />

  <!-- L'identifiant du document -->
  <xsl:variable name="eadid">
    <xsl:choose>
      <xsl:when test="$mode = 'dyn'">
        <xsl:value-of select="/sdx:document/pleade:subdoc/pleade:ancestors/@pleade:eadid"/>
      </xsl:when>
      <xsl:otherwise><!-- TODO --></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Les propriétés d'affichage -->
  <xsl:variable name="prop-url" select="concat('cocoon:/functions/ead/get-properties/', $eadid, '/display.xml')" />
  <xsl:variable name="display-properties" select="
      if($cdc castable as xs:boolean and xs:boolean($cdc)) then ''
      else if(doc-available($prop-url)) then document($prop-url)/properties
      else ''
    "/>

    <!-- Un flag pour le débogage -->
    <xsl:variable name="isDebug" select="
      if($debug castable as xs:boolean) then xs:boolean($debug)
      else false()
    "/>

  <!--+
      | Traitement de la racine
      +-->
  <xsl:template match="/">
    <!-- Une info pour le dev, ca sort dans la console Tomcat. -->
    <xsl:if test="( not($cdc castable as xs:boolean) or not(xs:boolean($cdc)) ) and ($display-properties='' or not($display-properties/*))"><xsl:message>[<xsl:value-of select="d:to-string(d:new())" xmlns:d="java:java.util.Date"/>][PLEADE][ead2html:line 121] Erreur "display-properties" est vide pour eadid: "<xsl:value-of select="$eadid" />"</xsl:message></xsl:if>
    <xsl:choose>
      <xsl:when test=" $mode = 'static' ">
        <!--$full = true ou false ne change rien ici, on génère de toute façon le document complet. Si $full = false,
        alors le xsl:result-document générera tous les fragments et le document complet sera effacé (cf. build.xml)-->
        <xsl:apply-templates /> <!--s'applique toujours à /ead-->
      </xsl:when>
      <xsl:when test=" $mode = 'dyn' "> <!--on ne génère que le fragment considéré par cocoon:eadDoc.xml?id=-->
        <div class="pl-pgd-subdoc{if($cdc='true') then ' pl-pgd-cdc' else ''}">
          <xsl:apply-templates select="/sdx:document/sdx:exception[@code='2000']"/>
          <xsl:if test="$for-pdf = 'true'">
            <xsl:copy-of select="sdx:document/pleade:subdoc/@pleade:*"/>
          </xsl:if>

          <!-- Document Précédent/Suivant -->
          <xsl:if test="$qid != '' and sdx:document/sdx:navigation/*[self::sdx:previous or self::sdx:next]">
            <div class="navigation-results">
              <p>
                <span class="nbpages"><i18n:text key="results.docs.browse" catalogue="module-eadeac">naviguer dans les résultats de la recherche : </i18n:text></span>
                <span id="{$eadid}_browse" class="navpage">
                  <xsl:if test="sdx:document/sdx:navigation/sdx:previous">
                    <a href="?id={$eadid}&amp;c={sdx:document/sdx:navigation/sdx:previous/@id}&amp;qid={$qid}" title="module-eadeac:results.docs.previous.title" i18n:attr="title"><i18n:text key="results.docs.previous" catalogue="module-eadeac">&lt;</i18n:text></a>
                  </xsl:if>
                  <xsl:text>&#160;</xsl:text>
                  <!--<xsl:if test="sdx:document/sdx:navigation/sdx:previous and sdx:document/sdx:navigation/sdx:next">
                    <xsl:text> - </xsl:text>
                  </xsl:if>-->
                  <xsl:if test="sdx:document/sdx:navigation/sdx:next">
                    <a href="?id={$eadid}&amp;c={sdx:document/sdx:navigation/sdx:next/@id}&amp;qid={$qid}" title="module-eadeac:results.docs.next.title" i18n:attr="title"><i18n:text key="results.docs.next" catalogue="module-eadeac">&gt;</i18n:text></a>
                  </xsl:if>
                </span>
              </p>
            </div>
          </xsl:if>

          <xsl:choose>
            <xsl:when test="sdx:document/pleade:subdoc"> <!--composant -->
              <xsl:apply-templates select="sdx:document/pleade:subdoc/*"/>
            </xsl:when>
            <xsl:when test="sdx:document/c">  <!-- FIXME: comme ça pour les eadheader -->
                <xsl:variable name="id">
                <xsl:choose>
                  <xsl:when test="@pleade:id"><xsl:value-of select="@pleade:id" /></xsl:when>
                  <xsl:when test="did/@pleade:id"><xsl:value-of select="did/@pleade:id" /></xsl:when>
                </xsl:choose>
                </xsl:variable>
                <div id="{$id}">
                  <xsl:apply-templates select="sdx:document/c/*"/>
                  <xsl:call-template name="make-children-block-dyn"/>
                  <!-- <xsl:for-each select="sdx:document/c">
                    <xsl:call-template name="make-ancestors-block-dyn"/>
                    <xsl:call-template name="make-component-block"/>
                    <xsl:call-template name="make-children-block-dyn"/>
                  </xsl:for-each> -->
                </div>
              <!-- <xsl:apply-templates select="sdx:document/c/*"/> -->
            </xsl:when>
            <!-- Cas du cadre de classement : on a directement un c -->
            <xsl:when test="c">
              <xsl:apply-templates select="c"/>
            </xsl:when>
            <xsl:otherwise> <!--eadheader-->
              <xsl:apply-templates select="sdx:document/header/eadheader"/>
            </xsl:otherwise>
          </xsl:choose>
          <!-- Pour les Notes EAD façon Panel YUI
          <script typex="text/javascript">
            initNotes();
          </script> -->
          <xsl:if test="not($cdc = 'true') and not(/sdx:document/sdx:exception[@code='2000'])">
            <xsl:comment>FlowPlayer</xsl:comment>
            <xsl:call-template name="flashError"/>
            <script type="text/javascript">
              flowplayer("a.pl-flow-video", {
                  src: "theme/flowplayer/flowplayer-3.2.7.swf",
                  w3c: true,
                  onFail: function() {
                    eadWindow.contentManager.isFlashError('video');
                  }
                }, {clip: {
                  autoPlay: false,
                  autoBuffering: true,
                  scaling: 'orig'
                }
              });
              flowplayer("a.pl-flow-audio", {
                  src:"theme/flowplayer/flowplayer-3.2.7.swf",
                  w3c: true,
                  onFail: function() {
                    eadWindow.contentManager.isFlashError('audio');
                  }
                }, {
                plugins: {
                  controls: {
                    fullscreen: false,
                    height: 20,
                    autoHide: false
                  }
                },
                clip: {
                  autoPlay: false,
                  autoBuffering: true
                }
              });
            </script>
          </xsl:if>
        </div>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!--ne rien faire lorsque ce n'est pas dit explicitement par un template-->
  <xsl:template match="*" />

  <!-- A titre d'auto-documentation, on précisera dans le chemin des match les parents quand ceux-ci sont uniques,
  sauf ead/eadheader et ead/frontmatter, qui pourront être matché directement (sans parent ead) en dynamique-->

  <xsl:template match="ead"> <!-- Conteneur (eadheader,frontmatter?,archdesc)-->
  <!--ne sera jamais appelé en dynamique (seuls les fragments sont traités)-->
    <html>
      <head>
        <!-- TODO: mettre le titre de l'IR (template à faire, utiliser titlestmt... voire subtitle, à décider)-->
        <title>[todo]:mettre le titre de l'IR</title>
        <!-- Décider de l'organisation des fichiers ressources -->
        <!--YUI-->
        <xsl:comment>YUI</xsl:comment>
        <script type="text/javascript" src="{$theme-relPath}/yui/utilities/utilities.js">&#160;</script>
        <script type="text/javascript" src="{$theme-relPath}/yui/container/container-min.js">&#160;</script>
        <link rel="stylesheet" type="text/css" href="{$theme-relPath}/yui/assets/skins/pleade/container.css" />
        <!--pleade-->
        <xsl:comment>pleade</xsl:comment>
        <script type="text/javascript" src="{$theme-relPath}/js/ead.js">&#160;</script>
        <link href="{$theme-relPath}/css/pleade.css" type="text/css" rel="stylesheet" />
        <link href="{$theme-relPath}/css/module-eadeac.css" type="text/css" rel="stylesheet" />
        <link href="{$theme-relPath}/css/module-ead-xslt.css" type="text/css" rel="stylesheet" />
      </head>
      <body>
        <xsl:choose>
          <xsl:when test="$full = 'false'">
            <!--genère le fragment du eadheader -->
            <xsl:result-document href="fragments/{eadheader/@pleade:id}.html" format="fragment">
              <xsl:apply-templates select="eadheader"/>
            </xsl:result-document>
            <!--genère le fragment du frontmatter si présent-->
            <xsl:if test="frontmatter">
              <!--FIXME : problème sur le frontmatter/@pleade:id qui n'a pas la bonne valeur-->
              <!--<xsl:message>frontmatter/@pleade:id : <xsl:value-of select="frontmatter/@pleade:id"/></xsl:message>-->
              <!--<xsl:result-document href="fragments/{frontmatter/@pleade:id}.html" format="fragment">-->
              <xsl:result-document href="fragments/{@pleade:id}-frontmatter.html" format="fragment">
                <xsl:apply-templates select="frontmatter"/>
              </xsl:result-document>
            </xsl:if>
            <!--génère les fragments des composants (cf. match="archdesc | c | c01 |...")-->
            <xsl:apply-templates/>
          </xsl:when>
          <xsl:when test="$full = 'true'">
            <xsl:apply-templates />
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="error">
              <xsl:with-param name="key" select="'ead-error-full-value-unrecognized'"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </body>
    </html>
  </xsl:template>

  <!-- ***************************************************************************** -->
                                      <!-- EADHEADER -->
  <!-- ***************************************************************************** -->

  <xsl:template match="eadheader"><!--  Conteneur : (eadid,filedesc,profiledesc?,revisiondesc?) -->
    <!--page de titre-->
    <xsl:call-template name="display-as-block"/>
  </xsl:template>

  <!--*************************** EADID ***************************-->
  <xsl:template match="eadid"><!-- Champ – valeur : (#PCDATA) -->
    <!--affiché en entête-->
    <table class="pl-pgd-fv pl-ead-eadid">
      <xsl:call-template name="i18n-attribute">
        <xsl:with-param name="attribute-name" select="'summary'"/>
        <xsl:with-param name="key" select="'ead-eadid-table-@summary'"/>
      </xsl:call-template>
      <caption>
        <xsl:call-template name="get-i18n-message">
          <xsl:with-param name="key" select="'eadid-table-caption'"/>
        </xsl:call-template>
      </caption>

        <!--Identifiant Pleade FIXME: vraiment nécessaire? Si oui, comment aller le chercher? -->
      <!-- <tr>
        <th scope="row" class="pl-tbl-th">
          <xsl:call-template name="get-i18n-message">
            <xsl:with-param name="key" select="'eadid-table-pleade-id'"/>
          </xsl:call-template>
        </th>
        <td>
          <xsl:value-of select="/ead/@pleade:id"/>
        </td>
      </tr> -->

      <!--Identifiant principal-->
      <tr>
        <th scope="row" class="pl-tbl-th">
          <span class="pl-tbl-th-sp">
            <xsl:call-template name="get-label"/>
          </span>
        </th>
        <td>
          <xsl:value-of select="."/>
          <!--<xsl:value-of select="text()"/> ne suffit pas car il peut y avoir un sdx:hilite sur une partie du eadid-->
        </td>
      </tr>

      <xsl:if test="@publicid">
          <!--Identifiant public-->
        <tr>
          <th scope="row" class="pl-tbl-th">
            <span class="pl-tbl-th-sp">
              <xsl:call-template name="get-i18n-message">
                <xsl:with-param name="key" select="'eadid-table-publicid'"/>
              </xsl:call-template>
            </span>
          </th>
          <td>
            <xsl:value-of select="@publicid"/>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="@identifier">
          <!--Identifiant (@identifier)-->
        <tr>
          <th scope="row" class="pl-tbl-th">
            <span class="pl-tbl-th-sp">
              <xsl:call-template name="get-i18n-message">
                <xsl:with-param name="key" select="'eadid-table-identifier'"/>
              </xsl:call-template>
            </span>
          </th>
          <td>
            <xsl:value-of select="@identifier"/>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="@url">
          <!--url-->
        <tr>
          <th scope="row" class="pl-tbl-th">
            <span class="pl-tbl-th-sp">
              <xsl:call-template name="get-i18n-message">
                <xsl:with-param name="key" select="'eadid-table-url'"/>
              </xsl:call-template>
            </span>
          </th>
          <td>
            <xsl:value-of select="@url"/>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="@urn">
          <!--urn-->
        <tr>
          <th scope="row" class="pl-tbl-th">
            <span class="pl-tbl-th-sp">
              <xsl:call-template name="get-i18n-message">
                <xsl:with-param name="key" select="'eadid-table-urn'"/>
              </xsl:call-template>
            </span>
          </th>
          <td>
            <xsl:value-of select="@urn"/>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="@countrycode">
          <!--Code de pays-->
        <tr>
          <th scope="row" class="pl-tbl-th">
            <span class="pl-tbl-th-sp">
              <xsl:call-template name="get-i18n-message">
                <xsl:with-param name="key" select="'eadid-table-countrycode'"/>
              </xsl:call-template>
            </span>
          </th>
          <td>
            <xsl:value-of select="@countrycode"/>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="@mainagencycode">
        <!--Agence responsable-->
        <tr>
          <th scope="row" class="pl-tbl-th">
            <span class="sp-pl-tbl-th">
              <xsl:call-template name="get-i18n-message">
                <xsl:with-param name="key" select="'eadid-table-mainagencycode'"/>
              </xsl:call-template>
            </span>
          </th>
          <td>
            <xsl:value-of select="@mainagencycode"/>
          </td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>

  <!--*************************** FILEDESC ***************************-->

  <xsl:template match="filedesc"> <!-- Conteneur : (titlestmt,editionstmt?,publicationstmt?,seriesstmt?,notestmt?)-->
    <table class="pl-pgd-fv pl-ead-filedesc">
      <xsl:call-template name="i18n-attribute">
        <xsl:with-param name="attribute-name" select="'summary'"/>
        <xsl:with-param name="key" select="'ead-filedesc-table-@summary'"/>
      </xsl:call-template>
      <caption>
        <xsl:call-template name="get-label"/>
      </caption>
      <xsl:apply-templates />
    </table>
  </xsl:template>

  <!-- ********** titlestmt ********** -->
  <xsl:template match="filedesc/titlestmt"> <!--(titleproper+,subtitle*,author?,sponsor?)-->
    <tr class="pl-pgd-eadheader-subtitle pl-ead-titlestmt">
      <th colspan="2" scope="colgroup">
        <xsl:call-template name="get-label"/>
      </th>
    </tr>
    <!--Titre propre-->
    <tr class="pl-ead-titleproper">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-label">
            <xsl:with-param name="el" select="titleproper"/>
          </xsl:call-template>
        </span>
      </th>
      <td>
        <xsl:choose>
          <xsl:when test="count(titleproper) > 1">
            <ul>
              <xsl:apply-templates select="titleproper"/>
            </ul>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="titleproper"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
    <!--Sous-titre-->
    <xsl:if test="subtitle">
      <tr class="pl-ead-subtitle">
        <th scope="row" class="pl-tbl-th">
          <span class="pl-tbl-th-sp">
            <xsl:call-template name="get-label">
              <xsl:with-param name="el" select="subtitle"/>
            </xsl:call-template>
          </span>
        </th>
        <td>
          <xsl:choose>
            <xsl:when test="count(subtitle) > 1">
              <ul>
                <xsl:apply-templates select="subtitle"/>
              </ul>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select="subtitle"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
    </xsl:if>
    <!--auteur-->
    <xsl:if test="author">
      <tr class="pl-ead-author">
        <th scope="row" class="pl-tbl-th">
          <span class="pl-tbl-th-sp">
            <xsl:call-template name="get-label">
              <xsl:with-param name="el" select="author"/>
            </xsl:call-template>
          </span>
        </th>
        <td>
          <xsl:apply-templates select="author"/>
        </td>
      </tr>
    </xsl:if>
    <!--commanditaire-->
    <xsl:if test="sponsor and normalize-space(sponsor) != ''">
      <tr class="pl-ead-sponsor">
        <th scope="row" class="pl-tbl-th">
          <span class="pl-tbl-th-sp">
            <xsl:call-template name="get-label">
              <xsl:with-param name="el" select="sponsor"/>
            </xsl:call-template>
          </span>
        </th>
        <td>
          <xsl:apply-templates select="sponsor"/>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template match="filedesc/titlestmt[ count(titleproper) > 1 ]/titleproper"> <!--titlestmt/titleproper ?-->
    <!-- FIXME : voir les autres contextes -->
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>
  <xsl:template match="filedesc/titlestmt[ count(subtitle) > 1 ]/subtitle"> <!--titlestmt/subtitle ?-->
    <!-- FIXME : voir les autres contextes-->
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>
  <xsl:template match="filedesc/titlestmt/author"> <!--titlestmt/author ?-->
    <!-- FIXME : voir les autres contextes-->
    <xsl:apply-templates />
  </xsl:template>
  <xsl:template match="filedesc/titlestmt/sponsor"> <!--titlestmt/sponsor ?-->
    <!-- FIXME : voir les autres contextes-->
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="filedesc/editionstmt"> <!--conteneur : (edition|p)+-->
    <tr class="pl-pgd-eadheader-subtitle pl-ead-editionstmt">
      <th colspan="2" scope="colgroup" class="pl-tbl-th">
        <xsl:call-template name="get-label"/>
      </th>
    </tr>
    <tr>
      <th scope="row" class="pl-tbl-th">&#xA0;</th>
      <td>
        <xsl:apply-templates /><!-- FIXME -->
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/editionstmt/edition"> <!--bloc : (#PCDATA|ptr|extptr|emph|lb)*-->
    <!-- FIXME : voir les autres contextes + css-->
    <xsl:call-template name="display-as-block">
      <xsl:with-param name="out-el-name" select="'p'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="filedesc/editionstmt/p"> <!--bloc : (#PCDATA|ptr|extptr|emph|lb)*-->
    <!-- FIXME : voir les autres contextes + css-->
    <xsl:call-template name="display-as-block">
      <xsl:with-param name="out-el-name" select="'p'"/>
    </xsl:call-template>
  </xsl:template>

  <!-- ********** publicationstmt ********** -->

  <xsl:template match="filedesc/publicationstmt"> <!--(publisher|date|address|num|p)+-->
    <tr class="pl-pgd-eadheader-subtitle pl-ead-publicationstmt">
      <th colspan="2" scope="colgroup" class="pl-tbl-th">
        <xsl:call-template name="get-label"/>
      </th>
    </tr>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="filedesc/publicationstmt/publisher">
    <tr class="pl-ead-publisher">
      <th class="pl-tbl-th">
        <xsl:call-template name="get-label"/>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/publicationstmt/date">
    <tr class="pl-ead-date">
      <th class="pl-tbl-th">
        <xsl:call-template name="get-i18n-message">
          <xsl:with-param name="key" select="'ead-publicationstmt-date'"/>
        </xsl:call-template>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/publicationstmt/address">
    <tr class="pl-ead-address">
      <th class="pl-tbl-th">
        <xsl:call-template name="get-i18n-message">
          <xsl:with-param name="key" select="'ead-publicationstmt-address'"/>
        </xsl:call-template>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/publicationstmt/num">
    <tr class="pl-ead-num">
      <th class="pl-tbl-th">
        <!--FIXME --> {?}
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/publicationstmt/p">
    <tr class="pl-ead-p">
      <th class="pl-tbl-th">&#xA0;</th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <!-- ********** seriesstmt ********** -->

  <xsl:template match="filedesc/seriesstmt"><!-- (titleproper|num|p)+ -->
    <tr class="pl-pgd-eadheader-subtitle pl-ead-seriesstmt">
      <th colspan="2" scope="colgroup" class="pl-tbl-th">
        <xsl:call-template name="get-label"/>
      </th>
    </tr>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="filedesc/seriesstmt/titleproper">
    <tr class="pl-ead-titleproper">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-i18n-message">
            <xsl:with-param name="key" select="'ead-seriesstmt-titleproper'"/>
          </xsl:call-template>
        </span>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/seriesstmt/num">
    <tr class="pl-ead-num">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-i18n-message">
            <xsl:with-param name="key" select="'ead-seriesstmt-num'"/>
          </xsl:call-template>
        </span>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/seriesstmt/p">
    <tr class="pl-ead-p">
      <th scope="row" class="pl-tbl-th">&#xA0;</th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <!-- ********** notestmt ********** -->

  <xsl:template match="filedesc/notestmt"><!-- (note)+-->
    <tr class="pl-pgd-eadheader-subtitle pl-ead-notestmt">
      <th colspan="2" scope="colgroup" class="pl-tbl-th">
        <xsl:call-template name="get-label"/>
      </th>
    </tr>
    <tr>
      <th scope="row" class="pl-tbl-th">&#xA0;</th>
      <td>
        <xsl:apply-templates /><!-- FIXME -->
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="filedesc/notestmt/note">
    <xsl:call-template name="display-as-block"/>
  </xsl:template>

  <!-- PROFILEDESC ***************************-->

  <xsl:template match="profiledesc"><!-- Conteneur : (creation?,langusage?,descrules?)-->
    <xsl:if test="*">
      <table class="pl-pgd-fv pl-ead-profiledesc">
        <xsl:call-template name="i18n-attribute">
          <xsl:with-param name="attribute-name" select="'summary'"/>
          <xsl:with-param name="key" select="'ead-profiledesc-table-@summary'"/>
        </xsl:call-template>
        <caption>
          <xsl:call-template name="get-label"/>
        </caption>
        <xsl:apply-templates/>
      </table>
    </xsl:if>
  </xsl:template>

  <xsl:template match="profiledesc/creation"> <!-- Champ – valeur : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref|title|archref|date)*-->
    <tr class="pl-ead-creation">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-label"/>
        </span>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="profiledesc/langusage"> <!-- Champ – valeur : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref|title|archref|language)* -->
    <tr class="pl-ead-langusage">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-label"/>
        </span>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="profiledesc/descrules"> <!-- Champ – valeur : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref|title|archref)*-->
    <tr class="pl-ead-descrules">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-label"/>
        </span>
      </th>
      <td>
        <xsl:apply-templates />
      </td>
    </tr>
  </xsl:template>

  <!-- REVISIONDESC ***************************-->

  <xsl:template match="revisiondesc"> <!--Conteneur avec étiquette : (list|change+)-->
    <table class="pl-pgd-fv pl-ead-revisiondesc">
      <xsl:call-template name="i18n-attribute">
        <xsl:with-param name="attribute-name" select="'summary'"/>
        <xsl:with-param name="key" select="'ead-revisiondesc-table-@summary'"/>
      </xsl:call-template>
      <caption>
        <xsl:call-template name="get-label"/>
      </caption>
      <xsl:choose>
        <xsl:when test="list">
          <tr class="pl-ead-list">
            <td>
              <xsl:apply-templates />
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise> <!--change-->
          <xsl:apply-templates />
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="revisiondesc/change"> <!--Conteneur : (date,item+)-->
    <tr class="pl-ead-change">
      <th scope="row" class="pl-ead-date pl-tbl-th">
        <xsl:apply-templates select="date"/>
      </th>
      <td class="pl-ead-item">
        <xsl:choose>
          <xsl:when test="count(item) = 1 ">  <!--s'il n'y a qu'un seul item, ce  n'est pas la peine de faire une liste-->
            <xsl:apply-templates select="item"/>
          </xsl:when>
          <xsl:otherwise> <!--on présente les item sous forme de liste lorsqu'il y en a plusieurs-->
            <ul>
              <xsl:apply-templates select="item"/>
            </ul>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
  </xsl:template>

  <!--change/item : bloc : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date|num|origination|repository|,unittitle|unittitle|ref|extref|linkgrp|bibref|title|archref|address|chronlist|list|note|table|blockquote)*-->
  <xsl:template match="revisiondesc/change[count(item) > 1]/item">
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>

  <xsl:template match="revisiondesc/change[count(item) = 1]/item">
    <xsl:apply-templates />
  </xsl:template>


  <!-- ***************************************************************************** -->
                                      <!-- FRONTMATTER -->
  <!-- ***************************************************************************** -->

  <xsl:template match="frontmatter"> <!-- Conteneur : (titlepage?,div*)-->
    <xsl:call-template name="display-as-block"/>
  </xsl:template>

  <xsl:template match="frontmatter/titlepage"> <!-- Conteneur :(address|chronlist|list|note|table|blockquote|p|author|date|edition|num|publisher|bibseries|sponsor|titleproper|subtitle)+ -->
    <!--parent : frontmatter uniquement-->
    <div class="pl-ead-titlepage">
      <xsl:apply-templates mode="titlepage-block"/>
    </div>
  </xsl:template>

  <xsl:template match="div"> <!--section : (head?,(address|chronlist|list|note|table|blockquote|p)*, div*)-->
    <xsl:call-template name="display-as-section"/>
  </xsl:template>

  <xsl:template match="frontmatter/titlepage/*" mode="titlepage-block">
    <!--tous les sous-élément du titlepage sont dans un bloc (div) pour être placés comme on veut sur la page de titre-->
    <div class="pl-pgd-titlepage-block">
      <xsl:apply-templates select="."/> <!--affichage par défaut de l'élément sauf si surchargé dans ce contexte-->
    </div>
  </xsl:template>

  <!-- ***************************************************************************** -->
            <!-- STRUCTURALS ARCHIVAL ELEMENTS ARCHDESC, C  AND SCOPECONTENT -->
  <!-- ***************************************************************************** -->
  <!--unité d'affichage => archdesc, c, c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, c11, c12  squelette de la structure ead   -->

  <xsl:template match="archdesc | c | c01 | c02 | c03 | c04 | c05 | c06 | c07 | c08 | c09 | c10 | c11 | c12">
    <!-- archdesc : Conteneur : (runner*,did,(accessrestrict|accruals|acqinfo|altformavail|appraisal|arrangement|bibliography|bioghist|controlaccess|custodhist|descgrp|fileplan|index|odd|originalsloc|otherfindaid|phystech|prefercite|processinfo|relatedmaterial|scopecontent|separatedmaterial|userestrict|dsc|dao|daogrp|note)*) -->
    <!--c{n} : Conteneur : (head?,did,(accessrestrict|accruals|acqinfo|altformavail|appraisal|arrangement|bibliography|bioghist|controlaccess|custodhist|descgrp|fileplan|index|odd|originalsloc|otherfindaid|phystech|prefercite|processinfo|relatedmaterial|scopecontent|separatedmaterial|userestrict|dsc|dao|daogrp|note)*,(thead?,c{n+1}+)*)
    tpattern non supporté. Définit une unité d'affichage.-->
    <xsl:choose>
      <xsl:when test=" $mode='static' and $full = 'false' and @pleade:break = 'true' ">
        <xsl:result-document href="fragments/{@pleade:id}.html" format="fragment">
          <div id="{@pleade:id}">
            <!--bloc de liens vers les ancêtres-->
            <xsl:call-template name="make-ancestors-block-static"/>
            <!--affichage du composant-->
            <xsl:call-template name="make-component-block"/>
            <!--bloc de liens vers les enfants-->
            <xsl:call-template name="make-children-block-static"/>
          </div>
        </xsl:result-document>
      </xsl:when>
      <!-- en mode dynamique on travail forcément sur un fragment, on affiche les liens enfants/ancêtres avec un autre traitement -->
      <xsl:when test="$mode = 'dyn' ">
        <xsl:variable name="id">
          <xsl:choose>
            <xsl:when test="@pleade:id"><xsl:value-of select="@pleade:id" /></xsl:when>
            <xsl:when test="did/@pleade:id"><xsl:value-of select="did/@pleade:id" /></xsl:when>
          </xsl:choose>
        </xsl:variable>
        <xsl:if test="$isDebug"><xsl:message>Traite <xsl:value-of select="name()"/> (id=<xsl:value-of select="$id"/>) mode dynamique</xsl:message></xsl:if>
        <div id="{$id}" class="pl-pgd-component">
          <xsl:if test="@pleade:break='true'">
            <xsl:call-template name="tool-box-builder" />
          </xsl:if>
          <div class="pl-pgd-cnt" id="pl-pgd-cnt">
            <xsl:if test="$isDebug"><xsl:message>  Requesting make-ancestors-block-dyn</xsl:message></xsl:if>
            <xsl:call-template name="make-ancestors-block-dyn"/>
            <xsl:if test="$isDebug"><xsl:message>  Ending make-ancestors-block-dyn</xsl:message></xsl:if>
            <xsl:if test="$isDebug"><xsl:message>  Requesting make-component-block</xsl:message></xsl:if>
            <xsl:call-template name="make-component-block"/>
            <xsl:if test="$isDebug"><xsl:message>  Ending make-component-block</xsl:message></xsl:if>
            <xsl:if test="$isDebug"><xsl:message>  Requesting make-children-block-dyn</xsl:message></xsl:if>
            <xsl:call-template name="make-children-block-dyn"/>
            <xsl:if test="$isDebug"><xsl:message>  Ending make-children-block-dyn</xsl:message></xsl:if>
          </div>
        </div>
      </xsl:when>
      <xsl:otherwise> <!-- en mode = "static" avec  full = "true" (ou dans le cas de @pleade:break = "false") : on continue à générer le document (sans liens enfants/ancêtres) dans la sortie -->
        <xsl:call-template name="make-component-block"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template match="c/thead | c01/thead | c02/thead | c03/thead | c04/thead | c05/thead | c06/thead | c07/thead | c08/thead | c09/thead | c10/thead | c11/thead | c12/thead">
    <!--(row+)-->
    <!--FIXME : non supporté pour l'instant--></xsl:template>

  <xsl:template match="ead/archdesc/runner"> <!--Texte : (#PCDATA|ptr|extptr|emph|lb)*-->
    <!--TODO : uniquement dans les pdf (mais vérifier avant qu'en CSS @media print ça ne peut pas être utile...) --></xsl:template>

  <!--traitement des enfants de type section des c...-->
  <xsl:template match="accessrestrict/accessrestrict | odd/odd">
    <xsl:call-template name="display-as-block-with-label">
      <xsl:with-param name="el" select="."/>
      <xsl:with-param name="out-el-name" select="'div'"/>
      <xsl:with-param name="block-out-el-class" select="concat( 'pl-pgd-subsect pl-ead-', local-name(.) )"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="accessrestrict | accruals | acqinfo | altformavail | appraisal | arrangement | bibliography | bioghist | custodhist | descgrp | fileplan | index | odd | originalsloc | otherfindaid | phystech | prefercite | relatedmaterial | scopecontent | separatedmaterial | userestrict | dsc[*[not(self::pleade:subdoc-link)]]">
    <xsl:call-template name="display-as-section"/>
  </xsl:template>

  <!-- Le controlaccess est un peu particulier, mais ça débute comme une section -->
  <xsl:template match="controlaccess"> <!--section : (head?,(address|chronlist|list|note|table|blockquote|p|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|title|controlaccess)+)-->
    <!--Ses sous-éléments relatifs à l'indexation (corpname,famname,geogname,name,occupation,persname,subject,genreform,function,title) sont traités de manière particulière.-->
    <xsl:param name="el" select="."/>
    <xsl:if test="*[. != '' or @normal != '']">
      <div class="pl-pgd-sect pl-ead-{local-name($el)}">
        <xsl:if test="not(preceding-sibling::controlaccess)"><xsl:call-template name="display-label"/></xsl:if>
        <xsl:for-each-group select="*[@pleade:index-id]" group-by="@pleade:index-id">
          <p class="pl-ead-p pl-ead-controlaccess-group">
            <span class="pl-ead-controlaccess-group-header">
              <i18n:text key="ead.nav.index.{@pleade:index-id}"/>
              <i18n:text key="ead.display.index.label-separator"/>
            </span>
            <span class="pl-ead-controlaccess-group-content">
              <xsl:for-each select="current-group()">
                <xsl:variable name="value" select="@pleade:value"/>
                <xsl:variable name="encoded-value" select="urle:encode(string($value), 'UTF-8')" xmlns:urle="java:java.net.URLEncoder"/>
                <xsl:if test="position() > 1">&#xA0;&#x2022; </xsl:if>
                <xsl:choose>
                  <xsl:when test="@pleade:index-local-name">
                    <a href="docsearch-term.xsp?r={$eadid}&amp;f={@pleade:index-local-name}&amp;v={$encoded-value}" onclick="windowManager.updateDocumentResults('pl-pg-body-main-mcol', this.href); return false;"><xsl:apply-templates/></a>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:apply-templates/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </span>
          </p>
        </xsl:for-each-group>
        <!-- On traite maintenant les éléments qui n'ont pas été pris en compte dans des index -->
        <!-- FIXME : il faudrait prévoir un traitement spécifique pour les sous-éléments qui ne sont des termes d'indexation
          => ajout du test pour savoir si l'élément est un pleade-index
        -->
        <xsl:for-each-group select="*[not(@pleade:index-id) and not(self::head)]" group-by="name()">
          <p class="pl-ead-p pl-ead-controlaccess-group">
            <span class="pl-ead-controlaccess-group-header">
              <i18n:text key="ead.nav.index.{name()}"/>
              <i18n:text key="ead.display.index.label-separator"/>
            </span>
            <span class="pl-ead-controlaccess-group-content">
              <xsl:for-each select="current-group()">
                <xsl:if test="position() > 1">&#xA0;&#x2022; </xsl:if>
                <xsl:apply-templates/>
              </xsl:for-each>
            </span>
          </p>
        </xsl:for-each-group>
        <!--<xsl:apply-templates select="controlaccess"/>-->
      </div>
    </xsl:if>
  </xsl:template>


  <!--autres enfants de c... à traitement non générique-->
  <xsl:template match="processinfo"> <!--section : (head?,(address|chronlist|list|note|table|blockquote|p|processinfo)+)-->
    <!--La plupart du temps, notamment pour les documents issus de Arkhéïa, on ne sort pas cet élément.-->
    <!--FIXME -->
    </xsl:template>

  <!-- dsc : section : ((head?,(address|chronlist|list|note|table|blockquote|p)*),(((thead?,((c,thead?)+|(c01,thead?)+))|dsc*)))
  C'est une section un peu particulière, car son rôle est d'abord de regrouper des sous-éléments c.
  Il est « obligatoire » dans archdesc si on veut des sous-niveaux de description.-->

  <xsl:template match="dsc/thead"> <!--(row+)-->
   <!--FIXME : non supporté pour l'instant-->
   </xsl:template>

  <!-- accessrestrict : section : (head?, (address|chronlist|list|note|table|blockquote|p|legalstatus|accessrestrict)+)-->

  <xsl:template match="accessrestrict/legalstatus"> <!--bloc : (#PCDATA|ptr|extptr|emph|lb|date)*-->
    <!--fixme : doit-on l'étiquetter ? A voir. Au moins le classer.-->
    <!--<div class="pl-ead-legalstatus">
      <xsl:apply-templates />
    </div>-->
    <!-- <xsl:apply-templates select="." mode="block"/> -->
    <p class="pl-ead-p pl-ead-{local-name(.)}">
      <xsl:call-template name="display-as-text-with-label">
        <xsl:with-param name="el" select="."/>
      </xsl:call-template>
    </p>
  </xsl:template>



  <!-- descgrp : section : (head?,(address|chronlist|list|note|table|blockquote|p|accessrestrict|accruals|acqinfo|altformavail|appraisal|arrangement|bibliography| bioghist|controlaccess|custodhist|descgrp|fileplan|index|odd|originalsloc|otherfindaid|phystech|prefercite|processinfo|relatedmaterial| scopecontent|separatedmaterial|userestrict)+)-->
  <!--Il s'agit en fait d'une « super » section par rapport à nos sections habituelles (scopecontent, ...).
  Il ne s'agit pas d'un élément à sémantique archivistique, mais plutôt un élément qui permet de faire des regroupements d'éléments à sémantique archivistique.
  En terme d'affichage, on le considère comme une section, et on l'utilise pour « calculer » le niveau hiérarchique des sections. Par exemple, si on a c/scopecontent,
  notre scopecontent est une section de niveau 1, mais si on a c/descgrp/scopecontent, notre scopecontent est une section de niveau 2.-->

  <!-- scopecontent : section : (head?,(address|chronlist|list|note|table|blockquote|p|arrangement|scopecontent|dao|daogrp)+)
    Peut contenir arrangement, mais cela ne devrait pas influencer l'affichage. Peut également contenir des images.
    L'idée générale des sections est : <div class="section scopecontent"> ... </div> -->

  <!-- accruals : section : (head?, (address|chronlist|list|note|table|blockquote|p|accruals)+) -->
  <!-- altformavail : section : (head?, (address|chronlist|list|note|table|blockquote|p|altformavail)+)-->
  <!-- appraisal : section : (head?, (address|chronlist|list|note|table|blockquote|p|appraisal)+)-->
  <!-- bibliography :section : (head?,(address|chronlist|list|note|table|blockquote|p|ref|extref|linkgrp|bibref|title|archref|bibliography)+)-->
  <!-- fileplan : section : (head?,(address|chronlist|list|note|table|blockquote|p|fileplan)+)-->
  <!-- originalsloc : section : (head?, (address|chronlist|list|note|table|blockquote|p|originalsloc)+)-->
  <!-- phystech : section : (head?,(address|chronlist|list|note|table|blockquote|p|phystech)+)-->
  <!-- prefercite : section : (head?,(address|chronlist|list|note|table|blockquote|p|prefercite)+)-->
  <!-- userestrict : section : (head?,(address|chronlist|list|note|table|blockquote|p|userestrict)+)-->

  <!-- acqinfo : section : (head?, (address|chronlist|list|note|table|blockquote|p|acqinfo)+)
  Attention au fait qu'il peut se retrouver à l'intérieur d'un custodhist. Mais cela ne devrait pas influencer l'affichage.-->

  <!-- arrangement : section : (head?, (address|chronlist|list|note|table|blockquote|p|arrangement)+)
  Peut aussi être dans un scopecontent, mais cela ne devrait pas influencer l'affichage.-->

  <!-- custodhist : section : (head?, (address|chronlist|list|note|table|blockquote|p|custodhist|acqinfo)+)
  Peut contenir acqinfo, mais cela ne devrait pas influencer l'affichage.-->

  <!-- otherfindaid : section : (head?, (address|chronlist|list|note|table|blockquote|p|ref|extref|linkgrp|bibref|title|archref|otherfindaid)+)
  Quelques contenus particuliers (liens hypertexte, références bibliographiques).-->

  <!-- relatedmaterial : section : (head?,(address|chronlist|list|note|table|blockquote|p|ref|extref|linkgrp|bibref|title|archref|relatedmaterial)+)
  Quelques contenus particuliers (liens hypertexte, références bibliographiques).-->

  <!-- separatedmaterial : section : (head?,(address|chronlist|list|note|table|blockquote|p|ref|extref|linkgrp|bibref|title|archref|separatedmaterial)+)
  Quelques contenus particuliers (liens hypertexte, références bibliographiques).-->

  <!-- bioghist : section : (head?,(address|chronlist|list|note|table|blockquote|p|bioghist|dao| daogrp)+)
  Elle peut contenir des dao.-->

  <!-- odd : section : (head?,(address|chronlist|list|note|table|blockquote|p|dao|daogrp|odd)+)
  Elle peut contenir des dao.-->

  <!-- index : section : (head?, (address|chronlist|list|note|table|blockquote|p)*, ((listhead?,indexentry+)| index+))
  Contient en plus une liste d'entrées d'index => cf mode="index"--><!-- FIXME : index mode="index" ? -->


  <!-- ***************************************************************************** -->
                                      <!-- DID AND CHILDS-->
  <!-- ***************************************************************************** -->
  <xsl:template match="did"> <!--Conteneur : (head?,(abstract|container|dao|daogrp|langmaterial|materialspec|note|origination|physdesc|physloc|repository|unitdate|unitid|unittitle)+)-->
    <xsl:if test="$active-synch-toc='true' and $display-properties!='' and $display-properties/property[@name = 'display-navigation'] = 'true'">
      <xsl:apply-templates select="." mode="synchronise-tool" />
    </xsl:if>

    <!--Les enfants seront de type champ – valeur. Il peut avoir un titre (head)-->
    <table class="pl-pgd-fv pl-ead-did">
      <xsl:call-template name="i18n-attribute">
        <xsl:with-param name="attribute-name" select="'summary'"/>
        <xsl:with-param name="key" select="'ead-did-table-@summary'"/>
      </xsl:call-template>
      <xsl:call-template name="display-label">
        <xsl:with-param name="out-el-name" select="'caption'"/>
      </xsl:call-template>
      <xsl:apply-templates />
    </table>
    <xsl:if test="parent::c[@pleade:original-element='archdesc'] and $ead-archdesc-toc='true'">
      <!--on ne traite que les élélement de type "section" dans le mode "archdesc-toc"-->
      <xsl:if test="../accessrestrict or ../accruals or ../acqinfo or ../altformavail or ../appraisal or ../arrangement or ../bibliography or ../bioghist
              or ../custodhist or ../descgrp or ../fileplan  or ../index or ../odd or ../originalsloc or ../otherfindaid or ../phystech
              or ../prefercite or ../relatedmaterial or ../scopecontent or ../separatedmaterial or ../userestrict ">
        <div class="pl-ead-archdesc-toc">
          <p class="pl-ead-archdesc-toc-header"><i18n:text key="ead.archdesc.toc.title">sommaire&#xA0;:</i18n:text></p>
          <ul class="pl-ead-archdesc-toc">
          <xsl:apply-templates select="  ../accessrestrict | ../accruals | ../acqinfo | ../altformavail | ../appraisal | ../arrangement | ../bibliography | ../bioghist
                  | ../custodhist | ../descgrp | ../fileplan  | ../index | ../odd | ../originalsloc | ../otherfindaid | ../phystech
                  | ../prefercite | ../relatedmaterial | ../scopecontent | ../separatedmaterial | ../userestrict " mode="archdesc-toc"/>
          </ul>
        </div>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <!--+
      | Affichage du bouton de synchronisation du sommaire
      | Ne s'affiche que pour les composants qui sont dans la tdm
      +-->
  <xsl:template match="did" mode="synchronise-tool" priority="2">
    <xsl:variable name="toc-url" select="concat('cocoon://functions/ead/get-toc/',$eadid,'.xml')"/>
    <xsl:variable name="toc-doc" select="
      if (doc-available($toc-url)) then document($toc-url)/ead
      else ''
    "/>
    <xsl:variable name="id" select="parent::*/@pleade:id" />

    <!-- On affiche le bouton seulement si composant dans la TOC -->
    <xsl:if test="$toc-doc//c[@pleade:id = $id]">
      <xsl:variable name="ancs">
        <xsl:for-each select="parent::*/preceding-sibling::pleade:ancestors/pleade:ancestor[@pleade:toc='true']">
          <xsl:sort select="@pleade:pos" />
          <xsl:value-of select="@pleade:id" />
          <xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <xsl:variable name="pos" select="parent::*/@pleade:pos" />
      <xsl:variable name="onclick">eadWindow.synchronizeToc('<xsl:value-of select="$id" />','<xsl:value-of select="$pos" />','<xsl:value-of select="$ancs" />');</xsl:variable>
  
      <div class="pl-bttn-sync">
        <button onclick="{$onclick}"><i18n:text key="ead.synchronize">synchroniser le sommaire</i18n:text></button>
      </div>
    </xsl:if>

  </xsl:template>

  <xsl:template match="  accessrestrict|accruals|acqinfo|altformavail|appraisal|arrangement|
                        bibliography|bioghist|custodhist|descgrp|fileplan|
                        index|odd|originalsloc|otherfindaid|phystech|prefercite|
                        relatedmaterial|scopecontent|separatedmaterial|userestrict"   mode="archdesc-toc">
      <li>
        <a href="#{@pleade:id}">
          <xsl:call-template name="display-label">
            <xsl:with-param name="out-el-name" select="'span'"/>
            <xsl:with-param name="out-el-class" select="''"/>
          </xsl:call-template>
        </a>
      </li>
  </xsl:template>

  <!-- Traitement d'une date dans un intitulé -->
  <xsl:template match="unittitle/unitdate|*[not(self::did)]/unitdate">
    <span class="pl-ead-unitdate"><xsl:apply-templates/></span>
  </xsl:template>

  <xsl:template match="did/abstract | did/container | did/dao | did/daogrp | did/langmaterial | did/materialspec | did/note | did/origination | did/physdesc | did/physloc | did/repository | did/unitdate | did/unitid | did/unittitle">
    <tr class="pl-tbl-lgn-{local-name()}">
      <th scope="row" class="pl-tbl-th">
        <span class="pl-tbl-th-sp">
          <xsl:call-template name="get-label"/>
        </span>
      </th>
      <td class="pl-tbl-{local-name()}">
        <xsl:choose>
          <xsl:when test="self::dao">
            <xsl:call-template name="process-dao"/>
          </xsl:when>
          <xsl:when test="self::daoloc"><xsl:call-template name="process-daoloc"/></xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates />
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="did/repository/address"><!-- texte avec étiquette : (addressline+) -->
    <div class="pl-ead-address">
      <xsl:call-template name="display-as-text-with-label">
        <xsl:with-param name="text-ns">
          <xsl:apply-templates select="addressline" mode="inline"/>
        </xsl:with-param>
      </xsl:call-template>
    </div>
  </xsl:template>


  <!-- ***************************************************************************** -->
                            <!-- GENERICS ELEMENTS HTML LIKE -->
  <!-- ***************************************************************************** -->

  <xsl:template match="lb"> <!--line-break : EMPTY, pas d'attributs-->
    <br/>
  </xsl:template>

  <xsl:template match="list"> <!--Conteneur : (head?,(item+|(listhead?,defitem+)))-->
    <div class="pl-ead-list">
      <xsl:if test="head">
        <xsl:apply-templates select="head"/>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="item"> <!-- cas : item+ => <ul> -->
          <!--attributs spécifique de liste sur l'élément item :
                item@numeration = arabic, upperalpha, loweralpha, upperroman, lowerroman
                item@type = simple, deflist, marked, ordered
                item@continuation = continues, starts  => FIXME : qu'est ce que ça veut dire exactement ?
                -->
          <xsl:choose>
            <xsl:when test="(@numeration or @type = 'ordered') and @type != 'marked' ">
              <ol>
                <xsl:attribute name="style">
                  <xsl:text>list-style-type:</xsl:text>
                  <xsl:choose> <!--FIXME : vérifier que "list-style-type" est bien supporté par les navigateurs avec toutes ces valeurs-->
                    <xsl:when test="@type = 'simple'">none;</xsl:when>
                    <xsl:when test="@numeration = 'arabic'">
                      <xsl:text>decimal;</xsl:text>
                    </xsl:when>
                    <xsl:when test="@numeration = 'upperalpha'">
                      <xsl:text>upper-alpha;</xsl:text>
                    </xsl:when>
                    <xsl:when test="@numeration = 'loweralpha'">
                      <xsl:text>lower-alpha;</xsl:text>
                    </xsl:when>
                    <xsl:when test="@numeration = 'upperroman'">
                      <xsl:text>upper-roman;</xsl:text>
                    </xsl:when>
                    <xsl:when test="@numeration = 'lowerroman'">
                      <xsl:text>lower-roman;</xsl:text>
                    </xsl:when>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:apply-templates select="item"/>
              </ol>
            </xsl:when>
            <xsl:when test="@type = 'deflist'">
              <xsl:call-template name="error">
                <xsl:with-param name="key" select="'ead-error-list-@type-deflist-unsupported'" />
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise><!-- cas où @type = "marked" (peu importe la valeur de @numeration) -->
              <ul>
                <xsl:apply-templates select="item"/>
              </ul>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise> <!-- cas : (listhead?,defitem+) -->
          <xsl:apply-templates select="listhead"/>
          <dl>
            <xsl:apply-templates select="defitem"/>
          </dl>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>

  <xsl:template match="list/head">
    <p class="pl-pgd-list-head pl-ead-head">
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <xsl:template match="list/defitem"> <!--Conteneur pour liste de définitions : (label,item)-->
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="list/defitem/label"> <!-- Bloc (dt): (#PCDATA|ptr|extptr|emph|lb|abbr|expan|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date|num|origination|repository|unitdate|unittitle|ref|extref|linkgrp|bibref|title|archref)*-->
    <dt>
      <xsl:apply-templates />
    </dt>
  </xsl:template>

  <!--item : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date|num|origination|repository|unitdate|unittitle|ref|extref|linkgrp|bibref|title|archref|address|chronlist|list|note|table|blockquote)*-->

  <xsl:template match="defitem/item">  <!--Bloc ( html:dd )-->
    <dd>
      <xsl:apply-templates />
    </dd>
  </xsl:template>

  <xsl:template match="list/item">
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>

  <xsl:template match="list/listhead"> <!--Conteneur : (head01?,head02?)-->
    <!--Difficile à bien mettre en forme-->
    <!--<div class="pl-ead-listhead">
      <xsl:apply-templates/>
    </div>-->
    <!--<xsl:apply-templates select="." mode="block"/>-->
    <xsl:call-template name="display-as-block"/>
  </xsl:template>

  <xsl:template match="list/listhead/head01">
    <p>
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <xsl:template match="list/listhead/head02">
    <p>
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <xsl:template match="chronlist"> <!--Tableau : (head?,(listhead)?,(chronitem)+)-->
    <table class="pl-pgd-table pl-ead-chronlist">
      <xsl:call-template name="i18n-attribute">
        <xsl:with-param name="attribute-name" select="'summary'"/>
        <xsl:with-param name="key" select="'ead-chronlist-table-@summary'"/>
      </xsl:call-template>
      <xsl:variable name="caption">
        <xsl:call-template name="get-label"/>
      </xsl:variable>
      <xsl:if test="$caption != '' or listhead ">
        <caption>
          <xsl:copy-of select="$caption"/>
        </caption>
      </xsl:if>
      <xsl:apply-templates select="*"/>
    </table>
  </xsl:template>

  <!--FIXME : sous chronlist on suppose que listhead permet de faire une entête pour les date (head01) et une autre pour les event (head02)-->
  <xsl:template match="chronlist/listhead">
    <thead class="pl-ead-listhead">
      <tr>
        <xsl:apply-templates />
      </tr>
    </thead>
  </xsl:template>

  <xsl:template match="chronlist/listhead/head01">
    <th scope="col" class="pl-tbl-th">
      <span class="pl-tbl-th-sp">
        <xsl:apply-templates />
      </span>
    </th>
  </xsl:template>

  <xsl:template match="chronlist/listhead/head02">
    <th scope="col" class="pl-tbl-th">
      <span class="pl-tbl-th-sp">
        <xsl:apply-templates />
      </span>
    </th>
  </xsl:template>

  <xsl:template match="chronlist/chronitem"> <!--Rangée de tableau : (date,(event|eventgrp))-->
    <tr>
      <xsl:apply-templates />
    </tr>
  </xsl:template>

  <xsl:template match="chronlist/chronitem/date"> <!--Texte : (#PCDATA|ptr|extptr|emph|lb)*-->
    <th class="pl-tbl-th">
      <span class="pl-tbl-th-sp">
        <xsl:apply-templates />
      </span>
    </th>
  </xsl:template>

  <xsl:template match="chronlist/chronitem/event"><!-- Bloc : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date|num|origination|repository|unitdate|unittitle|ref|extref|linkgrp|bibref|title|archref|address|chronlist|list|note|table|blockquote)*-->
    <td>
      <xsl:apply-templates />
    </td>
  </xsl:template>

  <xsl:template match="chronlist/chronitem/eventgrp"> <!--Conteneur : (event+)-->
    <td class="pl-ead-eventgrp">
      <ul>
        <xsl:apply-templates />
      </ul>
    </td>
  </xsl:template>

  <xsl:template match="eventgrp/event"><!-- bloc : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date|num|origination|repository|unitdate|unittitle|ref|extref|linkgrp|bibref|title|archref|address|chronlist|list|note|table|blockquote)*-->
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>


  <!--note : (address|chronlist|list|note|table|blockquote|p)+-->

  <!--contexte texte (dans des éléments avec #PCDATA) : Icône qui ouvre une popup-->
  <xsl:template match="archref/note | entry/note | event/note | extref/note | extrefloc/note | item/note | p/note | ref/note | refloc/note">
    <!--<div class="todo">
      <xsl:apply-templates />
    </div>-->
    <xsl:variable name="_id" select="generate-id()" />
    <xsl:variable name="nbnt" select="count(preceding::note) + 1" />
    <!-- Façon Tooltip YUI -->
    <span id="bttn_nt_{$_id}" class="pl-ead-nt"><sup>[<xsl:value-of select="if($nbnt) then $nbnt else 'note'" />]</sup></span>
    <xsl:if test="p[.!='']">
      <xsl:variable name="qot">"</xsl:variable>
      <script type="text/javascript">
      var tooltip_<xsl:value-of select="generate-id()"/> = new YAHOO.widget.Tooltip("myTooltip", { context:"bttn_nt_<xsl:value-of select="generate-id()"/>", text:"<xsl:for-each select="p[.!='']"> <xsl:if test="position()!=1"><xsl:text>\r\n</xsl:text></xsl:if> <xsl:value-of select="replace(normalize-space(.), $qot, concat('\\', $qot))" /></xsl:for-each>" , width:'500px' } );
    </script>
    </xsl:if>
    <!-- Façon Panel YUI -->
    <!--<xsl:if test="p!=''">
      <span id="nt_{$_id}" class="pl-ead-nt"><xsl:for-each select="p[.!='']"><xsl:if test="position()!=1"><xsl:text>&#10;</xsl:text></xsl:if><xsl:value-of select="normalize-space(.)" /></xsl:for-each></span>
    </xsl:if>-->
  </xsl:template>

  <xsl:template match="frontmatter/titlepage/titleproper/note"> <!--sans etiquette dans la page de titre-->
    <xsl:call-template name="display-as-text"/>
  </xsl:template>

  <xsl:template match="note"> <!-- contextes par défaut (non-PCDATA) : Bloc avec étiquette-->
    <xsl:call-template name="display-as-block-with-label"/>
  </xsl:template>

  <xsl:template match="p">
    <xsl:call-template name="display-as-block">
      <xsl:with-param name="out-el-name" select="'p'"/>
    </xsl:call-template>
    <!--<p class="pl-ead-p">
      <xsl:apply-templates />
    </p>-->
  </xsl:template>

  <!--un template général pour adresse, les autres contextes sont spécifiés par ailleurs -->
  <xsl:template match="address">
    <xsl:call-template name="display-as-block" />
    <!--<div class="pl-ead-address">
      <xsl:apply-templates />
    </div>-->
  </xsl:template>

  <xsl:template match="addressline"> <!--"mode" par défaut-->
    <xsl:call-template name="display-as-block">
      <xsl:with-param name="out-el-name" select="'p'"/>
    </xsl:call-template>
    <!--<p class="pl-ead-addressline">
      <xsl:apply-templates />
    </p>-->
  </xsl:template>

  <xsl:template match="addressline" mode="inline">
    <xsl:apply-templates />
    <xsl:if test=" not( position() = last() ) ">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="blockquote"> <!--Conteneur : (address|chronlist|list|note|table|p)+-->
    <xsl:call-template name="display-as-block">
      <xsl:with-param name="out-el-name" select="'blockquote'"/>
    </xsl:call-template>
    <!--<blockquote class="pl-ead-blockquote">
      <xsl:apply-templates />
    </blockquote>-->
  </xsl:template>

  <!--TABLEAUX-->

  <xsl:template match="table"><!--tableau : (head?,tgroup+)-->
    <!--On utilise le modèle de tableaux HTML. Le head devient le caption. On doit par contre faire plusieurs table s'il y a plusieurs tgroup.-->
    <!--modele html <!ELEMENT table (caption?, (col*|colgroup*), thead?, tfoot?, (tbody+|tr+))>-->
    <!--FIXME : la dtd n'est pas assez stricte : elle impose un attribut @cols=n à tgroup mais elle n'impose pas que le nombre d'entry = n (en tout cas pas > n !)
    j'ai préféré ici laissé les cellules se contruire, le navigateur saura ajouter des cellules vide si nécessaire et de plus cela reste valide xhtml-->
    <xsl:choose>
      <xsl:when test="count(tgroup) = 1">
        <table class="pl-ead-table">
          <!--<xsl:call-template name="display-label">
            <xsl:with-param name="out-el-name" select="'caption'"/>
          </xsl:call-template>-->
          <xsl:apply-templates />
        </table>
      </xsl:when>
      <xsl:when test="count(tgroup) > 1">
        <xsl:call-template name="display-label"/> <!--plusieurs tableaux, le titre est dans un paragraphe au début-->
        <xsl:apply-templates />
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="error">
          <xsl:with-param name="key" select="'ead-error-table-no-tgroup'" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="table[count(tgroup) = 1]/tgroup"> <!--tableau :(colspec*,thead?,tbody) @cols #REQUIRED-->
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="table[count(tgroup) > 1]/tgroup"><!--tableau :(colspec*,thead?,tbody) @cols #REQUIRED-->
    <table class="pl-ead-tgroup">
      <!--<xsl:call-template name="display-label">
        <xsl:with-param name="out-el-name" select="'caption'"/>
        <xsl:with-param name="el" select="../table"/>
      </xsl:call-template>-->
      <xsl:apply-templates />
    </table>
  </xsl:template>

  <xsl:template match="table/head">
    <xsl:if test=".!=''">
      <xsl:call-template name="display-label">
        <xsl:with-param name="out-el-name" select="'caption'"/>
        <xsl:with-param name="el" select="parent::table" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="table/tgroup/colspec"> <!--EMPTY, attributs : colnum, char, colsep, colwidth, charoff, colname, rowsep, align-->
    <!--todo-->
    <!--<colgroup align="left" char="" charoff="" class="" dir="ltr" span="1" style="" title="" valign="top" width="">
      <col align="left" char="" charoff="" class="" dir="ltr" span="1" style="" title="" valign="top" width=""/>
    </colgroup>--></xsl:template>

  <!--FIXME : dans tous les éléments de tableau qui suivent, est-il nécessaire de les classer? c
  c'est un peu lourd et les selecteurs css permettent de cibler chaque élément du contexte de tableau
  c'est une question générale qui concerne d'autre élément (ex revisiondesc => class="pl-ead-change" n'est pas "absoluement" nécessaire.
  Par contre TH n'est pas pris en compte dans IE. Il faut donc classer tous les TH
  -->
  <xsl:template match="table/tgroup/thead"><!--(row+)-->
    <thead>
      <xsl:apply-templates />
    </thead>
  </xsl:template>

  <xsl:template match="table/tgroup/tbody"><!--(row+)-->
    <tbody>
      <xsl:apply-templates />
    </tbody>
  </xsl:template>

  <xsl:template match="table/tgroup/thead/row | table/tgroup/tbody/row"><!--(entry+)-->
    <tr>
      <xsl:apply-templates />
    </tr>
  </xsl:template>

  <xsl:template match="table/tgroup/tbody/row/entry"> <!--(#PCDATA|ptr|extptr|emph|lb|abbr|expan|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date|num|origination|repository|unitdate|unittitle|ref|extref|linkgrp|bibref|title|archref|address|list|note)*-->
    <td>
      <xsl:apply-templates />
    </td>
  </xsl:template>

  <xsl:template match="table/tgroup/thead/row/entry">
    <th>
      <xsl:apply-templates />
    </th>
  </xsl:template>

  <!-- ***************************************************************************** -->
                            <!-- TEXTUAL ELEMENTS -->
  <!-- ***************************************************************************** -->

  <!--on définit les affichage par défaut en texte de éléments, à surcharger si besoin-->
  <xsl:template match="imprint | bibseries | date | edition | publisher | author | num | sponsor | titleproper | subtitle ">
    <xsl:call-template name="display-as-text"/>
  </xsl:template>

  <xsl:template match="emph"><!-- texte : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref|title|archref)*-->
    <span class="pl-ead-emph">
      <xsl:apply-templates />
    </span>
  </xsl:template>

  <xsl:template match="abbr"> <!--texte : (#PCDATA)-->
    <acronym id="abbr_{generate-id()}" title="{@expan}">
      <xsl:attribute name="class">
        <xsl:text>pl-ead-abbr</xsl:text>
        <xsl:if test="@expan"> pl-pgd-tooltip</xsl:if>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </acronym>
    <!-- [JC] La construciton de tous les tooltips en une seule fois semble bien plus rapide (cf module-eadeac.js) -->
    <!--<xsl:if test="@expan">
      <script type="text/javascript">
        var tooltip_<xsl:value-of select="generate-id()"/> = new YAHOO.widget.Tooltip("myTooltip", { context:"abbr_<xsl:value-of select="generate-id()"/>", width:250} );
      </script>
    </xsl:if>-->
  </xsl:template>

  <xsl:template match="expan"> <!--texte : (#PCDATA)-->
  <!--principe des info-bulles au survol des mots et un soulignement en pointillé de l'abréviation.-->
    <span id="expan_{generate-id()}">
      <xsl:attribute name="class">
        <xsl:text>pl-ead-expan</xsl:text>
        <xsl:if test="@abbr"> pl-pgd-tooltip</xsl:if>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </span>
    <xsl:if test="@abbr">
      <script type="text/javascript">
      var tooltip_<xsl:value-of select="generate-id()"/> = new YAHOO.widget.Tooltip("myTooltip", { context:"expan_<xsl:value-of select="generate-id()"/>", text:"<xsl:value-of select="@abbr"/>" } );
    </script>
    </xsl:if>
  </xsl:template>


  <!-- texte  (#PCDATA|ptr|extptr|emph|lb|date|num)*  -->
  <!--fixme : title est-il un élément d'indexation ? si oui, définir les différents contexte-->
  <xsl:template match="title">
    <span class="pl-ead-title">
      <xsl:apply-templates />
    </span>
  </xsl:template>


  <!-- texte avec étiquette ************************* -->

  <xsl:template match="physdesc/extent"> <!--Texte avec étiquette : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref| title|archref)*-->
    <xsl:call-template name="display-as-text-with-label">
      <xsl:with-param name="text-ns">
        <xsl:apply-templates />
        <xsl:if test="@unit">
          <span class="pl-ead-att-unit">
            <i18n:text key="units.separator.{@unit}" /><xsl:value-of select="@unit"/>
          </span>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="dimensions"> <!--Texte avec etiquette : (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref| title|archref|dimensions)*-->
    <!--FIXME : rôle de l'attribut @label par rapport à @type ?-->
    <xsl:call-template name="display-as-text-with-label">
      <xsl:with-param name="label-ns">
        <xsl:call-template name="get-label"/>
      </xsl:with-param>
      <xsl:with-param name="text-ns">
        <xsl:apply-templates />
        <xsl:if test="@unit">
          <span class="pl-ead-att-unit">
            <i18n:text key="units.separator.{@unit}" /><xsl:value-of select="@unit"/>
          </span>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="physfacet"> <!-- texte avec etiquette :   (#PCDATA|ptr|extptr|emph|lb|abbr|expan|ref|extref|linkgrp|bibref|title|archref|corpname|famname|geogname|name|occupation|persname|subject|genreform|function|date)*-->
    <!--Particularités : Voir dimensions.-->
    <xsl:call-template name="display-as-text-with-label">
      <xsl:with-param name="label-ns">
        <xsl:call-template name="get-label"/>
      </xsl:with-param>
      <xsl:with-param name="text-ns">
        <xsl:apply-templates />
        <xsl:if test="@unit">
          <span class="pl-ead-att-unit">
            <i18n:text key="units.separator.{@unit}" /><xsl:value-of select="@unit"/>
          </span>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="unittitle/imprint"> <!--(#PCDATA|ptr|extptr|emph|lb|publisher|geogname|date)*-->
    <!-- FIXME : dans ce contexte texte avec étiquette ou bloc ? -->
    <!--Faire attention aux documents issus de Arkhéïa qui utilisent cela pour ISBD.-->
    <xsl:call-template name="display-as-text-with-label"/>
  </xsl:template>

  <xsl:template match="language"><!-- (#PCDATA|ptr|extptr|emph|lb)*-->
    <!--FIXME : Texte avec etiquette ou texte seul ?-->
    <xsl:call-template name="display-as-text-with-label"/>
  </xsl:template>

  <!-- ***************************************************************************** -->
                            <!-- ELEMENTS D'INDEXATION -->
  <!-- ***************************************************************************** -->

  <!--corpname|famname|geogname|name|occupation|persname|subject|genreform|function|title ?-->
  <!--corpname : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb|subarea)*-->
  <xsl:template match="bibref/corpname | entry/corpname | event/corpname | extref/corpname | extrefloc/corpname | item/corpname | label/corpname | origination/corpname | p/corpname | physdesc/corpname | physfacet/corpname | ref/corpname | refloc/corpname | repository/corpname | unittitle/corpname">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-corpname">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/corpname, indexentry/corpname, namegrp/corpname  -->

  <!--famname : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb)* -->
  <xsl:template match="bibref/famname | entry/famname | event/famname | extref/famname | extrefloc/famname | item/famname | label/famname | origination/famname | p/famname | physdesc/famname | physfacet/famname | ref/famname | refloc/famname | unittitle/famname">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-famname">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/famname, indexentry/famname, namegrp/famname  -->

  <!--geogname : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb)* -->
  <xsl:template match="entry/geogname | event/geogname | extref/geogname | extrefloc/geogname | imprint/geogname | item/geogname | label/geogname | p/geogname | physdesc/geogname | physfacet/geogname | ref/geogname | refloc/geogname | unittitle/geogname">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-geogname">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/geogname, indexentry/geogname, namegrp/geogname  -->

  <!--name : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb)* -->
  <xsl:template match="bibref/name | entry/name | event/name | extref/name | extrefloc/name | item/name | label/name | origination/name | p/name | physdesc/name | physfacet/name | ref/name | refloc/name | repository/name | unittitle/name">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-name">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/name, indexentry/name, namegrp/name  -->

  <!--occupation : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb)* -->
  <xsl:template match="entry/occupation | event/occupation | extref/occupation | extrefloc/occupation | item/occupation | label/occupation | p/occupation | physdesc/occupation | physfacet/occupation | ref/occupation | refloc/occupation | unittitle/occupation">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-occupation">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/occupation, indexentry/occupation, namegrp/occupation  -->

  <!--persname : contexte texte : texte :  (#PCDATA|ptr|extptr|emph|lb)* -->
  <xsl:template match="bibref/persname | entry/persname | event/persname | extref/persname | extrefloc/persname | item/persname | label/persname | origination/persname | p/persname | physdesc/persname | physfacet/persname | ref/persname | refloc/persname | unittitle/persname">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-persname">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/persname, indexentry/persname, namegrp/persname  -->

  <!--subject : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb)*  -->
  <xsl:template match="entry/subject | event/subject | extref/subject | extrefloc/subject | item/subject | label/subject | p/subject | physdesc/subject | physfacet/subject | ref/subject | refloc/subject | unittitle/subject">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-subject">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/subject, indexentry/subject, namegrp/subject  -->

  <!--genreform : contexte "texte" : texte : (#PCDATA|ptr|extptr|emph|lb)*-->
  <xsl:template match="entry/genreform | event/genreform | extref/genreform | extrefloc/genreform | item/genreform | label/genreform | p/genreform | physdesc/genreform | physfacet/genreform | ref/genreform | refloc/genreform | unittitle/genreform">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-genreform {if (preceding-sibling::* or ../text()) then 'pl-pgd-inline-next' else ''}">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/genreform, indexentry/genreform  -->

  <!--function : contexte texte : texte : (#PCDATA|ptr|extptr|emph|lb)*  -->
  <xsl:template match="entry/function | event/function | extref/function | extrefloc/function | item/function | label/function | p/function | physdesc/function | physfacet/function | ref/function | refloc/function | unittitle/function">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-pgd-index pl-ead-function">
        <xsl:apply-templates select="." mode="index-link"/>
      </span>
    </xsl:if>
  </xsl:template>
  <!--todo autres contexte : controlaccess/function, indexentry/function, namegrp/function  -->

  <!-- + title élément d'indexation ?-->
  
  <!-- Template pour afficher un terme d'indexation et son lien Tarzan -->
  <xsl:template match="*" mode="index-link">
    <xsl:choose>
      <xsl:when test="@pleade:index-local-name">
        <xsl:variable name="value" select="@pleade:value"/>
        <xsl:variable name="encoded-value" select="urle:encode(string($value), 'UTF-8')" xmlns:urle="java:java.net.URLEncoder"/>
        <!-- Affichage en info-bulle de la forme normalisée -->
        <xsl:if test="@normal">
          <xsl:attribute name="title"><xsl:value-of select="@normal"/></xsl:attribute>
        </xsl:if>
        <a href="docsearch-term.xsp?r={$eadid}&amp;f={@pleade:index-local-name}&amp;v={$encoded-value}" onclick="windowManager.updateDocumentResults('pl-pg-body-main-mcol', this.href); return false;"><xsl:apply-templates/></a>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!-- ***************************************************************************** -->
                                <!-- LINKING ELEMENTS -->
  <!-- ***************************************************************************** -->
  <!-- Traitement des liaisons de type lien hypertexte. Un travail importat a été fait à
    la publication, pour faciliter le traitement ici.  -->
  <xsl:template match="ref | refloc | ptr | ptrloc | archref | extref | extrefloc | extptr | extptrloc">
    <xsl:choose>
      <xsl:when test="(self::ref or self::archref or self::extref or self::extrefloc) and not(@xpointer!='') and not(@href!='') and not(@target != '')">
        <!-- Pas un line hypertexte ; juste une référence -->
        <xsl:apply-templates select="." mode="reference" />
      </xsl:when>
      <xsl:otherwise>
        <!-- Le reste est traité comme un lien hypertexte -->
        <xsl:apply-templates select="." mode="link" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="ref | refloc | ptr | ptrloc | archref | extref | extrefloc | extptr | extptrloc" mode="link">
    <!-- Le type de lien (attached, relative, external, ...) -->
    <xsl:variable name="type">
      <!-- En général on le connaît déjà, sauf si c'est un lien vers un autre IR publié -->
      <xsl:choose>
        <xsl:when test="@pleade:link-type = 'relative'">
          <!-- On doit vérifier les propriétés d'affichage -->
          <xsl:variable name="ext" select="pleade:get-extension(@href)"/>
          <!-- Les extensions à considérer comme liens vers des IR
                Si on n'a pas de propriétés d'affichage (un bogue de l'application), on traite les liens avec extension 'xml'.-->
          <xsl:variable name="exts" select="if($display-properties and $display-properties/property[@name = 'ead-link-extensions']) then $display-properties/property[@name = 'ead-link-extensions'] else 'xml html'" />
          <xsl:variable name="ead-link-extensions" select="fn:tokenize($exts, '\s')"/>
          <xsl:choose>
            <xsl:when test="$ead-link-extensions[. = $ext]">
              <!-- C'est un lien vers un IR -->
              <xsl:value-of select="'ead'"/>
            </xsl:when>
            <xsl:otherwise>
              <!-- Sinon on conserve le type original (relative) -->
              <xsl:value-of select="@pleade:link-type"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <!-- Sinon on le connaît déjà -->
          <xsl:value-of select="@pleade:link-type"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- L'adresse visible du lien, on a une fonction spécifique pour cela -->
    <xsl:variable name="public-url" select="pleade:get-public-url(., $type, $eadid)"/>
    <!-- La classe -->
    <xsl:variable name="class">
      <!-- Tous les liens ont ces classes au moins -->
      <xsl:value-of select="concat('pl-pgd-link pl-pgd-link-', local-name(), ' pl-pgd-link-', $type, ' pl-pgd-link-', substring-after(@pleade:mime-type, '/'))"/>
    </xsl:variable>
    <!-- La cible, pour les liens qui ne sont pas des documents EAD -->
    <xsl:variable name="target">
      <xsl:if test="$type != 'internal' and $type != 'ead'">_blank</xsl:if>
    </xsl:variable>
    <!-- Le onclick, pour les liens internes ou les liens avec un altrender="window:_opener" ou les liens externes vers des documents target != '' -->
    <xsl:variable name="onclick">
      <xsl:variable name="q">'</xsl:variable>
      <xsl:choose>
        <xsl:when test="$type = 'internal'">
          <xsl:variable name="fragmentId" select="@pleade:target-fragment"/>
          <xsl:value-of select="concat('eadWindow.getContentManager().loadContent(', $q, $fragmentId, $q, ', false, ', $q, $qid, $q, '); return false;')"/>
          <!--<xsl:value-of select="concat('loadContent(', $q, $fragmentId, $q, '); return false;')"/>-->
        </xsl:when>
        <xsl:when test="@altrender = 'target:_opener'">
          <xsl:value-of select="concat('if (window.opener) { window.opener.location.href = ', $q, @href, $q, '; window.opener.focus(); return false; } else return true;' )"/>
        </xsl:when>
        <xsl:when test="$cdc and $type = 'ead'">
          <xsl:value-of select="concat('return windowManager.winFocus(this.href,',$q, 'docead', $q, ');')"/>
        </xsl:when>
        <xsl:when test="$target != '' and @pleade:mime-type != 'application/pdf'">
          <xsl:value-of select="concat('return windowManager.winFocus(this.href,' , $q, 'ext-ress',$q, ', ',$q,'null',$q, ', null, ', $q,'true', $q,');')"/>
        </xsl:when>
      </xsl:choose>
      <xsl:if test="$type = 'internal'">
      </xsl:if>
    </xsl:variable>
    <!-- Le titre (affiché en bulle d'aide) -->
    <xsl:variable name="title">
      <xsl:choose>
        <xsl:when test="@title and normalize-space(@title) != ''">
          <xsl:value-of select="normalize-space(@title)"/>
        </xsl:when>
        <xsl:when test="starts-with($public-url, 'mailto:')">
          <!-- On utilise i18n -->
          <xsl:text>ead.links.mailto.title</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <!-- On utilise i18n -->
          <xsl:text>ead.links.title...</xsl:text><xsl:value-of select="name()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- Un attribut pour indiquer de traduire le titre -->
    <xsl:variable name="i18n">
      <xsl:if test="not(@title)">title</xsl:if>
    </xsl:variable>
    <!-- Un contenu forgé, au cas où l'élément soit vide -->
    <xsl:variable name="content">
      <xsl:choose>
      <xsl:when test="normalize-space(.) = ''">
        <xsl:choose>
          <xsl:when test="normalize-space(@title) != ''"><xsl:text> </xsl:text><xsl:value-of select="@title"/></xsl:when>
          <xsl:when test="$type = 'internal'">
            <xsl:variable name="linkedUnitURL" select="concat('cocoon://functions/ead/get-fragment/',@pleade:target-fragment,'.xml')"/>
            <xsl:choose>
              <xsl:when test="doc-available($linkedUnitURL)">
                <xsl:value-of select="document($linkedUnitURL)/sdx:document/pleade:subdoc/@pleade:title"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:message>[ead2html.xsl] L'unité documentaire <xsl:value-of select="@pleade:target-fragment"/> n'existe pas !</xsl:message>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <i18n:text key="ead.links.{if(starts-with($public-url, 'mailto:')) then 'mailto.' else ''}content">cliquer sur ce lien pour consulter le document</i18n:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- Dans tous les cas on va sortir un lien HTML -->
    <!-- [CB] on n'affiche le lien que si l'unité documentaire cible existe
        FIXME : ne fonctionne que si l'unité documentaire est un fragment
    -->
    <xsl:if test="not($type = 'internal' and $content = '')">
      <a href="{$public-url}" class="{$class}">
        <!-- On sort les différents attributs en fonction des informations que l'on a -->
        <xsl:if test="$target != ''">
          <xsl:attribute name="target"><xsl:value-of select="$target"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="$title != ''">
          <xsl:attribute name="title"><xsl:value-of select="$title"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="$i18n != ''">
          <xsl:attribute name="i18n:attr"><xsl:value-of select="$i18n"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="$onclick != ''">
          <xsl:attribute name="onclick"><xsl:value-of select="$onclick"/></xsl:attribute>
        </xsl:if>
        <!-- On traite le contenu, sauf si on sait qu'il est déjà vide -->
        <xsl:choose>
          <xsl:when test="normalize-space($content) != ''">
            <xsl:copy-of select="$content"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates/>
          </xsl:otherwise>
        </xsl:choose>
      </a>
    </xsl:if>
  </xsl:template>
  <xsl:template match="*" mode="reference">
    <span class="{concat('pl-pgd-ref pl-pgd-ref-', local-name())}">
      <xsl:choose>
        <xsl:when test=".!=''"><xsl:apply-templates mode="ref-display"/></xsl:when>
        <xsl:otherwise><xsl:comment><xsl:value-of select="local-name()" /> vide!</xsl:comment></xsl:otherwise>
      </xsl:choose>
    </span>
  </xsl:template>
  <xsl:template match="*" mode="ref-display">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="archref/physdesc"><!--todo-->
    <span class="pl-ead-physdesc todo">
      <xsl:apply-templates />
    </span>
  </xsl:template>

  <xsl:template match="archref/physloc"><!--todo-->
    <span class="pl-ead-physloc todo">
      <xsl:apply-templates />
    </span>
  </xsl:template>

  <xsl:template match="bibref"> <!--todo-->
    <xsl:choose>
      <xsl:when test="parent::p">
        <xsl:apply-templates />
      </xsl:when>
      <xsl:otherwise>
        <p><xsl:apply-templates /></p>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="bibref/title">
    <xsl:if test="not(@pleade:hidden = 'true')">
      <span class="pl-ead-bibref-title">
        <xsl:apply-templates/>
      </span>
    </xsl:if>
  </xsl:template>

  <!-- ******************************************************************* -->
  <!-- Gestion des documents numériques (DAO, DAOLOC, DAOGRP, ...) -->

  <!-- Les différents parents du dao:
      - archdesc, c, c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, c11, c12:
        Le dao est un élément de type bloc, au même titre que les scopecontent
        et similaires. Il peut donc être répété, ils ne
      - bioghist, odd, scopecontent:
      - archref:
      - did:
    Il peut contenir un daodesc.
  -->

  <!-- Les différents parents du daoloc:
    - daogrp:

    Il peut contenir un daodesc.

    A noter que selon la Taglib EAD (http://www.loc.gov/ead/tglib/elements/daoloc.html), l'utilisation
    du daoloc concerne les liens étendus seulement. Mais nous l'utilisons ici pour des raisons historiques
    (Pleade < 3).
  -->

  <!-- Les différents parents du daogrp:
      - archdesc, c, c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, c11, c12:
        Le dao est un élément de type bloc, au même titre que les scopecontent
        et similaires. Il peut donc être répété, ils ne
      - bioghist, odd, scopecontent:
      - archref:
      - did:
    Il peut contenir:
      (daodesc?,(daoloc|ptrloc|extptrloc|refloc|extrefloc|arc|resource)+)
    Le daodesc sera considéré comme le head du bloc.

    On va s'intéresser à daoloc.
  -->
  <xsl:template match="archdesc/daogrp | c/daogrp | c01/daogrp | c02/daogrp | c03/daogrp | c04/daogrp | c05/daogrp | c06/daogrp | c07/daogrp | c08/daogrp | c09/daogrp | c10/daogrp | c11/daogrp | c12/daogrp">
    <xsl:call-template name="display-as-section"/>
  </xsl:template>
  <xsl:template match="bioghist/daogrp | odd/daogrp | scopecontent/daogrp">
    <xsl:call-template name="display-as-section"/>
  </xsl:template>
  <xsl:template match="archref/daogrp">
    <xsl:apply-templates mode="inline"/>
  </xsl:template>

  <!-- Les différents parents du daodesc:
    - daogrp (inline ou pas)
    - dao
    - daoloc
  -->
  <xsl:template match="daodesc" mode="inline">
    <!-- On ne fait que traiter le contenu -->
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="daodesc/head">
    <p class="pl-pgd-medias-head"><xsl:apply-templates/></p>
  </xsl:template>

  <!-- On intercepte les différents contextes pour les documents
      numériques et on crée l'enveloppe nécessaire -->
  <xsl:template match="dao" name="process-dao">
    <!-- On va distinguer en fonction du parent -->
    <xsl:choose>
      <xsl:when test="@role='last' or @role='image:last' or @role='navimages:image:last'"/><!-- On ne traite pas les aperçus ici -->
      <xsl:when test="@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'"/><!-- On ne traite pas les aperçus ici -->
      <!-- Dans un daogrp, le regroupement est déjà fait. -->
      <xsl:when test="parent::daogrp">
        <xsl:if test="not(preceding-sibling::*[1][self::daoloc])">
        <xsl:call-template name="output-daos">
          <xsl:with-param name="daos">
            <xsl:apply-templates select="." mode="daos"/>
          </xsl:with-param>
        </xsl:call-template>
        </xsl:if>
      </xsl:when>
      <xsl:when test="parent::*[@pleade:component = 'true'] or parent::did">
        <!-- Ici ce sont des sections, mais on va regrouper les multiples dao -->
        <xsl:if test="not(preceding-sibling::*[1][self::dao])">
          <!-- On ne traite que le premier d'une série -->
          <!-- On sort une "section" -->
          <div class="pl-pgd-sect pl-ead-dao">
            <!-- Titre normal -->
            <xsl:if test="not(parent::did)"> <!-- dans le cas du did l'étiquette est déjà mise -->
              <xsl:call-template name="display-label"/>
            </xsl:if>
            <!-- Pour le contenu, on traite chaque dao qui suit immédiatement -->
            <xsl:call-template name="output-daos">
              <xsl:with-param name="daos">
                <xsl:apply-templates select="." mode="daos"/>
              </xsl:with-param>
            </xsl:call-template>
          </div>
        </xsl:if>
      </xsl:when>
      <xsl:when test="parent::scopecontent or parent::bioghist or parent::odd">
        <!-- Ici on les considère comme des paragraphes. -->
        <!-- Pour le contenu, on traite chaque dao qui suit immédiatement -->
        <xsl:if test="not(preceding-sibling::*[1][self::dao])">
          <xsl:call-template name="output-daos">
            <xsl:with-param name="daos">
              <xsl:apply-templates select="." mode="daos"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
      </xsl:when>
      <xsl:when test="parent::archref">
        <!-- Ici ça devrait être du inline, ce qui est moins évident, mais c'est le problème du archref -->
        <xsl:apply-templates select="." mode="output"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Pour les daoloc, c'est toujours dans un daogrp -->
  <xsl:template match="daoloc" name="process-daoloc">
    <xsl:if test="not(preceding-sibling::*[1][self::daoloc])">
      <xsl:call-template name="output-daos">
        <xsl:with-param name="daos">
          <xsl:apply-templates select="." mode="daos"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Un petit template pour aider à fabriquer une chaîne de dao ou daoloc qui se suivent -->
  <xsl:template match="dao | daoloc" mode="daos">
    <xsl:copy-of select="."/>
    <xsl:apply-templates select="following-sibling::*[1][self::dao or self::daoloc]" mode="daos"/>
  </xsl:template>

  <!-- Sortie d'un tableau de dao -->
  <xsl:template name="output-daos">
    <xsl:param name="daos"/>

    <xsl:if test="$daos">

      <xsl:for-each select="$daos/*[self::dao or self::daoloc][not(@role='last') and not(@role='thumbnail') and not(@role='image:last') and not(@role='image:thumbnail') and not(@role='navimages:image:last') and not(@role='navimages:image:thumbnail')]">
        <xsl:call-template name="output-dao-td">
            <xsl:with-param name="dao">
              <xsl:choose>
                <xsl:when test="@role='first' or @role='image:first' or @role='navimages:image:first'">
                  <xsl:copy-of select="$daos" />
                </xsl:when>
                <xsl:when test="following-sibling::*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']">
                  <xsl:copy-of select=". | following-sibling::*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="." />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
      </xsl:for-each>

    </xsl:if>

  </xsl:template>

  <!-- Sortie d'une cellule de dao -->
  <xsl:template name="output-dao-td">
    <xsl:param name="dao"/>
    <xsl:if test="$dao">
      <xsl:apply-templates select="$dao" mode="output"/>
    </xsl:if>
  </xsl:template>

  <!-- Traitement réel des documents numériques -->
  <xsl:template match="daoloc | dao" mode="output">

    <xsl:if test="not(@role='last') and not(@role='thumbnail') and not(@role='image:last') and not(@role='image:thumbnail') and not(@role='navimages:image:last') and not(@role='navimages:image:thumbnail')">

    <!-- On va d'abord sortir un aperçu, selon le type d'objet, voire l'objet lui-même -->
    <div class="pl-pgd-medias-content">

      <!-- Sortir le traitement des objets multimédias de l'affichage lui-même -->
      <xsl:apply-templates select="." mode="select-display"/>

      <!-- Ensuite on sort un titre s'il y en a un -->
      <xsl:variable name="title">
        <xsl:call-template name="get-dao-title">
          <xsl:with-param name="dao" select="if(../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then ../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'][1] else self::*" />
        </xsl:call-template>
      </xsl:variable>

      <xsl:if test="normalize-space($title) != ''">
        <p class="pl-pgd-medias-title">
          <xsl:copy-of select="$title"/>
        </p>
      </xsl:if>

    </div>

    </xsl:if>

  </xsl:template>

  <!-- On traite les différents objets numériques que l'on peut avoir dans des dao|daoloc -->
  <xsl:template match="dao|daoloc" mode="select-display">
    <!-- L'adresse publique du document -->
    <xsl:variable name="public-url" select="pleade:get-public-url(., @pleade:link-type, $eadid)"/>

    <!--  Le HTML va dépendre du type d'objet -->
      <xsl:choose>

        <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
        <!-- Les images, en série ou non, on va afficher un aperçu et un lien pour voir en grand écran -->
        <xsl:when test="@pleade:link-type!='absolute' and (@pleade:link-type = 'series' or @pleade:mime-type = 'image/jpeg' or @pleade:mime-type = 'image/gif' or @pleade:mime-type = 'image/png')">
          <xsl:apply-templates select="." mode="pleade-image"/>
        </xsl:when>

        <!-- Un lien DAO qui n'a pas pu être résolu correctement lors de l'indexation -->
        <xsl:when test="@pleade:link-type = 'dao-link-error'">
          <xsl:apply-templates select="." mode="dao-link-error"/>
        </xsl:when>

        <!-- Pour les images absolues, on affiche la vignette avec un lien hypertexte pour ouvrir une popup. -->
        <xsl:when test="starts-with(@pleade:mime-type, 'image/')">
          <xsl:call-template name="medias-image-output">
            <!-- Passer l'image à ImageMagick pour construire la vignette.
            Seulement on peut avoir un problème de flux et l'image se construit
            mal. On préfère la contrainte HTML.
            <xsl:with-param name="thumbnail-url" select="concat($public-img-server-base, '_functions/image.', pleade:get-extension(@pleade:url), '?url=', $public-url, '&amp;w=', $active-img-thumbnail-size, '&amp;h=', $active-img-thumbnail-size)"/>-->
            <xsl:with-param name="thumbnail-url" select="@pleade:url"/>
            <xsl:with-param name="thumbnail-size" select="$active-img-thumbnail-size" />
            <xsl:with-param name="viewer-url" select="@pleade:url" />
          </xsl:call-template>
        </xsl:when>

        <!-- En premier lieu, les formats vidéos supportés -->
        <xsl:when test="tokenize($supported-video-formats, ',') = @pleade:mime-type">
          <xsl:if test="$isDebug"><xsl:message>We have a video format that is currently supported for the player (<xsl:value-of select="@pleade:mime-type"/>)</xsl:message></xsl:if>
          <xsl:call-template name="output-video">
            <xsl:with-param name="url" select="$public-url"/>
            <xsl:with-param name="title" select="@title"/>
            <xsl:with-param name="alt-content">
              <xsl:call-template name="output-media-infos">
                <xsl:with-param name="media" select="."/>
                <xsl:with-param name="url" select="$public-url"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>

        <!-- Ensuite, les formats audio supportés -->
        <xsl:when test="tokenize($supported-audio-formats, ',') = @pleade:mime-type">
          <xsl:if test="$isDebug"><xsl:message>We have an audio format that is currently supported for the player (<xsl:value-of select="@pleade:mime-type"/>)</xsl:message></xsl:if>
          <xsl:call-template name="output-audio">
            <xsl:with-param name="url" select="$public-url"/>
            <xsl:with-param name="title" select="@title"/>
            <xsl:with-param name="alt-content">
              <xsl:call-template name="output-media-infos">
                <xsl:with-param name="media" select="."/>
                <xsl:with-param name="url" select="$public-url"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>

        <!-- Enfin, tout le reste -->
        <!-- Video WMV -->
        <xsl:when test="@pleade:mime-type = 'video/x-ms-wmv'">
          <xsl:call-template name="output-windows-media-player">
            <xsl:with-param name="id" select="concat('pl-pgd-media-', @pleade:id)"/>
            <xsl:with-param name="w" select="'350'"/>
            <xsl:with-param name="h" select="'340'"/>
            <xsl:with-param name="url" select="$public-url"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Flash en général -->
        <xsl:when test="@pleade:mime-type = 'application/x-shockwave-flash'">
          <xsl:call-template name="output-flash-content">
            <xsl:with-param name="id" select="concat('pl-pgd-media-', generate-id(.))"/>
            <xsl:with-param name="w" select="'350'"/>
            <xsl:with-param name="h" select="'340'"/>
            <xsl:with-param name="movie" select="$public-url"/>
            <xsl:with-param name="alt-content">
              <xsl:call-template name="output-media-infos">
                <xsl:with-param name="media" select="."/>
                <xsl:with-param name="url" select="$public-url"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>

        <!-- Son WMA -->
        <xsl:when test="@pleade:mime-type='audio/x-ms-wma'">
          <xsl:call-template name="output-windows-media-player">
            <xsl:with-param name="id" select="concat('pl-pgd-media-', @pleade:id)"/>
            <xsl:with-param name="w" select="'350'"/>
            <xsl:with-param name="h" select="'65'"/>
            <xsl:with-param name="url" select="$public-url"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Son Wave -->
        <xsl:when test="@pleade:mime-type='audio/wav'">
          <xsl:call-template name="output-windows-media-player">
            <xsl:with-param name="id" select="concat('pl-pgd-media-', @pleade:id)"/>
            <xsl:with-param name="w" select="'350'"/>
            <xsl:with-param name="h" select="'65'"/>
            <xsl:with-param name="url" select="$public-url"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Vidéo AVI -->
        <xsl:when test="@pleade:mime-type = 'video/x-msvideo'">
          <xsl:call-template name="output-windows-media-player">
            <xsl:with-param name="id" select="concat('pl-pgd-media-', @pleade:id)"/>
            <xsl:with-param name="w" select="'350'"/>
            <xsl:with-param name="h" select="'340'"/>
            <xsl:with-param name="url" select="$public-url"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Vidéo MPEG -->
        <xsl:when test="@pleade:mime-type = 'video/mpeg'">
          <xsl:call-template name="output-windows-media-player">
            <xsl:with-param name="id" select="concat('pl-pgd-media-', @pleade:id)"/>
            <xsl:with-param name="w" select="'350'"/>
            <xsl:with-param name="h" select="'340'"/>
            <xsl:with-param name="url" select="$public-url"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Pour tous les autres documents, on affiche simplement un lien pour télécharger -->
        <xsl:otherwise>
          <xsl:variable name="target">
            <xsl:choose>
              <xsl:when test="@pleade:mime-type = 'application/pdf'">_blank</xsl:when>
              <xsl:otherwise>_self</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:call-template name="output-media-infos">
            <xsl:with-param name="media" select="."/>
            <xsl:with-param name="url" select="pleade:get-public-url(., @pleade:link-type, $eadid)"/>
            <xsl:with-param name="target" select="$target"/>
          </xsl:call-template>
        </xsl:otherwise>

      </xsl:choose>
  </xsl:template>

  <!-- Affichage des images prises en charge par la visionneuse ou le serveur d'images -->
  <xsl:template match="dao | daoloc" mode="pleade-image">
  
    <xsl:if test="$isDebug"><xsl:message>      Traite <xsl:value-of select="name()"/> mode pleade-image</xsl:message></xsl:if>
  
    <!-- L'adresse publique du document -->
    <xsl:variable name="public-url" select="pleade:get-public-url(., @pleade:link-type, $eadid)"/>

    <xsl:variable name="thumb" select="if(../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then ../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'][1] else self::*" />

    <xsl:variable name="eadimage-path" select="concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', @pleade:url, 'eadimage.xml?pleade-url=', @pleade:url, '&amp;pleade-img=', @pleade:img)"/>
    <xsl:if test="$isDebug"><xsl:message>        Loading: <xsl:value-of select="concat($eadimage-path,' (', current-time(), ')')"/></xsl:message></xsl:if>
    <xsl:variable name="eadimage" select="if($thumb/@pleade:link-type = 'series' and doc-available($eadimage-path)) then document($eadimage-path)/result/eadimage else ()"/>
    <xsl:if test="$isDebug"><xsl:message>        Loaded: <xsl:value-of select="concat($eadimage-path,' (', current-time(), ')')"/></xsl:message></xsl:if>

    <xsl:choose>
      <xsl:when test="@pleade:link-type!='attached' and @pleade:link-type!='relative' and not($eadimage/@find='true')">
        <xsl:if test="$isDebug"><xsl:message>        Image non attachee et non relative introuvable dans les racines</xsl:message></xsl:if>
        <xsl:apply-templates select="." mode="dao-link-error"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$isDebug"><xsl:message>          Requesting function pleade:thumbnailURL(<xsl:value-of select="concat($thumb,',',$eadimage,',',$public-url)"/>)</xsl:message></xsl:if>
        <!-- L'URL de la vignette est donnée par une fonction -->
        <xsl:variable name="thumbnail-url" select="pleade:thumbnailURL($thumb, $eadimage, $public-url)"/>
        <xsl:if test="$isDebug"><xsl:message>          Ending function pleade:thumbnailURL = <xsl:value-of select="$thumbnail-url"/></xsl:message></xsl:if>
        <!-- SI le rôle est 'first', on cherche la présence d'un 'last' pour le passer également -->
        <xsl:variable name="last" select="if(@role='first' or @role='image:first' or @role='navimages:image:first') then ../*[@role='last' or @role='image:last' or @role='navimages:image:last'][1] else ()"/>
        <!-- On va afficher un lien vers la visionneuse -->
        <xsl:variable name="viewer-url" select="pleade:linkToViewer(., $thumb, $eadimage, $last)"/>
        <xsl:if test="$isDebug"><xsl:message>          Ending function pleade:linkToViewer = <xsl:value-of select="$viewer-url"/></xsl:message></xsl:if>
        <xsl:if test="$isDebug"><xsl:message>          Requesting template medias-viewer-output</xsl:message></xsl:if>
        <xsl:call-template name="medias-viewer-output">
          <xsl:with-param name="thumbnail-url" select="$thumbnail-url" />
          <xsl:with-param name="thumbnail-size" select="$active-img-thumbnail-size" />
          <xsl:with-param name="viewer-url" select="$viewer-url" />
        </xsl:call-template>
        <xsl:if test="$isDebug"><xsl:message>        Ending template medias-viewer-output</xsl:message></xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:if test="$isDebug"><xsl:message>        Traite <xsl:value-of select="name()"/> mode pleade-image OK</xsl:message></xsl:if>

  </xsl:template>

  <!--
      | Fonctions pour l'affichage d'une vignette dans la fenêtre EAD
      |  Ces fonctions peuvent être réutilisées pour afficher sous une autre forme les images liées
      -->
  <!--
        Fonction qui retourne l'URL de la vignette
        Les paramètres sont :
          $thumb : dao ou daoloc qui sert pour la vignette
          $eadimage : les caractéristiques de l'image renvoyées par le serveur d'images
          $public-url = pleade:get-public-url(., @pleade:link-type, $eadid)
        Les variables suivantes doivent aussi être définies :
          $public-img-server-base : URL publique du serveur d'images
          $active-img-thumbnail-size : hauteur ou largeur de la vignette
      -->
  <xsl:function name="pleade:thumbnailURL" as="xs:string">
    <xsl:param name="thumb"/>
    <xsl:param name="eadimage"/>
    <xsl:param name="public-url"/>

    <xsl:choose>

      <xsl:when test="$thumb/@pleade:link-type = 'series'">
        <!-- On interroge le serveur d'images pour récupérer ce qui va bien -->
        <xsl:value-of select="concat($public-img-server-base, $eadimage/@src)"/>
      </xsl:when>

      <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
      <!-- Pour les URL absolues on passe par le serveur d'images -->
      <!--<xsl:when test="$thumb/@pleade:link-type = 'absolute'">
        <xsl:value-of select="concat($public-img-server-base, '_functions/image.', pleade:get-extension(@pleade:url), '?url=', $public-url, '&amp;w=', $active-img-thumbnail-size, '&amp;h=', $active-img-thumbnail-size)"/>
      </xsl:when>-->
      <!-- Pour les images relatives, on a un pipeline qui s'en occupe dans le module serveur d'images. -->
      <xsl:when test="$thumb/@pleade:link-type = 'relative'">
        <xsl:value-of select="concat($public-img-server-base,if($thumb[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then pleade:get-public-url($thumb, $thumb/@pleade:link-type, $eadid) else 'ead-docs-dir/',$thumb/@pleade:url, '?w=', $active-img-thumbnail-size, '&amp;h=', $active-img-thumbnail-size)"/>
      </xsl:when>
      <!-- Pour les autres (attached) on passe par un processus Cocoon -->
      <xsl:otherwise>
        <xsl:value-of select="concat('functions/images/resize/', if($thumb[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then pleade:get-public-url($thumb, $thumb/@pleade:link-type, $eadid) else $public-url, '?w=', $active-img-thumbnail-size)"/><!-- INFO (MP) : Ici pas de 'h' car le reader Cocoon chargé du redimensionnement ne sait pas travailler avec les 2 infos (w et h). -->
      </xsl:otherwise>

    </xsl:choose>
  </xsl:function>
  <!--
        Fonction qui retourne la cible du lien vers la visionneuse
        Les paramètres sont :
          $el : élément courant (dao | daoloc)
          $thumb : dao ou daoloc qui sert pour la vignette
          $eadimage : les caractéristiques de l'image renvoyées par le serveur d'images
        Les variables suivantes doivent aussi être définies :
          $img-viewer-base : URL relative de la visionneuse
      -->
  <xsl:function name="pleade:linkToViewer">
    <xsl:param name="el"/>
    <xsl:param name="thumb"/>
    <xsl:param name="eadimage"/>
    <xsl:param name="last"/>

    <xsl:choose>
      <xsl:when test="$el/@pleade:link-type = 'series'">
        <!-- Le début est toujours pareil -->

        <xsl:value-of select="concat($img-viewer-base, $eadimage/@href)"/>
        <xsl:choose>
          <xsl:when test="$el/@role='image' or $el/@role='navimages:image'">
            <xsl:value-of select="concat('?name=', $el/@pleade:img)"/>
          </xsl:when>
          <xsl:when test="$el/@role='first' or $el/@role='image:first' or $el/@role='navimages:image:first'">
            <xsl:value-of select="concat('?np=', $el/@pleade:img)" />
            <xsl:if test="$last">
              <xsl:value-of select="concat('&amp;nd=', $last/@pleade:img)" />
            </xsl:if>
            <xsl:if test="$thumb">
              <xsl:value-of select="concat('&amp;ns=', $thumb/@pleade:img)" />
            </xsl:if>
          </xsl:when>
          <xsl:when test="$thumb[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'] and $thumb/@pleade:url = $el/@pleade:url">
            <xsl:value-of select="concat('?ns=', $thumb/@pleade:img)" />
          </xsl:when>
          <!-- Si on pointe sur une image, on ajoute un ns= -->
          <xsl:when test="$el/@pleade:img!=''">
            <xsl:value-of select="concat('?ns=', $el/@pleade:img)"/>
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <!-- Une image relative -->
      <xsl:when test="$el/@pleade:link-type = 'relative'">
        <xsl:value-of select="concat($img-viewer-base, '_ead/', pleade:get-public-url($el, $el/@pleade:link-type, $eadid),'/viewer.html')" />
      </xsl:when>
      <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
      <!-- Une image absolue -->
      <!--<xsl:when test="@pleade:link-type = 'absolute'">
        <xsl:value-of select="@pleade:url" />
      </xsl:when>-->
      <xsl:otherwise>
        <!-- Une image attachée -->
        <xsl:value-of select="concat($img-viewer-base, '_ead/', pleade:get-public-url($el, $el/@pleade:link-type, $eadid), '/viewer.html')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!-- Affichage des liens dao erronés (n'ont pas pu être résolus lors de la publication) -->
  <xsl:template match="dao | daoloc" mode="dao-link-error">
    <p class="pl-pgd-medias-head msg-fail">
      <!--<i18n:translate>
        <i18n:text key="ead-error-dao-link-unresolved-params">&#160;</i18n:text>
        <i18n:param><xsl:value-of select="@href" /></i18n:param>
        <i18n:param><xsl:value-of select="@role" /></i18n:param>
      </i18n:translate>-->
      <i18n:text key="ead-error-dao-link-unresolved">&#160;</i18n:text>
    </p>
    <!--<img border="0" width="{$img-thumbnail-size}" height="{$img-thumbnail-size}" src="" alt="ead-error-dao-link-unresolved" i18n:attr="alt"/>-->
  </xsl:template>

  <!-- Traitement du lien pour l'affichage d'une image dans la visionneuse Pleade. -->
  <xsl:template name="medias-viewer-output">
    <xsl:param name="thumbnail-url" />
    <xsl:param name="thumbnail-size" />
    <xsl:param name="viewer-url" />
    <a class="pl-pgd-medias-thumbnail" title="medias.viewer.link.title" i18n:attr="title" href="{$viewer-url}" onclick="return eadWindow.getWindowManager().openImgViewer(this.href);">
      <!-- Si on a un aperçu on l'affiche -->
      <xsl:if test="$thumbnail-url != ''">
        <!--<img border="0" src="{$thumbnail-url}" width="{$thumbnail-size}" alt="medias.thumbnail.alt" i18n:attr="alt"/>-->
        <img border="0" src="{$thumbnail-url}" alt="medias.thumbnail.alt" i18n:attr="alt"/>
      </xsl:if>
    </a>
  </xsl:template>

  <!-- Traitement du lien pour l'affichage d'une image hors visionneuse Pleade.
  On s'en sert par exemple pour les images absolues. -->
  <xsl:template name="medias-image-output">
    <xsl:param name="thumbnail-url" />
    <xsl:param name="thumbnail-size" />
    <xsl:param name="viewer-url" />
    <a class="pl-pgd-medias-thumbnail" title="medias.viewer.link.title" i18n:attr="title" href="{$viewer-url}" target="_new">
      <!-- Si on a un aperçu on l'affiche -->
      <xsl:if test="$thumbnail-url != ''">
        <!--<img border="0" src="{$thumbnail-url}" alt="medias.thumbnail.alt" i18n:attr="alt"/>-->
        <img border="0" src="{$thumbnail-url}" width="{$thumbnail-size}" alt="medias.thumbnail.alt" i18n:attr="alt"/>
      </xsl:if>
    </a>
  </xsl:template>

  <!-- Trouve le titre (peut-être vide) d'un dao ou daoloc -->
  <xsl:template name="get-dao-title">
    <xsl:param name="dao" select="."/>
    <xsl:variable name="content">
      <xsl:apply-templates select="$dao/daodesc" mode="inline"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="normalize-space($content) != ''"><xsl:copy-of select="$content"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="$dao/@title | $dao/label"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Une macro pour sortir des informations sur un contenu multimédia -->
  <xsl:template name="output-media-infos">
    <xsl:param name="media"/>  <!-- dao, daoloc, ... -->
    <xsl:param name="mimetype" select="$media/@pleade:mime-type"/>
    <xsl:param name="url" select="$media/@pleade:url"/>
    <xsl:param name="target" select="'_self'"/>
    <div class="pl-pgd-medias-infos">
      <xsl:choose>
        <xsl:when test="$mimetype = 'application/pdf'">
          <xsl:call-template name="output-media-infos-pdf">
            <xsl:with-param name="media" select="$media" />
            <xsl:with-param name="mimetype" select="$mimetype" />
            <xsl:with-param name="url" select="$url" />
            <xsl:with-param name="target" select="$target" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="output-media-infos-others">
            <xsl:with-param name="media" select="$media" />
            <xsl:with-param name="mimetype" select="$mimetype" />
            <xsl:with-param name="url" select="$url" />
            <xsl:with-param name="target" select="$target" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>

  <xsl:template name="output-media-infos-pdf">
    <xsl:param name="media"/>  <!-- dao, daoloc, ... -->
    <xsl:param name="mimetype" select="$media/@pleade:mime-type"/>
    <xsl:param name="url" select="$media/@pleade:url"/>
    <xsl:param name="target" select="'_self'"/>
      <xsl:variable name="k" select="'medias.infos.intro'"/>
      <p>
      <i18n:text key="medias.infos.intro.pdf"/>
      <a href="{$url}" target="{$target}" title="medias.infos.download.title.pdf" i18n:attr="title">
        <i18n:text key="medias.infos.download.text.pdf">télécharger le document</i18n:text>
      </a>
      <!--<i18n:text key="{$k}3"><xsl:value-of select="$k"/>3</i18n:text>
      <a href="{$url}" target="{$target}"><i18n:text key="{$k}4"><xsl:value-of select="$k"/>4</i18n:text></a>
      <i18n:text key="{$k}5"><xsl:value-of select="$k"/>5</i18n:text>-->
    </p>
  </xsl:template>

  <xsl:template name="output-media-infos-others">
    <xsl:param name="media"/>  <!-- dao, daoloc, ... -->
    <xsl:param name="mimetype" select="$media/@pleade:mime-type"/>
    <xsl:param name="url" select="$media/@pleade:url"/>
    <xsl:param name="target" select="'_self'"/>
    <xsl:variable name="i18n-key-suffix" select="
      if( $mimetype='audio/mpeg' or $mimetype='application/x-shockwave-flash' or $mimetype='video/x-flv' ) then '.flash'
      else ''
    " />
    <!-- <p>
        <i18n:text key="{$k}1"><xsl:value-of select="$k"/>1</i18n:text>
        <xsl:value-of select="$mimetype"/>
        <i18n:text key="{$k}2"><xsl:value-of select="$k"/>2</i18n:text>
      </p>
      <p>
        <i18n:text key="{$k}3"><xsl:value-of select="$k"/>3</i18n:text>
        <a href="{$url}" target="{$target}"><i18n:text key="{$k}4"><xsl:value-of select="$k"/>4</i18n:text></a>
        <i18n:text key="{$k}5"><xsl:value-of select="$k"/>5</i18n:text>
    </p> -->
    <p>
    <i18n:translate>
      <i18n:text key="medias.infos.intro{$i18n-key-suffix}">télécharger le document sur votre poste de travail en utilisant le lien ci-contre&#160;: </i18n:text>
      <i18n:param><xsl:value-of select="$mimetype" /></i18n:param>
    </i18n:translate>
    <a href="{$url}" target="{$target}" title="medias.infos.download.title{$i18n-key-suffix}" i18n:attr="title">
      <i18n:text key="medias.infos.download.text{$i18n-key-suffix}">télécharger le document</i18n:text>
    </a>
      </p>
  </xsl:template>

  <!-- Une macro pour sortir un lecteur Windows media -->
  <xsl:template name="output-windows-media-player">
    <xsl:param name="id" select="''"/> <!-- Identifiant de l'objet -->
    <xsl:param name="w" select="''"/> <!-- Largeur -->
    <xsl:param name="h" select="''"/> <!-- Hauteur -->
    <xsl:param name="url" select="''"/> <!-- URL du contenu -->
    <xsl:param name="alt-content">
      <xsl:call-template name="output-media-infos">
        <xsl:with-param name="media" select="."/>
        <xsl:with-param name="url" select="$url"/>
      </xsl:call-template>
    </xsl:param>
    <object id="{$id}" width="{$w}" height="{$h}"
      classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"
      standby="medias.plugin.standby" i18n:attr="standby"
      type="application/x-oleobject"
    >
      <param name="autoStart" value="false"/>
      <param name="enableContextMenu" value="true"/>
      <param name="enabled" value="true"/>
      <param name="URL" value="{$url}"/>
      <object id="{$id}-embed" width="{$w}" height="{$h}"
        standby="medias.plugin.standby" i18n:attr="standby"
        type="application/x-ms-wmp"
      >
        <param name="autoStart" value="false"/>
        <param name="enableContextMenu" value="true"/>
        <param name="enabled" value="true"/>
        <param name="URL" value="{$url}"/>
        <xsl:copy-of select="$alt-content"/>
      </object>
    </object>
  </xsl:template>

  <!-- Une macro pour sortir du contenu Flash. On utilise l'outil swobject
    (http://code.google.com/p/swfobject/wiki/SWFObject_2_0_documentation) -->
  <!-- [JC] 20081125: L'utilisation de swfobject est-elle réellement requise ?
  Il semble que ce script ajoute un visibility:hidden, et ne fasse pas grand chose
  d'autre... -->
  <xsl:template name="output-flash-content">
    <xsl:param name="id" select="''"/> <!-- Identifiant de l'objet -->
    <xsl:param name="w" select="''"/> <!-- Largeur -->
    <xsl:param name="h" select="''"/> <!-- Hauteur -->
    <xsl:param name="movie" select="''"/> <!-- URL du contenu -->
    <xsl:param name="alt-content"> <!-- Contenu alternatif -->
      <div class="pl-pgd-media-alt-content">
        <xsl:variable name="k" select="'medias.plugin.alt-content'"/>
        <p><i18n:text key="{$k}"><xsl:value-of select="$k"/></i18n:text></p>
      </div>
    </xsl:param>
    <xsl:param name="other-params"/> <!-- Autres paramètres -->
    <xsl:param name="version" select="'8.0.0'"/>
    <object id="{$id}" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{$w}" height="{$h}">
      <param name="movie" value="{$movie}"/>
      <xsl:if test="$other-params"><xsl:copy-of select="$other-params"/></xsl:if>
      <!--[if !IE]>-->
      <object id="{$id}-embed" type="application/x-shockwave-flash" data="{$movie}" width="{$w}" height="{$h}">
        <xsl:if test="$other-params"><xsl:copy-of select="$other-params"/></xsl:if>
      <!--<![endif]-->
        <xsl:copy-of select="$alt-content"/>
      <!--[if !IE]>-->
      </object>
      <!--<![endif]-->
    </object>
  </xsl:template>

  <xsl:template name="output-video">
    <xsl:param name="url"/>
    <xsl:param name="title"/>
    <xsl:param name="alt-content"/>
    <xsl:call-template name="output-supported-format">
      <xsl:with-param name="width" select="'350'"/>
      <xsl:with-param name="height" select="'340'"/>
      <xsl:with-param name="url" select="$url"/>
      <xsl:with-param name="title" select="$title"/>
      <xsl:with-param name="type" select="'video'"/>
      <xsl:with-param name="alt-content" select="$alt-content"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="output-audio">
    <xsl:param name="url"/>
    <xsl:param name="title"/>
    <xsl:param name="alt-content"/>
    <xsl:call-template name="output-supported-format">
      <xsl:with-param name="width" select="'350'"/>
      <xsl:with-param name="height" select="'20'"/>
      <xsl:with-param name="url" select="$url"/>
      <xsl:with-param name="title" select="$title"/>
      <xsl:with-param name="type" select="'audio'"/>
      <xsl:with-param name="alt-content" select="$alt-content"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="output-supported-format">
    <xsl:param name="width"/>
    <xsl:param name="height"/>
    <xsl:param name="url"/>
    <xsl:param name="title"/>
    <xsl:param name="type"/>
    <xsl:param name="alt-content"/>
    <a href="{$url}" target="_blank" class="pl-flow-{$type}">
      <xsl:if test="normalize-space($title) != ''">
        <xsl:attribute name="title"><xsl:value-of select="$title"/></xsl:attribute>
      </xsl:if>
      <!-- [JC] TODO: il est possible de mettre une jacquette pour un fichier MP3 et une image représentative pour les formats vidéo ; mais je ne sais pas comment on pourrait renseigner ça proprement dans l'EAD... -->
      <!--<img src="img/video_thumb.jpg" alt="medias.infos.download.text" i18n:attr="alt"/>-->
      <xsl:value-of select="' '"/>
    </a>
  </xsl:template>

    <!--+
        |
        +-->
    <xsl:template match="origination/text()">

      <xsl:choose>

        <xsl:when test="$active-eac='true' and $mode='dyn'">

          <!-- L'URL pour interroger la base EAC sur l'existence éventuelle d'un doc EAC -->
          <!-- TODO (MP) : On se base pour le moment sur le contenu textuel de "origination", il faudra prévoir le cas où on a un identifiant quelque part.  -->
          <xsl:variable name="url">
            <xsl:value-of select="string('cocoon://functions/sdx-terms.xml?cache=false&amp;base=eac&amp;hpp=-1&amp;field=preferredname&amp;value=')" />
            <xsl:value-of select="normalize-space(.)" />
            <!-- INFO (MP) : Visiblement, pour un appel interne (cocoon:/) pas besoin d'encoder l'URL.
            <xsl:value-of select="urle:encode(string(normalize-space(.)), 'UTF-8')" xmlns:urle="java:java.net.URLEncoder" />-->
          </xsl:variable>
<!--
<xsl:message>
[EAD] Lien EAC pour "<xsl:value-of select="." />"
      url = "<xsl:value-of select="$url" />"
<xsl:choose>
  <xsl:when test="doc-available($url)">
    <xsl:choose>
      <xsl:when test="document($url)/sdx:document/sdx:terms[not(number(@nb)&gt;0)]">
      L'URL ne renvoie rien ; tente le coup sans attribut "value"
        <xsl:variable name="url2" select="string('cocoon://functions/sdx-terms.xml?cache=false&amp;base=eac&amp;hpp=-1&amp;field=preferredname')" />
        <xsl:choose>
          <xsl:when test="doc-available($url2)">
            <xsl:variable name="cur" select="normalize-space(.)" />
            <xsl:variable name="urls" select="urle:encode($cur, 'UTF-8')" xmlns:urle="java:java.net.URLEncoder" />
            <xsl:choose>
              <xsl:when test="document($url2)/sdx:document/sdx:terms/sdx:term[normalize-space(@escapedValue) = $urls]">
      On a une escapedValue = "<xsl:value-of select="$urls" />"
              </xsl:when>
              <xsl:when test="document($url2)/sdx:document/sdx:terms/sdx:term[normalize-space(@value) = $cur]">
      On a une value = "<xsl:value-of select="$cur" />"
              </xsl:when>
              <xsl:otherwise>
      Pas de matche !
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
      L'URL sans "value" ne renvoie pas de document XML valide !
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
      L'URL renvoie une reference : <xsl:value-of select="document($url)/sdx:document/sdx:terms/sdx:term[@docId!=''][1]/@docId" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:when>
  <xsl:otherwise>
      L'URL ne renvoie pas de document XML valide !
  </xsl:otherwise>
</xsl:choose>
</xsl:message>-->

          <!-- L'identifiant du doc EAC -->
          <xsl:variable name="eac-id">
            <xsl:if test="doc-available($url)">
              <!-- FIXME (MP) : Faut-il pendre en compte les cas où on a plusieurs doc EAC correspondant ? -->
              <xsl:value-of select="document($url)/sdx:document/sdx:terms/sdx:term[@docId!=''][1]/@docId" />
            </xsl:if>
          </xsl:variable>

          <xsl:choose>

            <!-- Activation du lien ouvrant le doc EAC -->
            <xsl:when test="$eac-id!=''">
              <a href="eac.html?id={$eac-id}" target="eac" onclick="return !(eadWindow.toggleEacBlock('{$eac-id}'));" onkeypress="return !(eadWindow.toggleEacBlock('{$eac-id}'));"
                 class="pl-lnk-eac" i18n:attr="title" title="ead.link.eac.title">
                <xsl:value-of select="." />
              </a>

              <div id="divEadDoc_{$eac-id}" class="div-eac-doc">
                <div class="close">
                  <a href="javascript:void(0);" onclick="return !(eadWindow.toggleEacBlock('{$eac-id}'));" title="module-eadeac:pl-commons.btn-close.panel.title" i18n:attr="title">
                    <i18n:text key="pl-commons.btn-close" catalogue="module-eadeac"/>
                  </a>
                </div>
              </div>

            </xsl:when>

            <!-- Pas de lien, affichage du contenu textuel de "origination" -->
            <xsl:otherwise>
              <xsl:value-of select="." />
            </xsl:otherwise>

          </xsl:choose>

<!-- DEBUG -->
<!--<xsl:message>
   url = <xsl:value-of select="$url" />
eac-id = <xsl:value-of select="$eac-id" /></xsl:message>-->

        </xsl:when>

        <!-- Pas de service EAC, on affiche simplement le contenu sans lien. -->
        <xsl:otherwise>
          <xsl:value-of select="." />
        </xsl:otherwise>

      </xsl:choose>

    </xsl:template>

    <!--Prise en charge de @render pour ses différentes valeurs possibles-->
    <!--@render = altrender  bold  bolddoublequote  bolditalic  boldsinglequote  boldsmcaps boldunderline
              doublequote  italic  nonproport  singlequote  smcaps  sub  super  underline-->
    <xsl:template match="text()[parent::*/@render]" priority="-1">
      <xsl:variable name="render">
        <xsl:choose>
          <xsl:when test="parent::*/@render = 'altrender'">
            <xsl:value-of select="parent::*/@altrender"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="parent::*/@render"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="content">
        <xsl:choose>
          <xsl:when test="contains($render, 'singlequote')">
            <xsl:text>'</xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>'</xsl:text>
          </xsl:when>
          <xsl:when test="contains($render, 'doublequote')">
            <i18n:text key="ead.display.doublequote.open"/>
            <xsl:value-of select="."/>
            <i18n:text key="ead.display.doublequote.close"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$render = 'sub'">
          <sub class="pl-ead-att-render-{$render}"><xsl:value-of select="$content"/></sub>
        </xsl:when>
        <xsl:when test="$render = 'super'">
          <sup class="pl-ead-att-render-{$render}"><xsl:value-of select="$content"/></sup>
        </xsl:when>
        <xsl:otherwise>
          <span class="pl-ead-att-render-{$render}">
            <xsl:value-of select="$content"/>
          </span>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

    <!--Mise en subrillance des termes sdx:hilite-->
    <xsl:template match="sdx:hilite">
      <xsl:choose>
        <xsl:when test="pleade:hilite-string(.)">
          <span class="pl-hilite"><xsl:value-of select="."/></span>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

    <!-- Affichage des éléments head -->
    <xsl:template match="head">
      <p class="pl-ead-head"><xsl:apply-templates select="." mode="label"/></p>
    </xsl:template>

  <xsl:template match="sdx:document/sdx:exception[@code='2000']">
    <div class="pl-pgd-component">
      <i18n:text key="ead.component.notfound"/>
    </div>
  </xsl:template>

  <xsl:template name="flashError">
    <div id="flashError">
      <img src="theme/images/flash.jpg" alt="" />
      <h3><i18n:text key="flash.error.title" catalogue="module-eadeac"/></h3>
      <p><i18n:text key="flash.error.content" catalogue="module-eadeac"/></p>
    </div>
  </xsl:template>

</xsl:stylesheet>
