<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="xsl fn saxon"
>

	<!-- Ajoute des informations concernant les liens hypertextuels internes. Ces informations
		 sont ajoutées au moment de la publication car c'est le seul moment où le document
		 EAD est entier. On peut donc en profiter pour résoudre les ID/IDREF.

		 Pour les liens vers des documents externes, on en profite pour déterminer s'ils doivent
		 constituer des documents attachés, définir l'adresse absolue, etc.
	-->

	<!-- Des fonctions générales, notamment pour résoudre des liens -->
	<xsl:import href="../../../../commons/xsl/functions.xsl"/>

	<!-- Des fonctions spécifiques à la gestion des ressources externes -->
	<xsl:import href="../../link-helpers.xsl"/>
	
	<!-- Des utilitaires pour la publication -->
	<xsl:import href="publication-helpers.xsl"/>

	<!-- Ce paramètre est envoyé par SDX (peut ne pas exister, si on indexe un DOM par exemple) -->
	<xsl:param name="docUrl" select="''"/>	<!-- TODO: statique -->

	<!-- Ce paramètre provient du sitemap : il donne l'URL de base des documents EAD -->
	<xsl:param name="ead-docs-dir" select="''"/>
	
	<!-- Le répertoire de stockage des PDF -->
	<xsl:param name="fs-pdf" select="''"/>
	
	<!-- Le répertoire de stockage des fichiers autres que PDF -->
	<xsl:param name="fs-files" select="''"/>

	<xsl:param name="debug" select="'false'"/>

	<!-- Un pointeur sur le fichier de propriétés de publication -->
	<!--<xsl:variable name="properties-url" select="concat('cocoon://functions/ead/get-properties/', /ead/@pleade:id, '/publication.xml')"/>
	<xsl:variable name="pub-properties" select="if(doc-available($properties-url)) then document($properties-url)/properties else ()"/>-->

	<!-- La liste des extensions de fichier à prendre comme document attachés pour les liens -->
	<xsl:variable name="links-attached-extensions" select="fn:tokenize($pub-properties/property[@name = 'attached-extensions'], '\s')"/>

	<!-- La liste des extensions de fichier à prendre comme document attachés pour les dao/daoloc -->
	<xsl:variable name="dao-attached-extensions" select="fn:tokenize($pub-properties/property[@name = 'dao-attached-extensions'], '\s')"/>

	<!--Les dao / daoloc doivent être implicitement ou pas considérés comme des séries -->
	<xsl:variable name="dao-implicit-series" select="boolean($pub-properties/property[@name = 'dao-implicit-series'] = 'true')"/>

	<!-- Matche la racine -->
	<xsl:template match="/">
		<xsl:message>	Traitement des liens (etape 8 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:if test="not($pub-properties/property)"><xsl:message terminate="yes">		Erreur interne : Impossible de recuperer le fichier de configuration de publication pour le document courant !</xsl:message></xsl:if>
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{ead/@pleade:id}/{ead/@pleade:id}-8.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- On intercepte tous les éléments -->
	<xsl:template match="*">

		<!-- On copie tous les éléments -->
		<xsl:copy>

			<!-- Les attributs existants -->
			<xsl:apply-templates select="@*"/>

			<!-- Les liaisons vers des ressources externes -->
			<xsl:if test="pleade:is-link(.) or pleade:is-digital-object(.)">

				<!-- Une variable pour distinguer les liens des archives numériques -->
				<xsl:variable name="element-type">
					<xsl:choose>
						<xsl:when test="pleade:is-link(.)">link</xsl:when>
						<xsl:otherwise>dao</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<!-- Il y a différents cas de figure en fonction du type de lien -->
				<xsl:choose>

					<!-- On peut identifier assez facilement les liens internes -->
					<xsl:when test="@target">
						<xsl:variable name="target" select="@target"/>
						<xsl:attribute name="pleade:link-type">internal</xsl:attribute>
						<xsl:variable name="target-element" select="//*[@id = $target][1]"/>
						<xsl:attribute name="pleade:target-fragment">
							<xsl:value-of select="$target-element/ancestor-or-self::*[@pleade:break='true'][1]/@pleade:id"/>
						</xsl:attribute>
						<xsl:attribute name="pleade:target-id">
							<xsl:value-of select="$target-element/@pleade:id"/>
						</xsl:attribute>
					</xsl:when>

					<!-- Les séries d'images ont un traitement un peu particulier (pas de mime-type notamment) -->
					<xsl:when test="$element-type = 'dao' and ($dao-implicit-series or starts-with(@role, 'navimages:series') or starts-with(@role, 'navimages:image') or starts-with(@role, 'image') or @role = 'pleade:series' or @role = 'series' or @role = 'first' or @role = 'last' or @role = 'image:first' or @role = 'image:last' or ends-with(@href, '/'))">

						<xsl:call-template name="get-img-series-attributes" />

					</xsl:when>

					<!-- Dans tous les autres cas, on aura une partie du traitement qui sera similaire -->
					<xsl:otherwise>

						<!-- L'adresse fournie par l'élément EAD -->
						<xsl:variable name="href" select="normalize-space(@href)"/>
						<!-- L'adresse absolue -->
						<xsl:variable name="absurl" select="fn:resolve-uri($href, $docUrl)"/>
						<!-- Le type MIME (l'attribut peut être vide si non trouvé) -->
						<xsl:attribute name="pleade:mime-type"><xsl:value-of select="pleade:get-mimetype($href)"/></xsl:attribute>
<!--<xsl:message>
        href=<xsl:value-of select="$href" />
				ext=<xsl:value-of select="pleade:get-extension($href)"/>
				$links-attached-extensions = <xsl:value-of select="$links-attached-extensions"/>
				is-att=<xsl:value-of select="pleade:is-att($href, $links-attached-extensions)"/>
      absurl=<xsl:value-of select="$absurl" />
  fileExists=<xsl:value-of select="netutils:fileExists($absurl)" xmlns:netutils="java:org.pleade.utils.NetUtilities" />
	fileExistsInfs-pdf=<xsl:value-of select="netutils:fileExists(concat($fs-pdf, @href))" xmlns:netutils="java:org.pleade.utils.NetUtilities" />
      relurl=<xsl:value-of select="netutils:relativize($ead-docs-dir, $absurl)" xmlns:netutils="java:org.pleade.utils.NetUtilities" />
element-type=<xsl:value-of select="$element-type" />
</xsl:message>-->
						<!-- On va distinguer les documents à attacher des autres -->
						<xsl:choose>
							<!-- Les adresses absolues ne sont jamais attachées-->
							<!--<xsl:when test="(fn:matches($href, '^[a-z]+://') and not(fn:matches($href, '^file:/'))) or starts-with($href, 'mailto:')">-->
							<xsl:when test="fn:matches($href, '^[a-z]+:/') or starts-with($href, 'mailto:')">
								<xsl:attribute name="pleade:link-type">absolute</xsl:attribute>
								<xsl:attribute name="pleade:url"><xsl:value-of select="$href"/></xsl:attribute>
							</xsl:when>
							<!--+
							    | Les documents que l'on doit attacher 
							    | Le lien vers les PDF peuvent être relatifs au dossier de stockage donné par le paramètre fs-pdf
							    +-->
							<xsl:when test="netutils:fileExists($absurl) or (pleade:get-mimetype($href) = 'application/pdf' and netutils:fileExists(concat($fs-pdf, $href)))" xmlns:netutils="java:org.pleade.utils.NetUtilities">
								<xsl:choose>
									<xsl:when test="($element-type = 'link' and pleade:is-att($href, $links-attached-extensions)) or ($element-type = 'dao' and pleade:is-att($href, $dao-attached-extensions))">
										<xsl:attribute name="pleade:link-type">attached</xsl:attribute>
										<xsl:attribute name="pleade:file"><xsl:value-of select="if (pleade:get-mimetype($href) = 'application/pdf' and netutils:fileExists(concat($fs-pdf, $href))) then concat($fs-pdf, $href) else $absurl"/></xsl:attribute>
										<xsl:attribute name="pleade:ext"><xsl:value-of select="pleade:get-extension($href)"/></xsl:attribute>
									</xsl:when>
									<!-- Le cas des PDF stockés dans $fs-pdf -->
									<xsl:when test="netutils:fileExists(concat($fs-pdf, $href))">
										<xsl:attribute name="pleade:link-type">detached</xsl:attribute>
										<xsl:attribute name="pleade:file"><xsl:value-of select="$absurl"/></xsl:attribute>
										<!-- FIXME: serait bien d'intercepter des erreurs dans l'URL, par exemple des espaces... -->
										<xsl:attribute name="pleade:url"><xsl:value-of select="$href"/></xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="pleade:link-type">relative</xsl:attribute>
										<xsl:attribute name="pleade:file"><xsl:value-of select="$absurl"/></xsl:attribute>
										<!-- FIXME: serait bien d'intercepter des erreurs dans l'URL, par exemple des espaces... -->
										<xsl:variable name="relurl" select="netutils:relativize($ead-docs-dir, $absurl)" xmlns:netutils="java:org.pleade.utils.NetUtilities"/>
										<xsl:attribute name="pleade:url"><xsl:value-of select="$relurl"/></xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<!-- Le cas du serveur d'images ne doit prendre en compte que des images -->
							<xsl:when test="$element-type = 'dao' and starts-with(pleade:get-mimetype($href), 'image')">
								<!-- On sort une erreur pour le cas que l'on ne sait pas traiter. -->
								<xsl:attribute name="pleade:link-type"><xsl:text>dao-link-error</xsl:text></xsl:attribute>
								<xsl:attribute name="pleade:url"><xsl:value-of select="@href" /></xsl:attribute>
							</xsl:when>
              <!-- On considère que tout lien vers un fichier XML est un lien interne vers un autre docyument EAD ; on le traite on conséquence -->
              <xsl:when test="pleade:get-mimetype($href) = 'application/xml'">
                <xsl:attribute name="pleade:link-type">relative</xsl:attribute>
                <xsl:variable name="relurl" select="netutils:relativize($ead-docs-dir, $absurl)" xmlns:netutils="java:org.pleade.utils.NetUtilities"/>  <!-- FIXME: serait bien d'intercepter des erreurs dans l'URL, par exemple des espaces... -->
                <xsl:attribute name="pleade:url"><xsl:value-of select="$relurl"/></xsl:attribute>
              </xsl:when>
              <!-- Ensuite on va considérer que ce sont des chemins relatifs -->
              <xsl:otherwise>
                <xsl:choose>
                  <!-- SI les fichiers (autres que PDF) sont stockés dans un dossier spécifique) -->
                  <xsl:when test="$fs-files != ''">
                    <xsl:attribute name="pleade:link-type">detached</xsl:attribute>
                    <xsl:attribute name="pleade:file"><xsl:value-of select="$absurl"/></xsl:attribute>
                    <!-- FIXME: serait bien d'intercepter des erreurs dans l'URL, par exemple des espaces... -->
                    <xsl:attribute name="pleade:url"><xsl:value-of select="$href"/></xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="pleade:link-type">relative</xsl:attribute>
                    <xsl:variable name="relurl" select="netutils:relativize($ead-docs-dir, $absurl)" xmlns:netutils="java:org.pleade.utils.NetUtilities"/>	<!-- FIXME: serait bien d'intercepter des erreurs dans l'URL, par exemple des espaces... -->
                    <xsl:attribute name="pleade:url"><xsl:value-of select="$relurl"/></xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>

					</xsl:otherwise>

				</xsl:choose>

			</xsl:if>

			<xsl:apply-templates select="node()"/>

		</xsl:copy>

	</xsl:template>

	<!--+
	    | Formatage des attributs @pleade:link-type, @pleade:url et @pleade:img d'une série d'images
	    +-->
	<xsl:template name="get-img-series-attributes">

		<xsl:param name="href" select="@href" />

		<xsl:attribute name="pleade:link-type">
			<xsl:text>series</xsl:text>
		</xsl:attribute>

		<!-- L'attribut pleade:url contient l'adresse de la série, indépendamment du fait de pointer sur une image en particulier -->
		<xsl:variable name="url">
			<xsl:choose>
				<!-- Cas facile : href="dossier/sous-dossier/" -->
				<xsl:when test="ends-with($href, '/')"><xsl:value-of select="$href"/></xsl:when>
				<!-- Pour ceux qui oublient de mettre un "/" à la fin -->
				<xsl:when test="not(contains(fn:tokenize($href, '/')[last()], '.'))"><xsl:value-of select="$href"/>/</xsl:when>
				<!-- Les autres ont probablement une image, on la retire de l'URL -->
				<xsl:otherwise>
					<xsl:variable name="img" select="fn:tokenize($href, '/')[last()]"/>
					<xsl:value-of select="substring-before($href, $img)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:attribute name="pleade:url">
			<xsl:value-of select="$url"/>
		</xsl:attribute>

		<!-- S'il y a une image, on doit l'ajouter dans un autre attribut -->
		<xsl:variable name="img" select="substring-after(@href, $url)"/>

		<xsl:if test="$img != ''">
			<xsl:attribute name="pleade:img"><xsl:value-of select="$img"/></xsl:attribute>
		</xsl:if>

	</xsl:template>

	<!-- De manière générale, on copie tout (les commentaires sont déjà supprimés) -->
	<xsl:template match="text()|comment()|processing-instruction()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

