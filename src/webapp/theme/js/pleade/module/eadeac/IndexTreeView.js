//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de gérer l'index de la fenêtre EAD
 * @author Julien Huet
 */
/**
 * @class Classe permettant de gérer l'index de la fenêtre EAD
 * @augments TreeView
 * @author Julien Huet
 */
var IndexTreeView = new Class(/** @lends IndexTreeView.prototype */{
	Extends: TreeView,

	/**
	* <strong>Constructor</strong><br/> 
	* @param {EadWindow} eadWindow
	*/
	initialize: function(eadWindow){
		this.eadWindow = eadWindow;
		
		if(!$('pl-pgd-index')){
			return null;
		}
		
		var qidParam = '';
		
		var _qid = this.eadWindow.getContentManager().getQid();
		var _eadid = this.eadWindow.getContentManager().getEadId();
		var _base = this.eadWindow.getContentManager().getBase();
		
		if (!_qid.blank() && _qid != null) {
			qidParam = '?qid=' + _qid;
		}
		
		var urlStart = 'functions/' + _base + '/get-index/';
		var urlEnd = '/pleade-index.ajax-html' + qidParam;
		var url = urlStart + _eadid + urlEnd;
		
		// appel au constructeur parent
		this.parent(url, $('pl-pgd-index'), urlStart, urlEnd, eadWindow.getContentManager(), $('tabs'));

		this.noTreeMessage = _usersMessages.indeces_list_empty;
		
		// On redefini la fonction permettant de recuperer le lien pour charger les enfants
		this.getTree().getChildrenUrl = function(node){
			return node.getDataHref();
		};
		
		// surcharge de la methode  setHtmlNodeEvent qui permet de rajouter un evement html
		this.getTree().setHtmlNode = function(child){
			if(child.getNodeLink()){
				child.getNodeLink().setProperty('onclick','return false');
			}
			return child.getHtml();
		};
	},

	/**
	* Surcharge de la fonction onSelect 
	* @private
	* @param {Object} node
	*/
	onSelect: function(node){
		// on regarde si le noeud possede un lien dans ses proprités
		if ( node.data.href ) {
			// recherche dans le nom du noeud s'il possede plus de deux documents
			if ( node.name.contains('pl-index-multi') ) {
				this.treeManager.eadWindow.getIndex().onLabelClickIndexMultiple(node);
			} else if ( node.name.contains('pl-index-single') ) {
				this.treeManager.eadWindow.getIndex().onLabelClickIndexSingle(node);
			} else {
				this.treeManager.eadWindow.getIndex().onLabelClickDoNothing(node);
			}
		}
	},

	/**
	* Fonction appelée lorsque l'on clique sur un noeud de l'index qui possède plusieurs documents
	* @private
	* @param {Mif.Tree.Node} node
	*/
	onLabelClickIndexMultiple: function(node){
		this.eadWindow.getContentManager().updateAjaxContent(node.data.href, $('pl-pg-body-main-mcol'));
	},

	/**
	* Fonction appelée lorsque l'on clique sur un noeud de l'index qui possède un seul document
	* @private
	* @param {Mif.Tree.Node} node
	*/
	onLabelClickIndexSingle: function(node){
		this.eadWindow.getContentManager().loadFragment(node.data.href);
	},

	/**
	* Fonction appelée lorsque l'on clique sur un noeud de l'index qui ne possède pas de document
	* @param {Mif.Tree.Node} node
	*/
	onLabelClickDoNothing: function(node){
		return false;
	}
});