<?xml version="1.0" encoding="UTF-8"?>
<!--+
    | <sdx:document [...]>
		|  <sdx:loggingUtilsIndexation xml:lang="fr" id="ead" locale="fr_FR" encoding="UTF-8" object="fr.gouv.culture.sdx.utils.logging.LoggingUtilsIndexation@10db16c">
		|   <sdx:IndexationID>1245930753401</sdx:IndexationID>
		|   <sdx:nbdocs>9</sdx:nbdocs>
		|   <sdx:doc_start>1245930772018</sdx:doc_start>
		|   <sdx:doc_end>0</sdx:doc_end>
		|   <sdx:doc_attached_rank>0</sdx:doc_attached_rank>
		|   <sdx:doc_nbattached>0</sdx:doc_nbattached>
		|   <sdx:steps>0</sdx:steps>
		|   <sdx:sdxdbid>ead</sdx:sdxdbid>
		|   <sdx:doc_rank>9</sdx:doc_rank>
		|   <sdx:start>1245930753401</sdx:start>
		|   <sdx:doc_id>FRAD064_IR0121_de-1</sdx:doc_id>
		|   <sdx:doc_nbsubocs>1</sdx:doc_nbsubocs>
		|   <sdx:doc_subdoc_rank>0</sdx:doc_subdoc_rank>
		|   <sdx:end>1245930775481</sdx:end>
		|   <sdx:step>2</sdx:step>
		|  </sdx:loggingUtilsIndexation>
		| </sdx:document>
    +-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		exclude-result-prefixes="xsl xsp sdx">

		<xsl:template match="info">
			<div class="pl-msg">
				<xsl:value-of select="@message" />
			</div>
		</xsl:template>

		<xsl:template match="sdx:loggingIndexation">
			<table style="border:1px solid" class="sdx-lui" id="indexationlog{if(sdx:IndexationID!='') then concat('-',sdx:IndexationID) else ''}">
				<tbody>
				<xsl:apply-templates select="sdx:sdxdbid" />
				<xsl:apply-templates select="sdx:steps" />
				<xsl:apply-templates select="sdx:step" />
				<xsl:apply-templates select="sdx:nbdocs" />
				<xsl:apply-templates select="sdx:doc_rank" />
				<xsl:apply-templates select="sdx:start" />
				<xsl:apply-templates select="sdx:end" />
				<xsl:if test="sdx:end='0' or sdx:end='-1' or sdx:end=''">
					<tr class="sdx-lui-doc" id="indexationlog{if(sdx:doc_id!='') then concat('-',sdx:doc_id) else ''}" colspan="2">
						<td class="label title">
							<i18n:text key="log-indexation.{local-name()}">&#160;</i18n:text>
						</td>
					</tr>
					<xsl:apply-templates select="sdx:doc_rank" />
					<xsl:apply-templates select="sdx:doc_start" />
					<xsl:apply-templates select="sdx:doc_end" />
					<xsl:apply-templates select="sdx:doc_id" />
					<xsl:apply-templates select="sdx:doc_nbattached" />
					<xsl:apply-templates select="sdx:doc_attached_rank" />
					<xsl:apply-templates select="sdx:doc_nbsubdocs" />
					<xsl:apply-templates select="sdx:doc_subdoc_rank" />
				</xsl:if>
				</tbody>
			</table>
		</xsl:template>

		<xsl:template match="sdx:loggingUtilsIndexation/sdx:*">
			<tr class="sdx-lui sdx-lui-{local-name()}">
				<td class="label"><i18n:text key="log-indexation.{local-name()}"><xsl:value-of select="local-name()" /></i18n:text></td>
				<td class="value"><xsl:apply-templates mode="value" /></td>
			</tr>
		</xsl:template>

		<xsl:template match="sdx:loggingUtilsIndexation/sdx:*" mode="value">
			<xsl:choose>
			  <xsl:when test="self::sdx:step"></xsl:when>
			  <xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:template>

</xsl:stylesheet>
