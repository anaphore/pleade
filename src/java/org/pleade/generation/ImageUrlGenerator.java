/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.generation;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.generation.AbstractGenerator;
import org.apache.excalibur.source.Source;
import org.apache.excalibur.source.SourceNotFoundException;
import org.pleade.utils.ImageInformation;
import org.pleade.utils.ImageUtilities;
import org.pleade.utils.NetUtilities;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.generation.ServiceableGenerator;
import org.apache.cocoon.caching.CacheableProcessingComponent;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import org.apache.excalibur.source.Source;
import org.apache.excalibur.source.SourceException;
import org.apache.excalibur.source.SourceValidity;
import org.apache.cocoon.components.source.SourceUtil;

public class ImageUrlGenerator extends ServiceableGenerator implements CacheableProcessingComponent {

    /** The input source */
    protected Source inputSource;

    /** Le prefixe des elemnents */
    private final static String PREFIX = "dir";

    /** Le namespace des elements */
    private final static String URI = "http://apache.org/cocoon/directory/2.0";

    /** le nom de l'element racine */
    protected final static String DIRECTORY_ELEMENT_NAME = "directory";

    /** Le nom des elements pour les images (fichiers) */
    protected final static String FILE_ELEMENT_NAME = "file";

    /** Le nom de l'attribut pour la src */
    protected final static String IMAGE_NAME_ATTR_NAME = "name";
    /** Le nom de l'attribut pour la largeur */
    protected final static String IMAGE_WIDTH_ATTR_NAME = "width";
    /** Le nom de l'attribut pour la hauteur */
    protected final static String IMAGE_HEIGHT_ATTR_NAME = "height";
    /** Le nom de l'attribut pour le DPI */
    protected final static String IMAGE_DPI_ATTR_NAME = "dpi";
    /** Le nom de l'attribut pour le MIME Type */
    protected final static String IMAGE_MIMETYPE_ATTR_NAME = "mimetype";


    /**
     * Recycle this component.
     * All instance variables are set to <code>null</code>.
     */
    public void recycle() {
        if (null != this.inputSource) {
            super.resolver.release(this.inputSource);
            this.inputSource = null;
        }
        super.recycle();
    }


    /**
     * Setup the file generator.
     * Try to get the last modification date of the source for caching.
     */
    public void setup(SourceResolver resolver, Map objectModel, String src, Parameters par)
        throws ProcessingException, SAXException, IOException {

        super.setup(resolver, objectModel, src, par);
        try {
            this.inputSource = super.resolver.resolveURI(src);
        } catch (SourceException se) {
            throw SourceUtil.handle("Error during resolving of '" + src + "'.", se);
        }
    }

    /**
     * Generate the unique key.
     * This key must be unique inside the space of this component.
     *
     * @return The generated key hashes the src
     */
    public Serializable getKey() {
        return this.inputSource.getURI();
    }

    /**
     * Generate the validity object.
     *
     * @return The generated validity object or <code>null</code> if the
     *         component is currently not cacheable.
     */
    public SourceValidity getValidity() {
        return this.inputSource.getValidity();
    }

    public void generate() throws SAXException, ProcessingException {

        // Le debut du document
        this.contentHandler.startDocument();
        this.contentHandler.startPrefixMapping(PREFIX, URI);

        // L'element racine
        this.contentHandler.startElement(URI, DIRECTORY_ELEMENT_NAME, PREFIX + ":" + DIRECTORY_ELEMENT_NAME, new AttributesImpl());

        try {
            InputStream is = this.inputSource.getInputStream();
            ImageInformation iInfo = ImageUtilities.getImageInformation(this.objectModel, getLogger(), this.source, is);

            if (is != null)
                is.close();

            if ( iInfo != null ) {
                // Maintenant on peut sortir les attributs si on a les informations
                AttributesImpl attributes = new AttributesImpl();
                attributes.addAttribute("", IMAGE_NAME_ATTR_NAME, IMAGE_NAME_ATTR_NAME, "CDATA", NetUtilities.getFilePart(this.source));
                if ( iInfo.getWidth() != 1 ) attributes.addAttribute("", IMAGE_WIDTH_ATTR_NAME, IMAGE_WIDTH_ATTR_NAME, "CDATA", "" + iInfo.getWidth());
                if ( iInfo.getHeight() != 1 ) attributes.addAttribute("", IMAGE_HEIGHT_ATTR_NAME, IMAGE_HEIGHT_ATTR_NAME, "CDATA", "" + iInfo.getHeight());
                if ( iInfo.getDpi() != 1 ) attributes.addAttribute("", IMAGE_DPI_ATTR_NAME, IMAGE_DPI_ATTR_NAME, "CDATA", "" + iInfo.getDpi());
                if ( iInfo.getMimetype() != null ) attributes.addAttribute("", IMAGE_MIMETYPE_ATTR_NAME, IMAGE_MIMETYPE_ATTR_NAME, "CDATA", iInfo.getMimetype());
                this.contentHandler.startElement(URI, FILE_ELEMENT_NAME, PREFIX + ":" + FILE_ELEMENT_NAME, attributes);
                this.contentHandler.endElement(URI, FILE_ELEMENT_NAME, PREFIX + ":" + FILE_ELEMENT_NAME);
            }
        } catch (IOException e) {
            getLogger().error("Probleme lors de la lecture de l'image", e);
        }

        // La fin de l'element racine
        this.contentHandler.endElement(URI, DIRECTORY_ELEMENT_NAME, PREFIX + ":" + DIRECTORY_ELEMENT_NAME);

        // La fin du document
        this.contentHandler.endPrefixMapping(PREFIX);
        this.contentHandler.endDocument();
    }

}
