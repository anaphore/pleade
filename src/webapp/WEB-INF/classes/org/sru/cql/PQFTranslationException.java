// $Id: PQFTranslationException.java 19706 2010-11-25 16:52:20Z jcwiklinski $

package org.sru.cql;
import java.lang.Exception;


/**
 * Base class for exceptions occurring when translating parse trees to PQF.
 *
 * @version $Id: PQFTranslationException.java 19706 2010-11-25 16:52:20Z jcwiklinski $
 */
public class PQFTranslationException extends Exception {
    PQFTranslationException(String s) {
	super(s);
    }
}
