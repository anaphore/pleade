<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		exclude-result-prefixes="xsl i18n">

	<!-- Cette XSLT permet de retourner une liste d'index à naviguer pour un
			document EAD. Cette liste pourra par la suite sérialisée en
			JSON pour être naviguée.

		En entée, elle reçoit trois sources de données:

			- le fichier pleade-index.xml : liste des index générés
			- les propriétés d'affichage pour le document
			- la définition des index de l'installation

		Ces trois sources sont incluses dans un élément root.

		En entrée, elle prend un fichier pleade-index.xml. Elle utilise également
		un fichier de configuration
	-->

	<!-- Le XML en entrée est un fichier pleade-index.xml -->

	<!-- L'identifiant du document EAD -->
	<xsl:param name="eadid" select="''"/>

	<!-- L'identifiant de l'index -->
	<xsl:param name="index" select="''"/>

	<!-- L'identifiant de la recherche courante le cas échéant -->
	<xsl:param name="qid" select="''"/>

	<xsl:param name="base" select="'ead'" />

	<!--le qid sous forme de parametre-->
	<xsl:variable name="qidParam">
		<xsl:if test="$qid!=''">
			<xsl:text>&amp;qid=</xsl:text>
			<xsl:value-of select="$qid"/>
		</xsl:if>
	</xsl:variable>

	<!-- Le champ SDX local -->
	<xsl:variable name="local-sdxfield" select="/*/@local-sdxfield"/>

	<!-- L'adresse de base pour appeler les fonctions -->
	<xsl:variable name="base-url" select="concat('functions/',$base,'/get-index/', $eadid, '/')"/>


	<!-- L'élément racine -->
	<xsl:template match="/*">
		<root array="true" fulltext="true">
			<!-- TODO: bonne liste d'index! -->
			<xsl:choose>
				<xsl:when test="self::indices">
					<!-- TODO: seulement ceux à afficher! -->
					<xsl:apply-templates select="index"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="*"/>
				</xsl:otherwise>
			</xsl:choose>
		</root>
	</xsl:template>

	<!-- Un index dans une liste d'index -->
	<xsl:template match="indices/index">
		<index>
			<label><i18n:text key="{$base}.nav.index.{@index-id}"><xsl:value-of select="@index-id" /></i18n:text></label>
			<href>
				<xsl:value-of select="$base-url"/><xsl:value-of select="substring-before(@file, '.xml')"/>
				<xsl:text>.json</xsl:text>
				<xsl:value-of select="translate($qidParam,'&amp;','?')"/>
			</href>
			<hasChildren>true</hasChildren>
		</index>
	</xsl:template>

	<!-- Un groupe dans un index -->
	<xsl:template match="group">
		<group>
			<label><xsl:value-of select="@id"/></label>
			<href>
				<xsl:value-of select="$base-url"/><xsl:value-of select="$index"/><xsl:text>/initial.json</xsl:text><xsl:value-of select="concat('?ichar=', urle:encode(string(@id), 'UTF-8'))" xmlns:urle="java:java.net.URLEncoder"/><xsl:value-of select="$qidParam"/>
			</href>
			<hasChildren>true</hasChildren>
		</group>
	</xsl:template>

	<!-- Un terme dans un index ou un groupe -->
	<xsl:template match="term">
		<term>
			<label><xsl:value-of select="."/>&#160;(<xsl:value-of select="@nb"/>)</label>
			<xsl:choose>
				<xsl:when test="@target!=''">
					<href><xsl:value-of select="$base" />-fragment.xsp?c=<xsl:value-of select="@target"/><xsl:value-of select="$qidParam"/></href>
				</xsl:when>
				<xsl:otherwise>
					<href>docsearch-term.xsp?r=<xsl:value-of select="$eadid"/>&amp;base=<xsl:value-of select="$base" />&amp;f=<xsl:value-of select="$local-sdxfield"/>&amp;v=<xsl:value-of select="string(.)"/></href>  
				</xsl:otherwise>
			</xsl:choose>
			<hasChildren>false</hasChildren>
			<nb><xsl:value-of select="@nb"/></nb>
		</term>
	</xsl:template>

</xsl:stylesheet>
