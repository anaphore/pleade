<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT pour supprimer les contenus protégés avant leur affichage
		La suppression est conditionnelle à :
		 - un rôle de l'utilisateur : admin ou eadeac-editor 
		FIXME : Pourrait l'être à une origine (internet vs. intranet)
-->
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xsl"
	version="2.0">

	<xsl:param name="eadid" select="''"/>
  <xsl:param name="cid" select="''"/>

	<!-- Les propriétés d'affichage -->
	<xsl:variable name="prop-url" select="concat('cocoon:/functions/ead/get-properties/', $eadid, '/display.xml')" />
	<xsl:variable name="display-properties" select="
			if(doc-available($prop-url)) then document($prop-url)/properties
			else ''
		"/>

	<xsl:template match="/">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="xhtml:div[@id='pl-pg-body-main-lcol']">
		<!-- On ne recopie pas la navigation si le paramètre d'affichage est à false -->
		<xsl:if test="$display-properties/property[@name='display-navigation'] = 'true'">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- On copie tout -->
	<xsl:template match="node()|@*">
		<xsl:choose>
			<xsl:when test="namespace-uri() != ''">
				<xsl:copy>
					<xsl:apply-templates select="node()|@*"/>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy copy-namespaces="no">
					<xsl:apply-templates select="node()|@*"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
