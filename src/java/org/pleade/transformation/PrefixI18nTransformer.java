/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation;
import org.apache.cocoon.transformation.I18nTransformer;
import org.apache.cocoon.xml.ParamSaxBuffer;

public class PrefixI18nTransformer extends I18nTransformer
{
	private static String PREFIX_SEPARATOR = ".";

	protected ParamSaxBuffer getMessage(String catalogueId, String key) {
		ParamSaxBuffer ret = super.getMessage(catalogueId, key);
		if (ret != null)
			return ret;
        String newKey = stripSuffix(key);
		if (newKey != null)
			return getMessage(catalogueId, newKey);
		return null;
	}

	private static String stripSuffix(String key) {
		if (key == null)
			return null;
		int pos = key.lastIndexOf(PREFIX_SEPARATOR);
		if (pos > 0)
			return key.substring(0, pos);
		return null;
	}

	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++)
			System.out.println(args[i] + " => " + stripSuffix(args[i]));
	}
}
