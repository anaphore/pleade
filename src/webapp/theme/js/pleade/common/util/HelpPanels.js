//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de gerer les panneaux d'aide. Cette class est notameent utilisée pour les implementations.
 * @author Julien Huet
 */
/**
 * @class Classe permettant de gerer les panneaux d'aide. Cette class est notameent utilisée pour les implementations.
 * @author Julien Huet
 */
var HelpPanels = new Class(/** @lends HelpPanels.prototype */{

	/**<strong>Constructor</strong><br/> */
	initialize: function(){},

	/**
	* Fonction permettant de generer des panneaux d'aide pour tous les elements de la pages qui possède une class css 'panel-help' 
	* @param {Integer} width  Lageur des panneaux
	* @param {Integer} height Hauteur des panneaux
	*/
	initHelpPanels: function(width, height){
		if ( !height ) {
			height = '200';
		}
		if ( !width ) {
			width = '400';
		}
		width = width + 'px';
		height = height + 'px';

		$$('.panel-help').each(function(div) {
			div.setStyle('display', 'block'); //on s'assure que ce sera affiche puisqu'on le cache au chargement de la page
			var id = div.getAttribute('id');
			var idButton = id + '-button';

			var panel = new YuiPanel(id,width,height);

			$(idButton).addEvent('click', function() {
				try {
					if ( panel.isVisible() ) {
						panel.hide();
					} else {
						panel.show();
					}
				} catch(e) {
					panel.show();
				}
			});

			panel.render();
		});
	}
});
