<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns="http://www.w3.org/1999/XSL/Format"
		exclude-result-prefixes="xsl">
	
	<!-- Les attributs de formatage appliqués à l'ensemble du contenu -->
	<xsl:attribute-set name="general">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- Attributs pour l'ensemble du pied de page -->
	<xsl:attribute-set name="footer" use-attribute-sets="general">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="padding-top">2pt</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- La première ligne du pied de page à gauche -->
	<xsl:attribute-set name="footer-line1">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

	<!-- La deuxième ligne du pied de page à gauche -->
	<xsl:attribute-set name="footer-line2">
		<xsl:attribute name="font-style">italic</xsl:attribute>
		<xsl:attribute name="margin-top">2pt</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- Ligne du pied pour le copyright -->
	<xsl:attribute-set name="footer-line3">
	</xsl:attribute-set>
	
	<!-- Les ancêtres -->
	<xsl:attribute-set name="ancestors">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="margin">0pt</xsl:attribute>
		<xsl:attribute name="font-weight">normal</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- Le titre d'une unité -->
	<xsl:attribute-set name="component-title">
		<xsl:attribute name="padding">2pt</xsl:attribute>
		<xsl:attribute name="padding-top">5pt</xsl:attribute>
		<xsl:attribute name="font-family">Verdana</xsl:attribute>
		<xsl:attribute name="margin-bottom">2pt</xsl:attribute>
		<!-- <xsl:attribute name="border-top">0.5pt solid black</xsl:attribute> -->
	</xsl:attribute-set>
	<!-- Ensuite pour les différents niveaux -->
	<xsl:attribute-set name="component-title-1" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">0pt</xsl:attribute>
		<xsl:attribute name="font-size">120%</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="border">1pt solid #333333</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-2" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">12pt</xsl:attribute>
		<xsl:attribute name="font-size">110%</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-3" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
		<xsl:attribute name="font-size">105%</xsl:attribute>
		<!-- <xsl:attribute name="font-weight">bold</xsl:attribute> -->
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-4" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
		<xsl:attribute name="font-size">102%</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-5" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-6" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-7" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-8" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-9" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="component-title-10" use-attribute-sets="component-title">
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- Un en-tête de section, par exemple scopecontent TODO: différents niveaux -->
	<xsl:attribute-set name="pl-pgd-head" use-attribute-sets="block">
		<xsl:attribute name="font-style">italic</xsl:attribute>
		<xsl:attribute name="font-family">Verdana</xsl:attribute>
		<xsl:attribute name="margin-top">9pt</xsl:attribute>
		<xsl:attribute name="margin-bottom">0pt</xsl:attribute>
	</xsl:attribute-set>

	<!-- Un bloc pour l'ensemble d'une unité de description -->
	<xsl:attribute-set name="component-content">
		<xsl:attribute name="margin-left">5mm</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- Les débuts de groupes de mots clés -->
	<xsl:attribute-set name="controlaccess-group-header">
		<xsl:attribute name="font-style">italic</xsl:attribute>
	</xsl:attribute-set>

	<!-- Styles génériques pour un paragraphe -->
	<xsl:attribute-set name="block">
		<xsl:attribute name="margin-top">0pt</xsl:attribute>
		<xsl:attribute name="margin-bottom">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<!-- Un tableau champ / valeur -->
	<xsl:attribute-set name="table-fv">
		<xsl:attribute name="margin-top">0pt</xsl:attribute>
		<xsl:attribute name="margin-bottom">3pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="inline">
		<!-- <xsl:attribute name="font-size">12pt</xsl:attribute> -->
	</xsl:attribute-set>

	<xsl:attribute-set name="table">
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
		<xsl:attribute name="margin-bottom">10px</xsl:attribute>
		<xsl:attribute name="border">1px solid #38c4f0</xsl:attribute>
	</xsl:attribute-set>

	<!-- TODO -->
	<xsl:attribute-set name="caption">
		<xsl:attribute name="alignment-adjust">middle</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="font-size">120%</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="table-row">
		<xsl:attribute name="background-color">#EEEEEE</xsl:attribute>
		<!-- <xsl:attribute name="border-bottom">1px solid #333333</xsl:attribute> -->
	</xsl:attribute-set>

	<xsl:attribute-set name="td-as-table-cell">
		<xsl:attribute name="margin-left">0pt</xsl:attribute>
		<xsl:attribute name="border">1px solid #FFFFFF</xsl:attribute>
		<xsl:attribute name="padding">2pt</xsl:attribute>
		<xsl:attribute name="padding-left">4pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="th-as-table-cell">
		<xsl:attribute name="margin-left">0pt</xsl:attribute>
		<xsl:attribute name="padding">2pt</xsl:attribute>
		<xsl:attribute name="border">1px solid #FFFFFF</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
		<!-- 
		<xsl:attribute name="background-color">#f0f7fc</xsl:attribute> -->
	</xsl:attribute-set>

	<xsl:attribute-set name="list-block">
		<xsl:attribute name="provisional-label-separation">3pt</xsl:attribute>
		<xsl:attribute name="provisional-distance-between-starts">18pt</xsl:attribute>
		<xsl:attribute name="margin-left">12pt</xsl:attribute>
		<xsl:attribute name="margin-top">6pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="list-item" />

	<xsl:attribute-set name="list-item-label">
		<xsl:attribute name="end-indent">label-end()</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="list-item-body">
		<xsl:attribute name="start-indent">body-start()</xsl:attribute>
	</xsl:attribute-set>

	<!--pour "sur-classer" les élément ead (pl-ead-...)-->
	<xsl:template name="extra-attributes">
		<!--DEBUG-->
			<!--<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>-->
			<xsl:if test="contains(@class, 'todo')">
				<xsl:attribute name="background-color">red</xsl:attribute>
			</xsl:if>
			<xsl:if test="contains(@class, 'fixme')">
				<xsl:attribute name="background-color">yellow</xsl:attribute>
			</xsl:if>
			<xsl:if test="contains(@class, 'pl-pgd-error')">
				<xsl:attribute name="background-color">red</xsl:attribute>
			</xsl:if>
		<!-- /DEBUG-->

		<xsl:if test="contains(@class, 'pl-ead-frontmatter')">
			<xsl:attribute name="border">1px solid red</xsl:attribute>
			<!--FIXME : faire une seule page pour le frontmatter-->
			<xsl:attribute name="margin-top">5cm</xsl:attribute>
			<xsl:attribute name="margin-bottom">5cm</xsl:attribute>
		</xsl:if>
		
		<xsl:if test="contains(@class, 'pl-ead-att-render-italic')">
			<xsl:attribute name="font-style">italic</xsl:attribute>
		</xsl:if>

		<xsl:if test="contains(@class, 'pl-pgd-titlepage-block') and xhtml:span[@class='pl-ead-edition']">
			<xsl:attribute name="right">2cm</xsl:attribute>
		</xsl:if>

	</xsl:template>

	<!--liste des attribute-sets de presentation générique (pl-pgd-...)-->

	<xsl:attribute-set name="pl-pgd-c" use-attribute-sets="block"/>

	<xsl:attribute-set name="pl-pgd-sect" use-attribute-sets="block"/>

	<xsl:attribute-set name="pl-pgd-list-head" use-attribute-sets="block" />

	<xsl:attribute-set name="pl-pgd-titlepage-block" use-attribute-sets="block" />

	<xsl:attribute-set name="pl-pgd-eadheader-subtitle" use-attribute-sets="table-row">
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="pl-pgd-table" use-attribute-sets="table" />

	<xsl:attribute-set name="pl-pgd-fv" use-attribute-sets="table" />
	
	<xsl:attribute-set name="did-label">
	</xsl:attribute-set>

  <!-- liste des attribute-sets spécifiques à certains exports PDF -->
  <xsl:attribute-set name="unit-context">
    <xsl:attribute name="margin-left">0.3cm</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>