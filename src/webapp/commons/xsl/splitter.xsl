<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Une XSLT utilisée pour découper un fichier XML en multiples fichiers XML
    |
    | On ne considère que les éléments fils de la racine documentaire. Ainsi
    | pour la structure :
    | <racine>
    |  <fils>[...]</fils>
    |  <fils>[...]</fils>
    | </racine>
    |
    | On aura autant de fichiers XML que d'élément "fils".
    | Le répertoire cible est construit à partir du paramètre "src" auquel on
    | accole un suffixe "-splitter". Si le paramètre "src" est vide, on utilise
    | le repertoire temporaire indiqué par la Machine Virtuelle Java (JVM).
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">

  <xsl:output indent="yes" method="xml"/>

  <xsl:param name="src" select="''" />
  <xsl:param name="target-dir" select="''" />
  <xsl:param name="include" select="''" />
  <xsl:param name="exclude" select="''" />

  <!-- Le séparateur du système courant -->
  <xsl:variable name="fileSep" select="sys:get-property('file.separator')" xmlns:sys="java.lang.System" />

  <!-- Le répertoire cible -->
  <xsl:variable name="dir">
    <xsl:choose>
      <xsl:when test="$target-dir!=''">
        <xsl:value-of select="$target-dir" />
      </xsl:when>
      <xsl:otherwise>
        <!--<xsl:value-of select="concat(sys:get-property('java.io.tmpdir'), $fileSep, 'splitter')" xmlns:sys="java.lang.System" />-->
        <xsl:value-of select="file:new(string(sys:get-property('java.io.tmpdir')),string('splitter'))"
            xmlns:sys="java:java.lang.System" xmlns:file="java:java.io.File" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Le protocole 'file'. On veut savoir si on met 2 ou trois slashes ('/') suivant la valeur de $target-dir -->
	<xsl:variable name="file-protocol">
		<xsl:text>file://</xsl:text>
		<xsl:if test="not(starts-with($dir, '/'))">
			<xsl:text>/</xsl:text>
		</xsl:if>
	</xsl:variable>

  <!--+
      |
      +-->
  <xsl:template match="/*">
    <!--<xsl:message>[SPLITTER] src="<xsl:value-of select="$src" />" fileSep="<xsl:value-of select="$fileSep" />" target-dir="<xsl:value-of select="$target-dir" />"</xsl:message>-->
    <splitter-report>
      <src><xsl:value-of select="$src" /></src>
      <target-dir><xsl:value-of select="$dir" /></target-dir>
      <file-seprator><xsl:value-of select="$fileSep" /></file-seprator>
      <include><xsl:value-of select="$include" /></include>
      <exclude><xsl:value-of select="$exclude" /></exclude>
      <xsl:apply-templates />
    </splitter-report>
  </xsl:template>

  <!--+
      | Un enregistrement
      +-->
  <xsl:template match="*">

    <xsl:variable name="split">
      <xsl:choose>
        <xsl:when test="$include='' and $exclude=''">
          <xsl:text>o</xsl:text>
        </xsl:when>
        <xsl:when test="$include!='' and contains( $include, concat(',',name(),',') )">
          <xsl:text>o</xsl:text>
        </xsl:when>
        <xsl:when test="$exclude!='' and not( contains( $exclude, concat(',',name(),',') ) )">
          <xsl:text>o</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>n</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:if test="$split='o'">

      <!--<xsl:variable name="href" select="concat($dir,$fileSep,generate-id(),'.xml')" />-->
      <!-- La construction de l'URL du document : $target-dir comme URL de base, un generate-id() comme nom de fichier, '.xml' comme extension -->
      <xsl:variable name="href" select="url:new( string( concat( $file-protocol, file:new( string($dir), string(concat(generate-id(), '.xml')) ) ) ) )"
          xmlns:file="java:java.io.File" xmlns:url="java:java.net.URL" />

      <result-document no="{position()}" href="{$href}" element-name="{name()}" />

      <xsl:result-document method="xml" href="{$href}">
        <xsl:copy-of select="." copy-namespaces="no" />
      </xsl:result-document>

    </xsl:if>

  </xsl:template>

</xsl:stylesheet>
