/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils.ant;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Tache ANT qui permet de convertir un chemin de fichier
 * en URL.
 * <p>Definir ainsi:</p>
 * <pre>&lt;taskdef name="url" classname="org.pleade.utils.ant.FileToUrl"/&gt;</pre>
 * <p>Utiliser ainsi:</p>
 * <pre>&lt;url property="url.sortie" file="${build.dir}/sortie"/&gt;</pre>
 */
public class FileToUrl extends Task {

    /** Le nom de la propriete qui sera definie. */
    private String propertyName = null;
    
    /** Le chemin fichier passe */
    private File file = null;
    
    /** Definit la propriete a remplir. */
    public void setProperty(String p) {
        this.propertyName = p;
    }
    
    /** Definit le fichier a convertir en URL. */
    public void setFile(File f) {
        this.file = f;
    }
    
    /** Execute la tache, donc remplit la propriete avec l'URl du fichier. */
    public void execute() throws BuildException {
        if ( this.propertyName == null ) throw new BuildException("L'attribut \"property\" est obligatoire");
        if ( this.file == null ) throw new BuildException("L'attribut \"file\" est obligtoire");
        try {
            getProject().setProperty(this.propertyName, this.file.toURL().toExternalForm());
        }
        catch (MalformedURLException e) {
            throw new BuildException(e);
        }
    }
}
