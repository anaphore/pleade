<?xml version="1.0" encoding="UTF-8"?>
<!-- @author : mdia -->
<!--
Pleade: Outil de publication pour instruments de recherche, notices
d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Paternité : Vous devez citer le nom de l'auteur original de la manière
indiquée par l'auteur de l'oeuvre ou le titulaire des droits qui vous
confère cette autorisation (mais pas d'une manière qui suggérerait
qu'ils vous soutiennent ou approuvent votre utilisation de l'oeuvre).
Pas d'Utilisation Commerciale : Vous n'avez pas le droit d'utiliser
cette création à des fins commerciales.
Pas de Modification : Vous n'avez pas le droit de modifier, de
transformer ou d'adapter cette création.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
    exclude-result-prefixes="xsl sdx" version="2.0">

  <xsl:template match="/">
    <sdxdoc>
      <xsl:apply-templates select="sdx:document/sdx:results"/>
    </sdxdoc>
  </xsl:template>

  <xsl:template match="sdx:results">
    <nbrresult><xsl:value-of select="count(.//sdx:result)"/></nbrresult>

    <xsl:for-each select="sdx:result">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="sdx:result">
    <result>
      <title><xsl:value-of select=".//sdx:field[@name='fatitle']"/></title>
      <pleadeId><xsl:value-of select=".//sdx:field[@name='root-id']" /></pleadeId>
    </result>
  </xsl:template>

</xsl:stylesheet>
