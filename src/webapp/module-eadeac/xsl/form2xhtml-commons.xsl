<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Construction de formulaire XHTML.
		|	Fonctions génériques
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:xsp-session="http://apache.org/xsp/session/2.0"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml xsp-session dir">

	<!--+
	    |	Attribution des valeurs par défaut d'un select
	    +-->
	<xsl:template name="build-select-options">

		<xsl:param name="n" />
		<!-- La liste des options pour construire le select -->
		<xsl:param name="ls" />
		<!-- Quelle est la valeur par défaut -->
		<xsl:param name="d" />

		<xsl:apply-templates select="$ls/option" mode="main">
			<xsl:with-param name="d" select="$d" />
		</xsl:apply-templates>

	</xsl:template>
	<xsl:template match="xhtml:option | option" mode="main">

		<xsl:param name="d" select="''" />

		<option>
			<xsl:apply-templates select="@*" mode="main" />
			<xsl:if test="$d!='' and current()/@value = $d">
				<xsl:attribute name="selected">
					<xsl:text>selected</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()" mode="main" />
		</option>

	</xsl:template>

	<!--+
	    | Construire les conteneurs de valeurs sélectionnés pour les suggests
	    +-->
	<xsl:template name="build-div-suggest-values">
		<xsl:param name="i" />
		<xsl:param name="d" />
		<xsl:param name="sep" />

<!--<xsl:message>[build-div-suggest-values]
  i = <xsl:value-of select="$i" />
  d = <xsl:value-of select="$d" />
sep = `<xsl:value-of select="$sep" />`
<a href="#" onclick="javascript: this.parentNode.dispose();"><xsl:value-of select="$c" /></a>
		
</xsl:message>-->

		<xsl:for-each select="tokenize($d, $sep)">
			<xsl:variable name="c" select="." />
			<xsl:if test="$c!=''">
				<div class="pl-form-suggest-value">
				<!--<a href="#" onclick="javascript: this.parentNode.dispose();"><xsl:value-of select="$c" /></a>-->
				<a href="#" onclick="$(this.parentNode).dispose();"><xsl:value-of select="$c" /></a>
				</div>
			</xsl:if>
		</xsl:for-each>

	</xsl:template>


	<!--+
	    |	Attribution de la valeur par défaut d'un input "checkbox" ou "radio"
	    +-->
	<xsl:template name="input-default-value">

		<xsl:param name="i" />
		<xsl:param name="d" />
		<xsl:param name="sep" />

<!--<xsl:message>
DEBUG
i=<xsl:value-of select="$i" />
d=<xsl:value-of select="$d" /></xsl:message>-->

		<xsl:if test="$d!=''">
			<xsl:choose>
				<xsl:when test="(current()/@type='checkbox' or current()/@type='radio')">
					<xsl:choose>
						<xsl:when test="contains($d, $sep)">
							<!-- Si on a plusieurs valeurs par défaut, il faut boucler -->
							<xsl:variable name="c" select="current()/@value" />
							<xsl:for-each select="tokenize($d, $sep)">
								<xsl:if test=".=$c">
									<xsl:attribute name="checked">
										<xsl:text>checked</xsl:text>
									</xsl:attribute>
								</xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="$d=current()/@value">
							<xsl:attribute name="checked">
								<xsl:text>checked</xsl:text>
							</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="value">
						<xsl:value-of select="$d" />
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="node()" mode="template" />

	</xsl:template>


	<!--+
			|	Construction de l'aide
			+-->
	<xsl:template name="build-help">
		<xsl:param name="v" select="''" />
		<xsl:param name="k" select="$v" />
		<xsl:param name="c" select="'module-eadeac-edit'"/>
		
		<xsl:if test="$v!=''">
			<!-- On construit le bouton d'aide et la bulle qui sera affichée -->
			<div class="pl-form-help">
				<a id="panel-help-{$v}-button" href="javascript:void(0);" i18n:attr="title"
						title="{$c}:{$k}.help.button.title">
					<img src="images/pl-form-help.png" i18n:attr="alt"
							alt="{$c}:{$k}.help.button.alt" />
				</a>
			</div>
			<div id="panel-help-{$v}" class="panel-help" style="display:none;">
				<div class="hd">
					<xsl:comment>u</xsl:comment>
					<i18n:text key="{$k}.help.panel.hd" catalogue="{$c}" />
				</div>
				<div class="bd">
					<xsl:comment>u</xsl:comment>
					<i18n:text key="{$k}.help.panel.bd" catalogue="{$c}" />
				</div>
				<!--<div class="ft">
					<xsl:comment>u</xsl:comment>
					<i18n:text key="{$k}.help.panel.ft" catalogue="{$c}" />
				</div>-->
			</div>
		</xsl:if>
	</xsl:template>

	<!--+
			|	Construction d'un tableau Javascript pour l'utilisation des autocomplete
			+-->
	<xsl:template match="index-list | fieldList" mode="JSArray">
		<xsl:for-each select="child::*">
			<xsl:text>["</xsl:text>
			<!--<xsl:value-of select="@id[parent::index] or @name[parent::field]" />-->
			<xsl:value-of select="self::index/@id | self::field/@name" />
			<xsl:text>","</xsl:text>
			<xsl:value-of select="@label" />
			<xsl:text>"]</xsl:text>
			<xsl:if test="position()!=last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
