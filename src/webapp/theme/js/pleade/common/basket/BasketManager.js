//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du porte document (ajout et suppression de document).
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion du porte document (ajout et suppression de document).
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var BasketManager = new Class(/** @lends BasketManager.prototype */ {

  /**
  * Le panier que l'on gère
  * @private
  * @type Basket
  */
  basket: null,

  /**
  * Affichage d'un message lorsque le document a été ajouté au panier
  * @private
  * @default false
  * @type boolean
  */
  msgOnAdd: false,

  /**
  * <strong>Constructor</strong><br/> Initialisation du manager
  * @param {Basket} basket
  */
  initialize: function(basket){
    this.basket = basket;
  },

  /**
  * Initialiser le panier du panier du manager
  * @param {Basket} basket
  */
  setBasket: function(basket){
    this.basket = basket;
  },

  /**
  * Fonction gerant l'ajout de documents dans le porte-documents
  * @param {String} params Les parametres qui seront passes avec l'URL.
  * @param {String} target L'objet HTML/DOM sur lequel on agira pour suivre le processus.
  * @param {String} base L'identifiant de la base de documents. (optionel).
  */
  addToBasket : function( params, target, base, disables ) {
    if ( !params || !target ) {
      return false;
    }
    if ( disables==null ) {
      disables = new Array();
      disables[0] = target;
    }
    var oInnerHTML = target.innerHTML;
    var url = 'functions/' + ( (!base) ? 'ead' : base) + '/addToBasket.json?';
    var req = new Request({
      url : url,
      onRequest: function(){
        var elt = new Element('span',{
          html : _usersMessagesBasket.basket_msg_adding
        });
        var targ = new Element(target);
        elt.addClass('access');
        elt.addClass('pl-bskt-bttn-loading');
        targ.adopt(elt);
        target.innerHTML = '<span class="access">' + _usersMessagesBasket.basket_msg_adding + '</span>';
        target.addClass('pl-bskt-bttn-loading');
      },
      onFailure: function(){
        target.innerHTML = '<span class="access">' + _usersMessagesBasket.basket_msg_addToBasket_ko + '</span>';
        target.removeClass('pl-bskt-bttn-loading');
      },
      onSuccess: function(xhr){ //FIXME : fonction a revoir pb dans l'ajout. Je n'arrive pas a recuperer la valeur success
        target.innerHTML = oInnerHTML;
        target.removeClass('pl-bskt-bttn-loading');
        var success=false;
        var msgCode = '';
        if(xhr){
          var oJson = JSON.decode(xhr);
          success = oJson.success;
          if ( oJson.msgCode ) {
            msgCode = oJson.msgCode;
          }
        }
        if ( success ) {
          if ( disables ) {
            disables.each(function(o){
              if ( o ) {
                o.disabled = true;
                o.addClass('disabled');
                if ( this.msgOnAdd ) {
                  msgCode = 'basket_msg_addToBasket_ok';
                }
              }
            });
          }
        } else {
          if ( !msgCode ) {
            msgCode = 'basket_msg_addToBasket_ko';
          }
        }
        this.cancel();
        var msg = '_usersMessagesBasket.' + msgCode;
        if ( msg ) {
          _msg = eval(msg);
          if ( _msg != undefined && _msg != null ) {
            alert(eval(msg));
          }
        }
      }
    });
    req.send(params);
  },

  /**
  * Fonction gerant l'ajout d'une page complete de documents dans le porte-documents.
  * @param {String} params Les parametres qui seront passes avec l'URL.
  * @param {String} target L'objet HTML/DOM sur lequel on agira pour suivre le processus.
  * @param {String} base L'identifiant de la base de documents. (optionel).
  */
  addPageToBasket: function(params, target, base){
    return this.addToBasket(params, target, base);
    // TODO (MP) : Trouver moyen de desactiver les boutons d'une page de resultat.
  },

  /**
  * Fonction gerant l'ajout d'un resultat de recherche dans le porte-documents.
  * @param {String} params Les parametres qui seront passes avec l'URL.
  * @param {String} target L'objet HTML/DOM sur lequel on agira pour suivre le processus.
  * @param {String} base L'identifiant de la base de documents. (optionel).
  */
  addResultsToBasket: function(params, target, base){
    return this.addToBasket(params, target, base);
    // TODO (MP) : Trouver moyen de desactiver les boutons d'un resultat.
  },

  /**
  * Fonction gerant l'ajout d'un document dans le porte-documents.
  * @param {String} params Les parametres qui seront passes avec l'URL.
  * @param {String} target L'objet HTML/DOM sur lequel on agira pour suivre le processus.
  * @param {String} base L'identifiant de la base de documents. (optionel).
  */
  addDocToBasket : function(params, target, base){
    //alert("module-eadeac-basket.js initHelp()");
    return this.addToBasket(params, target, base);
  },

  /**
  * Fonction pour retirer des documents du panier
  *
  * @param {String} mode Le mode choisi : 'all' ou l'ensemble des input sélectionnés
  * @param {Node} target L'objet XHTML/DOM a mettre a jour pour suivre l'evolution du processus.
  * @param {String} base L'identifiant de la base de document (optionel)
  * @param {function} fct  La fonction utilisee en fin de processus. Si null, on recharge la page.
  */
  deleteFromBasket: function( mode, target, base, fct ) {
    if ( !mode || !target ) return false;

    var _origHtml = target.innerHTML;

    var _params = "";
    if ( typeOf(mode) == 'string' && mode == 'all' ) {
      _params = 'id=all';
    } else {
      mode.each(function(o){
        _params += '&id=' + o.get('value');
      });
      _params = _params.substring(1,_params.length);
    }

    var _url = 'functions/' + ( (!base) ? 'ead' : base) + '/deleteFromBasket.json?';

    var _ajax = new Request.JSON({
      url: _url,
      onRequest: function(){
        target.innerHTML = _usersMessagesBasket.basket_msg_deleting;
      },
      onFailure: function(){
        target.innerHTML = _usersMessagesBasket.basket_msg_deleteFromBasket_ko;
      },
      onSuccess: function(responseJSON, responseText){
        target.innerHTML = _origHtml;
        var success = false;
        var msg = _usersMessagesBasket.basket_msg_deleteFromBasket_ko;
        if(responseText){
          var oJson = JSON.decode(responseText);
          success = oJson.success;
          msg = oJson.description;
        }
        if(msg) {
          alert(msg);
        }
        if(fct) {
          fct();
        } else if(mode == 'all') {
          //si mode vaut 'all', on a vidé le panier, on ne cherche pas à charger en Ajax
          window.location.reload();
        } else {
          var _url = String(window.location).replace('basket\.html', 'functions/' + base + '/getBasket.ajax');
          //var _url = String(window.location).gsub('basket\.html.*', 'functions/' + base + '/getBasket.ajax');
          basket.ajaxBasketLoad(_url, base);
        }
      }
    });
    _ajax.send(_params);
  },

  /**
  * Fonction de vidange complete du panier
  * @param {Node} target L'objet XHTML/DOM a mettre a jour pour suivrel'evolution du processus.
  * @param {String} base L'identifiant de la base de document (optionel)
  */
  flushBasket : function(target, base){
    try{
      refreshDataTB = function(){
        if(tbls!=null){
          try{
            if ( base && !base.blank() && tbls[tblId] ) {
              // on connait precisemment la dataTable que l'on veut rafraichir
              if ( this.basket ) {
                this.basket.refreshDataTable(tlbs['pl-bskt-cnt-' + base]);
              }
            } else if ( selectedBases != null && selectedBases.length > 0 ) {
              // on connait precisemment la/les dataTable(s) que l'on veut rafraichir
              var dt=null;
              $A(selectedBases).each(function(o, i){
                dt = tbls['pl-bskt-cnt-' + o];
                if ( dt != null && dt.sortable != null ) {
                  if ( this.basket ) {
                    this.basket.tblId = 'pl-bskt-cnt-' + o;
                    this.basket.bbase = o;
                    this.basket.refreshDataTable(dt);
                  }
                }
              });
            } else {
              window.location.reload();
            }
          } catch(e){
            window.location.reload();
          }
        } else {
          window.location.reload();
        }
      };

      this.deleteFromBasket('id=all', target, base, refreshDataTB);
    } catch(e){}
  },

  saveBasket: function(_name, base){
    // FIXME: gestion de la base...
    //var _url = 'functions/' + ( (!base) ? 'ead' : base) + '/stored/basket/add.json?';
    var _url = 'functions/stored/basket/add.json?';
    var _params = 'basket_name=' + _name;

    var _ajax = new Request.JSON({
      url: _url,
      onSuccess: function(responseJSON, responseText){
        /* TODO */
      }
    });
    _ajax.send(_params);

  }
});
