<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: getBasket2xml.xsl 19866 2011-01-06 10:24:52Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Retourne les documents EAD contenus dans le porte-documents en XML
    | On peut alimenter une DataTable YUI avec.
    |
    | On ne prend en compte qu'une seule base de documents. A charge de
    | l'appelant de sélectionner la bonne base. Pour ce faire, il peut parser
    | sdx:caddy et compter le nombre de base différentes.
    +-->
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:xsp="http://apache.org/xsp"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  exclude-result-prefixes="xsl xsp sdx pleade fn">

  <!-- Fonctions d'utilités générales -->
  <xsl:import href="../../commons/xsl/functions.xsl"/>

  <xsl:param name="sort_field" select="''"/>
  <xsl:param name="sort_order" select="''"/>

  <xsl:param name="base" select="'ead'" />
  <xsl:variable name="_base">
    <xsl:choose>
      <xsl:when test="$base!=''"><xsl:value-of select="$base" /></xsl:when>
      <xsl:otherwise><xsl:text>ead</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!--+
      | Création de la racine XHTML
      +-->
  <xsl:template match="/sdx:document">
    <resultSet>
      <xsl:apply-templates select="sdx:results" mode="resume" />
      <xsl:apply-templates select="sdx:results" mode="results" />
    </resultSet>
  </xsl:template>

  <!--+
      | Le résumé de la recherche avec son résultat
      +-->
  <xsl:template match="sdx:results" mode="resume">

    <xsl:attribute name="returned">
      <xsl:value-of select="( number(@end) - number(@start) ) + 1"/>
    </xsl:attribute>
    <xsl:attribute name="total">
      <xsl:value-of select="@nb"/>
    </xsl:attribute>
    <xsl:attribute name="start">
      <xsl:value-of select="@start"/>
    </xsl:attribute>
    <xsl:attribute name="sort">
      <xsl:choose>
        <xsl:when test="$sort_field != ''">
          <xsl:value-of select="$sort_field"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="sdx:sort/sdx:field[1]/@name"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="dir">
      <xsl:choose>
        <xsl:when test="$sort_order != ''">
          <xsl:value-of select="$sort_order"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="sdx:sort/sdx:field[1]/@order"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <!--+
      | Affichage des résultats de recherche
      +-->
  <xsl:template match="sdx:results" mode="results">
    <xsl:apply-templates select="/sdx:document/sdx:caddy" />
  </xsl:template>

  <!--+
      | Affichage d'un ensemble de documents appartenant à la même base
      | Ici, on construit une TABLE qui sera formatée ensuite par le module
      | Datatable de YUI
      +-->
  <xsl:template match="sdx:caddy">

    <!-- Au lieu de boucler sur les sdx:result directement, on boucle sur
    les sdx:document du panier. Cela permet de trier les docs par ordre
    d'entrée dans le panier -->
    <xsl:for-each select="sdx:document[@base=$_base or not(@base) or @base='']">
      <xsl:sort select="@time" data-type="number" order="descending" /><!-- Tri sur la date d'arrivée du doc dans le panier -->
      <xsl:variable name="id" select="@id" />
      <xsl:apply-templates select="/sdx:document/sdx:results/sdx:result[sdx:field[@name='sdxdocid']/@value=$id]" mode="result">
        <!-- Une fois trier par ordre d'arrivée dans le panier, on affine le tri suivant le titre, la date, la cote -->
        <xsl:sort select="sdx:field[@name='preferredname'][1]" />
        <xsl:sort select="sdx:field[@name='udate'][1]" />
        <xsl:sort select="sdx:field[@name='unitid'][1]" />
      </xsl:apply-templates>
    </xsl:for-each>

  </xsl:template>

  <!--+
      | Construction d'une ligne de résultat pour les documents EAD
      +-->
  <xsl:template match="sdx:result[sdx:field[@name='sdxdbid']/@value=$_base]" mode="result">
    <result>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'sdxdocid'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'preferredname'" />
      </xsl:apply-templates>
      <!--<xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'udate'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'uunitid'" />
      </xsl:apply-templates>-->
    </result>
  </xsl:template>

  <!--+
      | Construction d'une cellule de résultat
      +-->
  <xsl:template match="sdx:result" mode="build-cell">
    <xsl:param name="n" select="''" />
    <xsl:choose>
      <xsl:when test="$n=''"/>
      <xsl:when test="$n='preferredname'">
        <xsl:variable name="href">
          <xsl:value-of select="concat( 'eac.html?id=', sdx:field[@name='root-id']/@escapedValue )" />
          <xsl:if test="not( sdx:field[@name='top' and @value='1'] )">
            <xsl:value-of select="concat( '&amp;c=', sdx:field[@name='sdxdocid']/@escapedValue )" />
          </xsl:if>
        </xsl:variable>
        <preferredname>
          <xsl:variable name="docid" select="sdx:field[@name='sdxdocid']/@value" />
          <xsl:if test="sdx:field[@name='ctitle' and @value!='']">
            <a class="pl-ancestors" id="{$docid}l" onclick="$('{$docid}c').toggle();" style="display:inline;">[...]</a>
            <xsl:text> </xsl:text>
          </xsl:if>
          <a href="{$href}" onclick="return windowManager.winFocus(this.href, 'doceac');">
            <xsl:choose>
              <xsl:when test="sdx:field[@name=$n and .!='']">
                <xsl:apply-templates select="sdx:field[@name=$n]" />
              </xsl:when>
              <xsl:otherwise>
                <i18n:text key="basket.results.item.unititle.default">document sans titre</i18n:text>
              </xsl:otherwise>
            </xsl:choose>
          </a>
          <xsl:if test="sdx:field[@name='ctitle' and @value!='']">
            <xsl:text> </xsl:text>
            <div class="pl-ancestors" id="{$docid}c" onclick="$('{$docid}c').toggle();" style="display:none;">
              <xsl:for-each select="sdx:field[@name='ctitle']">
                <xsl:sort select="@value" order="ascending" />
                <xsl:value-of select="substring-after( @value, '-' )" />
                <xsl:if test="position()!=last()">
                  <i18n:text key="basket.results.item.context.sep">  &gt;  </i18n:text>
                </xsl:if>
              </xsl:for-each>
            </div>
          </xsl:if>
        </preferredname>
      </xsl:when>
      <!-- Un cas particulier quand on veut la liste des ancêtres -->
      <xsl:when test="$n='ancestors'">
        <ancestors>
          <xsl:choose>
            <xsl:when test="sdx:field[@name='ctitle' and @value!='']">
              <xsl:variable name="docid" select="sdx:field[@name='sdxdocid']/@value" />
              <div id="{$docid}l" onclick="$('{$docid}l').hide; $('{$docid}c').show;" style="display:block;">[...]</div>
              <div id="{$docid}c" onclick="$('{$docid}c').hide; $('{$docid}l').show;" style="display:none;">
                <xsl:for-each select="sdx:field[@name='ctitle']">
                  <xsl:sort select="@value" order="ascending" />
                  <xsl:value-of select="substring-after( @value, '-' )" />
                  <xsl:if test="position()!=last()">
                    <i18n:text key="basket.results.item.context.sep">  &gt;  </i18n:text>
                  </xsl:if>
                </xsl:for-each>
              </div>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </ancestors>
      </xsl:when>
      <xsl:when test="$n='sdxdocid'">
        <sdxdocid>
          <input type="checkbox" name="id" value="{sdx:field[@name=$n]/@escapedValue}" />
        </sdxdocid>
      </xsl:when>
      <!-- Cas générique, on prend la valeur du sdx:field demandé -->
      <xsl:otherwise>
        <xsl:element name="{$n}">
          <xsl:if test="sdx:field[@name=$n and .!='']">
            <xsl:apply-templates select="sdx:field[@name=$n]" />
          </xsl:if>
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
