//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer le treeView du formulaire de recherche avancé
 * @author Julien Huet
 */
/**
 * @class Classe permettant de créer le treeView du formulaire de recherche avancé
 * @augments TreeView
 * @author Julien Huet
 */
var SearchCdc = new Class(/** @lends SearchCdc */{
	Extends: TreeView,
	
	/**
	* <strong>Constructor</strong><br/> 
	* @author jhuet
	* @param {SearchForm} searchForm
	*/
	initialize: function(searchForm){
		this.searchForm = searchForm;	
		var url = 'functions/ead/cdc-searchform.ajax-html' + this.getCdcIdsToLoad(true);
		this.parent(url, $('_cdc_'), null, null);
		this.getTree().initCheckbox('simple');
		this.getTree().getChildrenUrl = function(node){
			return node.getDataHref();
		}
	},

	/**
	* Récupère les identifiants cachés du cadre de clasement
	* en cas de modification de la recherche
	*/
	getCdcIdsToLoad : function(first) {
		// On enlève le ?
		var url_params = window.location.search.slice(1,window.location.search.length);
		// On sépare le paramètres....
		var params = url_params.split('&');
	
		var _ids = new Array();
		for ( i=0 ; i < params.length ; i++ ) {
			param = params[i].split('=');
			if(param[0] == '_cdcid') {
				_ids[_ids.length] = param[1];
			}
		}
	
		var result = '';
		if ( _ids.length > 0) {
			result = (first) ? '?' : '&';
			result += '_cdcids=' + _ids.join(';');
		}
		return result;
	},
	
	/**
	* Retourne la liste des entrées du cadre de classement qui ont été cochées
	*/
	getCdcIdsChecked : function() {
	  return this.getTree().getChecked();
	},
	
	/**
	* Surcharge de la fonction appelée lorsque l'on clique sur un noeud de l'arbre
	* Ici on annule le css de selection et on coche ou décoche les checkbox de l'arbre
	* @param {Mif.Tree.Node} node
	*/
	onSelect: function(node){
		node['switch']();
		var wrapper=node.getDOM('wrapper');
		wrapper.removeClass('mif-tree-node-selected');
	}
});