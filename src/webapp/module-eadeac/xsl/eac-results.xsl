<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: eac-results.xsl 18152 2010-03-04 13:10:30Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT qui affiche le résultat d'une recherche dans la base EAC
		+-->
<xsl:stylesheet 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns="http://www.w3.org/1999/xhtml" 
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" 
		xmlns:xsp="http://apache.org/xsp" 
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1" 
		xmlns:pleade="http://pleade.org/ns/pleade/1.0" 
		xmlns:fn="http://www.w3.org/2005/xpath-functions" 
		exclude-result-prefixes="xsl xsp sdx i18n pleade" version="2.0">

	<!-- On importe le contexte d'un résultat de requête : nbre de résultats, formulation de la requête -->
	<xsl:import href="cocoon://cdc-results.xsl"/>

	<xsl:import href="navigation.xsl" />

	<!-- Mode pour les résultats : par exemple suggest -->
	<xsl:param name="mode" select="''"/>

	<!--id du div dans lequel doivent apparaître les résultats-->
	<xsl:param name="divID" select="'eac-results'"/>

  <!-- TODO : Activation du bouton pour ajouter au panier -->
	<xsl:param name="active-basket" select="'false'" />

    <!--+
        | Match la racine
        +-->
	<xsl:template match="/">
		<!-- On traite un résultat de recherche normal. -->
		<xsl:apply-templates select="sdx:document"/>
	</xsl:template>

  <!--+
      | Construction de la racine XHTML
      +-->
	<xsl:template match="/sdx:document">
	<!--<xsl:message>[eac-result.xsl] n-start = <xsl:value-of select="$n-start"/></xsl:message>-->
	<!--<xsl:message>[eac-result.xsl] sep ="<xsl:value-of select="$list-sep" />"</xsl:message>-->
		<html>
			<head>
				<title>
					<i18n:text key="results-title.page.{$form-id}">results-title.page.<xsl:value-of select="$form-id" />
					</i18n:text>
				</title>

				<xsl:if test="$active-basket='true'">
					<script type="text/javascript" src="i18n/module-eadeac-basket-js.js">//</script>
					<script type="text/javascript" src="js/pleade/common/basket/BasketEntreprise.js">//</script>
				</xsl:if>
				<script type="text/javascript" src="i18n/module-eadeac-js.js">//</script>
				<script type="text/javascript">
					<xsl:comment>
						<xsl:if test="sdx:parameters/sdx:parameter[@name = 'linkBack']/@value = 'true'">
							<xsl:text>
								function back2form(){
									var re = new RegExp("name=([^&amp;]+)");
									var m = re.exec(location.href);
									if (m == null || m[1] == null || m[1] == '' ) {
										//pas de nom ici, on remplace simplement 'results.html' par 'search-form.html'
										location.href = location.href.gsub("results.", "search-form." );
									} else { // on a un nom, on l'utilise !
										var reurl = new RegExp("[^/]*results\.html", "g");
										location.href = location.href.replace(reurl, m[1] + '-search-form.html');
									}
								}
							</xsl:text>
						</xsl:if>
						//</xsl:comment>
				</script>
			</head>

			<body>
				<xsl:apply-templates select="." mode="pl-title" />

				<i18n:text key="results-intro.query"/>

        <!--affichage des critères de recherche-->
				<xsl:apply-templates select="sdx:parameters" mode="criteria"/>

        <!--affichage des résultats-->
				<xsl:apply-templates select="sdx:results | message" />

				<xsl:if test="sdx:results/sdx:result">
					<div id="{$real-div-id}">
						<ul class="pl-list-results">
							<xsl:apply-templates select="sdx:results/sdx:result" mode="list-result">
								<xsl:sort select="if (not(exists(sdx:parameters/sdx:parameter[@name='sf' and @value!=''])) )
																				then sdx:field[@name='fatitle'][1]
																				else ()"/>
								<xsl:sort select="sdx:field[@name='pos'][1]"/>
							</xsl:apply-templates>
						</ul>
					</div>
				</xsl:if>

				<!-- On prépare l'appel qui laisse une trace dans les logs de statistiques -->
				<xsl:variable name="bases">
					<xsl:for-each select="/sdx:document/sdx:parameters/sdx:parameter[@name='base' and @value!='']">
						<xsl:value-of select="concat( if(position()=1) then '?' else '&amp;','base=', @value )" />
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="stats-url" select="concat('cocoon:/stats.xml', $bases, '&amp;name=', /sdx:document/sdx:parameters/sdx:parameter[@name='name']/@value, '&amp;nb-crit=', /sdx:document/criteres/@nb, '&amp;nb-docs=', /sdx:document/sdx:terms[@name = 'root-id']/@nb, '&amp;nb-units=', /sdx:document/sdx:results/@nb, '&amp;query=')"/>
				<div id="pl-results-stats-info">
					<xsl:value-of select="$stats-url"/>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- Affichage de la barre de navigation -->
	<xsl:template match="sdx:results">
		<!-- La clé i18n selon singulier / pluriel -->
		<xsl:variable name="key-suffix">
				<xsl:choose>
						<xsl:when test="@nb = 1">one</xsl:when>
						<xsl:otherwise>many</xsl:otherwise>
				</xsl:choose>
		</xsl:variable>

		<!-- Le panneau de navigation dans les résultats -->
		<div class="navigation-results">
			<p>
				<span class="nbresults">
					<xsl:value-of select="@nb"/>
					<xsl:text> </xsl:text>
					<i18n:text key="docsearch.results.{$key-suffix}"/>
				</span>
				<span class="nbpages">
					<i18n:text key="docsearch.page"/>
					<xsl:text> </xsl:text>
					<strong><xsl:value-of select="@page"/></strong>
					<xsl:text> </xsl:text><i18n:text key="docsearch.page.of"/><xsl:text> </xsl:text>
					<xsl:value-of select="@pages"/>
				</span>
				<xsl:if test="@nbPages > 1">
					<span class="navpage">
						<xsl:apply-templates select="." mode="hpp">
							<xsl:with-param name="query" select="concat('&amp;',substring-before(translate(parent::sdx:document/@query, '?', ''), '&amp;hpp='))"/>
						</xsl:apply-templates>
					</span>
				</xsl:if>
				<xsl:if test="$active-basket='true'">
					<xsl:variable name="docids">
						<xsl:for-each select="sdx:result">
							<xsl:value-of select="concat('id=', sdx:field[@name='sdxdocid']/@value)" />
							<!-- INFO (MP) : On n'envoie pas l'identifiant "URL
							encoded" car cela peut poser des problemes dans le cas ou
							cet identifiant possede des espaces et autre caractere
							interdit en URL.
							<xsl:value-of select="concat('id=', sdx:field[@name='sdxdocid']/@escapedValue)" />-->
							<xsl:if test="position()!=last()"><xsl:text>&#38;</xsl:text></xsl:if>
						</xsl:for-each>
					</xsl:variable>
					<span class="bskt-bttn">
						<button type="button" title="basket.button.add-to-basket.page.title"
										i18n:attr="title" class="pl-bskt-bttn-page"
										onclick="addPageToBasket('{$docids}', this);"
										onkeypress="addPageToBasket('{$docids}', this);"
						>
							<span class="access"><i18n:text key="basket.button.add-to-basket.page.text">
								ajouter cette page
							</i18n:text></span>
						</button>
						<button type="button" title="basket.button.add-to-basket.results.title"
										i18n:attr="title" class="pl-bskt-bttn-results"
										onclick="addResultsToBasket('qid={@qid}', this);"
										onkeypress="addResultsToBasket('qid={@qid}', this);"
						>
							<span class="access">
								<i18n:text key="basket.button.add-to-basket.results.text">
									ajouter tous les résultats
								</i18n:text>
							</span>
						</button>
					</span>
				</xsl:if>
			</p>
		</div>
	</xsl:template>

	<!--+
      | Construction d'un résultat
      +-->
	<xsl:template match="sdx:result" mode="list-result">
		<li>
			<xsl:apply-templates select="sdx:field[@name='sdxdocid']" mode="result-component"/>
			<xsl:apply-templates select="sdx:field[@name='preferredname']" mode="result-component"/>
			<xsl:apply-templates select="sdx:field[@name='udate']" mode="result-component"/>
			<!-- <xsl:apply-templates select="sdx:field[@name='uunitid']" mode="result-component"/> -->
		</li>
	</xsl:template>

	<!--+
      | Construction des composants dans un résultat
      +-->
	<xsl:template match="sdx:field[@name='sdxdocid']" mode="result-component">
	 <xsl:if test="$active-basket='true'">
		<xsl:variable name="id2add" select="@value"/>
		<!-- bouton d'ajout au porte-docs -->
		<button class="pl-bskt-bttn-doc"
						title="module-eadeac:basket.button.add-to-basket.doc.title"
						i18n:attr="title" id="bskt-add-{$id2add}"
						onclick="basket.basketManager.addDocToBasket('id={$id2add}', this, 'eac');"
						onkeypress="basket.basketManager.addDocToBasket('id={$id2add}', this, 'eac');">
			<xsl:if test="not( $id2add!='' )">
				<xsl:attribute name="disabled"><xsl:text>disabled</xsl:text></xsl:attribute>
				<xsl:attribute name="class">
					<xsl:text>pl-bskt-bttn-doc disabled</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<span class="access"><i18n:text key="module-eadeac:basket.button.add-to-basket.doc.text">ajouter au porte-documents</i18n:text></span>
		</button>
	 </xsl:if>
	</xsl:template>
	<xsl:template match="sdx:field[@name='preferredname']" mode="result-component">
		<span class="cdc-title">
			<xsl:variable name="href">
				<xsl:value-of select="concat( 'eac.html?id=', ../sdx:field[@name='root-id']/@escapedValue )" />
			</xsl:variable>
			<xsl:variable name="docid" select="sdx:field[@name='sdxdocid']/@value" />
			<a href="{$href}" onclick="return windowManager.winFocus(this.href, 'docead');">
				<xsl:choose>
					<xsl:when test=".!=''">
						<xsl:apply-templates />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Document sans titre</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</a>
		</span>
	</xsl:template>
	<xsl:template match="sdx:field[@name='udate']" mode="result-component">
		<xsl:value-of select="$list-sep"/>
		<span class="cdc-unitdate"><xsl:apply-templates /></span>
	</xsl:template>
	<xsl:template match="sdx:field[@name='uunitid']" mode="result-component">
		<xsl:value-of select="$list-sep"/>
		<span class="cdc-unitid"><xsl:apply-templates /></span>
	</xsl:template>
	<xsl:template match="node()" mode="result-component">
	</xsl:template>

</xsl:stylesheet>
