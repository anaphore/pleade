<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT gérant l'affichage des informations utilisateurs dans la page XHTML
		finale

		Elle est importée par "pleade-aggregate.xsl" dirigeant l'agrégation des
		parties statiques et dynamiques.

		Cette XSLT travaille un XML :

		<userinfos>

		    <authentication>
         <ID>admin</ID>
         <roles>admin,eadeac-editor</roles>

         <data>
            <name>Administrateur</name>
            <email>admin@pleade.org</email>
         </data>
      </authentication>

	  </userinfos>

-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:xsp="http://apache.org/xsp"
	xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsp-session="http://apache.org/xsp/session/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xsl xsp sdx i18n xhtml xsp-session">

	<!-- Infos utilisateur dans le fichier de configuration -->
	<xsl:variable name="userinfos" select="/aggregate/userinfos" />

	<!--	Interception de l'élément XHTML déclenchant l'affichage des infos
				utilisateur dans le modèle -->
	<xsl:template match="xhtml:*[@id='userinfos']" mode="template">

		<xsl:if test="$userinfos/authentication or
									( $show-pleade-zones='true' and ($userinfos/@pleade-zones!='' or $userinfos/@pleade-zone!='') )">

			<xsl:copy>
				<xsl:apply-templates select="@*" mode="template" />
				<xsl:apply-templates select="$userinfos/authentication" mode="main" />
				<xsl:apply-templates select="$userinfos/@pleade-zones | $userinfos/@pleade-zone" mode="main" />
			</xsl:copy>

		</xsl:if>

	</xsl:template>


	<xsl:template match="@pleade-zones" mode="main">
		<xsl:if test="$show-pleade-zones='true'">
			<table class="userinfos pleade-zones" align="left">
				<tbody>
					<tr>
						<td class="{local-name()}">
							<i18n:text key="userinfos.zones" catalogue="pleade">Zones&#160;:&#32;</i18n:text>
							<xsl:value-of select="."/>
						</td>
					</tr>
				</tbody>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="@pleade-zone" mode="main">
		<xsl:if test="$show-pleade-zones='true' and not($userinfos/@pleade-zone!='')">
			<table class="userinfos pleade-zone" align="right">
				<tbody>
					<tr>
						<td class="{local-name()}">
							<i18n:text key="userinfos.zone" catalogue="pleade">Zone&#160;:&#32;</i18n:text>
							<xsl:value-of select="."/>
						</td>
					</tr>
				</tbody>
			</table>
		</xsl:if>
	</xsl:template>

  <!--+
      | Le bloc d'identification
      +-->
	<xsl:template match="authentication" mode="main">
    <xsl:variable name="onclick">
      <xsl:text>window.location.href='logout.html';</xsl:text>
    </xsl:variable>
		<table class="userinfos" align="right">
			<tbody>
				<xsl:if test="data/name or data/email or roles!=''">
				<tr>
					<xsl:apply-templates select="data/name" mode="main" />
					<xsl:apply-templates select="roles" mode="main" />
					<td>
            <!-- NOTE (MP) :  Le bouton est construit tout de suite avec la structure XHTML attendue par YUI car pleade-aggregate.xsl ne passera pas sur ce bloc XHTML.  -->
            <span id="bt-bttm-lgt" class="yui-button yui-push-button">
							<span class="first-child">
                <button title="pleade:logout.button.title" i18n:attr="title"
                       onclick="{$onclick}" onkeypress="{$onclick}">
                  <i18n:text key="logout.button.text" catalogue="pleade">
                    déconnexion
                  </i18n:text>
                </button>
              </span>
            </span>
					</td>
				</tr>
				</xsl:if>
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="name" mode="main">
		<td class="{local-name()}">
			<xsl:choose>
				<xsl:when test="../email!=''">
					<a href="mailto:{normalize-space(../email[1])}">
						<xsl:apply-templates select="parent::data" mode="display" />
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="parent::data" mode="display" />
				</xsl:otherwise>
			</xsl:choose>
		</td>
	</xsl:template>
	<xsl:template match="email" mode="main">
		<td class="{local-name()}">
			<a href="mailto:{normalize-space(.)}">
				<xsl:apply-templates />
			</a>
		</td>
	</xsl:template>
	<xsl:template match="roles" mode="main">
		<td class="{local-name()}">
			<i18n:text key="userinfos.roles" catalogue="pleade">Rôles&#160;:&#32;</i18n:text>
			<xsl:for-each select="tokenize(normalize-space(.), ',')">
				<xsl:if test="position() &gt; 1">
					<xsl:text>,&#32;</xsl:text>
				</xsl:if>
				<xsl:value-of select="." />
			</xsl:for-each>
		</td>
	</xsl:template>
	<xsl:template match="data" mode="display">
		<xsl:apply-templates select="civility" mode="display" />
		<xsl:apply-templates select="surname" mode="display" />
		<xsl:apply-templates select="name" mode="display" />
		<xsl:apply-templates select="number" mode="display" />
	</xsl:template>
	<xsl:template match="civility | surname" mode="display">
		<span class="{local-name()}">
			<xsl:apply-templates />
		</span>
		<xsl:text>&#32;</xsl:text>
	</xsl:template>
	<xsl:template match="name" mode="display">
		<span class="{local-name()}">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	<xsl:template match="number" mode="display">
		<xsl:text>&#32;(</xsl:text>
		<i18n:text key="userinfos.number" catalogue="pleade">n°&#160;:&#32;</i18n:text>
		<xsl:apply-templates />
		<xsl:text>)</xsl:text>
	</xsl:template>

</xsl:stylesheet>
