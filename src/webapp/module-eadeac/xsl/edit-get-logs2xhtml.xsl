<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Administration du module EAD-EAC
    | Exporter les logs de publication en XHTML
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:dir="http://apache.org/cocoon/directory/2.0"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		exclude-result-prefixes="xsl xsp sdx i18n xhtml dir xs">

  <xsl:param name="type" select="''" />
  <xsl:variable name="_type">
    <xsl:choose>
      <xsl:when test="$type!=''"><xsl:value-of select="$type" /></xsl:when>
      <xsl:otherwise><xsl:text>pleade-stats</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="i18n-root" select="concat('ead-admin.export-',$_type)" />
  <xsl:variable name="i18n-cat" select="string('module-eadeac-edit')" />

  <!-- Imports -->
	<xsl:include href="form2xhtml-commons.xsl"/>

  <!-- Un format pour les tailles des fichiers de logs -->
  <xsl:decimal-format name="size" decimal-separator="," grouping-separator=" "/>

	<!-- Constructions des prefixes utilisés.
	Renvoie quelque chose comme "cocoon,log4j,publication,sdx".
	On prend la liste des fichiers (dir:directory/dir:file) et on boucle sur chacun
	pour récuperer la liste des types de logs. Donc pour 2 fichiers "cocoon-20090701.log", "cocoon-20090702.log",
	on récupère "cocoon". Pour un unique fichier "log4j.log", on récupère "log4j".-->
	<xsl:variable name="prefixes">
		<xsl:for-each select="/dir:directory/dir:file">
			<xsl:variable name="name" select="
				if ( matches(@name, '\-[0-9]+\.[a-z]+$') )
					then replace(@name, '\-[0-9]+\.[a-z]+$', '')
					else replace(@name, '\.[a-z]+', '')
			" />
			<xsl:choose>
			  <xsl:when test="position()=1"><xsl:value-of select="$name" /></xsl:when>
				<xsl:when test="not(preceding-sibling::dir:file) or not(starts-with(preceding-sibling::dir:file[1]/@name, $name))"><xsl:value-of select="concat(',',$name)" /></xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:variable>

	<!-- Un marqueur indiquant l'utilisation ou non d'onglets -->
	<xsl:variable name="isTabs" as="xs:boolean" select="
		if ( count( tokenize($prefixes, ',') ) =  1 ) then false()
		else true()
	" />

  <!--+
      | Construction de la racine XHTML
      +-->
	<xsl:template match="/">

		<html>

			<head>
				<title>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.html.title">
            statistiques
          </i18n:text>
				</title>
				<xsl:if test="$isTabs">
        <xsl:comment>yui</xsl:comment>
        <script type="text/javascript" src="yui/tabview/tabview-min.js">//</script>
				<link type="text/css" href="yui/assets/skins/pleade/tabview.css" rel="stylesheet" />
				</xsl:if>
				<xsl:comment>pleade</xsl:comment>
        <script type="text/javascript" src="i18n/module-eadeac-edit-js.js">//</script>
		<script type="text/javascript" src="js/pleade/admin/eadeac/Stats.js">//</script>
       <!-- <script type="text/javascript" src="js/module-eadeac.js">//</script>
        <script type="text/javascript" src="js/module-eadeac-edit.js">//</script>-->
        <script type="text/javascript">
		  var stats = '';
		  var b_tabs = false;
		  <xsl:if test="$isTabs">
			b_tabs = true;
		  </xsl:if>
          window.addEvent("load", init);
		  function init(){
			stats = new Stats(b_tabs)
		  }
		  
		
        </script>
			</head>

			<body>

        <!-- Le titre de la page -->
        <div class="pl-title">
          <xsl:call-template name="build-help">
            <xsl:with-param name="v" select="'title'" />
            <xsl:with-param name="k" select="concat($i18n-root, '.title')" />
						<xsl:with-param name="c" select="$i18n-cat"/>
          </xsl:call-template>
          <h1>
            <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.pl-title">
              statistiques
            </i18n:text>
          </h1>
        </div>

				<div class="pl-form-intro pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.intro" />
				</div>

        <!-- Le contenu dynamique -->
        <xsl:call-template name="tabs" />

				<div class="pl-form-conclu pl-form-message">
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.conclusion" />
				</div>

			</body>

		</html>

	</xsl:template>

  <!--+
      | Le contenu dynamique avec onglet
      +-->
  <xsl:template name="tabs">

    <div id="tabs" class="yui-navset pl-pgd-tabs">

      <xsl:call-template name="tabs-buttons" />

      <xsl:call-template name="tabs-content" />

    </div>

  </xsl:template>

  <!--+
      | Les boutons des onglets
      |
      | Pour l'instant un seul "recherche", mais à l'avenir on en veut d'autres
      +-->
  <xsl:template name="tabs-buttons">

		<xsl:if test="$isTabs">

		<ul id="pl-tabs" class="yui-nav">

			<xsl:for-each select="tokenize($prefixes,',')">
				<xsl:variable name="prefix" select="normalize-space(.)" />
				<li class="{if(position()=1) then 'selected' else ''}">
					<a href="#{$prefix}">
						<em>
							<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.tabs.{$prefix}">
								<xsl:value-of select="$prefix" />
							</i18n:text>
						</em>
					</a>
				</li>
			</xsl:for-each>

		</ul>

		</xsl:if>

  </xsl:template>

  <!--+
      | Le contenu des onglets
      +-->
  <xsl:template name="tabs-content">

		<xsl:variable name="dir" select="dir:directory" />

		<div id="pl-box" class="yui-content">

			<xsl:for-each select="tokenize($prefixes,',')">
				<xsl:variable name="prefix" select="normalize-space(.)" />

				<div id="{$prefix}" class="tab">

					<xsl:call-template name="build-help">
						<xsl:with-param name="v" select="concat('logs-',$prefix)" />
						<xsl:with-param name="k" select="concat($i18n-root, '.logs-list')" />
						<xsl:with-param name="c" select="$i18n-cat"/>
					</xsl:call-template>

					<fieldset>

						<legend>
							<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.logs-list.legend">
								<xsl:text>liste des fichiers téléchargeables</xsl:text>
							</i18n:text>
						</legend>

						<!-- La liste des fichiers téléchargeables -->
						<xsl:apply-templates select="$dir" mode="tbl-html">
							<xsl:with-param name="prefix" select="$prefix" />
						</xsl:apply-templates>

					</fieldset>

				</div>

			</xsl:for-each>

    </div>

  </xsl:template>

  <!--+
      | Le tableau contenant la liste des fichiers téléchargeables
      +-->
  <xsl:template match="dir:directory" mode="tbl-html">

		<xsl:param name="prefix" select="''" />

    <xsl:choose>

      <xsl:when test="not(dir:file[starts-with(@name,$prefix)])">
        <p class="description">
          <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.no-log">
            il n'existe pas de fichier récupérable pour le moment
          </i18n:text>
        </p>
      </xsl:when>

      <xsl:otherwise>

        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="{$prefix}"
              class="pl-rslts" summary="La liste des fichiers de logs récupérables">

          <thead>
            <th class="label pl-tbl-th pl-tbl-stts-nm" scope="col">
              <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.col-title.name">nom</i18n:text>
            </th>
            <th class="label pl-tbl-th pl-tbl-stts-sz" scope="col">
              <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.col-title.size">taille</i18n:text>
            </th>
            <th class="label pl-tbl-th pl-tbl-stts-gt" scope="col"><xsl:comment>get</xsl:comment></th>
          </thead>

          <tbody>
            <xsl:apply-templates select="dir:file[starts-with(@name,$prefix)]" mode="tbl-html" />
          </tbody>

        </table>

      </xsl:otherwise>

    </xsl:choose>

  </xsl:template>

  <!--+
      | La description d'un fichier téléchargeable dans le tableau
      +-->
  <xsl:template match="dir:file" mode="tbl-html">

    <xsl:variable name="odd">
      <xsl:choose>
        <xsl:when test="position() mod 2 = 0"><xsl:text>even</xsl:text></xsl:when>
        <xsl:otherwise><xsl:text>odd</xsl:text></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr class="pl-tbl-tr-{$odd}">
      <!-- name -->
      <xsl:apply-templates select="@name" mode="tbl-html" />
      <!-- size -->
      <xsl:apply-templates select="@size" mode="tbl-html" />
      <!-- get -->
      <xsl:apply-templates select="." mode="tbl-html-gt" />
    </tr>

  </xsl:template>

  <!--+
      | Le nom du fichier
      +-->
  <xsl:template match="dir:file/@name" mode="tbl-html">
    <td class="pl-tbl-stts-nm">
      <xsl:choose>
        <xsl:when test=".!=''"><xsl:value-of select="." /></xsl:when>
        <xsl:otherwise><xsl:text>&#160;</xsl:text></xsl:otherwise>
      </xsl:choose>
    </td>
  </xsl:template>

  <!--+
      | La taille en kilo octets
      +-->
  <xsl:template match="dir:file/@size" mode="tbl-html">
    <td class="pl-tbl-stts-sz">
      <xsl:choose>
        <xsl:when test=".!=''"><xsl:value-of select="format-number(. div 1024, '# ###,00', 'size')"/></xsl:when>
        <xsl:otherwise><xsl:text>&#160;</xsl:text></xsl:otherwise>
      </xsl:choose>
      <xsl:text>&#160;</xsl:text>
      <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.log-size.kb">ko</i18n:text>
    </td>
  </xsl:template>

  <!--+
      | Le bouton pour récupérer le fichier
      +-->
  <xsl:template match="dir:file" mode="tbl-html-gt">

    <xsl:variable name="class" select="string('yui-button yui-push-button')" />

    <xsl:choose>

		  <xsl:when test="starts-with(@name,'publication-')">
				<xsl:apply-templates select="." mode="tbl-html-gt-publication">
					<xsl:with-param name="class" select="$class" />
				</xsl:apply-templates>
		  </xsl:when>

		  <xsl:otherwise>
				<xsl:apply-templates select="." mode="tbl-html-gt-default">
					<xsl:with-param name="class" select="$class" />
				</xsl:apply-templates>
		  </xsl:otherwise>

		</xsl:choose>

	</xsl:template>

	<!-- Traitement par défaut : un bouton pour récupérer le log au format brute. -->
	<xsl:template match="dir:file" mode="tbl-html-gt-default">
		<xsl:param name="class" />

    <td class="pl-tbl-stts-gt">
      <button id="ex" type="button" class="{$class} pl-tbl-stts-gt"
        i18n:attr="title" title="{$i18n-cat}:{$i18n-root}.button.get-log.title">
        <xsl:attribute name="onclick">javascript:window.location.href="logs/<xsl:value-of select="urle:encode(normalize-space(@name), 'UTF-8')" xmlns:urle="java:java.net.URLEncoder"/>";</xsl:attribute>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.button.get-log.text">
          <xsl:text>télécharger</xsl:text>
        </i18n:text>
      </button>
		</td>

  </xsl:template>

  <!-- On traite un log de publication, on veut plus de boutons -->
  <xsl:template match="dir:file[starts-with(@name, 'publication-')]" mode="tbl-html-gt-publication" priority="+2">
		<xsl:param name="class" />

		<xsl:variable name="urle" select="urle:encode(normalize-space(@name), 'UTF-8')" xmlns:urle="java:java.net.URLEncoder" />

		<td class="pl-tbl-stts-gt">
      <button id="ex" type="button" class="{$class} pl-tbl-stts-gt"
        i18n:attr="title" title="{$i18n-cat}:{$i18n-root}.button.get-publication-log.log.title">
        <xsl:attribute name="onclick">javascript:window.location.href="logs/<xsl:value-of select="$urle"/>";</xsl:attribute>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.button.get-publication-log.log">
          <xsl:text>format brut</xsl:text>
        </i18n:text>
      </button>
      &#32;
      <button id="excsv" type="button" class="{$class} pl-tbl-stts-gt"
        i18n:attr="title" title="{$i18n-cat}:{$i18n-root}.button.get-publication-log.csv.title">
        <xsl:attribute name="onclick">javascript:window.location.href="logs/<xsl:value-of select="replace($urle,'.log','.csv')"/>";</xsl:attribute>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.button.get-publication-log.csv">
          <xsl:text>format CSV</xsl:text>
        </i18n:text>
      </button>
      &#32;
      <button id="extxt" type="button" class="{$class} pl-tbl-stts-gt"
        i18n:attr="title" title="{$i18n-cat}:{$i18n-root}.button.get-publication-log.txt.title">
        <xsl:attribute name="onclick">javascript:window.location.href="logs/<xsl:value-of select="replace($urle,'.log','.txt')"/>";</xsl:attribute>
        <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.button.get-publication-log.txt">
          <xsl:text>format texte</xsl:text>
        </i18n:text>
      </button>
		</td>

  </xsl:template>

</xsl:stylesheet>
