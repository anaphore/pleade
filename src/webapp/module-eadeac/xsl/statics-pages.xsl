<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet 
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="xsl xhtml">

  <!-- Un paramètre qui indique une partie d'URL qui a été supprimée, pour ajuster
      les liens -->
  <xsl:param name="strip" select="''"/>
  <xsl:param name="requested-page"/>
  <xsl:param name="slideshow-enabled"/>

  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="text() | comment() | processing-instruction()">
    <xsl:copy-of select="." copy-namespaces="no" />
  </xsl:template>

  <xsl:template match="@*" priority="-1">
    <xsl:copy-of select="." copy-namespaces="no" />
  </xsl:template>

  <xsl:template match="xhtml:img/@src[not(contains(., ':'))]
                        | xhtml:input/@src[not(contains(., ':'))]
                        | xhtml:script/@src[not(contains(., ':'))]
                        | xhtml:link[@rel='stylesheet']/@href[not(contains(., ':'))]
                        | xhtml:*/@background[not(contains(., ':'))]">
    <xsl:attribute name="{name()}"><xsl:value-of select="concat($strip, .)"/></xsl:attribute>
    <xsl:attribute name="pleade-url-convert"><xsl:text>no</xsl:text></xsl:attribute>
  </xsl:template>

  <!-- Processus similaire pour les liens hypertexte -->
  <xsl:template match="xhtml:a/@href[not(contains(., ':'))]">
    <xsl:attribute name="{name()}"><xsl:value-of select="concat($strip, .)"/></xsl:attribute>
    <xsl:attribute name="pleade-url-convert"><xsl:text>no</xsl:text></xsl:attribute>
  </xsl:template>

</xsl:stylesheet>
