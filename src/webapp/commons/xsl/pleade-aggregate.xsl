<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSLT gérant la fusion du modèle HTML et de l'HTML dynamique.
    |
    | Le modèle est traité jusqu'à son élément HTML @id='pl-pg-body-main-mcol'.
    | A partir de là, on traite l'HTML dynamique à partir du contenu de l'élément HTML <body>.
    | Attention, l'élément HTML @id='pl-pg-body-main-mcol' du modèle n'est pas recopié.
    | Si on en a besoin, il faut le produire via l'HTML dynamique.
    |
    | Le contenu de l'entête <head> de la partie dynamique est traité. On y récupère
    | le titre de la page (balise <title>) et l'ajout de <script> et de <link>.
    |
    | Les balises i18n sont recopiées de manière à laisser le transformateur i18n
    | travailler à la suite de la fusion.
    |
    | Un mécanisme simple permet de "dynamiser" la partie statique issue du modèle
    | HTML pour contextualiser l'espace courant (mettre en valeur le bouton
    | "Recherche" dans le menu par exemple). Pour cela une variable "page" est tirée
    | du Sitemap.
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns="http://www.w3.org/1999/xhtml"
                              xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                              xmlns:xsp="http://apache.org/xsp"
                              xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                              xmlns:xhtml="http://www.w3.org/1999/xhtml"
                              xmlns:xsp-session="http://apache.org/xsp/session/2.0"
                              xmlns:fn="http://www.w3.org/2005/xpath-functions"
                              exclude-result-prefixes="xsl xsp sdx i18n xhtml xsp-session fn">

  <!-- Import de la feuille XSLT gérant l'affichage des menus -->
  <xsl:include href="pleade-menus.xsl"/>
  <!-- Import de la feuille XSLT gérant l'affichage des informations utilisateur -->
  <xsl:include href="pleade-userinfos.xsl"/>

  <xsl:param name="distrib" select="'false'"/>

  <!--+
      | Paramètres dynamiques
      +-->
  <!-- -->
  <!-- Activation de l'interface de sélection de la langue -->
  <xsl:param name="enable-language-selection" select="'false'"/>
  <!-- un paramètre pour indiquer si on souhaite afficher ou non la zone en bas de page -->
  <xsl:param name="show-pleade-zones" select="'false'" />
  <!-- le chemin du theme courant -->
  <xsl:param name="theme" select="'theme/'" />
  <!-- la page active -->
  <xsl:param name="page" select="''" />
  <!-- le path complet de la servlet sur le serveur -->
  <xsl:param name="catalina-home" select="''" />
  <xsl:param name="servlet-home" select="''" />
  <!-- partie d'URL interceptée par la sitemap -->
  <xsl:param name="sitemapURI" select="''" />
  <!-- l'URL complète de la requête -->
  <xsl:param name="queryString" select="''" />
  <!-- URL relative de base de la sitemap -->
  <xsl:param name="sitemapBaseLink" select="''" />
  <!-- la même chose que ci-dessus mais passé en paramètre d'URL
       ça permet de savoir où on est lorsqu'on fait des redirections cocoon:/-->
  <xsl:param name="requestBaseLink" select="''" />
  <!-- l'URL relative de base de la Sitemap
       comme expliqué plus haut, la valeur envoyée par Cocoon est surchargeable
       par le paramètre "sbl" en URL. -->
  <xsl:variable name="baselink">
    <xsl:choose>
      <xsl:when test="$requestBaseLink!=''">
        <xsl:value-of select="$requestBaseLink" />
      </xsl:when>
      <xsl:when test="$sitemapBaseLink">
        <xsl:value-of select="$sitemapBaseLink" />
      </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <!-- le chemin relatif vers le dossier de thème par rapport à l'URL courante -->
  <xsl:variable name="themepath">
    <xsl:value-of select="$baselink" />
    <xsl:if test="$baselink!='' and not(ends-with($baselink, '/'))">
      <xsl:text>/</xsl:text>
    </xsl:if>
    <xsl:value-of select="$theme" />
  </xsl:variable>

    <!-- la variable contenant le modele, doit contenir un élément
         @id=pl-pg-body-main-mcol pour y ajouter le contenu -->
  <xsl:variable name="template" select="/aggregate/template/xhtml:html"/>
  <!-- variable où l'on met le contenu (qui devra toujours commencer par html :
       permettra d'ajouter le titre de la page, des css, js, etc. -->
  <xsl:variable name="main" select="/aggregate/main/xhtml:html"/>

  <!-- l'identifiant du menu gauche -->
  <xsl:variable name="vleft-menu-id-tmp">
		<xsl:call-template name="menu2work">
      <xsl:with-param name="type" select="'vleft'" />
    </xsl:call-template>
  </xsl:variable>
	<!-- Les '::::' sont présents dans le cas des menus accordéons -->
  <xsl:variable name="vleft-menu-id" select="
			if(substring-before($vleft-menu-id-tmp,'::::') != '') then substring-before($vleft-menu-id-tmp,'::::')
			else $vleft-menu-id-tmp
  "/>
  <xsl:variable name="vleft-item-id" select="
			if(substring-after($vleft-menu-id-tmp,'::::') != '') then substring-after($vleft-menu-id-tmp,'::::')
			else ''
  "/>
  
  <!-- l'identifiant du menu de type accordéon (un seul dans la page !) -->
  <xsl:variable name="accordion-menu" select="/aggregate/menus/menu[@id = $vleft-menu-id]/descendant-or-self::menu[contains(@class,'pl-accordion')][1]"/>

    <!--+
        | Traitement de la racine de l'aggrégat
        +-->
  <xsl:template match="/">

<!--<xsl:message>
Informations :
         theme = `<xsl:value-of select="$theme" />`
          page = `<xsl:value-of select="$page" />`
    sitemapURI = `<xsl:value-of select="$sitemapURI" />`
    queryString = `<xsl:value-of select="$queryString" />`
sitemapBaseLink= `<xsl:value-of select="$sitemapBaseLink" />`
requestBaseLink= `<xsl:value-of select="$requestBaseLink" />`
       baselink= `<xsl:value-of select="$baselink" />`
      themepath= `<xsl:value-of select="$themepath" />`
  vleft-menu-id= `<xsl:value-of select="$vleft-menu-id" />`
  vleft-item-id= `<xsl:value-of select="$vleft-item-id" />`
  accordion-menu= `<xsl:value-of select="$accordion-menu/@id"/>`
</xsl:message>-->

    <xsl:apply-templates select="$template" mode="template" />
  </xsl:template>

    <!--+
        | Interception du BODY du template
        | Le but est d'ajouter un div/@class=yui-skin-pleade
        +-->
  <xsl:template match="xhtml:body" mode="template">
    <body>
      <xsl:apply-templates select="@*" mode="template" />
      <xsl:attribute name="class">
        <xsl:value-of select="@class" />
        <xsl:if test="not(contains(@class, 'pl-pg'))">
          <xsl:text> pl-pg</xsl:text>
        </xsl:if>
				<xsl:if test="$page='ead' or $page='ead-fragment'">
					<xsl:value-of select="concat(' ',$page)" />
				</xsl:if>
      </xsl:attribute>
      <div class="yui-skin-pleade">
        <xsl:apply-templates select="node()" mode="template" />
      </div>
    </body>
  </xsl:template>

  <!--+
      | Insertion du formulaire pour la sélection de la langue de l'interface
      | Fenêtre EAD.
      +-->
  <xsl:template match="xhtml:*[@class='pl-pg-context-bg']" mode="template">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*" mode="template" />
      <xsl:if test="$page = 'ead'">
        <xsl:call-template name="language_selection_form"/>
      </xsl:if>
      <xsl:apply-templates select="node()" mode="template" />
    </xsl:element>
  </xsl:template>

  <!--+
      | Insertion du formulaire pour la sélection de la langue de l'interface
      | Autres fenêtres.
      +-->
  <xsl:template match="xhtml:*[@id='pl-pg-body-top']" mode="template">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*" mode="template" />
      <xsl:if test="not($page = 'ead')">
        <xsl:call-template name="language_selection_form"/>
      </xsl:if>
      <xsl:apply-templates select="node()" mode="template" />
    </xsl:element>
  </xsl:template>

  <!--+
      | Formulaire pour la sélection de la langue de l'interface
      +-->
  <xsl:template name="language_selection_form">
    <!-- TODO: les paramètres d'URL doivent être parsés pour créer autant de champs hidden qsue nécessaire dans le formulaire -->
    <xsl:if test="$enable-language-selection = 'true'">
      <xsl:comment> Sélection de la langue </xsl:comment>
      <form id="locale-form">
        <div id="language_selection">
          <label for="locale">
            <i18n:text i18n:catalogue="module-eadeac" key="eadeac.template.locale.selection"/>
          </label>
          <select name="locale" id="locale" class="pl-form-slct">
            <option value="">
              <i18n:text i18n:catalogue="module-eadeac" key="eadeac.template.locale.auto"/>
            </option>
            <option value="fr">
              <i18n:text i18n:catalogue="module-eadeac" key="eadeac.template.locale.fr"/>
            </option>
            <option value="en">
              <i18n:text i18n:catalogue="module-eadeac" key="eadeac.template.locale.en"/>
            </option>
          </select>
          <!-- [JC] On stocke d'abord le bouton dans une variable pour ensuite le faire passer dans le template adéquat -->
          <xsl:variable name="lang_button">
            <button type="submit" class="yui-button yui-submit-button pl-form-search-submit" id="pl-btn-locale">
              <i18n:text i18n:catalogue="module-eadeac" key="eadeac.template.locale.change"/>
            </button>
          </xsl:variable>
          <xsl:apply-templates select="$lang_button" mode="yui-button"/>
        </div>
        <script type="text/javascript">
          <xsl:comment>
            /** Initialisation du bouton de changement de langue */
            $('locale-form').addEvent('submit', function(event) {
              Event.stop(event);
              var _qs = [];
              <!-- Récupération de l'URL courante intégrale -->
              var _href = window.location.href;
              if ( window.location.href.indexOf('?') != -1 ) {
                <!-- Si on a une queryString, on la récupère -->
                _qs = location.search.substring(1).parseQueryString();
              }
              <!-- On ne veut plus maintenant que l'adresse de la page -->
              _href = _href.substring(0, _href.indexOf('?')) + '?';
              <!-- Ajout/substitution de la locale par celle sélectionnée -->
              _qs.locale = $('locale').getSelected()[0].value;
              <!-- Reconstruiction de l'URL finale -->
              _href += Hash.toQueryString(_qs);
              <!-- Et hop! :) -->
              window.location.href = _href;
            });
          </xsl:comment>
        </script>
      </form><xsl:comment> /Sélection de la langue </xsl:comment>
    </xsl:if>
  </xsl:template>

    <!--+
        | Interception de l'élément @id="pl-pg-body" de la page statique
        +-->
  <xsl:template match="xhtml:*[@id='pl-pg-body']" mode="template">

    <xsl:variable name="class">
      <xsl:value-of select="@class" />
      <xsl:choose>
        <xsl:when test="$page='ead' or $page='ead-fragment'"/>
        <xsl:when test="$vleft-menu-id=''">
          <xsl:text> pl-pg-body-wo-menu</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text> pl-pg-body-w-menu</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*" mode="template" />
      <xsl:if test="normalize-space($class)!=''">
        <xsl:attribute name="class">
          <xsl:value-of select="normalize-space($class)" />
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()" mode="template" />
    </xsl:element>

  </xsl:template>

    <!--+
        | Interception du conteneur de la partie principale
        +-->
  <xsl:template match="xhtml:*[@id='pl-pg-body-main']" mode="template">

<!--<xsl:message>[AGGREGATE] menu2work=<xsl:value-of select="$vleft-menu-id" /></xsl:message>-->

    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*" mode="template" />

      <xsl:choose>
        <xsl:when test="$page='ead' or $page='ead-fragment'">
          <xsl:apply-templates select="xhtml:*" mode="template" />
        </xsl:when>
        <xsl:when test="$vleft-menu-id!=''">
          <xsl:apply-templates select="xhtml:*" mode="template">
            <!--<xsl:with-param name="vleft-menu-id" select="$vleft-menu-id" />-->
          </xsl:apply-templates>
        </xsl:when>
        <xsl:otherwise>
          <!-- Pas de menu, on ne traite que le coeur -->
          <xsl:apply-templates select="xhtml:*[@id='pl-pg-body-main-mcol']" mode="template">
          <!--<xsl:with-param name="vleft-menu-id" select="$vleft-menu-id" />-->
          </xsl:apply-templates>
        </xsl:otherwise>
      </xsl:choose>

    </xsl:element>

  </xsl:template>

    <!--+
        | Ajout du contenu dynamique dans l'élément @id="pl-pg-body-main-mcol" du
        | modele statique
        +-->
  <xsl:template match="xhtml:*[@id='pl-pg-body-main-mcol']" mode="template">

    <!--<xsl:param name="vleft-menu-id" select="''" />-->

    <xsl:variable name="class">
      <xsl:choose>
        <xsl:when test="$vleft-menu-id=''">
          <xsl:text>pl-pg-body-main-mcol-wo-menu</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>pl-pg-body-main-mcol-w-menu</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

<!--<xsl:message>[AGGREGATE] intercepte <xsl:value-of select="name()" />/@id='<xsl:value-of select="@id" />', vleft-menu-id=`<xsl:value-of select="$vleft-menu-id" />`</xsl:message>-->

    <xsl:element name="{local-name()}">

      <xsl:apply-templates select="@*" mode="template" />

      <xsl:attribute name="class">
        <xsl:value-of select="normalize-space( concat( @class, ' ', $class ) )" />
                <!--<xsl:if test="not(contains(@class, 'yui-skin-pleade'))">
                    <xsl:text> yui-skin-pleade</xsl:text>
                </xsl:if>-->
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="$main/xhtml:body/node()">
          <xsl:comment>u</xsl:comment><!-- Pour être certain que le div ne soit pas vide -->
          <xsl:apply-templates select="$main/xhtml:body/node()" mode="main" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="node()" mode="template" />
        </xsl:otherwise>
      </xsl:choose>

    </xsl:element>

    <!--cela ne fait que recopié le <div style="display:none;"> car on ne veut pas voir le <h1> ici. Les appels ajax remplaceront de toute façon cette partie-->

  </xsl:template>

  <!--+
    | Traitement de l'entête 'head'
    +-->
  <xsl:template match="xhtml:html/xhtml:head" mode="template">
    <head>
      <!--pour tous les éléments sauf script, link et style : title, meta etc...-->
      <xsl:apply-templates select="*[not(local-name() = 'script' or local-name() = 'link' or local-name() = 'style')]" mode="template" />

      <!-- On recopie les META qui ont pu être ajoutées en i18n dans une XSL -->
      <xsl:variable name="meta-main" select="$main/xhtml:head/meta[@name]|$main/xhtml:head/xhtml:meta[@name]"/>
      <xsl:apply-templates select="$meta-main" mode="main"/>
      <xsl:call-template name="meta-servlet-name"/>

      <!-- INFO (MP) : Désormais, on choisit de déclarer le favicon dans les
          modèles.
      <link rel="shortcut icon" type="image/x-icon" href="{$themepath}images/favicon.ico" /> -->

      <!-- Les CSS
        déclaration des CSS, on définit l'ordre suivant :
        1) YUI
        2) pleade.css
        3) module-*.css
        4) local/*.css
        -->
      <xsl:variable name="css-template" select="xhtml:link[@rel='stylesheet'] | link[@rel='stylesheet']" />
      <xsl:variable name="css-main" select="$main/xhtml:head/xhtml:link[@rel='stylesheet']" />
      <!-- YUI -->
      <xsl:apply-templates select="$css-template[starts-with(@href, 'yui/')]" mode="template" />
      <xsl:apply-templates select="$css-main[starts-with(@href, 'yui/')]" mode="main" />
      <link type="text/css" rel="stylesheet" href="{$themepath}yui/assets/skins/pleade/container.css"/>
      <link type="text/css" rel="stylesheet" href="{$themepath}yui/assets/skins/pleade/container.css"/>
      <link type="text/css" rel="stylesheet" href="{$themepath}yui/assets/skins/pleade/button.css"/>
      <!-- ajout des cdss pour le treeview -->
      <link type="text/css" rel="stylesheet" href="{$themepath}css/mif-tree.css"/>
      <!-- De quoi avoir les menus en accordéon -->
      <xsl:if test="$accordion-menu/@id!=''">
        <link type="text/css" rel="stylesheet" href="{$themepath}yui/assets/skins/pleade/accordionview.css"/>
      </xsl:if>

      <!-- Pleade -->
      <link type="text/css" rel="stylesheet" href="{$themepath}css/pleade.css" />
      <!-- JC FIXME: l'appel à local/pleade.css devrait se faire ici... il faut tester auparavant pour éviter els effets de bord -->
      <!--<link type="text/css" rel="stylesheet" href="{$themepath}css/local/pleade.css" />-->

      <!-- Modules Pleade -->
      <xsl:apply-templates select="$css-template[starts-with(@href, 'css/module-')]" mode="template" />
      <xsl:apply-templates select="$css-main[starts-with(@href, 'css/module-')]" mode="main" />

      <!-- CSS Locales -->
      <xsl:apply-templates select="$css-template[starts-with(@href, 'css/local/')]" mode="template" />
      <xsl:apply-templates select="$css-main[starts-with(@href, 'css/local/')]" mode="main" />

      <!-- Les styles du template et de la page -->
      <xsl:if test="xhtml:style or style">
        <xsl:text>&#10;</xsl:text><xsl:comment>style(s) ajoute(s) par le modele</xsl:comment><xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="xhtml:style | style" mode="template" />
      </xsl:if>
      <xsl:if test="$main/xhtml:head/xhtml:style">
        <xsl:text>&#10;</xsl:text><xsl:comment>style(s) specifique(s) a la page `<xsl:value-of select="$page" />`</xsl:comment><xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="$main/xhtml:head/xhtml:style" mode="main" />
      </xsl:if>

      <!-- Les links autres que "stylesheet" -->
      <xsl:if test="xhtml:link[not(@rel='stylesheet')] or style[not(@rel='stylesheet')]">
        <xsl:text>&#10;</xsl:text><xsl:comment>link(s) ajoute(s) par le modele</xsl:comment><xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="xhtml:link[not(@rel='stylesheet')] | style[not(@rel='stylesheet')]" mode="template" />
      </xsl:if>
      <xsl:if test="$main/xhtml:head/xhtml:link[not(@rel='stylesheet')]">
        <xsl:text>&#10;</xsl:text><xsl:comment>link(s) specifique(s) a la page `<xsl:value-of select="$page" />`</xsl:comment><xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="$main/xhtml:head/xhtml:link[not(@rel='stylesheet')]" mode="main" />
      </xsl:if>

      <!-- On intègre ici systématiquement les feuilles de styles pour IE -->
      <xsl:call-template name="styles-ie"/>

      <!-- Les scripts
        déclaration des scripts, on définit l'ordre suivant :
        1) Mootools
        2) YUI utilities
        3) YUI container
        4) YUI menu
        5) YUI button
        6) pleade
        7) déclarations du modèle
        8) déclarations du coeur de page
        9) scripts communs, (eg, initialisation des boutons YUI)
        Les scripts ne sont pas inclus dans les pages d'aide, ils y sont inutiles
        -->
      <xsl:if test="not(contains($page, 'help-'))">
        <!-- Pour la distribution, on sert des fichiers regroupés et compressés -->
        <xsl:choose>
          <xsl:when test="$distrib = 'true'"><!-- TODO à vérifier si toujours ok -->
            <script type="text/javascript" src="{$themepath}js/mootools/mootools-core-1.3.1-yc.js">// </script>
            <script type="text/javascript" src="{$themepath}js/mootools/mootools-more-1.3.1.1-yc.js">// </script>
          </xsl:when>
          <!-- Hors distribution, on sert chacun des fichiers, non compressés -->
          <xsl:otherwise>
            <script type="text/javascript" src="{$themepath}js/mootools/mootools-core-1.3.1-nc.js">// </script>
            <script type="text/javascript" src="{$themepath}js/mootools/mootools-more-1.3.1.1-nc.js">// </script>
          </xsl:otherwise>
        </xsl:choose>

        <!--  JC FIXME: est-ce qu'on l'on a réellement besoin de charger autant de fichiers à si haut niveau ?
        Il est de nombreuses pages par exemple où l'on a pas le moindre besoin des arbres, etc... -->
        <script type="text/javascript" src="{$themepath}js/pleade/common/util/mootools-common.js">// </script>
        <script type="text/javascript" src="{$themepath}yui/utilities/utilities.js">//</script>
        <script type="text/javascript" src="{$themepath}yui/container/container-min.js">//</script>
        <script type="text/javascript" src="{$themepath}yui/menu/menu-min.js">//</script>
        <script type="text/javascript" src="{$themepath}yui/button/button-min.js">//</script>
        <!-- De quoi avoir les menus en accordéon -->
        <xsl:if test="$accordion-menu/@id!=''">
          <script type="text/javascript" src="{$themepath}yui/accordionview/accordionview-min.js">//</script>
        </xsl:if>

        <script type="text/javascript" src="{$themepath}js/mif/tree/mif.tree-v1.2.6.4.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/util/AjaxRequestHandler.js">// </script>	
        <script type="text/javascript" src="{$themepath}js/pleade/common/manager/SuggestManager.js">// </script>

        <script type="text/javascript" src="{$themepath}js/pleade/common/panel/YuiPanel.js">// </script>	
        <script type="text/javascript" src="{$themepath}js/pleade/common/util/HelpPanels.js">// </script>	
        <script type="text/javascript" src="{$themepath}js/pleade/common/treeview/TreeView.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/loadingmanager/Loading.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/loadingmanager/LoadingMessage.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/treeview/HtmlNode.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/treeview/MifTreeCustom.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/manager/LayoutManager.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/manager/WindowManager.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/basket/BasketManager.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/basket/Basket.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/search-result/ResultTreeView.js">// </script>
        <script type="text/javascript" src="{$themepath}js/pleade/common/search-result/MainResultTreeView.js">// </script>

        <script type="text/javascript" src="{$themepath}js/pleade/module/cdc/CdcTreeView.js">// </script>

        <script type="text/javascript" src="{$themepath}js/pleade/module/thesaurus/ThesXTreeView.js">// </script>
      </xsl:if>
      <xsl:if test="xhtml:script or script">
        <xsl:text>&#10;</xsl:text><xsl:comment>script(s) ajoute(s) par le modele</xsl:comment><xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="xhtml:script | script" mode="template" />
      </xsl:if>
      <xsl:if test="$main/xhtml:head/xhtml:script">
        <xsl:text>&#10;</xsl:text><xsl:comment>script(s) specifique(s) a la page `<xsl:value-of select="$page" />`</xsl:comment><xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="$main/xhtml:head/xhtml:script" mode="main" />
      </xsl:if>
      <xsl:call-template name="more-js"/>
      <xsl:if test="not(contains($page, 'help-'))">
        <script type="text/javascript">
          <xsl:comment>
            /* initialisation des boutons YUI */
            var initializedButtons; // la liste des boutons initialises
            function initButtons() {
              var oButton;
              var oButtons = $$('.yui-button');
              if(initializedButtons==null){
                initializedButtons = new Array(oButtons.length);
              }
              oButtons.each( function(o,i){
                if ( o.id==null || o.id.blank() || initializedButtons.indexOf( o.id ) != -1 );
                else {
                  if ( o.hasClass("yui-submit-button") ) {
                    oButton = new YAHOO.widget.Button( o.id, {type: "submit"} );
                  } else if ( o.hasClass("yui-reset-button") ) {
                    oButton = new YAHOO.widget.Button( o.id, {type: "reset"} );
                  } else {
                    oButton = new YAHOO.widget.Button( o.id );
                  }
                  initializedButtons.push( o.id );
                }
              });
            }
            <xsl:if test="$accordion-menu/@id!=''"><!-- TODO (MP) : Pouvoir paramétrer l'aspect de l'accordéon -->
            function initAccordion(){
              <!-- Trouver l'identifiant du menu de type accordéon courant. On se base sur la classe "current" d'un des menus s'il existe -->
              <xsl:variable name="accordion-entry-active" select="$accordion-menu/item[descendant::item[@id=$vleft-item-id]]"/>
              <xsl:variable name="accordion-entry-pos" select="
                  if ($accordion-entry-active) then count($accordion-entry-active/preceding-sibling::item)
                  else 0
                "/>
              <!--<xsl:message>[ACCORDEON ENTRY <xsl:value-of select="$accordion-entry-active/@id"/>] <xsl:value-of select="$accordion-entry-pos"/></xsl:message>-->
              accordion = new YAHOO.widget.AccordionView('<xsl:value-of select="concat($menu-id-prefix,$accordion-menu/@id)"/>', {collapsible: true, width: 'auto', expandItem: <xsl:value-of select="$accordion-entry-pos"/>, animationSpeed: '0.3', animate: true, effect: YAHOO.util.Easing.easeBothStrong});
            }
            window.addEvent("load", initAccordion);<!-- initialisation du menu accordéon -->
            </xsl:if>
            <!-- initialisation des boutons pour tout le contenu -->
            window.addEvent("load", initButtons);

            var windowManager = new WindowManager();
            var basket = new Basket();
            <xsl:call-template name="pleade-zones" />
          //</xsl:comment>
        </script>
      </xsl:if>
    </head>
  </xsl:template>

  <!-- Fonction permettant de sortir le nom de la servlet dans une balise HTML META "servletName" -->
  <xsl:template name="meta-servlet-name">
    <xsl:variable name="q"><xsl:text>"</xsl:text></xsl:variable>
    <xsl:variable name="servlet-path-complete" select="if($servlet-home!='' and $servlet-home!='$') then $servlet-home else $catalina-home"/>
    <xsl:variable name="string-tmp" select="
                  if ( starts-with($servlet-path-complete,$q) and ends-with($servlet-path-complete,$q) ) 
                      then substring( $servlet-path-complete, 2, string-length($servlet-path-complete) - 2)
                  else $servlet-path-complete"/>
    <xsl:if test="$servlet-path-complete!=''">
      <!--+
          | INFO (MP): Renvoie le nom du dossier représentant la servlet dans 
          | le système de fichiers du serveur hébergeant l'application Web.
          | Attention, ça fonctionne dans le cas où l'URL complète renvoyée
          | par "cocoon-context" se termine par un slash.
          | Si ce n'est pas le cas, alors le "last() - 1" doit être 
          | modifié en "last()".
          +-->
      <meta name="servletName" content="{fu:getName(fu:normalizeNoEndSeparator($string-tmp))}" xmlns:fu="org.apache.commons.io.FilenameUtils"/>
    </xsl:if>
  </xsl:template>

  <!-- pour l'ajout de JS spécifiques à un projet -->
  <xsl:template name="more-js"/>

  <xsl:template name="styles-ie">
    <!-- On intègre ici systématiquement les feuilles de styles pour IE -->
    <xsl:comment><![CDATA[[if lte IE 7]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$themepath"/>css/IE6styles.css<![CDATA["/></style><![endif]]]></xsl:comment>
    <xsl:comment><![CDATA[[if IE 7]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$themepath"/>css/IE7styles.css<![CDATA["/></style><![endif]]]></xsl:comment>
    <xsl:comment><![CDATA[[if IE]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$themepath"/>css/IEstyles.css<![CDATA["/></style><![endif]]]></xsl:comment>
    <!-- On intègre les memes en local-->
    <xsl:comment><![CDATA[[if lte IE 7]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$themepath"/>css/local/pleade-IE6-local.css<![CDATA["/></style><![endif]]]></xsl:comment>
    <xsl:comment><![CDATA[[if IE 7]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$themepath"/>css/local/pleade-IE7-local.css<![CDATA["/></style><![endif]]]></xsl:comment>
    <xsl:comment><![CDATA[[if IE]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$themepath"/>css/local/pleade-IE-local.css<![CDATA["/></style><![endif]]]></xsl:comment>
  </xsl:template>

  <xsl:template name="pleade-zones">
    <xsl:variable name="q"><xsl:text>"</xsl:text></xsl:variable>
    var pleadeZones="<xsl:value-of select="/aggregate/userinfos/@pleade-zones" />";
    var pleadeZone="<xsl:value-of select="/aggregate/userinfos/@pleade-zone" />";
  </xsl:template>

  <!--+
      | Traitement du titre de la page
      | On récupère le titre de la page du contenu dynamique pour remplacer celui
      | de la page finale (sauf si inexistant ou vide)
      +-->
  <xsl:template match="xhtml:html/xhtml:head/xhtml:title" mode="template">
    <title>
            <!-- On insère d'abord un préfixe de titre -->
      <text key="page.title.prefix" xmlns="http://apache.org/cocoon/i18n/2.1" />
      <xsl:choose>
        <xsl:when test="normalize-space($main/xhtml:head/xhtml:title[1]) != ''">
          <xsl:apply-templates select="$main/xhtml:head/xhtml:title[1]/node()" mode="main" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="node()" mode="template" />
        </xsl:otherwise>
      </xsl:choose>
      <!-- A la fin on insère un suffixe de titre -->
      <i18n:text key="page.title.suffix" />
    </title>
  </xsl:template>

  <!--+
      | Ré-écriture des URL pour les ressources.
      | Le but est d'ajouter l'URL de base vers le dossier thème.
      | L'URL de ce dernier est passé en dynamique. C'est le Sitemap appelant
      | qui calcule l'URL de base de l'application. De cette manière, une ressource
      | est toujours appelée avec la même URL (http://monApp/theme/***). La cache du
      | navigateur fonctionne donc quelque soit le module courant (visionneuse,
      | recherche, ect.).
      +-->
  <xsl:template match="xhtml:img/@src | xhtml:input/@src | xhtml:script/@src | xhtml:link/@href | xhtml:*/@background" mode="template">
    <xsl:choose>
      <xsl:when test="contains(.,':') or parent::*/@pleade-url-convert='no'">
        <xsl:copy-of select="." copy-namespaces="no" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="source" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="xhtml:img/@src | xhtml:input/@src | xhtml:script/@src | xhtml:link[@rel='stylesheet']/@href | xhtml:*/@background" mode="main">
    <xsl:choose>
      <xsl:when test="contains(.,':') or parent::*/@pleade-url-convert='no'">
        <xsl:copy-of select="." copy-namespaces="no" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="source" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- Processus similaire pour les liens hypertexte -->
  <xsl:template match="xhtml:a/@href[not(contains(., ':'))]" mode="template">
    <xsl:attribute name="href">
      <xsl:value-of select="concat($baselink, .)"/>
    </xsl:attribute>
  </xsl:template>
  <xsl:template match="@*" mode="source">
    <xsl:attribute name="{name()}">
      <xsl:value-of select="concat($themepath, .)" />
    </xsl:attribute>
  </xsl:template>

  <!--+
      | Construction du titre de page
      | On se base sur un élément XHTML contenant la chaîne 'pl-title' dans son attribut @class.
      +-->
  <xsl:template match="xhtml:*[contains(@class, 'pl-title')]" mode="main">
    <!--        <xsl:message>[MAIN] construction du titre de page</xsl:message>-->
    <xsl:variable name="class" select="normalize-space(replace(@class, 'pl-title', ''))" />
    <div class="pl-title">
      <div class="pl-title-brdr-t">
        <div class="pl-title-tr">
          <xsl:comment>u</xsl:comment>
        </div>
        <div class="pl-title-tl">
          <xsl:comment>u</xsl:comment>
        </div>
      </div>
      <div class="pl-title-main">
        <div class="pl-title-brdr-r">
          <div class="pl-title-brdr-l">
            <xsl:element name="{local-name()}">
              <xsl:if test="$class!=''">
                <xsl:attribute name="class">
                  <xsl:value-of select="$class" />
                </xsl:attribute>
              </xsl:if>
              <xsl:apply-templates select="@*[local-name()!='class'] | node()" mode="main" />
            </xsl:element>
          </div>
        </div>
      </div>
      <div class="pl-title-brdr-b">
        <div class="pl-title-br">
          <xsl:comment>u</xsl:comment>
        </div>
        <div class="pl-title-bl">
          <xsl:comment>u</xsl:comment>
        </div>
      </div>
    </div>
  </xsl:template>

  <!--+
      | Construction des boutons YUI
      | On travaille uniquement sur les boutons/@class=yui-button ...
      +-->
  <xsl:template match="xhtml:*[contains(@class, 'yui-button') and not(parent::span/@class='first-child')] | *[contains(@class, 'yui-button') and not(parent::span/@class='first-child')]" mode="template">
    <xsl:apply-templates select="." mode="yui-button" />
  </xsl:template>
  <xsl:template match="xhtml:*[contains(@class, 'yui-button') and not(parent::span/@class='first-child')] | *[contains(@class, 'yui-button') and not(parent::span/@class='first-child')]" mode="main">
    <xsl:apply-templates select="." mode="yui-button" />
  </xsl:template>
  <xsl:template match="*" mode="yui-button">
    <xsl:variable name="c2" select="normalize-space(replace( @class, 'yui\-([a-z]+\-)?button', '' ))" />
    <xsl:variable name="c1" select="normalize-space(concat( substring-before(@class, $c2), substring-after(@class, $c2) ))" />
    <xsl:variable name="id">
      <xsl:choose>
        <xsl:when test="@id!=''">
          <xsl:value-of select="@id" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="generate-id(.)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <span id="{$id}" class="{$c1}">
      <span class="first-child">
        <xsl:element name="{local-name()}">
          <xsl:if test="$c2!=''">
            <xsl:attribute name="class">
              <xsl:value-of select="$c2" />
            </xsl:attribute>
          </xsl:if>
          <xsl:apply-templates select="@*[not(local-name()='class')] | node()" mode="template" />
        </xsl:element>
      </span>
    </span>
  </xsl:template>

  <!-- On intercepte ici la partie "titre" de l'instrument de recherche dans le template ead -->
  <!--<xsl:template match="xhtml:div[@id='pl-title-ead-index']" mode="template">-->
      <!-- On prend le h1 dans le body généré -->
      <!--<xsl:apply-templates select="$main/xhtml:body/xhtml:div/xhtml:h1[@class='pl-title']" mode="main"/>
  </xsl:template>-->
  <!-- pour ignorer du contenu -->
  <xsl:template match="xhtml:*[@class = 'pl-ignore-content-main']" mode="main"/>

  <xsl:template match="xhtml:*[@class = 'pl-title-ead']" mode="template">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*" mode="template"/>
      <xsl:apply-templates select="$main/xhtml:body/xhtml:div/xhtml:h1[@class='pl-title']" mode="main"/>
    </xsl:element>
  </xsl:template>

  <!-- Balises i18n -->
  <xsl:template match="i18n:* | @i18n:* | i18n:*/@*" mode="template">
    <xsl:apply-templates select="." />
  </xsl:template>
  <xsl:template match="i18n:* | @i18n:* | i18n:*/@*" mode="main">
    <xsl:apply-templates select="." />
  </xsl:template>
  <xsl:template match="i18n:*">
    <xsl:element name="{local-name()}" namespace="http://apache.org/cocoon/i18n/2.1">
      <xsl:apply-templates select="@* | node()" />
    </xsl:element>
  </xsl:template>
  <xsl:template match="@i18n:* | i18n:*/@*">
    <xsl:attribute name="{local-name()}" namespace="http://apache.org/cocoon/i18n/2.1">
      <xsl:value-of select="." />
    </xsl:attribute>
  </xsl:template>

  <!-- Copie générique -->
  <xsl:template match="node() | @*" mode="template" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="template" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="*" mode="main" priority="-1">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@* | node()" mode="main" />
    </xsl:element>
  </xsl:template>
  <xsl:template match="@*" mode="main" priority="-1">
    <xsl:attribute name="{local-name()}">
      <xsl:value-of select="." />
    </xsl:attribute>
  </xsl:template>
  <xsl:template match="text()|comment()|processing-instruction()" mode="main" priority="-1">
    <xsl:copy-of select="." />
  </xsl:template>
  <xsl:template match="@pleade-url-convert" mode="main" />
  <xsl:template match="@pleade-url-convert" mode="template" />
</xsl:stylesheet>

