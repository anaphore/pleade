<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
>


	<!-- Méthodes liées à la fragmentation des documents (pour l'indexation et la table des matières) -->

	<!-- Une variable booléenne pour savoir si la méthode de construction de la table des matières est la même que la fragmentation du document lui-même. -->
	<xsl:variable name="same-method" select="boolean($toc-method = '_same' or ($toc-method = $frag-method and $toc-method-info = $frag-method-info))"/>

	<!-- Template général pour vérifier si un composant est un fragment. Si oui, retourne "true". -->
	<xsl:template name="is-fragment">
		<xsl:param name="method" select="$frag-method"/>		<!-- La méthode à utiliser -->
		<xsl:param name="info" select="$frag-method-info"/>		<!-- Les informations complémentaires pour la méthode -->
		<xsl:param name="checking-toc" select="false()"/>		<!-- Si vrai, on demande de vérifier pour la table des matières -->
		<xsl:param name="checking-notsearchable" select="false()"/>	<!-- Si vrai, on demande de vérifier pour les composants cherchables -->
		<xsl:param name="frag-value" select="'false'"/>			<!-- La valeur obtenue pour la fragmentation du même élément -->
		<xsl:choose>
			<!-- Si on vérifie la table des matières et que c'est la même méthode, alors on sait quoi répondre -->
			<xsl:when test="$checking-toc and $same-method"><xsl:value-of select="$frag-value"/></xsl:when>
			<!-- On fragmente toujours sur le archdesc -->
			<xsl:when test="self::archdesc and not($checking-notsearchable)">true</xsl:when>
			<!-- Si on fragmente sur tous les éléments, alors on retourne tout de suite oui -->
			<xsl:when test="$method = 'all-components'">true</xsl:when>
			<!-- Si aucune fragmentation, on retourne tout de suite false -->
			<xsl:when test="$method='_none'"/>
			<!-- Maintenant, les méthodes spécifiques -->
			<xsl:when test="$method = 'element-name'">
				<xsl:call-template name="break-by-element-name">
					<xsl:with-param name="pleade-param" select="$info"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method = 'h-level'">
				<xsl:call-template name="break-by-h-level">
					<xsl:with-param name="pleade-param" select="$info"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method = 'a-level'">
				<xsl:call-template name="break-by-a-level">
					<xsl:with-param name="pleade-param" select="$info"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$method = 'with-level'">
				<xsl:call-template name="break-by-with-level"/>
			</xsl:when>
			<xsl:when test="$method = 'without-level'">
				<xsl:call-template name="break-by-without-level"/>
			</xsl:when>
			<xsl:when test="$method = 'last-components-in-AC-tree'">
				<xsl:call-template name="break-by-last-components-in-AC-tree"/>
			</xsl:when>
			<xsl:when test="$method = 'c-with-child-or-in-file-plan'">
				<xsl:call-template name="break-by-c-with-child-or-in-file-plan"/>
			</xsl:when>
			<xsl:when test="$method = 'c-not-partie-article'">
				<xsl:call-template name="break-by-not-partie-article"/>
			</xsl:when>
			<xsl:when test="$method = 'AC-title'">
				<xsl:call-template name="break-by-AC-title"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Fragmentation en fonction du nom de l'élément. -->
	<xsl:template name="break-by-element-name">
		<xsl:param name="pleade-param" select="''"/>
		<!-- Le patron pour obtenir la liste des noms d'éléments -->
		<xsl:variable name="pattern">(\s|\|)</xsl:variable>
		<xsl:variable name="name" select="local-name()"/>
		<xsl:if test="fn:tokenize($pleade-param, $pattern)[. = $name]">true</xsl:if>
	</xsl:template>

	<!-- Fragmentation en fonction du niveau hiérarchique. -->
	<xsl:template name="break-by-h-level">
		<xsl:param name="pleade-param" select="''"/>

		<!-- La séquence des spécifications de niveau -->
		<xsl:variable name="pattern">(\s|\|)</xsl:variable>
		<xsl:variable name="levels" select="fn:tokenize($pleade-param, $pattern)"/>

		<!-- On vérifie d'abord par le haut car c'est nettement plus efficace -->
		<xsl:variable name="hlevel" select="count(ancestor-or-self::*[self::archdesc or self::c or starts-with(name(), 'c0') or starts-with(name(), 'c1')])"/>
		<xsl:choose>
			<xsl:when test="$levels[. = concat($hlevel, 'h') or . = concat($hlevel, 'u')]">true</xsl:when>
			<xsl:otherwise>
				<!-- On doit (peut-être vérifier le bas -->
				<xsl:if test="contains($pleade-param, 'b')">
					<!-- Pas le choix! -->
					<xsl:variable name="max-depth">
						<xsl:for-each select=".//c | .//c01 | .//c02 | .//c03 | .//c04 | .//c05 | .//c06 | .//c07 | .//c08 | .//c09 | .//c10 | .//c11 | .//c12">
							<xsl:sort select="count(ancestor-or-self::*[self::archdesc or self::c or starts-with(name(), 'c0') or starts-with(name(), 'c1')])" data-type="number"/>
							<xsl:if test="position() = last()">
								<xsl:value-of select="count(ancestor-or-self::*[self::archdesc or self::c or starts-with(name(), 'c0') or starts-with(name(), 'c1')])"/>
							</xsl:if>
						</xsl:for-each>
						<xsl:if test="not(.//c or .//c01 or .//c02 or .//c03 or .//c04 or .//c05 or .//c06 or .//c07 or .//c08 or .//c09 or .//c10 or .//c11 or .//c12)">
							<xsl:value-of select="0"/>
						</xsl:if>
					</xsl:variable>
					<xsl:variable name="max-level">
						<xsl:choose>
							<xsl:when test="number($max-depth)"><xsl:value-of select="$max-depth"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="$hlevel"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="blevel" select="$max-depth - $hlevel + 1"/>
					<xsl:if test="$levels[. = concat($blevel, 'b')]">true</xsl:if>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Fragmentation par le niveau archivistique. -->
	<xsl:template name="break-by-a-level">
		<xsl:param name="pleade-param" select="''"/>
		<!-- Si pas d'attribut @level ou @otherlevel, alors on peut optimiser -->
		<xsl:if test="@level or @otherlevel">
			<!-- La séquence des niveaux archivistiques à vérifier -->
			<xsl:variable name="pattern">
				<xsl:choose>
					<xsl:when test="contains($pleade-param, '|')">(\|)</xsl:when>
					<xsl:otherwise>(\s)</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="levels" select="fn:tokenize($pleade-param, $pattern)"/>
			<xsl:if test="$levels[. = current()/@level or . = current()/@otherlevel]">true</xsl:if>
		</xsl:if>
	</xsl:template>

	<!-- Fragmentation si un attribut @level est présent -->
	<xsl:template name="break-by-with-level">
		<xsl:if test="@level">true</xsl:if>
	</xsl:template>

	<!-- Fragmentation s'il n'y a pas d'attribut @level -->
	<xsl:template name="break-by-without-level">
		<xsl:if test="not(@level)">true</xsl:if>
	</xsl:template>

	<!-- Fragmentation uniquement pour les composants fiches d'articles ou de parties d'articles dans Aide au classement -->
	<xsl:template name="break-by-last-components-in-AC-tree">
		<xsl:if test="self::c and (starts-with(@id, 'pa-') or starts-with(@id, 'de-'))">true</xsl:if>
	</xsl:template>

	<!-- Fragmentation si un enfant ou si dans un plan de classement. -->
	<xsl:template name="break-by-c-with-child-or-in-file-plan">
		<xsl:if test="(self::c and descendant::c) or (self::c and starts-with(@id, 'tt')) or (self::c and preceding-sibling::c[contains(@id, 'tt')]) or (self::c and following-sibling::c[contains(@id, 'tt')])">true</xsl:if>
	</xsl:template>
	<!-- Fragmentation si ce n'est pas une aprtie d'article -->
	<xsl:template name="break-by-not-partie-article">
		<xsl:if test="self::c and not(starts-with(@id, 'pa-'))">true</xsl:if>
	</xsl:template>
	<!-- Fragmentation si c'est un titre aide au classement -->
	<xsl:template name="break-by-AC-title">
		<xsl:if test="self::c and starts-with(@id, 'tt')">true</xsl:if>
	</xsl:template>

</xsl:stylesheet>
