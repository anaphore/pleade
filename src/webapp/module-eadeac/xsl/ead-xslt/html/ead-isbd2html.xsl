<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: ead2html.xsl 12608 2008-10-28 19:09:23Z cbougouin $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		exclude-result-prefixes="xsl i18n pleade sdx fn dir">

		<!-- séparateur des zones ISBD -->
		<xsl:variable name="zone-sep" select="'&#xA0;&#x2013;&#xA0;'"/>
		<xsl:variable name="dot" select="'.'"/>

	<!--		CETTE TRANSFORMATION GENERE LE HTML D'UNE NOTICE BIBLIOGRAPHIQUE AU FORMAT ISBD
			Norme ISBD : http://www.ifla.org/VII/s13/pubs/cat-isbd.htm
									http://www.ifla.org/VI/3/nd1/isbdlist.htm
			Plusieurs ISBD selon le type de ressources :
				- ISBD(A) : documents anciens
				- ISBD(CM) : Cartes
				- ISBD(CR) : Publications en série et autres ressources continues
				- ISBD(ER) : Ressources électroniques
				- ISBD(M): Monographies
				- ISBD(NBM) : "Non-livres"
				- ISBD(PM) : Musique imprimée
				- ISBD(S) : Publication en Série

				ISBD - Général

	Ci-dessous, les différentes zones de l'ISBD avec pour chacune la ponctuation. Pour chaque élément, on précise si les composants sont [O] obligatoires, [F] facultatifs, [C] conditionnels, [R] répétables
	Zone 1 Titre et mention de responsabilité
				1.1 Titre propre [O]
		[]	1.2 Indication général du genre de document [F]
		=		1.3 Titre parallèle [C, R]
		:		1.4 Complément du titre [C, R]
		/		1.5 Mention de responsabilité (première) [O]
		;		1.6 Autres mentions de responsabilité [C, R]

	. - Zone 2 Edition
				2.1 Mention d'édition [O]
		=		2.2 Mention parallèle d'édition [F, R]
		/		2.3 Mention de responsabilité relative à l'édition (première) [O]
		;		2.3 Autres mentions de responsabilité relatives à l'édition [C, R]
		,		2.4 Autre mention d'édition [O, R]
		/		2.5 Mentions de responsabilité suivant une autre mention d’édition (première) [O]
		;		2.5 Autres mentions de responsabilité suivant une autre mention d’édition [C, R]

	. - Zone 3 Zone particulière à certains types de ressources (ou à certaines catégories de publication) (par exemple, pour ISBD(S))
				3.1 Données mathématiques (Ressources cartographiques)	[O, R]
					3.1.1  Mention d’échelle
		;			3.1.2  Mention de projection
	 ()			3.1.3  Mention de coordonnées et d’équinoxe
	 	;				3.1.3.2 mention d'équinoxe
				3.2 Mention de la présentation musicale (Musique notée) [O]
					3.2.1  Mention de présentation musicale
		=			3.2 2   Mention parallèle de présentation musicale
				3.3 Numérotation (Publications en série) [O]
				3.4 Type et de la taille de la ressource électronique
					3.4.1 Type de ressource électronique [R]
		()		3.4.2 Taille de la ressource électronique [F, R]

	. - Zone 4 Publication, diffusion, etc
				4.1 Lieu de publication, diffusion, etc (premier) [0]
		;		4.1 Autres lieux de publication, diffusion, etc. [C, R]
		:   4.2 Nom de l'éditeur, diffuseur, etc [O, R]
		[]	4.3 Mention de la fonction d'éditeur, diffuseur, etc [F]
		,		4.4 Date de publication, diffusion, etc [O]
		(		4.5  Lieu d’impression, fabrication ou gravure [C, R]
		:		4.6  Nom de l’imprimeur, fabricant ou graveur (pour les monographies anciennes) et/ou information sur l’impression [C, R]
		,)	4.7  Date d’impression ou de fabrication [C]

	. - Zone 5 Collation [C,R]
				5.1 Nombre d'unités et indication spécifique du genre de document [O]
		:		5.2 Autres caractéristiques matérielles [C]
		;		5.3 Dimensions [C]
		+		5.4	Matériel d'accompagnement [F, R]

	. - Zone 6 Collection () [R]
				6.1 Titre propre de la collection [O]
		=		6.2 Titre parallèle de la collection [C, R]
		:		6.3 Compléments du titre de collection [C, R]
		/		6.4	Mention de responsabilité relative à la collection (première) [C]
		;		6.4 Autres mentions de responsabilité [C, R]
		,		6.5 ISSN de collection [O]
		;		6.6 Numérotation à l'intérieur de la collection [C]

	. - Zone 7 Notes [C, R]

	. - Zone 8 Numéro normalisé et modalités d'acquisition [R]
				8.1  Identificateur de la ressource [O]
		=		8.2  Titre clé (ressources continues) [C]
		:		8.3  Modalités d’acquisition et/ou prix [F, R]
		()	8.4  Qualificatifs [F, R]

	Exemple pour Monographie
			Le catalogage : méthode et pratiques. 1, Monographies et publications en série [Texte imprimé] / par Isabelle Dussert-Carbone et Marie-Renée Cazabon. - Nouv. éd. - Paris : Éd. du Cercle de la Librairie, 1991. - 479 p. : ill. ; 24 cm. - (Collection Bibliothèques, ISSN 0184-0886).
			Bibliogr. p. 465-476. - ISBN 2-7654-0458-5
	-->

	<!-- Le cas particulier des fiches ISBD de Aide au classement -->
	<!--Conteneur : did(head?,(abstract|container|dao|daogrp|langmaterial|materialspec|note|origination|physdesc|physloc|repository|unitdate|unitid|unittitle)+)-->
	<!-- Elément courant = did -->
	<xsl:template match="did[starts-with(@altrender, 'isbd')]">
		<xsl:if test="$active-synch-toc='true'">
			<xsl:apply-templates select="." mode="synchronise-tool" />
		</xsl:if>
		<div class="pl-pgd-sect pl-ead-did-isbd">
			<!-- Génère un <p> vide parfois (la clé i18n construite n'est pas correcte) -->
			<!--<xsl:call-template name="display-label"/>-->
			<table class="pl-pgd-fv pl-ead-did pl-ead-did-isbd">
				<xsl:call-template name="i18n-attribute">
					<xsl:with-param name="attribute-name" select="'summary'"/>
					<xsl:with-param name="key" select="'ead-did-table-@summary'"/>
				</xsl:call-template>
				<!-- <xsl:call-template name="display-label">
					<xsl:with-param name="out-el-name" select="'caption'"/>
				</xsl:call-template> -->
				<xsl:call-template name="isbd-display"/>
				<!-- Pour les éléments hors notice ISBD -->
				<xsl:apply-templates mode="not-isbd"/>
			</table>
		</div>
	</xsl:template>

	<!-- En mode ISBD, ceux qui sont traités normalement -->
	<xsl:template match="head|abstract|container|dao|daogrp|note|langmaterial|origination|physloc|repository|unitid" mode="not-isbd">
		<xsl:apply-templates select="."/>
	</xsl:template>
	<!-- Ceux qui sont déjà traités dans la notice ISBD -->
	<xsl:template match="unittitle|physdesc|materialspec" mode="not-isbd">
	</xsl:template>
	<!-- Ceux qui font doublon et que l'on ne veut pas sortir -->
	<xsl:template match="unitdate" mode="not-isbd">
	</xsl:template>
	<xsl:template match="odd[odd[@type='note isbd'] or odd[@type='document hôte'] or odd[@type='numéro identification et prix'] and ancestor::did[starts-with(@altrender, 'isbd')]]" priority="10">
	</xsl:template>
	<!-- <xsl:template match="odd/odd[@type='numéro identification et prix' and ../did[starts-with(@altrender, 'isbd')]]">
	</xsl:template> -->

	<!-- isbd display : repris dans Pleade 2 -->
	<!-- has been checked and updated according to Aide au classement 3 -->
	<xsl:template name="isbd-display">
		<tr class="pl-tbl-lgn-isbd">
			<th scope="row" class="pl-tbl-th">
				<span class="pl-tbl-th-sp">
					<i18n:text key="ead.did.isbd.{@altrender}"><xsl:text>ead.did.isbd.</xsl:text><xsl:value-of select="@altrender"/></i18n:text>
				</span>
			</th>
			<td class="pl-tbl-did-isbd pl-ead-did-{altrender}">
				<xsl:choose>
					<xsl:when test="@altrender='isbd-m-fr'">
						<xsl:apply-templates select="." mode="isbd-m"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-cm-fr'">
						<xsl:apply-templates select="." mode="isbd-cm"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-cr-fr'">
						<xsl:apply-templates select="." mode="isbd-cr"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-fr-composants'">
						<xsl:apply-templates select="." mode="isbd-fr-composants"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-er-fr'">
						<xsl:apply-templates select="." mode="isbd-er-fr"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-nbm-fr-imagefixe'">
						<xsl:apply-templates select="." mode="isbd-imagefixe"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-nbm-fr-son'">
						<xsl:apply-templates select="." mode="isbd-son"/>
					</xsl:when>
					<xsl:when test="@altrender='isbd-nbm-fr-video'">
						<xsl:apply-templates select="." mode="isbd-video"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-imports/> <!-- Affichage normal tabulaire -->
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
		<xsl:apply-templates select="physdesc[genreform[@source='liste-typedocAC']]"/>
	</xsl:template>
	<xsl:template match="genreform[@source='liste-typedocAC' and ancestor::did[starts-with(@altrender, 'isbd')]]" priority="10">
		<xsl:choose>
			<xsl:when test=".='Monographie imprimée'">
				<i18n:text key="ead.isbd.genreform.typedocAC.monographie">livre</i18n:text>
			</xsl:when>
			<xsl:when test=".='Périodique bibliothèque'">
				<i18n:text key="ead.isbd.genreform.typedocAC.periodique">périodique</i18n:text>
			</xsl:when>
			<xsl:when test=".='Partie composante'">
				<i18n:text key="ead.isbd.genreform.typedocAC.composante">article ou tiré à part de périodique</i18n:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="physdesc/extent[../genreform[@source='liste-typedocAC' and ancestor::did[starts-with(@altrender, 'isbd')]]]" priority="10">
	</xsl:template>

	<!-- *************************************************************************
					Les différents formats ISBD
			***************************************************************************** -->
	<!-- ISBD(M) pour les monographies -->
	<xsl:template match="did" mode="isbd-m">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<xsl:call-template name="isbd-zone4"/>
		<xsl:call-template name="isbd-zone5"/>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>

	<!-- ISBD for book components -->
	<xsl:template match="did" mode="isbd-fr-composants">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<!-- <xsl:call-template name="isbd-zone4"/> ne s'applique pas -->
		<xsl:if test="physdesc/extent != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[1]" mode="isbd"/>
			</span>
		</xsl:if>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
		<xsl:if test="../odd/odd[@type='document hôte']">
			<span class="pl-ead-isbd-zone7 pl-ead-odd-doc-hote">
				<!-- <xsl:text>In : </xsl:text> -->
				<xsl:apply-templates select="../odd/odd[@type='document hôte']" mode="isbd"/>
			</span>
		</xsl:if>
	</xsl:template>

	<!-- ISBD(CM) pour les cartes -->
	<xsl:template match="did" mode="isbd-cm">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<xsl:call-template name="isbd-zone3.1"/>
		<xsl:call-template name="isbd-zone4"/>
    <!-- [JC : en doublon avec les lignes au dessous ? -->
		<!--<xsl:call-template name="isbd-zone5"/>-->
		<xsl:if test="physdesc/extent[@type='collation'] != '' or physdesc/physfacet[@type='technique' or @type='support' or @type='noir et blanc ou couleur'] != '' or physdesc/dimensions != '' or physdesc/extent[preceding-sibling::extent] != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[@type='collation']" mode="isbd"/>
				<xsl:if test="physdesc/physfacet[@type='technique' or @type='support' or @type='noir et blanc ou couleur'] != ''">
					<xsl:text>&#xA0;:&#xA0;</xsl:text>
					<xsl:if test="physdesc/physfacet[@type='technique']">
						<span class="pl-ead-physfacet-tech">
							<xsl:apply-templates select="physdesc/physfacet[@type='technique']"/>
						</span>
					</xsl:if>
					<xsl:if test="physdesc/physfacet[@type='support']">
						<xsl:text>,&#xA0;</xsl:text>
						<span class="pl-ead-physfacet-support">
							<xsl:apply-templates select="physdesc/physfacet[@type='support']"/>
						</span>
					</xsl:if>
					<xsl:if test="physdesc/physfacet[@type='noir et blanc ou couleur']">
						<xsl:text>,&#xA0;</xsl:text>
						<span class="pl-ead-physfacet-nbcolor">
							<xsl:apply-templates select="physdesc/physfacet[@type='noir et blanc ou couleur']"/>
						</span>
					</xsl:if>
				</xsl:if>
				<xsl:if test="physdesc/dimensions != ''">
					<xsl:text>&#xA0;;&#xA0;</xsl:text>
					<span class="pl-ead-dimensions">
						<xsl:apply-templates select="physdesc/dimensions"/>
					</span>
				</xsl:if>
				<xsl:if test="physdesc/extent[preceding-sibling::extent] != ''">
					<!-- Reprise de Pleade 2 : for ac 3, below -->
					<!-- <xsl:if test="physdesc/extent[@type='matériel accompagnement]"> -->
					<xsl:text>&#xA0;+&#xA0;</xsl:text>
					<span class="pl-ead-extent">
						<xsl:apply-templates select="physdesc/extent[preceding-sibling::extent]"/>
					</span>
				</xsl:if>
				<xsl:value-of select="$dot"/>
			</span>
		</xsl:if>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>

	<!-- ISBD(S) pour les publications en série -->
	<xsl:template match="did" mode="isbd-cr">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<xsl:call-template name="isbd-zone3.3"/>
		<xsl:call-template name="isbd-zone4"/>
		<xsl:if test="physdesc/extent != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[1]" mode="isbd"/>
			</span>
		</xsl:if>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>

	<!-- ISBD(ER) pour les ressources électroniques -->
	<xsl:template match="did" mode="isbd-er-fr">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<xsl:call-template name="isbd-zone3.4"/>
		<!-- <xsl:call-template name="isbd-zone3.1"/> dans Pleade 2 mais pas de raison d'être là -->
		<xsl:call-template name="isbd-zone4"/>
		<xsl:if test="physdesc/extent[@type='collation'] != '' or physdesc/physfacet[@type='noir et blanc ou couleur'] != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:choose>
					<xsl:when test="count(physdesc/extent[@type='collation'] and . != '')=2">
						<xsl:apply-templates select="physdesc/extent[@type='collation'][2]" mode="isbd"/>
					</xsl:when>
					<xsl:when test="count(physdesc/extent[@type='collation'] and . != '')=1">
						<xsl:apply-templates select="physdesc/extent[@type='collation']" mode="isbd"/>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="physdesc/physfacet[@type='noir et blanc ou couleur'] != ''">
					<xsl:text>&#xA0;:&#xA0;</xsl:text>
					<span class="pl-ead-physfacet-nbcolor">
						<xsl:apply-templates select="physdesc/physfacet[@type='noir et blanc ou couleur']"/>
					</span>
				</xsl:if>
			</span>
		</xsl:if>
		<!-- Reprise de Pleade 2 : below, old template for AC2 or AC3 preliminary versions -->
		<!-- <xsl:if test="physdesc/extent[contains(., 'disque')]">
			<xsl:value-of select="$zone-sep"/>
			<span class="pl-ead-extent-disque">
				<xsl:apply-templates select="physdesc/extent[contains(., 'disque')]"/>
			</span>
			<xsl:if test="physdesc/physfacet[@type='noir et blanc ou couleur']">
				<xsl:value-of select="$zone-sep"/>
				<span class="pl-ead-physfacet-nbcolor">
					<xsl:apply-templates select="physdesc/physfacet[@type='noir et blanc ou couleur']"/>
				</span>
			</xsl:if>
		</xsl:if>
		<xsl:if test="physdesc/extent[preceding-sibling::extent] or physdesc/extent[contains(., 'cart')]">
			<xsl:value-of select="$zone-sep"/>
			<span class="pl-ead-extent">
				<xsl:apply-templates select="physdesc/extent[preceding-sibling::extent] | physdesc/extent[contains(., 'cart')]"/>
			</span>
		</xsl:if>-->
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>

	<!-- ISBD pour images -->
	<xsl:template match="did" mode="isbd-imagefixe">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<xsl:call-template name="isbd-zone4"/>
		<xsl:if test="physdesc/extent[@type='collation'] != '' or physdesc/physfacet[@type='support' or @type='noir et blanc ou couleur'] != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[@type='collation']" mode="isbd"/>
				<xsl:if test="physdesc/physfacet[@type='support' or @type='noir et blanc ou couleur' or @type='technique'] != ''">
					<xsl:if test="physdesc/physfacet[@type='technique'] != ''">
						<xsl:text>&#xA0;:&#xA0;</xsl:text>
						<span class="pl-ead-physfacet-tech">
							<xsl:apply-templates select="physdesc/physfacet[@type='technique']"/>
						</span>
					</xsl:if>
					<xsl:if test="physdesc/physfacet[@type='support'] or physdesc/physfacet[@type='noir et blanc ou couleur']">
						<xsl:text>,&#xA0;</xsl:text>
						<xsl:if test="physdesc/physfacet[@type='support']">
							<span class="pl-ead-physfacet-support">
								<xsl:apply-templates select="physdesc/physfacet[@type='support']"/>
							</span>
							<xsl:if test="physdesc/physfacet[@type='noir et blanc ou couleur']">
								<xsl:text>,&#xA0;</xsl:text>
								<span class="pl-ead-physfacet-nbcolor">
									<xsl:apply-templates select="physdesc/physfacet[@type='noir et blanc ou couleur']"/>
								</span>
							</xsl:if>
						</xsl:if>
					</xsl:if>
				</xsl:if>
				<xsl:if test="physdesc/dimensions != ''">
					<xsl:text>&#xA0;;&#xA0;</xsl:text>
					<span class="pl-ead-dimensions">
						<xsl:apply-templates select="physdesc/dimensions"/>
					</span>
				</xsl:if>
				<xsl:if test="physdesc/extent[preceding-sibling::extent] != ''">
					<!-- Reprise de Pleade 2 : for ac 3, below -->
					<!-- <xsl:if test="physdesc/extent[@type='matériel accompagnement]"> -->
					<xsl:text>&#xA0;+&#xA0;</xsl:text>
					<span class="pl-ead-extent">
						<xsl:apply-templates select="physdesc/extent[preceding-sibling::extent]"/>
					</span>
				</xsl:if>
			</span>
		</xsl:if>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>

	<!-- ISBD pour les vidéogrammes -->
	<xsl:template match="did" mode="isbd-video">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone2"/>
		<xsl:call-template name="isbd-zone4"/>
		<xsl:if test="physdesc/extent[@type='collation' or @type='matériel accompagnement'] != '' or materialspec[@label='durée'] != '' or phystech/p != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[@type='collation']" mode="isbd"/>
				<xsl:if test="materialspec[@label='durée']">
					<span class="pl-ead-materialspec-duree">
						<xsl:text> (</xsl:text>
						<xsl:apply-templates select="materialspec[@label='durée']" mode="isbd"/>
						<xsl:text>)</xsl:text>
					</span>
				</xsl:if>
				<xsl:if test="phystech/p != ''">
					<xsl:text>&#xA0;:&#xA0;</xsl:text>
					<span class="pl-ead-phystech">
						<xsl:apply-templates select="phystech/p"/>
					</span>
				</xsl:if>
				<xsl:if test="physdesc/extent[@type='matériel accompagnement'] != ''">
					<xsl:text>&#xA0;+&#xA0;</xsl:text>
					<span class="pl-ead-extent-mataccomp">
						<xsl:apply-templates select="physdesc/extent[@type='matériel accompagnement']"/>
					</span>
				</xsl:if>
				<!-- Reprise de Pleade 2 <xsl:if test="physdesc/physfacet[@type='support'] or physdesc/physfacet[@type='noir et blanc ou couleur']">
					<xsl:text>,&#xA0;</xsl:text>
					<xsl:if test="physdesc/physfacet[@type='support']">
						<span class="pl-ead-physfacet-support">
							<xsl:apply-templates select="physdesc/physfacet[@type='support']"/>
						</span>
						<xsl:if test="physdesc/physfacet[@type='noir et blanc ou couleur']">
							<xsl:text>,&#xA0;</xsl:text>
							<span class="pl-ead-physfacet-nbcolor">
								<xsl:apply-templates select="physdesc/physfacet[@type='noir et blanc ou couleur']"/>
							</span>
						</xsl:if>
					</xsl:if>
				</xsl:if> -->
			</span>
		</xsl:if>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>

	<xsl:template match="materialspec[@label='durée' and ancestor::did[starts-with(@altrender, 'isbd')]]" mode="isbd">
		<xsl:value-of select="."/>
	</xsl:template>

	<!-- ISBD pour les sons -->
	<xsl:template match="did" mode="isbd-son">
		<xsl:call-template name="isbd-zone1"/>
		<xsl:call-template name="isbd-zone4"/>
		<xsl:if test="physdesc/extent[@type='collation'] != '' or phystech/p != '' or materialspec[@label='durée'] != '' or physdesc/dimensions != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[@type='collation']" mode="isbd"/>
				<xsl:if test="materialspec[@label='durée'] != ''">
					<span class="pl-ead-materialspec-duree">
						<xsl:text> (</xsl:text>
						<xsl:apply-templates select="materialspec[@label='durée']"/>
						<xsl:text>)</xsl:text>
					</span>
				</xsl:if>
				<xsl:if test="phystech/p != ''">
					<xsl:text>&#xA0;:&#xA0;</xsl:text>
					<span class="pl-ead-phystech">
						<xsl:apply-templates select="phystech/p"/>
					</span>
				</xsl:if>
				<xsl:if test="physdesc/dimensions != ''">
					<xsl:text>&#xA0;;&#xA0;</xsl:text>
					<span class="pl-ead-dimensions">
						<xsl:apply-templates select="physdesc/dimensions"/>
					</span>
				</xsl:if>
			</span>
		</xsl:if>
		<xsl:call-template name="isbd-zone6"/>
		<xsl:call-template name="isbd-zone7"/>
		<xsl:call-template name="isbd-zone8"/>
	</xsl:template>


	<!-- ***************************************************************************
												Formatage des zones ISBD
	******************************************************************************* -->
	<!-- Zone 1 : Titre et mention de responsabilité -->
	<xsl:template name="isbd-zone1">
		<span class="pl-ead-did-isbd-zone1">
			<xsl:apply-templates select="unittitle/text() | unittitle/sdx:hilite"/>
		</span>
	</xsl:template>
	<!-- Zone 2 Edition -->
	<xsl:template name="isbd-zone2">
		<xsl:if test="unittitle/edition != ''">
			<span class="pl-ead-did-isbd-zone2">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="unittitle/edition" mode="isbd"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template match="unittitle/edition" mode="isbd">
		<xsl:choose>
			<xsl:when test="starts-with(normalize-space(.), '&#x2013; ')">
				<xsl:value-of select="substring-after(normalize-space(.), '&#x2013; ')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Zone 3 Précisions relatives au support -->
	<!-- 3.1 Données mathématiques (Ressources cartographiques) -->
	<xsl:template name="isbd-zone3.1">
		<xsl:if test="materialspec/materialspec[@label='Echelle' or @label='Projection'or @label='Coordonnées' or @label='Equinoxe'] != ''">
			<span class="pl-ead-did-isbd-zone3">
				<xsl:value-of select="$zone-sep"/>
				<span class="pl-ead-materialspec-ech"><xsl:apply-templates select="materialspec/materialspec[@label='Echelle']"/></span>
				<xsl:if test="materialspec/materialspec[@label='Projection'] != ''">
					<xsl:text>; </xsl:text>
					<span class="pl-ead-materialspec-proj"><xsl:apply-templates select="materialspec/materialspec[@label='Projection']"/></span>
				</xsl:if>
				<xsl:if test="materialspec/materialspec[@label='Coordonnées' or @label='Equinoxe'] != ''">
					<xsl:text>(</xsl:text>
						<span class="pl-ead-materialspec-coord"><xsl:apply-templates select="materialspec/materialspec[@label='Coordonnées']"/></span>
						<xsl:if test="materialspec/materialspec[@label='Coordonnées' and @label='Equinoxe'] != ''">
							<xsl:text>, </xsl:text>
						</xsl:if>
						<span class="pl-ead-materialspec-equinox"><xsl:apply-templates select="materialspec/materialspec[@label='Equinoxe']"/></span>
					<xsl:text>)</xsl:text>
				</xsl:if>
			</span>
		</xsl:if>
	</xsl:template>

  <xsl:template match="materialspec[@label='Echelle' or @label='Projection'or @label='Coordonnées' or @label='Equinoxe']">
    <xsl:value-of select="."/>
  </xsl:template>

	<!-- 3.3 Numérotation (Publications en série) -->
	<xsl:template name="isbd-zone3.3">
		<xsl:if test="unittitle/num">
			<span class="pl-ead-did-isbd-zone3">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="unittitle/num"/>
				<xsl:value-of select="$dot"/>
			</span>
		</xsl:if>
	</xsl:template>
	<!-- 3.4 Type et de la taille de la ressource électronique -->
	<xsl:template name="isbd-zone3.4">
		<xsl:if test="physdesc/extent != '' or physdesc/genreform[@source='liste-typedocAC'] != ''">
			<span class="pl-ead-did-isbd-zone3">
				<xsl:value-of select="$zone-sep"/>
				<xsl:for-each select="physdesc/genreform[@source='liste-typedocAC']">
					<xsl:if test="not(position() = 1) ">
						<i18n:text key="ead.did.isbd.and.sep"> et </i18n:text>
					</xsl:if>
					<xsl:apply-templates select="." mode="isbd"/>
				</xsl:for-each>
				<xsl:if test="physdesc/extent[@type='taille ressource électronique'] != ''">
					<span class="pl-ead-extent-ressel">
						<xsl:text> (</xsl:text>
						<xsl:apply-templates select="physdesc/extent[@type= 'taille ressource électronique']" mode="isbd"/>
						<xsl:text>)</xsl:text>
					</span>
				</xsl:if>
				<xsl:value-of select="$dot"/>
			</span>
		</xsl:if>
	</xsl:template>
	<!-- Zone 4 : Publication, diffusion -->
	<xsl:template name="isbd-zone4">
		<xsl:if test="unittitle/imprint != ''">
			<span class="pl-ead-did-isbd-zone4">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="unittitle/imprint" mode="isbd"/>
				<xsl:value-of select="$dot"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template match="imprint" mode="isbd"> <!-- (#PCDATA|ptr|extptr|emph|lb|publisher|geogname|date) -->
		<xsl:for-each select="geogname[. != '']">
			<xsl:if test="not(position() = 1)">
				<xsl:text> ; </xsl:text>
			</xsl:if>
			<span class="pl-ead-geogname"><xsl:apply-templates select="."/></span>
		</xsl:for-each>
		<xsl:if test="not(geogname != '') and publisher != ''">
			<i18n:text key="ead.did.isbd.imprint.no.geogname">[S.l.]</i18n:text>
		</xsl:if>
		<xsl:if test="publisher != ''">
			<xsl:text>&#160;: </xsl:text>
			<span class="pl-ead-publisher"><xsl:apply-templates select="publisher"/></span>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="not(geogname != '') and not(publisher != '')">
				<i18n:text key="ead.did.isbd.imprint.neither.geogpublisher">[S.l. : s.n.]</i18n:text>
			</xsl:when>
			<xsl:when test="not(publisher) and geogname != ''">
				<xsl:text>&#160;: </xsl:text>
				<i18n:text key="ead.did.isbd.imprint.no.publisher">[s.n.]</i18n:text>
			</xsl:when>
		</xsl:choose>
		<xsl:if test="date != ''">
			<xsl:text>, </xsl:text>
			<xsl:apply-templates select="date"/>
		</xsl:if>
	</xsl:template>
	<!-- Zone 5 : Collation -->
	<xsl:template name="isbd-zone5">
		<xsl:if test="physdesc/extent[@type='collation'] != ''">
			<span class="pl-ead-did-isbd-zone5">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="physdesc/extent[@type='collation']" mode="isbd"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template match="physdesc/genreform[@source='liste-typedocAC']" mode="isbd">
		<span class="pl-ead-genreform-typedocAC">
			<xsl:apply-templates select="."/>
		</span>
	</xsl:template>
	<xsl:template match="physdesc/genreform[not(@source='liste-typedocAC')]" mode="isbd">
		<xsl:apply-templates />
	</xsl:template>
	<xsl:template match="physdesc/extent" mode="isbd">
		<span class="pl-ead-extent">
			<xsl:choose>
				<xsl:when test="starts-with(., '&#x2013;')">
					<xsl:value-of select="normalize-space(substring-after(., '&#x2013;'))"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>
	<!-- Zone 6 : Collection -->
	<xsl:template name="isbd-zone6">
		<xsl:if test="unittitle/bibseries != ''">
			<span class="pl-ead-did-isbd-zone6">
				<xsl:value-of select="$zone-sep"/>
				<xsl:text>(</xsl:text>
				<xsl:apply-templates select="unittitle/bibseries" mode="isbd"/>
				<xsl:text>).</xsl:text>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template match="unittitle/bibseries" mode="isbd">
		<xsl:choose>
			<xsl:when test="starts-with(normalize-space(.), '&#x2013; (')">
				<xsl:value-of select="substring-after(normalize-space(.), '&#x2013; (')"/>
			</xsl:when>
			<xsl:when test="starts-with(normalize-space(.), '&#x2013; ')">
				<xsl:value-of select="substring-after(normalize-space(.), '&#x2013; ')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Zone 7 : Notes -->
	<xsl:template name="isbd-zone7">
		<xsl:if test="../odd/odd[@type='note isbd'] != ''">
			<span class="pl-ead-isbd-zone7 pl-ead-odd-note-isbd">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="../odd/odd[@type='note isbd']" mode="isbd"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template match="odd[@type='note isbd' or @type='document hôte']" mode="isbd">
		<xsl:for-each select="p">
			<xsl:value-of select="."/>
			<xsl:text> </xsl:text>
		</xsl:for-each>
	</xsl:template>
	<!-- Zone 8 : Numéro normalisé et modalités d'acquisition -->
	<xsl:template name="isbd-zone8">
		<xsl:if test="../odd/odd[@type='numéro identification et prix'] != ''">
			<span class="pl-ead-isbd-zone8 pl-ead-odd-num-prix">
				<xsl:value-of select="$zone-sep"/>
				<xsl:apply-templates select="../odd/odd[@type='numéro identification et prix']" mode="isbd"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template match="odd[@type='numéro identification et prix']" mode="isbd">
		<xsl:for-each select="p">
			<xsl:choose>
				<xsl:when test="starts-with(normalize-space(.), '&#x2013; ')">
					<xsl:value-of select="substring-after(normalize-space(.), '&#x2013; ')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text> </xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- on ne traite pas -->
	<xsl:template match="*" mode="isbd">
	</xsl:template>

</xsl:stylesheet>
