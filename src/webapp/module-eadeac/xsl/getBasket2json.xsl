<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Retourne les documents EAD contenus dans le porte-documents au format JSON
    | On peut alimenter une DataTable YUI avec.
    |
    | {"resultSet":
    |     {
    |       "returned":20,
    |       "total":543,
    |       "start":1,
    |       "sort":null,
    |       "dir":null,
    |       "results":[
    |         {"id":"FRDAFANCH00AP_000000002_e0000002","root-id":null,"ancestors":null,"fucomptitle":null,"udate":null,"uunitid":null},
    |         {"id":"FRDAFANCH00AP_000000002_e0000032","root-id":null,"ancestors":null,"fucomptitle":null,"udate":null,"uunitid":null},
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                xmlns:xsp="http://apache.org/xsp"
                xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                xmlns:pleade="http://pleade.org/ns/pleade/1.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                exclude-result-prefixes="xsl xsp sdx pleade fn">

	<xsl:param name="sort_field" select="''"/>
	<xsl:param name="sort_order" select="''"/>

  <xsl:param name="base" select="'ead'" />
  <xsl:variable name="_base">
    <xsl:choose>
      <xsl:when test="$base!=''"><xsl:value-of select="$base" /></xsl:when>
      <xsl:otherwise><xsl:text>ead</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!--+
      | Création de la racine XHTML
      +-->
  <xsl:template match="/sdx:document">
    <root>
      <resultSet>
        <xsl:apply-templates select="sdx:results" mode="resume" />
        <xsl:apply-templates select="sdx:results" mode="results" />
      </resultSet>
    </root>
  </xsl:template>

  <!--+
      | Le résumé de la recherche avec son résultat
      +-->
  <xsl:template match="sdx:results" mode="resume">
    <returned>
      <xsl:value-of select="( number(@end) - number(@start) ) + 1"/>
    </returned>
    <total>
      <xsl:value-of select="@nb"/>
    </total>
    <start>
      <xsl:value-of select="@start"/>
    </start>
    <end>
      <xsl:value-of select="@end" />
    </end>
    <page>
      <xsl:value-of select="@page" />
    </page>
    <pages>
      <xsl:value-of select="@pages" />
    </pages>
    <sort>
			<xsl:choose>
				<xsl:when test="$sort_field != ''">
					<xsl:value-of select="$sort_field"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="sdx:sort/sdx:field[1]/@name"/>
				</xsl:otherwise>
			</xsl:choose>
    </sort>
    <dir>
			<xsl:choose>
				<xsl:when test="$sort_order != ''">
					<xsl:value-of select="$sort_order"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="sdx:sort/sdx:field[1]/@order"/>
				</xsl:otherwise>
			</xsl:choose>
    </dir>
  </xsl:template>

  <!--+
      | Affichage des résultats de recherche
      +-->
  <xsl:template match="sdx:results" mode="results">
    <xsl:apply-templates select="/sdx:document/sdx:caddy" />
  </xsl:template>

  <!--+
      | Affichage d'un ensemble de documents appartenant à la même base
      | Ici, on construit une TABLE qui sera formatée ensuite par le module
      | Datatable de YUI
      +-->
  <xsl:template match="sdx:caddy">

    <results array="true">

    <!-- Au lieu de boucler sur les sdx:result directement, on boucle sur
    les sdx:document du panier. Cela permet de trier les docs par ordre
    d'entrée dans le panier -->
    <xsl:for-each select="sdx:document[@base=$_base or not(@base) or @base='']">
      <xsl:sort select="@time" data-type="number" order="descending" /><!-- Tri sur la date d'arrivée du doc dans le panier -->
        <xsl:variable name="id" select="@id" />
        <xsl:variable name="time" select="@time" />
        <xsl:apply-templates select="/sdx:document/sdx:results/sdx:result[sdx:field[@name='sdxdocid']/@value=$id]" mode="result">
          <!-- Une fois trier par ordre d'arrivée dans le panier, on affine le tri suivant le titre, la date, la cote -->
          <xsl:sort select="sdx:field[@name='fucomptitle'][1]" />
          <xsl:sort select="sdx:field[@name='udate'][1]" />
          <xsl:sort select="sdx:field[@name='unitid'][1]" />
          <xsl:with-param name="time" select="$time" />
        </xsl:apply-templates>
    </xsl:for-each>

    </results>

  </xsl:template>

  <!--+
      | Construction d'une ligne de résultat pour les documents EAD
      +-->
  <xsl:template match="sdx:result[sdx:field[@name='sdxdbid']/@value=$_base]" mode="result">
    <xsl:param name="time" select="''" />
    <result>
      <time><xsl:value-of select="$time" /></time>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'sdxdbid'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'sdxdocid'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'root-id'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'ancestors'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'fucomptitle'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'udate'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'uunitid'" />
      </xsl:apply-templates>
    </result>
  </xsl:template>

  <!--+
      | Construction d'une cellule de résultat
      +-->
  <xsl:template match="sdx:result" mode="build-cell">
    <xsl:param name="n" select="''" />
    <xsl:choose>
      <xsl:when test="$n=''"/>
      <!-- Un cas particulier quand on veut la liste des ancêtres -->
      <xsl:when test="$n='ancestors'">
        <ancestors>
          <!--<xsl:if test="sdx:field[@name='ctitle' and @value!='']">-->
            <xsl:for-each select="sdx:field[@name='ctitle']">
              <xsl:sort select="@value" order="ascending" />
              <xsl:value-of select="substring-after( @value, '-' )" />
              <xsl:if test="position()!=last()">
								<i18n:text key="basket.results.item.context.sep">  &gt;  </i18n:text>
              </xsl:if>
            </xsl:for-each>
          <!--</xsl:if>-->
        </ancestors>
      </xsl:when>
      <xsl:when test="$n='root-id'">
        <rootId>
          <xsl:value-of select="sdx:field[@name=$n]/@escapedValue"/>
        </rootId>
      </xsl:when>
      <!-- Cas générique, on prend la valeur du sdx:field demandé -->
      <xsl:otherwise>
        <xsl:element name="{$n}">
          <!--<xsl:if test="sdx:field[@name=$n and .!='']">-->
            <xsl:apply-templates select="sdx:field[@name=$n]" />
          <!--</xsl:if>-->
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
