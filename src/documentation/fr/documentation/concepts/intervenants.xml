<?xml version="1.0"?>
<?xml-stylesheet href="../../../_config/docbook.css" type="text/css"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "../../../_config/docbookx.dtd">
<article lang="fr"> 
  <title>Les intervenants</title> 
  <titleabbrev>Intervenants</titleabbrev> 
  <articleinfo> 
	 <author><firstname>Martin</firstname><surname>Sévigny</surname> 
		<affiliation> 
		  <orgname>AJLSM</orgname> 
		</affiliation> 
		<email>sevigny@ajlsm.com</email> 
	 </author> 
	 <abstract> 
		<para>Différents groupes d'intervenants peuvent être impliqués à un
		  moment ou un autre dans l'utilisation, la configuration ou le développement de
		  PLEADE. Ce document présente ces différents groupes et leurs
		  préoccupations.</para> 
	 </abstract><date role="modification">2003-11-21</date> 
  </articleinfo> 
  <para>PLEADE est plus qu'un logiciel, il s'agit d'un projet de logiciel
	 libre, avec une communauté d'intervenants qui devrait grandir avec le temps.
	 Dans ce document, nous présentons les différentes catégories d'intervenants
	 potentiels pour PLEADE, afin de montrer que tout la monde a sa place dans cette
	 communauté.</para> 
  <para>Nous vous rappelons que si vous êtes intéressés à contribuer d'une
	 manière ou d'un autre à PLEADE, vous êtes invités à joindre la liste
	 <emphasis role="italic"><ulink
	 url="http://sourceforge.net/mailarchive/forum.php?forum_id=33259">pleade-users</ulink></emphasis>
	 et à y présenter votre motivation ou vos intérêts.</para> 
  <section>
	 <title>Les utilisateurs</title>
	 <para>Les utilisateurs jouent bien sûr un rôle clé dans le projet PLEADE.
		Nous pouvons distinguer différents types d'utilisateurs ainsi que différentes
		responsabilités par rapport au projet.</para>
	 <variablelist>
		<varlistentry><term>Lecteurs</term>
		  <listitem>
			 <para>Les lecteurs sont les personnes qui consultent les instruments
				de recherche publiés à l'aide de PLEADE. La plupart du temps, ces lecteurs ne
				seront pas conscients de la présence de PLEADE, mais pour certains une certaine
				habitude sera prise et ils pourront devenir des intervenants intéressants pour
				le projet, par leurs remarques sur l'utilisation de l'outil.</para>
		  </listitem>
		</varlistentry>
		<varlistentry><term>Editeurs de contenus</term>
		  <listitem>
			 <para>Les éditeurs de contenus sont ceux qui vont alimenter un site
				PLEADE. Ils sont en général responsable de l'édition des instruments de
				recherche dans leur institution, et ils ont les droits nécessaires dans leur
				installation PLEADE pour ajouter des documents, les supprimer, etc.</para>
		  </listitem>
		</varlistentry>
		<varlistentry><term>Administrateurs</term>
		  <listitem>
			 <para>Les administrateurs sont les personnes qui vont installer et
				paramétrer PLEADE pour une institution. Ils jouent un peu le rôle d'un
				<emphasis role="italic">webmestre</emphasis> pour le site créé avec
				PLEADE.</para>
		  </listitem>
		</varlistentry>
	 </variablelist>
	 <para>Veuillez noter que la documentation technique de PLEADE que vous
		consultez actuellement s'adresse plus particulièrement aux éditeurs de contenus
		et aux administrateurs.</para>
  </section>
  <section> 
	 <title>Les commanditaires</title> 
	 <para>Les commanditaires sont les personnes morales ou physiques qui
		investissent des fonds dans le développement ou la promotion de PLEADE. Même si
		plusieurs activités peuvent être réalisées sous la forme de volontariat, il y a
		certains efforts, comme les développements initiaux ou d'envergure,
		l'organisation d'événements, qui ne peuvent être effectués dans un
		investissement financier.</para> 
	 <para>Pour l'instant, ce sont les sociétés AJLSM et Anaphore qui ont
		financé le développement de PLEADE. Soulignons toutefois la généreuses
		contribution du Centre historique des Archives nationales (CHAN, France) pour
		l'organisation de la journée d'information et de lancement qui a eu lieu le 7
		octobre 2003.</para> 
	 <para>Il est tout à fait possible de devenir commanditaire de PLEADE en
		indiquant de quelle manière vous souhaitez que les fonds soient
		dépensés.</para> 
  </section> 
  <section> 
	 <title>Les développeurs</title> 
	 <para>Plusieurs personnes croient que les développeurs sont les personnes
		clés dans projets de logiciels libres. Cela peut être vrai dans certains cas,
		mais bien souvent ces derniers jouent un rôle moins important que bien d'autres
		intervenants, notamment les personnes impliquées dans la promotion.</para> 
	 <para>Les développeurs veillent au développement du code informatique, à sa
		documentation, et à sa maintenance corrective. Même s'il faut posséder un
		minimum de compétences techniques pour en faire partie, il y a certaines
		opérations qui peuvent s'avérer à la portée d'un plus grand nombre, notamment
		les tâches reliées à l'aspect visuel de l'outil.</para> 
  </section> 
  <section> 
	 <title>Les contributeurs</title> 
	 <para>Les contributeurs sont toutes les personnes qui apportent quelque
		chose au projet sans que ce soit du développement informatique pur et dur. Ce
		groupe d'intervenants est peut-être le plus important pour la survie à long
		terme d'un projet de logiciel libre. Qui plus est, c'est probablement le groupe
		où l'on retrouve les compétences les plus variées, ce qui devrait avoir pour
		effet d'attirer un grand nombre de candidats.</para> 
	 <para>Les contributeurs sont amenés à apporter un grand nombre de choses au
		projet, tel que :</para> 
	 <itemizedlist> 
		<listitem> 
		  <para>La traduction des outils ou de la documentation</para> 
		</listitem> 
		<listitem> 
		  <para>De la documentation utilisateurs, voire technique</para> 
		</listitem> 
		<listitem> 
		  <para>Des tutoriels</para> 
		</listitem> 
		<listitem> 
		  <para>Des exemples d'utilisation</para> 
		</listitem> 
		<listitem> 
		  <para>De la formation</para> 
		</listitem> 
		<listitem> 
		  <para>De l'aide en ligne</para> 
		</listitem> 
	 </itemizedlist> 
	 <para>Ce ne sont que des exemples, mais qui illustrent bien la diversité
		des compétences. D'ailleurs, nous isolons un tel groupe pour bien les démarquer
		des développeurs, afin que le plus grand nombre ait envie de contribuer à un
		projet comme PLEADE.</para> 
  </section> 
  <section> 
	 <title>Les intégrateurs</title> 
	 <para>Les intégrateurs sont les personnes ou le plus souvent sociétés qui
		vont proposer des services autour de l'outil. Ces services peuvent être des
		prestations d'installation, de la formation, des développements spécifiques, du
		paramétrage, etc.</para> 
	 <para>Il s'agit d'un groupe important car il est directement en contact
		avec les utilisateurs (qu'ils soient administrateurs, éditeurs de contenus ou
		lecteurs), et il peut ainsi faire remonter les remarques et les demandes auprès
		des responsables du projet.</para> 
  </section> 
</article> 
