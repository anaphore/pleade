<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  xmlns:saxon="http://saxonf.sf.net"
  exclude-result-prefixes="xsl fn saxon"
>

  <!-- Transformation d'identité. A surcharger si les documents EAD doivent être
      modifiés à l'entrée, avant publication.
  -->

  <xsl:param name="debug" select="'false'"/>

  <xsl:template match="/ead">
    <xsl:message></xsl:message>
    <xsl:message>Indexation de <xsl:value-of select="eadheader/eadid"/></xsl:message>
    <xsl:message>Debut de l'analyse : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
    <xsl:if test="eadheader/eadid = ''"><xsl:message terminate="yes">    Erreur: Ce document ne possède pas d'identiant (L'élément eadid est vide). Pleade ne peut donc pas le publier...</xsl:message></xsl:if>
    <xsl:choose>
      <xsl:when test="$debug='true'">
        <xsl:variable name="result">
          <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
          </xsl:copy>
        </xsl:variable>
        <xsl:copy-of saxon:ead-once="yes" select="$result"/>
        <xsl:if test="$debug='true'">
          <xsl:message>  Le mode debogage est active ; le document sera copie apres chaque transformation dans le dossier : <xsl:value-of select="concat(sys:getProperty('java.io.tmpdir'),'/',eadheader/eadid)" xmlns:sys="java:java.lang.System"/></xsl:message>
          <xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{eadheader/eadid}/{eadheader/eadid}-1.xml"
            xmlns:sys="java:java.lang.System">
            <xsl:copy-of saxon:read-once="yes" select="$result"/>
          </xsl:result-document>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- On copie tout -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
