//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer l'arbre du sommaire dans la fenêtre EAD
 * @author Julien Huet
 */
/**
 * @class Classe permettant de créer l'arbre du sommaire dans la fenêtre EAD
 * @augments TreeView
 * @author Julien Huet
 */
var TocTreeView = new Class(/** @lends TocTreeView.prototype */{
	Extends: TreeView,
	
	/**
	 * <strong>Constructor</strong><br/> 
	 * @param {EadWindow} eadWindow
	 */
	initialize: function(eadWindow){
		this.eadWindow = eadWindow;

		if( !$('pl-pgd-toc') ) {
			return false;
		}

		var _eadid = this.eadWindow.getContentManager().getEadId();
		var _archdescID = this.eadWindow.getContentManager().getArchdescID();
		var _base = this.eadWindow.getContentManager().getBase();
		
		var url = 'toc/' + _eadid + '.ajax-html'; // Mode statique
		var urlStart = 'functions/' + _base + '/get-toc-fragment/' + _eadid + '/';
		var urlEnd = '.ajax-html';

		if ( this.eadWindow.getContentManager().getMode() == 'dyn' ) {
			url = urlStart + _eadid + urlEnd 
		}
		
		if (eadWindow.getfragmentID(_archdescID,true) != _archdescID) {
			url += '?c=' + eadWindow.getfragmentID(_archdescID,true) ;
		}

		// appel au constructeur parent
		this.parent(url,$('pl-pgd-toc'),urlStart,urlEnd,eadWindow.getContentManager(),$('tabs'));

		this.noTreeMessage = 'La table des matières est vide.'; //TODO: i18n - check

		// surcharge de la methode  setHtmlNodeEvent qui permet de rajouter un evement html
		this.getTree().setHtmlNode = function(child){
			if ( child.getNodeLink() ) {
				child.getNodeLink().setProperty('onclick','return false');
			}
			return child.getHtml();
		};
		
		if ( eadWindow.getfragmentID(_archdescID, true) != _archdescID ) {
			this.setNodeIdToFocus(eadWindow.getfragmentID(_archdescID, true));
		}
	},

	/**
	* Fonction appelée lorsque l'on clique sur le noeud de la TOC
	* @param {Mif.Tree.Node} node
	*/
	onSelect: function(node){
		this.treeManager.loadContent(node.data.id, false, qid); // qid variable globale déclarée dans ead2html-index.xsl (peut etre vide)
	}
});