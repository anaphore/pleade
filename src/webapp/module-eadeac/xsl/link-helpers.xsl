<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
>

	<!-- Fonctions utiles pour les liaisons vers des ressources
			externes. -->

	<!-- Des fonctions générales, notamment pour résoudre des liens -->
	<xsl:import href="../../commons/xsl/functions.xsl"/>

	<!-- Une fonction qui permet de savoir si un élément est un lien -->
	<xsl:function name="pleade:is-link" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:value-of select="boolean($el/self::ref or $el/self::refloc
																	or $el/self::ptr or $el/self::ptrloc
																	or ($el/self::archref[@href!='' or @xpointer!=''])
																	or $el/self::extref or $el/self::extrefloc or $el/self::extptr or $el/self::extptrloc)
																	or $el/self::bibref[@href!='' or @xpointer != '']"/>
	</xsl:function>

	<!-- Une fonction qui permet de savoir si un élément est un object numérique -->
	<xsl:function name="pleade:is-digital-object" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:value-of select="boolean($el/self::dao or $el/self::daoloc)"/>
	</xsl:function>

	<!-- Une fonction qui permet de savoir si une cible d'un lien doit être considérée comme
			un document attaché -->
	<xsl:function name="pleade:is-att" as="xs:boolean">
		<xsl:param name="href"/>					<!-- La cible du lien -->
		<xsl:param name="attached-extensions"/>		<!-- La liste des extensions à considérer comme des fichiers attachés -->
		<!-- On déterminer d'abord l'extension -->
		<xsl:variable name="ext" select="pleade:get-extension($href)"/>
		<!-- On retourne vrai si l'extension est dans la liste -->
		<xsl:value-of select="boolean($attached-extensions[. = $ext])"/>
	</xsl:function>

	<!-- Une fonction qui retourne l'adresse publique (visible par le navigateur) d'un lien -->
	<xsl:function name="pleade:get-public-url" as="xs:string">
		<xsl:param name="el"/>	<!-- L'élément de lien -->
		<xsl:param name="t"/>	<!-- Le type de lien, si vide on prend @pleade:link-type -->
    <xsl:param name="feadid"/> <!-- L'eadid -->
		<xsl:variable name="type">
			<xsl:choose>
				<xsl:when test="$t = ''"><xsl:value-of select="$el/@pleade:link-type"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="$t"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="return">
			<xsl:choose>
				<!-- Les liens internes : on aura seulement à mettre à jour le div -->
				<xsl:when test="$type = 'internal'">
					<xsl:value-of select="concat('ead.html?c=', $el/@pleade:target-fragment)"/>
					<xsl:if test="$el/@pleade:target-fragment != $el/@pleade:target-id">
						<xsl:value-of select="concat('#', $el/@pleade:target-id)"/>
					</xsl:if>
				</xsl:when>
				<!-- Les liens vers d'autres documents EAD -->
				<xsl:when test="$type = 'ead'">
					<!-- On doit trouver l'EADID de la cible -->
					<xsl:variable name="target-eadid" select="pleade:get-url-file-wo-extension($el/@pleade:url)"/>
					<xsl:value-of select="concat('ead.html?id=', $target-eadid)"/>
					<xsl:if test="contains($el/@pleade:url, '#')">
						<xsl:value-of select="concat('&amp;c=', $target-eadid, '_', substring-after($el/@pleade:url, '#'))"/>
					</xsl:if>
				</xsl:when>
				<!-- Les liens vers des URL absolues -->
				<xsl:when test="$type = 'absolute'">
					<xsl:value-of select="$el/@pleade:url"/>
				</xsl:when>
				<!-- Les liens vers des documents attachés -->
				<xsl:when test="$type = 'attached'">
					<xsl:value-of select="concat('functions/ead/attached/', $feadid, '/', $el/@pleade:id, '.', $el/@pleade:ext)"/>
				</xsl:when>
				<!-- Les liens relatifs -->
				<xsl:when test="$type = 'relative'">
					<xsl:value-of select="concat('functions/ead/relative/', $el/@pleade:url)"/>
				</xsl:when>
				<!-- Les fichiers multimédia stockés-->
				<xsl:when test="$type = 'detached'">
					<xsl:value-of select="concat('functions/ead/detached/', $el/@pleade:url)"/>
				</xsl:when>
				<!-- Les liens relatifs gérés par le serveur d'images -->
				<xsl:when test="$type = 'img-server'">
					<xsl:value-of select="concat('functions/ead/img-server/', $el/@pleade:url)"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$return"/>
	</xsl:function>

</xsl:stylesheet>
