//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer facilement requetes ajax en mootools
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Classe permettant de créer facilement requetes ajax en mootools
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var AjaxRequestHandler = new Class(/** @lends AjaxRequestHandler.prototype */{
	
	/**
	* Charge une URL dans un div.
	* @param {String} url url utilisé pour la requete ajax
	* @param {Element} div Element que l'on souhaite rafraichir
	* @param {Function} onRequestFn
	* @param {Function} onSuccessFn
	* @param {String} loadMethod Méthode pour l'animation de chargement. 'message' pour l'affachage en ligne, rien pour l'affichage normal, centré sur la page
	*/
	loadAjaxContent: function(url, div, onRequestFn, onSuccessFn, loadMethod) {
		var request = $empty;
		var success = $empty;
		var loading = null;
		if (loadMethod == 'message') {
			loading = new LoadingMessage(div);
		} else {
			loading = new Loading(div);
		}
		loading.start();
		
		if ( onRequestFn ) {
			request = onRequestFn;
		}
		if ( onSuccessFn ) {
			success = onSuccessFn;
		}
		/** FIXME: on passe en GET à cause de problèmes d'encodage avec Firefox (3.6?) 
		Comme on ne sait pas quel type d'URL on peut avoir, ce n'est pas une idée lumineuse cependant... */
		_method = 'get';
		if ( Browser.Engine.trident ) {
			_method = 'post';
		}

		var req = new Request.HTML({
			method: _method,
			url : url,
			evalResponse: true,
			update: $(div),
			onSuccess: function(){
				success();
				loading.stop();
			},
			onRequest: request
		});
		req.send();
	}
});
