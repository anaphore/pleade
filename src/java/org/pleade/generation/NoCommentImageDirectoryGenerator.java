/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.generation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Stack;

import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.ResourceNotFoundException;
import org.apache.cocoon.generation.DirectoryGenerator;
import org.pleade.utils.ImageInformation;
import org.pleade.utils.ImageUtilities;
import org.xml.sax.SAXException;

/**
 * Une legere variation du ImageDirectoryGenerator de Cocoon pour
 * ne pas sortir les commentaires dans le flux XML.
 */
public class NoCommentImageDirectoryGenerator extends DirectoryGenerator {

    /** Le nom de l'attribut pour la largeur */
    protected final static String IMAGE_WIDTH_ATTR_NAME = "width";
    /** Le nom de l'attribut pour la hauteur */
    protected final static String IMAGE_HEIGHT_ATTR_NAME = "height";
    /** Le nom de l'attribut pour le DPI */
    protected final static String IMAGE_DPI_ATTR_NAME = "dpi";
    /** Le nom de l'attribut pour le MIME Type */
    protected final static String IMAGE_MIMETYPE_ATTR_NAME = "mimetype";

    /** Le debut d'une URI pour des fichiers. */
    private static final String FILE = "file:";

    /**
     * Ajout les attributs specifiques aux images dans la sortie XML.
     */
    protected void setNodeAttributes(File path) throws SAXException {
        
        // D'abord les informations generales du DirectoryGenerator (date, ...)
        super.setNodeAttributes(path);
        
        // Si c'est un dossier alors on ne fait rien de special
        if (path.isDirectory()) {
            return;
        }
        
        
        try {
            FileInputStream fis = new FileInputStream(path);
            
            // On appelle maintenant la methode qui fournit les informations sur une image
            ImageInformation iInfo = ImageUtilities.getImageInformation(objectModel, getLogger(), path.getAbsolutePath(), fis);
            if ( fis != null ) fis.close();
            if ( iInfo != null ) {
                // Maintenant on peut sortir les attributs si on a les informations
                if ( iInfo.getWidth() != 1 ) attributes.addAttribute("", IMAGE_WIDTH_ATTR_NAME, IMAGE_WIDTH_ATTR_NAME, "CDATA", "" + iInfo.getWidth());
                if ( iInfo.getHeight() != 1 ) attributes.addAttribute("", IMAGE_HEIGHT_ATTR_NAME, IMAGE_HEIGHT_ATTR_NAME, "CDATA", "" + iInfo.getHeight());
                if ( iInfo.getDpi() != 1 ) attributes.addAttribute("", IMAGE_DPI_ATTR_NAME, IMAGE_DPI_ATTR_NAME, "CDATA", "" + iInfo.getDpi());
                if ( iInfo.getMimetype() != null ) attributes.addAttribute("", IMAGE_MIMETYPE_ATTR_NAME, IMAGE_MIMETYPE_ATTR_NAME, "CDATA", iInfo.getMimetype());
            }
        }
        catch (IOException e) {
            getLogger().error("Probleme d'acces a l'image." + e);
        }
        
    }
    
    /**
     * Lance la sortie des informations.
     * On a surcharge cette methode pour que la date de modification du dossier source
     * soit prise en compte pour la cache.
     */
    public void generate() throws SAXException, ProcessingException {
        // Sauf la ligne commentee ci-dessous, tout le contenu est issu du DirectoryGenerator
        try {
            String systemId = this.directorySource.getURI();
            if (!systemId.startsWith(FILE)) {
                throw new ResourceNotFoundException(systemId + " does not denote a directory");
            }
            // This relies on systemId being of the form "file://..."
            File directoryFile = new File(new URL(systemId).getFile());
            if (!directoryFile.isDirectory()) {
                throw new ResourceNotFoundException(super.source + " is not a directory.");
            }
            
            // On ajoute le dossier dans la liste des objets pour la cache
            if (this.validity != null) this.validity.addFile(directoryFile);

            this.contentHandler.startDocument();
            this.contentHandler.startPrefixMapping(PREFIX, URI);

            Stack ancestors = getAncestors(directoryFile);
            addAncestorPath(directoryFile, ancestors);

            this.contentHandler.endPrefixMapping(PREFIX);
            this.contentHandler.endDocument();
        } catch (IOException ioe) {
            throw new ResourceNotFoundException("Could not read directory " + super.source, ioe);
        }
    }

 }
