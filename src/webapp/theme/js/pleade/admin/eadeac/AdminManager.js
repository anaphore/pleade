//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du cadre de classement dans la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion du cadre de classement dans la partie admin
 * @author Julien Huet
 * @augments HelpPanels
 */
var AdminManager = new Class(/** @lends AdminManager.prototype */ {
    Implements: [HelpPanels],

    /**
     * Caractère de séparation utilisé pour les chemins de fichier
     * @private
     */
    filesep: '/',

    /**
     * Backup
     * @private
     */
    bckps: '',

    /**
     * <strong>Constructor</strong><br/> Initialisation du formulaire pour la gestion du cadre de classement
     * @param {Object} bckps
     * @param {boolean} b_tabs
     * @param (Object) ti
     */
    initialize: function(bckps, b_tabs, ti, cp_cur){
        this.initHelpPanels();
        this.bckps = bckps;
        if (b_tabs) {
            this.tabs = this.initTabs(ti, cp_cur);
        }
    },

    /**
     * Initialisation de la tabulation YUI
     * @param {Object} ti
     * @param {Object} cp_cur
     */
    initTabs: function(ti, cp_cur){
        var out = new YAHOO.widget.TabView('tabs');

        if ($type(ti) == 'number') {
            try {
                if (ti > 0) {
                    out.set('activeIndex', ti - 1);
                }
            } catch (e) {}
        }

        if ($type(ti) == 'string') {
            try {
                if ((ti == 'last') || cp_cur == 'addfile') {
                    out.set('activeIndex', out._tabParent.childElementCount - 1);
                }
            } catch (e) {}
        }
        return out;
    },

    /**
     * Fonction permettant de récupérer un fichier depuis le control et le répertoire passé en paramètre.
     * Cette fonction permet tout simplement de construire la bonne URL pour récupérer le fichier.
     * @param {String} control ID du control dont on souhaite récupérer la valeur
     * @param {String} dir Repertoire dans lequel on va aller chercher le fichier sur le serveur
     */
    url2getFile: function(control, dir){
        if ( control == null ) {
            return false;
        }

        var mvalue = null;
        //var ctrl = control;
        if (control) {
            mvalue = control.get('value');
        };
        if (mvalue == null || mvalue.blank()) {
            alert(_usersMessages.ead_admin_no_file_selected);
            return false;
        }
        window.location.href = '../download/' + dir + ((dir.endsWith(filesep)) ? '' : '/') + mvalue;
    },

    /**
     * Fonction permettant de controler la restauration de fichier backup
     * @param {Object} mvalue
     */
    ctrlBckp: function(control){
        var mvalue = null;
        //var ctrl = $(control);
        if (control) {
            mvalue = control.get('value');
        };
        //alert('CdcManager.xsl ctrlBckp');
        if (mvalue == null || mvalue.blank()) {
            alert(_usersMessages.ead_admin_no_file_selected);
            return false;
        }
        var bv, bd, ret;
        $A(bckps).each(function(o, i){
            if (o == null) 
                ;
            else 
                if (o[0] == (mvalue + '.backup')) {
                    bv = mvalue + '.backup';
                    bd = o[1];
                    ret = true;
                    return;
                }
        });
        if (ret == true) {
            _msg = (bd != null) ? _usersMessages.ead_admin_back2bakup_of.replace(/%s/, bd) : _usersMessages.ead_admin_back2bakup;
            if ( confirm(_msg) ) {
                return ret;
            } else {
                return false;
            }
        } else {
            alert(_usersMessages.ead_admin_no_backup_available);
            return false;
        }
    },
	
    /**
     * Verification du formulaire pour le CDC manager
     * @param {Element} mform Formulaire que l'on souhaire controler
     * @param {String} action Action que l'on souhaite effecuer. Soit 'newfile', 'addfile'
     * @param {Object} current
     * @param {Integer} tabindex Index de tabulation du formulaire que l'on traite
     */
    form2work: function(mform, action, current, tabindex){
        if (mform == null || action == null || action.blank() ||
        current == null) 
            return false;
        var maction, mcurrent, mnewfile, mtabindex;
        maction = mform.elements['action'];
        mcurrent = mform.elements['current'];
        mtabindex = mform.elements['tabindex'];
        if (maction == null || maction.value == null ||
        mcurrent == null ||
        mcurrent.value == null) 
            return false;
        maction.value = action;
        mcurrent.value = current;
        if (action == 'newfile' || action == 'addfile') {
            //<!-- Pour installer un nouveau fichier, il faut l'URL du nouveau fichier -->
            var npt = $('newfile' + current);
            if (npt == null || npt.value == null || npt.value.blank()) {
              alert(_usersMessages.ead_admin_no_file_selected);
              return false;
            }
            var ds = $('doc-select');

            if (action == 'newfile') {
              if (ds != null) {
                var sel = $('doc-select').getSelected()[0]['value'];
                if ( sel == '') {
                  alert(_usersMessages.ead_admin_no_file_selected);
                  return false;
                } else {
                  mcurrent.value = sel;
                }
              }
            }

            if (action == 'addfile') {
              var reg = npt.value.substring(npt.value.lastIndexOf(filesep) + 1);
              var ds = $('dir-select');
              if (ds != null) {
                if (!ds.getSelected()[0]['value']) {
                  alert(_usersMessages.ead_admin_no_subdir_selected);
                  return false;
                } else {
                  mcurrent.value = ds.getSelected()[0]['value'] + filesep + npt.value;
                }
              }
            }
            npt.name = 'newfile';
        }
        if (tabindex == null || tabindex == '' ||
        mtabindex == null ||
        mtabindex.value == null) 
            ;
        else {
          mtabindex.value = tabindex
        }
        return true;
    },

    /**
     * Fonction permettant de supprimer un fichier
     * @param {Element} mform Forumlaire utilisé pour la suppression de fichier
     */
    deleteFile: function(mform){
        var v = $('doc-select').get('value');
        var out = false;
        if (v == '') {
            alert(_usersMessages.ead_admin_no_file_selected);
        }
        else 
            if ( confirm(_usersMessages.ead_admin_delete_file_confirm_text + '\'' + v + '\'') ) {
                var ret = this.form2work(mform, 'deletefile', v, 0);
                out = ret;
            }
        return out;
    }
});
