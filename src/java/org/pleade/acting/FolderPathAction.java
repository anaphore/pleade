/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.acting;

import java.io.File;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.acting.ServiceableAction;
import org.apache.cocoon.environment.Redirector;
import org.apache.cocoon.environment.SourceResolver;

public class FolderPathAction extends ServiceableAction implements Configurable {

    // Des variables statiques pour identifier les parametres (d'action ou de configuration)
		// Les racines dans lesquelles le serveur d'images va rechercher l'image demandee
    private static final String CONFIGURATION_RACINES = "racines";
    // La meme chose que ci-dessus mais passe en parametre de l'action, pas en configuration
    private static final String PARAMETRE_RACINES = "racines";
    // Le parametre passe a l'action : le chemin relatif de l'image demandee
    private static final String PARAMETRE_CHEMIN_RELATIF = "chemin-relatif";
    // Les 2 valeurs ci-dessous sont renvoyees par l'action dans la sitemap
    // Le chemin absolue de l'image
    private static final String PARAMETRE_CHEMIN_BASE = "chemin-base";
    // L'URL absolue de l'image
    private static final String PARAMETRE_URL_BASE = "url-base";

    // La liste des racines, calculee au moment de la configuration
    private File[] racines;
    
    private boolean ldebug = false;
    private Logger logger = null;

    /**
     * Configuration de l'action, avec verification des racines fournies.
     */
    public void configure(Configuration conf) throws ConfigurationException {
    		logger = getLogger();
    		ldebug = logger.isDebugEnabled(); 
        // On s'assure qu'on a un parametre
        Configuration tempConf = conf.getChild(CONFIGURATION_RACINES);
        if ( tempConf == null ) throw new ConfigurationException("L'action \"chemin-dossier\" doit avoir un parametre \"racines\". Veuillez verifier le fichier parametres.xconf.");
        String tempRacines = tempConf.getValue();
        if ( tempRacines == null ) throw new ConfigurationException("L'action \"chemin-dossier\" doit avoir un parametre \"racines\". Veuillez verifier le fichier parametres.xconf.");
        if (ldebug) logger.debug("Racines : " + tempRacines);

        // On va chercher les dossiers depuis le parametre
        this.racines = getRootDirectories(tempRacines);
        if (this.racines == null) throw new ConfigurationException("Le parametre \"racines\" de l'action \"chemin-dossier\" n'identifie pas un ou des dossiers valides.");

        // La ca devrait etre bon
        if (ldebug) logger.debug("Nombre de racines : " + racines.length);
    }

    /**
     * L'action elle-meme, les parametres "chemin-base" et "url-base" seront retournes au sitemap.
     */
    public Map act(Redirector redirector, SourceResolver resolver, Map objectModel, String src, Parameters parameters) throws Exception {

        // On verifie le chemin demande, s'il n'est pas fourni ou vide, alors on retourne une "erreur"
        String chemin = parameters.getParameter(PARAMETRE_CHEMIN_RELATIF);
        if (ldebug) logger.debug("Chemin relatif demande : " + chemin.toString());
        if (chemin == null || chemin.trim().equals(""))
            return null;

        // Calcul les racines qui seront utilisees : celles passees en parametre ou celles definies en configuration
        String pracines = parameters.getParameter(PARAMETRE_RACINES, null);
        File[] _racines = (pracines==null && !"".equals(pracines)) ? racines : getRootDirectories(pracines);
        if (ldebug) { 	
        	StringBuffer sb = new StringBuffer();
        	for(int i=0, l=_racines.length; i<l; i++){
        		sb.append(((File)_racines[i]).toString());
        	}
        	logger.debug("Racines utilisees : " + sb);
        	sb = null;
        }
        if (_racines==null || _racines.length==0) {
        	_racines = racines;
        	if(logger.isErrorEnabled()) logger.error("Les racines passees en parametre de l'action sont nulles ou vides: "+pracines);
        }
        pracines = null;

        // On boucle sur les racines pour trouver la premiere qui contient le chemin relatif
        for (int i = 0; i < _racines.length; i++) {
            // On construit un objet File qui pourrait pointer sur le dossier recherche
            File file = new File(_racines[i], chemin);
            if (file.exists() && file.isDirectory() && file.canRead()) {
                // On l'a trouve, donc on ajoute les parametres de sortie
                objectModel.put(PARAMETRE_CHEMIN_BASE, file.getCanonicalPath());
                objectModel.put(PARAMETRE_URL_BASE, file.toURI().normalize().toString());
                if (ldebug) logger.debug("Chemin absolu : " + file.getCanonicalPath().toString());
                if (ldebug) logger.debug("URL absolue : " + file.toURI().normalize().toString().toString());

                // Et on retourne quelque chose
                return objectModel;
            }
        }

        // Si on est ici, cela signifie que le dossier n'a pas ete trouve, donc on retourne une "erreur"
        return null;
    }

    /**
     * Retoure la liste des dossiers racines.
     * Les dossiers ne sont pas verifies pour leur existance, etc.
     * @param racines   Les racines sous la forme d'une chaine de caracteres, separees par FILE.PATH_SEPARATOR.
     * @return  La liste des racines, ou null s'il y a un probleme.
     */
    private File[] getRootDirectories(String racines) {

        // On recupere les parties
        String parties[] = splitRoots(racines);

        // S'il y a eu erreur on retourne null
        if (parties == null) return null;

        // On fabrique le tableau de sortie
        File ret[] = new File[parties.length];
        for (int i = 0; i < parties.length; i++) ret[i] = new File(parties[i]);

        // Que l'on retourne
        return ret;
    }

    /**
     * Separe la liste des racines en fonction du File.pathSeparator.
     * @param racines   La liste des racines.
     * @return          La liste des racines sous la forme d'un tableau de String
     */
    private String[] splitRoots(String racines) {

        // On s'assure d'avoir un contenu valide.
        if (racines == null) return null;

        // On separe la chaine de caracteres
        StringTokenizer stringtokenizer = new StringTokenizer(racines, File.pathSeparator);

        // On fabrique le tableau de sortie
        String ret[] = new String[stringtokenizer.countTokens()];
        for (int i = 0; stringtokenizer.hasMoreTokens(); i++) ret[i] = stringtokenizer.nextToken();

        // Que l'on retourne...
        return ret;
    }

}
