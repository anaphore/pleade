<?xml version="1.0" encoding="UTF-8"?>
<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" version="1.0" sdx:copy="for namespace">

	<xsl:import href="skin.xsl"/>

	<!-- parameter language to choose a message set -->
	<o:param name="lang-default" select="'fr'"/>

	<!-- some useful stuff to have here -->
	<xsl:param name="messages-URI" select="concat('messages.', substring($lang, 1, 2))"/><!-- take set of language -->
	<o:variable name="messages" select="document($messages-URI)/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)] | document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
	<xsl:param name="id"/>
	<xsl:param name="appPath" select="concat('/', string(/*/*/sdx:application/sdx:Path), '/')"/>

	<!-- here is our CSS include -->
	<xsl:template match="sdx:xplorecss" mode="head">
		<link rel="stylesheet" type="text/css" href="res/explore.css"/>
	</xsl:template>

	<!-- well.. the page title... -->
	<xsl:template name="title">
		<o:apply-templates select="($messages/xfm:caption[@id=&#34;title&#34;])[1]"/>
	</xsl:template>

	<!-- a notice to let the super-user know that the application's backup he requested is a success -->
	<xsl:template match="saveApplicationDone">
		<span class="notice"><o:apply-templates select="($messages/xfm:caption[@id=&#34;nSaveApplicationDone&#34;])[1]"/></span>
	</xsl:template>

	<!-- a notice to let the super-user know that the documentBase's optimization he requested is a success (yay !) -->
	<xsl:template match="optimizationDone">
		<span class="notice"><o:apply-templates select="($messages/xfm:caption[@id=&#34;nOptimizationDone&#34;])[1]"/><xsl:if test="@base != ''">&#160;:&#160;<xsl:value-of select="@base"/></xsl:if></span>
	</xsl:template>

	<!-- a notice to let the super-user know that the documentBase's integrity check he requested is a success -->
	<xsl:template match="checkIntegrityDone">
		<span class="notice"><o:apply-templates select="($messages/xfm:caption[@id=&#34;nCheckIntegrityDone&#34;])[1]"/><xsl:if test="@base != ''">&#160;:&#160;<xsl:value-of select="@base"/></xsl:if></span>
	</xsl:template>

	<!-- warn the super-user about the fact that he asked an optimization for a documentBase that doesn't exist -->
	<xsl:template match="nullDocumentBase">
		<span class="warn"><o:apply-templates select="($messages/xfm:caption[@id=&#34;nNullDocumentBase&#34;])[1]"/></span>
	</xsl:template>

	<!-- Text output interception -->
	<xsl:template match="text()">
		<xsl:choose>
			<xsl:when test="self::text()='true'">
				<span class="true"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gTrue&#34;] )[1]"/></span>
			</xsl:when>
			<xsl:when test="self::text()='false'">
				<o:apply-templates select="($messages/xfm:caption[@id=&#34;gFalse&#34;] )[1]"/>
			</xsl:when>
			<xsl:when test="self::text()='null' or self::text()='none'">
				<span class="null"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gEmpty&#34;] )[1]"/></span>
			</xsl:when>
			<xsl:when test="self::text()=''">null</xsl:when>
			<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Exploration interface -->
	<xsl:template match="sdx:explore">

		<!-- Application level -->
		<div class="application">
		<!-- Some feedback from the super-user requests done by this interface -->
		<xsl:apply-templates select="/nullDocumentBase"/>
		<xsl:apply-templates select="/optimizationDone"/>
		<!-- Here follow the data about the application -->
		<h1><o:apply-templates select="($messages/xfm:caption[@id=&#34;tApp&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:application/sdx:ID"/>&#160;<xsl:value-of select="$appPath"/></h1>
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;gProps&#34;] )[1]"/></h2>
		<ul>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Encoding"/></strong></li>
		<li>XML-Lang&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:XML-Lang"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;headSessionObject&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Session_Object_Limit"/></strong></li>
		</ul>
		<!-- Tools -->
		<p class="tools">
			<!-- Backup tool -->
			<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
				<input type="hidden" class="xfm_input" name="op" value="backup"/>
				<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
				<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
				<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hSaveApplication&#34;]}" value="{$messages/xfm:caption[@id=&#34;iSaveApplication&#34;]}"/>
			</form>
			<!-- End of backup tool -->
			<!-- Optimization tool -->
			<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
				<input type="hidden" class="xfm_input" name="op" value="optimize"/>
				<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
				<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
				<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hOptimizeApplication&#34;]}" value="{$messages/xfm:caption[@id=&#34;iOptimizeBaseDoc&#34;]}"/>
			</form>
			<!-- End of optimization tool -->
			<!-- Integrity tool -->
			<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
				<input type="hidden" class="xfm_input" name="op" value="integrity"/>
				<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
				<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
				<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hCheckIntegrityApplication&#34;]}" value="{$messages/xfm:caption[@id=&#34;iCheckIntegrity&#34;]}"/>
			</form>
			<!-- End of integrity tool -->
		</p>
		<!-- End of tools -->
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;headDefault&#34;] )[1]"/></h2>
		<ul>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocBase&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Default_DocumentBase"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;docBaseType&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Default_Documentbase_Type"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;thesType&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Default_Thesaurus_Type"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;adminIdGroup&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Default_Administrator_Group_ID"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;adminIdUser&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Default_Administrator_User_ID"/></strong></li>
		</ul>
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;headDir&#34;] )[1]"/></h2>
		<ul>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;headClass&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Classes_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDataBases&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Databases_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocBases&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Documentbases_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;headLib&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Libraries_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gRepos&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Repositories_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;headThes&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Thesauri_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;headDbs&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Users_Databases_Directory_Name"/></strong></li>
		<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;headDocBases&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:application/sdx:Users_Documentbases_Directory_Name"/></strong></li>
		</ul>
		</div>

		<!-- Table of contents -->
		<h1><o:apply-templates select="($messages/xfm:caption[@id=&#34;tContents&#34;] )[1]"/></h1>
		<div id="index">
		<!-- Repositories -->
		<xsl:if test="count(child::*/sdx:repository) &gt; 0">
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;gRepos&#34;] )[1]"/></h2>
		<xsl:apply-templates select="."><xsl:with-param name="index" select="1"/><xsl:sort select="@id"/></xsl:apply-templates>
		</xsl:if>
		<!-- Field lists -->
		<xsl:if test="count(child::*/sdx:fieldList) &gt; 0">
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;gFieldList&#34;] )[1]"/></h2>
		<xsl:apply-templates select="child::*/sdx:fieldList"><xsl:with-param name="index" select="1"/><xsl:sort select="@id"/></xsl:apply-templates>
		</xsl:if>
		<!-- Documents Bases -->
		<xsl:if test="count(child::*/sdx:Document_Bases/sdx:databaseBacked) &gt; 0">
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocBases&#34;] )[1]"/></h2>
		<xsl:apply-templates select="child::*/sdx:Document_Bases/sdx:databaseBacked"><xsl:with-param name="index" select="1"/><xsl:sort select="@id"/></xsl:apply-templates>
		</xsl:if>
		<!-- Thesauri -->
		<xsl:if test="count(child::*/sdx:Thesauri/sdx:databaseBacked) &gt; 0">
		<h2><o:apply-templates select="($messages/xfm:caption[@id=&#34;headThes&#34;] )[1]"/></h2>
		<xsl:apply-templates select="child::*/sdx:Thesauri/sdx:databaseBacked"><xsl:with-param name="index" select="3"/><xsl:sort select="@id"/></xsl:apply-templates>
		</xsl:if>
		</div>

		<!-- Content -->
		<h1><o:apply-templates select="($messages/xfm:caption[@id=&#34;tData&#34;] )[1]"/></h1>
		<!-- Document Bases -->
		<xsl:if test="count(child::*/sdx:Document_Bases/sdx:databaseBacked) &gt; 0">
		<h1><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocBases&#34;] )[1]"/></h1>
		<ul><xsl:apply-templates select="sdx:application/sdx:Document_Bases/child::*"><xsl:with-param name="index" select="0"/></xsl:apply-templates></ul>
		</xsl:if>
		<!-- Thesauri -->
		<xsl:if test="count(child::*/sdx:Thesauri/sdx:databaseBacked) &gt; 0">
		<h1><o:apply-templates select="($messages/xfm:caption[@id=&#34;headThes&#34;] )[1]"/></h1>
		<ul><xsl:apply-templates select="sdx:application/sdx:Thesauri/child::*"><xsl:with-param name="index" select="2"/></xsl:apply-templates></ul>
		</xsl:if>
	</xsl:template>


	<!-- Properties display managment -->
	<xsl:template match="sdx:explore//*">
		<xsl:param name="index"/>
		<xsl:choose>

			<!-- Thesauri : Table of contents -->
			<xsl:when test="local-name()='databaseBacked' and $index='3'">
				<div class="documentbase">
					<a href="#{@object}"><xsl:value-of select="@id"/></a>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]
				</div>
	 		</xsl:when>

			<!-- Thesauri : content -->
			<xsl:when test="local-name()='databaseBacked' and $index='2'">
				<div class="documentbase"><a name="{@object}"/><h2><xsl:value-of select="@id"/>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]</h2>
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;gProps&#34;] )[1]"/>&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:XML-Lang"/></h3>
				<ul>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;thesType&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Thesaurus_Type"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gIsDefault&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Is_default"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;autoOptimize&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Auto_Optimize"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;keepOriginal&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Keep_Original_Documents"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;depth&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Depth"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocCount&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Document_Count"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;source&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Source"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;relation&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Relation"/></strong></li>
				</ul>
				<!-- Database -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDataBase&#34;] )[1]"/>&#160;[&#160;<xsl:value-of select="sdx:database/@id"/>&#160;]</h3>
				<ul>
				<xsl:if test="sdx:database/sdx:Database_Type/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dbType&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:Database_Type"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:Database_Directory_Path/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDir&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:Database_Directory_Path"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:member/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dbMember&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:member"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:JDBC_Table_Name/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dbJDBCTableName&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:JDBC_Table_Name"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:Document_Count/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocCount&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:Document_Count"/></strong></li></xsl:if>
				</ul>
				<!-- Pipeline -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;pipeline&#34;] )[1]"/>&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:pipeline/sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:pipeline/sdx:XML-Lang"/></h3>
				<ul>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gLocale&#34;] )[1]"/>&#160;=&#160;<strong><xsl:value-of select="sdx:pipeline/sdx:Locale"/></strong></li>
				<xsl:if test="sdx:pipeline/sdx:sdxDate/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateSDX&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:sdxDate"/></strong></li></xsl:if>
				<xsl:if test="sdx:pipeline/sdx:sdxISO8601Date/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateISO&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:sdxISO8601Date"/></strong></li></xsl:if>
				<xsl:if test="sdx:pipeline/sdx:sdxDateMilliseconds/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateMilli&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:sdxDateMilliseconds"/></strong></li></xsl:if>
				<xsl:if test="sdx:pipeline/sdx:docUrl/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gUrlDoc&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:docUrl"/></strong></li></xsl:if>
				</ul>
				<!-- Index -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;index&#34;] )[1]"/>&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:index_encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:index_xml-lang"/></h3>
				<ul>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;engine&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_engine"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;isOptimized&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_is_optimized"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;useCF&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_use_compound_files"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocCount&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_document_count"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;path&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_path"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexCreate&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_creation_date"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexMaj&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_last_modification_date"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexFullSize&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_full_size"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexCountFiles&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_count_files"/></strong></li>
				</ul>
				<xsl:apply-templates select="sdx:fieldList"/>
				</div>
			</xsl:when>

			<!-- DocumentBase : Table of contents -->
			<xsl:when test="local-name()='databaseBacked' and $index='1'">
				<div class="documentbase">
					<a href="#{@object}"><xsl:value-of select="@id"/></a>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]
					<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;gRepos&#34;] )[1]"/></h3>
					<xsl:for-each select="sdx:SDX_DocumentBase_Repositories/sdx:repository">
						<xsl:apply-templates select=".">
							<xsl:with-param name="index" select="2"/>
							<xsl:sort select="@id"/>
						</xsl:apply-templates>
					</xsl:for-each>
					<!-- Tools -->
					<p class="tools">
						<!-- Optimization tool -->
						<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
							<input type="hidden" class="xfm_input" name="op" value="optimize"/>
							<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
							<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
							<input type="hidden" class="xfm_input" name="base" value="{@id}"/>
							<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hOptimize&#34;]}" value="{$messages/xfm:caption[@id=&#34;iOptimizeBaseDoc&#34;]}"/>
						</form>
						<!-- End of optimization tool -->
						<!-- Integrity tool -->
						<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
							<input type="hidden" class="xfm_input" name="op" value="integrity"/>
							<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
							<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
							<input type="hidden" class="xfm_input" name="base" value="{@id}"/>
							<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hCheckIntegrity&#34;]}" value="{$messages/xfm:caption[@id=&#34;iCheckIntegrity&#34;]}"/>
						</form>
						<!-- End of integrity tool -->
					</p>
					<!-- End of tools -->
				</div>
	 		</xsl:when>

			<!-- DocumentBase : content -->
			<xsl:when test="local-name()='databaseBacked'">
				<div class="documentbase"><a name="{@object}"/><h2><xsl:value-of select="@id"/>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]</h2>
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;gProps&#34;] )[1]"/>&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:XML-Lang"/></h3>
				<ul>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gIsDefault&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Is_default"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;defaultRepo&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Default_Repository"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;autoOptimize&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Auto_Optimize"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexFileSize&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Split_Size"/>&#160;<xsl:apply-templates select="sdx:Split_Unit"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexMaxDoc&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Split_Doc"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;keepOriginal&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Keep_Original_Documents"/></strong></li>
				</ul>
				<!-- Tools -->
				<!-- Optimization tool -->
				<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
					<input type="hidden" class="xfm_input" name="op" value="optimize"/>
					<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
					<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
					<input type="hidden" class="xfm_input" name="base" value="{@id}"/>
					<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hOptimize&#34;]}" value="{$messages/xfm:caption[@id=&#34;iOptimizeBaseDoc&#34;]}"/>
				</form>
				<!-- End of optimization tool -->
				<!-- Integrity tool -->
				<form class="tool" action="{/sdx:document/@uri}" method="get" onsubmit="if (window.xfm_submit) return xfm_submit(this);">
					<input type="hidden" class="xfm_input" name="op" value="integrity"/>
					<input type="hidden" class="xfm_input" name="app" value="{$app}"/>
					<input type="hidden" class="xfm_input" name="lang" value="{$lang}"/>
					<input type="hidden" class="xfm_input" name="base" value="{@id}"/>
					<input type="submit" onclick="this.form.action='{@href}'" title="{$messages/xfm:hint[@id=&#34;hCheckIntegrity&#34;]}" value="{$messages/xfm:caption[@id=&#34;iCheckIntegrity&#34;]}"/>
				</form>
				<!-- End of integrity tool -->
				<!-- End of tools -->
				<!-- Identify (from OAI) : displaying data via a loop because XPath doesn't work here without the namespace. TODO: fix it -->
				<xsl:for-each select="child::*">
					<xsl:if test="local-name(.)='Identify'">
						<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiIdentify&#34;] )[1]"/></h3>
						<ul>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiRepoName&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[1]"/></strong></li>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiBaseUrl&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[2]"/></strong></li>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiProtocol&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[3]"/></strong></li>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiMail&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[4]"/></strong></li>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiTimeStamp&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[5]"/></strong></li>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiDelete&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[6]"/></strong></li>
						<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;oaiGranularity&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="child::*[7]"/></strong></li>
						</ul>
					</xsl:if>
				</xsl:for-each>
				<!-- Database -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDataBase&#34;] )[1]"/>&#160;[&#160;<xsl:value-of select="sdx:database/@id"/>&#160;]</h3>
				<ul>
				<xsl:if test="sdx:database/sdx:Database_Type/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dbType&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:Database_Type"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:Database_Directory_Path/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDir&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:Database_Directory_Path"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:member/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dbMember&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:member"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:JDBC_Table_Name/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dbJDBCTableName&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:JDBC_Table_Name"/></strong></li></xsl:if>
				<xsl:if test="sdx:database/sdx:Document_Count/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocCount&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:database/sdx:Document_Count"/></strong></li></xsl:if>
				</ul>
				<!-- Pipeline -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;pipeline&#34;] )[1]"/>&#160;[&#160;<xsl:apply-templates select="sdx:pipeline/sdx:ID"/>&#160;]&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:pipeline/sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:pipeline/sdx:XML-Lang"/></h3>
				<ul>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gLocale&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:Locale"/></strong></li>
				<xsl:if test="sdx:pipeline/sdx:sdxDate/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateSDX&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:sdxDate"/></strong></li></xsl:if>
				<xsl:if test="sdx:pipeline/sdx:sdxISO8601Date/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateISO&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:sdxISO8601Date"/></strong></li></xsl:if>
				<xsl:if test="sdx:pipeline/sdx:sdxDateMilliseconds/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateMilli&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:sdxDateMilliseconds"/></strong></li></xsl:if>
				<xsl:if test="sdx:pipeline/sdx:docUrl/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gUrlDoc&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:docUrl"/></strong></li></xsl:if>
				<li>
					<!-- Transformation -->
					<div class="transformation">
					<span class="transformation"><o:apply-templates select="($messages/xfm:caption[@id=&#34;transformation&#34;] )[1]"/>&#160;[&#160;<xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:ID"/>&#160;]&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:value-of select="sdx:pipeline/sdx:transformation/sdx:XML-Lang"/></span>
					<ul>
					<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gLocale&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:Locale"/></strong></li>
					<xsl:if test="sdx:pipeline/sdx:transformation/sdx:sdxDate/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateSDX&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:sdxDate"/></strong></li></xsl:if>
					<xsl:if test="sdx:pipeline/sdx:transformation/sdx:sdxISO8601Date/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateISO&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:sdxISO8601Date"/></strong></li></xsl:if>
					<xsl:if test="sdx:pipeline/sdx:transformation/sdx:sdxDateMilliseconds/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDateMilli&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:sdxDateMilliseconds"/></strong></li></xsl:if>
					<xsl:if test="sdx:pipeline/sdx:transformation/sdx:docUrl/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gUrlDoc&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:docUrl"/></strong></li></xsl:if>
					<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gName&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:pipeline/sdx:transformation/sdx:Name"/></strong></li>
					</ul>
					</div>
				</li>
				</ul>
				<!-- Repository -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;gRepos&#34;] )[1]"/></h3>
				<xsl:for-each select="sdx:SDX_DocumentBase_Repositories/sdx:repository">
					<ul>
					<xsl:apply-templates select=".">
						<xsl:sort select="@id"/>
					</xsl:apply-templates>
					</ul>
				</xsl:for-each>
				<!-- Index -->
				<h3><o:apply-templates select="($messages/xfm:caption[@id=&#34;index&#34;] )[1]"/>&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:index_encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:index_xml-lang"/></h3>
				<ul>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;engine&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_engine"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;isOptimized&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_is_optimized"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;useCF&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_use_compound_files"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gDocCount&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_document_count"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;path&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_path"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexCreate&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_creation_date"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexMaj&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_last_modification_date"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexFullSize&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_full_size"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;indexCountFiles&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:index_count_files"/></strong></li>
				</ul>
				<xsl:apply-templates select="sdx:fieldList"/>
				</div>
	 		</xsl:when>

	 		<!-- Repositories : Application level -->
			<!-- Still have to see this one in a real case as sdxtest does not provide this kind of repository -->
	 		<xsl:when test="local-name()='repository' and $index='1'">
				<xsl:if test="local-name(parent::*)='repositories'"><div class="repository"><a href="#app{local-name()}{@id}"><xsl:value-of select="@id"/></a>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]</div></xsl:if>
	 		</xsl:when>

	 		<!-- Repositories : Table of contents -->
	 		<xsl:when test="local-name()='repository' and $index='2'">
				<div class="repository"><a href="#{local-name()}{parent::*/parent::*/@id}{@id}"><xsl:value-of select="@id"/></a>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]</div>
	 		</xsl:when>

	 		<!-- Repositories : Content -->
	 		<xsl:when test="local-name()='repository'">
				<li>
				<div class="repository"><span class="repository">
				<xsl:if test="local-name(parent::*)='repositories'"><a name="app{local-name()}{@id}"/></xsl:if>
				<xsl:if test="local-name(parent::*)!='repositories'"><a name="{local-name()}{parent::*/parent::*/@id}{@id}"/></xsl:if>
				<xsl:value-of select="@id"/>&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:XML-Lang"/>
				</span>
				<ul>
				<!-- Shared data -->
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gIsDefault&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Is_default"/></strong></li>
				<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gType&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Repository_Type"/></strong></li>
				<!-- Specific data -->
				<xsl:if test="sdx:Data_Source_Identifier/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;dataSrcId&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Data_Source_Identifier"/></strong></li></xsl:if>
				<xsl:if test="sdx:Read_Only/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;readOnly&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Read_Only"/></strong></li></xsl:if>
				<xsl:if test="sdx:Use_cache/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;useCache&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Use_cache"/></strong></li></xsl:if>
				<xsl:if test="sdx:Check_On_Get/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;checkOnGet&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Check_On_Get"/></strong></li></xsl:if>
				<xsl:if test="sdx:Base_Directory/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;baseDirectory&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Base_Directory"/></strong></li></xsl:if>
				<xsl:if test="sdx:Base_URL/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;baseURL&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Base_URL"/></strong></li></xsl:if>
				<xsl:if test="sdx:Documents_Directory/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;docDirectory&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Documents_Directory"/></strong></li></xsl:if>
				<xsl:if test="sdx:File_prefix/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;filePrefix&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:File_prefix"/></strong></li></xsl:if>
				<xsl:if test="sdx:File_suffix/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;fileSuffix&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:File_suffix"/></strong></li></xsl:if>
				<xsl:if test="sdx:Extent/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;extent&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Extent"/></strong></li></xsl:if>
				<xsl:if test="sdx:Depth/text() != ''"><li><o:apply-templates select="($messages/xfm:caption[@id=&#34;depth&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Depth"/></strong></li></xsl:if>
				<xsl:for-each select="sdx:database"><xsl:apply-templates select="."/></xsl:for-each>
				</ul>
				</div>
				</li>
	 		</xsl:when>

	 		<!-- Fields list : Application level -->
			<!-- Still have to see this one in a real case too -->
	 		<xsl:when test="local-name()='fieldList' and $index='1'">
				<div class="fieldlist">
				<a href="#app{local-name()}{@id}"><xsl:value-of select="@id"/></a>&#160;[&#160;<xsl:value-of select="@object"/>&#160;]
				<span><xsl:value-of select="count(sdx:field/child::*)"/>&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;fields&#34;] )[1]"/>&#160;|&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;defaultField&#34;] )[1]"/>&#160;:&#160;<xsl:apply-templates select="sdx:default_field"/>&#160;|&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;ignoreCase&#34;] )[1]"/>&#160;:&#160;<xsl:apply-templates select="sdx:ignore_case"/></span>
				</div>
	 		</xsl:when>

	 		<!-- Fields list : Content -->
	 		<xsl:when test="local-name()='fieldList'">
	  		<h3 class="fieldlist"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gFieldList&#34;] )[1]"/>&#160;[&#160;<xsl:apply-templates select="sdx:ID"/>&#160;]&#160;::&#160;<o:apply-templates select="($messages/xfm:caption[@id=&#34;gEncode&#34;] )[1]"/>&#160;<xsl:apply-templates select="sdx:Encoding"/>&#160;::&#160;XML-Lang&#160;<xsl:apply-templates select="sdx:XML-Lang"/></h3>
			<ul>
			<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gLocale&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="@locale"/></strong></li>
			<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;defaultField&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Default_Field"/></strong></li>
			<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;gWordAnalyzer&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Default_Analyser"/></strong></li>
			<li><o:apply-templates select="($messages/xfm:caption[@id=&#34;ignoreCase&#34;] )[1]"/>&#160;=&#160;<strong><xsl:apply-templates select="sdx:Ignore_Case"/></strong></li>
			</ul>
	  		<table class="properties">
				<tr class="property-row-head">
					<td class="property-no"><o:apply-templates select="($messages/xfm:caption[@id=&#34;number&#34;] )[1]"/></td>
					<td class="property-name"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gName&#34;] )[1]"/></td>
					<td class="property-type"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gType&#34;] )[1]"/></td>
					<td class="property-default"><o:apply-templates select="($messages/xfm:caption[@id=&#34;default&#34;] )[1]"/></td>
					<td class="property-brief"><o:apply-templates select="($messages/xfm:caption[@id=&#34;brief&#34;] )[1]"/></td>
					<td class="property-storeTermVector"><o:apply-templates select="($messages/xfm:caption[@id=&#34;storeTermVector&#34;] )[1]"/></td>
					<td class="property-locale"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gLocale&#34;] )[1]"/></td>
					<td class="property-analyzer"><o:apply-templates select="($messages/xfm:caption[@id=&#34;gWordAnalyzer&#34;] )[1]"/></td>
					<td class="property-config"><o:apply-templates select="($messages/xfm:caption[@id=&#34;config&#34;] )[1]"/></td>
				</tr>
				<xsl:for-each select="./sdx:field">
					<xsl:apply-templates select="child::*">
						<xsl:sort select="sdx:name"/>
					</xsl:apply-templates>
				</xsl:for-each>
	  		</table>
	 		</xsl:when>

	 		<!-- Fields -->
	 		<xsl:when test="local-name()='field'">
				<!-- Even row number (used for display style via CSS) -->
				<xsl:if test="position() mod 2 = 0">
					<tr class="property-row-even">
						<td class="property-no"><xsl:value-of select="position()"/></td>
						<td class="property-name"><xsl:apply-templates select="sdx:name"/></td>
						<td class="property-type"><xsl:apply-templates select="sdx:type_name"/></td>
						<td class="property-default"><xsl:apply-templates select="sdx:is_default/text()"/></td>
						<td class="property-brief"><xsl:apply-templates select="sdx:in_brief/text()"/></td>
						<td class="property-storeTermVector"><xsl:apply-templates select="sdx:storeTermVector"/></td>
						<td class="property-locale"><xsl:apply-templates select="sdx:locale"/></td>
						<td class="property-analyzer"><xsl:apply-templates select="sdx:analyzer/sdx:type"/></td>
						<td class="property-config">todo</td>
					</tr>
				</xsl:if>
				<!-- Odd row number (used for display style via CSS) -->
				<xsl:if test="position() mod 2 != 0">
					<tr class="property-row-odd">
						<td class="property-no"><xsl:value-of select="position()"/></td>
						<td class="property-name"><xsl:apply-templates select="sdx:name"/></td>
						<td class="property-type"><xsl:apply-templates select="sdx:type_name"/></td>
						<td class="property-default"><xsl:apply-templates select="sdx:is_default/text()"/></td>
						<td class="property-brief"><xsl:apply-templates select="sdx:in_brief/text()"/></td>
						<td class="property-storeTermVector"><xsl:apply-templates select="sdx:storeTermVector"/></td>
						<td class="property-locale"><xsl:apply-templates select="sdx:locale"/></td>
						<td class="property-analyzer"><xsl:apply-templates select="sdx:analyzer/sdx:type"/></td>
						<td class="property-config">todo</td>
					</tr>
				</xsl:if>
	 		</xsl:when>

			<!--
				In any other case, we output the data as an unordered html list, so we can keep the tree structure as we go through the childs elements.
				As this should no longer happen other than for debuging purpose, we add a [*] before each element to keep a track of what is going out of here.
			-->
	 		<xsl:otherwise>
				<xsl:if test="current()!=text()">
					<li><em style="font-style:italic">[*]&#160;</em><xsl:value-of select="name()" /><xsl:if test="count(child::*) = 0 and text() != ''">&#160;=&#160;<strong><xsl:apply-templates select="."/></strong></xsl:if>
					<xsl:if test="count(child::*) > 0"><ul><xsl:apply-templates select="child::*"/></ul></xsl:if>
					</li>
				</xsl:if>
				<xsl:if test="current()=text()">
					<xsl:apply-templates/>
				</xsl:if>
	 		</xsl:otherwise>

		</xsl:choose>
	</xsl:template>


	<!-- ======  COMMON TEMPLATES  ======================
			short templates to format messages
			known issues : no correct id for a repeated help button
			then hide/show message can't work
			but is it useful to repeat the same help ?
	-->

	<o:template match="xfm:help">
		<o:param name="id" select="@id"/>
		<input type="button" class="xfm_help" tabindex="32767" value="?">
		<o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
		</input>
		<table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
			<o:attribute name="id">
			<o:value-of select="$id"/>_help</o:attribute>
			<tr>
				<td style="width:90%;" class="xfm_bar">
					<o:value-of select="(caption | title | h1 )[1]"/>
				</td>
				<td style="text-align:right" class="xfm_bar">
					<input type="button" class="xfm_key" value="X">
					<o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
					</input>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="xfm_text">
				<o:apply-templates mode="copy"/>
				</td>
			</tr>
		</table>
	</o:template>

	<o:template match="xfm:caption" priority="-1">
		<o:apply-templates mode="copy"/>
	</o:template>

	<!-- Escape js string (or other search/replace)-->
	<o:template match="* | @* | text()" mode="replace" name="xfm:replace">
		<o:param name="string" select="normalize-space(.)"/>
		<o:param name="search">'</o:param>
		<o:param name="replace">\'</o:param>
		<o:choose>
			<o:when test="contains($string, $search)">
				<o:value-of select="concat(substring-before($string, $search), $replace)"/>
				<o:call-template name="xfm:replace">
					<o:with-param name="string" select="substring-after($string, $search)"/>
					<o:with-param name="search" select="$search"/>
					<o:with-param name="replace" select="$replace"/>
				</o:call-template>
			</o:when>
			<o:otherwise>
				<o:value-of select="$string"/>
			</o:otherwise>
		</o:choose>
	</o:template>

	<o:template match="*" priority="-1" mode="copy">
		<o:element name="{name()}">
			<o:apply-templates select="@*" mode="copy"/>
			<o:apply-templates mode="copy"/>
		</o:element>
	</o:template>

	<o:template match="@*" mode="copy">
		<o:copy/>
	</o:template>


	<!-- ======  MESSAGES @xml:lang="fr"  ====================== -->
	<xfm:messages xml:lang="fr">
		<xfm:caption id="title">SDX Administration - Application - Explorer</xfm:caption>

		<!-- Titres -->
		<xfm:caption id="tApp">Application</xfm:caption>
		<xfm:caption id="tContents">Table des mati&#232;res</xfm:caption>
		<xfm:caption id="tData">Contenu de l'application</xfm:caption>

		<!-- Informations de debut de page -->
		<xfm:caption id="headDefault">Valeurs par d&#233;fauts</xfm:caption>
		<xfm:caption id="headDir">R&#233;pertoires</xfm:caption>
		<xfm:caption id="headDb">Base de donn&#233;es interne des utilisateurs</xfm:caption>
		<xfm:caption id="headSessionObject">Limite d'objets par session</xfm:caption>
		<xfm:caption id="headClass">Classes</xfm:caption>
		<xfm:caption id="headLib">Biblioth&#232;ques</xfm:caption>
		<xfm:caption id="headThes">Thesauri</xfm:caption>
		<xfm:caption id="headDbs">Bases de donn&#233;es internes des utilisateurs</xfm:caption>
		<xfm:caption id="headDocBases">Base de documents des utilisateurs</xfm:caption>

		<!-- Informations recurrentes ('g' signifie 'global') -->
		<xfm:caption id="gEmpty">Aucune Valeur</xfm:caption>
		<xfm:caption id="gTrue">vrai</xfm:caption>
		<xfm:caption id="gFalse">faux</xfm:caption>
		<xfm:caption id="gEncode">Encodage</xfm:caption>
		<xfm:caption id="gDocBases">Bases de documents</xfm:caption>
		<xfm:caption id="gDocBase">Base de documents</xfm:caption>
		<xfm:caption id="gDataBases">Bases de donn&#233;es internes</xfm:caption>
		<xfm:caption id="gDataBase">Base de donn&#233;es interne</xfm:caption>
		<xfm:caption id="gRepos">Entrep&#244;ts</xfm:caption>
		<xfm:caption id="gRepo">Entrep&#244;t</xfm:caption>
		<xfm:caption id="gFieldList">Liste de champs</xfm:caption>
		<xfm:caption id="gDir">R&#233;pertoire</xfm:caption>
		<xfm:caption id="gProps">Propri&#233;t&#233;s</xfm:caption>
		<xfm:caption id="gIsDefault">Par d&#233;faut</xfm:caption>
		<xfm:caption id="gLocale">Localisation</xfm:caption>
		<xfm:caption id="gName">Nom</xfm:caption>
		<xfm:caption id="gType">Type</xfm:caption>
		<xfm:caption id="gDateSDX">Date SDX</xfm:caption>
		<xfm:caption id="gDateISO">Date ISO-8601</xfm:caption>
		<xfm:caption id="gDateMilli">Date en millisecondes</xfm:caption>
		<xfm:caption id="gUrlDoc">URL du document</xfm:caption>
		<xfm:caption id="gWordAnalyzer">Analyseur de mots</xfm:caption>
		<xfm:caption id="gDocCount">Nombre de documents</xfm:caption>

		<!-- Informations specifiques -->
		<xfm:caption id="docBaseType">Type de base de documents</xfm:caption>
		<xfm:caption id="thesType">Type de thesaurus</xfm:caption>
		<xfm:caption id="adminIdGroup">Identifiant de groupe administrateur</xfm:caption>
		<xfm:caption id="adminIdUser">Identifiant utilisateur d'administrateur</xfm:caption>
		<xfm:caption id="defaultRepo">Entrep&#244;t par d&#233;faut</xfm:caption>
		<xfm:caption id="keepOriginal">Conserver les documents originaux</xfm:caption>
		<xfm:caption id="pipeline">Pipeline <em style="font-style:italic">(canalisation)</em></xfm:caption>
		<xfm:caption id="transformation">Transformation</xfm:caption>
		<xfm:caption id="index">Index</xfm:caption>
		<xfm:caption id="engine">Moteur</xfm:caption>
		<xfm:caption id="path">Chemin</xfm:caption>
		<xfm:caption id="readOnly">Lecture seule</xfm:caption>
		<xfm:caption id="useCache">Utilise le cache</xfm:caption>
		<xfm:caption id="checkOnGet">V&#233;rification lors de l'obtention</xfm:caption>
		<xfm:caption id="baseDirectory">R&#233;pertoire de base</xfm:caption>
		<xfm:caption id="docDirectory">R&#233;pertoire des documents</xfm:caption>
		<xfm:caption id="filePrefix">Pr&#233;fixe de nom de fichier</xfm:caption>
		<xfm:caption id="fileSuffix">Suffixe de nom de fichier</xfm:caption>
		<xfm:caption id="extent">Ampleur</xfm:caption>
		<xfm:caption id="depth">Profondeur</xfm:caption>
		<xfm:caption id="fields">Champs</xfm:caption>
		<xfm:caption id="defaultField">Champ par d&#233;faut</xfm:caption>
		<xfm:caption id="ignoreCase">Ignore la casse</xfm:caption>
		<xfm:caption id="number">No</xfm:caption>
		<xfm:caption id="default">Par d&#233;faut</xfm:caption>
		<xfm:caption id="brief">Bref</xfm:caption>
		<xfm:caption id="config">Config</xfm:caption>
		<xfm:caption id="dbMember">Membres</xfm:caption>
		<xfm:caption id="dbJDBCTableName">Nom de la table JDBC</xfm:caption>
		<xfm:caption id="useCF">Utilise les fichiers composites</xfm:caption>
		<xfm:caption id="dbType">Type de base de donn&#233;es</xfm:caption>
		<xfm:caption id="indexCreate">Date de cr&#233;ation</xfm:caption>
		<xfm:caption id="indexMaj">Date de derni&#232;re modification</xfm:caption>
		<xfm:caption id="source">Source</xfm:caption>
		<xfm:caption id="relation">Relation</xfm:caption>
		<xfm:caption id="baseURL">URL de base</xfm:caption>
		<xfm:caption id="dataSrcId">Identifiant de la source de donn&#233;es</xfm:caption>
		<xfm:caption id="storeTermVector">TermVector</xfm:caption>
		<xfm:caption id="autoOptimize">Optimisation automatique</xfm:caption>
		<xfm:caption id="isOptimized">Optimis&#233;</xfm:caption>
		<xfm:caption id="indexFullSize">Taille totale de l'index</xfm:caption>
		<xfm:caption id="indexCountFiles">Nombre de fichiers d'index</xfm:caption>
		<xfm:caption id="indexFileSize">Taille maximum d'un fichier d'index</xfm:caption>
		<xfm:caption id="indexMaxDoc">Nombre de documents maximums par fichier d'index</xfm:caption>

		<!-- Donnees OAI -->
		<xfm:caption id="oaiIdentify">Identification</xfm:caption>
		<xfm:caption id="oaiRepoName">Nom de l'entrep&#244;t</xfm:caption>
		<xfm:caption id="oaiBaseUrl">URL de base</xfm:caption>
		<xfm:caption id="oaiProtocol">Version du protocole</xfm:caption>
		<xfm:caption id="oaiMail">Mail de l'administrateur</xfm:caption>
		<xfm:caption id="oaiTimeStamp">Derni&#232;re activation</xfm:caption>
		<xfm:caption id="oaiDelete">Suppression d'enregistrements</xfm:caption>
		<xfm:caption id="oaiGranularity">Format date</xfm:caption>

		<!-- Messages d'informations pour le super-utilisateur -->
		<xfm:caption id="nOptimizationDone">Optimisation r&#233;ussie</xfm:caption>
		<xfm:caption id="nNullDocumentBase">La base de document demand&#233;e n'existe pas</xfm:caption>
		<xfm:caption id="nSaveApplicationDone">Sauvegarde de l'application r&#233;ussie</xfm:caption>
		<xfm:caption id="nCheckIntegrityDone">V&#233;rification l'int&#233;grit&#233; r&#233;ussie</xfm:caption>

		<!-- Boutons : libelle / infos contextuelles -->
		<xfm:caption id="iOptimizeBaseDoc">Optimiser</xfm:caption>
		<xfm:hint id="hOptimize">Optimise les &#233;l&#233;ments de la base de document, en particulier l'index (long).</xfm:hint>
		<xfm:hint id="hOptimizeApplication">Optimise toutes les bases de documents de l'application (long).</xfm:hint>
		<xfm:caption id="iSaveApplication">Sauvegarder</xfm:caption>
		<xfm:hint id="hSaveApplication">Effectue une sauvegarde compl&#232;te de l'application</xfm:hint>
		<xfm:caption id="iCheckIntegrity">Int&#233;grit&#233;</xfm:caption>
		<xfm:hint id="hCheckIntegrity">V&#233;rifie l'int&#233;grit&#233; de la base de document</xfm:hint>
		<xfm:hint id="hCheckIntegrityApplication">V&#233;rifie l'int&#233;grit&#233; de l'application compl&#232;te</xfm:hint>

	</xfm:messages>

	<!-- ======  MESSAGES @xml:lang="en"  ====================== -->
	<xfm:messages xml:lang="en">
		<xfm:caption id="title">SDX Administration - Application - Explore</xfm:caption>

		<!-- Titles -->
		<xfm:caption id="tApp">Application</xfm:caption>
		<xfm:caption id="tContents">Table of contents</xfm:caption>
		<xfm:caption id="tData">Application content</xfm:caption>

		<!-- Data from page begin -->
		<xfm:caption id="headDefault">Default values</xfm:caption>
		<xfm:caption id="headDir">Directories</xfm:caption>
		<xfm:caption id="headDb">Internal users database</xfm:caption>
		<xfm:caption id="headSessionObject">Session object limit</xfm:caption>
		<xfm:caption id="headClass">Classes</xfm:caption>
		<xfm:caption id="headLib">Libraries</xfm:caption>
		<xfm:caption id="headThes">Thesauri</xfm:caption>
		<xfm:caption id="headDbs">Internal users databases</xfm:caption>
		<xfm:caption id="headDocBases">Users documentBases</xfm:caption>

		<!-- Multiple occurences data ('g' stand for 'global') -->
		<xfm:caption id="gEmpty">No Value</xfm:caption>
		<xfm:caption id="gTrue">true</xfm:caption>
		<xfm:caption id="gFalse">false</xfm:caption>
		<xfm:caption id="gEncode">Encoding</xfm:caption>
		<xfm:caption id="gDocBases">DocumentBases</xfm:caption>
		<xfm:caption id="gDocBase">DocumentBase</xfm:caption>
		<xfm:caption id="gDataBases">Internal databases</xfm:caption>
		<xfm:caption id="gDataBase">Internal database</xfm:caption>
		<xfm:caption id="gRepos">Repositories</xfm:caption>
		<xfm:caption id="gRepo">Repository</xfm:caption>
		<xfm:caption id="gFieldList">Fields list</xfm:caption>
		<xfm:caption id="gDir">Directory</xfm:caption>
		<xfm:caption id="gProps">Properties</xfm:caption>
		<xfm:caption id="gIsDefault">Is default</xfm:caption>
		<xfm:caption id="gLocale">Locale</xfm:caption>
		<xfm:caption id="gName">Name</xfm:caption>
		<xfm:caption id="gType">Type</xfm:caption>
		<xfm:caption id="gDateSDX">SDX date</xfm:caption>
		<xfm:caption id="gDateISO">ISO-8601 date</xfm:caption>
		<xfm:caption id="gDateMilli">Date in milliseconds</xfm:caption>
		<xfm:caption id="gUrlDoc">Document's URL</xfm:caption>
		<xfm:caption id="gWordAnalyzer">Word Analyzer</xfm:caption>
		<xfm:caption id="gDocCount">Document count</xfm:caption>

		<!-- Specific data -->
		<xfm:caption id="docBaseType">DocumentBase type</xfm:caption>
		<xfm:caption id="thesType">Thesaurus type</xfm:caption>
		<xfm:caption id="adminIdGroup">Administrator group ID</xfm:caption>
		<xfm:caption id="adminIdUser">Administrator user ID</xfm:caption>
		<xfm:caption id="defaultRepo">Default repository</xfm:caption>
		<xfm:caption id="keepOriginal">Keep original documents</xfm:caption>
		<xfm:caption id="pipeline">Pipeline</xfm:caption>
		<xfm:caption id="transformation">Transformation</xfm:caption>
		<xfm:caption id="index">Index</xfm:caption>
		<xfm:caption id="engine">Engine</xfm:caption>
		<xfm:caption id="path">Path</xfm:caption>
		<xfm:caption id="readOnly">Read only</xfm:caption>
		<xfm:caption id="useCache">Use cache</xfm:caption>
		<xfm:caption id="checkOnGet">Check on get</xfm:caption>
		<xfm:caption id="baseDirectory">Base directory</xfm:caption>
		<xfm:caption id="docDirectory">Document directory</xfm:caption>
		<xfm:caption id="filePrefix">File prefix</xfm:caption>
		<xfm:caption id="fileSuffix">File suffix</xfm:caption>
		<xfm:caption id="extent">Extent</xfm:caption>
		<xfm:caption id="depth">Depth</xfm:caption>
		<xfm:caption id="fields">Fields</xfm:caption>
		<xfm:caption id="defaultField">Default field</xfm:caption>
		<xfm:caption id="ignoreCase">Ignore case</xfm:caption>
		<xfm:caption id="number">No</xfm:caption>
		<xfm:caption id="default">Default</xfm:caption>
		<xfm:caption id="brief">Brief</xfm:caption>
		<xfm:caption id="config">Config</xfm:caption>
		<xfm:caption id="dbMember">Members</xfm:caption>
		<xfm:caption id="dbJDBCTableName">JDBC table name</xfm:caption>
		<xfm:caption id="useCF">Use compound files</xfm:caption>
		<xfm:caption id="dbType">Database  type</xfm:caption>
		<xfm:caption id="indexCreate">Creation date</xfm:caption>
		<xfm:caption id="indexMaj">Last modification date</xfm:caption>
		<xfm:caption id="source">Source</xfm:caption>
		<xfm:caption id="relation">Relation</xfm:caption>
		<xfm:caption id="baseURL">Basic URL</xfm:caption>
		<xfm:caption id="dataSrcId">Data source identifier</xfm:caption>
		<xfm:caption id="storeTermVector">TermVector</xfm:caption>
		<xfm:caption id="autoOptimize">Automatic optimization</xfm:caption>
		<xfm:caption id="isOptimized">Optimised</xfm:caption>
		<xfm:caption id="indexFullSize">Total index size</xfm:caption>
		<xfm:caption id="indexCountFiles">Index files count</xfm:caption>
		<xfm:caption id="indexFileSize">Maximum size for an index file</xfm:caption>
		<xfm:caption id="indexMaxDoc">Maximum indexed document for an index file</xfm:caption>

		<!-- OAI Data -->
		<xfm:caption id="oaiIdentify">Identify</xfm:caption>
		<xfm:caption id="oaiRepoName">Repository name</xfm:caption>
		<xfm:caption id="oaiBaseUrl">Base URL</xfm:caption>
		<xfm:caption id="oaiProtocol">Protocol version</xfm:caption>
		<xfm:caption id="oaiMail">Mail admin</xfm:caption>
		<xfm:caption id="oaiTimeStamp">Earliest timestamp</xfm:caption>
		<xfm:caption id="oaiDelete">Deleted record</xfm:caption>
		<xfm:caption id="oaiGranularity">Granularity</xfm:caption>

		<!-- Information message for the superuser -->
		<xfm:caption id="nOptimizationDone">Optimization successful</xfm:caption>
		<xfm:caption id="nNullDocumentBase">The requested DocumentBase doesn't exist for this application</xfm:caption>
		<xfm:caption id="nSaveApplicationDone">Application backup successful</xfm:caption>
		<xfm:caption id="nCheckIntegrityDone">Integrity check successful</xfm:caption>

		<!-- Button caption and hints -->
		<xfm:caption id="iOptimizeBaseDoc">Optimize</xfm:caption>
		<xfm:hint id="hOptimize">Optimize the DocumentBase, mainly the index (long).</xfm:hint>
		<xfm:hint id="hOptimizeApplication">Optimize all application's DocumentBases (long).</xfm:hint>
		<xfm:caption id="iSaveApplication">Backup</xfm:caption>
		<xfm:hint id="hSaveApplication">Do a full application backup</xfm:hint>
		<xfm:caption id="iCheckIntegrity">Integrity</xfm:caption>
		<xfm:hint id="hCheckIntegrity">Check the DocumentBase integrity</xfm:hint>
		<xfm:hint id="hCheckIntegrityApplication">Check the application integrity</xfm:hint>

	</xfm:messages>

	<!-- ======  MESSAGES @xml:lang="es"  ====================== -->
	<xfm:messages xml:lang="es">
		<xfm:caption id="title">Administraci&#243;n de SDX - Uso - Explorar</xfm:caption>

		<!-- Titulos -->
		<xfm:caption id="tApp">Uso</xfm:caption>
		<xfm:caption id="tContents">Tabla de contenido</xfm:caption>
		<xfm:caption id="tData">Contenido del uso</xfm:caption>

		<!-- Los datos de la pagina comienzan -->
		<xfm:caption id="headDefault">Valores prefijados</xfm:caption>
		<xfm:caption id="headDir">Directorios</xfm:caption>
		<xfm:caption id="headDb">Base de datos interna de los usuarios</xfm:caption>
		<xfm:caption id="headSessionObject">L&#237;mite del objeto de la sesi&#243;n</xfm:caption>
		<xfm:caption id="headClass">Clases</xfm:caption>
		<xfm:caption id="headLib">Bibliotecas</xfm:caption>
		<xfm:caption id="headThes">Tesoros</xfm:caption>
		<xfm:caption id="headDbs">Base de datos interna de los usuarios</xfm:caption>
		<xfm:caption id="headDocBases">Bases del documento de los usuarios</xfm:caption>

		<!-- Datos multiples de los sucesos (soporte de 'g' para 'global') -->
		<xfm:caption id="gEmpty">Ning&#250;n Valor</xfm:caption>
		<xfm:caption id="gTrue">verdadero</xfm:caption>
		<xfm:caption id="gFalse">falso</xfm:caption>
		<xfm:caption id="gEncode">Codificaci&#243;n</xfm:caption>
		<xfm:caption id="gDocBases">Bases Del Documento</xfm:caption>
		<xfm:caption id="gDocBase">Base Del Documento</xfm:caption>
		<xfm:caption id="gDataBases">Bases de datos interna</xfm:caption>
		<xfm:caption id="gDataBase">Base de datos interna</xfm:caption>
		<xfm:caption id="gRepos">Dep&#243;sitos</xfm:caption>
		<xfm:caption id="gRepo">Dep&#243;sito</xfm:caption>
		<xfm:caption id="gFieldList">Lista de los campos</xfm:caption>
		<xfm:caption id="gDir">Directorio</xfm:caption>
		<xfm:caption id="gProps">Caracter&#237;sticas</xfm:caption>
		<xfm:caption id="gIsDefault">Es el defecto</xfm:caption>
		<xfm:caption id="gLocale">Localizaci&#243;n</xfm:caption>
		<xfm:caption id="gName">Nombre</xfm:caption>
		<xfm:caption id="gType">Tipo</xfm:caption>
		<xfm:caption id="gDateSDX">Fecha de SDX</xfm:caption>
		<xfm:caption id="gDateISO">Fecha de ISO-8601</xfm:caption>
		<xfm:caption id="gDateMilli">Feche en milisegundos</xfm:caption>
		<xfm:caption id="gUrlDoc">URL de documento</xfm:caption>
		<xfm:caption id="gWordAnalyzer">Analizador De la Palabra</xfm:caption>
		<xfm:caption id="gDocCount">Documente a conde</xfm:caption>

		<!-- Datos especificos -->
		<xfm:caption id="docBaseType">Tipo del bajo documento</xfm:caption>
		<xfm:caption id="thesType">Tipo del tesoro</xfm:caption>
		<xfm:caption id="adminIdGroup">Identificaci&#243;n de grupo del administrador</xfm:caption>
		<xfm:caption id="adminIdUser">Identificaci&#243;n del usuario del administrador</xfm:caption>
		<xfm:caption id="defaultRepo">Dep&#243;sito del defecto</xfm:caption>
		<xfm:caption id="keepOriginal">Guardar los documentos originales</xfm:caption>
		<xfm:caption id="pipeline">Pipeline <em style="font-style:italic">(Tuber&#237;a)</em></xfm:caption>
		<xfm:caption id="transformation">Transformaci&#243;n</xfm:caption>
		<xfm:caption id="index">&#205;ndice</xfm:caption>
		<xfm:caption id="engine">Motor</xfm:caption>
		<xfm:caption id="path">Trayectoria</xfm:caption>
		<xfm:caption id="readOnly">Le&#237;do solamente</xfm:caption>
		<xfm:caption id="useCache">Utilice el escondrijo</xfm:caption>
		<xfm:caption id="checkOnGet">Consigue el cheque encendido</xfm:caption>
		<xfm:caption id="baseDirectory">Directorio bajo</xfm:caption>
		<xfm:caption id="docDirectory">Directorio de documento</xfm:caption>
		<xfm:caption id="filePrefix">Prefijo del archivo</xfm:caption>
		<xfm:caption id="fileSuffix">Sufijo del archivo</xfm:caption>
		<xfm:caption id="extent">Grado</xfm:caption>
		<xfm:caption id="depth">Profundidad</xfm:caption>
		<xfm:caption id="fields">Campos</xfm:caption>
		<xfm:caption id="defaultField">Campo del defecto</xfm:caption>
		<xfm:caption id="ignoreCase">No haga caso del caso</xfm:caption>
		<xfm:caption id="number">N</xfm:caption>
		<xfm:caption id="default">Defecto</xfm:caption>
		<xfm:caption id="brief">Escrito</xfm:caption>
		<xfm:caption id="config">Config</xfm:caption>
		<xfm:caption id="dbMember">Miembros</xfm:caption>
		<xfm:caption id="dbJDBCTableName">Nombre de mesa de JDBC</xfm:caption>
		<xfm:caption id="useCF">Utilice los archivos compuestos</xfm:caption>
		<xfm:caption id="dbType">De tipo base de datos</xfm:caption>
		<xfm:caption id="indexCreate">Fecha de creaci&#243;n</xfm:caption>
		<xfm:caption id="indexMaj">Dure la fecha de la modificaci&#243;n</xfm:caption>
		<xfm:caption id="source">Fuente</xfm:caption>
		<xfm:caption id="relation">Relaci&#243;n</xfm:caption>
		<xfm:caption id="baseURL">URL b&#225;sico</xfm:caption>
		<xfm:caption id="dataSrcId">Identificaci&#243;n de fuente de datos</xfm:caption>
		<xfm:caption id="storeTermVector">TermVector</xfm:caption>
		<xfm:caption id="autoOptimize">Optimizaci&#243;n autom&#225;tica</xfm:caption>
		<xfm:caption id="isOptimized">Optimizado</xfm:caption>
		<xfm:caption id="indexFullSize">Tama&#241;o total de &#237;ndice</xfm:caption>
		<xfm:caption id="indexCountFiles">Los archivos del &#237;ndice cuentan</xfm:caption>
		<xfm:caption id="indexFileSize">El tama&#241;o m&#225;ximo para un archivo del &#237;ndice</xfm:caption>
		<xfm:caption id="indexMaxDoc">El m&#225;ximo index&#243; documento para un archivo del &#237;ndice</xfm:caption>

		<!-- Datos de OAI -->
		<xfm:caption id="oaiIdentify">Identifique</xfm:caption>
		<xfm:caption id="oaiRepoName">Nombre del dep&#243;sito</xfm:caption>
		<xfm:caption id="oaiBaseUrl">URL de la base</xfm:caption>
		<xfm:caption id="oaiProtocol">Versi&#243;n del protocolo</xfm:caption>
		<xfm:caption id="oaiMail">Correo del admin</xfm:caption>
		<xfm:caption id="oaiTimeStamp">El timestamp m&#225;s temprano</xfm:caption>
		<xfm:caption id="oaiDelete">Expediente suprimido</xfm:caption>
		<xfm:caption id="oaiGranularity">Formato</xfm:caption>

		<!-- Information message for the superuser -->
		<xfm:caption id="nOptimizationDone">Optimizaci&#243;n exitosa</xfm:caption>
		<xfm:caption id="nNullDocumentBase">El DocumentBase solicitado no existe para esta aplicaci&#243;n</xfm:caption>
		<xfm:caption id="nSaveApplicationDone">Aplicaci&#243;n de respaldo exitosa</xfm:caption>
		<xfm:caption id="nCheckIntegrityDone">Cheque de integridad exitoso</xfm:caption>

		<!-- Button caption and hints -->
		<xfm:caption id="iOptimizeBaseDoc">Optimice</xfm:caption>
		<xfm:hint id="hOptimize">Optimice el bases del documento, principalmente el &#237;ndice (largo).</xfm:hint>
		<xfm:hint id="hOptimizeApplication">Optimice todo bases del documento (largo) de la aplicaci&#243;n. </xfm:hint>
		<xfm:caption id="iSaveApplication">Respaldo</xfm:caption>
		<xfm:hint id="hSaveApplication">Haga un respaldo repleto de la aplicaci&#243;n</xfm:hint>
		<xfm:caption id="iCheckIntegrity">Integridad</xfm:caption>
		<xfm:hint id="hCheckIntegrity">Verifique la integridad debBases del documento</xfm:hint>
		<xfm:hint id="hCheckIntegrityApplication">Verifique la integridad de la aplicaci&#243;n</xfm:hint>

	</xfm:messages>

	<!-- ======  MESSAGES @xml:lang="pt"  ====================== -->
	<xfm:messages xml:lang="pt">
		<xfm:caption id="title">Administra&#231;&#227;o de SDX - Aplica&#231;&#227;o - Explorar</xfm:caption>

		<!-- Titulos -->
		<xfm:caption id="tApp">Aplica&#231;&#227;o</xfm:caption>
		<xfm:caption id="tContents">tabela da &#237;ndices</xfm:caption>
		<xfm:caption id="tData">&#205;ndice da aplica&#231;&#227;o</xfm:caption>

		<!-- Os dados da pagina comecam -->
		<xfm:caption id="headDefault">Valores de defeito</xfm:caption>
		<xfm:caption id="headDir">Diret&#243;rios</xfm:caption>
		<xfm:caption id="headDb">Base de dados interna dos usu&#225;rios</xfm:caption>
		<xfm:caption id="headSessionObject">Limite do objeto da sess&#227;o</xfm:caption>
		<xfm:caption id="headClass">Classes</xfm:caption>
		<xfm:caption id="headLib">Bibliotecas</xfm:caption>
		<xfm:caption id="headThes">Enciclop&#233;dias</xfm:caption>
		<xfm:caption id="headDbs">Base de dados interna dos usu&#225;rios</xfm:caption>
		<xfm:caption id="headDocBases">Base do documento dos usu&#225;rios</xfm:caption>

		<!-- Dados multiplos dos ocorrencias (carrinho de 'g' para 'global') -->
		<xfm:caption id="gEmpty">Nenhum Valor</xfm:caption>
		<xfm:caption id="gTrue">verdadero</xfm:caption>
		<xfm:caption id="gFalse">falso</xfm:caption>
		<xfm:caption id="gEncode">Codifica&#231;&#227;o</xfm:caption>
		<xfm:caption id="gDocBases">Base do documentos</xfm:caption>
		<xfm:caption id="gDocBase">Base do documentos</xfm:caption>
		<xfm:caption id="gDataBases">Base de dados interna</xfm:caption>
		<xfm:caption id="gDataBase">Base de dados interna</xfm:caption>
		<xfm:caption id="gRepos">Reposit&#243;rios</xfm:caption>
		<xfm:caption id="gRepo">Reposit&#243;rio</xfm:caption>
		<xfm:caption id="gFieldList">Lista dos campos</xfm:caption>
		<xfm:caption id="gDir">Diret&#243;rio</xfm:caption>
		<xfm:caption id="gProps">Propriedades</xfm:caption>
		<xfm:caption id="gIsDefault">&#201; o defeito</xfm:caption>
		<xfm:caption id="gLocale">Localiza&#231;&#227;o</xfm:caption>
		<xfm:caption id="gName">Nome</xfm:caption>
		<xfm:caption id="gType">Tipo</xfm:caption>
		<xfm:caption id="gDateSDX">Data de SDX</xfm:caption>
		<xfm:caption id="gDateISO">Data de ISO-8601</xfm:caption>
		<xfm:caption id="gDateMilli">Date em milliseconds</xfm:caption>
		<xfm:caption id="gUrlDoc">URL do documento</xfm:caption>
		<xfm:caption id="gWordAnalyzer">Analisador da Palavra</xfm:caption>
		<xfm:caption id="gDocCount">Documente contagem</xfm:caption>

		<!-- Dados especificos -->
		<xfm:caption id="docBaseType">Tipo do basear do documentos</xfm:caption>
		<xfm:caption id="thesType">Tipo da enciclop&#233;dia</xfm:caption>
		<xfm:caption id="adminIdGroup">Grupo ID do administrador</xfm:caption>
		<xfm:caption id="adminIdUser">Usu&#225;rio ID do administrador</xfm:caption>
		<xfm:caption id="defaultRepo">Reposit&#243;rio do defeito</xfm:caption>
		<xfm:caption id="keepOriginal">Mantenha os originais documentos</xfm:caption>
		<xfm:caption id="pipeline">Pipeline <em style="font-style:italic">(Encanamento)</em></xfm:caption>
		<xfm:caption id="transformation">Transforma&#231;&#227;o</xfm:caption>
		<xfm:caption id="index">&#205;ndice</xfm:caption>
		<xfm:caption id="engine">Motor</xfm:caption>
		<xfm:caption id="path">Trajeto</xfm:caption>
		<xfm:caption id="readOnly">Lido somente</xfm:caption>
		<xfm:caption id="useCache">Usa o esconderijo</xfm:caption>
		<xfm:caption id="checkOnGet">O cheque em receber</xfm:caption>
		<xfm:caption id="baseDirectory">Diret&#243;rio do basear</xfm:caption>
		<xfm:caption id="docDirectory">Diret&#243;rio de documento</xfm:caption>
		<xfm:caption id="filePrefix">Prefixo da lima</xfm:caption>
		<xfm:caption id="fileSuffix">Sufixo da lima</xfm:caption>
		<xfm:caption id="extent">Extens&#227;o</xfm:caption>
		<xfm:caption id="depth">Profundidade</xfm:caption>
		<xfm:caption id="fields">Campos</xfm:caption>
		<xfm:caption id="defaultField">Campo do defeito</xfm:caption>
		<xfm:caption id="ignoreCase">Ignore o caso</xfm:caption>
		<xfm:caption id="number">N</xfm:caption>
		<xfm:caption id="default">Defeito</xfm:caption>
		<xfm:caption id="brief">Sum&#225;rio</xfm:caption>
		<xfm:caption id="config">Config</xfm:caption>
		<xfm:caption id="dbMember">Membros</xfm:caption>
		<xfm:caption id="dbJDBCTableName">Nome de tabela de JDBC</xfm:caption>
		<xfm:caption id="useCF">Use arquivos compostos</xfm:caption>
		<xfm:caption id="dbType">Tipo de base de dados</xfm:caption>
		<xfm:caption id="indexCreate">Data de cria&#231;&#227;o</xfm:caption>
		<xfm:caption id="indexMaj">Dure data de modifica&#231;&#227;o</xfm:caption>
		<xfm:caption id="source">Fonte</xfm:caption>
		<xfm:caption id="relation">Rela&#231;&#227;o</xfm:caption>
		<xfm:caption id="baseURL">URL b&#225;sico</xfm:caption>
		<xfm:caption id="dataSrcId">Identifier de fonte de dados</xfm:caption>
		<xfm:caption id="storeTermVector">TermVector</xfm:caption>
		<xfm:caption id="autoOptimize">Optimization autom&#225;tico</xfm:caption>
		<xfm:caption id="isOptimized">Optimizado</xfm:caption>
		<xfm:caption id="indexFullSize">Tamanho total de &#237;ndice</xfm:caption>
		<xfm:caption id="indexCountFiles">Os arquivos de &#237;ndice contam</xfm:caption>
		<xfm:caption id="indexFileSize">Tamanho m&#225;ximo para um arquivo de &#237;ndice</xfm:caption>
		<xfm:caption id="indexMaxDoc">Documento m&#225;ximo de indexed para um arquivo de &#237;ndice</xfm:caption>

		<!-- Dados de OAI -->
		<xfm:caption id="oaiIdentify">Identifique</xfm:caption>
		<xfm:caption id="oaiRepoName">Nome do reposit&#243;rio</xfm:caption>
		<xfm:caption id="oaiBaseUrl">URL da basear</xfm:caption>
		<xfm:caption id="oaiProtocol">Vers&#227;o do protocolo</xfm:caption>
		<xfm:caption id="oaiMail">Correio do admin</xfm:caption>
		<xfm:caption id="oaiTimeStamp">O timestamp o mais adiantado</xfm:caption>
		<xfm:caption id="oaiDelete">Registro suprimido</xfm:caption>
		<xfm:caption id="oaiGranularity">Formato</xfm:caption>

		<!-- Information message for the superuser -->
		<xfm:caption id="nOptimizationDone">O Optimization bem sucedido</xfm:caption>
		<xfm:caption id="nNullDocumentBase">O DocumentBase solicitado n&#227;o existe para esta aplica&#231;&#227;o</xfm:caption>
		<xfm:caption id="nSaveApplicationDone">O apoio de aplica&#231;&#227;o bem sucedido</xfm:caption>
		<xfm:caption id="nCheckIntegrityDone">O cheque de integridade bem sucedido</xfm:caption>

		<!-- Button caption and hints -->
		<xfm:caption id="iOptimizeBaseDoc">Optimizar</xfm:caption>
		<xfm:hint id="hOptimize">Optimizar o base do documento, principalmente o &#237;ndice (longo).</xfm:hint>
		<xfm:hint id="hOptimizeApplication">Optimize toda aplica&#231;&#227;o base do documento (longo). </xfm:hint>
		<xfm:caption id="iSaveApplication">Apoio</xfm:caption>
		<xfm:hint id="hSaveApplication">Fa&#231;a um pleno apoio de aplica&#231;&#227;o</xfm:hint>
		<xfm:caption id="iCheckIntegrity">Integridade</xfm:caption>
		<xfm:hint id="hCheckIntegrity">Verifique a integridade de base do documento</xfm:hint>
		<xfm:hint id="hCheckIntegrityApplication">Verifique a integridade de aplica&#231;&#227;o</xfm:hint>

	</xfm:messages>

	<o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>