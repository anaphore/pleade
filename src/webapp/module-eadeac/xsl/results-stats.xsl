<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Post-traitement du HTML pour sortir une ligne de stats -->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:h="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="xsl">

	<!-- On intercepte le div qui contient la requête pour les stats (et on ne le sort pas) -->
	<xsl:template match="h:div[@id = 'pl-results-stats-info']">
		<!-- <xsl:variable name="url" select="concat(normalize-space(.), urle:encode(normalize-space(//h:span[@id = 'pl-results-query-text'][1]), 'UTF-8'))" xmlns:urle="java:java.net.URLEncoder"/>-->
		<xsl:variable name="url" select="concat(normalize-space(.), normalize-space(//h:span[@id = 'pl-results-query-text'][1]))"/>
		<xsl:apply-templates select="document($url)/*" mode="stats"/>
	</xsl:template>
	<xsl:template match="node()" mode="stats"/>

	<!-- De manière générale on copie -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- Interception des noeuds a namespace vide ! -->
	 <xsl:template match="*[namespace-uri()='']">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
