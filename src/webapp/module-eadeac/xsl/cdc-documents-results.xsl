<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		Cette XSLT permet de sortir les documents d'une entrée de cadre de classement,
		mais uniquement s'ils font partie d'un résultat de recherche.
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	exclude-result-prefixes="xsl">

	<!-- Les termes root-id -->
	<xsl:variable name="terms-root-id" select="/root/sdx:document/sdx:terms[@name = 'root-id']"/>

	<!-- L'élément racine du aggregate : on traite les résultats -->
	<xsl:template match="/root">
		<xsl:apply-templates select="sdx:results"/>
	</xsl:template>

	<!-- Pour les résultats, on filtre -->
	<xsl:template match="sdx:result">
		<xsl:variable name="docId" select="sdx:field[@name = 'sdxdocid']/@value"/>
		<xsl:if test="$terms-root-id/sdx:term[@value = $docId]">
			<xsl:copy>
				<xsl:attribute name="nbresults"><xsl:value-of select="$terms-root-id/sdx:term[@value = $docId]/@docs"/></xsl:attribute>
				<xsl:apply-templates select="@*|node()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- En général, on copie -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
