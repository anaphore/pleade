<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Export ZIP du panier
    +-->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
    xmlns:zip="http://apache.org/cocoon/zip-archive/1.0"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:dir="http://apache.org/cocoon/directory/2.0"
    exclude-result-prefixes="xsl sdx">

  <xsl:import href="../../commons/xsl/functions.xsl"/>
  <xsl:import href="link-helpers.xsl"/>

  <xsl:param name="debug" select="'false'"/>
  <xsl:param name="eadeac-basket-zip-include-doc" select="'true'"/>
  <xsl:param name="internal-img-server-base" select="'cocoon://img-server/'"/><!-- URL interne du serveur d'image -->

  <xsl:template match="/">
    <zip:archive>
      <!-- Inclusion des pièces attachées dans le zip du panier -->
      <xsl:apply-templates/>
      <!-- DEBUG : le contenu -->
      <xsl:if test="$debug = 'true'">
        <debug_content>
          <xsl:copy-of select="/"/>
        </debug_content>
      </xsl:if>
    </zip:archive>
  </xsl:template>

  <xsl:template match="sdx:result">
    <xsl:variable name="id" select="sdx:field[@name='sdxdocid']/@value"/>
    <xsl:variable name="frag-url" select="concat('cocoon://ead-fragment.debug?c=', $id)"/>
    <xsl:variable name="frag" select="if(doc-available($frag-url)) then doc($frag-url)/sdx:document/pleade:subdoc else ()"/>
    <xsl:if test="$frag = ''">
      <xsl:message>
        [zip.xsl]: the fragment with the id `<xsl:value-of select="$id"/>` cannot be retrieved (url was: <xsl:value-of select="$frag-url"/>)
      </xsl:message>
    </xsl:if>
    <xsl:variable name="eadid" select="$frag/@pleade:eadid"/>

    <!-- Inclusion de la notice, si eadeac-basket-zip-include-doc vaut true -->
    <xsl:if test="$eadeac-basket-zip-include-doc = 'true'">
      <zip:entry name="{$id}/{$id}.pdf" src="cocoon://ead.pdf?c={$id}"/>
    </xsl:if>

    <!-- Les propriétés d'affichage -->
    <xsl:variable name="prop-url" select="concat('cocoon:/functions/ead/get-properties/', $eadid, '/display.xml')" />
    <xsl:variable name="display-properties" select="
        if(doc-available($prop-url)) then document($prop-url)/properties
        else ''
      "/>

    <!-- Inclusion des documents attachés -->
    <!-- Les archref, extref, ref et extrefloc -->
    <xsl:for-each select="$frag//*[local-name()='ref' or local-name()='archref' or local-name()='extref' or local-name()='extrefloc']">
      <!-- Ce code est repris de ead2html.xsl -->
      <xsl:choose>
        <xsl:when test="(self::ref or self::archref or self::extref or self::extrefloc) and not(@xpointer!='') and not(@href!='') and not(@target != '')">
          <!-- On a juste une référence, on ne fait rien ici -->
        </xsl:when>
        <xsl:otherwise>
          <!-- Le type de lien (attached, relative, external, ...) -->
          <xsl:variable name="type">
            <!-- En général on le connaît déjà, sauf si c'est un lien vers un autre IR publié -->
            <xsl:choose>
              <xsl:when test="@pleade:link-type = 'relative'">
                <!-- On doit vérifier les propriétés d'affichage -->
                <xsl:variable name="ext" select="pleade:get-extension(@href)"/>
                <!-- Les extensions à considérer comme liens vers des IR
                      Si on n'a pas de propriétés d'affichage (un bogue de l'application), on traite les liens avec extension 'xml'.-->
                <xsl:variable name="exts" select="if($display-properties and $display-properties/property[@name = 'ead-link-extensions']) then $display-properties/property[@name = 'ead-link-extensions'] else 'xml html'" />
                <xsl:variable name="ead-link-extensions" select="fn:tokenize($exts, '\s')"/>
                <xsl:choose>
                  <xsl:when test="$ead-link-extensions[. = $ext]">
                    <!-- C'est un lien vers un IR -->
                    <xsl:value-of select="'ead'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <!-- Sinon on conserve le type original (relative) -->
                    <xsl:value-of select="@pleade:link-type"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <!-- Sinon on le connaît déjà -->
                <xsl:value-of select="@pleade:link-type"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <!-- L'adresse visible du lien, on a une fonction spécifique pour cela -->
          <xsl:variable name="public-url" select="pleade:get-public-url(., $type, $eadid)"/>

          <!-- On n'intervient que sur les liens quie en sont pas des documents EAD, on ignore les mailto et les liens externes -->
          <!-- FIXME: les liens externes ne sont pas ignorés ! -->
          <xsl:if test="$type != 'internal' and $type != 'ead' and not(starts-with($public-url, 'mailto:'))">
            <!-- L'URL du document, on a une fonction spécifique pour cela -->
            <xsl:variable name="public-url" select="pleade:get-public-url(., $type, $eadid)"/>
            <zip:entry name="{$id}/{@href}" src="cocoon://{$public-url}"/>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>

    <!-- Les dao -->
    <xsl:for-each select="$frag//*[local-name()='dao' or local-name()='daoloc']">
      <!-- L'adresse publique du document -->
      <xsl:variable name="public-url" select="pleade:get-public-url(., @pleade:link-type, $eadid)"/>

      <xsl:variable name="thumb" select="if(../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail']) then ../*[@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'][1] else self::*" />

      <!--  Le HTML va dépendre du type d'objet -->
      <xsl:choose>
        <xsl:when test="@role='last' or @role='image:last' or @role='navimages:image:last'"/><!-- On ne traite pas les aperçus ici -->
        <xsl:when test="@role='thumbnail' or @role='image:thumbnail' or @role='navimages:image:thumbnail'"/><!-- On ne traite pas les aperçus ici -->
        <!-- Les images absolues sont résolues autrement : on ouvre pas la visionneuse pour les afficher. -->
        <!-- Les images, en série ou non, on va afficher un aperçu et un lien pour voir en grand écran -->
        <xsl:when test="@pleade:link-type!='absolute' and (@pleade:link-type = 'series' or @pleade:mime-type = 'image/jpeg' or @pleade:mime-type = 'image/gif' or @pleade:mime-type = 'image/png')">
          <!--<xsl:apply-templates select="." mode="pleade-image"/>-->
          <xsl:variable name="eadimage-path" select="concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', @pleade:url, 'eadimage.xml?pleade-url=', @pleade:url, '&amp;pleade-img=', @pleade:img)"/>
          <xsl:if test="$debug = 'true'"><xsl:message>        Loading: <xsl:value-of select="concat($eadimage-path,' (', current-time(), ')')"/></xsl:message></xsl:if>
          <xsl:variable name="eadimage" select="if($thumb/@pleade:link-type = 'series' and doc-available($eadimage-path)) then document($eadimage-path)/result/eadimage else ()"/>
          <xsl:if test="$debug = 'true'"><xsl:message>        Loaded: <xsl:value-of select="concat($eadimage-path,' (', current-time(), ')')"/></xsl:message></xsl:if>

          <xsl:choose>
            <xsl:when test="@pleade:link-type!='attached' and @pleade:link-type!='relative' and not($eadimage/@find='true')">
              <!-- On ne fait rien -->
              <xsl:if test="$debug"><xsl:message>        Image non attachee et non relative introuvable dans les racines</xsl:message></xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <!-- Si le rôle est 'first', on cherche la présence d'un 'last' pour le passer également -->
              <xsl:variable name="last" select="if(@role='first' or @role='image:first' or @role='navimages:image:first') then ../*[@role='last' or @role='image:last' or @role='navimages:image:last'][1] else ()"/>
              <!-- On va afficher un lien vers la visionneuse -->
              <xsl:variable name="images" select="pleade:getImages(., $last, $id, $eadid)"/>
              <xsl:if test="$debug"><xsl:message>          Ending function pleade:getImages = <xsl:value-of select="$images"/></xsl:message></xsl:if>
              <xsl:copy-of select="$images"/>
            </xsl:otherwise>
          </xsl:choose>

        </xsl:when>
        <!-- Un lien DAO qui n'a pas pu être résolu correctement lors de l'indexation -->
        <xsl:when test="@pleade:link-type = 'dao-link-error'"/><!-- On ne fait rien dans ce cas -->
        <!-- Pour les images absolues, on affiche la vignette avec un lien hypertexte pour ouvrir une popup. -->
        <xsl:when test="starts-with(@pleade:mime-type, 'image/')">
          <zip:entry name="{$id}/{@href}" src="cocoon://{@pleade:url}"/>
        </xsl:when>
        <xsl:otherwise>
          <zip:entry name="{$id}/{@href}" src="cocoon://{$public-url}"/>
        </xsl:otherwise>
      </xsl:choose>

    </xsl:for-each>

    <xsl:if test="$debug = 'true'">
      <debug_fragment>
        <xsl:copy-of select="$frag"/>
      </debug_fragment>
    </xsl:if>
  </xsl:template>

  <!--
        Fonction qui retourne le lien vers la série d'images (basé sur pleade:linkToViewer de ead2html.xsl)
      -->
  <xsl:function name="pleade:getImages">
    <xsl:param name="el"/>
    <xsl:param name="last"/>
    <xsl:param name="id"/>
    <xsl:param name="eadid"/>
    <xsl:choose>
      <xsl:when test="$el/@pleade:link-type = 'series'">
        <!-- Le début est toujours pareil -->

        <xsl:variable name="serie-url">
          <xsl:value-of select="concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', $el/@pleade:url, '/dir.xml')"/>
          <xsl:choose>
            <xsl:when test="$el/@role='image' or $el/@role='navimages:image'">
              <xsl:value-of select="concat('?name=', $el/@pleade:img)"/>
            </xsl:when>
            <xsl:when test="$el/@role='first' or $el/@role='image:first' or $el/@role='navimages:image:first'">
              <xsl:value-of select="concat('?np=', $el/@pleade:img)" />
              <xsl:if test="$last">
                <xsl:value-of select="concat('&amp;nd=', $last/@pleade:img)" />
              </xsl:if>
            </xsl:when>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="serie" select="if(doc-available($serie-url)) then doc($serie-url)/dir:directory else ()"/>
        <xsl:choose>
          <xsl:when test="$serie = ''">
            <xsl:message>
              [zip.xsl]: requested serie could not be found (url was `<xsl:value-of select="$serie-url"/>`)
            </xsl:message>
          </xsl:when>
          <xsl:otherwise>
            <xsl:for-each select="$serie/dir:file">
              <zip:entry name="{$id}/{concat($serie/@chemin-serie, @name)}" src="{concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', $serie/@chemin-serie, @name)}"/>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- Une image relative -->
      <xsl:when test="$el/@pleade:link-type = 'relative'">
        <xsl:variable name="img" select="concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', '_ead/', pleade:get-public-url($el, $el/@pleade:link-type, $eadid))" />
        <zip:entry name="{$id}/{$el/@href}" src="{$img}"/>
      </xsl:when>
      <xsl:otherwise>
        <!-- Une image attachée -->
        <xsl:variable name="img" select="concat($internal-img-server-base, if(ends-with($internal-img-server-base, '/')) then '' else '/', '_ead/', pleade:get-public-url($el, $el/@pleade:link-type, $eadid))"/>
        <zip:entry name="{$id}/{$el/@href}" src="{$img}"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>