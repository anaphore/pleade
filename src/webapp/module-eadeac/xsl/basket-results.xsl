<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Coeur de page : affichage du contenu du panier
    |
    | Cette XSL prend en compte les documents de la base EAD
    | Pour les autres bases, l'affichage doit être pris en charge par l'XSLT qui
    | surchage dans une fonction dont la signature est :
    | <xsl:template match="sdx:result[sdx:field[@name='sdxdbid']/@value='{$base}']" mode="result">
    |
    | TODO (MP) : Implémenter l'affichage des doc EAC ?
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns="http://www.w3.org/1999/xhtml"
                              xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                              xmlns:xsp="http://apache.org/xsp"
                              xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                              xmlns:pleade="http://pleade.org/ns/pleade/1.0"
                              xmlns:fn="http://www.w3.org/2005/xpath-functions"
                              exclude-result-prefixes="xsl xsp sdx i18n pleade fn">

  <!-- Fonctions d'utilités générales -->
  <xsl:import href="../../commons/xsl/functions.xsl"/>
  <!-- Le code pour traiter la navigation d'une page à l'autre -->
  <xsl:import href="navigation.xsl"/>

  <!-- Activation ou non de l'export PDF -->
  <xsl:param name="active-basket-pdf" select="'true'"/>
  <!-- Activation ou non de l'export ZIP -->
  <xsl:param name="active-basket-zip" select="'true'"/>
  <!-- Activation du panier permanent -->
  <xsl:param name="active-basket-permanent" select="'true'"/>
  <!-- Identifiant du panier qui a été demandé -->
  <xsl:param name="stored_basket_id" />
  <!-- Les rôles de l'utilisateur -->
  <xsl:param name="userroles"/>

  <!-- Tri stocké en session -->
  <xsl:param name="sort_field" select="''"/>
  <xsl:param name="sort_order" select="''"/>
  
  <!--+
      | Création de la racine XHTML
      +-->
  <xsl:template match="/sdx:document">
    <html>
      <xsl:apply-templates select="." mode="html-head" />
      <xsl:apply-templates select="." mode="html-body" />
    </html>
  </xsl:template>

  <!--+
      | Construction de l'en-tête XHTML
      +-->
  <xsl:template match="sdx:document" mode="html-head">

    <head>

      <title>
        <i18n:text key="basket.html.title">panier</i18n:text>
      </title>

      <xsl:apply-templates select="." mode="script-head" />
    </head>

  </xsl:template>

  <!--+
      | Construction du corps de page XHTML
      +-->
  <xsl:template match="sdx:document" mode="html-body">
    <body>
      <h1 class="pl-title pl-box pl-box-title">
        <i18n:text key="basket.page.title">porte-documents</i18n:text>
      </h1>
      <i18n:text key="basket.intro"/>
      <xsl:if test="contains($userroles, 'user')">
        <xsl:variable name="stored_baskets-url" select="'cocoon://functions/stored/basket/list.xml'"/>
        <xsl:if test="doc-available($stored_baskets-url)">
          <xsl:variable name="stored_baskets" select="doc($stored_baskets-url)/basket"/>
          <div id="pl-stored_baskets">
            <label for="stored_baskets">Paniers enregistrés</label>
            <select name="stored_baskets" id="stored_baskets">
              <option value="">-</option>
              <xsl:for-each select="$stored_baskets/basket">
                <option value="{@id}">
                  <xsl:if test="@id = $stored_basket_id">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="name"/>
                </option>
              </xsl:for-each>
            </select>
          </div>
          <script type="text/javascript">
            <xsl:comment>
              window.addEvent("load", function(o,i){
                basket.initStoredBaskets();
              });
            //</xsl:comment>
          </script>
        </xsl:if>
      </xsl:if>
      <xsl:apply-templates select="sdx:caddy" mode="total-server"/>
    </body>
  </xsl:template>

  <!--+
      | Règle surchargeable pour sortir des bouts de Javascripts supplémentaires
      | dans l'en-tête XHTML
      +-->
  <xsl:template match="sdx:document" mode="script-head">
    <!-- Pour les onglets -->
    <script type="text/javascript" src="yui/tabview/tabview-min.js">//</script>
    <link type="text/css" href="yui/assets/skins/pleade/tabview.css" rel="stylesheet" />

    <script type="text/javascript" src="i18n/module-eadeac-basket-js.js">//</script>

    <script type="text/javascript">
      <xsl:comment>
      var selecteds = new Array();
      var selectedBases = new Array();
      //</xsl:comment>
    </script>

    <xsl:apply-templates select="." mode="script-head-total-server" />

  </xsl:template>

  <!--+
      | Script d'en-tête, pour les surcharges
      +-->
  <xsl:template match="sdx:document" mode="script-head-total-server"></xsl:template>

   <!--+
       | Affichage d'un message
       +-->
  <xsl:template match="message">
    <p class="pl-message">
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <!--+
      | Message de panier vide
      +-->
  <xsl:template match="sdx:caddy" mode="no-content">
    <p class="pl-form-message">
      <i18n:text key="basket.results-title.none">votre panier est vide</i18n:text>
    </p>
  </xsl:template>

  <!--+
      | Traitement du panier en mode "total-server"
      +-->
  <xsl:template match="sdx:caddy" mode="total-server">

    <xsl:choose>
      <xsl:when test="not( number(@nb) &gt; 0 )">
        <xsl:apply-templates select="." mode="no-content" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="datatable" />
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!--+
      | Affichage des résultats de recherche
      +-->
  <xsl:template match="sdx:caddy" mode="datatable">

    <!-- Une manière de compter les bases de documents dans le panier : un | par base -->
    <xsl:variable name="nb-bases">
      <xsl:apply-templates select="." mode="nb-bases" />
    </xsl:variable>
    <!-- Elément courant -->
    <xsl:variable name="current" select="."/>
  
    <div class="rslts-cnt">
      <!-- Plusieurs bases dans le panier -->
      <xsl:variable name="tabs">
        <xsl:for-each-group group-by="sdx:document/@base" select="/sdx:document/sdx:caddy">
          <xsl:sort order="descending" select="current-grouping-key()" />
          <strong><xsl:value-of select="current-grouping-key()" /></strong>
        </xsl:for-each-group>
      </xsl:variable>
  
      <div class="yui-navset pl-pg-tabs" id="pl-pg-tabs">
        <ul class="yui-nav">
          <xsl:for-each select="$tabs/*">
            <xsl:variable name="v" select="normalize-space(.)" />
            <li>
              <xsl:if test="position()=1">
                <xsl:attribute name="class"><xsl:text>selected</xsl:text></xsl:attribute>
              </xsl:if>
              <a href="#{$v}"><em><i18n:text key="basket.base.name.{$v}">Base &quot;<xsl:value-of select="$v" />&quot;</i18n:text></em></a>
            </li>
          </xsl:for-each>
        </ul><!-- /yui-nav -->
        <div class="yui-content">
          <xsl:for-each select="$tabs/*">
            <xsl:variable name="v" select="normalize-space(.)" />
            <div class="tab" id="{$v}">
              <xsl:apply-templates select="$current" mode="manage-basket-buttons">
                <xsl:with-param name="v" select="$v"/>
              </xsl:apply-templates>
              <div id="{$v}_basket_content">
                <xsl:variable name="basket-content-url" select="concat('cocoon://functions/', $v, '/getBasket.xsp')"/>
                <xsl:if test="doc-available($basket-content-url)">
                  <xsl:copy-of select="doc($basket-content-url)"/>
                </xsl:if>
              </div>
            </div>
          </xsl:for-each>
  
        </div><!-- /yui-content -->
      </div><!-- /pl-pg-tabs -->
    </div><!-- /rslts-cnt -->
  
    <script type="text/javascript">
      <xsl:comment>
         window.addEvent("load", function(o,i){
          basket.initBasketTabs("", "", "<xsl:value-of select="/sdx:document/basket-sortable/@value"/>", "<xsl:value-of select="$sort_field"/>", "<xsl:value-of select="$sort_order"/>");
        });
      //</xsl:comment>
    </script>

  </xsl:template>
  
  <!--+
      | Les boutons de gestion du panier
      +-->
  <xsl:template match="sdx:caddy" mode="manage-basket-buttons">
    <xsl:param name="v"/>
    <xsl:variable name="class" select="string('yui-button yui-push-button basket-button')" />
    <!-- Bouton pour exporter le contenu du panier au format PDF -->
    <xsl:if test="$active-basket-pdf = 'true' and $v = 'ead'">
      <button type="button" title="basket.button.export-pdf.title" i18n:attr="title" id="btn_pdf-{$v}" class="{$class} pl-bskt-bttn-pdf">
        <span class="access">
          <i18n:text key="basket.button.export-pdf.text">
            <xsl:text>exporter au format pdf</xsl:text>
          </i18n:text>
        </span>
      </button>
    </xsl:if>
    <xsl:if test="$active-basket-zip = 'true' and $v = 'ead'">
      <button type="button" title="basket.button.export-zip.title" i18n:attr="title" id="btn_zip-{$v}" class="{$class} pl-bskt-bttn-zip">
        <span class="access">
          <i18n:text key="basket.button.export-zip.text">
            <xsl:text>exporter au format zip</xsl:text>
          </i18n:text>
        </span>
      </button>
    </xsl:if>
    <xsl:if test="contains($userroles, 'user') and $active-basket-permanent = 'true' and $v = 'ead'">
      <button type="button" title="basket.button.save.title" i18n:attr="title" id="btn_save-{$v}" class="{$class} pl-bskt-bttn-save">
        <span class="access">
          <i18n:text key="basket.button.save.text">
            <xsl:text>enregistrer le panier</xsl:text>
          </i18n:text>
        </span>
      </button>
    </xsl:if>
    <button id="btn_update-{$v}" type="button" class="{$class} pl-bskt-dlt" i18n:attr="title" title="basket.button.delete-from-basket.title">
      <i18n:text key="basket.button.delete-from-basket.text">
        <xsl:text>retirer les document sélectionnés</xsl:text>
      </i18n:text>
    </button>
    <button id="btn_flush-{$v}" type="button" class="{$class} pl-bskt-flush" title="basket.button.flush-basket.title" i18n:attr="title">
      <i18n:text key="basket.button.flush-basket.text">
        <xsl:text>vider le panier</xsl:text>
      </i18n:text>
    </button>
  </xsl:template>

  <!--+
      | Fonction pour les bases présentes dans le panier
      +-->
  <xsl:template match="sdx:caddy" mode="nb-bases">
    <xsl:for-each-group group-by="@base" select="/sdx:document/sdx:caddy/sdx:document">
      <xsl:text>|</xsl:text>
    </xsl:for-each-group>
  </xsl:template>

</xsl:stylesheet>
