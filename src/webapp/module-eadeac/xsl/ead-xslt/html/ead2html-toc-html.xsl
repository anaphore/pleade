<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: ead2html-toc.xsl 14503 2009-04-21 09:25:43Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<xsl:stylesheet version="2.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		exclude-result-prefixes="xsl i18n pleade fn">

	<!-- XSLT qui produit une partie de table des matières à sérialiser en JSON.
			Elle s'applique au fichier de table des matières généré par le processus
			de publication de Pleade, mais inclut dans un élément root qui contient
			également des propriétés de publication pour la partie dynamique. -->

	<!-- Méthodes communes -->
	<xsl:import href="common.xsl"/>

	<!-- Pour le mode statique -->
	<xsl:output method="text" indent="yes" encoding="UTF-8" omit-xml-declaration="no"	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>
	<xsl:output name="json" method="text" encoding="UTF-8"/>

	<!-- Le nombre maximal d'enfants à afficher avant de couper (passé par le sitemap) -->
	<xsl:param name="max" select="50"/>

	<!-- Si ce paramètre est un nombre > 1, alors on doit envoyer seulement une
			partie -->
	<xsl:param name="n" select="0"/>
	<!-- la profondeur des niveaux que l'on souhaite afficher. Match le paramètre" toc-init-max-levels" du fichier display.xconf correspondant à l'IR -->
	<xsl:param name="levelsToShowDefault" select="-1"/>
	<!-- Composant à visualiser : valeur du paramètre d'URL c -->
	<xsl:param name="child2show" select="''"/>
	
	<!-- d'abord dans les propriétés du doc, sinon dans les propriétés générales -->
	<xsl:variable name="levelsDoc" select="/root/display-doc/properties[@id = 'display']/property[@name='toc-init-max-levels']"/>
	<xsl:variable name="levelsGen" select="/root/display-gen/properties[@id = 'display']/property[@name='toc-init-max-levels']"/>
	<xsl:variable name="levelsToShow" select="
		if($levelsDoc!='') then ($levelsDoc)
		else if($levelsGen!='') then ($levelsGen)
		else $levelsToShowDefault
	"/>

	<xsl:variable name="qid" select="''"/><!-- obligatoire pour l'import de common.xsl, meme si pas utilisé ici : FIXME  [MR] : comment faire pour éviter ça ?-->
	
	<!-- On se définit une variable pour traiter le cas de l'absence du paramètre -->
	<xsl:variable name="no">
		<xsl:choose>
			<xsl:when test="number($n) and number($n) > 0"><xsl:value-of select="number($n)"/></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Le mode de consultation : dyn ou static -->
	<xsl:param name="mode" select="'static'"/>

	<!-- L'identifiant de l'entrée de tdm à sortir (pour mode dynamique uniquement) -->
	<xsl:param name="c" select="''"/>

	<!-- La propriété sur le contrôle des caractères -->
	<xsl:variable name="title-limit">
		<xsl:variable name="prop" select="/root/properties[@id = 'publication']/property[@name='title-limit']"/>
		<xsl:choose>
			<xsl:when test="$prop and number($prop) and number($prop) > 0"><xsl:value-of select="number($prop)"/></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!--=> FIXME :  ne pas s'occuper de @pleade:break = 'true' ni des dsc-->
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:apply-templates select="ead"/>
			</xsl:when>
			<xsl:otherwise> <!--$mode='dyn'-->
				<!-- En dynamique, on traite l'élément c souhaité -->
				<div>
					<ul id="treeRoot">
						<xsl:apply-templates select="//*[@pleade:id = $c][1]" mode="make-html-dyn">
							<xsl:with-param name="start">0</xsl:with-param>
							<xsl:with-param name="end"><xsl:value-of select="number($levelsToShow)"/></xsl:with-param>
						</xsl:apply-templates>
					</ul>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- FIXME: toujours utile? -->
	<xsl:template match="*" />

	<!-- Traitement du document complet, en mode statique -->
	<xsl:template match="ead">
		<!--génère le fichier json pour le 1er niveau de l'arbre-->
		<xsl:result-document href="{@pleade:id}.ajax-html" format="html">
			<xsl:call-template name="make-html"/>
		</xsl:result-document>
		<!--générer les autres fichiers json-->
		<xsl:apply-templates mode="make-html"/>
	</xsl:template>

	<!-- Traitement d'une entrée en mode statique -->
	<xsl:template match="c" mode="make-html">
		<xsl:if test="c"> <!--pas de json s'il n'y a rien à mettre dedans-->
			<xsl:result-document href="{@pleade:id}.ajax-html" format="html">
					<xsl:call-template name="make-html"/>
			</xsl:result-document>
		</xsl:if>
		<xsl:apply-templates mode="make-html"/>
	</xsl:template>

	<!-- Un noeud racine en mode dynamique -->
	<xsl:template match=" ead | c " mode="make-html-dyn">
		<xsl:param name="start"/>
		<xsl:param name="end"/>
		<xsl:call-template name="make-html">
			<xsl:with-param name="start" select="$start"/>
			<xsl:with-param name="end" select="$end"/>
		</xsl:call-template>
	</xsl:template>

	<!-- Les entrées d'un noeud racine de la toc à sortir -->
	<xsl:template name="make-html">
		<xsl:param name="start"/>
		 <xsl:param name="end"/>
		<!-- Le nombre d'enfants -->
		<xsl:variable name="nb" select="count(*)"/>
		<!-- L'identifiant -->
		<xsl:variable name="id" select="@pleade:id"/>
		<xsl:choose>
			<!-- Le cas où l'on demande une partie seulement : $no > 0, suite à un regroupement préalable -->
			<xsl:when test="$no > 0">
				<xsl:apply-templates select="*[(position() >= $no) and (position() &lt; $no + $max)]" mode="html-content">
					<xsl:with-param name="start" select="$start"/>
					<xsl:with-param name="end" select="$end"/>
				</xsl:apply-templates>
			</xsl:when>
			<!-- Le cas où on ne dépasse pas la limite -->
			<xsl:when test="$nb &lt;= $max">
				<xsl:apply-templates select="*" mode="html-content">
					<xsl:with-param name="start" select="$start"/>
					<xsl:with-param name="end" select="$end"/>
				</xsl:apply-templates>
			</xsl:when>
			<!-- Le cas où on dépasse la limite -->
			<xsl:otherwise>
				<xsl:apply-templates select="*" mode="html-content">
						<xsl:with-param name="start" select="$start"/>
						<xsl:with-param name="end" select="$end"/>
					</xsl:apply-templates>
			
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Sortie d'un élément c en ul -->
	<xsl:template match="c" mode="html-content">
		<xsl:param name="start"/>
		 <xsl:param name="end"/>
		<xsl:call-template name="process-c-html">
				<xsl:with-param name="start" select="$start"/>
				<xsl:with-param name="end" select="$end"/>
		</xsl:call-template>
	</xsl:template>
		
	<!-- Le contenu réel d'un <c> en json -->
	<xsl:template name="process-c-html">
			<xsl:param name="start"/>
		 <xsl:param name="end"/>
		<!-- Etiquette -->
		<li id="{@pleade:id}">
			<xsl:apply-templates select="." mode="write-html-content" />
		<!-- Enfants -->
		<xsl:if test="boolean(c)">
			<ul>
			<xsl:if test="($end = -1) or ($start &lt;= $end) or ($child2show != '' and descendant::c[@pleade:id=$child2show])">
				<xsl:apply-templates select="child::c" mode="html-content">
					<xsl:with-param name="start" select="$start+1"/>
					<xsl:with-param name="end" select="$end"/>
				</xsl:apply-templates>
			</xsl:if>
			</ul>
		</xsl:if>
			</li>
	</xsl:template>
	
	<xsl:template match="c" mode="write-html-content">
		 <!-- lien -->
		 <xsl:variable name="_href" select="concat(@pleade:id, '.html')"/>
    <xsl:if test="@pleade:ancobject">
      <span class="anc_illustrated">&#160;<xsl:comment>u</xsl:comment></span>
    </xsl:if>

     <xsl:if test="@pleade:object-type">
      <xsl:for-each select="tokenize(@pleade:object-type, ' ')">
          <span class="{.}_illustrated">&#160;<xsl:comment>u</xsl:comment></span>
        </xsl:for-each>
    </xsl:if>
		<a name="link" href="{$_href}"><xsl:copy-of select="pleade:get-label(@pleade:title, .)" /></a>
	</xsl:template>

	<!-- Une fonction pour retourner le label -->
	<xsl:function name="pleade:get-label">
		<xsl:param name="title"/>
		<xsl:param name="element"/>
		<xsl:choose>
			<!-- Si on a un titre, on l'utilise -->
			<xsl:when test="$title and $title != ''">
				<xsl:choose>
					<xsl:when test="$title-limit > 0">
						<xsl:choose>
							<xsl:when test="string-length($title) > $title-limit">
								<xsl:variable name="_v" select="substring($title, 1, $title-limit)" />
								<xsl:variable name="_s" select="if ( substring($_v, string-length($_v)) ='.' ) then ' [...]' else '...'" />
								<xsl:value-of select="concat($_v, $_s)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$title"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise><xsl:value-of select="$title"/></xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<i18n:text key="ead.toc.{$element/@pleade:original-element}"></i18n:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>


	<!-- Une fonction pour retourner une adresse d'un noeud -->
	<xsl:function name="pleade:get-href" as="xs:string">
		<!-- L'identifiant du noeud (peut être vide pour les groupes -->
		<xsl:param name="id"/>
		<!-- Le numéro du groupe -->
		<xsl:param name="no"/>
		<!-- Le mode -->
		<xsl:param name="mode"/>
		<xsl:choose>
			<xsl:when test="$mode = 'static'">
				<!-- Ici, on retourne simplement l'id avec .html -->
				<xsl:value-of select="concat($id, '.html')"/>
			</xsl:when>
			<xsl:when test="$no > 0">
				<!-- On a un numéro en mode dynamique, donc c'est une entrée groupée -->
				<xsl:value-of select="concat($id, '.html?n=', $no)"/>
			</xsl:when>
			<xsl:when test="$id != ''">
				<!-- On a un identifiant en mode dynamique, donc c'est une entrée normale -->
				<xsl:value-of select="concat($id, '.html')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:function>

	<xsl:template name="hasChildren">
		<xsl:choose>
			<xsl:when test="c">
				<xsl:text>true</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>