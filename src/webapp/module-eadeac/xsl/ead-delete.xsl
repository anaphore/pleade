<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT construisant l'XHTML de la partie dynamique de la recherche
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		exclude-result-prefixes="xsl xsp sdx i18n pleade fn">

	<!-- Fonctions d'utilités générales -->
	<xsl:import href="../../commons/xsl/functions.xsl"/>
	
	<!-- Définition de paramètres qui vont permettre d'ajuster les clés i18n et le catalogue -->
	<xsl:param name="i18n-cat" select="'module-eadeac-edit'"/>
  <xsl:param name="i18n-root" select="'edit.ead-delete'" />

	<xsl:template match="/sdx:document">
		<xsl:variable name="a">
			<xsl:choose>
				<xsl:when test="fn:sum(number(sdx:deletions/sdx:summary/@deletions))">
					<xsl:value-of select="fn:sum(number(sdx:deletions/sdx:summary/@deletions))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="f">
			<xsl:choose>
				<xsl:when test="fn:sum(number(sdx:deletions/sdx:summary/@failures))">
					<xsl:value-of select="fn:sum(number(sdx:deletions/sdx:summary/@failures))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="d">
			<xsl:choose>
				<xsl:when test="fn:sum(number(sdx:deletions/sdx:summary/@duration))">
					<xsl:value-of select="fn:sum(number(sdx:deletions/sdx:summary/@duration))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<html>

			<head>
				<title>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.html.title">supprimer des documents</i18n:text>
				</title>
			</head>

			<body>

				<h1 class="pl-title">
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.pl.title">supprimer des documents</i18n:text>
				</h1>

				<div>
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.intro" />
				</div>

				<table border="0" cellspacing="0" cellpadding="0" class="pl-ead-delete"
						summary="Resultat de publication de documents...">

					<tbody>

						<tr>

							<td class="label">
								<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.deletions">suppressions</i18n:text>
							</td>
							<td class="value">
								<xsl:value-of select="pleade:format-number($a)" />
							</td>

						</tr>

						<tr>

							<td class="label">
								<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.failures">échecs</i18n:text>
							</td>
							<td class="value">
								<xsl:value-of select="pleade:format-number($f)" />
							</td>

						</tr>

						<tr>

							<td class="label">
								<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.duration">durée</i18n:text>
							</td>
							<td class="value">
								<xsl:value-of select="concat(pleade:format-number($d, '', '2', ':'), ' sec.')" />
							</td>

						</tr>

					</tbody>

				</table>

				<div>
					<xsl:comment>u</xsl:comment>
					<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.conclusion" />
				</div>

			</body>

		</html>

	</xsl:template>

</xsl:stylesheet>
