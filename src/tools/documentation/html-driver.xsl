<?xml version="1.0" encoding="UTF-8"?>
<!--
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!-- Driver file for converting documentation to HTML -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- Importation des outils Docbook -->
	<xsl:import href="@docbook-ajlsm.html.driver@"/>
	<!-- <xsl:import href="http://www.ajlsm.com/docbook-tools/xsl/html/docbook-ajlsm.xsl"/> -->

	<!-- Les messages d'interface -->
	<xsl:variable name="mess" select="document('messages.xml')/messages"/>
	
	<!-- La langue -->
	<xsl:variable name="lang" select="string(/*/@lang)"/>

	<!-- Ajout d'un en-tête standard -->
	<xsl:template name="user.header.content">
		<xsl:call-template name="sortie-entete"/>
		<form style="margin:0" name="nav" class="nav">
			<xsl:comment>
				<xsl:value-of select="name()"/>
			</xsl:comment>
			<xsl:call-template name="toc.ancestors.links">
				<xsl:with-param name="toc.ancestors" select="$toc.ancestors"/>
			</xsl:call-template>
			<xsl:call-template name="toc.select"/>
		</form>
	</xsl:template>
	
	<!-- Ajout d'un pied de page standard -->
	<xsl:template name="user.footer.content">
		<xsl:param name="node" select="."/>
		<div class="pied-de-page">
			<hr/>
			<xsl:value-of select="$mess/pied-de-page/copyright[lang($lang)]"/>
			<br/>
			<b>
				<xsl:value-of select="$mess/pied-de-page/question-avant[lang($lang)]"/>
				<a href="http://sourceforge.net/mail/?group_id=80362">
					<xsl:value-of select="$mess/pied-de-page/listes[lang($lang)]"/>
				</a>
				<xsl:value-of select="$mess/pied-de-page/question-apres[lang($lang)]"/>
				<xsl:text> </xsl:text>
 				<xsl:value-of select="$mess/pied-de-page/download-avant[lang($lang)]"/>
				<a href="https://sourceforge.net/project/showfiles.php?group_id=80362">
					<xsl:value-of select="$mess/pied-de-page/download[lang($lang)]"/>
				</a>
				<xsl:value-of select="$mess/pied-de-page/download-apres[lang($lang)]"/>
			</b>
		</div>
	</xsl:template>
	
	<xsl:variable name="depth">
		<xsl:call-template name="toc.ancestors.path">
			<xsl:with-param name="toc.ancestors" select="$toc.ancestors"/>
		</xsl:call-template>
	</xsl:variable>

	<!-- Sortie de l'en-tête -->
	<xsl:template name="sortie-entete">
		<div class="entete">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr valign="middle">
					<td>
<!-- 						<a href="http://www.pleade.org">
							<img border="0" src="{$depth}images/pleade.png" width="104" height="25" alt="{$mess/entete/images/pleade[lang($lang)]}"/>
						</a>
						<xsl:text>&#xA0;&#xA0;</xsl:text>
						<a href="http://www.ajlsm.com">
							<img border="0" src="{$depth}images/ajlsm.png" width="200" height="131" alt="{$mess/entete/images/ajlsm[lang($lang)]}"/>
						</a>
						<xsl:text>&#xA0;&#xA0;</xsl:text>
						<a href="http://www.arkheia.net">
							<img border="0" src="{$depth}images/anaphore.png" width="294" height="66" alt="{$mess/entete/images/anaphore[lang($lang)]}"/>
						</a>
 --></td>
					<td>
<!-- 						<span class="titre-site">
							<xsl:value-of select="$mess/entete/titre-site[lang($lang)]"/>
						</span>
 --></td>
				</tr>
			</table>
		</div>
	</xsl:template>
	
	<xsl:template name="toc.ancestor">
		<xsl:param name="node" select="."/>
		<xsl:param name="toc.ancestors" select="nothing"/>
		<xsl:param name="prepath" select="''"/>
		<xsl:variable name="doc" select="document(concat($prepath, $index, $_xml), $node)"/>
		<xsl:choose>
			<xsl:when test="$doc//toc[@role='index']">
				<xsl:call-template name="toc.ancestor">
					<xsl:with-param name="prepath" select="concat($prepath, '../')"/>
					<xsl:with-param name="toc.ancestors">
						<tocentry>
							<xsl:attribute name="abbrev">
								<xsl:apply-templates select="$doc" mode="name"/>
							</xsl:attribute>
							<ulink>
								<xsl:attribute name="url">
									<xsl:value-of select="concat($prepath, $index, $_xml)"/>
								</xsl:attribute>
								<xsl:apply-templates select="$doc" mode="name"/>
							</ulink>
							<xsl:apply-templates select="$doc" mode="abstract"/>
						</tocentry>
						<xsl:copy-of select="$toc.ancestors"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="$toc.ancestors"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="toc.select">
		<xsl:param name="toc" select="$toc"/>
		<xsl:for-each select="$toc">
			<xsl:if test="//tocentry[ulink]">
				<select onchange="if (this.selectedIndex != -1) if (this.options[this.selectedIndex].value) window.location.href=this.options[this.selectedIndex].value;">
					<option/>
					<xsl:for-each select="//tocentry[ulink]">
						<option>
							<xsl:attribute name="value">
								<xsl:apply-templates select="ulink/@url"/>
							</xsl:attribute>
							<xsl:if test="@role ='self'">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="@abbrev"/>
						</option>
					</xsl:for-each>
				</select>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:variable name="toc">
		<xsl:if test="$toc.file">
			<toc role="index">
				<xsl:for-each select="$toc.file//toc[@role='index']//tocentry">
					<xsl:choose>
						<xsl:when test="ulink/@url[not(contains(., '://')) and contains(., $_xml)]">
							<xsl:variable name="doc" select="document(ulink/@url, $toc.file)"/>
							<tocentry>
								<xsl:if test="$self = $doc">
									<xsl:attribute name="role">self</xsl:attribute>
								</xsl:if>
								<xsl:attribute name="abbrev">
									<xsl:apply-templates select="$doc" mode="name"/>
								</xsl:attribute>
								<xsl:choose>
									<xsl:when test="ulink/@url">
										<ulink url="{ulink/@url}">
											<xsl:variable name="title">
												<xsl:apply-templates select="$doc" mode="title"/>
											</xsl:variable>
											<xsl:if test="normalize-space($title)=''">
												<xsl:value-of select="ulink"/>
											</xsl:if>
											<xsl:value-of select="$title"/>
										</ulink>
									</xsl:when>
									<xsl:otherwise>
										<xsl:copy-of select="title|subtitle|phrase[@role='title']|phrase[@role='subtitle']"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:apply-templates select="$doc" mode="abstract"/>
							</tocentry>
						</xsl:when>
						<xsl:otherwise>
							<xsl:copy-of select="."/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</toc>
		</xsl:if>
	</xsl:variable>
	
	<xsl:template match="set | book | part | chapter | article | section">
		<xsl:variable name="name" select="name()"/>
		<div class="{name(.)}">
			<xsl:call-template name="language.attribute"/>
			<xsl:if test="$generate.id.attributes != 0">
				<xsl:attribute name="id">
					<xsl:call-template name="object.id"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="title"/>
			<xsl:apply-templates select="abstract"/>
			<xsl:variable name="toc.params">
				<xsl:call-template name="find.path.params">
					<xsl:with-param name="table" select="normalize-space($generate.toc)"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:apply-templates select="* [name()!='abstract'] [not(contains(name(), 'info') or contains(name(), 'title'))] | title/following-sibling::processing-instruction()"/>
			<xsl:if test="toc[@role='index'] and $toc">
				<xsl:call-template name="toc.index">
					<xsl:with-param name="toc" select="$toc"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test=".=/*">
				<xsl:call-template name="process.footnotes"/>
			</xsl:if>
			<!-- <xsl:apply-templates select="*[contains(name(), 'info')] | title/preceding-sibling::node()"/> -->
		</div>
	</xsl:template>
	
	<!--No display of QandQ toc -->
	<xsl:template name="process.qanda.toc"/>
	
	<!-- Better display for questions -->
	<xsl:template match="question">
		<xsl:variable name="deflabel">
			<xsl:choose>
				<xsl:when test="ancestor-or-self::*[@defaultlabel]">
					<xsl:value-of select="(ancestor-or-self::*[@defaultlabel])[last()]/@defaultlabel"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$qanda.defaultlabel"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<tr class="{name(.)}">
			<td align="left" valign="top">
				<xsl:call-template name="anchor">
					<xsl:with-param name="node" select=".."/>
					<xsl:with-param name="conditional" select="0"/>
				</xsl:call-template>
				<xsl:call-template name="anchor">
					<xsl:with-param name="conditional" select="0"/>
				</xsl:call-template>
				
				<p>
					<b>
						<xsl:apply-templates select="." mode="label.markup"/>
						<xsl:text>. </xsl:text> <!-- FIXME: Hack!!! This should be in the locale! -->
					</b>
				</p>
			</td>
			<td align="left" valign="top">
				<xsl:choose>
					<xsl:when test="$deflabel = 'none' and not(label)">
						<b>
							<xsl:apply-templates select="*[name(.) != 'label']"/>
						</b>
					</xsl:when>
					<xsl:otherwise>
						<b><xsl:apply-templates select="*[name(.) != 'label']"/></b>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>


</xsl:stylesheet>

