//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Classe permettant de créer l'arbre pour le résultat de recherche
 * @author Julien Huet
 */
/**
 * @class Classe permettant de créer l'arbre pour le résultat de recherche
 * @author Julien Huet
 * @augments TreeView
 */
var ResultTreeView = new Class(/** @lends ResultTreeView */{
	Extends: TreeView,

	/**
	* <strong>Constructor</strong><br/>  Initialisation de l'arbre
	* @param {String} containerID Id de l'element où l'on va insérer le treeView
	* @param {String} divIdToLoad Id de l'element contenant le HTML composant l'arbre
	*/
	initialize: function(containerID,divIdToLoad){
		// appel au constructeur parent
		this.divToLoad = $(divIdToLoad);

		this.parent(null, $(containerID));
		this.enableExpandAllOnLoad();
		this.load(this.divToLoad);
		this.divToLoad.dispose();
	},

	/**
	* Fonction permettant de charger l'arbre depuis du code HTML
	* @param {Element}htmlToLoad html comprennant la liste UL LI
	* @returns
	*/
	load: function(htmlToLoad){
		var initLoad = this.tree.initTree(htmlToLoad);
		if ( initLoad ) {
			this.tree.load(initLoad);
			// ajout de l'arbre a son container principal
			this.mainContainer.adopt(this.treeContainer);
		}
		this.doOnLoad();
		return initLoad;
	}
});
