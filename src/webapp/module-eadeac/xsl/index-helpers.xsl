<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Méthodes et fonctions pour la gestion des index -->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xsl pleade xs fn"
>

	<!-- Une fonction qui retourne la clé associée à une entrée d'index -->
	<xsl:function name="pleade:get-index-key" as="xs:string">
		<xsl:param name="el"/>
		<!-- Le nom dépend selon qu'il s'agit d'une définition ou une entrée -->
		<xsl:variable name="name">
			<xsl:choose>
				<xsl:when test="$el/@element"><xsl:value-of select="$el/@element"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="local-name($el)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- On concatène les différentes informations : $name, @role, @source, @type -->
		<xsl:variable name="sep" select="';'"/>
		<xsl:value-of select="concat('name=', $name, $sep, 'role=', $el/@role, $sep, 'source=', $el/@source, $sep, 'type=', $el/@type)"/>
	</xsl:function>
	
	<!-- Une fonction qui indique si un élément est un élément d'index ou non -->
	<xsl:function name="pleade:is-index-element" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:choose>
			<xsl:when test="$el/ancestor-or-self::eadheader or $el/ancestor-or-self::frontmatter"><xsl:value-of select="false()"/></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="boolean($el/self::unitid or $el/self::publisher or $el/self::corpname or $el/self::famname or $el/self::geogname or $el/self::name or $el/self::occupation or $el/self::persname or $el/self::subject or $el/self::genreform or $el/self::function or $el/self::title)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	<!-- Une fonction qui indique si un index est sélectionné -->
	<xsl:function name="pleade:is-index-selected" as="xs:boolean">
		<xsl:param name="id"/>
		<xsl:param name="selected-index"/>
		<xsl:value-of select="boolean($selected-index = '_all_' or fn:tokenize($selected-index, ',')[. = $id])"/>
	</xsl:function>
	
	<!-- Retourne vrai si l'élément $el respecte l'index $def -->
	<xsl:function name="pleade:index-matches" as="xs:boolean">
		<xsl:param name="def"/>
		<xsl:param name="el"/>
		<!-- On test d'abord sans tenir compte des restrictions d'ancêtres -->
		<xsl:variable name="before-ancestors" select="boolean(
				( not($def/@element) or $def/@element = local-name($el) ) and
				( not($def/@source) or $def/@source = $el/@source ) and
				( not($def/@role) or $def/@role = $el/@role ) and
				( not($def/@type) or $def/@type = $el/@type )
			)
		"/>
		<!-- Maintenant on vérifie si les restrictions d'ancêtres sont respectées -->
		<xsl:choose>
			<!-- Si pas de restriction, alors on retourne ce qu'on a déjà calculé -->
			<xsl:when test="not($def/@ancestor)"><xsl:value-of select="$before-ancestors"/></xsl:when>
			<!-- Si déjà pas un match, alors on retourne faux, ça le sera pas plus avec les ancêtres -->
			<xsl:when test="not($before-ancestors)"><xsl:value-of select="false()"/></xsl:when>
			<xsl:otherwise>
				<!-- On doit vérifier les ancêtres -->
				<xsl:variable name="anc" select="$def/@ancestor"/>
				<xsl:value-of select="boolean($el/ancestor::*[local-name() = $anc])"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

</xsl:stylesheet>
