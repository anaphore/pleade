//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du redimensionnement de la fenêtre EAD
 * @author Julien Huet
 */
/**
 * @class Gestion du redimensionnement de la fenêtre EAD
 * @augments LayoutManager
 * @author Julien Huet
 */
var EadLayoutManager = new Class(/** @lends EadLayoutManager.prototype */{
	Implements: [LayoutManager],
	
	/**
	* Layout YUI
	* @private
	* @type YAHOO.widget.Layout
	*/
	layout: '',

	/**
	* La taille (minimale et initiale du panneau de gauche
	* @private
	* @type Integer
	*/
	layoutWidth: 250,
	
	/** <strong>Constructor</strong><br/>  */
	initialize: function(){	},
	
	/**
	* Dimensionnement de la page.
	* @param {Boolean} premier Savoir si c'est le premier chargement de la page
	*/
	adjustHeight: function(premier){
		var above = 0;
		if ( $('pl-pg-body-main-lcol') ) {
			var above = $('pl-pg-body-main-lcol').getOffsets().y;
		}
		// FIXME: d'où vient le pixel ?
		var below = this.getHeight($('pl-pg-footer'), true) + this.getHeight($('pl-pg-brdr-b'), true) + this.getHeight($('pl-pg-body-bottom'), true) + parseInt($('body').getStyle('margin-bottom')) + parseInt($('body').getStyle('padding-bottom')) + 1;
		var h = YAHOO.util.Dom.getClientHeight() - above - below;
		/* DEBUG (MP) Ecart de 4px entre IE Firefox (en trop dans IE) du a un above different
		////alert("h:"+h+
		"\nabove:"+Position.cumulativeOffset($("pl-pg-body-main-lcol"))[1]+
		"\npl-pg-footer:"+getHeight($("pl-pg-footer"), true)+
		"\npl-pg-brdr-b:"+getHeight($("pl-pg-brdr-b"), true)+
		"\npl-pg-body-bottom:"+getHeight($("pl-pg-body-bottom"), true)+
		"\nbody.margin-bottom:"+parseInt($("body").getStyle("margin-bottom"))+
		"\nbody.padding-bottom:"+parseInt($("body").getStyle("padding-bottom"))+
		"\ngetClientHeight:"+YAHOO.util.Dom.getClientHeight()
		);
		*/
		if ( premier ) {
			var lel = $('pl-pg-body-main');
			var lh = h - this.getVMargin(lel) - this.getVPadding(lel) - this.getVBorder(lel);
			if ($('pl-pg-body-main-lcol') ) {
				this.initLayout(lh);
			}
		} else {
			try {
				var lel = $('pl-pg-body-main');
				var lh = h - this.getVMargin(lel) - this.getVPadding(lel) - this.getVBorder(lel);
				if ( $('pl-pg-body-main-lcol') ) {
					this.resizeLayout(lh);
				}
			} catch (e) {};
		}
		//debugger;
		var mcol = $('pl-pg-body-main-mcol');
		
		if ( mcol ) {
			this.setTotalHeight(mcol, h);
			mcol.setStyle('overflow', 'auto');
		}

		this.setTotalHeight($('tabs'), h);

		if ( mcol ) {
			this.setTotalHeight(mcol, h);
		}

		// Petit travail sur le coeur : si on a un conteneur EAD, on le prend. sinon
		// on utilise le coeur de page.

		this.setTotalHeight(pgd, h);
		var pgd = $('pl-pgd-cnt');
		var tlbx = $('pl-pg-body-tool-box');
		var navres = $$('.navigation-results')[0];
		var pgdh = h;
		if ( tlbx ) {
			pgdh -= this.getHeight(tlbx, true);
		}
		if ( navres ) {
			pgdh -= this.getHeight(navres, true);
		}
		if (pgd != null) {
			this.setTotalHeight(pgd, pgdh);
			pgd.setStyle('overflow', 'auto');
			if (mcol != null) {
				mcol.setStyle('overflow', 'hidden');
			}
		} else {
			mcol.setStyle('overflow', 'auto');
		}
	},
	
	/**
	* Initialisation du layout YUI
	* @param {Integer} size Hauteur du layoyt
	*/
	initLayout: function(size){
		this.layout = new YAHOO.widget.Layout('pl-pg-body-main', {
			height: size,
			minHeight: 150, //So it doesn't get too small
			units: [
				{
					position: 'left',
					width: this.layoutWidth,
					resize: true,
					body: 'pl-pg-body-main-lcol',
					gutter: '0 5 0 2',
					minWidth: this.layoutWidth
				},
				{
					position: 'center',
					body: 'pl-pg-body-main-mcol'
				}
			]
		});
		this.layout.render();
	},
	
	/**Modification de la taille du layout
	* @param {Object} size
	*/
	resizeLayout: function(size){
		this.layout.set('height', size);
		this.layout.resize();
		var _data = this.layout.getUnitById('pl-pg-body-main-mcol');
		_data.resize(true);
	}
});
