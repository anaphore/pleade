<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="xsl fn saxon xs"
>

	<!-- Extraction de la table des matières. -->

	<!-- Fonctions d'utilités générales -->
	<xsl:import href="../../../../commons/xsl/functions.xsl"/>
	
	<!-- Des utilitaires pour la publication -->
	<xsl:import href="publication-helpers.xsl"/>

	<!-- L'URL absolue où sera stockée le fichier de table des matières. -->
	<xsl:param name="data-output-dir" select="''" />

	<!-- Le paramètre de publication -->
	<!-- FIXME: utilisé ?? -->
	<xsl:param name="delete-internal"/>
	
	<xsl:param name="debug" select="'false'"/>
	
	<!-- Les propriétés de publication -->
	<!--<xsl:variable name="properties-url" select="concat('cocoon://functions/ead/get-properties/', /ead/@pleade:id, '/publication.xml')"/>
	<xsl:variable name="pub-properties" select="if(doc-available($properties-url)) then document($properties-url)/properties else ()"/>-->

	<!-- Doit-on prendre en compte les illusatrations à l'indexation ? -->
	<xsl:variable name="show-illustrated" select="
			if($pub-properties/property[@name='show-illustrated'] != '') then xs:boolean($pub-properties/property[@name='show-illustrated'])
			else if($pub-properties/property[@name='show-illustrated']/@default != '') then xs:boolean($pub-properties/property[@name='show-illustrated']/@default)
			else false()
	"/>
	<!-- L'identifiant du document -->
	<xsl:variable name="eadid" select="/ead/@pleade:id"/>

  <xsl:template match="/">
    <xsl:message> 	Traitement de la table des matieres (etape 10 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
    <!--<xsl:message>   	Nombre d'entrees : <xsl:value-of select="count(ead//*[@pleade-toc='true'])"/></xsl:message>-->
    <xsl:apply-templates/>
  </xsl:template>


	<!-- Traitement de l'élément racine, pour démarrer le fichier. -->
	<xsl:template match="ead">
		<!-- On commence par copier le contenu complet pour l'étape suivante du
				traitement de publication -->
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" mode="copy" />
		</xsl:copy>

		<!-- Ensuite on démarre le traitement de la table des matières comme telle -->
		<xsl:call-template name="output-toc" />

	</xsl:template>

	<!-- Démarrage de la sortie de la table des matières, le contexte est /ead. -->
	<xsl:template name="output-toc">
		<!-- Sortie d'un document XML avec XSLT 2.0 -->
		<!-- Un hack pour le dossier datadir si celui-ci est une ressource UNC Windows,
				l'URL doit être du genre file:////partage/sous-dossier/... -->
		<xsl:variable name="tmp" select="concat($data-output-dir, '/', $eadid, '/toc.xml')"/>
		<xsl:variable name="href">
			<xsl:choose>
				<xsl:when test="starts-with($tmp, '//')"><xsl:value-of select="concat('file://', $tmp)"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="$tmp"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:result-document href="{$href}">
			<!-- L'élément racine est "ead" pour le distinguer des autres -->
			<ead pleade:id="{$eadid}">
				<!-- On traite tout le contenu en mode toc -->
				<xsl:apply-templates select="*" mode="toc"/>
			</ead>
		</xsl:result-document>
	</xsl:template>

	<!-- De manière générale, on arrête le traitement de table des matières, sauf
			pour les éléments que l'on intercepte après ce template. -->
	<xsl:template match="*" mode="toc" />

	<!-- Une entrée pour les métadonnées. -->
	<xsl:template match="eadheader" mode="toc">
		<c pleade:id="{@pleade:id}" pleade:original-element="eadheader"/>
		<!-- Pas de poursuite du traitement ici, aucun enfant de eadheader ne sera dans la table des matières. -->
	</xsl:template>

	<!-- Traitement du frontmatter -->
	<xsl:template match="frontmatter" mode="toc">
		<!-- Deux cas de figure : s'il a un seul sous-élément, alors on affiche directement
				ce sous-élément dans la table des matières. S'il a plusieur sous-éléments,
				alors on crée une entrée pour le frontmatter et on traite ses sous-éléments. -->
		<xsl:choose>
			<xsl:when test="count(*) > 1 ">
				<c pleade:id="{@pleade:id}" pleade:original-element="frontmatter">
					<xsl:apply-templates select="*" mode="toc"/>
				</c>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="*" mode="toc"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement de la page de titre -->
	<xsl:template match="titlepage" mode="toc">
	 	<c pleade:id="{@pleade:id}" pleade:original-element="titlepage" pleade:title="{.}"/>
		<!-- Aucune sous-entrée ici -->
	</xsl:template>

	<!-- Traitement d'un div dans le frontmatter -->
	<xsl:template match="div" mode="toc">
		<c pleade:id="{@pleade:id}" pleade:original-element="div" pleade:title="{@pleade:title}">
			<xsl:apply-templates select="div" mode="toc"/>
		</c>
	</xsl:template>

	<!-- Traitement d'un composant -->
	<xsl:template match="archdesc|c|c01|c02|c03|c04|c05|c06|c07|c08|c09|c10|c11|c12" mode="toc">

		<xsl:choose>
			<xsl:when test="@pleade:toc = 'true'">
				<!-- On ajoute une entrée -->
				<c>
					<xsl:apply-templates select="@pleade:id | @pleade:title | @pleade:dao | @audience" mode="copy"/>
					<xsl:if test="xs:boolean($show-illustrated)">
						<xsl:if test="dao|daogrp/daoloc">
							<xsl:attribute name="pleade:object-type">
								<xsl:variable name="types">
									<xsl:for-each select="dao|daogrp/daoloc">
										<xsl:value-of select="pleade:object-type(.)"/>
										<xsl:if test="position() != last()"><xsl:text>::</xsl:text></xsl:if>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="distinct-values(tokenize($types, '::'))"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="pleade:has-object-descendant(.)">
							<xsl:attribute name="pleade:ancobject">1</xsl:attribute>
						</xsl:if>
					</xsl:if>
					<xsl:apply-templates select="*" mode="toc"/>
				</c>
			</xsl:when>
			<xsl:otherwise>
				<!-- Pas dans la table des matières, mais on traite tout de même ses enfants. -->
				<xsl:apply-templates select="*" mode="toc"/>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<!-- Le dsc doit poursuivre le traitement. -->
	<xsl:template match="dsc" mode="toc">
		<xsl:apply-templates select="*" mode="toc"/>
	</xsl:template>

	<!-- Un template de copie. -->
	<xsl:template match="@*|node()" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" mode="copy" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

