//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Retourne différentes dimensions d'élements<br/> Permet de définir sio l'on souhaite inclure les marges, ...
 * @author Julien Huet
 */
/**
 * @class Retourne différentes dimensions d'élements<br/> Permet de définir sio l'on souhaite inclure les marges, ...
 * @author Julien Huet
 */
var LayoutManager = new Class(/** @lends LayoutManager.prototype */{
	
	/**
	* <strong>Constructor</strong><br/> Initialisation du layout manager
	*/
	initialize: function(){ },

	/**
	* Retourne la hauteur totale d'un élément car la méthode d'origine n'inclut pas les marges
	* @param {Element} el Elément dont on souhaite connaitre la largeur.
	* @param {Boolean} includeMargins true pour inclure les marges
	* @returns {Integer} h La heuteur de l'élément avec ou sans les marges (selon le paramètre)
	* @type Integer
	*/
	getHeight: function(el, includeMargins) {
		if ( !el || !$(el) ) {
			return 0;
		}
		var iMargin = (includeMargins) ? true : false;
		var h = $(el).getHeight();
		if ( iMargin ) {
			h += this.getVMargin(el);
		}
		return h;
	},

	/**
	* Retourne la largeur totale d'un élément car la méthode d'origine n'inclut pas les marges
	* @param {Element} el Elément dont on souhaite connaitre la largeur.
	* @param {Boolean} includeMargins true pour inclure les marges
	* @returns {Integer} h La largeur de l'élément avec ou sans les marges (selon le paramètre)
	* @type Integer
	*/
	getWidth: function(el, includeMargins) {
		if ( !el || !$(el) ) {
			return 0;
		}
		var iMargin = (includeMargins) ? true : false;
		var w = $(el).getWidth();
		if ( iMargin ) {
			w += this.getHMargin(el);
		}
		return w;
	},

	/**
	* Définit la hauteur totale d'un élément. <br/>
	*
	* La hauteur totale est la somme de: <br/>
	* - margin-top <br/>
	* - border-top-width <br/>
	* - padding-top <br/>
	* - hauteur comme telle de l'élément <br/>
	* - padding-bottom <br/>
	* - border-bottom-width <br/>
	* - margin-bottom <br/>
	*
	* Le paramètre height est la hauteur totale désirée. C'est la propriété
	* CSS height qui sera modifiée, les marges, bordures et padding ne sont pas
	* touchées. Le paramètre height est donc diminué de la somme des ces hauteurs.
	*
	* @param {Element} el Element dont on souhaite fixer la hauteur
	* @param {Integer} height la valeur de la hauteur désirée
	*/
	setTotalHeight: function(el, height) {
		if ( !height || !el || !$(el) ) {
			return;
		}
		var h = height - this.getVMargin(el) - this.getVPadding(el) - this.getVBorder(el);
		if ( h < 0 ) {
			h = 0;
		}
		$(el).setStyle('height', h + 'px');
	},

	/**
	* Retourne les marges verticales d'un élément
	* @param {Element} el Element concerné
	* @returns {Integer} les marges verticales de l'élément
	*/
	getVMargin: function(el) {
		return this.getVDimension(el, 'margin');
	},

	/**
	* Retourne les marges horizontales d'un élément
	* @param {element} el Element concerné
	* @returns les marges horizontales de l'élément
	* @type Int
	*/
	getHMargin: function(el) {
		return this.getHDimension(el, 'margin');
	},

	/**
	* Retourne le padding vertical d'un élément
	* @param {element} el Element concerné
	* @returns {Integer} le padding vertical de l'élément
	*/
	getVPadding: function(el) {
		return this.getVDimension(el, 'padding');
	},

	/**
	* Retourne l'épaisseur (verticale) des bordures horizontales
	* @param {element} el Element concerné
	* @returns {Integer} l'épaisseur (verticale) des bordures horizontales
	*/
	getVBorder: function(el) {
		return this.getVDimension(el, 'border', '-width');
	},

	/**
	* Retourne les valeurs top + bottom d'une dimension verticale
	* @param {element} el Element concerné
	* @param (String) prop  propriété CSS dont on souhaite connaître les valeurs top et bottom d'une dimension verticale (exemple : border, margin, padding)
	* @param (String) suffix permet de compléter la propriété CSS à l'aide d'un suffix
	* @returns {Integer} valeur top + bottom d'une dimension verticale
	*/
	getVDimension: function(el, prop, suffix) {
		if ( !prop || !el || !$(el) ) {
			return 0;
		}
		el = $(el);
		suffix = suffix || '';
		var hT = parseInt($(el).getStyle(prop + '-top' + suffix));
		var hB = parseInt($(el).getStyle(prop + '-bottom' + suffix));
		var h = 0;
		if ( hT ) {
			h += hT;
		}
		if ( hB ) {
			h += hB;
		}
		return h;
	},
	
	/**
	* Retourne les valeurs left + right d'une dimension verticale
	* @param {element} el Element concerné
	* @param (String) prop  propriété CSS dont on souhaite connaître les valeurs left et right d'une dimension verticale (exemple : border, margin, padding)
	* @param (String) suffix permet de compléter la propriété CSS à l'aide d'un suffix
	* @returns {Integer} valeur left + right d'une dimension verticale
	*/
	getHDimension: function(el, prop, suffix) {
		if ( !prop || !el || !$(el) ) {
			return 0;
		}
		el = $(el);
		suffix = suffix || '';
		var hT = parseInt($(el).getStyle(prop + '-left' + suffix));
		var hB = parseInt($(el).getStyle(prop + '-right' + suffix));
		var h = 0;
		if ( hT ) {
			h += hT;
		}
		if ( hB ) {
			h += hB;
		}
		return h;
	}
});
