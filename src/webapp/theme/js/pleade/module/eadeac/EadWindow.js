//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion de la fenêtre EAD
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion de la fenêtre EAD
 * @augments EadLayoutManager
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var EadWindow = new Class(/** @lends EadWindow.prototype */{
	Implements: [EadLayoutManager],
	//declaration des proprietés de la classe
	/**
	* Gestionnaire de contenu de la fenêtre EAD
	* @private
	* @type EadContentManager
	*/
	contentManager: '',

	/**
	* Tabulation de la fenetre EAD (contient les onglets du sommaire, de l'index et de la recherche)
	* @private
	* @type YAHOO.widget.TabView
	*/
	tabs: null,

	/**
	* Treeview du sommaire
	* @private
	* @type TocTreeView
	*/
	toc: '',

	/**
	* Treeview de l'index
	* @private
	* @type IndexTreeView
	*/
	index: '',

	/**
	* Un tableau contenant les hauteurs de notice
	* @private
	* @type Array
	*/	
	hs : '',

	/**
	* Un tableau contenant les notices eac recuperees
	* @private
	* @type Array
	*/
	cs : '',

	/**
	* <strong>Constructor</strong><br/> 
	*/
	initialize: function(){
		// initialisation du content manager, c'est par cette variable qu'on pourra naviguer dans la fenetre EAD
		this.contentManager = new EadContentManager(this);

		// On initialise la hauteur
		this.adjustHeight(true);

		// On charge le contenu principal en Ajax (qid est une variable globale déclarée dans le html cf. ead2html-index.xsl, peut être une chaine vide)
		this.contentManager.loadContent(this.contentManager.getArchdescID(), true, this.contentManager.getQid(),true)

		// on initialise les tabs pour cotenir l'index, le sommaire et la recherche
		// On ne fait que créer un objet qu pointe sur l'ID du div
		this.tabs = new YAHOO.widget.TabView('tabs');

		this.index= new IndexTreeView(this);
		this.toc = new TocTreeView(this);

		this.toc.load();
		this.index.load();

		this.initSearch();

		this.windowManager = new WindowManager();

		this.hs = new Array();
		this.cs = new Array();

		windowManager.initMenuBox();
	},

	/**
	* Fait appel au contentManager pour connaitre l'id du fragment affiché
	* @param {String} defaultFragmentID
	* @param {Boolean} checkC
	*/
	getfragmentID: function(defaultFragmentID, checkC){
		return this.contentManager.getfragmentID(defaultFragmentID, checkC);
	},

	/**
	* Fonction permettant de charger le panneau de recherche
	*/
	initSearch: function(){
		var cible = $('pl-pgd-search');
		if (cible != null) {
			var req = new Request.HTML({
				method: 'get',
				url: 'functions/ead/get-ead-docs-search-form.ajax?eadid=' + eadid,
				update: cible,
				evalScripts: false,
				evalResponse: true, 
				onComplete: this.contentManager.stopLoading()
			});
			req.send();
		}
	},

	/**
	* Retourne le treeView index
	* @return (IndexTreeView)
	*/
	getIndex: function(){
		return this.index;
	},

	/**
	* Retourne le treeView toc
	* @return (TocTreeView)
	*/
	getToc: function(){
		return this.toc;
	},

	/**
	* Retourn le gestionnaire de contenu
	* @return (EadContentManager)
	*/
	getContentManager: function(){
		return this.contentManager;
	},

	/**
	* Retounr le gestionnaire de fenêtre associé à la fenetre EAD
	* @return (WindowManager)
	*/
	getWindowManager: function(){
		return this.windowManager;
	},

	/**
	* toggleEacBlock
	* @param {Object} eacId
	* @param {Object} cible
	*/
	toggleEacBlock : function(eacId, cible){
		if (!eacId) {
			return false;
		}
		if ( !cible ) {
			cible = $('divEadDoc_' + eacId);
		}
		if ( !cible ) {
	        	cible = $$('div.div-eac-doc')[0];
		}
		if ( !cible ) {
			return false;
		}

		// Masquer
		if ( cible.visible() ) {
			// Masquage non anime
			// cible.hide();
			// Pour animer le masquage
			if ( this.hs && !this.hs[cible.id] ) {
				var h = this.getHeightForAnim(cible);
				this.hs[cible.id] = h;
			}
			var d = 300;
			var anim = new YAHOO.util.Anim(cible, {
				height: {
					to: 0
				}
			}, d / 1000, YAHOO.util.Easing.EaseOut);
			anim.animate();
			setTimeout(function(){
				cible.hide();
				adjustHeight();
			}, d + 20);
		} else {
			// Afficher
			var childs = cible.childElements();
			if ( this.cs && this.cs[cible.id] && childs != null && childs.size() > 1 ) {
				// le doc est deja la
				this.animateEacBlock(cible);
			} else {
				var req = new Request.HTML({
					method: 'get',
					url: 'eac.ajax?id=' + eacId,
					update: cible,
					onRequest: this.startEacBlockLoading(cible),
					onComplete: this.stopEacBlockLoading(cible)
				});
				req.send();
			}
		}
		return true;
	},

	/**
	* getHeightForAnim
	* @param {Object} cible
	*/
	getHeightForAnim : function(cible){
		if ( !cible ) {
	        	return null;
		}
		var h = cible.getHeight();
		try {
			var prev = cible.previous();
			if ( prev ) {
				h += prev.getHeight();
			}
		} catch (e) {}
		return h;
	},

	/** Fonction pour l'animation du bloc EAC
	* @param {Object} cible
	*/
	startEacBlockLoading: function(cible){},

	/** Fonction pour l'animation du bloc EAC
	* @param {Object} cible
	*/
	stopEacBlockLoading : function(cible){
		return function(){
			this.cs[cible.id] = true;
			animateEacBlock(cible);
		}
	},

	/** Fonction pour l'animation du bloc EAC
	* @param {Object} cible
	*/
	animateEacBlock : function(cible){
		// Affichage non anime
		// cible.show();
		// Pour animer l'affichage
		var h;
		if ( !this.hs[cible.id] ) {
			h = this.getHeightForAnim(cible);
		} else  {
			h = this.hs[cible.id];
		}
		cible.setStyle('height', '1px');
		var anim = new YAHOO.util.Anim(cible, {
			height: {
				from: 0,
				to: h
			}
		}, 0.5, YAHOO.util.Easing.EaseOut);
		anim.animate();
		cible.show();
		this.adjustHeight();
	},

	/**
	* Fonction utilisée pour la synchronisation du sommaire
	* @param id du noeud a synchroniser
	* @param {array} pos sa position dans l'arbre
	* @param {array} ancs Tableau contenant les identifiants des ancetres du noeud
	*/
	synchronizeToc: function( id, pos, ancs ) {
		var myAncestor = '';
		if ( ancs ) {
			myAncestor = $A(ancs.split(','));
			myAncestor.reverse()
		}
		// tranformation de la chaine de caractere comportant tous les ancetres en tableau
		this.focusOnTocTab();
		this.loading = new Loading($('tabs'),null,'Synchronisation en cours');
		this.loading.start();
		// surcharge de la methode onFocusNodeComplete
		this.toc.getTree().onFocusNodeComplete = function(node){
			this.loading.stop();
		}.bind(this);

		this.toc.getTree().selectNodeByID(id,myAncestor);
		this.contentManager.processAnchor(id);
	},

	/**
	* Fonction permettant de remettre le focus sur le tab YUI qui continet le sommaire
	*/
	focusOnTocTab: function(){
		if ( this.tabs.get('activeIndex') != 0 ) {
			this.tabs.set('activeIndex', 0);
		}
	}
});