<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		Cette XSLT permet d'extraire des informations complémentaires à propos
		d'une unité de description dans le cadre de classement.
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xsl fn">

	<!-- Import de l'XSLT qui aide au traitement des liens -->
	<xsl:import href="link-helpers.xsl"/>

	<!-- Le paramètre id contient l'identifiant de l'unité choisie -->
	<xsl:param name="id"/>

	<!-- Cette variable doit être définie car nécessaire pour l'import du fichier link-helpers -->
	<!-- FIXME : cette variable n'a pas toujours une valeur -->
	<xsl:variable name="eadid" select="/ead/@pleade:id"/>
	
	<!-- Variables pour définir les extensions des liens vers des documents EAD
			On prend la valeur par défaut.
	-->
	<xsl:variable name="prop-url" select="'cocoon://functions/ead/get-properties/display.xml'" />
	<xsl:variable name="display-properties" select="
			if(doc-available($prop-url)) then document($prop-url)/properties
			else 'toto'
		"/>
	<xsl:variable name="exts" select="if($display-properties and $display-properties/property[@name = 'ead-link-extensions']) then $display-properties/property[@name = 'ead-link-extensions']/@default else 'xml html'" />
	<xsl:variable name="ead-link-extensions" select="fn:tokenize($exts, '\s')"/>

	<!-- L'élément racine est le document EAD -->
	<xsl:template match="/ead">
		<!-- On traite le composant avec l'identifiant -->
		<xsl:variable name="c" select="archdesc//*[@pleade:component = 'true' and @pleade:id = $id]"/>	<!-- FIXME: on ne peut pas afficher des infos sur archdesc? -->
		<xsl:choose>
			<xsl:when test="$c"><xsl:apply-templates select="$c[1]"/></xsl:when>
			<xsl:otherwise>
				<!-- On va retourner un faux document ? -->
				<c>
					<did><unittitle>erreur</unittitle></did>
				</c>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- L'unité à décrire -->
	<xsl:template match="c">
		<xsl:copy>
			<!-- On n'inclut pas le did --> <!-- FIXME: toujours OK ? -->
			<xsl:apply-templates select="@*|*[not(self::did)]" mode="copy"/>
		</xsl:copy>
	</xsl:template>

	<!-- On prépare les liens vers des IRs -->
	<!-- FIXME (MP) : Faudrait-il résoudre les liens des images, fichiers multimédias, etc. ?
	Dans ce cas, il faudrait simuler (comme on le fait ici) la publication. -->
	<xsl:template match="archref|extref" mode="copy">
		<xsl:variable name="url" select="if(@pleade:url!='') then @pleade:url else @href" />
		<xsl:variable name="ext" select="pleade:get-extension(@href)"/>
		<xsl:variable name="ltype" select="
				if ( @pleade:link-type!='' ) then @pleade:link-type
				else if (pleade:is-url-absolute($url)) then 'absolute'
				else if (@role = 'inventaire') then 'ead'
				else if ($ead-link-extensions= $ext) then 'ead'
				else ''
		" />
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="copy" />
			<xsl:if test="pleade:is-link(.)"><!-- On s'assure qu'on traite un lien -->
				<xsl:attribute name="pleade:link-type"><xsl:value-of select="$ltype" /></xsl:attribute>
				<xsl:attribute name="pleade:url"><xsl:value-of select="@href" /></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="text()" mode="copy" />
		</xsl:copy>
	</xsl:template>

	<!-- Les otherfindaid sans contenu ne sont pas sortis -->
	<xsl:template match="otherfindaid[normalize-space(.) = '']" mode="copy"/>

	<!-- On ne traite pas les sous unités -->
	<xsl:template match="dsc | *[@pleade:component = 'true']" mode="copy"/>

	<!-- On copie tout le reste -->
	<xsl:template match="@*|node()" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" mode="copy"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
