<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Construction d'un arbre complet des subsets
		+-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	exclude-result-prefixes="dir">

	<!--+
	    |	Création de la racine du fichier appication.xconf
	    +-->
	<xsl:template match="/dir:directory">
		<subsets>
			<xsl:apply-templates/>
		</subsets>
	</xsl:template>

	<xsl:template match="dir:file[contains(@name, '.xconf')]">
		<xsl:variable name="subset-file-url" select="concat('cocoon:/functions/get-subset.xml?name=', substring-before( @name, '.xconf' ) )	"/>
		<xsl:choose>
			<xsl:when test="doc-available($subset-file-url)">
				<xsl:apply-templates select="document($subset-file-url)" mode="copy-subset">
					<xsl:with-param name="file" select="@name"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>[utils-make-subset-hierarchy.xsl] Le document <xsl:value-of select="$subset-file-url"/> n'existe pas !</xsl:message>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="*" mode="copy-subset"/>

	<xsl:template match="name" mode="copy-subset">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:value-of select="."/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="subset" mode="copy-subset">
		<xsl:param name="file" select="''"/>
		<xsl:copy>
			<xsl:copy-of select="@id"/>
			<xsl:if test="$file != ''">
				<xsl:attribute name="file"><xsl:value-of select="$file"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates mode="copy-subset"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
