<?xml version="1.0" encoding="UTF-8"?>
<?xml-logicsheet href="context://module-eadeac/xsp/xsp-pleade.xsl"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Gestion de contenu. Gérer les documents publiés.
		|	Liste les documents de premier niveau.
		+-->
<xsp:page language="java"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx">

	<xsp:structure>
		<xsp:include>org.apache.excalibur.source.SourceValidity</xsp:include>
	</xsp:structure>

	<xsp:logic>
		/**
		* Methode pour identifier cette page dans la gestion de la cache Cocoon
		* Construction de la cle sur la patron : ead-manage_{hpp}(_{appbypath})?(_{base})?
		*/
		public Serializable getKey()
		{
			String	noCache	= parameters.getParameter("noCache", request.getParameter("noCache") ),
				hpp	= parameters.getParameter("hpp", "50" ),
				base	= parameters.getParameter("base", request.getParameter("base") ),
				apppath	= parameters.getParameter("path-sdxapp", request.getParameter("app") ),
				key 	= null;


			if ( noCache!=null ) {
				return null;
			} else {
				key =	"ead-manage_" + hpp
					+ ( (Utilities.checkString(apppath))?"_"+apppath:"" )
					+ ( (Utilities.checkString(base))?"_"+base:"" );
				// DEBUG System.out.println("[EADMANAGE] key:"+key);
				return key;
			}
		}

		/**
		* Methode appellee par Cocoon pour voir si cette page est valide
		* On tente de connaitre la validite de la base de documents SDX courante.
		* Si c'est impossible, on recupere la validite de l'application elle-meme.
		* Si c'est impossible, on renvoie &lt;code>null&lt;/code>
		*/
		public SourceValidity getValidity() {

			SourceValidity	sourceValidity = null;
			String	noCache	= parameters.getParameter("noCache", request.getParameter("noCache") ),
				base	= parameters.getParameter("base", request.getParameter("base") ),
				apppath	= parameters.getParameter("path-sdxapp", request.getParameter("app") );

			// En debogage, pas de cache
			if ( noCache != null ) {
				return null;
			}

			fr.gouv.culture.sdx.application.Application 	app	= null;
			fr.gouv.culture.sdx.documentbase.DocumentBase	docbase	= null;
			try {
				app = ((FrameworkImpl)manager.lookup(Framework.ROLE)).getApplicationByPath( apppath );
				docbase = app.getDocumentBase( base );
				if ( docbase!=null )	sourceValidity = docbase.getSourceValidity();
				else if ( app!=null )	sourceValidity = app.getSourceValidity();
			} catch ( Exception e ) {
				logError( e );
			} finally {
				// DEBUG System.out.println("[EADDOC] sourceValidity:"+sourceValidity);
				return sourceValidity;
			}
		}

		/**
		* Methode privee pour sortir des erreurs
		*/
		private void logError(Throwable error){
			if ( error != null ) {
				String msg = error.getLocalizedMessage();
				if ( !Utilities.checkString( msg ) ) {
					 msg = error.getMessage();
				}
		  		if ( getLogger().isErrorEnabled() ) {
					getLogger().error(msg, error);
				}
				// DEBUG error.printStackTrace( System.err );
			}
		}
	</xsp:logic>

	<sdx:page langSession="l" langParam="l" appbypathParam="app" appbypathSitemap="path-sdxapp" show="">

		<xsp:logic>

			String f = request.getParameter("field"),
			v = request.getParameter("value"),
      ff = request.getParameter("filter_field"),
      fv = request.getParameter("filter_value");

			//hpp paramétrable
			String hpp = parameters.getParameter( "hpp", null );
			if(
				request.getParameter("hpp") != null &amp;&amp; !"".equals(request.getParameter("hpp"))
				&amp;&amp; (hpp == null || hpp.equals(""))
			) {
				hpp = request.getParameter("hpp");
			}

			if ( !Utilities.checkString( f ) ) f = request.getParameter("field");
			if ( !Utilities.checkString( v ) ) v = request.getParameter("value");
      if ( !Utilities.checkString( ff ) ) ff = request.getParameter("filter_field");
      if ( !Utilities.checkString( fv ) ) fv = request.getParameter("filter_value");
      if ( "".equals(ff) ) { ff = null; }
      if ( "".equals(fv) ) { fv = null; }
			<sdx:extendSDXLocations/>
      if ( ff != null &amp;&amp; fv != null ) {
        <sdx:complexQuery>
          <sdx:fieldQuery fieldString="f" valueString="v" complexop="AND"/>
          <sdx:fieldQuery fieldString="ff" valueString="fv" complexop="AND"/>
        </sdx:complexQuery>
      } else {
        <sdx:fieldQuery fieldString="f" valueString="v" />
      }
      <sdx:results cache="true" hppString="hpp" />
			<sdx:show />
		</xsp:logic>
	</sdx:page>
</xsp:page>
