<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: slideshow.xsl 19731 2010-11-30 11:43:50Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices
d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Paternité : Vous devez citer le nom de l'auteur original de la manière
indiquée par l'auteur de l'oeuvre ou le titulaire des droits qui vous
confère cette autorisation (mais pas d'une manière qui suggérerait
qu'ils vous soutiennent ou approuvent votre utilisation de l'oeuvre).
Pas d'Utilisation Commerciale : Vous n'avez pas le droit d'utiliser
cette création à des fins commerciales.
Pas de Modification : Vous n'avez pas le droit de modifier, de
transformer ou d'adapter cette création.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
    exclude-result-prefixes="xsl sdx" version="2.0">

  <xsl:param name="fragment"/>
  <xsl:variable name="imu" select="'cocoon://functions/commons/get-input-module.xml?name='"/>
  <xsl:variable name="public-url" select="if(doc-available(concat($imu,'pleade.public-url'))) then document(concat($imu,'pleade.public-url'))/get-input-module/input:attribute-values[@name='pleade.public-url']/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0"/>
  <xsl:variable name="c-fragment-url" select="if($fragment=string('null') or $fragment=string('')) then string('null') else concat('cocoon://ead-fragment.debug?c=', normalize-space($fragment))" />

	<!-- Retourne l'année dans un string date normalisé -->
	<!-- @auteur DIA Modou -->
	<xsl:function name="pleade:getYearsFromString" as="xs:string">
		<xsl:param name="date" />

		<xsl:choose>
			<!-- Exemple pour ce cas '2010' -->
			<xsl:when test="contains($date,string('-')) = false()"><xsl:value-of select="$date"/></xsl:when>

			<!--  -->
			<xsl:when test="contains($date,string('-')) = true()">
				<xsl:choose>
					<!-- Exemple pour ce cas -2010 -->
					<xsl:when test="count(tokenize($date,'-')) = 2 and substring-before($date,'-') = ''">
						<xsl:value-of select="$date"/>
					</xsl:when>

					<!-- Exemple pour ce cas 2010-11 -->
					<xsl:when test="count(tokenize($date,'-')) = 2 and substring-before($date,'-') != ''">
						<xsl:value-of select="substring-before($date,'-')"/>
					</xsl:when>

					<!-- Exemple pour ce cas -2010-11 -->
					<xsl:when test="count(tokenize($date,'-')) = 3 and substring-before($date,'-') = ''">
						<xsl:value-of select="concat('-',substring-before(substring-after($date,'-'),'-'))"/>
					</xsl:when>

					<!-- Exemple pour ce cas 2010-11-29 -->
					<xsl:when test="count(tokenize($date,'-')) = 3 and substring-before($date,'-') != ''">
						<xsl:value-of select="substring-before($date,'-')"/>
					</xsl:when>

					<!-- Exemple pour ce cas -2010-11-29 -->
					<xsl:when test="count(tokenize($date,'-')) = 4 and substring-before($date,'-') = ''">
						<xsl:value-of select="concat('-',substring-before(substring-after($date,'-'),'-'))"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="number('error')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

			<!-- Les autres cas ne sont pas gérés car leur format n'est pas normalisé-->
			<xsl:otherwise>
				<xsl:value-of select="string('error')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- Retourne le nombre d'années dans un string -->
	<!-- @auteur DIA Modou -->
	<xsl:function name="pleade:compteYears" as="xs:string">
		<xsl:param name="date" />
		<xsl:value-of select="number(count(tokenize(string($date), '/')))"/>
	</xsl:function>

	<!-- Affiche les tags date.Si date composée, affiche un tag pour chaque date 
		FIXME on retourne à zotero uniquement la premiére date -->
	<!-- @auteur DIA Modou -->
	<xsl:template name="getDates">
		<xsl:param name="date" />

		<xsl:choose>
			<xsl:when test="number(pleade:compteYears($date))=1">
				<date class="totake"><xsl:value-of select="$date" /></date>
			</xsl:when>
			<xsl:when test="number(pleade:compteYears($date))>1">
				<date class="totake"><xsl:value-of select="substring-before($date,'/')" /></date>
				<!--<xsl:call-template name="getDates">
					<xsl:with-param name="date" select="substring-after($date,'/')" />
				</xsl:call-template>-->
			</xsl:when>
			<xsl:otherwise>
				<date class="totake"><xsl:value-of select="$date" /></date>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <xsl:function name="pleade:isnumber">
    <xsl:param name="select"/>
    <xsl:choose>
      <xsl:when test="$select">
        <xsl:analyze-string select="$select" regex="[\d]+" flags="m">
          <xsl:matching-substring>
            <xsl:value-of select="true()"/>
          </xsl:matching-substring>
          <xsl:non-matching-substring>
            <xsl:value-of select="false()"/>
          </xsl:non-matching-substring>
        </xsl:analyze-string>
      </xsl:when>
      <xsl:otherwise>
        NaN
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>


  <!-- fabrique loa balise auteur pour zotéro 
      Pour zotéro, un auteur est composé des mots, les premiers mots sont le prénom et le dernier le nom. 
      Pour les auteurs, ayant plusieurs noms, on envoit les noms séparés par des "-"-->
  <xsl:template name="buildAuthor">
    <xsl:param name="aut"/>

    <xsl:choose>
      <xsl:when test="count(tokenize($aut, '\('))=2 and count(tokenize($aut, '\)'))=2 and count(tokenize($aut, ','))=2">
        <xsl:variable name="date" select="normalize-space(substring-before(substring-after($aut,'\('),'\)'))"/>
        <xsl:choose>
          <xsl:when test="pleade:isnumber(substring-before($date,'-')) and pleade:isnumber(substring-after($date,'-'))">
            <xsl:variable name="nomAut" select="replace(normalize-space(substring-before($aut, ',')), ' ', '-')"/>
            <xsl:variable name="tmp" select="substring-before($aut, '(')"/>
            <xsl:variable name="pNomAut" select="normalize-space(substring-after($tmp, ','))"/>
            <author class="totake"><xsl:value-of select="concat($pNomAut, ' ', $nomAut)"/></author>
            <managed>true</managed>
          </xsl:when>
          <xsl:otherwise><managed>false</managed></xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <managed>false</managed>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>



	<!-- Affiche les tags author.Si auteur composée, affiche un tag pour chaque auteur -->
	<!-- @auteur DIA Modou -->
  <xsl:template name="getAuthors">
    <xsl:param name="author" />
    <xsl:choose>
      <xsl:when test="number(pleade:compteYears($author))=1">
        <xsl:call-template name="buildAuthor">
          <xsl:with-param name="aut" select="$author" />
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="number(pleade:compteYears($author))>1">
        <xsl:call-template name="buildAuthor">
          <xsl:with-param name="aut" select="substring-before($author,'/')" />
        </xsl:call-template>
        <xsl:call-template name="getAuthors">
          <xsl:with-param name="author" select="substring-after($author,'/')" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="buildAuthor">
          <xsl:with-param name="aut" select="$author" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

	<!-- Affiche le tag cote pour un fragment contenant une cote -->
	<!-- @auteur DIA Modou -->
	<xsl:template name="getCote">
    <xsl:choose>
      <xsl:when test="compare(normalize-space($c-fragment-url),'null')!=0">
        <xsl:choose>
          <xsl:when test="doc-available($c-fragment-url)">
            <xsl:variable name="c-fragment" select="doc($c-fragment-url)" />
            <xsl:for-each select="$c-fragment//unitid">
              <cote class="totake"><xsl:value-of select="normalize-space($c-fragment//unitid[1])" /></cote>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>[ZOTERO]: unable to retrieve fragment (url was: <xsl:value-of select="$c-fragment-url"/>)</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
	</xsl:template>


  <!--http://demo.ad33.ajlsm.com/functions/ead/get-properties/FRAD033_IR_29W_30W/display.xml-->
  <xsl:template match="/">
    <zotero>
      <archdesc>
        <xsl:variable name="archdesc-fragment-url" select="concat('cocoon://ead-fragment.debug?c=', /properties/property[@name='archdesc-id'] )"/>
        <xsl:choose>
          <xsl:when test="doc-available($archdesc-fragment-url)">
            <xsl:apply-templates select="doc($archdesc-fragment-url)" mode="go"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>[ZOTERO]: unable to retrieve archdesc fragment (url was: <xsl:value-of select="$archdesc-fragment-url"/>) </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </archdesc>
      <eadheader>
        <xsl:variable name="eadheader-fragment-url" select="concat('cocoon://ead-fragment.debug?c=', /properties/property[@name='eadheader-id'] )"/>
        <xsl:choose>
          <xsl:when test="doc-available($eadheader-fragment-url)">
            <xsl:apply-templates select="doc($eadheader-fragment-url)" mode="go"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>url pour récupération du eadheader : cocoon://ead-fragment.debug?c=<xsl:value-of select="/properties/property[@name='eadheader-id']"/> </xsl:message>
            <xsl:message>[ZOTERO]: unable to retrieve eadheader fragment (url was: <xsl:value-of select="$eadheader-fragment-url"/>) </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </eadheader>
	<book><!-- Récupération des données qui nous interesse -->
		<xsl:variable name="eadheader-fragment-url" select="concat('cocoon://ead-fragment.debug?c=', /properties/property[@name='eadheader-id'] )"/>
		<xsl:choose>
			<xsl:when test="doc-available($eadheader-fragment-url)">
				<xsl:variable name="eadheader-fragment" select="doc($eadheader-fragment-url)" />

				<link class="totake">
					<xsl:value-of select="concat(normalize-space($public-url), '/ead.html?id=', normalize-space($eadheader-fragment//pleade:subdoc/@pleade:eadid))" />
				</link>
				<current><xsl:value-of select="$c-fragment-url" /></current>
				<xsl:for-each select="$eadheader-fragment//titleproper">
					<title class="totake"><xsl:value-of select="normalize-space(.)" /></title>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//author">
					<xsl:call-template name="getAuthors">
						<xsl:with-param name="author" select="normalize-space(.)"/>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//sponsor">
					<sponsor class="totake"><xsl:value-of select="normalize-space(.)" /></sponsor>
				</xsl:for-each>
				<xsl:call-template name="getDates">
					<xsl:with-param name="date" select="if($eadheader-fragment//publicationstmt/date/@normal) then normalize-space($eadheader-fragment//publicationstmt/date/@normal) else normalize-space($eadheader-fragment//publicationstmt/date)"/>
				</xsl:call-template>
				<xsl:for-each select="$eadheader-fragment//publisher">
					<xsl:choose>
						<xsl:when test="contains(normalize-space(.),',')">
							<publisherAddr class="totake"><xsl:value-of select="normalize-space(substring-before(., ','))" /></publisherAddr>
							<publisher class="totake"><xsl:value-of select="normalize-space(.)" /></publisher>
						</xsl:when>
						<xsl:otherwise>
							<publisher class="totake"><xsl:value-of select="normalize-space(.)" /></publisher>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//publicationstmt/address/addressline">
					<publisherAddr class="totake"><xsl:value-of select="normalize-space(.)" /></publisherAddr>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//num">
					<num class="totake"><xsl:value-of select="normalize-space(.)" /></num>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//extref">
					<xsl:element name="doclink">
						<xsl:attribute name="class">totake</xsl:attribute>
						<xsl:attribute name="href"><xsl:value-of select="./@href" /></xsl:attribute>
						<xsl:attribute name="mime-type"><xsl:value-of select="./@pleade:mime-type" /></xsl:attribute>
						<xsl:value-of select="normalize-space(.)" />
					</xsl:element>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//subject">
					<subject class="totake"><xsl:value-of select="normalize-space(.)" /></subject>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//userestrict">
					<rights class="totake"><xsl:value-of select="normalize-space(.)" /></rights>
				</xsl:for-each>
				<xsl:for-each select="$eadheader-fragment//language">
					<lang class="totake"><xsl:value-of select="normalize-space(.)" /></lang>
				</xsl:for-each>
        <!-- FIXME: Incorrect -->
				<archLoc class="totake"><!--<xsl:value-of select="normalize-space($eadheader-fragment//sdx:document/@server)" />--></archLoc>
        <!-- FIXME: Incorrect -->
				<serverName class="totake"><!--<xsl:value-of select="tokenize(normalize-space($eadheader-fragment//sdx:document/@server), '/')[4]" />--></serverName>
				<xsl:for-each select="$eadheader-fragment//notestmt/note/p[1]">
					<bookNote class="totake"><xsl:value-of select="normalize-space(.)" /></bookNote>
					<xsl:choose>
						<xsl:when test="contains(., 'p.') or contains(lower-case(.), 'manuscrits')"><typeDoc class="totake">book</typeDoc></xsl:when>
					</xsl:choose>
				</xsl:for-each>
				<xsl:call-template name="getCote" />
			</xsl:when>
			<xsl:otherwise>
			<xsl:message>[ZOTERO]: unable to retrieve eadheader fragment (url was: <xsl:value-of select="$eadheader-fragment-url"/>) </xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</book>
    </zotero>
  </xsl:template>

  <xsl:template match="node() | @*" mode="go">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="go" />
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>


