<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Displays a menu-bar -->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	 xmlns:nav="http://ajlsm.com/sdx/navigation/cbe/1.0"
	 xmlns:local-conf="http://pleade.org/ns/local-conf/1.0"
	 exclude-result-prefixes="sdx nav local-conf">

	<!-- A variable for a single quote -->
	<xsl:variable name="q">'</xsl:variable>

	<!-- We intercept menu bars unless explicitely stated otherwise -->
	<xsl:template match="menu-bar"/>

	<!-- A menu bar displayed as a simple table -->
	<xsl:template match="menu-bar[@type='simple-table']" mode="output">

		<!-- We output a dynamic part of the script -->
		<script type="text/javascript">
			<!-- Number of top-level menus -->
			<xsl:text>var menuCount = </xsl:text>
			<xsl:value-of select="count(menu)"/>
			<xsl:text>;</xsl:text>
			<!-- Other global parameters -->
			<xsl:text>var menuArray, activeMenu=1, menuOpen=false;</xsl:text>
		</script>
		<!-- A DIV for the whole menu-bar -->
		<div class="menu-bar">
			<table class="top-level-menu">
				<!-- Properties from the menu configuration -->
				<xsl:copy-of select="config/table-properties/@*"/>
				<tr>
					<!-- Properties from the menu configuration -->
					<xsl:copy-of select="config/row-properties/@*"/>
					<!--language interface-->
					<td style="width:50%">
						<xsl:variable name="nb" select="count($g-local-config/local-conf:ui-languages/local-conf:ui-language)"/>
						<xsl:if test="$nb > 1">
							<table style="width:20%;border:none" cellspacing="0" cellpadding="0">
							 	<tr>
							 		<!--introducing languages-->
							 		<td style="text-align:center;vertical-align:middle;white-space:nowrap" class="top-level-menu">
							 			<div class="ui-languages">
							 				<xsl:text>&#xa0;</xsl:text>
							 				<xsl:value-of select="$g-ui-strings/ui-languages/header"/>
							 			</div>
							 		</td>
							 		<!--the select-->
							 		<td>
										<xsl:apply-templates select="$g-local-config/local-conf:ui-languages"/>
									</td>
							 	</tr>
							 </table>
						</xsl:if>
					</td>
					<!-- We process each top level menu definition -->
					<td class="top-level-menu">
						<table cellpadding="0" cellspacing="0" style="border:none">
							<tr style="text-align:center;vertical-align:middle;">
								<xsl:apply-templates select="menu" mode="output"/>
							</tr>
						</table>
					</td>
					<td style="width:50%;text-align:right">
					  <xsl:text>&#160;&#160;</xsl:text>
					</td>
				</tr>
			</table>

			<!-- Hidden DIV for items -->
			<xsl:apply-templates select="menu" mode="items"/>

		</div>
		<xsl:call-template name="display-subset-path"/>
	</xsl:template>

	<!-- One top level menu, output in a visible DIV -->
	<xsl:template match="menu" mode="output">

		<!-- A pointer to the menu-bar configuration -->
		<xsl:variable name="config" select="ancestor-or-self::menu-bar/config"/>

		<!-- A menu is in a table cell -->
		<td style="white-space:nowrap">
			<!-- The content must be in a DIV to allow dynamic behaviour -->
			<div class="{$config/top-level-menu/@class}">
				<!-- The ID depends on the position of the menu -->
				<xsl:attribute name="id">
					<xsl:value-of select="concat('menuLabel',count(preceding-sibling::menu) + 1)"/>
				</xsl:attribute>
				<!-- The mouseover behaviour, only if it has items -->
				<xsl:if test="items/item">
					<xsl:attribute name="onmouseover">
						<xsl:value-of select="concat('menuShow(event,',count(preceding-sibling::menu) + 1,');')"/>
					</xsl:attribute>
				</xsl:if>
				<!-- The onclick behaviour, only when there are no items -->
				<xsl:variable name="ref">
					<xsl:value-of select="concat($g-url-index,'/',@link)"/>
					<xsl:call-template name="menubar-complete-link">
						<xsl:with-param name="menu" select="."/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="not(items/item)">
					<!-- The target of this menu (if there is any) -->
					<xsl:attribute name="onclick">
						<xsl:value-of select="concat('window.location.href=', $q, $ref , $q, ';')"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="not(items/item)">
						<a href="{$ref}">
							<!-- Finally, the menu label itself -->
							<xsl:copy-of select="label[lang($g-lang)]/node()"/>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="style">cursor:default;</xsl:attribute>
						<xsl:copy-of select="label[lang($g-lang)]/node()"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</td>
	</xsl:template>


	<!-- Creates a hidden DIV for the items of a menu -->
	<xsl:template match="menu" mode="items">
		<!-- A pointer to the menu-bar configuration -->
		<xsl:variable name="config" select="ancestor-or-self::menu-bar/config"/>
		<!-- a DIV even if there's no submenu -->
		<div id="{concat('menu',count(preceding-sibling::menu) + 1)}" class="{$config/drop-down-menu/@class}">
			<xsl:choose>
				<!-- If there are items -->
				<xsl:when test="items/item">
					<xsl:attribute name="class"><xsl:value-of select="$config/drop-down-menu/@class"/></xsl:attribute>
				</xsl:when>
				<!--no submenu, we simply hide the div-->
				<xsl:otherwise>
					<xsl:attribute name="style">
						<xsl:value-of select="'visibility:hidden'"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Now the items... -->
			<xsl:apply-templates select="items/item" mode="item"/>
		</div>
	</xsl:template>

	<!-- An item -->
	<xsl:template match="item" mode="item">
		<!-- The target of this menu (if there is any) -->
		<xsl:variable name="ref">
			<xsl:value-of select="concat($g-url-index,'/',@link)"/>
			<xsl:call-template name="menubar-complete-link">
				<xsl:with-param name="menu" select="."/>
			</xsl:call-template>
		</xsl:variable>
		<!-- A DIV to separate menu items -->
		<div class="submenu-separator">
			<p onclick="window.location.href={$q}{$ref}{$q};">
				<!-- Finally, the menu label itself -->
				<a href="{$ref}">
					<xsl:copy-of select="label[lang($g-lang)]/node()"/>
				</a>
			</p>
		</div>
	</xsl:template>

	<!-- To complete a link : override this template -->
	<xsl:template name="menubar-complete-link">
		<xsl:param name="menu" select="."/>
	</xsl:template>

	<!--the subset-path-->
	<xsl:template name="display-subset-path"/>

</xsl:stylesheet>
