<?xml version="1.0" encoding="UTF-8"?>
<!--
SDX: Documentary System in XML.
Copyright (C) 2000, 2001, 2002  Ministere de la culture et de la communication (France), AJLSM

Ministere de la culture et de la communication,
Mission de la recherche et de la technologie
3 rue de Valois, 75042 Paris Cedex 01 (France)
mrt@culture.fr, michel.bottin@culture.fr

AJLSM, 17, rue Vital Carles, 33000 Bordeaux (France)
sevigny@ajlsm.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" xmlns:url="http://www.jclark.com/xt/java/java.net.URLEncoder" exclude-result-prefixes="xsl sdx url">
    <!--
     |
     |  ======== SDX DEFAULT ==========
     |
     |  Cette feuille fournie des modèles basés sur les
     |  éléments SDX par défaut.
     |  Elle est augmentée à l'occasion d'une application
     |  dont certains modèles s'avèrent génériques.
     |
     |  TODO: modulariser plus
     |        relier à une feuille sdx.css
     |        localiser les messages (sans fonction document())
     |
     |-->
    <!--
     |
     |    Présentation de résultats
     |
     |-->
    <!-- sdx:results -->
    <xsl:template match="sdx:results" name="sdx:results">
        <xsl:param name="sdx:results" select="."/>
        <xsl:apply-templates select="$sdx:results" mode="sdx:head"/>
        <hr/>
        <div class="sdx_results">
            <xsl:apply-templates/>
        </div>
        <hr/>
        <xsl:apply-templates select="$sdx:results" mode="sdx:foot"/>
    </xsl:template>
    <!-- ligne d'information de résultats -->
    <xsl:template name="sdx:results-head" match="sdx:results" mode="sdx:head">
        <xsl:param name="sdx:results" select="."/>
        <xsl:param name="action" select="/sdx:document/@uri"/>
        <xsl:param name="target" select="'_self'"/>
        <xsl:param name="search" select="true()"/>
        <xsl:param name="queryParam" select="'q'"/>
        <xsl:param name="bqidParam"/>
        <xsl:param name="qidParam" select="'qid'"/>
        <xsl:if test="$search">
            <xsl:call-template name="sdx:results-search">
                <xsl:with-param name="sdx:results" select="$sdx:results"/>
                <xsl:with-param name="bqidParam" select="$bqidParam"/>
                <xsl:with-param name="queryParam" select="$queryParam"/>
            </xsl:call-template>
        </xsl:if>
        <form class="sdx_results-head" style="display:inline; white-space:nowrap; " action="{$action}" target="{$target}">
            <input type="hidden" name="{$qidParam}" value="{$sdx:results/@qid}"/>
            <xsl:apply-templates select="$sdx:results/@pages"/>
            <xsl:text> &#160; </xsl:text>
            <xsl:apply-templates select="$sdx:results/@nb"/>
            <xsl:text> &#160; </xsl:text>
            <xsl:apply-templates select="$sdx:results/@hpp"/>
        </form>
    </xsl:template>
    <!-- rechercher dans ces résultats -->
    <xsl:template name="sdx:results-search">
        <xsl:param name="sdx:results" select="."/>
        <xsl:param name="action" select="/sdx:document/@uri"/>
        <xsl:param name="target" select="'_self'"/>
        <xsl:param name="bqidParam"/>
        <xsl:param name="queryParam" select="'q'"/>
        <form style="display:inline; white-space:nowrap; " action="{$action}" target="{$target}">
            <input class="sdx_search" type="submit" value="Rechercher"/>
            <input class="sdx_search" accesskey="f" onfocus="this.select()" type="text" name="{$queryParam}" value="{$sdx:results/sdx:query/@text}"/>
            <xsl:if test="$bqidParam">
                <input type="checkbox" title="" checked="checked" name="{$bqidParam}" value="{$sdx:results/@qid}"/>
                <small>dans ces résultats</small>
            </xsl:if>
        </form>
    </xsl:template>
    <!-- présentation des pages -->
    <xsl:template match="sdx:results/@pages | sdx:terms/@pages"/>
    <xsl:template match="sdx:results/@pages[number(.) &gt; 1] | sdx:terms/@pages[number(.) &gt; 1]" name="sdx:results-pages">
        <xsl:param name="pages" select="."/>
        <xsl:text> Page&#160;</xsl:text>
        <xsl:apply-templates select="$pages/../@page"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$pages"/>
        <xsl:text>&#32;</xsl:text>
    </xsl:template>
    <!-- boutons pages précédente-actuelle-suivante, dans un formulaire -->
    <xsl:template match="sdx:results/@page | sdx:terms/@page" name="sdx:results-page">
        <xsl:param name="page" select="."/>
        <xsl:param name="size" select="string-length(string($page/../@pages))"/>
        <xsl:param name="pages" select="$page/../@pages"/>
        <xsl:param name="pageParam">
            <xsl:choose>
                <!-- si navigation de résultat en résultat -->
                <xsl:when test="number(../@hpp)=1">n</xsl:when>
                <xsl:otherwise>p</xsl:otherwise>
            </xsl:choose>
        </xsl:param>
        <xsl:variable name="prev">
            <xsl:choose>
                <xsl:when test="number($page) &gt; 1">
                    <xsl:value-of select="number($page)-1"/>
                </xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="next">
            <xsl:choose>
                <xsl:when test="number($page) &lt; number($pages)">
                    <xsl:value-of select="number($page) +1"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="number($pages)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span style="white-space:nowrap">
            <input class="sdx_prev" accesskey="-" title=" &lt; page {$prev} (alt -) " value="&lt;" type="submit" style="cursor:pointer" onclick="this.form.{$pageParam}.value='{$prev}'; return true"/>
            <input class="sdx_active" style="font-weight:bold;" type="text" name="{$pageParam}" value="{$page}" size="{$size}"/>
            <input class="sdx_next" accesskey="=" value="&gt;" title=" (alt +) page {$next} &gt;" onclick="this.form.{$pageParam}.value='{$next}'; return true;" type="submit" style="cursor:pointer"/>
        </span>
    </xsl:template>
    <!-- nombre de résultats -->
    <xsl:template match="sdx:results/@nb | sdx:terms/@nb"/>
    <xsl:template match="sdx:results/@nb[number(.)&lt;1] | sdx:terms/@nb[number(.)&lt;1]">
    Aucun résultat
    </xsl:template>
    <xsl:template match="sdx:results/@nb[number(.) &gt; 1][../@start != ../@end] | sdx:terms/@nb[number(.) &gt; 1][../@start != ../@end]" name="sdx:results-nb">
        <xsl:param name="nb" select="."/>
        <xsl:param name="start" select="$nb/../@start"/>
        <xsl:param name="end" select="$nb/../@end"/>
        <xsl:text> Résultats&#160;</xsl:text>
        <xsl:value-of select="$start"/>
        <xsl:text>&#160;à&#160;</xsl:text>
        <xsl:value-of select="$end"/>
        <xsl:text>&#160;/&#160;</xsl:text>
        <xsl:value-of select="$nb"/>
        <xsl:text>&#32;</xsl:text>
    </xsl:template>
    <!-- seléctionner le nombre de résultats par page dans un formulaire (sauf hpp=1) -->
    <xsl:template match="sdx:results/@hpp | sdx:terms/@hpp"/>
    <xsl:template match="sdx:results/@hpp[number(.) &gt; 1] | sdx:terms/@hpp[number(.) &gt; 1]" name="sdx:results-hpp">
        <xsl:param name="hpp" select="."/>
        <xsl:param name="hppParam" select="'hpp'"/>
        <xsl:text>&#32;</xsl:text>
        <select name="{$hppParam}">
            <xsl:attribute name="onchange"><xsl:text>this.form.submit(); </xsl:text></xsl:attribute>
            <option value="{normalize-space($hpp)}" selected="selected">
                <xsl:value-of select="$hpp"/>
            </option>
            <option value="5">
                <xsl:text>5</xsl:text>
            </option>
            <option value="10">
                <xsl:text>10</xsl:text>
            </option>
            <option value="20">
                <xsl:text>20</xsl:text>
            </option>
            <option value="50">
                <xsl:text>50</xsl:text>
            </option>
            <option value="100">
                <xsl:text>100</xsl:text>
            </option>
        </select>
        <xsl:text>/page </xsl:text>
    </xsl:template>
    <!-- navigation page à page de résultats -->
    <xsl:template name="sdx:results-foot" match="sdx:results | sdx:terms" mode="sdx:foot">
        <!-- noeud contextuel -->
        <xsl:param name="sdx:results" select="."/>
        <xsl:param name="page" select="$sdx:results/@page"/>
        <xsl:param name="pages" select="$sdx:results/@pages"/>
        <!-- url cible -->
        <xsl:param name="action" select="/sdx:document/@uri"/>
        <xsl:param name="target" select="'_self'"/>
        <xsl:param name="method" select="'get'"/>
        <xsl:param name="search" select="true()"/>
        <!-- paramètres http -->
        <xsl:param name="qidParam" select="'qid'"/>
        <xsl:param name="hppParam" select="'hpp'"/>
        <xsl:param name="pageParam">
            <xsl:choose>
                <!-- si navigation de résultat en résultat -->
                <xsl:when test="number(@hpp)=1">n</xsl:when>
                <xsl:otherwise>p</xsl:otherwise>
            </xsl:choose>
        </xsl:param>
        <xsl:param name="size" select="5"/>
        <!-- style bouton -->
        <xsl:variable name="style-key" select="concat(
        'cursor: pointer; font-family: monospace; font-weight:900; text-align: center; width:',
         string-length($pages)+2,
         'ex;'
         )"/>
        <!-- style lien -->
        <xsl:variable name="style-link" select="concat('text-align:center; width:', string-length(string(@pages)), 'ex')"/>
        <xsl:variable name="href" select="concat($action, '?', $qidParam, '=', @qid, '&amp;', $hppParam, '=', @hpp, '&amp;', $pageParam, '=')"/>
        <xsl:if test="number(@pages) &gt; 1">
            <table style="width:100%;" cellpadding="0" cellspacing="1" border="0" class="pages">
                <tr>
                    <form action="{$action}" target="{$target}" method="{$method}">
                        <input type="hidden" name="{$qidParam}" value="{@qid}"/>
                        <input type="hidden" name="{$hppParam}" value="{@hpp}"/>
                        <td nowrap="nowrap">
                            <!-- |< -->
                            <input class="sdx_first" onclick="this.form.{$pageParam}.value='1'; return true; " type="submit" style="{$style-key}">
                                <xsl:attribute name="value">1</xsl:attribute>
                                <xsl:attribute name="title"> |&lt; page 1 </xsl:attribute>
                            </input>
                            <!-- << -->
                            <xsl:if test="($page - $size - 1) &gt; 1">
                                <input class="sdx_prev" onclick="this.form.{$pageParam}.value='{$page - $size - 1}'; return true; " type="submit" style="{$style-key}">
                                    <xsl:attribute name="value">&lt;&lt;</xsl:attribute>
                                    <xsl:attribute name="title">  &lt;&lt; page <xsl:value-of select="($page - $size - 1)"/></xsl:attribute>
                                </input>
                            </xsl:if>
                        </td>
                        <!-- pages - -->
                        <td style="width:50%;text-align:right;white-space:nowrap">&#160;
                            <xsl:text>&#160;</xsl:text>
                            <xsl:if test="number(@page)&gt;1">
                                <xsl:call-template name="_hppCount">
                                    <xsl:with-param name="min">
                                        <xsl:choose>
                                            <xsl:when test="number(@page)-$size &gt; 0">
                                                <xsl:value-of select="number(@page)-$size"/>
                                            </xsl:when>
                                            <xsl:otherwise>1</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:with-param>
                                    <xsl:with-param name="max" select="number(@page)-1"/>
                                    <xsl:with-param name="href" select="$href"/>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:text>&#160;</xsl:text>
                        </td>
                        <!-- page active -->
                        <td style="width:50%;text-align:center;white-space:nowrap">
                            <xsl:apply-templates select="$sdx:results/@page">
                                <xsl:with-param name="pageParam" select="$pageParam"/>
                            </xsl:apply-templates>
                        </td>
                        <!-- pages + -->
                        <td style="width:50%;text-align:left;white-space:nowrap">
                            <xsl:text>&#160;</xsl:text>
                            <xsl:if test="number(@page) &lt; number(@pages)">
                                <xsl:call-template name="_hppCount">
                                    <xsl:with-param name="min" select="number(@page) + 1"/>
                                    <xsl:with-param name="max">
                                        <xsl:choose>
                                            <xsl:when test="(number(@page)+$size) &lt;= number(@pages)">
                                                <xsl:value-of select="number(@page)+$size"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="number(@pages)"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:with-param>
                                    <xsl:with-param name="href" select="$href"/>
                                    <xsl:with-param name="page" select="$page"/>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:text>&#160;</xsl:text>
                        </td>
                        <td style="white-space:nowrap">
                            <xsl:if test="($page + $size +1) &lt; $pages">
                                <input class="sdx_next" onclick="this.form.{$pageParam}.value='{$page + $size +1}'; return true; " type="submit" style="{$style-key}">
                                    <xsl:attribute name="value">&gt;&gt;</xsl:attribute>
                                    <xsl:attribute name="title"> page <xsl:value-of select="$page + $size + 1"/> &gt;&gt; </xsl:attribute>
                                </input>
                            </xsl:if>
                            <!-- dernière page -->
                            <input class="sdx_last" onclick="this.form.{$pageParam}.value='{$pages}';return true;" value="{$pages}" type="submit" style="{$style-key}">
                                <xsl:attribute name="title"> page <xsl:value-of select="$pages"/> &gt;| </xsl:attribute>
                            </input>
                        </td>
                    </form>
                    <xsl:if test="$search">
                        <td nowrap="nowrap">
                            <xsl:call-template name="sdx:results-search">
                                <xsl:with-param name="sdx:results" select="$sdx:results"/>
                            </xsl:call-template>
                        </td>
                    </xsl:if>
                </tr>
            </table>
        </xsl:if>
    </xsl:template>
    <!-- modèle privé de comptage des pages "hitsPerPage" -->
    <xsl:template name="_hppCount">
        <xsl:param name="href"/>
        <xsl:param name="min"/>
        <xsl:param name="max"/>
        <xsl:variable name="style-link" select="concat('width:', string-length(string(@pages)), 'ex')"/>
        <xsl:text>&#160;</xsl:text>
        <a class="sdx_page" href="{$href}{$min}">
            <xsl:value-of select="
                    format-number(
                        $min,
                        substring (
                            '000000000', 1,
                            string-length(@pages)
                        )
                    )
                        "/>
        </a>
        <xsl:text>&#160;</xsl:text>
        <xsl:if test="$min &lt; $max">
            <xsl:call-template name="_hppCount">
                <xsl:with-param name="min" select="$min+1"/>
                <xsl:with-param name="max" select="$max"/>
                <xsl:with-param name="href" select="$href"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- delete results
should be used in a sdx:results scope
parameters action and target specifies the page wich do the job
 -->
    <xsl:template name="sdx:results-delete">
        <xsl:param name="action" select="/sdx:document/@uri"/>
        <xsl:param name="target" select="'_self'"/>
        <xsl:param name="sdx:results" select="."/>
        <xsl:variable name="query">
            <xsl:apply-templates select="$sdx:results/sdx:query" mode="string"/>
        </xsl:variable>
        <!-- delete documents -->
        <form style="margin:0; display:inline" action="{$action}" target="{$target}" method="get">
            <xsl:for-each select="$sdx:results/sdx:sort/sdx:field">
                <input type="hidden" name="sf" value="{@name}"/>
                <input type="hidden" name="so" value="{@order}"/>
            </xsl:for-each>
            <input type="hidden" name="q" value="{$sdx:results/@luceneQuery}"/>
            <input type="hidden" name="qid" value="{$sdx:results/@qid}"/>
            <input type="hidden" name="p" value="{$sdx:results/@page}"/>
            <input type="hidden" name="hpp" value="{$sdx:results/@hpp}"/>
            <input type="hidden" name="delete" value="true"/>
            <label>
                <xsl:text>Supprimer&#160;</xsl:text>
                <input style="cursor:pointer; " type="submit" name="delete" value="Page {$sdx:results/@page}" title=" Supprimer {$sdx:results/@end - $sdx:results/@start +1} documents, page {$sdx:results/@page} de la requête [{$query}]. " onclick="return confirm(' Êtes-vous sûr de vouloir supprimer {$sdx:results/@end - $sdx:results/@start + 1} documents ? ');"/>
                <input style="cursor:pointer; " type="submit" name="delete" value="[{$query}]" title=" Supprimer {$sdx:results/@nb} documents, les {$sdx:results/@pages} pages de la requête [{$query}]. " onclick="this.form.hpp.value=-1; this.form.p.value=1 ; return confirm(' Êtes-vous sûr de vouloir supprimer {$sdx:results/@nb} documents ? ');"/>
            </label>
        </form>
    </xsl:template>
    <!-- sdx:query -->
    <xsl:template match="sdx:query" mode="string">
        <xsl:choose>
            <xsl:when test="sdx:query[2]">
                <xsl:value-of select="@luceneQuery"/>
            </xsl:when>
            <xsl:when test="sdx:query">
                <xsl:value-of select="sdx:query/@luceneQuery"/>
            </xsl:when>
            <xsl:when test="@type='simple'">
                <xsl:value-of select="@text"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@luceneQuery"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- sdx:result -->
    <xsl:template match="sdx:result" name="sdx:result">
        <xsl:param name="sdx:result" select="."/>
        <!-- paramètre du template -->
        <xsl:param name="action" select="/sdx:document/@uri"/>
        <xsl:param name="target" select="'_self'"/>
        <xsl:param name="delete" select="'false'"/>
        <xsl:param name="action-delete" select="$action"/>
        <xsl:param name="target-delete" select="$target"/>
        <!-- information SDX -->
        <xsl:variable name="id" select="$sdx:result/sdx:field[@name='sdxdocid']/@value"/>
        <table class="sdx_result" border="0" cellpadding="0" cellspacing="0">
            <form action="{$action}" target="{$target}">
                <input type="hidden" name="qid" value="{$sdx:result/../@qid}"/>
                <input type="hidden" name="no" value="{$sdx:result/@no}"/>
                <input type="hidden" name="id" value="{$id}"/>
                <input type="hidden" name="base" value="{$sdx:result/sdx:field[@name='sdxbaseid']/@value}"/>
                <input type="hidden" name="app" value="{$sdx:result/sdx:field[@name='sdxappid']/@value}"/>
                <tr>
                    <td>&#160;</td>
                    <td style="width:20%;vertical-align:top;">
                        <strong>
                            <xsl:value-of select="
                    format-number(
                        $sdx:result/@no,
                        substring (
                            '00000000', 1,
                            string-length($sdx:result/../@nb)
                        )            
                    )
                        "/>
                        </strong>
                    </td>
                    <td>&#160;</td>
                    <xsl:if test="$delete='true'">
                        <td style="width:15%;vertical-align:top;">
                            <input onclick="
                            this.form.no.value=''; 
                            this.form.qid.value=''; 
                            this.form.action='{$action-delete}'; 
                            this.form.target='{$target-delete}';
                            " type="submit" class="sdx_key" title=" Supprimer le document id={$id} " name="delete" value="X"/>
                        </td>
                    </xsl:if>
                    <td style="width:15%;vertical-align:top;">
                        <input type="submit" class="sdx_key" title=" Voir le document id={$id} " name="go" value="&gt;"/>
                    </td>
                    <td>&#160;</td>
                    <td style="width:100%">
                        <xsl:apply-templates select="$sdx:result/node()">
                            <xsl:sort select="@name"/>
                            <xsl:sort select="@value"/>
                        </xsl:apply-templates>
                    </td>
                    <td>&#160;&#160;&#160;</td>
                </tr>
            </form>
        </table>
    </xsl:template>
    <!-- sdx:field -->
    <xsl:template match="sdx:field">
        <div class="sdx_field">
            <span class="sdx_field-name" style="font-weight:bold">
                <xsl:value-of select="@name"/>
            </span>
            <xsl:text>:|</xsl:text>
            <span style="font-weight:bold;" class="sdx_field-value">
                <xsl:value-of select="@value"/>
            </span>
            <xsl:text>|</xsl:text>
        </div>
    </xsl:template>
    <!-- sdx:terms -->
    <xsl:template match="sdx:terms">
        <h2>Termes</h2>
        <ol>
            <xsl:apply-templates/>
        </ol>
    </xsl:template>
    <xsl:template match="sdx:terms[@nb=0]">
        <h2>Aucun résultat.</h2>
    </xsl:template>
    <!-- sdx:term -->
    <xsl:template match="sdx:term">
        <li>
            <xsl:value-of select="@field"/> : <xsl:value-of select="@value"/>
        </li>
    </xsl:template>
    <!--
     |
     | SUPPRESSIONS
     |
     |-->
    <xsl:template match="sdx:deletions[@qid]">
        <div class="sdx_deletions" style="margin-top:1ex; margin-bottom:1ex">
            <h2>
                <xsl:text>Suppressions </xsl:text>
                <small>
        [<xsl:apply-templates select="sdx:query" mode="string"/>],
        <xsl:if test="number(@pages)&gt;1">
                        <xsl:text> page  </xsl:text>
                        <xsl:value-of select="@page"/>,
        </xsl:if>
                    <xsl:value-of select="number(@end) - number(@start) + 1"/>
                    <xsl:text> documents </xsl:text>
                </small>
            </h2>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="sdx:deletions[@app]">
        <xsl:choose>
            <xsl:when test="parent::*[name()= 'sdx:deletions']">
                <h3>
                    <xsl:text>sdxappid:</xsl:text>
                    <span style="color:Highlight; font-weight:bold">
                        <xsl:value-of select="@app"/>
                    </span>
                    <xsl:text> &#160; sdxbaseid:</xsl:text>
                    <span style="color:Highlight; font-weight:bold">
                        <xsl:value-of select="@base"/>
                    </span>
                </h3>
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <div class="sdx_deletions" style="margin-top:1ex; margin-bottom:1ex">
                    <h2>
                        <xsl:text>Suppressions </xsl:text>
                        <small>
                            <xsl:text> sdxappid:</xsl:text>
                            <span style="color:Highlight; font-weight:bold">
                                <xsl:value-of select="@app"/>
                            </span>
                            <xsl:text> &#160; sdxbaseid:</xsl:text>
                            <span style="color:Highlight; font-weight:bold">
                                <xsl:value-of select="@base"/>
                            </span>
                        </small>
                    </h2>
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="sdx:deletions">
        <xsl:apply-templates/>
    </xsl:template>
    <!-- sdx:deletion -->
    <xsl:template match="sdx:deletions//sdx:document">
        <div class="sdx_deletion" style="margin-left:2em">
            <xsl:text> id:</xsl:text>
            <span style="color:Highlight; font-weight:bold">
                <xsl:value-of select="@id"/>
            </span>
            <!--
            <xsl:text> base:</xsl:text>
            <span style="color:Highlight; font-weight:bold">
                <xsl:value-of select="../@base"/>
            </span>
            <xsl:text/>
            <xsl:text> app:</xsl:text>
            <span style="font-weight:bold; color:Highlight">
                <xsl:value-of select="../@app"/>
            </span>
            -->
        </div>
    </xsl:template>
    <!-- sdx:user -->
    <!-- should take messages from a file -->
    <xsl:template match="sdx:user">
        <br/>
        <br/>
        <hr/>
        <small>
            <xsl:if test="not(@anonymous)">
                <xsl:value-of select="@firstname"/>&#160;<xsl:value-of select="@lastname"/>
        &#160;&#160;(<xsl:value-of select="@id"/>),
vous êtes identifié comme
            <xsl:choose>
                    <xsl:when test="@anonymous"/>
                    <xsl:when test="@superuser='true'">
                    superutilisateur.
                </xsl:when>
                    <xsl:when test="@admin='true'">
                    administrateur de l'application
                    <xsl:value-of select="@app"/>.
                </xsl:when>
                    <xsl:when test="@app">
                    utilisateur de lapplication
                    <xsl:value-of select="@app"/>.
                </xsl:when>
                </xsl:choose>
                <br/>
            </xsl:if>
        </small>
        <br/>
        <br/>
    </xsl:template>
    <!-- match sdx:navigation for an include document -->
    <xsl:template name="sdx:navigation" match="sdx:navigation">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <form>
                    <td>
                        <xsl:apply-templates select="sdx:previous"/>
                    </td>
                    <td style="width:100%">&#32;</td>
                    <td style="vertical-align:right;">
                        <xsl:apply-templates select="sdx:next"/>
                    </td>
                </form>
            </tr>
        </table>
    </xsl:template>
    <!-- sdx:next -->
    <xsl:template match="sdx:next">
        <input type="button" class="sdx_next">
            <xsl:attribute name="value">&gt;</xsl:attribute>
            <xsl:attribute name="title"> document suivant de la requête &gt; </xsl:attribute>
            <xsl:attribute name="style">cursor: pointer; font-family: Arial, sans-serif; font-weight:900; width: 3ex; </xsl:attribute>
            <xsl:attribute name="onclick"><!-- target ? --><xsl:text>window.location.href='</xsl:text><xsl:value-of select="/sdx:document/@uri"/><xsl:text>?</xsl:text><xsl:text>app=</xsl:text><xsl:value-of select="@app"/><xsl:text>&amp;</xsl:text><xsl:text>base=</xsl:text><xsl:value-of select="@base"/><xsl:text>&amp;</xsl:text><xsl:text>id=</xsl:text><xsl:value-of select="@docId"/><xsl:text>&amp;</xsl:text><xsl:text>qid=</xsl:text><xsl:value-of select="../@queryId"/><xsl:text>&amp;</xsl:text><xsl:text>n=</xsl:text><xsl:value-of select="@no"/><xsl:text>'</xsl:text></xsl:attribute>
        </input>
    </xsl:template>
    <!-- sdx:previous -->
    <xsl:template match="sdx:previous">
        <input type="button" class="sdx_prev">
            <xsl:attribute name="value">&lt;</xsl:attribute>
            <xsl:attribute name="title"> &lt; document précédent de la requête </xsl:attribute>
            <xsl:attribute name="style">cursor: pointer; font-family: Arial, sans-serif; font-weight:900; width: 3ex; </xsl:attribute>
            <xsl:attribute name="onclick"><!-- target ? --><xsl:text>window.location.href='</xsl:text><xsl:value-of select="/sdx:document/@uri"/><xsl:text>?</xsl:text><xsl:text>app=</xsl:text><xsl:value-of select="@app"/><xsl:text>&amp;</xsl:text><xsl:text>base=</xsl:text><xsl:value-of select="@base"/><xsl:text>&amp;</xsl:text><xsl:text>id=</xsl:text><xsl:value-of select="@docId"/><xsl:text>&amp;</xsl:text><xsl:text>qid=</xsl:text><xsl:value-of select="../@queryId"/><xsl:text>&amp;</xsl:text><xsl:text>n=</xsl:text><xsl:value-of select="@no"/><xsl:text>'</xsl:text></xsl:attribute>
        </input>
    </xsl:template>
    <!-- UPLOAD -->
    <xsl:template match="sdx:uploadDocuments">
        <div style="margin-bottom:1em" class="sdx_uploadDocuments">
            <!--
        <h2>
            <xsl:text>Import de document</xsl:text>
            <xsl:if test="number(sdx:summary/@additions) &gt; 1">s</xsl:if>
        </h2>
-->
            <xsl:apply-templates select="sdx:summary"/>
            <xsl:apply-templates select="sdx:uploadDocument"/>
            <!--
        <xsl:for-each select="//sdx:parameter[@name='url'][1]">
            <br/>
            <strong>
                <xsl:text> URL recherchée : </xsl:text>
            </strong>
            <div>
                <iframe width="100%" height="200" src="{@value}" title="Un nouveau document cherchable"/>
            </div>
        </xsl:for-each>
-->
        </div>
    </xsl:template>
    <xsl:template match="sdx:uploadDocument">
        <div style="margin-left:2em" class="sdx_uploadDocument">
            <strong>
                <xsl:value-of select="@id"/>
            </strong>
            <xsl:text> indexé. </xsl:text>
        </div>
    </xsl:template>
    <xsl:template match="sdx:uploadDocument[sdx:exception]">
        <li>
            <strong>
                <xsl:value-of select="@id"/>
            </strong>
            <xsl:text> erreur. </xsl:text>
            <xsl:apply-templates select="sdx:exception/sdx:message"/>
        </li>
    </xsl:template>
    <xsl:template match="sdx:summary">
        <div class="sdx_summary">
            <xsl:apply-templates select="@*"/>
        </div>
    </xsl:template>
    <xsl:template match="@additions">
        <xsl:if test="number(.) &gt; 0">
            <xsl:value-of select="."/> document<xsl:if test="number(.) &gt; 1">s</xsl:if>
        ajouté<xsl:if test="number(.) &gt; 1">s</xsl:if>,
        </xsl:if>
    </xsl:template>
    <xsl:template match="@failures">
        <xsl:if test="number(.) &gt; 0">
            <xsl:value-of select="."/> échec<xsl:if test="number(.) &gt; 1">s</xsl:if>,
        </xsl:if>
    </xsl:template>
    <xsl:template match="@replacements">
        <xsl:value-of select="."/> document<xsl:if test="number(.) &gt; 1">s</xsl:if> remplacé<xsl:if test="number(.) &gt; 1">s</xsl:if>
    </xsl:template>
    <xsl:template match="@duration"> (<xsl:value-of select="."/> secondes).</xsl:template>
    <xsl:template match="sdx:uploadDocument/sdx:exception">
        <h2>Erreur</h2>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="sdx:uploadDocument/sdx:exception/sdx:message">
        <small>
            <xsl:apply-templates/>
        </small>
    </xsl:template>
    <!-- HILITE -->
    <xsl:template match="sdx:hilite">
        <span class="sdx_hilite" style="background:#FFFFD0; color:black;">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="sdx:*" priority="-1">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="sdx:*/@*" priority="-1"/>
</xsl:stylesheet>
