<?xml version="1.0"?>
<!-- $Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="fn xs xsl" version="2.0"
>

	<!-- Une collection de "fonctions" (xsl:function ou xsl:template name="") qui
		 peuvent être utiles un peu partout dans l'application. -->

	<!-- Retourne la partie "path" d'une URL relative -->
	<xsl:function name="pleade:get-url-path" as="xs:string">
		<xsl:param name="url"/>
		<xsl:variable name="parts" select="fn:tokenize($url, '/')"/>
		<xsl:variable name="res">
			<xsl:for-each select="$parts[position() != last()]">
				<xsl:if test="position() > 1">/</xsl:if>
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="$res"/>
	</xsl:function>

	<!-- Retourne la partie "file" d'une URL relative -->
	<xsl:function name="pleade:get-url-file" as="xs:string">
		<xsl:param name="url"/>
		<xsl:variable name="parts" select="fn:tokenize($url, '/')"/>
		<xsl:value-of select="$parts[position() = last()]"/>
	</xsl:function>

	<!-- Retourne la partie "file" sans extension d'une URL relative -->
	<xsl:function name="pleade:get-url-file-wo-extension" as="xs:string">
		<xsl:param name="url"/>
		<xsl:variable name="file-part" select="pleade:get-url-file($url)"/>
		<xsl:value-of select="fn:tokenize($file-part, '\.')[1]"/>
	</xsl:function>

	<!-- Retourne la partie extension d'une URL ou d'un nom de fichier -->
	<xsl:function name="pleade:get-extension" as="xs:string">
		<xsl:param name="file"/>		<!-- Le nom à vérifier -->
		<!-- La logique consiste à fractionner l'URL sur différents caractères:
				? ou # et on prend le premier morceau (avant les paramètres ou le fragment)
				/ ou \ et on prend le dernier morceau (partie "fichier")
				. et on prend le dernier morceau (extension)
		-->
		<xsl:value-of select="fn:tokenize(fn:tokenize(fn:tokenize($file, '(#|\?)')[1], '(/|\\)')[last()], '\.')[last()]"/>
	</xsl:function>

	<!-- Retourne le type MIME d'un URL -->
	<xsl:function name="pleade:get-mimetype" as="xs:string">
		<!-- On doit passer le paramètre "url" -->
		<xsl:param name="url"/>
		<!-- Pour les adresses de type http://www.google.fr, il faut passer ce qui suit le nom
				d'hôte -->
		<xsl:choose>
			<xsl:when test="fn:matches($url, '^[a-z]+://[a-z\.]+')">
				<xsl:variable name="path">
					<xsl:variable name="b" select="substring-after($url, '://')"/>
					<xsl:choose>
						<xsl:when test="contains($b, '/')">
							<xsl:value-of select="substring-after($b, '/')"/>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$path = ''">
						<xsl:text>text/html</xsl:text>	<!-- On suppose du HTML sur le Web... -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="mimeutils:getMIMEType(stringutils:toLowerCase(concat('.', string(pleade:get-extension($path)))))" xmlns:mimeutils="java:org.apache.cocoon.util.MIMEUtils" xmlns:stringutils="java:org.pleade.utils.StringUtilities"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- A partir d'une extension, on appelle une classe Cocoon -->
				<xsl:value-of select="mimeutils:getMIMEType(stringutils:toLowerCase(concat('.', string(pleade:get-extension($url)))))" xmlns:mimeutils="java:org.apache.cocoon.util.MIMEUtils" xmlns:stringutils="java:org.pleade.utils.StringUtilities"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- TODO: remplacer ces deux fonctions par quelque chose qui utilise
		http://commons.apache.org/lang/api-release/org/apache/commons/lang/StringEscapeUtils.html
	-->
	<!-- Echappe une chaîne de caractères pour insérer dans une variable Javascript -->
	<xsl:function name="pleade:escape-js-string" as="xs:string">
		<xsl:param name="s"/>
		<xsl:param name="del"/>
    <xsl:choose>
      <xsl:when test="$s='' or $del=''">  <!-- chaine vide ou caractere a echappe vide -->
        <xsl:value-of select="$s" />
      </xsl:when>
      <xsl:when test="not(contains($s, $del))"> <!-- le caractere a echappe n'est pas dans la chaine, pas de besoin de l'echapper -->
        <xsl:value-of select="$s" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="pleade:escape-string($s, $del, '\\')"/>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:function>

	<!-- Echappe une chaîne de caractères -->
	<xsl:function name="pleade:escape-string" as="xs:string">
		<xsl:param name="s"/>
		<xsl:param name="c"/>
		<xsl:param name="e"/>
		<xsl:value-of select="fn:replace($s, $c, concat($e, $c))"/>
	</xsl:function>

	<xsl:function name="pleade:format-number" as="xs:string">
		<xsl:param name="n" />
		<xsl:value-of select="pleade:format-number($n, '', '', '')" />
	</xsl:function>
	<xsl:function name="pleade:format-number" as="xs:string">
		<xsl:param name="n" />
		<xsl:param name="l" />
		<xsl:param name="sz" />
		<xsl:param name="sp" />
		<xsl:variable name="_sp">
			<xsl:choose>
				<xsl:when test="$l='fr' or $l=''">
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="_sz">
			<xsl:choose>
				<xsl:when test="number($sz)">
					<xsl:value-of select="$sz" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(3)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:number grouping-separator="{$_sp}"
								grouping-size="{$_sz}"
								lang="{$l}" value="$n" />
	</xsl:function>

	<xsl:function name="pleade:truncate-string" as="xs:string">
		<xsl:param name="string"/>
		<xsl:param name="n"/>
		<xsl:value-of select="pleade:truncate-string($string, $n, ' [...]')"/>
	</xsl:function>
	<xsl:function name="pleade:truncate-string" as="xs:string">
		<!--Renvoie les n premiers caractères d'une chaîne. (taille de la chaîne = L, taille de endString = e)
		Si L est sup à (n-e), renvoie "(n-e) premiers caractères" + "endString"
		Si L est inf à n-e caractères, renvoie L sans changements
		=> La chaîne renvoyée a donc max n caractères (endString compris)-->
		<xsl:param name="string"/>
		<xsl:param name="n"/>
		<xsl:param name="endString"/>
		<xsl:variable name="l" select="string-length($string)" />
		<xsl:variable name="e" select="string-length($endString)"/>
		<xsl:choose>
			<xsl:when test="$l > ($n - $e)">
				<xsl:value-of select="concat(substring( $string, 1, ($n - $e) ), $endString)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:function name="pleade:hilite-string" as="xs:boolean">
		<xsl:param name="string"/>
		<xsl:variable name="escape-hilite-list" select="'0,1'"/>
		<xsl:variable name="escape-hilite-list-token" select="tokenize($escape-hilite-list, ',')"/>
		<xsl:choose>
			<xsl:when test="$escape-hilite-list-token[ . = $string]">
				<xsl:text>false</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>true</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

		<!-- Une fonction qui permet de savoir si un élément est un lien -->
	<xsl:function name="pleade:cdc-entry-hidden" as="xs:boolean">
		<xsl:param name="cdc"/>
		<xsl:param name="id"/>
		<xsl:value-of select="boolean($cdc/archdesc//c[@pleade:component = 'true'][@pleade:id = $id][@altrender='hidden' or @altrender='hidden-in-results'])"/>
	</xsl:function>

	<!-- Une fonction qui permet de savoir si un élément représente un objet numérique -->
	<xsl:function name="pleade:is-object" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:value-of select="boolean($el/self::dao or $el/self::daoloc)"/>
	</xsl:function>

	<!-- Une fonction qui permet de récupérer le type d'objet -->
	<xsl:function name="pleade:object-type" as="xs:string">
		<xsl:param name="dao"/>
		<xsl:choose>
			<xsl:when test="not($dao/@pleade:mime-type) and $dao/@pleade:link-type = 'series'">
				<xsl:text>image</xsl:text>
			</xsl:when>
			<xsl:when test="not($dao/@pleade:mime-type) and not($dao/@pleade:link-type = 'series')">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="$dao/@pleade:mime-type = '' and $dao/@pleade:link-type = 'absolute'">
				<xsl:text>external</xsl:text>
			</xsl:when>
			<xsl:when test="contains($dao/@pleade:mime-type, '/')">
				<xsl:value-of select="substring-before($dao/@pleade:mime-type, '/')"/>
			</xsl:when>
			<xsl:when test="$dao/@pleade:mime-type and $dao/@pleade:mime-type = ''">
				<xsl:text>other</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$dao/@pleade:mime-type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- Une fonction pour savoir si l'élément courant possède un descendant illustré -->
	<xsl:function name="pleade:has-object-descendant" as="xs:boolean">
		<xsl:param name="el"/>
		<xsl:value-of select="boolean( $el/descendant::*[@pleade:break = 'true' and descendant::dao][1]
																or $el/descendant::*[@pleade:break = 'true' and descendant::daoloc][1] )"/>
	</xsl:function>

	<!--+
			| Une fonction pour savoir si le champ courant est de type date
			|
			| La difficulté est qu'on sait seulement que les champs de type "date"
			| par défaut de Pleade sont : "edate", "bdate", "sdxmoddate".
			| La manière utilisée ici pour reconnaître les champs "date" est de
			| regarder s'il s'agit de l'un des champs donnés ci-avant, sinon on
			| supprime les chiffres de la valeur à traiter et s'il ne reste que
			| "- -T::Z" (dans les guillemets, l'espace entre les tirets n'est là que
			| parce qu'on n'a pas le droit de mettre 2 tirets à la suite dans un
			| commentaire XML) alors on sait qu'on traite un champ de type "date"
			+-->
	<xsl:function name="pleade:is-date-field" as="xs:boolean">
		<xsl:param name="f"/>
		<xsl:param name="v"/>
		<xsl:choose>
			<xsl:when test="$f='' and $v=''">0</xsl:when>
		  <xsl:when test="$f='edate' or $f='bdate' or $f='sdxmoddate'">1</xsl:when>
			<xsl:when test="fn:replace($v,'[0-9]','')='--T::Z'">1</xsl:when>
		  <xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!--+
	    | Retourne "true" ou "false" si l'URL est absolue ou non
			|
			| Pour savoir si une URL est absolue, on regarde si l'URL débute pas un
			| protocole d'URL (^[a-z]+:/) ou si elle débute pas "mailto:"
			|
			| #1: débute par une lettre de lecteur (MS Windows)
			| #2: débute par un protocole (eg, http://)
			| #3: un mailto:
	    +-->
	<xsl:function name="pleade:is-url-absolute" as="xs:boolean">
		<xsl:param name="url" />
		<xsl:value-of select="fn:matches($url,'^[a-z]:/','i')
													or fn:matches($url,'^[a-z]{2,}:/+','i')
													or starts-with($url,'mailto:')" />
	</xsl:function>

	<!-- Convertir les URLs Windows en URIs
			 2 cas de figures : 1) on a une URL absolue ; 2) on a une URL relative.-->
	<xsl:function name="pleade:windowsfilepath_to_uri" as="xs:string">
		<xsl:param name="href" />
		<xsl:choose>
			<xsl:when test="pleade:is-url-absolute($href)"><xsl:value-of select="pleade:relative_windowsfilepath_to_uri($href)" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="pleade:relative_windowsfilepath_to_uri($href)" /></xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- http://blogs.msdn.com/ie/archive/2006/12/06/file-uris-in-windows.aspx -->
	<xsl:function name="pleade:absolute_windowsfilepath_to_uri" as="xs:string">
		<xsl:param name="filepath" as="xs:string"/>
		<!-- Conversion steps:
				 1) replace backslashes with slashes
				 2) encode percent sign
				 3) percent-encode URI fragment identifier character
				 4) percent-encode characters illegal in URIs, using iri-to-uri
				 5) add three slashes before drive letter
				 6) prefix result with "file:" -->
		<xsl:value-of select="concat( 'file:', replace( pleade:relative_windowsfilepath_to_uri($filepath), '^([A-Za-z]):', '///$1:') )"/>
	</xsl:function>
	<xsl:function name="pleade:relative_windowsfilepath_to_uri" as="xs:string">
		<xsl:param name="filepath" as="xs:string"/>
		<!-- Conversion steps:
				 1) replace backslashes with slashes
				 2) encode percent sign
				 3) percent-encode URI fragment identifier character
				 4) percent-encode characters illegal in URIs, using iri-to-uri -->
		<xsl:value-of select="iri-to-uri( replace( replace( replace($filepath, '\\', '/'), '%', '%25'), '#', '%23') )"/>
	</xsl:function>
	
	<!-- Une fonction qui retourne la cote correction formatée ; on traite le cas des cotes pour éviter la série de cotes extrêmes -->
	<xsl:function name="pleade:format-unitid" as="xs:string">
		<xsl:param name="unitid"/>
		<xsl:choose>
			<!-- Cas des cotes extrêmes multiples, on ne prend que la première et la dernière -->
			<xsl:when test="contains($unitid, ' ; ')">
				<xsl:variable name="parts" select="tokenize($unitid, ';')"/>
				<xsl:value-of select="if ($parts != '') then concat(normalize-space($parts[1]), ' (...) ', normalize-space($parts[position() = last()])) else normalize-space($unitid)"/>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="normalize-space($unitid)"/></xsl:otherwise>
		</xsl:choose>
	</xsl:function>

</xsl:stylesheet>
