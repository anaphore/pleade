//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion des statistiques dans la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion des statistiques dans la partie admin
 * @author Julien Huet
 * @augments HelpPanels
 */
var Stats = new Class(/** @lends Stats */{
	Implements: [HelpPanels],

	/**
	 * <strong>Constructor</strong><br/> Inialisation des tabulations
	 */
	initialize: function(b_tabs) {
		this.initHelpPanels();
		if ( b_tabs ) {
			this.tabs = new YAHOO.widget.TabView('tabs');
		}
	}
});