<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Coeur de page : affichage de l'historique des requêtes
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns="http://www.w3.org/1999/xhtml"
                              xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                              xmlns:xsp="http://apache.org/xsp"
                              xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                              xmlns:pleade="http://pleade.org/ns/pleade/1.0"
                              xmlns:fn="http://www.w3.org/2005/xpath-functions"
															xmlns:input="http://apache.org/cocoon/xsp/input/1.0"
                              exclude-result-prefixes="xsl xsp sdx i18n pleade input">

  <!-- Fonctions d'utilités générales -->
  <xsl:import href="../../commons/xsl/functions.xsl"/>
  <!-- Affichage des résultats de recherche : on s'en sert pour l'affichage lisible de la requête (mode="criteria") -->
	<xsl:import href="cocoon://cdc-results.xsl"/>

	<!-- Langue d'interface -->
	<xsl:param name="locale" select="'fr'"/>

  <!-- TODO (MP) : Localiser la sortie de la date ? Envoyer ceci dans functions.xsl ?  -->
	<xsl:variable name="date-format" select="string('dd/MM/yyyy HH:mm:ss')" />
	<!-- TimeZone du serveur -->
	<xsl:variable name="utz-url" select="'cocoon://functions/commons/get-input-module.xml?name=user.timezone'"/>
	<xsl:variable name="utz" select="
			if(doc-available($utz-url)) then document($utz-url)/get-input-module/input:attribute-values[@name='user.timezone']/input:value
			else 'Europe/Paris'
	"/>

  <!--+
      | Création de la racine XHTML
      +-->
  <xsl:template match="/sdx:document">
    <html>
      <xsl:apply-templates select="." mode="html-head" />
      <xsl:apply-templates select="." mode="html-body" />
    </html>

  </xsl:template>

  <!--+
      | Construction de l'en-tête XHTML
      +-->
  <xsl:template match="sdx:document" mode="html-head">

    <head>

      <title>
        <i18n:text key="historic.html.title">historique des requêtes</i18n:text>
      </title>

      <xsl:apply-templates select="." mode="script-head" />
    </head>

  </xsl:template>

  <!--+
      | Construction du corps de page XHTML
      +-->
  <xsl:template match="sdx:document" mode="html-body">

    <body>

      <table cellspacing="0" cellpadding="0" summary="Placement du titre" class="pl-title pl-box">
        <tbody>
          <tr>
            <td class="pl-box-title">
              <h1>
                <i18n:text key="historic.page.title">historique des requêtes</i18n:text>
              </h1>
            </td>
            <td class="pl-box-title-button">
              <xsl:apply-templates select="." mode="manage-historic-buttons" />
            </td>
          </tr>
        </tbody>
      </table>
			
			<i18n:text key="historic.intro"/>
			
      <xsl:apply-templates select="." mode="html-body-total-server" />

    </body>

  </xsl:template>

  <!--+
      | Boutons générales de gestion de l'historique
      |
      | Pour l'instant, un unique bouton pour vider complètement l'historique
      +-->
  <xsl:template match="sdx:document" mode="manage-historic-buttons">
    <xsl:variable name="class" select="string('yui-button yui-push-button')" />
      <xsl:variable name="onclick">
        <xsl:text>if(confirm(_usersMessagesHisto.historic_flush_historic_confirm)) window.location.href="?did=all";</xsl:text>
      </xsl:variable>
      <button id="flush-histo" type="button" class="{$class} pl-histo-flush" title="historic.button.flush-historic.title" i18n:attr="title">
        <xsl:attribute name="onclick">
          <xsl:value-of select="$onclick" />
        </xsl:attribute>
        <xsl:attribute name="onkeypress">
          <xsl:value-of select="$onclick" />
        </xsl:attribute>
        <xsl:if test="not(number(sdx:historic/@nb) &gt; 0)">
          <xsl:attribute name="disabled">
            <xsl:text>disabled</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="class">
            <xsl:value-of select="concat( $class, ' pl-histo-flush disabled' )" />
          </xsl:attribute>
        </xsl:if>
        <i18n:text key="historic.button.flush-historic.text">
          <xsl:text>vider l'historique</xsl:text>
        </i18n:text>
      </button>
  </xsl:template>

  <!--+
      | Contenu HTML
      +-->
  <xsl:template match="sdx:document" mode="html-body-total-server">

    <xsl:choose>
      <xsl:when test="message">
        <xsl:apply-templates select="message" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="sdx:historic" mode="total-server" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--+
      | Règle surchargeable pour sortir des bouts de Javascripts supplémentaires
      | dans l'en-tête XHTML
      +-->
  <xsl:template match="sdx:document" mode="script-head">

    <script type="text/javascript" src="i18n/module-eadeac-historic-js.js">//</script>
    <!--<script type="text/javascript" src="js/module-eadeac-historic.js">//</script>-->

    <xsl:apply-templates select="." mode="script-head-total-server" />

  </xsl:template>

  <!--+
      | Script d'en-tête du mode "total-server"
      +-->
  <xsl:template match="sdx:document" mode="script-head-total-server" />

   <!--+
       | Affichage d'un message
       +-->
  <xsl:template match="message">
    <p class="pl-message">
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <!--+
      | Message de l'historique vide
      +-->
  <xsl:template match="sdx:historic" mode="no-content">
    <p class="pl-form-message">
      <i18n:text key="historic.results-title.none">votre historique est vide</i18n:text>
    </p>
  </xsl:template>

  <!--+
      | Traitement de l'historique en mode "total-server"
      +-->
  <xsl:template match="sdx:historic" mode="total-server">

    <xsl:choose>
      <xsl:when test="not( number(@nb) &gt; 0 )">
        <xsl:apply-templates select="." mode="no-content" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="table-html" />
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!--+
      | Affichage d'un ensemble de documents appartenant à la même base
      | Ici, on construit une TABLE qui sera formatée ensuite par le module
      | Datatable de YUI
      +-->
  <xsl:template match="sdx:historic" mode="table-html">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="pl-histo"
            class="pl-rslts" summary="Le contenu de l'historique">

      <thead>
        <tr>
          <th class="label pl-tbl-th" scope="col">
            <xsl:comment>id</xsl:comment>
          </th>
		  <!-- FIXME: les cases à cocher ne sont pas nécessaires si on ne combine pas. -->
          <!-- <th class="label pl-tbl-th" scope="col">
            <xsl:comment>select</xsl:comment>
          </th> -->
          <th class="label pl-tbl-th" scope="col">
            <i18n:text key="historic.results.col-title.date">date</i18n:text>
          </th>
          <th class="label pl-tbl-th" scope="col">
            <i18n:text key="historic.results.col-title.title">requête</i18n:text>
          </th>
          <th class="label pl-tbl-th" scope="col">
            <i18n:text key="historic.results.col-title.nb">résultats</i18n:text>
          </th>
          <th class="label pl-tbl-th pl-histo-bttn-col" scope="col" axis="buttons">
            <xsl:comment>ex</xsl:comment>
          </th>
          <th class="label pl-tbl-th pl-histo-bttn-col" scope="col" axis="buttons">
            <xsl:comment>mod</xsl:comment>
          </th>
          <th class="label pl-tbl-th pl-histo-bttn-col" scope="col" axis="buttons">
            <xsl:comment>del</xsl:comment>
          </th>
        </tr>
      </thead>

      <tbody>
        <!-- Affichage des requêtes dans l'ordre décroissant des dates d'exécution -->
        <xsl:apply-templates select="sdx:search" mode="table-html">
          <xsl:sort select="@date" order="descending" />
        </xsl:apply-templates>
      </tbody>

    </table>

  </xsl:template>

  <!--+
      | Un historique dans un tableau XHTML
      +-->
  <xsl:template match="sdx:search" mode="table-html">

    <xsl:variable name="odd">
      <xsl:choose>
        <xsl:when test="position() mod 2 = 0"><xsl:text>even</xsl:text></xsl:when>
        <xsl:otherwise><xsl:text>odd</xsl:text></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr class="pl-tbl-tr-{$odd}">

      <!-- id -->
      <td>
        <xsl:value-of select="position()" />
      </td>

      <!-- select -->
	  <!-- FIXME: pas utile tant qu'on ne combine pas? -->
      <!-- <td>
        <input type="checkbox" class="pl-form-chckbx" />
      </td> -->

      <!-- date -->
      <td>
        <!-- Le format d'entrée de la date : on peut avoir sans TimeZone ou avec -->
				<xsl:variable name="format-in">
          <xsl:analyze-string select="@date"
                              regex="^\d\d\d\d\-\d\d\-\d\dT\d\d:\d\d:\d\d$">
            <xsl:matching-substring>
              <xsl:text>yyyy-MM-dd'T'HH:mm:ss</xsl:text>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
              <xsl:text>yyyy-MM-dd'T'HH:mm:ssZ</xsl:text>
            </xsl:non-matching-substring>
          </xsl:analyze-string>
        </xsl:variable>
        <!-- On connaît le format, maintenant on parse et on formate. Pour que le formatage, soit correct il faut préciser une timeZone -->
				<xsl:value-of select="dfu:format( sdf:parse(sdf:new($format-in),@date), $date-format, tz:getTimeZone($utz))"
										xmlns:sdf="java:java.text.SimpleDateFormat"
										xmlns:tz="java:java.util.TimeZone"
										xmlns:dfu="org.apache.commons.lang.time.DateFormatUtils"/>
      </td>

      <!-- title -->
      <xsl:apply-templates select="@queryString" mode="table-html" />

      <!-- nb -->
      <td>
        <xsl:value-of select="pleade:format-number( @nb )" />
      </td>

      <!-- buttons -->
      <xsl:apply-templates select="." mode="table-html-buttons" />

    </tr>

  </xsl:template>

  <!--+
      | Affichage de la requête
      |
      | On se sert du code de "cdc-results.xsl", mode="criteria"
      +-->
  <xsl:template match="@queryString" mode="table-html">

    <xsl:variable name="parameters">
      <sdx:parameters>
        <xsl:for-each select="tokenize(., '&amp;')">
          <xsl:variable name="n" select="substring-before(., '=')" />
          <xsl:variable name="v" select="substring-after(., '=')" />
          <sdx:parameter name="{$n}" value="{$v}"/>
        </xsl:for-each>
      </sdx:parameters>
    </xsl:variable>

	<xsl:variable name="formid">
		<xsl:value-of select="$parameters/sdx:parameters/sdx:parameter[@name = 'name']/@value"/>
	</xsl:variable>

    <xsl:variable name="criteria">
      <xsl:apply-templates select="$parameters/sdx:parameters/sdx:parameter[starts-with(@name, 'champ')] " mode="criteria">
        <xsl:sort select="number(substring-after( @name, 'champ'  ))"/>
	<xsl:with-param name="current_form-id" select="$formid"/>
      </xsl:apply-templates>
    </xsl:variable>

    <td>

      <xsl:choose>

				<xsl:when test="$criteria/pleade:criterion">
					<xsl:apply-templates select="$criteria/pleade:criterion" mode="criteria"/>
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="." />
				</xsl:otherwise>

			</xsl:choose>

    </td>

  </xsl:template>

  <!--+
      | Les boutons de gestion d'une requête dans l'historique
      +-->
  <xsl:template match="sdx:search" mode="table-html-buttons">

    <xsl:variable name="class" select="string('yui-button yui-push-button')" />

    <!-- ex -->
    <td class="pl-histo-bttn" axis="buttons">
      <button id="ex" type="button" class="{$class} pl-histo-ex" i18n:attr="title" title="historic.button.execute.title">
        <xsl:attribute name="onclick">javascript:window.location.href="results.html?<xsl:value-of select="@escapedQueryString" />";</xsl:attribute>
        <i18n:text key="historic.button.execute.text">
          <xsl:text>exécuter</xsl:text>
        </i18n:text>
      </button>
    </td>
    <!-- mod -->
    <td class="pl-histo-bttn" axis="buttons">
    	<xsl:if test="not(contains(@escapedQueryString, 'search_type=simple'))">
		<button id="mod" type="button" class="{$class} pl-histo-mod" i18n:attr="title" title="historic.button.modify.title">
			<xsl:attribute name="onclick">javascript:window.location.href="search-form.html?<xsl:value-of select="@escapedQueryString" />";</xsl:attribute>
				<i18n:text key="historic.button.modify.text">
					<xsl:text>modifier</xsl:text>
				</i18n:text>
		</button>
	</xsl:if>
    </td>
    <!-- del -->
    <td class="pl-histo-bttn" axis="buttons">
      <button id="del" type="button" class="{$class} pl-histo-del" i18n:attr="title" title="historic.button.delete.title">
        <xsl:attribute name="onclick">javascript:window.location.href="?did=<xsl:value-of select="@id" />";</xsl:attribute>
        <i18n:text key="historic.button.delete.text">
          <xsl:text>supprimer</xsl:text>
        </i18n:text>
      </button>
    </td>

  </xsl:template>

</xsl:stylesheet>
