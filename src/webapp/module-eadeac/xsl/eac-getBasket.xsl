<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: getBasket.xsl 19866 2011-01-06 10:24:52Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Coeur de page : affichage du contenu du panier
    |
    | Cette XSL prend en compte les documents de la base EAD
    | Pour les autres bases, l'affichage doit être pris en charge par l'XSLT qui
    | surchage dans une fonction dont la signature est :
    | <xsl:template match="sdx:result[sdx:field[@name='sdxdbid']/@value='{$base}']" mode="result">
    |
    | TODO (MP) : Implémenter l'affichage des doc EAC ?
    +-->
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:xsp="http://apache.org/xsp"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  exclude-result-prefixes="xsl xsp sdx i18n pleade fn">

  <!-- Fonctions d'utilités générales -->
  <xsl:import href="../../commons/xsl/functions.xsl"/>
  <!-- Le code pour traiter la navigation d'une page à l'autre -->
  <xsl:import href="navigation.xsl"/>

  <!-- La base courante -->
  <xsl:param name="base" select="'ead'"/>
  <!-- Tri stocké en session -->
  <xsl:param name="sort_field" select="''"/>
  <xsl:param name="sort_order" select="''"/>

  <!--+
      | Création de la racine XHTML
      +-->
  <xsl:template match="/sdx:document">
    <div>
      <xsl:apply-templates select="sdx:caddy"/>
    </div>
  </xsl:template>

  <!--+
      | Traitement du panier
      +-->
  <xsl:template match="sdx:caddy">

    <xsl:choose>
      <xsl:when test="not( number(@nb) &gt; 0 )">
        <xsl:apply-templates select="." mode="no-content" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="datatable" />
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!--+
      | Message de panier vide
      +-->
  <xsl:template match="sdx:caddy" mode="no-content">
    <p class="pl-form-message">
      <i18n:text key="basket.results-title.none">votre panier est vide</i18n:text>
    </p>
  </xsl:template>

  <!--+
      | Affichage des résultats de recherche
      +-->
  <xsl:template match="sdx:caddy" mode="datatable">
    <xsl:call-template name="pagination"/>
    <xsl:apply-templates select="." mode="table-html"/>
  </xsl:template>

  <!--+
      | Le résumé de la recherche avec son résultat
      +-->
  <xsl:template match="sdx:results" mode="resume">
    <p class="pl-results-count">
      <xsl:variable name="n">
        <xsl:choose>
          <xsl:when test="function-available('pleade:format-number')">
            <xsl:value-of select="pleade:format-number(@nb)" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@nb" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <strong>
        <xsl:value-of select="$n"/>
      </strong>

      <xsl:variable name="k">
        <xsl:choose>
          <xsl:when test="number($n) &gt; 1">
            <xsl:text>basket.results-title.many</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>basket.results-title.one</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <i18n:text key="{$k}">
        <xsl:value-of select="$k" />
      </i18n:text>
    </p>
  </xsl:template>

   <!--+
       | Affichage d'un message
       +-->
  <xsl:template match="message">
    <p class="pl-message">
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <!--+
      | Affichage d'un ensemble de documents appartenant à la même base
      +-->
  <xsl:template match="sdx:caddy" mode="table-html">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="resultats yui-dt" summary="Le contenu du panier" id="pl-bskt">
      <thead>
        <tr>
          <th class="label pl-tbl-th"><xsl:comment>u</xsl:comment></th>
          <th class="label pl-tbl-th">
            <i18n:text key="basket.results.col-title.intitule">intitulé</i18n:text>
          </th>
          <!--<th class="label pl-tbl-th">
            <i18n:text key="basket.results.col-title.date">date</i18n:text>
          </th>
          <th class="label pl-tbl-th">
            <i18n:text key="basket.results.col-title.cote">cote</i18n:text>
          </th>-->
        </tr>
      </thead>
      <tbody>
        <xsl:apply-templates select="/sdx:document/sdx:results" mode="result"/>
      </tbody>
    </table>
  </xsl:template>

  <!--+
      | Construction d'une ligne de résultat pour les documents EAD
      +-->
  <xsl:template match="sdx:result" mode="result">
    <xsl:variable name="odd-even">
      <xsl:choose>
        <xsl:when test="(position() mod 2) = 0">
          <xsl:text>odd</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>even</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <tr class="{$odd-even}">
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'sdxdocid'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'preferredname'" />
      </xsl:apply-templates>
      <!--<xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'udate'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="build-cell">
        <xsl:with-param name="n" select="'uunitid'" />
      </xsl:apply-templates>-->
    </tr>
  </xsl:template>

  <!--+
      | Construction d'une cellule de résultat
      +-->
  <xsl:template match="sdx:result" mode="build-cell">
    <xsl:param name="n" select="''" />
    <xsl:choose>
      <xsl:when test="$n=''"/>
      <xsl:when test="$n='preferredname'">
        <xsl:variable name="href">
          <xsl:value-of select="concat( 'eac.html?id=', sdx:field[@name='root-id']/@escapedValue )" />
          <xsl:if test="not( sdx:field[@name='top' and @value='1'] )">
            <xsl:value-of select="concat( '&amp;c=', sdx:field[@name='sdxdocid']/@escapedValue )" />
          </xsl:if>
        </xsl:variable>
        <td>
          <xsl:variable name="docid" select="sdx:field[@name='sdxdocid']/@value" />
          <xsl:if test="sdx:field[@name='ctitle' and @value!='']">
            <a class="pl-ancestors" id="{$docid}l" onclick="$('{$docid}c').toggle();">[...]</a>
            <xsl:text> </xsl:text>
          </xsl:if>
          <a href="{$href}" onclick="return windowManager.winFocus(this.href, 'doceac');">
            <xsl:choose>
              <xsl:when test="sdx:field[@name=$n and .!='']">
                <xsl:apply-templates select="sdx:field[@name=$n]" />
              </xsl:when>
              <xsl:otherwise>
                <i18n:text key="basket.results.item.unititle.default">document sans titre</i18n:text>
              </xsl:otherwise>
            </xsl:choose>
          </a>
          <xsl:if test="sdx:field[@name='ctitle' and @value!='']">
            <xsl:text> </xsl:text>
            <div class="pl-ancestors" id="{$docid}c" onclick="$('{$docid}c').toggle();" style="display:none;">
              <xsl:for-each select="sdx:field[@name='ctitle']">
                <xsl:sort select="@value" order="ascending" />
                <xsl:value-of select="substring-after( @value, '-' )" />
                <xsl:if test="position()!=last()">
                  <i18n:text key="basket.results.item.context.sep">  &gt;  </i18n:text>
                </xsl:if>
              </xsl:for-each>
            </div>
          </xsl:if>
        </td>
      </xsl:when>
      <!-- Un cas particulier quand on veut la liste des ancêtres -->
      <xsl:when test="$n='ancestors'">
        <td>
          <xsl:choose>
            <xsl:when test="sdx:field[@name='ctitle' and @value!='']">
              <xsl:variable name="docid" select="sdx:field[@name='sdxdocid']/@value" />
              <div id="{$docid}l" onclick="$('{$docid}l').hide; $('{$docid}c').show;" style="display:block;">[...]</div>
              <div id="{$docid}c" onclick="$('{$docid}c').hide; $('{$docid}l').show;" style="display:none;">
                <xsl:for-each select="sdx:field[@name='ctitle']">
                  <xsl:sort select="@value" order="ascending" />
                  <xsl:value-of select="substring-after( @value, '-' )" />
                  <xsl:if test="position()!=last()">
                    <i18n:text key="basket.results.item.context.sep">  &gt;  </i18n:text>
                  </xsl:if>
                </xsl:for-each>
              </div>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </xsl:when>
      <xsl:when test="$n='sdxdocid'">
        <td>
          <input type="checkbox" name="id" value="{sdx:field[@name=$n]/@escapedValue}" />
        </td>
      </xsl:when>
      <!-- Cas générique, on prend la valeur du sdx:field demandé -->
      <xsl:otherwise>
        <td>
          <xsl:choose>
            <xsl:when test="sdx:field[@name=$n and .!='']">
              <xsl:apply-templates select="sdx:field[@name=$n]" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--+
      | Fonction pour les bases présentes dans le panier
      +-->
  <xsl:template match="sdx:caddy" mode="nb-bases">
    <xsl:for-each-group group-by="@base" select="/sdx:document/sdx:caddy/sdx:document">
      <xsl:text>|</xsl:text>
    </xsl:for-each-group>
  </xsl:template>

  <!-- Affichage de la pagination -->
  <xsl:template name="pagination">
    <div class="results">
      <p>
        <!-- La clé i18n selon singulier / pluriel -->
        <xsl:variable name="key-suffix">
          <xsl:choose>
            <xsl:when test="/sdx:document/sdx:results/@nb = 1">one</xsl:when>
            <xsl:otherwise>many</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <span class="nbresults">
          <xsl:value-of select="/sdx:document/sdx:results/@nb"/>
          <xsl:text> </xsl:text>
          <i18n:text key="docsearch.results.{$key-suffix}"/>
        </span>
        <span class="nbpages"><xsl:text>Page </xsl:text><strong><xsl:value-of select="/sdx:document/sdx:results/@page"/></strong><xsl:text> de </xsl:text><xsl:value-of select="/sdx:document/sdx:results/@pages"/></span>
        <span id="basket_pagination" class="navpage">
          <xsl:comment>u</xsl:comment><!-- Pour éviter un span vide que IE n'apprécie pas -->
          <xsl:apply-templates select="/sdx:document/sdx:results" mode="hpp">
            <xsl:with-param name="url" select="''" />
          </xsl:apply-templates>
        </span>
      </p>
    </div>
  </xsl:template>

</xsl:stylesheet>
