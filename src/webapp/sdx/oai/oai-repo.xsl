<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2000-2011  Ministere de la culture et de la communication (France), AJLSM
See LICENCE file
-->
<!--+
    | Formatage de la réponse de l'entrepôt OAI
    +-->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
xmlns:qdc="http://pleade.org/oai/qdc"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:oai2="http://www.openarchives.org/OAI/2.0/"
xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
xmlns:sdxoai="http://www.culture.gouv.fr/ns/oai/"
xmlns:xsp="http://apache.org/xsp"
xmlns:ap="http://patrimoines.aquitaine.fr/ap/format-pivot/1.1"
exclude-result-prefixes="sdx sdxoai xsp">

  <xsl:variable name="nss">http://www.openarchives.org/OAI/2.0/oai_dc/,http://purl.org/dc/elements/1.1/,http://pleade.org/oai/qdc,http://purl.org/dc/terms/,http://patrimoines.aquitaine.fr/ap/format-pivot/1.1,http://www.openarchives.org/OAI/2.0/,http://www.w3.org/2001/XMLSchema-instance</xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="oai2:OAI-PMH">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="oai2:responseDate" />
      <xsl:apply-templates select="oai2:request" />
      <xsl:apply-templates select="node()[not(self::oai2:responseDate) and not(self::oai2:request)]" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="oai2:request/@verb">
    <xsl:if test=".!='IllegalVerb'">
      <xsl:copy><xsl:value-of select="."/></xsl:copy>
    </xsl:if>
  </xsl:template>

  <xsl:template match="oai2:record[not(oai2:metadata/*) and not(oai2:header/@status='deleted')]">
    <xsl:element name="error" namespace="http://www.openarchives.org/OAI/2.0/">An error occured with the document <xsl:value-of select="oai2:header/oai2:identifier" /></xsl:element>
  </xsl:template>

  <xsl:template match="*[local-name()='dummyRoot']" priority="+2">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="*">
    <xsl:choose>
      <xsl:when test="namespace-uri-from-QName(node-name(.)) = tokenize($nss, ',')">
        <!--<xsl:message>On a un namespace connu pour <xsl:value-of select="name()"/></xsl:message>-->
        <xsl:copy>
          <xsl:apply-templates select="@* | node()" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
          <xsl:message>Namespace inconnu pour <xsl:value-of select="concat('{', namespace-uri-from-QName(node-name(.)), '}', name())"/></xsl:message>
        <xsl:element name="{local-name()}">
          <xsl:apply-templates select="@* | node()" />
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@xml:*">
    <xsl:copy-of select="." copy-namespaces="no"/>
  </xsl:template>

  <xsl:template match="@*">
    <xsl:choose>
      <xsl:when test="namespace-uri-from-QName(node-name(.)) = tokenize($nss, ',')">
        <!--<xsl:message>On a un namespace connu pour <xsl:value-of select="name()"/></xsl:message>-->
        <xsl:attribute name="{name()}">
          <xsl:value-of select="." />
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>Namespace inconnu pour <xsl:value-of select="concat('{', namespace-uri-from-QName(node-name(.)), '}', name())"/></xsl:message>
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="." />
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="text()|processing-instruction()">
    <xsl:copy/>
  </xsl:template>

</xsl:stylesheet>
