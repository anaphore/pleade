<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:dir="http://apache.org/cocoon/directory/2.1">

  <xsl:template match="/">
   <html>
   <head>
    <title><xsl:value-of select="dir:directory/@name"/></title>
    </head>
    <body style="background:#ffffff">
     <h1>Directory Listing of <xsl:value-of select="dir:directory/@name"/></h1>
     <table border="0">
      <tr>
       <td>
        <a href="../"><em style="font-style:italic">parent directory</em></a>
       </td>
      </tr>
      <tr>
       <td>
        &#160;
       </td>
      </tr>
      <xsl:apply-templates/>
     </table>
    </body>
   </html>
  </xsl:template>

  <xsl:template match="dir:directory/dir:directory">
   <tr>
    <td>
     <a href="{@name}/"><em style="font-style:italic"><xsl:value-of select="@name"/></em></a>
    </td>
    <td>
     <xsl:value-of select="@date"/>
    </td>
   </tr>
  </xsl:template>

  <xsl:template match="dir:file">
   <tr>
    <td>
     <a href="{@name}"><xsl:value-of select="@name"/></a>
    </td>
    <td>
     <xsl:value-of select="@date"/>
    </td>
   </tr>
  </xsl:template>

</xsl:stylesheet>
