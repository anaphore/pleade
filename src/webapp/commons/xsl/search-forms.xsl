<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--Common xsl-->
<!-- [JC] FIXME: Ça semble être du pleade2... Cette XSL est-elle utilisée ? --> 

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:c-pleade="http://pleade.org/ns/c-pleade/1.0"
	xmlns:local-conf="http://pleade.org/ns/local-conf/1.0"
	xmlns:saxon="http://icl.com/saxon"
	exclude-result-prefixes="sdx c-pleade local-conf saxon">


	<!-- Common search form -->
	<xsl:template name="common-search-form">
		<div  class="common-search-form">
			<xsl:variable name="form" select="$g-local-config/local-conf:default-form/@name"/>
			<xsl:text>&#160;</xsl:text>
			<table width="100%" border="0" cellpadding="2" cellspacing="0">
				<tr>
					<!--simple search link-->
					<xsl:call-template name="display-a-simple-search"/>
					<xsl:call-template name="display-common-advanced-list"/>
				</tr>
			</table>
		</div>
	</xsl:template>

	<!--the simple search-->
	<xsl:template name="display-a-simple-search">
		<form action="{$g-url-index}/search-s.xsp" method="get">
			<td>
				<xsl:value-of select="$g-ui-strings/simple-search[lang($g-lang)]"/>
			</td>
			<td>
				<input name="q" type="text" value="{ancestor::sdx:document//sdx:results/sdx:query/@text}" size="20" tabindex="1" />
				<input name="base" type="hidden" value="ead" />
			</td>
			<td>&#160;</td>
			<td>
				<input name="base" type="hidden" value="ead" />
				<input type="submit" value="{$g-ui-strings/common-search-form/submit}"/>
			</td>
		</form>
	</xsl:template>

</xsl:stylesheet>
