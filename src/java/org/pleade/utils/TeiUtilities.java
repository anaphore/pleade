/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils;


/**
 * Utility methods and information for TEI documents.
 */
public class TeiUtilities {

    /**
     * Checks if an element is a component element.
     *
     * Component elements are:
     *
     * 	- text
		 *	- front
		 *	- back
     * 	- div
     * 	- div1, div2, ..., div7
     *
     * @params	name	The name of the element.
     */
    public static boolean elementIsComponent(String name) {
        if ( name == null ) return false;
        if ( name.equalsIgnoreCase("tei") ) return true;
        if ( name.equalsIgnoreCase("tei.2") ) return true;
        if ( name.equals("text") ) return true;
        if ( name.equals("group") ) return true;
        if ( name.equals("front") ) return true;
        if ( name.equals("body") ) return true;
        if ( name.equals("back") ) return true;
        if ( name.equals("div") ) return true;
        if ( name.startsWith("div") ) return true;
        return false;
    }

    /**
     * Returns true if the element is a forced pleade:break element.
     * Those elements are:
     * <ul>
     * <li>TEI</li>
     * <li>TEI.2</li>
     * <li>teiCorpus</li>
     * <li>teiHeader</li>
     * </ul>
     *
     * @param	localName		The local name of the element
     *
     * @return	boolean
     */
    public static boolean isForcedPleadeBreak(String localName){
    	if (localName==null || "".equals(localName)) return false;
    	else if ( "tei".equalsIgnoreCase(localName)
    					|| "tei.2".equalsIgnoreCase(localName)
    					|| "teiCorpus".equalsIgnoreCase(localName)
    					|| "teiHeader".equalsIgnoreCase(localName) ) return true;
    	else return false;
    }
}
