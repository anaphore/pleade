/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.acting;

import java.io.File;
import java.util.Map;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.acting.ServiceableAction;
import org.apache.cocoon.environment.Redirector;
import org.apache.cocoon.environment.SourceResolver;

public class RulesPathAction extends ServiceableAction implements Configurable {

    // Des variables statiques pour identifier les parametres (d'action ou de configuration)
    private static final String CONFIGURATION_REGLES_DEFAULT = "url-regles-defaut";
    private static final String CONFIGURATION_NOM_FICHIER_REGLES = "nom-fichier-regles";
    private static final String PARAMETRE_DOSSIER = "dossier";
    private static final String PARAMETRE_SORTIE_URL_REGLES = "url-regles";

    /** Le nom de fichier de regles */
    private String nomFichierRegles = "regles-transformation-images";

    /** L'URL par defaut des regles */
    private String urlReglesDefaut = "context://navimages2/visionneuse/navimages1/regles-transformation-images.xml";

    /**
     * Configuration de l'action, on conserve les parametres
     */
    public void configure(Configuration conf) throws ConfigurationException {
        // Le nom du fichier de regles a trouver
        Configuration tempConf = conf.getChild(CONFIGURATION_NOM_FICHIER_REGLES);
        if ( tempConf != null ) nomFichierRegles = tempConf.getValue();

        // L'URL par defaut des regles, si on n'en trouve pas
        tempConf = conf.getChild(CONFIGURATION_REGLES_DEFAULT);
        if ( tempConf != null ) urlReglesDefaut = tempConf.getValue();

    }

    /**
     * L'action elle-meme, le parametre "url-regles" sera retourne au sitemap systematiquement
     */
    public Map act(Redirector redirector, SourceResolver resolver, Map objectModel, String src, Parameters parameters) throws Exception {

        // On verifie le chemin demande, s'il n'est pas fourni ou vide, alors on retourne une "erreur"
        String dossier = parameters.getParameter(PARAMETRE_DOSSIER);
        if (getLogger().isDebugEnabled()) getLogger().debug("Dossier d'images ou trouver des regles : " + dossier.toString());
        if (dossier == null || dossier.trim().equals("")) return null;

        // On essaie de trouver le fichier
        File fichierRegles = trouveRegles(new File(dossier));

        if (fichierRegles == null ) {
            // On utilise la valeur par defaut
            objectModel.put(PARAMETRE_SORTIE_URL_REGLES, urlReglesDefaut);
        }
        else {
            // On retourne l'URL du fichier trouve
            objectModel.put(PARAMETRE_SORTIE_URL_REGLES, fichierRegles.toURL().toExternalForm());
        }

        // Si on est ici, cela signifie que le dossier n'a pas ete trouve, donc on retourne une "erreur"
        return objectModel;
    }

    /**
     * Trouve un fichier de regles, de maniere recursive
     * @param dossier   Le nom du dossier ou trouver un fichier de regles
     * @return          L'objet File du fichier de regles, ou null s'il n'y en a pas
     */
    private File trouveRegles(File dossier) {
        // Si le dossier n'est pas correct, on arrete la
        if ( dossier == null || !dossier.isDirectory() || !dossier.canRead() ) return null;

        // On construit l'objet fichier qui pourrait etre le bon
        File regles = new File(dossier, nomFichierRegles);

        // S'il existe, on le retourne
        if ( regles.exists() && regles.isFile() && regles.canRead() ) return regles;

        // Il n'existe pas, alors on poursuit avec le parent, si on n'est pas a la racine
        File parent = dossier.getParentFile();
        if ( parent == null ) return null;
        return trouveRegles(parent);
    }

}
