<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		xmlns:saxon="http://saxonf.sf.net"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		exclude-result-prefixes="xsl xs saxon">

	<!-- Fonctions et templates pour aider à la publication. -->

	<!-- Import des fonctions génériques -->
<!-- 	<xsl:import href="../../../../commons/xsl/functions.xsl"/> -->

	<xsl:param name="mode" select="'dyn'"/>

	<!-- L'URL où se trouvent les fichiers produits (utile en mode statique). -->
	<xsl:param name="data-output-dir" select="''" />
	
	<!-- Une fonction qui permet de savoir si un élément est public -->
	<xsl:function name="pleade:is-external" as="xs:boolean">
		<xsl:param name="element"/>
		<xsl:value-of select="boolean(not($element/@audience and $element/@audience = 'internal'))"/>
	</xsl:function>
	
	<!-- Une fonction pour savoir si l'élément a un ancêtre qui n'est pas external -->
	<xsl:function name="pleade:has-internal-ancestor" as="xs:boolean">
		<xsl:param name="element"/>
		<xsl:value-of select="boolean($element/ancestor::*[@audience = 'internal'][1])"/>
	</xsl:function>

	<!-- Une fonction pour retourner un jeu de propriétés -->
	<xsl:function name="pleade:get-properties" as="item()">
		<xsl:param name="doctype"/>
		<xsl:param name="docid"/>
		<xsl:param name="proptype"/>
		<xsl:param name="mode"/>
		<xsl:variable name="url">
		<xsl:choose>
			  <xsl:when test="$mode='dyn'">
				<xsl:value-of select="concat('cocoon://functions/',$doctype,'/get-properties/', $docid, '/', $proptype, '.xml')" />
			</xsl:when>
			  <xsl:otherwise>
				<xsl:value-of select="concat($data-output-dir, '/', $docid, '/', $proptype, '.xconf')" />
			</xsl:otherwise>
		</xsl:choose>
		</xsl:variable>
		<xsl:copy-of saxon:read-once="yes" select="if($url!='' and doc-available($url)) then document($url)/properties else pleade:get-default-properties($doctype,$proptype)" />
	</xsl:function>

	<!-- Une fonction pour retourner un jeu de propriétés par défaut (ie, pas celui du document en cours de publication) -->
	<xsl:function name="pleade:get-default-properties" as="item()">
		<xsl:param name="doctype"/>
		<xsl:param name="proptype"/>
		<xsl:variable name="url" select="concat('cocoon://functions/',$doctype,'/get-properties/', $proptype, '.xml')" />
		<xsl:copy-of saxon:read-once="yes" select="if($url!='' and doc-available($url)) then document($url)/properties else ()" />
	</xsl:function>

	<!-- Une fonction qui retourne la valeur d'une date normalisée -->
	<xsl:function name="pleade:get-normal-date" as="xs:string">
		<xsl:param name="el"/>
		<xsl:choose>
			<xsl:when test="$el/@normal"><xsl:value-of select="normalize-space($el/@normal)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="normalize-space($el)"/></xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- Une fonction qui permet de savoir si un élément est un fragment -->
	<xsl:function name="pleade:is-fragment" as="xs:boolean">
		<xsl:param name="element"/>
		<xsl:value-of select="boolean($element/@pleade:break and $element/@pleade:break = 'true')"/>
	</xsl:function>


	<!-- Un pointeur sur le fichier de propriétés de publication -->
	<xsl:variable name="pub-properties" select="pleade:get-properties('ead', if(/ead) then /ead/@pleade:id else /sdx:document/@id , 'publication', $mode)"/>

</xsl:stylesheet>
