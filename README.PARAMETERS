Ce fichier permet de suivre ce qui a été fait lors de la réorganisation de tous les paramètres dans Pleade en trois fichiers : pleade.properties, pleade-admin.properties, parametres.xconf
Pour chaque fichier de paramétrage initial, les paramètres ont été listés ; ils sont regroupés par bloc, pour chaque bloc on indique dans quel fichier se trouve maintenant cet ensemble de paramètres. Si rien n'est indiqué, c'est que les paramètres n'ont pas changé de fichier.

Dans pleade.properties
=======================

pleade.version=3.1-dev-20080516
pleade.app.version=1.0
pleade.application-id=org.pleade3.demo
pleade.eadeac.list-sep=\u0020\u2022\u0020

pleade.eadeac.active-basket=true
pleade.eadeac.active-pdf=true
pleade.eadeac.images-in-pdf=unit-only
pleade.img.thumbnail.size=150
pleade.eadeac.active-eac=true
=> maintenant dans parametres.xconf

pleade.eadeac.fs-pdf=/projets/mypdf
=> maintenant dans pleade-admin.properties

pleade.eadeac.suggest-hpp=10
pleade.eadeac.cdc-result-sort-key=fatitle
pleade.eadeac.search-help-mode=panels
pleade.eadeac.navindex-from-input=false
=> maintenant dans parametres.xconf

pleade.img-server.imagemagick-exe=/usr/bin/convert
pleade.img-server.racines=ACHANGER
pleade.images.preparees=ACHANGER
pleade.images.filigrane=context://theme/images/filigrane.png
=> maintenant dans pleade-admin.properties

pleade.check-reload=true
pleade.cache-at-startup=false
pleade.internal-only=false


Dans parametres.xconf
======================
<default-lang>fr</default-lang>

<url-interne>http://localhost:8090/</url-interne>
<public-url>http://localhost:8090</public-url>
<static-content>context://module-eadeac/pages</static-content>
<sdx-data-dir>context://module-eadeac/datadir</sdx-data-dir>
<ead-docs-dir>context://exemples/ead</ead-docs-dir>
=> maintenant dans pleade-admin.properties

<auth-file>true</auth-file>
<auth-sql>false</auth-sql>
<auth-ldap>false</auth-ldap>
<auth-arkheia>false</auth-arkheia>
<path-sdxapp>pleade</path-sdxapp>
=> maintenant dans pleade.properties

<cdc-src>context://module-eadeac/conf/cdc/cdc.xml</cdc-src>
=> maintenant dans pleade-admin.properties

<sdx-database></sdx-database>
<sdx-ead-repository></sdx-ead-repository>
<sdx-eac-repository></sdx-eac-repository>
=> maintenant dans pleade-admin.properties

<ead-oai-repo-name></ead-oai-repo-name>
<ead-oai-repo-admin-email></ead-oai-repo-admin-email>
<ead-oai-repo-nb>100</ead-oai-repo-nb>
<ead-oai-repo-records>components</ead-oai-repo-records>	<!-- Peut-être "finding aids" ou "components" -->

<thesx-url>cocoon://thesx/</thesx-url>
=> maintenant dans pleade.properties

<ead-docs-not-searchable></ead-docs-not-searchable>
<ead-cdc-results-auto-expand>_all</ead-cdc-results-auto-expand>
<ead-archdesc-toc>true</ead-archdesc-toc>

<log-file-directory>context://WEB-INF/logs</log-file-directory>
=> maintenant dans pleade-admin.properties

<ead-max-results>1000</ead-max-results>
=> maintenant dans parametres.xconf

<zones>zone-ip-local:127\.0\.0\.1</zones>
=> maintenant dans pleade-admin.properties

<ead-thumbnails-subdir>vignette</ead-thumbnails-subdir>
<droits>Pleade 3 - ACHANGER</droits>
<news-days>20</news-days>
<news-maxratio>60</news-maxratio>
=> maintenant dans parametres.xconf


Dans module-img-server-parameters.xconf
=======================================
<url-images-interne>http://localhost:8090/img-server</url-images-interne>
<url-images-public>http://localhost:8090</url-images-public>
=> maintenant dans pleade-admin.xconf (seule la partie img-server pourrait éventuellement être changée)

<nombre-maximal-images>10</nombre-maximal-images>
=> maintenant dans parametres.xconf


Dans module-img-viewer.xmap
=============================
<module-img-server-url>cocoon://img-server</module-img-server-url>
=> maintenant dans pleade.properties


Dans module-img-viewer-parameters.xconf
========================================
N-B : Tous ces paramètres ne sont pas forcément utilisés.
<!-- Fonctionnalités générales -->
<viewer-show-title>true</viewer-show-title>
<viewer-pdf-download>false</viewer-pdf-download>
<viewer-zip-download>false</viewer-zip-download>
<viewer-show-navigation>true</viewer-show-navigation>
<viewer-show-status>true</viewer-show-status>
<!-- Boutons -->
<viewer-zoom-enabled>true</viewer-zoom-enabled>
<viewer-lock-enabled>true</viewer-lock-enabled>
<viewer-resize-enabled>true</viewer-resize-enabled>
<viewer-rotation-enabled>false</viewer-rotation-enabled>
<viewer-contrast-enabled>false</viewer-contrast-enabled>
<viewer-luminosity-enabled>false</viewer-luminosity-enabled>
<viewer-preview-enabled>true</viewer-preview-enabled>
<viewer-preview-onload>false</viewer-preview-onload>
<viewer-print-enabled>true</viewer-print-enabled>
<!-- Fonctionnalités supplémentaires (peuvent ne pas etre implémentées encore) -->
<viewer-carousel-enabled>false</viewer-carousel-enabled>
<viewer-basket-enabled>false</viewer-basket-enabled>module-img-server-url
<viewer-mosaic-enabled>false</viewer-mosaic-enabled>


Dans pleade.xmap : paramètre du reader ImageMagick
===================================================
<cache-images-transformees>false</cache-images-transformees>
<formats-sortie>image/jpeg;image/png</formats-sortie>
=> maintenant dans pleade.properties

<nom-fichier-pas-filigrane>nowatermark</nom-fichier-pas-filigrane>
<taille-mini-filigrane>150x150</taille-mini-filigrane>
=> maintenant dans pleade-admin.properties

<!-- Timeout lors de la transformation d'images (en millisecondes). Par défaut, c'est 10 minutes -->
<timeout-image-reader>600000</timeout-image-reader>
=> maintenant dans pleade.properties
