<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Driver for common XSLTs : only includes the others -->

<xsl:stylesheet version="1.0"
								xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
								xmlns:pleade="http://pleade.org/ns/pleade/1.0"
								xmlns:local-conf="http://pleade.org/ns/local-conf/1.0"
								exclude-result-prefixes="xsl sdx pleade local-conf">

	<!-- global variables -->
	<xsl:include href="global-variables.xsl"/>

	<!-- navigation management -->
	<xsl:include href="menubars.xsl"/>

	<!-- html page management -->
	<xsl:include href="page.xsl"/>

	<!-- XHTML content -->
	<xsl:include href="xhtml.xsl"/>

	<!-- SDX content -->
	<xsl:include href="sdx.xsl"/>

	<!-- Search forms -->
	<xsl:include href="search-forms.xsl"/>

</xsl:stylesheet>
