<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Formatage JSON du résultat de l'ajout d'un document dans le panier
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                              exclude-result-prefixes="xsl sdx">

  <xsl:output method="text" encoding="UTF-8"/>

  <xsl:template match="/">
    <root>
      <xsl:apply-templates />
    </root>
  </xsl:template>

  <xsl:template match="/sdx:document">
    <xsl:variable name="content">
      <xsl:apply-templates select="success|error|info/@*" />
    </xsl:variable>
    <xsl:text>{</xsl:text><xsl:value-of select="normalize-space($content)" /><xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="success">
    <xsl:text>'success' : true, </xsl:text><xsl:apply-templates select="@*" />
  </xsl:template>

  <xsl:template match="error">
    <xsl:text>'success' : false, 'erreur' : '</xsl:text><xsl:value-of select="@msgCode"/><xsl:text>'</xsl:text><xsl:if test="@*"><xsl:text>, </xsl:text></xsl:if><xsl:apply-templates select="@*" />
  </xsl:template>

  <xsl:template match="success/@* | error/@* | info/@*">
    <xsl:if test="position()!=1"><xsl:text>, </xsl:text></xsl:if><xsl:text>'</xsl:text><xsl:value-of select="name()" /><xsl:text>' : '</xsl:text><xsl:value-of select="." /><xsl:text>'</xsl:text>
  </xsl:template>

</xsl:stylesheet>