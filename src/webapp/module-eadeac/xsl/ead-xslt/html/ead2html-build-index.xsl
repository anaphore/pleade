<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		exclude-result-prefixes="xsl i18n">

	<!-- Cette XSLT permet de créer un fichier d'index en y ajoutant notamment
			le nom des champs SDX correspondant. Elle travaille sur un aggregate /root
			qui contient le fichier d'index ainsi que la configuration locale des index. -->

	<!-- La liste des index en configuration -->
	<xsl:variable name="conf" select="/root/index-list"/>
  <!-- La liste des index à afficher stockée dans les propriétés du document courant-->
  <xsl:variable name="index-local-doc" select="/root/display-doc/properties[@id='display']/property[@name='index-local']" />
  <!-- La même chose mais général -->
  <xsl:variable name="index-local-gen" select="/root/display-gen/properties[@id='display']/property[@name='index-local']/@default" />

	<!-- Pour l'élément racine, on passe la main. -->
	<xsl:template match="/root">
		<xsl:apply-templates select="indices | index"/>
	</xsl:template>

	<!-- La liste des index définis pour un document -->
	<xsl:template match="indices">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<!-- On doit les sortir dans l'ordre défini par le @display-order -->
			<xsl:apply-templates select="index">
				<xsl:sort data-type="number">
					<xsl:variable name="id" select="@index-id"/>
					<xsl:variable name="order" select="$conf/index[@id = $id]/@display-order"/>
					<xsl:choose>
						<xsl:when test="number($order)"><xsl:value-of select="number($order)"/></xsl:when>
						<xsl:otherwise>999999</xsl:otherwise>
					</xsl:choose>
				</xsl:sort>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

	<!-- Un index -->
	<xsl:template match="index">
		<xsl:variable name="id" select="@index-id"/>
		<!-- Conditions pour afficher un index :
						1- La propriété "index-local" du document n'est pas nulle
							a- Elle contient '_all' ou l'identifiant de l'index courant : on affiche
							b- On n'affiche pas
						2- La propriété "index-local" générale n'est pas nulle
							a- Elle contient '_all' ou l'identifiant de l'index courant : on affiche
							b- On n'affiche pas
						3- Les deux propriétés sont nulles : on affiche
		 (MP) -->
			<xsl:if test="
						( not($index-local-doc!='') and not($index-local-gen='') )
						or
						( not($index-local-doc!='') and ($index-local-gen='_all' or tokenize($index-local-gen, ' ') = $id )
						or
						( $index-local-doc='_all' or tokenize($index-local-doc, ' ') = $id) )
			">
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
				<xsl:apply-templates select="$conf/index[@id = $id]/@*"/>
				<xsl:apply-templates select="node()"/>
			</xsl:copy>
    </xsl:if>
	</xsl:template>

	<!-- Un template de copie -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- On ne sort pas la configuration -->
	<xsl:template match="index-list"/>

</xsl:stylesheet>
