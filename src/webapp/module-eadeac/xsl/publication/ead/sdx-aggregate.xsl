<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="fn xs xsl saxon"
>

	<!-- XSLT qui permet de créer des champs par aggrégation. En entrée, on a un
		sdx:document qui contient des sdx:field, des contenus, et d'autres sdx:document.
		Tous ces éléments sont copiés, mais en plus on ajoute des nouveaux champs
		par aggrégation de champs existants. -->
	
	<xsl:param name="debug" select="'false'"/>
		
	<!-- Un pointeur sur le fichier de déclaration des champs agrégés -->
	<xsl:variable name="aggregated-fields" select="document('cocoon://conf/ead-aggregated-fields.xconf')/*"/>

	<xsl:template match="/">
		<xsl:message>	Traitement des champs aggreges (etape 12 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{sdx:document/@id}/{sdx:document/@id}-12.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- On intercepte un sdx:document pour ajouter les champs aggrégés -->
	<xsl:template match="sdx:document">
		<xsl:copy>
			<!-- On copie les attributs -->
			<xsl:apply-templates select="@*"/>
			<!-- Un pointeur sur le document courant -->
			<xsl:variable name="doc" select="."/>
			<!-- On ajoute les champs agrégés -->
			<xsl:for-each select="$aggregated-fields/aggregated-field">
				<!-- Le nom du champ -->
				<xsl:variable name="name" select="@name"/>
				<!-- Les champs dans la définition -->
				<xsl:variable name="def-fields" select="field"/>
				<!-- Les champs dans le document qui sont dans la définition -->
				<xsl:variable name="document-fields" select="$doc/sdx:field[@name = $def-fields/@name]"/>
				<!-- <xsl:message><xsl:value-of select="count($def-fields)"/> champs definis ; <xsl:value-of select="count($document-fields)"/> champs pertinents</xsl:message> -->
				<!-- On le fait uniquement si on a des champs qui en font partie -->
				<xsl:if test="$document-fields">
					<xsl:choose>
						<xsl:when test="@type = 'word'">
							<!-- Si c'est un champ word, alors on copie le texte dans un seul sdx:field -->
							<sdx:field name="{$name}">
								<xsl:for-each select="$document-fields">
									<xsl:text> </xsl:text>
									<xsl:value-of select="."/>
								</xsl:for-each>
							</sdx:field>
						</xsl:when>
						<xsl:otherwise>
							<!-- Pour les autres champs on crée autant d'occurrences -->
							<xsl:for-each select="$document-fields">
								<sdx:field name="{$name}"><xsl:value-of select="."/></sdx:field>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:for-each>
			<!-- Et on copie le reste du contenu -->
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- De manière générale, on copie les éléments -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
