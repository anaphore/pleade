/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation.utils;

import org.apache.cocoon.xml.XMLConsumer;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Handler2TransformationBridge extends DefaultHandler {
    private XMLConsumer consumer;
    public Handler2TransformationBridge(XMLConsumer cons) {
        this.consumer = cons;
    }
    public void characters(char[] arg0, int arg1, int arg2) throws SAXException {
        consumer.characters(arg0, arg1, arg2);
    }
    public void endDocument() throws SAXException {
        consumer.endDocument();
    }
    public void endElement(String arg0, String arg1, String arg2) throws SAXException {
        consumer.endElement(arg0, arg1, arg2);
    }
    public void endPrefixMapping(String arg0) throws SAXException {
        consumer.endPrefixMapping(arg0);
    }
    public void ignorableWhitespace(char[] arg0, int arg1, int arg2) throws SAXException {
        consumer.ignorableWhitespace(arg0, arg1, arg2);
    }
    public void processingInstruction(String arg0, String arg1) throws SAXException {
        consumer.processingInstruction(arg0, arg1);
    }
    public void setDocumentLocator(Locator arg0) {
        consumer.setDocumentLocator(arg0);
    }
    public void skippedEntity(String arg0) throws SAXException {
        consumer.skippedEntity(arg0);
    }
    public void startDocument() throws SAXException {
        consumer.startDocument();
    }
    public void startElement(String arg0, String arg1, String arg2, Attributes arg3) throws SAXException {
        consumer.startElement(arg0, arg1, arg2, arg3);
    }
    public void startPrefixMapping(String arg0, String arg1) throws SAXException {
        consumer.startPrefixMapping(arg0, arg1);
    }

}
