/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.transformation.index;

/**
		A grouper for a specific letter (first, second, ...).
 */
public class TargetLetterGrouper extends AbstractLetterGrouper {

    /**
     * Creates a grouper for a specific letter.
     * @param no	The letter to group on.
     */
    public TargetLetterGrouper(int no) {
        this.targetLetterNumber = no;
    }
    
    /**
     * Sets the target letter number.
     * @param no	The target letter
     */
    public void setLetterNumber(int no) {
        this.targetLetterNumber = no;
    }
    
    public String getTargetLetter(String value) {
        String val = setComparable(value);
        if ( val.length() < targetLetterNumber ) return "" + DEFAULT_CHARACTER;
        return normalizeChar(val.substring(targetLetterNumber - 1, targetLetterNumber).toUpperCase());
    }
    
}
