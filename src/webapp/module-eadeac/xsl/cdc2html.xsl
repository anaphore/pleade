<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: cdc2json.xsl 15999 2009-08-18 07:41:31Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Cette XSLT permet de transformer une partie d'un cadre de classement
		| en format JSON.
		|
		| On l'utilse dans les 3 contextes suivants :
		| #1 : sommaire des fonds (cdc)
		| #2 : résultats de recherche (results)
		| #3 : formulaire de recherche (searchform)
		|
		| Il est possible de cacher un noeud en utilisant l'attribut @altrender d'un
		| composant "c". Les valeurs traitées ici sont :
		| # 'hidden' : le composant est caché dans tous les cas (ie, toutes les
		|              valeurs de contexte décrites plus haut).
		| # 'hidden-in-{$context}' : le composant est caché dans le contexte précis
    +-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	exclude-result-prefixes="xsl sdx pleade xs fn i18n">


	<!-- Le contexte : cdc ou results ou searchform... -->
	<xsl:param name="context"/>

	<!-- Des indications par défaut pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand-default" select="''"/>

	<!-- Des indications dynamiques pour l'ouverture automatique des entrées -->
	<xsl:param name="auto-expand" select="''"/>

	<!-- Les paramètres suivant sont utiles pour le contexte de résultats -->
	<xsl:param name="qcdcall"/>
	<xsl:param name="qcdc"/>
	<xsl:param name="qrootid"/>
	<xsl:param name="oid"/>

	<xsl:param name="separator"><xsl:text disable-output-escaping="yes">&#160;&#8226;&#160;</xsl:text></xsl:param>

	<xsl:variable name="cdc-news-path" select="'cocoon://search-new-docs.xml'"/>
	<xsl:variable name="cdc-news">
		<xsl:if test="doc-available($cdc-news-path)">
			<xsl:copy-of select="document($cdc-news-path)/sdx:document/news"/>
		</xsl:if>
	</xsl:variable>
	
	<xsl:variable name="echap"><xsl:text>'</xsl:text></xsl:variable>

	<!-- S'il y a eu une erreur -->
	<xsl:template match="/error">
		<xsl:copy-of select="."/>
	</xsl:template>

	<!-- Match les "c" -->
	<xsl:template match="c">
		<xsl:choose>
			<xsl:when test="position()=1"><xsl:apply-templates select="." mode="first" /></xsl:when>
			<xsl:otherwise><xsl:apply-templates select="." mode="not-first" /></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement du premier "c" -->
	<xsl:template match="c" mode="first">
		<div id="treeRoot">	<!-- Pour forcer le résult JSON à être une array, même s'il y a un seul élément -->
			<ul>
			<xsl:apply-templates select="c" mode="not-first"/>
			</ul>
		</div>
	</xsl:template>

	<!-- Une entrée, qui peut être un document ou une partie du cadre de classement -->
	<xsl:template match="c" mode="not-first">
		<xsl:variable name="do"><xsl:apply-templates select="." mode="is-do" /></xsl:variable><!-- Marqueur indiquant si l'entrée courante sera traitée -->
		<xsl:if test="not($do castable as xs:boolean) or xs:boolean($do)">
			<xsl:variable name="_href">
				<xsl:apply-templates select="." mode="att-href" />
			</xsl:variable>
			<li id="{@pleade:id}">
				<!--une cellule pour le titre-->
				<xsl:variable name="tdId"><xsl:value-of select="concat('cdc-span-',@pleade:id)"/></xsl:variable>
				<xsl:apply-templates select="." mode="atts-others" /><!-- D'autres attributs éventuellement -->
        <xsl:if test="@illustratedChild eq 'true'">
        <span class="anc_illustrated">&#160;<xsl:comment>u</xsl:comment></span>
        </xsl:if>
				<span class="cdc-title" id="{$tdId}">
					<xsl:choose>
						<xsl:when test="(@pleade:type eq 'document') and ($context eq 'cdc' )">
							<xsl:apply-templates select="." mode="write-content">
								<xsl:with-param name="isTitleLink" select="'true'"/>
							</xsl:apply-templates>
						</xsl:when>
						<xsl:when test="($context eq 'searchform' )">
							<xsl:apply-templates select="." mode="write-content"/>
						</xsl:when>
					
						<xsl:when test="($context eq 'results' )">
							<xsl:apply-templates select="." mode="write-content"/>
							<xsl:choose>
							<xsl:when test="(@pleade:type eq 'document')">
								<span class='show-results'>
									<xsl:value-of select="' '"/><a href="{concat($_href,@pleade:id)}" onclick="return cdcResult.displayDocumentResults({concat($echap,@pleade:id,$echap,',',$echap,$tdId,$echap,',',$echap,$_href,$echap)});">
										<xsl:choose>
											<xsl:when test="(@nbresults eq 1)">
												<i18n:text key="cdc_showresults_text_one">voir le résultat</i18n:text>
											</xsl:when>
											<xsl:otherwise>
												<i18n:translate>
													<i18n:text key="cdc_showresults_text_many"/>
													<i18n:param><xsl:value-of select="@nbresults"/></i18n:param>
												</i18n:translate>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</span>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="' '"/><span class='cdc-nbresults'>
									<xsl:choose>
										<xsl:when test="(@nbresults eq 1)">
											<i18n:text key="cdc_showresults_one">(1 résultat)</i18n:text>
										</xsl:when>
										<xsl:otherwise>
											<i18n:translate>
												<i18n:text key="cdc_nbresults_text_many"/>
												<i18n:param><xsl:value-of select="@nbresults"/></i18n:param>
											</i18n:translate>
										</xsl:otherwise>
									</xsl:choose>
								</span>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="($context eq 'cdc' )">
							<xsl:apply-templates select="." mode="write-content"/>
						</xsl:when>
					</xsl:choose>
					<!--le cas d'un nouveau noeud (isNew = 1)-->
					<xsl:if test="$cdc-news/news/new[@sdxdocid=@pleade:id]">
						<span class="cdc-news">New...</span>
					</xsl:if>
				</span>
				<xsl:if test="(@hasChildren eq 'true')">
					<a style="visibility:hidden" href="{$_href}">_</a> <!-- Lien pour aller recuperer l'enfant en ajax -->
					<ul/>
				</xsl:if>
				<xsl:if test="($context eq 'cdc') and @moreinfo eq 'true'">
					<span class="cdc-moreinfo-span">
						<span class="moreinfo" id="{concat('cdc-span-',@pleade:id,'-moreinfo-span')}">
							<a title="cdc.moreinfo.title" i18n:attr="title" href="{concat($echap,'cdc-moreinfo?id=',$echap,@pleade:id,$echap)}" onclick="return cdcTreeView.displayMoreinfo({concat($echap,$tdId,$echap,',',$echap,@pleade:id,$echap)});">
								<i18n:text key="cdc.moreinfo.text">plus d'informations...</i18n:text>
							</a>
						</span>
					</span>
				</xsl:if>
			</li>
			<!--<xsl:apply-templates select="." mode="atts-inf-gen" />--><!-- Informations générales -->
			<!--<xsl:apply-templates select="." mode="att-isnew" />--><!-- Le document est-il nouveau ? -->
			<!--<xsl:apply-templates select="." mode="att-hidden" />--><!-- Les attributs hidden -->
			<!--<xsl:apply-templates select="." mode="att-class-css" />--><!-- Les classes CSS qu'on veut voir ajouter au noeud -->
			<!--<xsl:apply-templates select="." mode="atts-others" />--><!-- D'autres attributs éventuellement -->
		</xsl:if>
	</xsl:template>
		
	<xsl:template match="c" mode="write-content">
		<xsl:param name="isTitleLink" select="'false'"/> 
			<xsl:apply-templates select="." mode="write-unittitle">
				<xsl:with-param name="isLink" select="$isTitleLink"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="." mode="write-unitdate" />
			<xsl:apply-templates select="." mode="write-unitid" />
	</xsl:template>
	
	<xsl:template match="c" mode="write-unittitle">
		<xsl:param name="isLink" select="'false'"/>
		<span class="cdc-unittitle">
			<xsl:choose>
				<xsl:when test="$isLink = 'true'">
					<xsl:variable name="_href">
								<xsl:apply-templates select="." mode="att-href" />
					</xsl:variable>	
					<a title="cdc.document.title" i18n:attr="title" href="{$_href}" onclick="return windowManager.winFocus(this.href, 'docead');">
						<xsl:value-of select="did/unittitle"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="did/unittitle"/>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>
	
	<xsl:template match="c" mode="write-unitdate">
		<xsl:if test="did/unitdate[1]">
			<xsl:value-of select="$separator"/>
			<span class='cdc-unitdate'>
				<xsl:value-of select="did/unitdate[1]"/>
			</span>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="c" mode="write-unitid">
		<xsl:if test="did/unitid[1]">
			<xsl:value-of select="$separator"/>
			<span class='cdc-unitid'>
				<xsl:value-of select="did/unitid[1]"/>
			</span>
		</xsl:if>
	</xsl:template>
	
	<!-- Une fonction pour savoir si on traite le noeud courant.
			 # Si @altrender ne contient pas la chaîne 'hidden', on traite
			 # Si @altrender = 'hidden', on ne traite pas (ie, on cache dans tous les cas)
			 # Si la sous-chaîne de 'hidden-in-' trouvée dans @altrender égale le
			   marqueur du contexte (cf. variable XSL "context"), on ne traite pas
			 -->
	<xsl:template match="c" mode="is-do">
		<xsl:value-of select="
			if ( not(contains(@altrender, 'hidden')) ) then true()
			else if ( @altrender='hidden' ) then false()
			else if ( substring-after(@altrender,'hidden-in-') = $context ) then false()
			else true()
		" />
	</xsl:template>

	<!-- Informations générales -->
	<xsl:template match="c" mode="atts-inf-gen">
		<id><xsl:value-of select="@pleade:id"/></id>
		<type><xsl:value-of select="@pleade:type"/></type>
		<base><xsl:value-of select="@pleade:base"/></base>
		<hasChildren><xsl:value-of select="@hasChildren"/></hasChildren>
		<autoExpand><xsl:value-of select="@autoExpand"/></autoExpand>
		<nbDocs><xsl:value-of select="@nbdocs"/></nbDocs>
		<nbEntries><xsl:value-of select="@nbentries"/></nbEntries>
		<nbResults><xsl:value-of select="@nbresults"/></nbResults>
		<moreInfo><xsl:value-of select="@moreinfo"/></moreInfo>
		<context><xsl:value-of select="$context"/></context>
	</xsl:template>

	<!-- Le document est-il nouveau ? -->
	<xsl:template match="c" mode="att-isnew">
		<xsl:variable name="myid" select="@pleade:id"/>
		<xsl:if test="$cdc-news/news/new[@sdxdocid=$myid]">
			<isnew>1</isnew>
		</xsl:if>
	</xsl:template>

	<!-- Les informations pour cacher le noeud -->
	<xsl:template match="c" mode="att-hidden">
		<xsl:if test="starts-with(@altrender,'hidden-')">
			<hidden><xsl:value-of select="substring-after(@altrender,'hidden-')" /></hidden>
		</xsl:if>
	</xsl:template>

	<!-- Les classes CSS qu'on veut voir ajouter au noeud -->
	<xsl:template match="c" mode="att-class-css">
		<xsl:variable name="styles"><xsl:apply-templates select="." mode="styles-css" /></xsl:variable>
		<xsl:if test="$styles!=''">
			<styles><xsl:value-of select="$styles" /></styles>
		</xsl:if>
	</xsl:template>
	<xsl:template match="c" mode="styles-css">
		<xsl:if test="@altrender!=''"><xsl:value-of select="@altrender" /></xsl:if>
		<!--<xsl:if test="starts-with(@altrender,'hidden')"><xsl:value-of select="@altrender" /></xsl:if>-->
	</xsl:template>

	<!-- Pour surcharges -->
	<xsl:template match="c" mode="atts-others"/>

	<!-- L'attribut @href -->
	<xsl:template match="c" mode="att-href">
		<!-- L'adresse dépend du fait qu'il s'agit d'un document ou d'une entrée, mais aussi qu'on soit en contexte de résultat ou de navigation -->
				<xsl:choose>
					<xsl:when test="$context = 'cdc'">
						<xsl:choose>
							<xsl:when test="@pleade:type = 'document'">ead.html?id=<xsl:value-of select="@pleade:id"/></xsl:when>
							<xsl:otherwise>functions/ead/cdc.ajax-html?id=<xsl:value-of select="@pleade:id"/></xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$context = 'results'">
						<xsl:choose>
							<!-- <xsl:when test="@pleade:type = 'document'">docsearch.xsp?r=<xsl:value-of select="@pleade:id"/>&amp;bq=<xsl:value-of select="$oid"/></xsl:when> -->
							<xsl:when test="@pleade:type = 'document' and @pleade:base = 'ead'">docsearch.xsp?r=<xsl:value-of select="@pleade:id"/>&amp;bq=<xsl:value-of select="$oid"/></xsl:when>
							<xsl:when test="@pleade:type = 'document' and @pleade:base != 'ead'">module-<xsl:value-of select="@pleade:base"/>/docsearch.xsp?r=<xsl:value-of select="@pleade:id"/>&amp;bq=<xsl:value-of select="$oid"/></xsl:when>
							<xsl:otherwise>
								<!-- On va chercher la vraie valeur du paramètre: URL ou par défaut -->
								<xsl:variable name="ae">
									<xsl:choose>
										<xsl:when test="$auto-expand != ''"><xsl:value-of select="$auto-expand"/></xsl:when>
										<xsl:otherwise><xsl:value-of select="$auto-expand-default"/></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:value-of select="concat('functions/ead/cdc-results.json?id=', @pleade:id, '&amp;qcdcall=', $qcdcall, '&amp;qcdc=', $qcdc, '&amp;qrootid=', $qrootid, '&amp;oid=', $oid, '&amp;_ae=', $ae)"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$context = 'searchform'">functions/ead/cdc-searchform.ajax-html?id=<xsl:value-of select="@pleade:id"/></xsl:when>
				</xsl:choose>
	</xsl:template>

	<!-- Intitulé, cote, ... -->
	<xsl:template match="c" mode="content">
		<xsl:apply-templates select="did/unittitle[1]"/>
		<xsl:apply-templates select="did/unitid[1]"/>
		<xsl:apply-templates select="did/unitdate[1]"/>
		<xsl:apply-templates select="c"/>
	</xsl:template>

	<xsl:template match="unittitle|unitdate|unitid">
		<!--<xsl:element name="{local-name()}">-->
			<xsl:value-of select="."/>
		<!--</xsl:element>-->
	</xsl:template>

</xsl:stylesheet>
