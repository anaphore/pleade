//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Fichier contenant toutes les surcharges des fonctions native de mootools
 * @author Julien Huet
 */
/**
 * @class Surcharge permettant d'ajouter des fonctions de base a mootools
 * @author Julien Huet
 */
String.implement(/** @lends String.implement*/{
	
	/**
	* Savoir si une chaine de caractère est vide (les chaines de caractères vide comptent)
	* @returns {Boolean} retourne true si la chaine est vide
	*/
	empty: function(){
		return this.length === 0;
	},
	
	/**
	* Savoir si une chaine de caractère est vide (les chaines de caractères vide ne comptent pas)
	* @returns {Boolean} retourne true si la chaine est vide
	*/
	blank: function(){
		return this.trim().length === 0;
	},
	
	/**
	* Savoir si une chaine commence par le ou les caractères passé en paramètre
	* @param {String} str Chaine ou caractère qui devrait débuté la chaine
	* @returns {Boolean} retourne true si la chaine commence bien par le ou les caractères passé en paramètre.
	*/
	startsWith: function(str){
        	return (this.match("^"+str)==str);
	},
	
	/**
	* Savoir si une chaine fini par le ou les caractères passé en paramètre
	* @param {String} str Chaine ou caractère qui devrait finir la chaine
	* @returns {Boolean} retourne true si la chaine fini bien par le ou les caractères passé en paramètre.
	*/
	endsWith: function(str){
		return (this.match(str+"$")==str);
	}
});

/**
 * @class Surcharge permettant d'ajouter des fonctions de base a mootools
 * @author Julien Huet
 */
Element.implement({
	//implement show
	show: function(){
        	//this.setStyle('visibility','visible');
		this.setStyle('display','');
	},

	//implement hide   
	hide: function(){
		//this.setStyle('visibility','hidden');
		this.setStyle('display','none');  
	},

	toggle: function(fnOnComplete){
		if ( this.getStyle('display') == 'none' ) {
			this.show();
		} else {
			this.hide();
		}
	},

	scrollTo: function(eltToFind){
		if ( !eltToFind ) {
			return false;
		}
		if ( $type(eltToFind) == 'string' ) {
			eltToFind = $(eltToFind);
		}
		var offsetParentVar = eltToFind.offsetParent;
		var offsetValue = eltToFind.offsetTop;
		var count = 0;
		while ( (offsetParentVar!=null)&&(offsetParentVar.id != this.get('id')) ) {
			offsetValue += offsetParentVar.offsetTop;
			offsetParentVar = offsetParentVar.offsetParent;
			count++;
		}
		this.scrollTop = offsetValue;
	}
});

/**
 * @class Surcharge permettant d'ajouter des fonctions de base a mootools
 * @author Julien Huet
 */
// Log.logger = function(){
// 	if(window.console && console.log) {
// 		console.log(arguments[0][0]);
// 	} else {
// 		Log.logged.push(arguments);
// 	}
// };


