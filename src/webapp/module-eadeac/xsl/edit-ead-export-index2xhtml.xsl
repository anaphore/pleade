<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Gestion du contenu.
    | Exporter les index et les champs de type field
    +-->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
    xmlns:xsp="http://apache.org/xsp"
    xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xsl xsp sdx i18n xhtml pleade xs">

  <xsl:variable name="i18n-root" select="string('edit.export-index')" />
  <xsl:param name="i18n-cat" select="'module-eadeac-edit'" />

  <!-- Imports -->
  <xsl:include href="form2xhtml-commons.xsl"/>

  <!--+
      |
      +-->
  <xsl:template match="/">
    <html>
      <head>
        <title>
          <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.html.title">récupérer les index</i18n:text>
        </title>
        <xsl:comment>pleade</xsl:comment>
        <script type="text/javascript" src="i18n/module-eadeac-edit-js.js">//</script>
        <!--<script type="text/javascript" src="js/module-eadeac.js">//</script>-->
        <script type="text/javascript" src="js/module-eadeac-edit.js">//</script>
        <script type="text/javascript" src="js/pleade/common/util/HelpPanels.js">//</script>
        <script type="text/javascript" src="js/pleade/admin/eadeac/IndexAndFieldManager.js">//</script>
        <script type="text/javascript">
          var format = "xls"; // format d'export
          window.addEvent("load", init);
          var indexAndFieldManager = '';
          function init(){
            indexAndFieldManager = new IndexAndFieldManager();
          }
        </script>
      </head>
      <body>
        <div class="pl-title">
          <xsl:call-template name="build-help">
            <xsl:with-param name="v" select="'title'" />
            <xsl:with-param name="k" select="concat($i18n-root, '.title')" />
          </xsl:call-template>
          <h1>
            <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.pl-title">
              récupérer les index
            </i18n:text>
          </h1>
        </div>

        <div class="pl-form-intro pl-form-message">
          <xsl:comment>u</xsl:comment>
          <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.intro" />
        </div>

        <xsl:apply-templates select="root"/>
        <!-- <xsl:apply-templates select="root/index-list" /> -->

        <div class="pl-form-conclu pl-form-message">
          <xsl:comment>u</xsl:comment>
          <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.conclusion" />
        </div>
      </body>
    </html>
  </xsl:template>

  <!--+
      | Construction du contenu de la page : texte + formulaire
      +-->
  <xsl:template match="root">
    <div class="pl-form-container">
      <div class="pl-form-ead-admin">
        <xsl:call-template name="build-help">
          <xsl:with-param name="v" select="'indexes'" />
          <xsl:with-param name="k" select="concat($i18n-root, '.indexes-list')" />
        </xsl:call-template>
        <fieldset>
          <legend>
            <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.indexes-list.legend">
              liste des index récupérables
            </i18n:text>
          </legend>

          <select id="select-indexes" name="iid" tabindex="1" class="pl-form-slct">
            <xsl:choose>
              <xsl:when test="not(exists(index-list/index[@global-sdxfield!=''])) and not(exists(sdx:fieldList/sdx:field[@type='field']))">
                <option value="">
                  <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.no-index">
                    il n'existe pas d'index ou de champs récupérables pour le moment
                  </i18n:text>
                </option>
              </xsl:when>
              <xsl:otherwise>
                <!--<option value="">
                  <i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.select-one">
                    - - sélectionner un index - -
                  </i18n:text>
                </option>-->
                <xsl:apply-templates select="index-list"/>
                <xsl:apply-templates select="sdx:fieldList"/> 
              </xsl:otherwise>
            </xsl:choose>
          </select>
        </fieldset>

        <!-- Les boutons -->
        <div class="pl-form-submit">
          <button class="yui-button yui-push-button pl-form-button"
                  id="pl-form-button1" type="button" tabindex="2"
                  title="{$i18n-cat}:{$i18n-root}.get-index.title" i18n:attr="title">
            <xsl:choose>
              <xsl:when test="not(exists(index-list/index[@global-sdxfield!=''])) and not(exists(sdx:fieldList/sdx:field[@type='field']))">
                <xsl:attribute name="disabled">
                  <xsl:text>disabled</xsl:text>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <!-- FIXME: devrait plutôt être géré dans un fichier .js à part... -->
                <xsl:attribute name="onclick">
                  <xsl:text>javascript:var v=$('select-indexes').getSelected()[0].value; if(v!='') window.location.href='functions/export-index-'+v+'.'+format; else alert( _usersMessage.ead_admin_no_index_selected );</xsl:text>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <i18n:text key="{$i18n-root}.get-index.text" catalogue="{$i18n-cat}">
              <xsl:text>récupérer</xsl:text>
            </i18n:text>
          </button>
        </div>

      </div>
    </div>	
  </xsl:template>

  <!--+
      | Liste des index récupérables
      +-->
  <xsl:template match="index-list">
    <xsl:if test="exists(index[@global-sdxfield!=''])">
      <optgroup label="{$i18n-cat}:{$i18n-root}.index-list.optgroup" i18n:attr="label">
        <xsl:for-each select="index[@global-sdxfield!='']">
          <xsl:sort select="@id" />
          <xsl:variable name="v" select="normalize-space(@global-sdxfield)" />
          <xsl:variable name="i" select="normalize-space(@id)" />
          <option value="{$v}">
            <i18n:text key="ead.nav.index.{$i}" catalogue="module-ead-xslt">
              <xsl:value-of select="$i" />
            </i18n:text>
          </option>
        </xsl:for-each>
      </optgroup>
    </xsl:if>
  </xsl:template>

  <!--+
      | Liste des champs field récupérables
      +-->
  <xsl:template match="sdx:fieldList">
    <xsl:if test="exists(sdx:field[@type='field'])">
      <optgroup label="{$i18n-cat}:{$i18n-root}.field-list.optgroup" i18n:attr="label">
        <xsl:for-each select="sdx:field[@type='field']">
          <xsl:sort select="@id" />
          <xsl:variable name="i" select="normalize-space(@name)" />

          <!-- On ne propose d'exporter que certains champs -->
          <xsl:if test="pleade:is-field-exportable(@name)">
            <option value="{$i}">
              <!-- Les champs qui ne correspondent pas à de éléments EAD n'ont pas d'étiquettes utilisées pour l'affichage du doc EAD
                  donc il faut modifier selon le cas le catalogue et le début de la clé
              -->
              <xsl:variable name="cati18n" select="if ( pleade:field-name-for-label(@name, /root/sdx:fieldList) != $i) then 'module-ead-xslt' else $i18n-cat"/>
              <xsl:variable name="rooti18n" select="if ( pleade:field-name-for-label(@name, /root/sdx:fieldList) != $i) then 'ead' else concat($i18n-root, '.field')"/>
              <i18n:text key="{$rooti18n}.{pleade:field-name-for-label(@name, /root/sdx:fieldList)}" catalogue="{$cati18n}"> 
                <xsl:value-of select="$i" />
              </i18n:text> 
            </option>
          </xsl:if>

        </xsl:for-each>
      </optgroup>

    </xsl:if>
  </xsl:template>

  <!--+
      | Fonctions spécifiques à cette fonctionnalité
      +-->
  <!-- Une fonction pour dire si le champ est exportable -->
  <xsl:function name="pleade:is-field-exportable" as="xs:boolean">
    <xsl:param name="elname"/>
    <xsl:choose>
      <!-- Liste des champs non exportables (= champs de gestion, champs booléens, champs inintéressants) : top, cdcroot, cdcall, cdc, subsetall, pos, ancobject, object, att, type, searchable, unitid (gros index) -->
      <xsl:when test="$elname='top' or $elname='cdcroot' or $elname='cdcall' or $elname='cdc' or $elname='subsetall' or $elname='pos' or $elname='ancobject' 
                      or $elname='object' or $elname='att' or $elname='type' or $elname='searchable' or $elname='unitid'"><xsl:value-of select="false()"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="true()"/></xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!--+
      | Une fonction pour donner le nom du champ pour la clé i18n. Les clés sont généralement
      | constituées à partir du nom de l'élément EAD correspondant au champ de type mot
      +-->
  <xsl:function name="pleade:field-name-for-label" as="xs:string">
    <xsl:param name="elname"/>
    <xsl:param name="el-list"/>
    <xsl:choose>
      <xsl:when test="starts-with($elname, 'f')">
        <xsl:value-of select="if (exists($el-list/sdx:field[@name = substring($elname, 2) and @type = 'word'])) then substring($elname, 2) else $elname"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$elname"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>
