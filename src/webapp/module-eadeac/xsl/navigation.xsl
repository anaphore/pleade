<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
    Codes for navigation in pages - results
    -->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    exclude-result-prefixes="xsl sdx i18n">

	<!-- une variable pour identifier le div qui contient les résultats -->
	<!-- FIXME: pas terrible -->
	<!--<xsl:variable name="divid">
		<xsl:choose>
			<xsl:when test="$is-term">pl-pg-body-main-mcol</xsl:when>
			<xsl:otherwise>results-container-<xsl:value-of select="$eadid"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>-->

  <!--=======================================================================-->
  <!--                results and terms processing in hpp mode               -->
  <!--=======================================================================-->
  <xsl:template match="sdx:results | sdx:terms" mode="hpp">

  	<!--<xsl:param name="eadid" select="''"/>-->
    <xsl:param name="url" select="/sdx:document/@uri" />
    <xsl:param name="query" select="''" />
    <xsl:param name="mod" select="number(10)" />
    <xsl:param name="hpp" select="normalize-space(/sdx:document/sdx:parameters/sdx:parameter[@name = 'hpp']/@value)" />
    <xsl:param name="onclick" select="''" />
    <xsl:param name="qidParamName" select="'qid'" />


    <!-- url to send-->
    <!--<xsl:variable name="page">
      <xsl:value-of select="concat('docsearch', $term, '.xsp', '?qid=', @id, '&amp;r=', $eadid)" />
      <xsl:if test="$hpp!=''"><xsl:value-of select="concat('&amp;hpp=',$hpp)" /></xsl:if>
    </xsl:variable>-->
     <xsl:variable name="page">
      <xsl:value-of select="concat($url, '?', $qidParamName ,'=', @id, $query)" />
      <xsl:if test="$hpp!=''"><xsl:value-of select="concat('&amp;hpp=',$hpp)" /></xsl:if>
    </xsl:variable>

<!--<xsl:message>dans navigation.xsl avec
- url="<xsl:value-of select="$url" />"
- query="<xsl:value-of select="$query" />"
- hpp="<xsl:value-of select="$hpp" />"
- page="<xsl:value-of select="$page" />"
- mod="<xsl:value-of select="$mod"/>"
- onclick="<xsl:value-of select="$onclick"/>"
</xsl:message>-->

    <!--if more than one page-->
    <xsl:if test="number(@nbPages) &gt; 1">
      <xsl:call-template name="nav">
        <xsl:with-param name="page" select="$page" />
        <xsl:with-param name="mod" select="$mod" />
        <xsl:with-param name="onclick" select="$onclick"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>


  <!--=======================================================================-->
  <!--                "hitsPerPage" count                                    -->
  <!--=======================================================================-->
  <xsl:template name="_hppCount">
    <xsl:param name="min" />
    <xsl:param name="page" />
    <xsl:param name="i" select="1" />
    <xsl:param name="max" select="10" />
    <xsl:param name="selected" />
    <xsl:param name="onclick" select="''" />

<!--<xsl:message>
template "_hppCount" $onclick="<xsl:value-of select="$onclick"/>"
</xsl:message>-->

    <!-- Le lot précédent -->
		<xsl:if test="$i = $min and $i != 1">
			<xsl:call-template name="prec-group">
				<xsl:with-param name="i" select="$i"/>
				<xsl:with-param name="page" select="$page"/>
				<xsl:with-param name="onclick" select="$onclick"/>
			</xsl:call-template>
		</xsl:if>

    <!-- La page actuelle -->
    <xsl:if test="$i = $selected">
      <span title="results.nav.pages.current.title" i18n:attr="title"> <!--FIXME : title y/n?-->
        <xsl:value-of select="$i" />
      </span>
    </xsl:if>


      <!-- Les pages suivantes -->
    <xsl:if test="$i != $selected">
		<!-- return displayDocumentResults("FRPALME0000000000286A", "cdc-td-FRPALME0000000000286A", "docsearch.xsp?r=FRPALME0000000000286A&bq=eas1190261341500"); -->
      <a href="{$page}&amp;p={$i}" accesskey="{$i}" title="results.nav.pages.page.title" i18n:attr="title">
        <xsl:if test="$onclick!=''">
          <xsl:attribute name="onclick"><xsl:value-of select="$onclick" /></xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$i" />
      </a>
      <xsl:text> </xsl:text>
    </xsl:if>



    <xsl:choose>


      <!-- On boucle sur les autres pages -->
      <xsl:when test="$i &lt; $max">
        <xsl:call-template name="_hppCount">
          <xsl:with-param name="min" select="$min" />
          <xsl:with-param name="i" select="$i+1" />
          <xsl:with-param name="max" select="$max" />
          <xsl:with-param name="selected" select="$selected" />
          <xsl:with-param name="page" select="$page" />
					<xsl:with-param name="onclick" select="$onclick" />
        </xsl:call-template>
      </xsl:when>


      <!-- Le lot suivant -->
      <xsl:when test="$i = $max and $i != @nbPages">
				<xsl:call-template name="next-group">
					<xsl:with-param name="page" select="$page"/>
					<xsl:with-param name="i" select="$i"/>
					<xsl:with-param name="onclick" select="$onclick"/>
				</xsl:call-template>
      </xsl:when>

    </xsl:choose>

  </xsl:template>
	
	<!--=======================================================================-->
  <!--                for prec group                                          -->
  <!--=======================================================================-->
	<xsl:template name="prec-group">
		<xsl:param name="i"/>
		<xsl:param name="page"/>
		<xsl:param name="onclick" select="''"/>

		<a href="{$page}&amp;p={$i - 1}" title="results.nav.pages.group.prec.title" i18n:attr="title"><!--FIXME alt et accesskey y/n?-->
			<xsl:if test="$onclick!=''">
				<xsl:attribute name="onclick"><xsl:value-of select="$onclick" /></xsl:attribute>
			</xsl:if>
			<i18n:text key="results.nav.pages.group.prec">&lt;&lt;</i18n:text>
		</a>

		<xsl:text> </xsl:text>

	</xsl:template>
	
	<!--=======================================================================-->
  <!--                for next group                                          -->
  <!--=======================================================================-->
	<xsl:template name="next-group">
		<xsl:param name="i"/>
		<xsl:param name="page"/>
		<xsl:param name="onclick" select="''"/>
		<a href="{$page}&amp;p={$i + 1}" title="results.nav.pages.group.next.title" i18n:attr="title"><!--FIXME alt et accesskey y/n?-->
			<xsl:if test="$onclick!=''">
				<xsl:attribute name="onclick"><xsl:value-of select="$onclick" /></xsl:attribute>
			</xsl:if>
			<i18n:text key="results.nav.pages.group.next">&gt;&gt;</i18n:text>
		</a>
		<xsl:text> </xsl:text>

	</xsl:template>


  <!--=======================================================================-->
  <!--                for next page                                          -->
  <!--=======================================================================-->
  <!--FIXME nécessité  + alt et accesskey y/n?-->
  <!--<xsl:template name="next-page">

    <xsl:param name="page" />

      <xsl:if test="number(@currentPage) &lt; number(@nbPages)">

        <xsl:variable name="title">
          <xsl:text>Page de résultats suivante (raccourci: Alt s)</xsl:text>
        </xsl:variable>

        <a href="{$page}&amp;p={number(@currentPage)+1}" onclick="windowManager.updateDocumentResults('{$divid}', this.href); return false;" title="{$title}" accesskey="s">

          <xsl:text>&gt;&gt;</xsl:text>

        </a>

      </xsl:if>

  </xsl:template>-->


  <!--=======================================================================-->
  <!--                for preview page                                       -->
  <!--=======================================================================-->
  <!--FIXME nécessité  + alt et accesskey y/n?-->
<!--  <xsl:template name="prev-page">

    <xsl:param name="page" />

      <xsl:if test="@currentPage &gt; 1">

        <xsl:variable name="title">
          <xsl:text>Page de résultats précédente (raccourci: Alt p)</xsl:text>
        </xsl:variable>

        <a href="{$page}&amp;p={number(@currentPage)-1}" onclick="windowManager.updateDocumentResults('{$divid}', this.href); return false;" title="{$title}" accesskey="p">
          <xsl:text>&lt;&lt;</xsl:text>
        </a>

      </xsl:if>

  </xsl:template>
-->

  <!--=======================================================================-->
  <!--          Fonction 'first-page'                                        -->
  <!--=======================================================================-->
  <!-- NOTE: Bouton pour aller la première page de résultats [mp] -->
  <xsl:template name="first-page">
    <xsl:param name="page" />
		<xsl:param name="onclick" select="''" />
      <xsl:if test="number(@currentPage) &gt; 1">
        <a href="{$page}&amp;p=1" title="results.nav.pages.first.title" accesskey="f" i18n:attr="title"><!--FIXME alt et accesskey y/n?-->
          <xsl:if test="$onclick!=''">
          	<xsl:attribute name="onclick"><xsl:value-of select="$onclick" /></xsl:attribute>
					</xsl:if>
					<i18n:text key="results.nav.pages.first">|&lt;</i18n:text>
        </a>
				<xsl:text> </xsl:text>
      </xsl:if>
  </xsl:template>


  <!--=======================================================================-->
  <!--          Fonction 'last-page'                                         -->
  <!--=======================================================================-->
  <!-- NOTE: Bouton pour aller la dernière page de résultats [mp] -->
  <xsl:template name="last-page">
    <xsl:param name="page" />
		<xsl:param name="onclick" select="''" />
      <xsl:if test="number(@currentPage) &lt; number(@nbPages)">
        <a href="{$page}&amp;p={number(@nbPages)}" title="results.nav.pages.last.title" accesskey="l" i18n:attr="title"><!--FIXME alt et accesskey y/n?-->
					<xsl:if test="$onclick!=''">
          	<xsl:attribute name="onclick"><xsl:value-of select="$onclick" /></xsl:attribute>
					</xsl:if>
					<i18n:text key="results.nav.pages.last">&gt;|</i18n:text>
        </a>
				<xsl:text> </xsl:text>
      </xsl:if>
  </xsl:template>


  <!--=======================================================================-->
  <!--                Fonction 'nav'                                         -->
  <!--=======================================================================-->
  <xsl:template name="nav">
    <xsl:param name="page" />
    <xsl:param name="mod" />
		<xsl:param name="onclick" select="''" />

<!--<xsl:message>
template "nav" $onclick="<xsl:value-of select="$onclick"/>"
</xsl:message>-->

    <xsl:call-template name="first-page">
      <xsl:with-param name="page" select="$page" />
			<xsl:with-param name="onclick" select="$onclick" />
    </xsl:call-template>

<!--  <xsl:call-template name="prev-page">
      <xsl:with-param name="page" select="$page" />
    </xsl:call-template>-->


    <!-- count number of page to show -->
    <xsl:variable name="min" select="( ceiling(@currentPage div $mod) -1 ) * $mod + 1" />


    <xsl:variable name="max">
      <xsl:choose>
        <xsl:when test="@nbPages &lt; $min + $mod - 1">
          <xsl:value-of select="@nbPages" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$min+$mod - 1" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>



    <!-- count pages -->
      <xsl:call-template name="_hppCount">
        <xsl:with-param name="min" select="$min" />
        <xsl:with-param name="i" select="$min" />
        <xsl:with-param name="max" select="$max" />
        <xsl:with-param name="selected" select="@currentPage" />
        <xsl:with-param name="page" select="$page" />
				<xsl:with-param name="onclick" select="$onclick" />
      </xsl:call-template>

<!--    <xsl:call-template name="next-page">
      <xsl:with-param name="page" select="$page" />
    </xsl:call-template>-->

    <xsl:call-template name="last-page">
      <xsl:with-param name="page" select="$page" />
			<xsl:with-param name="onclick" select="$onclick" />
    </xsl:call-template>

  </xsl:template>

</xsl:stylesheet>


