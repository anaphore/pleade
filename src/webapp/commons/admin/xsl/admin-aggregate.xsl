<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | XSLT gérant la fusion des aggregate.
		| FIXME : il se peut qu'il reste encore des problèmes de contexte pour la fusion des menus commun et admin
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                              exclude-result-prefixes="xsl i18n">
    <!--+
        | Traitement de la racine de l'aggrégat
        +-->
  <xsl:template match="/aggregate">
    <xsl:apply-templates select="*" mode="template" />
  </xsl:template>
	
	<xsl:template match="common-menus" mode="template">
		<xsl:apply-templates select="*" mode="template" />
	</xsl:template>
	<xsl:template match="menus" mode="template">
		<xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="common-menu" />
			<xsl:apply-templates select="/aggregate/admin-menus/menus/menu" mode="template"/> 
    </xsl:copy>
	</xsl:template>
	<xsl:template match="admin-menus" mode="template">
	</xsl:template>

	<!-- Dans le cas de la fusion des menus commun et admin (catalogue i18n par défaut différent), on précise le catalogue des clés i18n  -->
	<xsl:template match="@title[not(contains(., ':')) and contains(parent::*/@i18n:attr, 'title')]" mode="common-menu">
		<xsl:attribute name="{local-name()}">
      <xsl:value-of select="concat('module-eadeac:',.)" />
    </xsl:attribute>
	</xsl:template>
	<xsl:template match="i18n:text[not(@catalogue) and not(@i18n:catalogue)]" mode="common-menu">
		<xsl:copy>
			<xsl:attribute name="catalogue">module-eadeac</xsl:attribute>
      <xsl:apply-templates select="@* | node()" mode="common-menu" />
    </xsl:copy>
	</xsl:template>
	<xsl:template match="@* | node()" mode="common-menu">
		<xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="common-menu" />
    </xsl:copy>
	</xsl:template>

  <!-- Copie générique -->
  <xsl:template match="node() | @*" mode="template" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="template" />
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
