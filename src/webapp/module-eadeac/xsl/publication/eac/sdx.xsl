<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Production du sdx:document pour SDX TODO: à compléter -->
<xsl:stylesheet 
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:saxon="http://saxonf.sf.net"
  exclude-result-prefixes="saxon">

  <xsl:param name="debug" select="'false'"/>

  <!-- L'identifiant de la fiche racine -->
  <xsl:variable name="eacid" select="//*[local-name() = 'eac-cpf']/@pleade:eacid"/>
  <!-- type -->
  <xsl:variable name="type" select="/*[local-name() = 'eac-cpf']/*[local-name() = 'cpfDescription']/*[local-name() = 'identity']/*[local-name() = 'entityType']"/>

  <!-- Template racine -->
  <xsl:template match="/">
    <xsl:message>  Production des informations pour l'indexation SDX (etape 6 / 6) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
    <xsl:message>    Identifiant du document : <xsl:value-of select="$eacid" /></xsl:message>
    <xsl:choose>
      <xsl:when test="$debug='true'">
        <xsl:variable name="result">
          <xsl:apply-templates/>
        </xsl:variable>
        <xsl:copy-of saxon:read-once="yes" select="$result"/>
        <xsl:if test="$debug='true'">
          <xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{$eacid}/{$eacid}-6.xml"
            xmlns:sys="java:java.lang.System">
            <xsl:copy-of saxon:read-once="yes" select="$result"/>
          </xsl:result-document>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- On intercepte l'élément racine pour créer un document SDX -->
  <xsl:template match="*[local-name()='eac-cpf']">
    <sdx:document id="{$eacid}">
      <sdx:field name="searchable">1</sdx:field>
      <!-- Doc racine -->
      <sdx:field name="top">1</sdx:field>
      <!-- Identifiant de la racine -->
      <sdx:field name="root-id"><xsl:value-of select="$eacid"/></sdx:field>
      <!-- Le type d'autorité -->
      <sdx:field name="type"><xsl:value-of select="$type"/></sdx:field>
      <!-- Le texte intégral -->
      <sdx:field name="fulltext">
        <xsl:apply-templates mode="fulltext"/>
      </sdx:field>
      <!-- Les dates d'existence -->
      <xsl:apply-templates select="*[local-name()='cpfDescription']/*[local-name()='description']/*[local-name()='existDates']" mode="content"/>
      <xsl:apply-templates select="*[local-name()='cpfDescription']"/>
    </sdx:document>
  </xsl:template>

  <xsl:template match="*[local-name()='cpfDescription']">
    <!-- And then we process subcomponents -->
    <xsl:apply-templates select="*[local-name()='alternativeSet'] | *[local-name()='description'] | *[local-name()='identity'] | *[local-name()='relations']"/>
  </xsl:template>

  <xsl:template match="*[local-name()='identity']">
    <xsl:variable name="pname"><xsl:call-template name="output-preferredname"/></xsl:variable>
    <sdx:field name="preferredname"><xsl:value-of select="$pname"/></sdx:field>
    <!-- TODO : trouver meilleure solution pour déterminer le nom du champ SDX en fonction du type de l'entité -->
    <xsl:variable name="nameType">
      <xsl:choose>
        <xsl:when test="$type = 'family'">famname</xsl:when>
        <xsl:when test="$type = 'corporateBody'">corpname</xsl:when>
        <xsl:otherwise>persname</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:apply-templates select="//*[local-name()='part']" mode="word">
      <xsl:with-param name="field" select="$nameType"/>
      <xsl:with-param name="content" select="$pname"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="//*[local-name()='part']" mode="field">
      <xsl:with-param name="field" select="concat('f', $nameType)"/>
      <xsl:with-param name="content" select="$pname"/>
    </xsl:apply-templates>
    <!-- Noms de l'entité -->
    <xsl:apply-templates select="//*[local-name()='part']" mode="word">
      <xsl:with-param name="field" select="'entityname'"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="//*[local-name()='part']" mode="field">
      <xsl:with-param name="field" select="'fentityname'"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="*[local-name()='placeEntry']">
    <sdx:field name="place"><xsl:apply-templates select="." mode="fulltext"/></sdx:field>
    <sdx:field name="fplace"><xsl:value-of select="normalize-space(.)"/></sdx:field>
  </xsl:template>

  <xsl:template match="*[local-name()='function']/*[local-name()='term'] | *[local-name()='functionRelation']/*[local-name()='relationEntry']">
    <sdx:field name="function"><xsl:apply-templates select="." mode="fulltext"/></sdx:field>
    <sdx:field name="ffunction"><xsl:value-of select="normalize-space(.)"/></sdx:field>
  </xsl:template>

  <xsl:template match="*[local-name()='legalStatus']/*[local-name()='term']">
    <sdx:field name="legalstatus"><xsl:apply-templates select="." mode="fulltext"/></sdx:field>
    <sdx:field name="flegalstatus"><xsl:value-of select="normalize-space(.)"/></sdx:field>
  </xsl:template>

  <xsl:template match="*[local-name()='biogHist'] | *[local-name()='generalContext'] | *[local-name()='mandate']">
    <xsl:apply-templates select="." mode="word">
      <xsl:with-param name="field" select="lower-case(local-name())"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="*[local-name()='function'] | *[local-name()='functionRelation']">
    <xsl:apply-templates select="." mode="word">
      <xsl:with-param name="field" select="'functiondesc'"/>
    </xsl:apply-templates>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="*[local-name()='structureOrGenealogy']">
    <xsl:apply-templates select="." mode="word">
      <xsl:with-param name="field" select="'structure'"/>
    </xsl:apply-templates>
  </xsl:template>

  <!--+
      | Contenus génériques : word, field, fulltext
      +-->
  <!-- Un champ word à sortir -->
  <xsl:template match="*" mode="word">
    <xsl:param name="content" select="."/>
    <xsl:param name="field"/>
    <sdx:field name="{$field}">
      <xsl:apply-templates select="$content" mode="fulltext"/>
    </sdx:field>
  </xsl:template>

  <!-- Un champ field à sortir -->
  <xsl:template match="*" mode="field">
    <xsl:param name="content" select="."/>
    <xsl:param name="field"/>
    <xsl:variable name="value" select="normalize-space($content)"/>
    <xsl:if test="$value != ''">
      <sdx:field name="{$field}"><xsl:value-of select="$value"/></sdx:field>
    </xsl:if>
  </xsl:template>

  <!-- Les éléments en mode plein texte -->
  <xsl:template match="*" mode="fulltext">
    <xsl:if test="not(self::*[local-name()='relationEntry']) and not(self::*[local-name()='resourceRelation'])">
      <xsl:text> </xsl:text>
      <xsl:apply-templates mode="fulltext"/>
    </xsl:if>
  </xsl:template>
  <xsl:template match="text()" mode="fulltext">
    <xsl:value-of select="."/>
  </xsl:template>

  <!-- +
       |    Contenus particuliers
       +-->
  <xsl:template name="output-preferredname">
    <xsl:choose>
      <xsl:when test="count(*[local-name()='nameEntry']/*[local-name()='part'])=1">
        <xsl:value-of select="normalize-space(*[local-name()='nameEntry']/*[local-name()='part'])"/>
      </xsl:when>
      <xsl:when test="*[local-name()='nameEntry']/*[local-name()='part'][contains(@localType, 'full')]">
        <xsl:value-of select="normalize-space(*[local-name()='nameEntry']/*[local-name()='part' and contains(@localType, 'full')])"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="*/*[local-name()='part'][1]"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Les dates -->
      <!-- <xsl:if test="self::unitdate">
        <xsl:variable name="date" select="pleade:get-normal-date(.)"/>
        <xsl:variable name="parts" select="fn:tokenize($date, '/')"/>
        <xsl:variable name="b" select="$parts[1]"/>
        <xsl:variable name="e">
          <xsl:choose>
            <xsl:when test="$parts[2]"><xsl:value-of select="$parts[2]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$b"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="bDate" select="dateutils:format(dateutils:parse($b))" xmlns:dateutils="java:org.pleade.utils.DateUtilities"/>
        <xsl:if test="$bDate">
          <sdx:field name="bdate"><xsl:value-of select="$bDate"/></sdx:field>
        </xsl:if>
        <xsl:variable name="eDate" select="dateutils:format(dateutils:parse($e))" xmlns:dateutils="java:org.pleade.utils.DateUtilities"/>
        <xsl:if test="$eDate">
          <sdx:field name="edate"><xsl:value-of select="$eDate"/></sdx:field>
        </xsl:if>
      </xsl:if> -->

  <xsl:template match="*[local-name()='existDates']" mode="content">
    <xsl:variable name="val">
      <xsl:choose>
        <xsl:when test="*[local-name()='dateRange'] or *[local-name()='dateSet']/*[local-name()='dateRange']">
          <!-- <xsl:value-of select=".//dateRange[1]/fromDate"/>
          <xsl:if test=".//dateRange[1]/toDate">
            <xsl:text> - </xsl:text>
            <xsl:value-of select=".//dateRange[1]/toDate"/>
          </xsl:if> -->
          <xsl:value-of select="string-join(*, ' - ')"/>
        </xsl:when>
        <!-- date -->
        <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <sdx:field name="udate"><xsl:value-of select="$val"/></sdx:field>
  </xsl:template>

  <!-- +
       |    Les exclusions
       +-->
  <xsl:template match="*[local-name()='objectBinWrap'] | *[local-name()='objectXMLWrap']" mode="word">
  </xsl:template>

</xsl:stylesheet>
