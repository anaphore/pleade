﻿<?xml version="1.0" encoding="UTF-8"?>
<!-- comment passer des variables de message ? -->
<root>
    <xfm:messages xml:lang="es" xmlns:xfm="http://www.w3.org/2002/01/xforms">
        <!-- specific to this application -->
        <xfm:caption id="login-please">Por favor, login</xfm:caption>
        <xfm:caption id="allowed">Usted est� autorizado para abrir esta aplicaci�n : </xfm:caption>
        <xfm:caption id="forbidden"> no est� autorizado para usar esta aplicaci�n : </xfm:caption>
        <xfm:caption id="notadmin">
            <p>Por favor, login antes.</p>
        </xfm:caption>
        <xfm:hint id="bases">Administra bases de documento de esta aplicaci�n</xfm:hint>
        <!-- Xform -->
        <xfm:hint id="xfm:select-add">[Return] Agrega item(es) seleccionados a la lista [Ctrl+Click] o [Shift+Up/Down] para seleccionar m�s de uno</xfm:hint>
        <xfm:hint id="xfm:select-del">Borra el item(es) seleccionado(s)[Ctrl+Clic] o [Shift+Up/Down] para seleccionar m�s de uno</xfm:hint>
        <xfm:hint id="xfm:select-up">[Ctrl+Up] Selected item go up</xfm:hint>
        <xfm:hint id="xfm:select-down">[Ctrl+Down] Selected item go down</xfm:hint>
        <xfm:hint id="xfm:select-reset">Borra todos los �temes</xfm:hint>
        <xfm:caption id="xfm:insert">+</xfm:caption>
        <xfm:hint id="xfm:insert">Add an item to the end of the list</xfm:hint>
        <xfm:caption id="xfm:delete">-</xfm:caption>
        <xfm:caption id="xfm:submit">Env�a</xfm:caption>
        <xfm:hint id="xfm:submit">Env�a datos al servidor</xfm:hint>
        <xfm:caption id="xfm:reset">Cancela</xfm:caption>
        <xfm:caption id="xfm:secret">Clave</xfm:caption>
        <xfm:hint id="xfm:secret">Escriba su clave en el primer casillero, confirme en el segundo.</xfm:hint>
        <xfm:alert id="xfm:secret">Algo est� mal? no coinciden las claves en ambos casilleros.</xfm:alert>
        <!-- messages -->
        <xfm:caption id="superuser-not-set">Super-Usuario no definido.</xfm:caption>
        <xfm:caption id="anonymous">Usuario an�nimo</xfm:caption>
        <xfm:caption id="admin-app">Administrador de la aplicaci�n </xfm:caption>
        <xfm:caption id="user-app">Usuario de la aplicaci�n </xfm:caption>
        <!-- fields -->
        <xfm:caption id="directory">Directorio</xfm:caption>
        <xfm:caption id="explorer">Explorar</xfm:caption>
        <xfm:caption id="firstname">Nombre</xfm:caption>
        <xfm:alert id="firstname" class="required">Es preciso un nombre</xfm:alert>
        <xfm:caption id="lastname">Apellido</xfm:caption>
        <xfm:alert id="lastname" class="required">Es preciso un apellido</xfm:alert>
        <xfm:caption id="id">Login o ID</xfm:caption>
        <xfm:alert id="id" class="required">Un ID es necesario</xfm:alert>
        <xfm:caption id="new-id">Nuevo login o ID</xfm:caption>
        <xfm:alert id="new-id" class="required">Un nuevo ID es necesario</xfm:alert>
        <xfm:caption id="old-id">Login antiguo</xfm:caption>
        <xfm:alert id="old-id" class="required">ID antigua es requerido</xfm:alert>
        <xfm:caption id="pass">Clave</xfm:caption>
        <xfm:alert id="pass" class="required">Una clave es necesaria</xfm:alert>
        <xfm:caption id="new-pass">Nueva clave</xfm:caption>
        <xfm:alert id="new-pass" class="required">Una clave nueva es necesaria</xfm:alert>
        <xfm:caption id="old-pass">Clave antigua</xfm:caption>
        <xfm:alert id="old-pass" class="required">La clave antigua es requerida</xfm:alert>
        <xfm:caption id="email">Correo Electr�nico</xfm:caption>
        <xfm:caption id="lang">Idioma</xfm:caption>
        <xfm:caption id="desk">Desk</xfm:caption>
        <xfm:hint id="desk">Aceso al escritorio de administraci�n</xfm:hint>
        <xfm:caption id="login">Login</xfm:caption>
        <xfm:caption id="login-as">Logged como: </xfm:caption>
        <xfm:caption id="user">Usuario</xfm:caption>
        <xfm:caption id="users">Usuarios</xfm:caption>
        <xfm:caption id="group">Grupo</xfm:caption>
        <xfm:caption id="groups">Grupos</xfm:caption>
        <xfm:caption id="description">Descripci�n</xfm:caption>
        <!-- targets -->
        <xfm:caption id="welcome">Bienvenido</xfm:caption>
        <xfm:hint id="welcome">Bienvenido : P�gina Principal de esta secci�n</xfm:hint>
        <xfm:hint id="go">Ir : ver esta aplicaci�n</xfm:hint>
        <!-- objects -->
        <xfm:caption id="folder">Carpeta</xfm:caption>
        <xfm:caption id="server">Servidor SDX</xfm:caption>
        <xfm:hint id="server">Administraci�n del servidor SDX</xfm:hint>
        <xfm:caption id="app">Aplicaci�n</xfm:caption>
        <xfm:caption id="apps">Aplicaciones</xfm:caption>
        <xfm:hint id="apps">Applicaciones : administraci�n de las aplicaciones en el servidor.</xfm:hint>
        <xfm:caption id="bases">Bases</xfm:caption>
        <xfm:caption id="identities">Identidades</xfm:caption>
        <xfm:hint id="identities">Identidades : administraci�n de grupos u usuarios para esta aplicaci�n.</xfm:hint>
        <xfm:caption id="su">Super-Usuario</xfm:caption>
        <xfm:hint id="su">Modifica datos del super usuario</xfm:hint>
        <!-- actions -->
        <xfm:caption id="go">Ir a</xfm:caption>
        <xfm:caption id="send">Enviar</xfm:caption>
        <xfm:hint id="send">Enviar : env�a estos datos al servidor</xfm:hint>
        <xfm:caption id="init">Inicia</xfm:caption>
        <xfm:caption id="open">Abre</xfm:caption>
        <xfm:caption id="close">Cierra</xfm:caption>
        <xfm:caption id="edit">Edita</xfm:caption>
        <xfm:hint id="edit">Edita : para modificar este documento</xfm:hint>
        <xfm:caption id="desk">Desk</xfm:caption>
        <xfm:caption id="admin">Admin</xfm:caption>
        <xfm:caption id="login">Login</xfm:caption>
        <xfm:caption id="logout">Logout</xfm:caption>
        <xfm:hint id="logout">Logout : pasa a usuario an�nimo.</xfm:hint>
        <xfm:caption id="enter">Ingresa</xfm:caption>
        <xfm:hint id="enter"/>
        <xfm:caption id="save">Guarda</xfm:caption>
        <xfm:caption id="delete">Borra</xfm:caption>
        <xfm:caption id="modify">Modifica</xfm:caption>
        <xfm:caption id="new">New</xfm:caption>
        <xfm:caption id="confirm">Confirma</xfm:caption>
        <xfm:caption id="reconfigure">Reconfigura</xfm:caption>
        <!-- list of languages -->
        <xfm:caption id="lang">Idioma</xfm:caption>
        <xfm:hint id="lang">Elija un idioma</xfm:hint>
        <xfm:caption parent="lang" id="af">Afrikaans</xfm:caption>
        <xfm:caption parent="lang" id="ar">Arabe</xfm:caption>
        <xfm:caption parent="lang" id="az">Azerbaijani</xfm:caption>
        <xfm:caption parent="lang" id="be">Byelorussian</xfm:caption>
        <xfm:caption parent="lang" id="br">Bret�n</xfm:caption>
        <xfm:caption parent="lang" id="bg">B�lgo</xfm:caption>
        <xfm:caption parent="lang" id="ca">Catal�n</xfm:caption>
        <xfm:caption parent="lang" id="cs">Checo</xfm:caption>
        <xfm:caption parent="lang" id="da">Holand�s</xfm:caption>
        <xfm:caption parent="lang" id="de">Alem�n</xfm:caption>
        <xfm:caption parent="lang" id="el">Griego</xfm:caption>
        <xfm:caption parent="lang" id="en">Ingl�s</xfm:caption>
        <xfm:caption parent="lang" id="eo">Esperanto</xfm:caption>
        <xfm:caption parent="lang" id="es">Espa�ol</xfm:caption>
        <xfm:caption parent="lang" id="et">Estoniano</xfm:caption>
        <xfm:caption parent="lang" id="eu">Vasco</xfm:caption>
        <xfm:caption parent="lang" id="fo">Faroese</xfm:caption>
        <xfm:caption parent="lang" id="fa">Persa</xfm:caption>
        <xfm:caption parent="lang" id="fi">Finland�s</xfm:caption>
        <xfm:caption parent="lang" id="fr">Franc�s</xfm:caption>
        <xfm:caption parent="lang" id="gl">Gallego</xfm:caption>
        <xfm:caption parent="lang" id="hr">Croata</xfm:caption>
        <xfm:caption parent="lang" id="hu">H�ngaro</xfm:caption>
        <xfm:caption parent="lang" id="hy">Armenio</xfm:caption>
        <xfm:caption parent="lang" id="ia">Interlingua</xfm:caption>
        <xfm:caption parent="lang" id="id">Indonesio</xfm:caption>
        <xfm:caption parent="lang" id="is">Island�s</xfm:caption>
        <xfm:caption parent="lang" id="it">Italiano</xfm:caption>
        <xfm:caption parent="lang" id="ja">Japan�s</xfm:caption>
        <xfm:caption parent="lang" id="kn">Kannada</xfm:caption>
        <xfm:caption parent="lang" id="ka">Georgiano</xfm:caption>
        <xfm:caption parent="lang" id="ko">Coreano</xfm:caption>
        <xfm:caption parent="lang" id="lv">Latvian</xfm:caption>
        <xfm:caption parent="lang" id="lt">Lituano</xfm:caption>
        <xfm:caption parent="lang" id="ml">Malayo</xfm:caption>
        <xfm:caption parent="lang" id="mr">Marathi</xfm:caption>
        <xfm:caption parent="lang" id="mk">Macedonio</xfm:caption>
        <xfm:caption parent="lang" id="mn">Mongoliano</xfm:caption>
        <xfm:caption parent="lang" id="nl">Dutch</xfm:caption>
        <xfm:caption parent="lang" id="no">Noruego</xfm:caption>
        <xfm:caption parent="lang" id="oc">Langue d'Oc</xfm:caption>
        <xfm:caption parent="lang" id="pa">Panjabi</xfm:caption>
        <xfm:caption parent="lang" id="pl">Polaco</xfm:caption>
        <xfm:caption parent="lang" id="pt">Portugu�s</xfm:caption>
        <xfm:caption parent="lang" id="ro">Rumano</xfm:caption>
        <xfm:caption parent="lang" id="ru">Ruso</xfm:caption>
        <xfm:caption parent="lang" id="sa">S�nscrito</xfm:caption>
        <xfm:caption parent="lang" id="sk">Eslovaco</xfm:caption>
        <xfm:caption parent="lang" id="sl">Eslovenio</xfm:caption>
        <xfm:caption parent="lang" id="sq">Albanese</xfm:caption>
        <xfm:caption parent="lang" id="sr">Serbian</xfm:caption>
        <xfm:caption parent="lang" id="sv">Swedish</xfm:caption>
        <xfm:caption parent="lang" id="ta">Tamil</xfm:caption>
        <xfm:caption parent="lang" id="tt">Tatar</xfm:caption>
        <xfm:caption parent="lang" id="te">Telugu</xfm:caption>
        <xfm:caption parent="lang" id="th">Thai</xfm:caption>
        <xfm:caption parent="lang" id="tr">Turco</xfm:caption>
        <xfm:caption parent="lang" id="uk">Ucrainiano</xfm:caption>
        <xfm:caption parent="lang" id="ur">Urdu</xfm:caption>
        <xfm:caption parent="lang" id="uz">Uzbek</xfm:caption>
        <xfm:caption parent="lang" id="vi">Vietnam�s</xfm:caption>
        <xfm:caption parent="lang" id="zh">Chino</xfm:caption>
    </xfm:messages>
</root>
