<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="1.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		exclude-result-prefixes="xsl i18n pleade sdx xs">
		<!-- 
		
			/!\/!\ CE FICHIER NE PEUT PAS ÊTRE SURCHARGÉ DIRECTEMENT, /!\/!\
			/!\/!\     IL FAUT SURCHARGER CEUX QUI L'IMPORTENT        /!\/!\
		
		-->
		
		
	<!--FIXME : si on passe en xslt 2.0 ici ça génère une erreur (redondance cyclique)-->
	
	<xsl:param name="isDebug"/>

	<!--En dynamic on laisse les etiquettes i18n qui seront résolues par cocoon à la fin $i18n-doc-root.ns ne sera donc jamais appelé
	(cela génèrerait un erreur le cas echéant car dans l'environnment dynamique le fichier i18n n'est plus à "cet endroit" relatif	-->
	<xsl:param name="cdc" select="'false'"/><!-- ('true' | 'false') -->
	<!--FIXME : peut être envoyé en paramètre via le build-->
	<xsl:variable name="i18n-doc-root.ns" select="document('../../i18n/module-ead-xslt.xml')/catalogue"/>

	<!--START FIXME : à mettre dans xsl de plus haut niveau > c'est vraiment générique-->
	<xsl:variable name="quote">
		<xsl:text>'</xsl:text>
	</xsl:variable>

	<xsl:function name="pleade:escapejs">
		<xsl:param name="text" />
		<xsl:value-of select="replace( $text, $quote, concat('\\', $quote ) )" />
	</xsl:function>

	<xsl:template name="remove-suffix">
		<xsl:param name="sep" select="'.'"/>
		<xsl:param name="str" select="''"/>
		<xsl:if test="contains($str, $sep)">
			<xsl:value-of select="substring-before($str, $sep)"/>
			<xsl:if test="contains(substring-after($str, $sep), $sep)">
				<xsl:value-of select="$sep"/>
				<xsl:call-template name="remove-suffix">
					<xsl:with-param name="sep" select="$sep"/>
					<xsl:with-param name="str" select="substring-after($str, $sep)"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!--END FIXME-->


	<!--Gestion des erreurs-->
	<xsl:template name="error">
	<!--todo : recopier les etiquette i18n quand on est en dynamique-->
		<xsl:param name="key" select="'ead-default-error'"/>
		<xsl:param name="display" select="'element'"/> <!--(element | text | none)-->
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:variable name="message">
					<xsl:choose>
						<xsl:when test="$i18n-doc-root.ns/message[@key = $key]">
							<xsl:copy-of select="$i18n-doc-root.ns/message[@key = $key]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:copy-of select="$i18n-doc-root.ns/message[@key = 'ead-default-error']"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$display = 'element'">
						<div class="pl-pgd-error">
							<xsl:copy-of select="$message"/>
						</div>
					</xsl:when>
					<xsl:when test="$display = 'text'">
						<xsl:value-of select="$message"/>
					</xsl:when>
				</xsl:choose>
				<xsl:message>
					<xsl:value-of select="$message"/>
				</xsl:message>
			</xsl:when>
			<xsl:otherwise><!--	 $mode = 'dyn' -->
				<!--TODO-->
				<xsl:choose>
					<xsl:when test="$display = 'element'">
						<div class="pl-pgd-error">
							<i18n:text>
								<xsl:value-of select="$key"/>
							</i18n:text>
						</div>
					</xsl:when>
					<xsl:when test="$display = 'text'">
						<i18n:text>
							<xsl:value-of select="$key"/>
						</i18n:text>
					</xsl:when>
				</xsl:choose>
				<xsl:message>
					<i18n:text>
						<xsl:value-of select="$key"/>
					</i18n:text>
				</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ************************* GENERIC TEMPLATES FOR DISPLAYING EAD ELEMENTS ************************* -->

	<!-- règle : les elements de type texte seront toujours affichés avec un <span> (cf. css) en revanche pour
	les éléments de type bloc,	on pourra choisir le nom de l'élément (typiquement <p> ou <div>)-->

	<xsl:template name="display-as-text"><!-- texte -->
		<xsl:param name="el" select="."/>
		<xsl:param name="out-el-class" select="concat( 'pl-ead-', local-name($el) )" />
		<span class="{$out-el-class}">
			<xsl:apply-templates select="$el/* | $el/text()"/>
		</span>
	</xsl:template>

	<xsl:template name="display-as-block"><!-- bloc -->
		<xsl:param name="el" select="."/>
		<xsl:param name="out-el-name" select="'div'"/>
		<xsl:param name="out-el-class" select="concat( 'pl-ead-', local-name($el) )" />
		<xsl:element name="{$out-el-name}">
			<xsl:attribute name="class">
				<xsl:value-of select="$out-el-class"/>
			</xsl:attribute>
			<xsl:apply-templates select="$el/* | $el/text()"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="display-as-text-with-label"> <!--texte avec étiquette-->
		<xsl:param name="el" select="."/>
		<xsl:param name="label-ns">
			<xsl:call-template name="get-label">
				<xsl:with-param name="el" select="$el"/>
			</xsl:call-template>
		</xsl:param>
		<xsl:param name="text-ns">
			<xsl:apply-templates select="$el/*[not(self::head)] | $el/text()"/>
		</xsl:param>
		<xsl:param name="text-class" select="concat( 'pl-ead-', local-name($el) )"/>
		<xsl:variable name="label-class" select="'pl-pgd-inline-label'"/> <!-- pas un param car ne doit pas changer pour fo -->
		<xsl:variable name="result">
			<span class="{$text-class}">
				<xsl:if test="$label-ns != ''"> <!-- Ne peut jamais être vide car par défaut on construit une clé i18n -->
					<span class="{$label-class}">
						<xsl:copy-of select="$label-ns"/>
						<xsl:if test="normalize-space($label-ns) != ''">
							<xsl:text>&#160;: </xsl:text>
						</xsl:if>
					</span>
				</xsl:if>
				<xsl:copy-of select="$text-ns"/>
			</span>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="(preceding-sibling::* or ../text()) and ancestor::did">
				<span class="pl-pgd-inline-next"><xsl:copy-of select="$result"/></span>
			</xsl:when>
			<xsl:otherwise><xsl:copy-of select="$result"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="display-as-block-with-label"><!-- bloc avec label -->
		<xsl:param name="el" select="."/>
		<xsl:param name="out-el-name" select="'div'"/>
		<xsl:param name="block-out-el-class" select="concat( 'pl-ead-', local-name($el) )"/>
		<xsl:variable name="label-out-el-class" select="'pl-pgd-block-label'"/><!-- pas un param car ne doit pas changer pour fo -->
		<xsl:element name="{$out-el-name}">
			<xsl:attribute name="class">
				<xsl:value-of select="$block-out-el-class"/>
			</xsl:attribute>
			<xsl:call-template name="display-label">
				<xsl:with-param name="el" select="$el"/>
				<!--<xsl:with-param name="out-el-class" select="concat( 'pl-pgd-head pl-ead-', local-name($el), '-head' )" /> c'est déjà la valeur par défaut-->
			</xsl:call-template>
			<xsl:apply-templates select="$el/*[not(self::head)] | $el/text()"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="display-as-section">
		<xsl:param name="el" select="."/>
		<div class="pl-pgd-sect pl-ead-{local-name($el)}" id="{$el/@pleade:id}">
			<xsl:call-template name="display-label"/>
			<xsl:apply-templates select="*[not(self::head)]" />
		</div>
		<xsl:if test="local-name($el) = 'daogrp'">
			<hr class="pl-separator"/>
		</xsl:if>

	</xsl:template>

<!--	<xsl:template name="display-as-fieldValue-container">
		<xsl:param name="el" select="."/>
		<table class="pl-pgd-fv pl-ead-{local-name($el)}">
			<xsl:call-template name="i18n-attribute">
				<xsl:with-param name="attribute-name" select="'summary'"/>
				<xsl:with-param name="key" select="'ead-filedesc-editionstmt-table-@summary'"/>
			</xsl:call-template>
			<xsl:apply-templates/>
		</table>
	</xsl:template>

	<xsl:template name="display-as-fieldValue">
		<xsl:param name="el" select="."/>
		<tr>
			<th scope="row">
				<xsl:call-template name="get-label">
					<xsl:with-param name="el" select="$el"/>
				</xsl:call-template>
			</th>
			<td>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>-->


	<!-- ****************************** DISPLAYING AND GETTING LABELS ****************************** -->

	<xsl:template name="display-label">
		<xsl:param name="el" select="."/>
		<xsl:param name="out-el-name" select="'p'"/>
		<xsl:param name="out-el-class" select="concat( 'pl-pgd-head pl-ead-', local-name($el), '-head' )" />
		<!--
		FIXME : nécessité de pl-ead-{local-name()}-head ?
			Exemples ou il est nécessaire :
				* table avec plusieurs tgroup, on affiche un <p> avant les tableaux pour le label.
				* div avec head
				=> aucun moyen (hierarchique) de savoir que ce p est un head de table.
		-->
		<xsl:variable name="label">
			<xsl:call-template name="get-label">
				<xsl:with-param name="el" select="$el"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$label != ''"> <!-- Ne peut jamais être vide car par défaut on construit une clé i18n -->
			<xsl:element name="{$out-el-name}">
				<xsl:attribute name="class">
					<xsl:value-of select="$out-el-class"/>
				</xsl:attribute>
				<xsl:copy-of select="$label"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<!-- Règles du get-label:
		1) Il peut retourner une structure HTML, pas uniquement du texte
		2) Il peut retourner un contenu vide
		3) Il peut s'appliquer sur un autre élément que "."
		4) Dans l'ordre, vérifier:
				@pleade:title
				child::head or child::daodesc
				@label
				recherche i18n
	-->
	<xsl:template name="get-label">
		<xsl:param name="el" select="."/>
		<!--DEBUG <xsl:message>name = <xsl:value-of select="name($el)"/></xsl:message>-->
		<xsl:choose>
			<xsl:when test="$el/@pleade:title">		<!-- FIXME: est-ce OK? -->
				<xsl:value-of select="@pleade:title"/>
			</xsl:when>
			<xsl:when test="$el/head != '' or $el/daodesc != ''">
				<xsl:apply-templates select="$el/head | $el/daodesc" mode="label"/>
			</xsl:when>
			<xsl:when test="$el/@label != ''">
				<xsl:value-of select="$el/@label"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="get-i18n-message">
					<!--<xsl:with-param name="key" select="concat('ead.', name($el), '.', $el/@altrender, '.', $el/@role, '.', $el/@type, '.', $el/@encodinganalog, '.', $el/@source)"/>-->
					<xsl:with-param name="key">
						<xsl:call-template name="make-i18n-key">
							<xsl:with-param name="el" select="$el"/>
						</xsl:call-template>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="make-i18n-key">
		<xsl:param name="el" select="."/>
		<xsl:value-of select="concat('ead.', name($el), '.', $el/@altrender, '.', $el/@role, '.', $el/@type, '.', $el/@encodinganalog, '.', $el/@source, (if($cdc castable as xs:boolean and xs:boolean($cdc)) then '.cdc' else ''))"/>
	</xsl:template>

	<xsl:template match="head | daodesc" mode="label">
		<xsl:apply-templates mode="label"/>
	</xsl:template>
	<xsl:template match="*" mode="label">
		<xsl:text> </xsl:text>
		<xsl:apply-templates mode="label"/>
	</xsl:template>
	<xsl:template match="text()" mode="label">
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template name="get-i18n-message">
		<xsl:param name="key" select="''"/>
		<!--DEBUG <xsl:message><xsl:value-of select="$key"/></xsl:message>-->
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:choose>
					<xsl:when test="$key = ''"/>
					<xsl:otherwise>
						<xsl:variable name="mess" select="$i18n-doc-root.ns/message[@key = $key]"/>
						<xsl:choose>
							<xsl:when test="$mess">
								<!--<span style="color:coral;">--> <!--todo a supprimer-->
								<xsl:copy-of select="$mess/node()"/>
								<!--</span>-->
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="get-i18n-message">
									<xsl:with-param name="key">
										<xsl:call-template name="remove-suffix">
											<xsl:with-param name="sep" select="'.'"/>
											<xsl:with-param name="str" select="$key"/>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn' -->
				<i18n:text>
					<xsl:value-of select="$key"/>
				</i18n:text>
				<!--FIXME : le i18n sera-t-il resolu de la même manière dynamiquement ?-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="i18n-attribute">
		<xsl:param name="attribute-name" select="''"/>
		<xsl:param name="key" select="''"/>
		<xsl:choose>
			<xsl:when test=" $mode = 'static' ">
				<xsl:attribute name="{$attribute-name}">
					<xsl:choose>
						<xsl:when test="$i18n-doc-root.ns/message[@key = $key]">
							<xsl:value-of select="$i18n-doc-root.ns/message[@key = $key]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="error">
								<xsl:with-param name="key" select="'ead-error-no-i18n-match'"/>
								<xsl:with-param name="display" select="'text'"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:when>
			<xsl:otherwise><!-- $mode = 'dyn' -->
				<xsl:attribute name="{$attribute-name}">
					<xsl:value-of select="$key"/>
				</xsl:attribute>
				<xsl:attribute name="i18n:attr">
					<xsl:value-of select="$attribute-name"/>
				</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ****************** TEMPLATE DISPLAYING ANCESTORS AND CHILD LINK OF A COMPONENT ****************** -->

	<xsl:template name="make-component-block">
		<xsl:if test="$isDebug"><xsl:message>		Traite <xsl:value-of select="name()"/><xsl:if test="@pleade:id"> (id=<xsl:value-of select="@pleade:id"/>)</xsl:if> mode make-children-block-dyn</xsl:message></xsl:if>
		<div>
			<xsl:attribute name="class">
				<xsl:text>pl-pgd-c pl-ead-</xsl:text>
				<xsl:choose>
					<!-- FIXME: devrait pas y avoir de différence! -->
					<xsl:when test=" $mode = 'static' ">
						<xsl:value-of select="local-name()"/>
					</xsl:when>
					<xsl:otherwise> <!--mode=dyn-->
						<xsl:value-of select="@pleade:original-element"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> pl-ead-att-level-</xsl:text>
				<xsl:value-of select="@level"/> <!-- @level : required --><!-- FIXME: pas required du tout! -->
				<xsl:if test="@otherlevel">
					<xsl:text> pl-ead-att-otherlevel-</xsl:text>
					<xsl:value-of select="@otherlevel"/>
				</xsl:if>
			</xsl:attribute>
			<!-- <xsl:call-template name="display-label"/> -->
			<xsl:if test="$isDebug"><xsl:message>		Requesting childs templates:<xsl:for-each select="*"><xsl:value-of select="concat('&#10;&#09;&#09;&#09;',position(), '- ',name())"/><xsl:if test="@pleade:id"> (id=<xsl:value-of select="@pleade:id"/>)</xsl:if><xsl:if test="position()!=last()"> ; </xsl:if></xsl:for-each><xsl:text>&#10;</xsl:text></xsl:message></xsl:if>
			<xsl:apply-templates />
			<xsl:if test="$isDebug"><xsl:message>		Ending childs templates</xsl:message></xsl:if>
		</div>
	</xsl:template>

	<!--ANCESTORS-->
	<xsl:template name="make-ancestors-block-static">
		<xsl:if test="ancestor::*[@pleade:component = 'true' and @pleade:break = 'true']">
			<div class="pl-pgd-ancestors">
				<xsl:apply-templates select="ancestor::*[@pleade:component = 'true' and @pleade:break = 'true'][position() = last()]" mode="ancestors-link">
					<xsl:with-param name="current-el" select="."/>
					<xsl:with-param name="i" select="number(1)"/>
				</xsl:apply-templates>
			</div>
		</xsl:if>
	</xsl:template>


	<xsl:template name="make-ancestors-block-dyn">
		<xsl:if test="preceding-sibling::pleade:ancestors/pleade:ancestor[@pleade:component='true']">
			<div class="pl-pgd-ancestors">&#xA0;
				<xsl:apply-templates select="preceding-sibling::pleade:ancestors/pleade:ancestor[1]" mode="ancestors-link-dyn" />
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="pleade:ancestor" mode="ancestors-link-dyn">
		<xsl:choose>
			<xsl:when test="@pleade:component='true'">
				<ul>
					<xsl:call-template name="display-link"/>
					<xsl:apply-templates select="following-sibling::pleade:ancestor[1]" mode="ancestors-link-dyn" />
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="following-sibling::pleade:ancestor[1]" mode="ancestors-link-dyn" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*" mode="ancestors-link" />

	<xsl:template match="*[@pleade:component = 'true' and @pleade:break = 'true']" mode="ancestors-link">
		<xsl:param name="current-el" select="/"/>
		<xsl:param name="i" select="number(0)"/>
		<ul>
			<xsl:call-template name="display-link"/>
			<!--<xsl:if test="$current-el:ancestor::*[@pleade:component = 'true' and @pleade:break = 'true'][position() = last() - $i]">-->
			<xsl:apply-templates select="$current-el/ancestor::*[@pleade:component = 'true' and @pleade:break = 'true'][position() = last() - $i]" mode="ancestors-link">
				<xsl:with-param name="current-el" select="$current-el"/>
				<xsl:with-param name="i" select="$i + 1"/>
			</xsl:apply-templates>
		</ul>
	</xsl:template>

	<!--CHILDREN-->
	<xsl:template name="make-children-block-static">
		<div class="pl-pgd-children">
			<xsl:apply-templates select="dsc" mode="child-link"/> <!--s'il y a un dsc il sera toujours avant les c d'après la dtd -->
			<xsl:if test="c | c01 | c02 | c03 | c04 | c05 | c06 | c07 | c08 | c09 | c10 | c11 | c12">
				<ul>
					<xsl:apply-templates select="c | c01 | c02 | c03 | c04 | c05 | c06 | c07 | c08 | c09 | c10 | c11 | c12" mode="child-link"/>
				</ul>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template name="make-children-block-dyn">
		<xsl:if test="$isDebug"><xsl:message>		Traite make-children-block-dyn</xsl:message></xsl:if>
		<div class="pl-pgd-children pl-pgd-sect">
			<xsl:if test=".//pleade:subdoc-link/*"> <!--FIXME : des dsc peuvent s'intercaller ici ! mais ce n'est pas facile à traiter (ils ne sont pas sous subdoc-link...)-->
				<p class="pl-pgd-head"><i18n:text key="ead.display.subdocs.head"/></p>
				<ul>
					<xsl:if test="$isDebug"><xsl:message>			Requesting child-link-dyn</xsl:message></xsl:if>
					<xsl:apply-templates select=".//pleade:subdoc-link/*[self::c or self::div]" mode="child-link-dyn"/>
				</ul>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="pleade:subdoc-link/*[self::c or self::div]" mode="child-link-dyn">
		<xsl:call-template name="display-link">
			<xsl:with-param name="eadid" select="/sdx:document/pleade:subdoc/@pleade:eadid"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="dsc" mode="child-link">
	<!--	( (head?, (address|chronlist|list|note|table|blockquote|p)*),  	(	(	( thead?, ( (c,thead?)+ | (c01,thead?)+ ) ) | dsc* )	)	 )	-->
		<!--	partie obligatoire du pattern (le reste = facultatif) : ( (c,thead?)+ | (c01,thead?)+) ) | dsc*
			=> soit on a un ou plusieurs dsc enfants, soit un ou plusieurs enfants c, soit un ou plusieurs enfants c01 !		-->
		<div class="pl-ead-dsc">
			<xsl:call-template name="display-label"/>
			<xsl:apply-templates select="address | chronlist | list | note | table | blockquote | p" />
			<xsl:if test="c | c01">
				<ul>
					<xsl:apply-templates select="c | c01" mode="child-link"/>
				</ul>
			</xsl:if>
			<xsl:apply-templates select="dsc" mode="child-link"/>
				<!--seulement l'un des deux apply-templates sera executé compte tenu de la dtd cf. ci-dessus-->
		</div>
	</xsl:template>

	<xsl:template match="c/thead | c01/thead | c02/thead | c03/thead | c04/thead | c05/thead | c06/thead | c07/thead | c08/thead | c09/thead | c10/thead | c11/thead | c12/thead" mode="child-link">
		<!--(row+)-->
		<!--FIXME : non supporté pour l'instant--></xsl:template>

	<xsl:template match="c | c01 | c02 | c03 | c04 | c05 | c06 | c07 | c08 | c09 | c10 | c11 | c12" mode="child-link">
		<!--archdsc ne peux pas être un ENFANT d'un composant car c'est le composant de plus haut niveau-->
		<xsl:call-template name="display-link"/>
	</xsl:template>

	<xsl:template name="display-link">
		<xsl:param name="eadid" select="''"/>
		<xsl:variable name="url">
			<xsl:choose>
				<xsl:when test="$eadid != ''">
					<xsl:text>?id=</xsl:text>
					<xsl:value-of select="$eadid"/>
					<xsl:text>&amp;c=</xsl:text>
					<xsl:value-of select="@pleade:id"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>javascript:void(0)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--utilisation de $qid qui est déclarée en variable globale dans ead2html.xsl, ead2html-index.xsl-->
		<li>
			<xsl:choose>
				<xsl:when test="$mode = 'static'">
					<a href="{$url}" onclick="eadWindow.getContentManager().loadContent('{@pleade:id}', false, '{$qid}');return false;" title="Afficher cette unité de description">
						<xsl:value-of select="@pleade:title"/>
					</a>
				</xsl:when>
				<xsl:when test="$mode = 'dyn'">
				<!--todo-->
					<a href="{$url}" onclick="eadWindow.getContentManager().loadContent('{@pleade:id}', false, '{$qid}');return false;" title="Afficher cette unité de description">
						<xsl:value-of select="@pleade:title"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="error">
						<xsl:with-param name="key" select="'ead-error-mode-value-unrecognized'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</li>
	</xsl:template>

	<xsl:template match="sdx:hilite">
		<span class="pl-hilite"><xsl:value-of select="."/></span>
	</xsl:template>


</xsl:stylesheet>
