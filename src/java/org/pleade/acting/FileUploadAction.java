/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.acting;

import java.io.File;
import java.util.Enumeration;
import java.util.Map;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.acting.ServiceableAction;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.Redirector;
import org.apache.cocoon.environment.Request;
import org.apache.cocoon.environment.SourceResolver;
import org.apache.cocoon.servlet.multipart.PartOnDisk;

/**
 * Cette action permet de definir des variables de sitemap pour une gestion
 * ulterieure des fichiers charges avec des formulaires.
 *
 */
public class FileUploadAction extends ServiceableAction implements Configurable {

    /** Le prefixe de la variable pour l'URL */
    public static final String VARIABLE_URL_PREFIX = "url-";

    /** Le prefixe de la variable pour le chemin */
    public static final String VARIABLE_PATH_PREFIX = "path-";

    /**
     * Aucune configuration pour cette action assez generique.
     */
    public void configure(Configuration conf) throws ConfigurationException {
    }

    /**
     * Ajoute des variables de sitemap, url-{request-param} et path-{request-param} pour
     * chaque fichier telecharge.
     */
    public Map act(Redirector redirector, SourceResolver resolver, Map objectModel, String src, Parameters parameters) throws Exception {

        // La requete
        Request request = ObjectModelHelper.getRequest(objectModel);

        // On boucle sur tous les parametres
        Enumeration params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String name = (String)params.nextElement();
            Object value = request.get(name);
            if ( value instanceof PartOnDisk ) {
                File f = ((PartOnDisk)value).getFile();
                objectModel.put(VARIABLE_PATH_PREFIX + name, f.getAbsolutePath());
                //System.out.println(VARIABLE_PATH_PREFIX + name + " = " + f.getAbsolutePath());
                objectModel.put(VARIABLE_URL_PREFIX + name, f.toURL().toExternalForm());
                //System.out.println(VARIABLE_URL_PREFIX + name + " = " + f.toURL().toExternalForm());
            }
        }
        // On retourne les eventuels parametres ajoutes
        return objectModel;
    }

}
