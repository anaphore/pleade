<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="saxon fn xs xsl"
>

	<!-- XSLT qui effectue la dernière étape de la publication, soit fournir les
			différents documents et champs à SDX.
			ATTENTION : on ne peut pas modifier le champ root-id
	-->

	<!-- TODO: documents attachés unitid? -->

	<!-- Fonctions d'utilités générales -->
	<xsl:import href="../../../../commons/xsl/functions.xsl"/>
	<!-- Fonctions de publication utiles -->
	<xsl:import href="publication-helpers.xsl"/>

	<!-- Le code lié à l'identification de l'entrée de cadre de classement -->
	<xsl:include href="cdc.xsl"/>

	<!-- Un fichier externe pour faciliter les modifications locales -->
	<xsl:include href="sdx-local.xsl"/>

	<!-- Ce paramètre est envoyé par SDX (peut ne pas exister, si on indexe un DOM par exemple) -->
	<xsl:param name="docUrl" select="''"/>

	<!-- La liste des documents EAD qui ne doivent pas être retournés en résulat de recherche -->
	<xsl:param name="ead-docs-not-searchable" select="''"/>
	
	<xsl:param name="debug" select="'false'"/>

	<!-- Un pointeur sur le fichier de propriétés de publication -->
	<!--<xsl:variable name="properties-url" select="concat('cocoon://functions/ead/get-properties/', /ead/@pleade:id, '/publication.xml')"/>
	<xsl:variable name="pub-properties" select="if(doc-available($properties-url)) then document($properties-url)/properties else ()"/>-->

	<!-- Doit-on prendre en compte les illusatrations à l'indexation ? -->
	<xsl:variable name="show-illustrated" select="if($pub-properties/property[@name='show-illustrated'] != '') then xs:boolean($pub-properties/property[@name='show-illustrated'])
																										else false()"/>

	<!-- La liste des index globaux à sortir -->
	<xsl:variable name="global-index-property" select="fn:tokenize($pub-properties/property[@name='index-global'], '\s')"/>

	<!-- Un pointeur sur la liste des champs de recherche -->
	<xsl:variable name="fieldlist" select="document('cocoon://conf/ead-fieldlist.xconf')/sdx:fieldList"/>

	<!-- Un pointeur sur la liste des définitions d'index -->
	<xsl:variable name="index-definitions" select="if(doc-available('cocoon://conf/ead-index.xconf')) then document('cocoon://conf/ead-index.xconf')/* else ()"/>

	<!-- L'identifiant du document racine -->
	<xsl:variable name="eadid" select="/ead/@pleade:id"/>

	<!-- Le titre du document racine -->
	<xsl:variable name="ead-title" select="/ead/@pleade:titleproper"/>

	<!-- Une fonction qui retourne la valeur d'une date normalisée -->
	<xsl:function name="pleade:get-normal-date" as="xs:string">
		<xsl:param name="el"/>
		<xsl:choose>
			<xsl:when test="$el/@normal"><xsl:value-of select="normalize-space($el/@normal)"/></xsl:when>
			<!-- Cas des intervalles de dates non normalisés écrits sous la forme YYYY-YYYY : on les normalise pour l'indexation sous la forme YYYY/YYYY -->
			<xsl:when test="fn:matches($el, '^[0-9]{4}-[0-9]{4}')"><xsl:value-of select="translate($el, '-', '/')"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="normalize-space($el)"/></xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- Une fonction qui permet de savoir si un élément est un fragment -->
	<!-- FIXME : cette fonction est aussi dans publication-helpers.xsl -->
	<xsl:function name="pleade:is-fragment" as="xs:boolean">
		<xsl:param name="element"/>
		<xsl:value-of select="boolean($element/@pleade:break and $element/@pleade:break = 'true')"/>
	</xsl:function>

	<!-- Un pointeur sur le fichier EAD du cadre de classement -->
	<xsl:variable name="cdc" select="document('cocoon://functions/ead/cdc.xml')/*"/>

	<!-- L'entrée de cadre de classement pour ce document (et donc toutes ses unités -->
	<xsl:variable name="cdc-entry-id">
		<xsl:call-template name="get-cdc-entry-id">
			<xsl:with-param name="cdc" select="$cdc"/>
			<xsl:with-param name="eadid" select="$eadid"/>
			<xsl:with-param name="url" select="$docUrl"/>
			<xsl:with-param name="doc" select="/ead"/>
		</xsl:call-template>
	</xsl:variable>

	<!-- Les champs liés aux subsets, valables pour toutes les unités -->
	<xsl:variable name="subset-fields">
		<xsl:call-template name="prepare-fields-subsets"/>
	</xsl:variable>

	<!-- La valeur par défaut pour indiquer si le document est cherchable ou pas -->
	<xsl:variable name="doc-searchable">
		<xsl:choose>
			<xsl:when test="fn:tokenize($ead-docs-not-searchable, '\s')[. = $eadid]">0</xsl:when>
			<!-- Non cherchable lorsque l'entrée correspondante du CDC est à altrender="hidden" -->
			<xsl:when test="pleade:cdc-entry-hidden($cdc, $cdc-entry-id)">0</xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- TODO: retirer une fois les développements effectués -->
	<!--<xsl:variable name="start-time" select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/>
	<xsl:variable name="start-time-milli" select="date:getTime(date:new())" xmlns:date="java:java.util.Date"/>-->

	<!-- On débute le traitement à la racine du document, soit /ead -->
	<xsl:template match="/ead">

		<xsl:message>	Production des informations pour l'indexation SDX (etape 11 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:message>		Identifiant du document : <xsl:value-of select="$eadid" /></xsl:message>
		<xsl:message>		Entrée du cadre de classement : <xsl:value-of select="$cdc-entry-id" /></xsl:message>
		
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result">
					<xsl:apply-templates select="." mode="do-it"/>
				</xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{@pleade:id}/{@pleade:id}-11.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="." mode="do-it"/>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template match="ead" mode="do-it">
		<!-- Sortie du document SDX principal -->
		<sdx:document id="{@pleade:id}">

			<!-- Le type de document -->
			<sdx:field name="type">ead</sdx:field>
			<!-- Un champ pour indiquer que l'élément a des descendants qui sont illustrés -->
			<xsl:if test="xs:boolean($show-illustrated)">
				<xsl:if test="pleade:has-object-descendant(.)">
					<sdx:field name="ancobject">1</sdx:field>
				</xsl:if>
			</xsl:if>

			<!-- Sortie des champs spécifiques au document racine -->
			<xsl:call-template name="output-fields-root"/>

			<!-- On poursuit le traitement normal -->
			<xsl:apply-templates select="*" mode="main"/>

		</sdx:document>
	</xsl:template>

	<!-- Sortie des champs spécifiques au document EAD racine (contexte: /ead) -->
	<xsl:template name="output-fields-root">

		<!-- Un champ qui indique que c'est un document racine -->
		<sdx:field name="top">1</sdx:field>

		<!-- Un champ pour associer ce document à la bonne entrée du cadre de classement -->
		<sdx:field name="cdcroot"><xsl:value-of select="$cdc-entry-id"/></sdx:field>

		<!-- Le champ pos -->
		<sdx:field name="pos"><xsl:value-of select="@pleade:pos"/></sdx:field>

		<!-- Dates et cotes, pour le tri FIXME: doit-on traiter le cas où archdesc n'a pas de date ou de cote ? -->
		<xsl:apply-templates select="(archdesc/did/unitid)[1]" mode="brief"/>
		<xsl:apply-templates select="(archdesc/did//unitdate)[1]" mode="brief"/>
		<!-- FIXME: doit-on également indexer le champ unitid? -->

		<!-- Les rubriques -->
		<xsl:copy-of saxon:read-once="yes" select="$subset-fields/sdx:field"/>

		<!-- L'eadid -->
		<sdx:field name="root-id"><xsl:value-of select="$eadid"/></sdx:field>

    <!-- FIXME: vérifier que l'indexation dans le champ unittitle ici ne perturbe pas la recherche de base ; on a besion d'indexer le titre en word pour pouvoir "filtrer" dans la liste des documents publiés -->
    <sdx:field name="unittitle"><xsl:value-of select="$ead-title"/></sdx:field>

		<!-- Le titre propre et le titre de l'unité de description -->
		<sdx:field name="fatitle"><xsl:value-of select="$ead-title"/></sdx:field>
		<sdx:field name="fucomptitle"><xsl:value-of select="$ead-title"/></sdx:field>

		<!--  Si le document est cherchable ou pas: toujours non pour la racine -->
		<sdx:field name="searchable">0</sdx:field>

	</xsl:template>

	<!-- Traitement des éléments en mode main, donc parcours général du document. -->
	<xsl:template match="*" mode="main">
		<!-- Seuls les éléments qui sont des fragments nous intéressent ici -->
		<xsl:choose>
			<xsl:when test="@pleade:break = 'true'">
				<sdx:document id="{@pleade:id}">
					<!-- Le type de document -->
					<sdx:field name="type">component</sdx:field>
					<!-- [JC] L'identifiant du parent -->
					<xsl:if test="ancestor::*[@pleade:break = 'true']">
						<sdx:field name="parent_id">
							<xsl:value-of select="ancestor::*[1]/@pleade:id"/>
						</sdx:field>
					</xsl:if>
					<!-- [JC] Les liens pour les dao et daoloc -->
					<xsl:for-each select="dao | daoloc | daogrp/dao | daogrp/daoloc">
						<sdx:field name="dao_href">
							<xsl:value-of select="@href"/>
						</sdx:field>
            <sdx:field name="navcotes">
              <xsl:value-of select="concat(/ead/eadheader/eadid, '|', format-number(count(parent::c/preceding::c[dao or daoloc])+1, '0000000'), '|', @href, '|', parent::c/did/unitid[1])"/>
            </sdx:field>
					</xsl:for-each>
					<xsl:if test="xs:boolean($show-illustrated)">
						<!-- Un champ pour indiquer que l'élément a des descendants qui sont illustrés -->
						<xsl:if test="pleade:has-object-descendant(.)">
							<sdx:field name="ancobject">1</sdx:field>
						</xsl:if>
					</xsl:if>
					<xsl:call-template name="process-fragment"/>
					<xsl:apply-templates select="*" mode="main"/>
				</sdx:document>
			</xsl:when>
			<xsl:otherwise>
				<!-- Pas un composant, on poursuit -->
				<xsl:apply-templates select="*" mode="main"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement d'un fragment (contexte: c, archdesc, ...) -->
	<xsl:template name="process-fragment">
		<!-- Le contenu XML pour ce fragment -->
		<pleade:subdoc pleade:eadid="{$eadid}">
			<!-- On copie les attributs -->
			<xsl:apply-templates select="@*" mode="subdoc"/>
			<!-- On insère les ancêtres -->
			<xsl:call-template name="output-ancestors" />
			<!-- On sort le contenu lui-même, jusqu'au prochain fragment -->
			<c pleade:original-element="{local-name()}">
				<xsl:apply-templates select="node()|@*" mode="subdoc"/>
			</c>
		</pleade:subdoc>

		<!-- Tous les champs qui ne sont pas spécifiquement liés à la recherche -->
		<!-- Les champs liés au cdc -->
		<xsl:call-template name="output-fields-cdc"/>

		<!-- La position -->
		<sdx:field name="pos"><xsl:value-of select="@pleade:pos"/></sdx:field>

		<!-- Les informations sur la racine : id et titre -->
		<sdx:field name="root-id"><xsl:value-of select="$eadid"/></sdx:field>
		<sdx:field name="fatitle"><xsl:value-of select="$ead-title"/></sdx:field>

		<!-- Le titre de l'unité -->
		<sdx:field name="fucomptitle"><xsl:value-of select="@pleade:title"/></sdx:field>

		<!-- Le niveau archivistique -->
		<xsl:if test="normalize-space(@level) != ''">
			<sdx:field name="level"><xsl:value-of select="normalize-space(@level)"/></sdx:field>
		</xsl:if>
		<xsl:if test="normalize-space(@otherlevel) != ''">
			<sdx:field name="level"><xsl:value-of select="normalize-space(@otherlevel)"/></sdx:field>
		</xsl:if>

		<!-- La cote et la date pour le tri -->
		<xsl:apply-templates select="(did/unitid)[1]" mode="brief"/>
		<xsl:apply-templates select="(did//unitdate)[1]" mode="brief"/>

		<!-- Les identifiants et titres des ancêtres, pour les résultats -->
		<xsl:for-each select="ancestor::*[@pleade:component]">
			<!-- Le titre -->
			<sdx:field name="ctitle"><xsl:value-of select="concat(position(), '-', @pleade:title)"/></sdx:field>
			<!-- L'identifiant -->
			<sdx:field name="cid"><xsl:value-of select="concat(position(), '-', @pleade:id)"/></sdx:field>
		</xsl:for-each>

		<!-- Si l'unité est cherchable ou non -->
		<sdx:field name="searchable">
			<xsl:choose>
				<xsl:when test="$doc-searchable = 0">0</xsl:when>
				<xsl:when test="@pleade:notsearchable = 'true'">0</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</sdx:field>
		
		<!-- A la fin, on appelle un template vide par défaut, mais qui peut être surchargé -->
		<xsl:call-template name="local-process-fragment"/>

		<!-- Les autres champs, mais uniquement si on est en external -->

		<xsl:if test="pleade:is-external(.)">
			<!-- On va conserver le contenu du fragment, pour le réutiliser -->
			<xsl:variable name="content">
				<!-- Le contenu dans l'élément -->
				<xsl:copy>
					<xsl:apply-templates select="@* | node()" mode="content"/>
				</xsl:copy>
			</xsl:variable>
			
			<!-- Le contenu vers le haut si parent non cherchable -->
			<xsl:variable name="content-up">
				<xsl:apply-templates select="parent::*" mode="content-up"/>
			</xsl:variable>

			<!-- Sortie du texte intégral -->
			<sdx:field name="fulltext">
				<xsl:apply-templates select="$content" mode="fulltext"/>
				<xsl:apply-templates select="$content-up" mode="fulltext"/>
			</sdx:field>

			<!-- On va sortir tous les champs que l'on obtient depuis le contenu -->
			<xsl:apply-templates select="$content//*" mode="content-fields">
				<xsl:with-param name="ComponentHasAncestor" select="exists(ancestor::*[@pleade:component])"/>
				<xsl:with-param name="ComponentHasSubcomponent" select="exists(c[@pleade:component])"/>
			</xsl:apply-templates>
			<!-- On va sortir tous les champs que l'on obtient depuis le contenu hérité des unités non cherchables parentes -->
			<xsl:apply-templates select="$content-up//*" mode="content-fields">
				<xsl:with-param name="mode" select="'content-up'"/>
			</xsl:apply-templates>

			<!-- Les champs liés aux rubriques -->
			<xsl:copy-of saxon:read-once="yes" select="$subset-fields/sdx:field"/>
		</xsl:if>

	</xsl:template>

	<!-- Origination, au contenu particulier -->
	<xsl:template name="output-field-origination">
		<xsl:param name="el" select="."/>
		<xsl:if test="$el and pleade:is-external($el)">
			<xsl:choose>
				<xsl:when test="$el/persname or $el/corpname or $el/famname">
					<!-- On indexe chaque terme -->
					<xsl:for-each select="$el/persname | $el/corpname | $el/famname">
						<xsl:variable name="value">
							<xsl:choose>
								<xsl:when test="@normal != ''"><xsl:value-of select="normalize-space(@normal)"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="normalize-space(.)"/></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<sdx:field name="forigination"><xsl:value-of select="$value"/></sdx:field>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<!-- On indexe le contenu -->
					<sdx:field name="forigination"><xsl:value-of select="normalize-space(.)"/></sdx:field>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Dans tous les cas on sort le contenu dans le champ word -->
			<xsl:apply-templates select="$el" mode="fulltext"/>
		</xsl:if>
	</xsl:template>

	<!-- Date ou cote en brief -->
	<xsl:template match="unitid" mode="brief">
		<sdx:field name="uunitid">
			<xsl:value-of select="pleade:format-unitid(.)"/>
		</sdx:field>
	</xsl:template>
	<xsl:template match="unitdate" mode="brief">
		<sdx:field name="udate"><xsl:value-of select="normalize-space(.)"/></sdx:field>
	</xsl:template>

	<!-- Préparation des champs liés aux rubriques -->
	<xsl:template name="prepare-fields-subsets">
		<xsl:variable name="prop" select="string($pub-properties/property[@name='subset-list'])"/>
		<xsl:if test="$prop">
			<xsl:for-each select="fn:tokenize($prop, '\s|,')">
				<xsl:variable name="subset-id" select="normalize-space(.)"/>
				<sdx:field name="subset"><xsl:value-of select="$subset-id"/></sdx:field>
				<xsl:for-each select="document('cocoon://functions/get-subset-hierarchy.xml')//subset[@id = $subset-id]/ancestor-or-self::subset">
					<sdx:field name="subsetall"><xsl:value-of select="@id"/></sdx:field>
				</xsl:for-each>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<!-- Sortie des champs liés au cdc -->
	<xsl:template name="output-fields-cdc">
		<xsl:if test="$cdc-entry-id != ''">
			<sdx:field name="cdc"><xsl:value-of select="$cdc-entry-id"/></sdx:field>
			<xsl:for-each select="$cdc//*[@pleade:id = $cdc-entry-id]/ancestor-or-self::*[@pleade:component = 'true']">
				<xsl:variable name="id" select="@pleade:id"/>
				<sdx:field name="cdcall"><xsl:value-of select="@pleade:id"/></sdx:field>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<!-- Un élément faisant partie du contenu d'un fragment -->
	<xsl:template match="*" mode="content-fields">
		<xsl:param name="ComponentHasAncestor"/>
		<xsl:param name="ComponentHasSubcomponent"/>
		<xsl:param name="mode" select="'content'"/> <!-- paramètre pour savoir si on traite du contenu (content) ou du contenu hérité vers du haut (content-up) -->
		<!-- Uniquement si external -->
		<xsl:if test="pleade:is-external(.)">
			<!-- Le nom de l'élément -->
			<xsl:variable name="element-name" select="local-name()"/>
			<!-- On vérifie s'il y a un champ word à ce nom, si oui on le traite -->
			<xsl:apply-templates select="$fieldlist/sdx:field[@name = $element-name and @type='word']" mode="word">
				<xsl:with-param name="content" select="."/>
			</xsl:apply-templates>
			<!-- Idem pour un champ sur le nom précédé de w -->
			<xsl:apply-templates select="$fieldlist/sdx:field[@name = concat('w', $element-name) and @type='word']" mode="word">
				<xsl:with-param name="content" select="."/>
			</xsl:apply-templates>
			<!-- Un champ de type field à son nom -->
			<xsl:apply-templates select="$fieldlist/sdx:field[@name = $element-name and @type = 'field' and not(@mode and @mode = 'index')]" mode="field">
				<xsl:with-param name="content" select="."/>
			</xsl:apply-templates>
			<!-- Idem mais avec un f devant -->
			<xsl:apply-templates select="$fieldlist/sdx:field[@name = concat('f', $element-name) and @type = 'field' and not(@mode and @mode = 'index')]" mode="field">
				<xsl:with-param name="content" select="."/>
			</xsl:apply-templates>
			<!-- Les différents traitements liés aux ressources externes -->
			<!-- Un champ pour les unités illustrées -->
			<!-- [CB] ces infos sont liées à une unité de description et n'ont pas à être héritées -->
			<xsl:if test="not($mode = 'content-up')">
				<xsl:if test="pleade:is-object(.)">
					<sdx:field name="object">
						<xsl:text>1</xsl:text> <!-- Pour la compatibilité descendante, on ajoute systématiquement "1"  ici -->
						<xsl:if test="xs:boolean($show-illustrated)">
							<xsl:text>::</xsl:text>
							<xsl:value-of select="pleade:object-type(.)"/>
						</xsl:if>
					</sdx:field>
				</xsl:if>
				<!-- Un champ pour les unités avec un document associé FIXME: cas des .xml ?-->
				<xsl:if test="@pleade:link-type = 'attached' or @pleade:link-type = 'relative'">
					<sdx:field name="att">1</sdx:field>
				</xsl:if>
				<!-- On attache les documents qui doivent l'être -->
				<xsl:if test="@pleade:link-type = 'attached'">
					<sdx:attachedDocument id="{@pleade:id}" url="{@pleade:file}" mimetype="{@pleade:mime-type}"/>
				</xsl:if>
				<!-- On indexe le contenu de ceux qui peuvent l'être -->
				<xsl:if test="(@pleade:link-type = 'attached' or @pleade:link-type = 'relative' or @pleade:link-type='detached') and @pleade:mime-type = 'application/pdf'">
					<!-- On va d'abord vérifier si le fichier existe -->
					<xsl:if test="netutils:fileExists(@pleade:file)" xmlns:netutils="java:org.pleade.utils.NetUtilities">
						<sdx:field name="ext">
							<xsl:apply-templates select="document(concat('cocoon://module-eadeac/functions/extract-pdf.xml?file=', @pleade:file))/extract-pdf/pdf-content" mode="fulltext"/>
						</sdx:field>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<!-- Les champs liés à des index -->
			<xsl:if test="@pleade:index-id">
				<!-- Le terme lui-même -->
				<xsl:variable name="value" select="@pleade:value"/>
				<!-- On boucle sur les identidiants d'index dont fait partie cet élément -->
				<!-- <xsl:for-each select="fn:tokenize(@pleade:index-list, ',')"> -->
					<!-- <xsl:variable name="index-id" select="."/> -->
					<xsl:variable name="index-id" select="@pleade:index-id"/>
					<xsl:variable name="index" select="$index-definitions/index[@id = $index-id]"/>
					<!-- Maintenant on sort les champs souhaités -->
					<!-- [CB] Pas d'héritage content-up (contenu des unités non cherchables) pour les index locaux -->
					<xsl:apply-templates select="if ($mode = 'content-up') then $index/@*[ends-with(local-name(), 'sdxfield') and local-name() != 'local-sdxfield'] else $index/@*[ends-with(local-name(), 'sdxfield')]" mode="index">
						<xsl:with-param name="value" select="$value"/>
						<xsl:with-param name="index-id" select="$index-id"/>
					</xsl:apply-templates>
				<!-- </xsl:for-each> -->
			</xsl:if>
			<!-- Les dates -->
			<xsl:if test="self::unitdate">
				<xsl:variable name="date" select="pleade:get-normal-date(.)"/>
				<xsl:variable name="parts" select="fn:tokenize($date, '/')"/>
				<xsl:variable name="b" select="$parts[1]"/>
				<xsl:variable name="e">
					<xsl:choose>
						<xsl:when test="$parts[2]"><xsl:value-of select="$parts[2]"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="$b"/></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="bDate" select="dateutils:format(dateutils:parse($b))" xmlns:dateutils="java:org.pleade.utils.DateUtilities"/>
				<xsl:if test="$bDate">
					<sdx:field name="bdate"><xsl:value-of select="$bDate"/></sdx:field>
				</xsl:if>
				<xsl:variable name="eDate" select="dateutils:format(dateutils:parse($e))" xmlns:dateutils="java:org.pleade.utils.DateUtilities"/>
				<xsl:if test="$eDate">
					<sdx:field name="edate"><xsl:value-of select="$eDate"/></sdx:field>
				</xsl:if>
			</xsl:if>
			<!-- La première année communicable -->
			<xsl:if test="self::accessrestrict[@type='premiere-annee-communicable']">
				<xsl:if test="p">
					<xsl:variable name="d" select="normalize-space(p[1])"/>
					<sdx:field name="pacomm"><xsl:value-of select="$d"/></sdx:field>
					<sdx:field name="pacommd"><xsl:value-of select="$d"/></sdx:field>
				</xsl:if>
			</xsl:if>
			<!-- A la fin, on appelle un template vide par défaut, mais qui peut être surchargé -->
			<xsl:call-template name="local-content-fields">
				<xsl:with-param name="ComponentHasAncestor" select="$ComponentHasAncestor"/>
				<xsl:with-param name="ComponentHasSubcomponent" select="$ComponentHasSubcomponent"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- Sortie des champs liés aux index -->
	<xsl:template match="@local-sdxfield" mode="index">
		<xsl:param name="value"/>
		<!-- On ajoute un attribut pour les retrouver dans l'étape suivante -->
		<sdx:field name="{.}" pleade:type="{local-name()}"><xsl:value-of select="$value"/></sdx:field>
	</xsl:template>
	<xsl:template match="@global-sdxfield | @word-sdxfield" mode="index">
		<xsl:param name="value"/>
		<xsl:param name="index-id"/>
		<xsl:if test="$global-index-property[. = '_all' or . = $index-id]">
			<!-- On ajoute un attribut pour les retrouver dans l'étape suivante -->
			<sdx:field name="{.}" pleade:type="{local-name()}"><xsl:value-of select="$value"/></sdx:field>
		</xsl:if>
	</xsl:template>

	<!-- Un champ word à sortir -->
	<xsl:template match="sdx:field" mode="word">
		<xsl:param name="content"/>
		<sdx:field name="{@name}">
			<xsl:apply-templates select="$content" mode="fulltext"/>
		</sdx:field>
	</xsl:template>

	<!-- Un champ field à sortir -->
	<xsl:template match="sdx:field" mode="field">
		<xsl:param name="content"/>
		<xsl:variable name="value" select="normalize-space($content)"/>
		<xsl:if test="$value != ''">
			<sdx:field name="{@name}"><xsl:value-of select="$value"/></sdx:field>
		</xsl:if>
	</xsl:template>

	<!-- Traitement d'un noeud pour un champ word -->
	<xsl:template match="node()" mode="fulltext">
		<xsl:choose>
			<xsl:when test="self::*">
				<xsl:if test="pleade:is-external(.)">
					<xsl:text> </xsl:text>
					<xsl:apply-templates mode="fulltext"/>
				</xsl:if>
			</xsl:when>
			<xsl:when test="self::text()">
				<xsl:value-of select="."/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Sortie de la liste des ancêtres d'un fragment -->
	<xsl:template name="output-ancestors">
		<xsl:param name="element" select="."/>
		<!-- Un élément racine pour regrouper les ancêtres -->
		<pleade:ancestors pleade:eadid="{$eadid}" pleade:ead-title="{$ead-title}">
			<xsl:for-each select="$element/ancestor::*[@pleade:component]">
				<pleade:ancestor>
					<!-- On copie simplement les attributs -->
					<xsl:apply-templates select="@*" mode="subdoc"/>
				</pleade:ancestor>
			</xsl:for-each>
		</pleade:ancestors>
	</xsl:template>

	<!-- Traitement des noeuds en mode content vers le haut -->
	<xsl:template match="*" mode="content-up">
		<xsl:choose>
			<xsl:when test="self::dsc"><xsl:apply-templates select="parent::*" mode="content-up"/></xsl:when>
			<xsl:when test="@pleade:component = 'true' and @pleade:notsearchable='true'">
				<xsl:apply-templates mode="content"/>
				<xsl:apply-templates select="parent::*" mode="content-up"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement des noeuds en mode content : copie, en s'arrêtant à un nouveau fragment -->
	<!-- FIXME: nouvelle méthode, à valider -->
	<xsl:template match="node()|@*" mode="content">
		<xsl:choose>
			<!--<xsl:when test="@pleade:component = 'true'"/>-->	<!-- On arrête vers le bas -->
			<!--<xsl:when test="@pleade:break = 'true'"/>--> 	<!-- On arrête -->
      <!-- On s'arrête aux unités documentaires filles (@pleade:break='true' ou éléments forcemment unités documentaires -->
      <xsl:when test="@pleade:break='true' or xs:boolean(pu:isForcedPleadeBreak(local-name()))" xmlns:pu="java:org.pleade.utils.PleadeUtilities"/>
      <!--<xsl:when test="xs:boolean(pu:isPleadeBreak(local-name(),@pleade:*))" xmlns:pu="java:org.pleade.utils.PleadeUtilities"/>--> <!-- FIXME (MP) : Ne fonctionne pas: le second argument doit être un élément org.xml.sax.Attributes. Est-il possible de le construire en XSLT 2 ? -->
			<xsl:otherwise>
				<!-- Le reste est recopié -->
				<xsl:copy>
					<xsl:apply-templates select="node()|@*" mode="content"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement des noeuds en mode subdoc : copie, sauf s'il s'agit d'un nouveau fragment -->
	<xsl:template match="node()|@*" mode="subdoc">
		<!-- On distingue les éléments des autres noeuds -->
		<xsl:choose>
			<xsl:when test="self::*">
				<xsl:choose>
					<xsl:when test="@pleade:break = 'true'">
						<!-- On sort un lien -->
						<pleade:subdoc-link>
							<!-- [JC] L'identifiant du parent - pour mémoire et debug -->
							<xsl:if test="ancestor::*[@pleade:break = 'true']">
								<xsl:attribute name="pleade:parent-id"><xsl:value-of select="ancestor::*[1]/@pleade:id"/></xsl:attribute>
							</xsl:if>
							<xsl:copy>
								<xsl:apply-templates select="@*" mode="subdoc" />
							</xsl:copy>
						</pleade:subdoc-link>
					</xsl:when>
					<xsl:when test="@pleade:component = 'true'">
						<!-- On renomme l'élément -->
						<c pleade:original-element="{local-name()}">
							<xsl:apply-templates select="node()|@*" mode="subdoc" />
						</c>
					</xsl:when>
					<xsl:otherwise>
						<!-- Les autres sont recopiés -->
						<xsl:copy>
							<xsl:apply-templates select="node()|@*" mode="subdoc" />
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Les autres noeuds sont copiés -->
				<xsl:copy>
					<xsl:apply-templates select="node()|@*" mode="subdoc" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- FIXME: déterminer si attached Traitement des liens en mode normal : créer un document attaché si nécessaire -->
	<!-- <xsl:template match="ref|refloc|ptr|ptrloc|archref|extref|extrefloc|extptr|extptrloc">
		<xsl:if test="@pleade:attached='true'">
			<sdx:attachedDocument id="{@pleade:id}" mimetype="{pleade:get-mimetype(@href)}" url="{@href}"/>
		</xsl:if>
	</xsl:template> -->

	<!-- Traitement du eadheader -->
	<xsl:template match="eadheader" mode="main">
		<!-- Toujours une unité documentaire -->
		<sdx:document id="{@pleade:id}">
			<!-- Son contenu -->
			<pleade:subdoc pleade:eadid="{$eadid}">
				<!-- On copie les attributs -->
				<xsl:apply-templates select="@*" mode="subdoc" />
				<c pleade:original-element="eadheader">
					<xsl:apply-templates select="node()|@*" mode="subdoc"/>
				</c>
			</pleade:subdoc>
			<!-- Le type de document -->
			<sdx:field name="type">eadheader</sdx:field>
			<sdx:field name="searchable">0</sdx:field>
		</sdx:document>
	</xsl:template>

	<!-- Traitement du frontmatter -->
	<xsl:template match="frontmatter" mode="main">
		<!-- On doit créer une unité documentaire pour le frontmatter seulement
				s'il a au moins deux enfants, sinon on a directemente ses enfants.
				Ce traitement est cohérent avec celui de la table des matières. -->
		<xsl:choose>
			<xsl:when test="count(*) > 1">
				<!-- Plus d'un enfant, donc il a au moins un div, voire un titlepage, donc un crée
						un document pour qu'il pointe sur les enfants -->
				<sdx:document id="{@pleade:id}">
					<!-- Le contenu -->
					<pleade:subdoc pleade:eadid="{$eadid}">
						<!-- On copie ses attributs -->
						<xsl:apply-templates select="@*" mode="subdoc"/>
						<!-- A noter qu'il n'a pas d'ancêtre -->
						<c pleade:original-element="frontmatter">
							<xsl:apply-templates select="@*" mode="subdoc"/>
							<!-- Un lien vers chaque partie -->
							<xsl:for-each select="*">
								<pleade:subdoc-link>
									<c><xsl:apply-templates select="@*" mode="subdoc"/></c>
								</pleade:subdoc-link>
							</xsl:for-each>
						</c>
					</pleade:subdoc>
					<!-- Le type de document -->
					<sdx:field name="type">frontmatter</sdx:field>
					<sdx:field name="searchable">0</sdx:field>
					<!-- On poursuit le traitement -->
					<xsl:apply-templates select="*" mode="main"/>
				</sdx:document>
			</xsl:when>
			<xsl:otherwise>
				<!-- Un seul enfant (page de titre), donc on lui passe le traitement -->
				<xsl:apply-templates select="*" mode="main"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement de la page de titre -->
	<xsl:template match="titlepage" mode="main">
		<!-- Toujours une unité documentaire -->
		<sdx:document id="{@pleade:id}">
			<!-- Le contenu -->
			<pleade:subdoc pleade:eadid="{$eadid}">
				<!-- Les attributs sont copiés -->
				<xsl:apply-templates select="@*" mode="subdoc"/>
				<!-- Les ancêtres (en principe le frontmatter, sauf s'il n'est pas sorti) -->
				<xsl:call-template name="output-ancestors" />
				<c pleade:original-element="titlepage">
					<xsl:apply-templates select="node()|@*" mode="subdoc"/>
				</c>
			</pleade:subdoc>
			<!-- Le type de document -->
			<sdx:field name="type">titlepage</sdx:field>
			<sdx:field name="searchable">0</sdx:field>
		</sdx:document>
	</xsl:template>

	<!-- Traitement des div dans le frontmatter -->
	<xsl:template match="div" mode="main">
		<!-- Toujours une unité documentaire -->
		<sdx:document id="{@pleade:id}">
			<!-- Son contenu est copié, on s'arrête au div -->
			<pleade:subdoc pleade:eadid="{$eadid}">
				<!-- On copie les attributs -->
				<xsl:apply-templates select="@*" mode="subdoc"/>
				<!-- Les ancêtres (au moins le frontmatter) -->
				<xsl:call-template name="output-ancestors"/>
				<c pleade:original-element="div">
					<xsl:apply-templates select="node()|@*" mode="subdoc"/>
				</c>
			</pleade:subdoc>
			<!-- Le type de document -->
			<sdx:field name="type">div</sdx:field>
			<sdx:field name="searchable">0</sdx:field>
			<!-- On poursuit avec les autres div imbriqués -->
			<xsl:apply-templates select="div" mode="main"/>
		</sdx:document>
	</xsl:template>

	<!-- Les div en mode subdoc : inclure un lien et arrêter le processus -->
	<xsl:template match="div" mode="subdoc">
		<pleade:subdoc-link>
			<xsl:copy>
				<xsl:apply-templates select="@*" mode="subdoc"/>
			</xsl:copy>
		</pleade:subdoc-link>
	</xsl:template>

</xsl:stylesheet>
