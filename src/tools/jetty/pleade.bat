@echo off

REM Fichier de commande qui permet de lancer Pleade dans Jetty et ensuite de
REM lancer le navigateur Web sur la page par defaut.

REM L'exécutable Java, on le suppose dans le PATH
SET JAVA_CMD=java

REM Le port d'écoute de Jetty
SET PORT=8090

REM On se déplace dans le sous-dossier Jetty pour que les chemins
REM relatifs soient OK.
cd jetty

REM On démarre Jetty
start %JAVA_CMD% -Xmx500m -Djetty.port=%PORT% -jar start.jar etc\jetty.xml

REM Puis on lance le navigateur Web par defaut, sur une page de demarrage
start start.html

REM On revient au repertoire precedent
cd ..

