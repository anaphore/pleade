<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Affichage d'informations sur le fragment (depuis la visionneuse).
    | Par défaut, on va aller chercher le titre de l'IR ; mais il est ausis possible,
    | via le mode "extended" de récupérer plus di'nformations sous forme de tableau.
    +-->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
    xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    exclude-result-prefixes="xsl sdx xhtml">

    <xsl:param name="image_path" select="''"/>
    <xsl:param name="from" select="''"/>
    <xsl:param name="baselink" select="''"/>

    <xsl:template match="sdx:results">
      <xsl:choose>
        <xsl:when test="$from = 'pdf'">
          <!-- On cherche si la cote est déjà dans le titre Pleade de manière à na pas la répéter. -->
          <xsl:variable name="uunitid" select="sdx:result[1]/sdx:field[@name='uunitid'][1]/@value" />
          <xsl:variable name="fucomptitle" select="sdx:result[1]/sdx:field[@name='fucomptitle'][1]/@value" />
          <phrase style="bold"><xsl:if test="not(contains($fucomptitle, $uunitid))"><xsl:value-of select="sdx:result[1]/sdx:field[@name='uunitid']"/> - </xsl:if><xsl:value-of select="sdx:result[1]/sdx:field[@name='fucomptitle']/@value"/></phrase>
        </xsl:when>
        <xsl:otherwise>
          <span id="{sdx:result[1]/sdx:field[@name='sdxdocid']}"><xsl:apply-templates select="sdx:result[1]" mode="all"/></span>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

    <xsl:template match="sdx:result" mode="all">
      <xsl:variable name="href">
        <xsl:value-of select="$baselink"/>
        <xsl:text>ead.html?id=</xsl:text>
        <xsl:value-of select="sdx:field[@name='root-id']/@value"/>
        <xsl:text>&amp;c=</xsl:text>
        <xsl:value-of select="sdx:field[@name='sdxdocid']/@value"/>
      </xsl:variable>
      <xsl:variable name="onclick">
        if(window.opener){ window.opener.location.href=this.href; window.opener.focus();} else {window.location.href=this.href;}return false;
      </xsl:variable>
      <a
        href="{$href}"
        id="piv-localisation-link"
        title=""
        i18n:attr="title"
        onclick="{$onclick}">
          <xsl:value-of select="sdx:field[@name='fucomptitle']"/>
        </a>
    </xsl:template>

</xsl:stylesheet>
