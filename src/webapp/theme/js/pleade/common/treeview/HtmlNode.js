//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion de noeud HTML utilisé lors de la création de l'arbre MifTreeCustom
 * @author Julien Huet
 */
/**
 * @class Gestion de noeud HTML utilisé lors de la création de l'arbre MifTreeCustom
 * @author Julien Huet
 */
var HtmlNode = new Class(/** @lends HtmlNode.prototype */{

	/**
	* <strong>Constructor</strong><br/> Creation d'un noeud html manipulable par nos méthodes
	* @param {Element} liHtml Balise Li qui forme le htmlNode
	*/
	initialize: function(liHtml){
		this.node = liHtml;
	},

	/**
	* Récupération de l'id du noeud
	* <li id="IdNoeud">...</li>
	* @returns {String} id
	*/
	getId: function(){
		return this.node.get('id');
	},

	/**
	* Savoir si la liste contient d'autre liste
	* @returns {Boolean} hasHtmlChild
	*/
	hasHtmlChild: function(){
		return this.node.getElements('ul').length > 0;
	},

	/**
	* Récupération de la liste des enfants du noeud courant
	* @returns {Element} Retourne la balise UL contenu dans le noeud
	*/
	getHtmlChild: function(){
		if ( this.hasHtmlChild() ) {
			return this.node.getElement('ul');
		} else {
			return null;
		}
	},

	/**
	* Récupération du lien du noeud
	* @private
	* @returns {Element} Retourne la première balise a du noeud
	*/
	getNodeLink: function(){
		if ( this.node.getElement('a') ) {
			return this.node.getElement('a');
		} else {
			return '#';
		}
	},

	/**
	* Retourne l'url du lien du noeud
	* @returns {String} Retourne l'url a utiliser pour le noeud Li
	*/
	getNodeLinkUrl: function(){
		if ( this.node.getElement('a') ) {
			return this.getNodeLink().get('href');
		} else {
			return '#';
		}
	},

	/**
	* Renvoie le type de noeud
	* @returns {String} Le type de noeud
	*/
	getNodeType: function(){
		var _type = this.node.retrieve('node:type');
		if ( _type == null || _type === '' ) {
			_type = 'undefined_node'
		}
		return _type;
	},

	/**
	* Mise a jour du lien du noeud
	* @param {String} link
	*/
	setNodeLink: function(link){
		if ( this.node.getElement('a') ) {
			this.node.getElement('a').set('href', link)
		}
	},

	/**
	* Suppréssion HTML des fils du noeud courant
	*/
	eraseHtmlChild: function(){
		this.node.getElement('ul').dispose();
	},

	/**
	* Récupération du contenu du noeud courant
	*/
	getHtml: function(){
		return this.node.get('html');
	}
});