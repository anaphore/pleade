<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- XHTML output, mainly for static pages -->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xhtml"
>

	<!-- First element -->
	<xsl:template match="xhtml:html">
		<xsl:apply-templates select="xhtml:body" mode="copy"/>
	</xsl:template>

	<!-- For the body, only the content -->
	<xsl:template match="xhtml:body" mode="copy">
		<xsl:apply-templates mode="copy"/>
	</xsl:template>

	<!-- Copying elements -->
	<xsl:template match="node()|@*" mode="copy">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" mode="copy"/>
		</xsl:copy>
	</xsl:template>

	<!-- We exclude the comments -->
	<xsl:template match="comment()" mode="copy"/>

	<!-- Only process the body -->
	<xsl:template match="xhtml:*"/>

</xsl:stylesheet>
