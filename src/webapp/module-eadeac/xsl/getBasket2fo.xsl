<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices
d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Paternité : Vous devez citer le nom de l'auteur original de la manière
indiquée par l'auteur de l'oeuvre ou le titulaire des droits qui vous
confère cette autorisation (mais pas d'une manière qui suggérerait
qu'ils vous soutiennent ou approuvent votre utilisation de l'oeuvre).
Pas d'Utilisation Commerciale : Vous n'avez pas le droit d'utiliser
cette création à des fins commerciales.
Pas de Modification : Vous n'avez pas le droit de modifier, de
transformer ou d'adapter cette création.
-->
<!--+
    | Retourne les documents EAD contenus dans le porte-documents au format FO
    |
    | On ne prend en compte qu'une seule base de documents. A charge de
    | l'appelant de sélectionner la bonne base. Pour ce faire, il peut parser
    | sdx:caddy et compter le nombre de base différentes.
    +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
                xmlns:xsp="http://apache.org/xsp"
                xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
                xmlns:pleade="http://pleade.org/ns/pleade/1.0"
								xmlns="http://www.w3.org/1999/XSL/Format"
								xmlns:df="java.text.DateFormat"
								xmlns:sdf="java.text.SimpleDateFormat"
								xmlns:date="java.util.Date"
                exclude-result-prefixes="xsl xsp sdx i18n pleade df sdf date">

	<!-- La définition des styles est dans un fichier à part -->
	<xsl:import href="ead-xslt/fo/html2fo-style.xsl"/>

	<!-- La sortie de l'en-tête est dans un fichier à part : on reprend le même que pour celui des documents EAD -->
	<xsl:import href="ead-xslt/fo/header.xconf"/>

	<!-- Fonctions d'utilités générales -->
	<xsl:import href="../../commons/xsl/functions.xsl"/>
	
	<!-- URL interne de l'application qui permet de reconstituer l'URL absolue d'accès aux images -->
	<xsl:param name="url-interne"/>

	<xsl:param name="format"/>

	<xsl:variable name="formatDate1" select="'dd/MM/yyyy'"/>

	<!-- L'élément racine qui est toujours un document XML car on part
			de l'URL getBasket.xml -->
	<xsl:template match="resultSet">
		<root>
			<!-- La définition des pages -->
			<layout-master-set>
				<!-- Une page "normale" TODO: dans les styles-->
				<simple-page-master master-name="normal" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1.5cm">
					<!-- Le corps de la page -->
					<region-body margin-top="1.3cm" margin-bottom="1.3cm"/>
					<!-- L'espace pour l'en-tête -->
					<region-before extent="1cm"/>
					<!-- L'espace pour le pied de page -->
					<region-after extent="1cm"/>
				</simple-page-master>
			</layout-master-set>
			<!-- On démarre une séquence de pages pour le contenu principal -->
			<page-sequence master-reference="normal">
				<!-- L'en-tête -->
				<static-content flow-name="xsl-region-before">
					<!-- On appelle un template qui est dans une XSLT à part -->
					<xsl:call-template name="output-header"/>
				</static-content>
				<!-- Le pied de page -->
				<static-content flow-name="xsl-region-after">
					<!-- On ne peut pas reprendre le même pied de page que pour les docuements EAD -->
					<xsl:call-template name="output-footer"/>
				</static-content>
				<!-- Le corps du document -->
				<flow flow-name="xsl-region-body">
					<xsl:choose>
						<xsl:when test="$format='document'">
							<xsl:for-each select="result">
								<xsl:variable name="ead_fo_path" select="concat('cocoon://ead.fo?c=', sdxdocid/input/@value)"/>
								<xsl:choose>
									<xsl:when test="doc-available($ead_fo_path)">
										<xsl:variable name="fodoc" select="document($ead_fo_path)//*[local-name()='flow']"/>
										<xsl:copy-of select="$fodoc/*[local-name()='block'][1]"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:message>Unable to find FO output for "<xsl:value-of select="sdxdocid/input/@value"/>" component (path was "<xsl:value-of select="$ead_fo_path"/>")</xsl:message>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<!-- Le titre -->
							<block xsl:use-attribute-sets="basket-title"><i18n:text key="basket.page.title" catalogue="module-eadeac">porte-documents</i18n:text> :
								<inline>
									<xsl:variable name="n">
										<xsl:choose>
											<xsl:when test="function-available('pleade:format-number')">
												<xsl:value-of select="pleade:format-number(@total)" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="@total" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
		
									<xsl:value-of select="$n"/>
		
									<xsl:variable name="k">
										<xsl:choose>
											<xsl:when test="number($n) &gt; 1">
												<xsl:text>basket.results-title.many</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>basket.results-title.one</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
		
									<i18n:text key="{$k}" catalogue="module-eadeac">
										<xsl:value-of select="$k" />
									</i18n:text>
		
								</inline>
							</block>
							<!-- Un bloc pour avoir les attributs génériques -->
							<block xsl:use-attribute-sets="general" keep-with-previous="always">
								<!-- Maintenant on traite un item -->
								<xsl:if test="result">
									<table xsl:use-attribute-sets="table-fv">
										<!-- On ne définit pas la largeur de la première colonne : FOP lui donne l'espace qui reste -->
										<table-column column-width="1cm"/>
										<!-- La taille des colonnes de gauche peuvent être définies -->
										<table-column column-width="auto"/>
										<table-column column-width="2.5cm"/>
										<table-column column-width="3cm"/>
										<table-header>
											<table-row xsl:use-attribute-sets="pl-pgd-eadheader-subtitle">
												<xsl:element name="table-cell" use-attribute-sets="th-as-table-cell">
													<block><i18n:text key="basket.fo.results.col-title.no" catalogue="module-eadeac">n°</i18n:text></block>
												</xsl:element>
												<xsl:element name="table-cell" use-attribute-sets="th-as-table-cell">
													<block><i18n:text key="basket.results.col-title.intitule" catalogue="module-eadeac">intitulé</i18n:text></block>
												</xsl:element>
												<xsl:element name="table-cell" use-attribute-sets="th-as-table-cell">
													<block><i18n:text key="basket.results.col-title.date" catalogue="module-eadeac">date</i18n:text></block>
												</xsl:element>
												<xsl:element name="table-cell" use-attribute-sets="th-as-table-cell">
													<block><i18n:text key="basket.results.col-title.cote" catalogue="module-eadeac">cote</i18n:text></block>
												</xsl:element>
											</table-row>
										</table-header>
										<table-body>
											<xsl:apply-templates select="result" mode="basket"/>
										</table-body>
									</table>
								</xsl:if>
							</block>
						</xsl:otherwise>
					</xsl:choose>

					<!-- On insère ici un marqueur afin de connaître le nombre total de pages -->
					<block id="last-page"/>
				</flow>
			</page-sequence>
		</root>
	</xsl:template>

	<xsl:template match="result" mode="basket">
		<table-row>
			<xsl:element name="table-cell" use-attribute-sets="td-as-table-cell">
				<block><xsl:value-of select="position()"/></block>
			</xsl:element>
			<xsl:element name="table-cell" use-attribute-sets="td-as-table-cell">
				<block>
					<xsl:apply-templates select="fucomptitle"/>
					<xsl:if test="normalize-space(fucomptitle) = ''"><xsl:text>&#160;</xsl:text></xsl:if>
				</block>
			</xsl:element>
			<xsl:element name="table-cell" use-attribute-sets="td-as-table-cell">
				<block>
					<xsl:value-of select="udate"/>
					<xsl:if test="normalize-space(udate) = ''"><xsl:text>&#160;</xsl:text></xsl:if>
				</block>
			</xsl:element>
			<xsl:element name="table-cell" use-attribute-sets="td-as-table-cell">
				<block>
					<xsl:apply-templates select="uunitid"/>
					<xsl:if test="normalize-space(uunitid) = ''"><xsl:text>&#160;</xsl:text></xsl:if>
				</block>
			</xsl:element>
		</table-row>
	</xsl:template>

	<xsl:template match="a">
		<xsl:value-of select="."/>
	</xsl:template>

	<!-- on sort le contexte -->
	<xsl:template match="a[@class = 'pl-ancestors']"/>
	<xsl:template match="div[@class = 'pl-ancestors']">
		<block xsl:use-attribute-sets="unit-context"><xsl:value-of select="."/></block>
	</xsl:template>

	<!-- Le template qui permet de sortir le pied de page -->
	<xsl:template name="output-footer">
		<block xsl:use-attribute-sets="footer">
			<!-- Un tableau à 2 colonnes, celle de droite pour le numéro de page -->
			<table>
				<!-- On ne définit pas la largeur de la première colonne : FOP lui donne l'espace qui reste -->
				<table-column/>
				<!-- La colonne de droite peut être limitée à 3cm, dans ce cas "Page 1000 de 1000" peut être écrit. -->
				<table-column column-width="2.5cm"/>
				<table-body>
					<table-row>
						<!-- La première celulle contient l'intitulé du document demandé puis
								l'intitulé de l'élément sur la page -->
						<table-cell padding="1pt" padding-right="2pt">
							<block xsl:use-attribute-sets="footer-line1">
								<i18n:text key="basket.fo.footer.title" catalogue="module-eadeac">Contenu du porte-documents</i18n:text>
							</block>
							<block xsl:use-attribute-sets="footer-line3">
								<i18n:text key="basket.fo.footer.copyright" catalogue="module-eadeac">copyright Pleade 3</i18n:text>
							</block>
							<block xsl:use-attribute-sets="footer-line2">
								<i18n:text key="basket.fo.footer.generated" catalogue="module-eadeac">Généré le </i18n:text>
								<xsl:variable name="myDf" select="sdf:new($formatDate1)"/>
								<xsl:variable name="myDate" select="date:new()" />
								<xsl:value-of select="df:format($myDf,$myDate)"/>
							</block>
						</table-cell>
						<!-- La seconde cellule contient la pagination et le nombre de pages -->
						<table-cell padding="1pt" padding-left="2pt" text-align="right">
							<block><i18n:text key="ead.fo.footer.page.before"/><page-number/><i18n:text key="ead.fo.footer.nbpages.before"/><page-number-citation ref-id="last-page"/></block></table-cell>
					</table-row>
				</table-body>
			</table>
		</block>
	</xsl:template>

	<!-- Styles particuliers aux PDF du panier -->
	<xsl:attribute-set name="basket-title">
		<xsl:attribute name="padding">2pt</xsl:attribute>
		<xsl:attribute name="padding-top">5pt</xsl:attribute>
		<xsl:attribute name="font-family">Verdana</xsl:attribute>
		<xsl:attribute name="margin-bottom">2pt</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>
