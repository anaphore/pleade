<?xml version="1.0" encoding="UTF-8"?>
<o:stylesheet xmlns:o="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xfm="http://www.w3.org/2002/01/xforms" xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx" version="1.0" sdx:copy="for namespace">
   <xsl:import href="skin.xsl"/> 
<!-- parameter language to choose a message set -->
   <o:param name="lang-default" select="'fr'"/> 

   <xsl:param name="messages-URI" select="concat('messages.', substring($lang, 1, 2))"/><!-- take set of language -->
   <o:variable name="messages" select="document($messages-URI)/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)] | document('')/*/xfm:messages[substring($lang, 1, 2)=substring(@xml:lang, 1, 2)]"/>
   <!-- Where to go up to link to the apps -->
   <xsl:variable name="server" select="'../../'"/>
   <xsl:template match="bar" mode="head">
      <title>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;title1&#34;] )[1]"/>
      </title>
   </xsl:template>
   <xsl:template match="applications">
      <h2>
         <o:apply-templates select="($messages/xfm:caption[@id=&#34;h21&#34;] )[1]"/>
      </h2>
      <table cellpadding="0" cellspacing="0" border="0">
         <form style="margin:0">
            <tr>
               <td>
                  <input style="cursor:pointer" type="button" onclick="window.location.href='loginsu.xsp';" value="{$messages/xfm:caption[@id='admin']}" title="{$messages/xfm:caption[@id='admin']}"/>
               </td>
               <td class="menu">
                  <a href="{$server}">
                     <xsl:text>  </xsl:text>
                     <o:apply-templates select="($messages/xfm:caption[@id=&#34;server&#34;] )[1]"/>
                  </a>
               </td>
            </tr>
            <xsl:for-each select="application">
               <tr>
                  <td>
                     <input style="cursor:pointer" type="button" onclick="window.location.href='login.xsp?app={@id}';" value="{$messages/xfm:caption[@id='admin']}" title="{$messages/xfm:caption[@id='admin']}"/>
                  </td>
                  <td class="menu">
                     <a href="{$server}{normalize-space(@path)}">
                        <xsl:text>  </xsl:text>
                        <xsl:value-of select="@id"/>
                     </a>
                  </td>
               </tr>
            </xsl:for-each>
         </form>
      </table>
   </xsl:template>  
<!-- ====== SCRIPT ====================== -->  

   <o:template name="xfm:script">
      <script type="text/javascript" language="javascript">
    
    function xfm_blur(o)
    {
        if (!o.className) return true;
        o.className=o.className.replace(/ ?xfm_focus/gi, ''); 
        return true;
    }

    function xfm_focus(o) 
    {
        document.xfm_last = o;
        if (!o.className) return true;
        o.className=o.className + ' xfm_focus'; 
        return true;
    }
    

            function xfm_load()
            {
                
                return true;
            }

            function xfm_badControl(form, name)
            {
                if (!form || !name) return false;
                if(form[name])
                {
                    // if (form[name].selectedIndex &amp;&amp; form[name].selectedIndex == -1) return true;
                    if (!form[name].value) return true;
                }
                return false;
            }

            
            function xfm_submit(form)
            {
                var message="";
                var name="";
                
                if (!message) return true ;
                alert(message);
                return false;
            }
            
            function xfm_reset(form)
            {
            
            }

                </script>
   </o:template>  
<!-- ======  COMMON TEMPLATES  ====================== 
        short templates to format messages
        known issues : no correct id for a repeated help button 
        then hide/show message can't work
        but is it useful to repeat the same help ?
        
-->  

   <o:template match="xfm:help">
      <o:param name="id" select="@id"/>
      <input type="button" class="xfm_help" tabindex="32767" value="?">
         <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
      </input>
      <table cellpadding="1" cellspacing="0" border="0" class="xfm_help" style="display:none;">
         <o:attribute name="id">
            <o:value-of select="$id"/>_help</o:attribute>
         <tr>
            <td style="width:90%;" class="xfm_bar">
               <o:value-of select="(caption | title | h1 )[1]"/>
            </td>
            <td style="text-align:right" class="xfm_bar">
               <input type="button" class="xfm_key" value="X">
                  <o:attribute name="onclick">if(document.getElementById) var o=document.getElementById('<o:value-of select="$id"/>_help'); if (o!=null) o.style.display=(o.style.display=='none')?'':'none'; </o:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td colspan="2" class="xfm_text">
               <o:apply-templates mode="copy"/>
            </td>
         </tr>
      </table>
   </o:template>
   <o:template match="xfm:caption" priority="-1">
      <o:apply-templates mode="copy"/>
   </o:template><!--
        Escape js string (or other search/replace)-->
   <o:template match="* | @* | text()" mode="replace" name="xfm:replace">
      <o:param name="string" select="normalize-space(.)"/>
      <o:param name="search">'</o:param>
      <o:param name="replace">\'</o:param>
      <o:choose>
         <o:when test="contains($string, $search)">
            <o:value-of select="concat(substring-before($string, $search), $replace)"/>
            <o:call-template name="xfm:replace">
               <o:with-param name="string" select="substring-after($string, $search)"/>
               <o:with-param name="search" select="$search"/>
               <o:with-param name="replace" select="$replace"/>
            </o:call-template>
         </o:when>
         <o:otherwise>
            <o:value-of select="$string"/>
         </o:otherwise>
      </o:choose>
   </o:template>
   <o:template match="*" priority="-1" mode="copy">
      <o:element name="{name()}">
         <o:apply-templates select="@*" mode="copy"/>
         <o:apply-templates mode="copy"/>
      </o:element>
   </o:template>
   <o:template match="@*" mode="copy">
      <o:copy/>
   </o:template>  
<!-- ======  MESSAGES @xml:lang="fr"  ====================== -->  

   <xfm:messages xml:lang="fr">
      <xfm:caption id="title1"> SDX Administration - Accueil </xfm:caption>
      <xfm:caption id="h21">Applications ouvertes sur ce serveur</xfm:caption>
   </xfm:messages>  
<!-- ======  MESSAGES @xml:lang="en"  ====================== -->  

   <xfm:messages xml:lang="en">
      <xfm:caption id="title1"> SDX Administration - Welcome </xfm:caption>
      <xfm:caption id="h21">Applications on this framework</xfm:caption>
   </xfm:messages>
   <o:template match="node()" mode="onload" priority="-2"/>
</o:stylesheet>