/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pleade.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * Methodes statiques pour traiter des dates dans le fuseau horaire GMT.
 */
public class DateUtilities {

    /**
     * Methode de test. Les dates reformatees seront affichees a l'ecran.
     * @param args  Les dates a formater.
     */
    public static void main(String[] args) {
    		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        for (int i=0; i<args.length; i++) {
            Date d = parse(args[i]);
            Date s = endOfDatePeriod(d, args[i]);
            System.out.println();
            System.out.println("Begin date: "+args[i] + " : format = " + format(d) + " ; toString = " + d.toString() + " ; getTime = " + d.getTime());
            System.out.println("  End date: "+args[i] + " : format = " + format(s) + " ; toString = " + s.toString() + " ; getTime = " + s.getTime());
            System.out.println("  Max date: format = " + format(getMaximumDate()) + " ; toString = " + getMaximumDate().toString() + " ; getTime = " + getMaximumDate().getTime());
            System.out.println("  Min date: format = " + format(getMinimumDate()) + " ; toString = " + getMinimumDate().toString() + " ; getTime = " + getMinimumDate().getTime());
            System.out.println("Default TimeZone: "+TimeZone.getDefault());
        }
    }

			/**
			 * Analyse une chaine et retourne une date. Les formats supportes sont bases sur
			 * la norme ISO 8601:
			 * <pre>
			 *   yyyy-MM-dd'T'HH:mm:ssZ
			 *   yyyy-MM-dd'T'HH:mm:ss
			 *   yyyy-MM-dd'T'HH:mm
			 *   yyyy-MM-dd'T'HH
			 *   yyyy-MM-dd
			 *   yyyy-MM
			 *   yyyy
			 * </pre>
			 * Tous les formats sauf le premier supposent que la date est dans le fuseau GMT.
			 * @param s     La chaine a analyser.
			 * @return      Une date, ou null si l'analyse n'aboutit pas.
			 */
			public static Date parse(String s) {
					if ( s == null ) return null;
					Date ret = parse(s, "yyyy-MM-dd'T'HH:mm:ssZ");
					if ( ret != null ) return ret;
					else {
							ret = parse(s, "yyyy-MM-dd'T'HH:mm:ss");
							if ( ret != null ) return ret;
							else {
									ret = parse(s, "yyyy-MM-dd'T'HH:mm");
									if ( ret != null ) return ret;
									else {
											ret = parse(s, "yyyy-MM-dd'T'HH");
											if ( ret != null ) return ret;
											else {
													ret = parse(s, "yyyy-MM-dd");
													if ( ret != null ) return ret;
													else {
															ret = parse(s, "yyyy-MM");
															if ( ret != null ) return ret;
															else {
																	if ( s.length() > 4 ) return null;
																	return parse(s, "yyyy");
															}
													}
											}
									}
							}
					}
			}

			/**
			 * Analyse une date selon un certain patron.
			 * @param s     La date.
			 * @param p     Le patron.
			 * @return      Une date ou null si l'analyse ne fonctionne pas.
			 */
			public static Date parse(String s, String p) {
					SimpleDateFormat df = new SimpleDateFormat(p);
					df.setTimeZone(TimeZone.getTimeZone("GMT"));
					try {
							Date d = df.parse(s);
							return d;
					}
					catch (ParseException e) {
							return null;
					}
			}

			/**
			 * Formate une date selon la norme ISO 8601. Le format est: yyyy-MM-dd'T'HH:mm:ssZ.
			 * On utilise toujours le fuseau GMT.
			 * @param d
			 * @return
			 */
			public static String format(Date d) {
					if (d == null) return null;
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
					df.setTimeZone(TimeZone.getTimeZone("GMT"));
					return df.format(d);
			}

			/**
			 * Renvoie une date en millisecondes
			 * @param d Date
			 * @return long
			 */
			public static long getLong(Date d) {
				if (d == null) return -1;
				return d.getTime();
			}

			/**
			 * Modifie une date pour qu'elle represente la fin d'une periode (annee, mois ou jour).
			 * La periode est calculee en fonction de la chaine de caracteres utilisee pour analyser
			 * la date. Par exemple, si s = 1999, alors on retourne le 31 decembre 1999. Si
			 * s = 1999-11, on retourne le 30 novembre.
			 * @param d     L'objet Date. Si null, on le calcule
			 * @param s     La chaine d'origine de la date
			 * @return      Une date, ou null
			 */
			public static Date endOfDatePeriod(Date d, String s) {
    		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        // On s'assure que l'on a des objets valides
        if ( s == null ) return null;
        if ( d == null ) d = parse(s);
        if ( d == null ) return null;
        // On identifie le dernier "champ" specifie dans la date
        StringTokenizer st = new StringTokenizer(s, "-");
        int nbParts = st.countTokens();
        int lastField = Calendar.YEAR;
        if ( nbParts == 2 ) lastField = Calendar.MONTH;
        if ( nbParts == 3 ) lastField = Calendar.DAY_OF_MONTH;
        // On utilise un objet Calendar sur cette date
        Calendar c = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.setTime(d);
        // Selon le dernier champ specifie, on ajuste le reste de l'annee
        switch (lastField) {
            case Calendar.YEAR:
                c.set(Calendar.MONTH, Calendar.DECEMBER);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                break;
            case Calendar.MONTH:
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                break;
        }
        // Ensuite on doit ajuster le temps
        c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
        c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, c.getActualMaximum(Calendar.MILLISECOND));
        // Et on retourne la date
        return c.getTime();
    }

    /**
     * Retourne une date minimale geree par l'application.
     * C'est le premier janvier 1, a 0h, qui est retourne.
     */
    public static Date getMinimumDate() {
    		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        Calendar c = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.clear();
        c.set(1, 0, 1, 0, 0, 0);    // 1er janvier annee 1, a minuit
        return c.getTime();
    }

    /**
     * Retourne une date maximale geree par l'application.
     * C'est le 31 decembre 9999, a 23:59:59, qui est retourne.
     */
    public static Date getMaximumDate() {
    		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        Calendar c = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.clear();
        c.set(9999, 11, 31, 23, 59, 59);    // 31 decembre 9999, juste avant minuit
        return c.getTime();
    }

		/** Formater des millisecondes en duree
		 */
		public static String duree(long duree) {
			String ret = null;
			Date date = new Date( duree );
			SimpleDateFormat format = new SimpleDateFormat( "HH'h 'mm'm 'ss's 'SSS' ms'" );
			format.setTimeZone( TimeZone.getTimeZone( "GMT" ) ); // ne pas oublier de fixer a GMT, ou sinon les milis auront une heure de trop
			ret = format.format( date );
			format = null; date = null;
			return ret;
		}
}
