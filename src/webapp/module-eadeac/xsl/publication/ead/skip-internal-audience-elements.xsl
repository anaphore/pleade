<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="xsl saxon">

	<!-- Transformation qui supprime tous les éléments avec @audience="internal"	-->

	<!-- Fonctions de publication utiles -->
	<xsl:import href="publication-helpers.xsl"/>

	<!-- FIXME: le PropertiesWriter devrait déjà être passé ici (ce n'est pas le cas pour le moment).
	Si c'était le cas, on aurait uniquement besoin de lire le fichier de configuration -->
	<xsl:param name="delete-internal" select="''"/>
	
	<xsl:param name="debug" select="'false'"/>

	<!-- Identifiant du document publié -->
	<xsl:variable name="eadid" select="/ead/eadheader/eadid"/>

	<!-- Les propriétés de publication -->
	<xsl:variable name="properties-url" select="concat('cocoon://functions/ead/get-properties/', $eadid, '/publication.xml')"/>
	<xsl:variable name="pub-properties" select="if(doc-available($properties-url)) then document($properties-url)/properties else ()"/>
	<!-- La propriété delete-internal -->
	<!-- FIXME: le PropertiesWriter devrait déjà être passé ici (ce n'est pas le cas pour le moment).
	Si c'était le cas, on aurait uniquement besoin de lire le fichier de configuration -->
	<xsl:variable name="delete-internal-prop" select="
			if ($delete-internal != '') then xs:boolean($delete-internal)
			else if ($pub-properties/property[@name='delete-internal'] != '') then xs:boolean($pub-properties/property[@name='delete-internal'])
			else if ($pub-properties/property[@name='delete-internal']/@default != '') then xs:boolean($pub-properties/property[@name='delete-internal']/@default)
			else true()"/>

	<xsl:template match="/">
		<xsl:message>	Traitement des elements @audience='internal' (etape 2 / 14) : supprimes = <xsl:value-of select="$delete-internal-prop"/> : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:if test="$delete-internal-prop and /ead/@audience = 'internal'"><xsl:message terminate="yes">		Erreur: Ce document possède un attribut "audience" dont la valeur est "<xsl:value-of select="/ead/@audience" />" directement sur l'élément "ead" et la propriété de publication "delete-internal" a la valeur "<xsl:value-of select="$delete-internal-prop" />". Pleade ne peut donc rien en faire...</xsl:message></xsl:if>
		<xsl:choose>
			<xsl:when test="debug='true'">
				<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{ead/eadheader/eadid}/{ead/eadheader/eadid}-2.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- On copie tout -->
	<xsl:template match="node()">
	 <xsl:if test="pleade:is-external(.) or not($delete-internal-prop)">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	 </xsl:if>
	</xsl:template>

	<xsl:template match="@*">
		<xsl:copy>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>



</xsl:stylesheet>

