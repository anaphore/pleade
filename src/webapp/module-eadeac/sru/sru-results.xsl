<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: format-sru.xsp 19706 2010-11-25 16:52:20Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  exclude-result-prefixes="sdx pleade i18n fn">

  <xsl:param name="recordSchema" select="'dc'"/>
  <xsl:param name="the_requete" />

  <xsl:template match="/">
    <searchRetrieveResponse>
      <version>1.2</version>
      <!-- La requête:format SRU -->
      <echoedSearchRetrieveRequest>
        <query>
          <xsl:value-of select="$the_requete"/>
        </query>
      </echoedSearchRetrieveRequest>
      <!-- Le nombre de réponses -->
      <numberOfRecords>
        <xsl:value-of select="sdx:document/sdx:results/@nb"/>
      </numberOfRecords>
      <!-- Resultats --><!-- numberOfRecords -->
      <xsl:apply-templates select="sdx:document/sdx:results"/>
    </searchRetrieveResponse>
  </xsl:template>

  <xsl:template match="@qid">
    <resultSetId>
      <xsl:value-of select="."/>
    </resultSetId>
  </xsl:template>

  <xsl:template match="sdx:results">
    <xsl:apply-templates select="@qid"/> <!--  resultSetId -->
    <records>
      <xsl:apply-templates select="sdx:result"/> <!--record -->
    </records>
  </xsl:template>

  <xsl:template match="sdx:result">
    <!--Récupération du fichier XML que contient les informations-->
    <xsl:variable name="docid" select="sdx:field[@name='sdxdocid']"/>
    <xsl:variable name="doc" select="if (doc-available(concat('cocoon://ead-fragment.debug?c=', $docid))) then doc(concat('cocoon://ead-fragment.debug?c=', $docid)) else ()"/>

    <record>
      <xsl:variable name="author">
        <xsl:choose>
          <xsl:when test="$doc//filedesc/titlestmt/author and $doc//filedesc/titlestmt/author != ''">
            <xsl:value-of select="$doc//filedesc/titlestmt/author"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="eadheader-id" select="doc(concat('cocoon://functions/ead/get-properties/', sdx:field[@name='root-id'], '/display.xml'))/properties/property[@name='eadheader-id']"/>
            <xsl:variable name="eadheader-doc" select="if (doc-available(concat('cocoon://ead-fragment.debug?c=', $eadheader-id))) then doc(concat('cocoon://ead-fragment.debug?c=', $eadheader-id)) else ()"/>
            <xsl:value-of select="$eadheader-doc//filedesc/titlestmt/author"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <recordSchema>
        <xsl:choose>
          <xsl:when test="$recordSchema != ''">
            <xsl:value-of select="upper-case($recordSchema)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>DC</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </recordSchema>
      <recordPacking>xml</recordPacking>
      <recordData>
        <!-- Let's check the requested format -->
        <xsl:choose>
          <xsl:when test="$recordSchema = 'qdc'">
            <!--<xsl:message>"qdc" format requested</xsl:message>-->
            <xsl:copy-of select="doc(concat('cocoon://sru_formats/', $docid,'/qdc.xml'))/*[local-name()= 'metadata-record']/*"/>
          </xsl:when>
          <xsl:when test="$recordSchema = 'oai_dc'">
            <!--<xsl:message>"oai_dc" format requested</xsl:message>-->
            <xsl:copy-of select="doc(concat('cocoon://sru_formats/', $docid,'/oai_dc.xml'))/*[local-name()= 'dc']/*"/>
          </xsl:when>
          <xsl:when test="$recordSchema = 'ead'">
            <!--<xsl:message>"ead" format requested</xsl:message>-->
            <xsl:copy-of select="doc(concat('cocoon://sru_formats/', $docid,'/ead.xml'))/sdx:document/*"/>
          </xsl:when>
          <xsl:otherwise>
            <!--<xsl:message>Format not specified or unknow (<xsl:value-of select="$recordSchema"/>); falling back to DC.</xsl:message>-->
            <xsl:apply-templates select="." mode="dc">
              <xsl:with-param name="doc" select="$doc"/>
              <xsl:with-param name="author" select="$author"/>
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </recordData>
      <recordIdentifier>
        <xsl:value-of select="$docid"/>
      </recordIdentifier>
      <recordPosition>
        <xsl:value-of select="$doc/sdx:document/pleade:subdoc/c/@pleade:no"/>
      </recordPosition>
    </record>
  </xsl:template>

  <xsl:template match="sdx:result" mode="dc">
    <xsl:param name="doc"/>
    <xsl:param name="author"/>

      <xsl:apply-templates select="$doc/sdx:document/pleade:subdoc/c/did/unittitle" mode="dc"/>

      <xsl:if test="$author != ''">
        <dc:creator>
          <xsl:value-of select="$author"/>
        </dc:creator>
      </xsl:if>

      <xsl:apply-templates select="$doc/sdx:document/pleade:subdoc/c/did/unittitle/unitdate" mode="dc"/>
      <xsl:apply-templates select="$doc/sdx:document/pleade:subdoc/c/did/unitdate" mode="dc"/>
      <xsl:apply-templates select="$doc/sdx:document/pleade:subdoc/c/did/repository/corpname" mode="dc"/>
      <xsl:apply-templates select="$doc/sdx:document/pleade:subdoc/c/did/physdesc/physfacet[1]" mode="dc"/>
      <xsl:apply-templates select="$doc/sdx:document/pleade:subdoc/c/did/langmaterial/language" mode="dc"/>
  </xsl:template>

  <xsl:template match="unittitle" mode="dc">
    <dc:title>
      <xsl:value-of select="."/>
    </dc:title>
  </xsl:template>

  <xsl:template match="unitdate" mode="dc">
    <dc:date>
      <xsl:value-of select="."/>
    </dc:date>
  </xsl:template>

  <xsl:template match="corpname" mode="dc">
    <dc:publisher>
      <xsl:value-of select="."/>
    </dc:publisher>
  </xsl:template>

  <xsl:template match="physfacet[1]" mode="dc">
    <dc:description>
      <xsl:value-of select="."/>
    </dc:description>
  </xsl:template>

  <xsl:template match="language" mode="dc">
    <dc:language>
      <xsl:value-of select="."/>
    </dc:language>
  </xsl:template>

</xsl:stylesheet>
