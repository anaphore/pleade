<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<!--
dc.description	:	A transformation for transforming Qualified Dublin Core documents into OAI DC.
					input document = OAI QDC record
					output document = OAI DC record
dc.date				: April 2004
dc.date.modified	: 27th September 2004
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
	xmlns:qdc="http://pleade.org/oai/qdc"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dcterms="http://purl.org/dc/terms/"
>

	<xsl:output method="xml" indent="yes"/>
	
	<!-- NB : This file needs the table file dcqtodc.xml -->
	

	<!-- ******************** begin GENERAL TEMPLATE ************************* -->
	<xsl:template match="/*">
		<!-- Define the root element of the output document -->
		<oai_dc:dc>
			<!-- Processing subelements -->
			<xsl:apply-templates/>
		</oai_dc:dc>
	</xsl:template>
	<!-- ******************** end GENERAL TEMPLATE *************************** -->
	
	<!-- Proceed sub-elements -->
	<xsl:template match="*">
		<xsl:choose>
			<!-- Copying without process DC elements -->
			<xsl:when test="namespace-uri() = 'http://purl.org/dc/elements/1.1/'">
				<xsl:copy-of select="."/>
			</xsl:when>
			
			<!-- Transforming QDC elements in DC simple elements (mapping is in a table file dcqtodc.xml) -->
			<xsl:when test="namespace-uri() = 'http://purl.org/dc/terms/'">
				<xsl:variable name="element" select="local-name()"/>
				<!-- We look for the name of the corresponding DC simple element -->
				<xsl:variable name="name-el" select="string(document('dcqtodc.xml')/table[@name='DCQtoDC']/element[@name=$element]/@refines)"/>
				<xsl:choose>
					<xsl:when test="$name-el != ''">
						<xsl:element name="{concat('dc:',$name-el)}">
							<xsl:value-of select="."/>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>No mapping for element <xsl:value-of select="$element"/></xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>Unsupported namespace : <xsl:value-of select="namespace-uri()"/></xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Not to output the comment -->
	<xsl:template match="comment()"></xsl:template>

</xsl:stylesheet>
