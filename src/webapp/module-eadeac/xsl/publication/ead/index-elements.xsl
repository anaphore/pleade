<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="xsl saxon"
>

	<!-- Ajoute des informations aux éléments d'indexation -->

	<!-- Fonctions de publication utiles -->
	<xsl:import href="publication-helpers.xsl"/>
	<!-- Fonctions liées aux index -->
	<xsl:import href="../../index-helpers.xsl"/>
	
	<xsl:param name="debug" select="'false'"/>
	
	<!-- Identifiant du document publié -->
	<xsl:variable name="eadid" select="/ead/@pleade:id"/>
	
	<!-- Les propriétés de publication -->
	<!--<xsl:variable name="properties-url" select="concat('cocoon://functions/ead/get-properties/', $eadid, '/publication.xml')"/>
	<xsl:variable name="pub-properties" select="if(doc-available($properties-url)) then document($properties-url)/properties else ()"/>-->
	<!-- La propriété delete-internal -->
	<xsl:variable name="delete-internal" select="
		if ($pub-properties/property[@name='delete-internal'] != '') 
			then xs:boolean($pub-properties/property[@name='delete-internal'])
		else true()"/> 

	<!-- Les définitions d'index -->
	<!-- TODO: mode statique -->
	<xsl:variable name="index-definitions-url" select="string('cocoon://conf/ead-index.xconf')"/>
	<xsl:variable name="index-definitions" select="
      if( doc-available($index-definitions-url) ) then document($index-definitions-url)/*
      else ()
  "/>

	<xsl:template match="/">
		<xsl:message>	Traitement des éléments d'indexation (etape 6 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:if test="not($index-definitions) or $index-definitions=''">
      <!-- On stoppe le processus d'indexation et on sort un message d'erreur car on ne peut pas récupérer la liste des index de la base EAD. -->
      <xsl:message terminate="yes">		Erreur interne : Impossible de recuperer la liste des index de la base EAD (conf/ead-index.xconf) !</xsl:message>
    </xsl:if>
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{$eadid}/{$eadid}-6.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- On intercepte tous les éléments pour ajouter leur identifiant et éventuellement d'autres informations. -->
	<xsl:template match="*">
		<xsl:copy>
			<!-- On copie les attributs déjà présents -->
			<xsl:apply-templates select="@*"/>
			<!-- Les éléments d'indexation
					Seuls les éléments qui réunissent toutes les conditions suivantes sont marqués pour la constitution des index :
					  * est un élément d'indexation
						* a une valeur : non vide ou avec un @normal non vide
						* est external (n'a pas @audience = 'internal')
						* n'a pas d'ancêtre internal
					Les deux derniers tests ne sont effectués que si le paramètre de publication delete-internal vaut false.
			-->
			<xsl:if test="pleade:is-index-element(.) and (. != '' or @normal != '') and (xs:boolean($delete-internal) 
										or (not(xs:boolean($delete-internal)) and pleade:is-external(.) and not(pleade:has-internal-ancestor(.))))">
				
        <!-- On garde une copie de l'élément -->
				<xsl:variable name="el" select="."/>
				<!-- On construit une liste d'index pertinents (sous-éléments index id="") -->
				<xsl:variable name="index-list">
					<xsl:for-each select="$index-definitions/index/content">
						<xsl:if test="pleade:index-matches(., $el)">
							<index id="{../@id}"/>
						</xsl:if>
					</xsl:for-each>
				</xsl:variable>
				<xsl:if test="$index-list/index">	<!-- Il pourrait ne pas y en avoir! -->
					<!-- La valeur de l'attribut index-ids -->
					<xsl:variable name="ids">
						<xsl:for-each select="$index-list/index">
							<xsl:if test="position() > 1">,</xsl:if>
							<xsl:value-of select="@id"/>
						</xsl:for-each>
					</xsl:variable>
					<!-- Le contenu -->
					<xsl:variable name="value">
						<xsl:choose>
							<xsl:when test="@normal"><xsl:value-of select="normalize-space(@normal)"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="normalize-space(.)"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<!-- Deux attributs, un pour la liste des identifiants et l'autre pour l'identifiant "principal" -->
					<xsl:variable name="main-id" select="($index-list/index)[1]/@id"/>
					<xsl:attribute name="pleade:index-list"><xsl:value-of select="$ids"/></xsl:attribute>
					<xsl:attribute name="pleade:index-id"><xsl:value-of select="$main-id"/></xsl:attribute>
					<!-- Un attribut pour le contenu -->
					<xsl:attribute name="pleade:value"><xsl:value-of select="$value"/></xsl:attribute>
					<!-- Un attribut pour indiquer s'il est local ou pas -->
					<xsl:variable name="index-local" select="boolean($index-definitions/index[@id = $main-id]/@local-sdxfield)"/>
					<xsl:attribute name="pleade:index-local">
						<xsl:value-of select="string($index-local)"/>
					</xsl:attribute>
					<xsl:if test="$index-local">
						<xsl:attribute name="pleade:index-local-name"><xsl:value-of select="$index-definitions/index[@id = $main-id]/@local-sdxfield"/></xsl:attribute>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<!-- Ensuite on copie le contenu -->
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>

	<!-- Le contenu est en général copié -->
	<xsl:template match="text()|processing-instruction()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

