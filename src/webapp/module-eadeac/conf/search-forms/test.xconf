<?xml version="1.0"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Ce fichier de configuration fait partie de la distribution standard
de Pleade. Vous pouvez le modifier à votre guise. -->
<form action="results.html" max-chars="50" documents="ead" auto-expand="2" display-results="simple">

	<!--Cadre de classement-->
	<key type="cdc"/>

	<!--critère caché -->
	<!--<key type="hidden" connector="2" field="unitid" value="bretagne" />-->

	<!--zone de texte, champ de recherche prédéfini (opérateur par défaut AND)-->
	<key type="text" style="width:10em;" field="fulltext" format="\w*\s\w*"/>
	<!--format="\w\s\w"-->


	<!--zone de texte, champ de recherche prédéfini avec opérateur OR-->
	<key type="text" field="bioghist" connector="2"/>

	<!--zone de texte, champ de recherche prédéfini avec opérateur NOT-->
	<key type="text" field="bibliography" connector="4"/>

	<!--zone de texte, champ de recherche prédéfini avec choix de l'opérateur AND/OR-->
	<key type="text" field="unitid" connector="3"/>

	<!--zone de texte avec suggestions et aide-->
	<key type="suggest" field="fgeogname" style="background:white" /><!--op-display="false"-->

	<key type="suggest" field="fgeogname" style="background:#00CCFF" help="true" op-display="true"/>

	<key type="suggest" field="fgeogname" style="background:coral" op-display="true" op="AND"/>

	<key type="suggest" field="fgeogname" multiple="false"/>

	<!--zone de texte avec choix du champ de recherche -->
	<key type="text" style="width:15em;" help="true" id="id1">
	<!--format="\w_\d"-->
		<!-- Les champs de recherche -->
		<field name="fulltext"/>
		<field name="scopecontent" default="true"/>
		<field name="unittitle"/>
	</key>

	<!--le suggest multi-champ n'est plus pris en compte, il ne s'affichera pas-->
	<!--<key type="suggest" style="width:15em;background:#00CCFF" help="true" id="zone2">
		<field name="fpersname"/>
		<field name="fgeogname"/>
	</key>-->

	<!--combo-list avec items-->
	<key type="list" variant="combo" selection="multiple" field="fpersname"
				connector="3" op-display='false'>
	<!--@selection n'est pas pris en compte pour ce type de liste,
	seul le dernier default=true sera selectionné (gérer par navigateur)-->
		<no-choice i18n-key="form-no-choice-barre"/>
		<no-choice i18n-key="form-no-choice-choix"/>
		<items>
			<item value="kant" i18n-key="form-item-kant"/>
			<item value="descartes" i18n-key="form-item-descartes"/>
			<item value="aristote" i18n-key="form-item-aristote" default="true"/>
			<item value="platon" i18n-key="form-item-platon" default="true"/>
		</items>
	</key>

	<key type="list" variant="checkbox" style="background:green;" display="vertical" field="fpersname">
		<items>
			<item value="Kant" i18n-key="form-item-kant"/>
			<item value="Descartes" i18n-key="form-item-descartes"/>
			<item value="Aristote" i18n-key="form-item-aristote" default="true"/>
			<item value="Platon" i18n-key="form-item-platon" default="true"/>
		</items>
	</key>


	<!--combo-list avec terms et @rows => rows n'est pas pris en compte sur un combo + une surcharge de "max-chars"-->
	<key type="list" variant="combo" rows="5" field="fpersname" help="true" max-chars="20">
		<no-choice i18n-key="form-no-choice-choix"/>
		<filters>
			 <filter field="unitid" value="1 W art. 1"/>
		</filters>
	</key>


	<!--fixed-list avec terms et @rows et @selection=muliple, il peut y avoir plusieurs default=true sur le items-->
	<key 	type="list" variant="fixed" selection="multiple" field="fgeogname"
				style="background:yellow" rows="5"
				op-display='true'>
		<items>
			<item value="paris" i18n-key="form-item-paris" default="true"/>
			<item value="pragues" i18n-key="form-item-pragues"/>
			<item value="moscou" i18n-key="form-item-moscou" default="true"/>
			<item value="berlin" i18n-key="form-item-berlin" default="true"/>
		</items>
	</key>

	<key type="list" variant="radio" display="horizontal" field="fgeogname" max-chars="40"/>

	<!--<key type="boolean" field="fgeogname" value-on="Afrique" default="on" connector="3" />-->

  <!-- variant=interval exact l'un ou les deux -->
	<!--<key type="date" variant="interval exact" format="\d\d\d\d" id="intervaleDates"/>-->

	 <!-- combinaison d'un @display-field avec un combo à selection multiple-->
		<key type="list" variant="combo" rows="5"
				selection="multiple" op-display="true"
				display-field="fatitle" field="root-id">
				<no-choice i18n-key="form-no-choice-choix"/>
		</key>

		<key type="list" variant="combo" rows="5" field="fpersname"
				selection="multiple" op-display="true">
				<no-choice i18n-key="form-no-choice-choix"/>
				<!--<filters>
					 <filter field="unitid" value="1 W art. 1"/>
				</filters>-->
				<!--<items>
					<item value="paris" i18n-key="form-item-paris" />
					<item value="pragues" i18n-key="form-item-pragues"/>
					<item value="moscou" i18n-key="form-item-moscou" />
					<item value="berlin" i18n-key="form-item-berlin" />
					<item value="Roqueblanque" i18n-key="form-item-roqueblanque" default="true"/>
					<item value="Allemagne" i18n-key="form-item-allemagne" default="true"/>
				</items>-->
		</key>

		<key type="list" variant="checkbox" display="horizontal" field="fpersname" op-display="true" />


	<!--zone de texte avec troncature implicite-->
	<key type="text" field="fulltext" truncate="left"/>
	<key type="text" field="fulltext" truncate="right"/>
	<key type="text" field="fulltext" truncate="both"/>
	<!--un truncate sur un text à champs multiple-->
	<key type="text" truncate="both">
		<field name="fulltext"/>
		<field name="scopecontent" default="true"/>
		<field name="unittitle"/>
	</key>
	<!--une valeur de truncate qui ne marche pas : pas pris en compte-->
	<key type="text" field="fulltext" truncate="toto"/>

</form>


<!--Documentation
key@type = suggest : idem text mais avec suggestion
key@help = true : ajout un bouton d'aide qui necessite les champs i18n suivant :
						search-form.{$form-id}.field.{@field}.help.button.title
						search-form.{$form-id}.field.{@field}.help.button.alt
						search-form.{$form-id}.field.{@field}.help.panel.hd : header (titre) de la bulle d'aide
						search-form.{$form-id}.field.{@field}.help.panel.bd : contenu de la bulle d'aide
						search-form.{$form-id}.field.{@field}.help.panel.ft : footer de la bulle d'aide


list @variant=combo : attributs pertinents : common (@connector, @field)
(le @rows aurait pu servir à limiter ou maximiser le scroll dans la liste déroulante mais une petite recherche google
montre que ce n'est pas vraiment possible en html/css, c'est le navigateur qui interprête.
 + items/item (@i18n-key)

list @variant=fixed : attributs pertinents : common + @rows + @selection
 + items/item (@i18n-key)

list@ variant=radio : attributs pertinents : common + @display

key[not(@field)] => ajouter @i18n-key

pas de no-choice sur radio/checkbox

@max-chars ne marche pas pour les items => c'est du i18n qui passe après la xsl, on peut pas tronquer

ne pas mettre deux fois le même champs avec help pour les deux => l'aide ne s'affiche pas

listes dépendantes :
  Placer l'attribut @updateSelect sur le champ source (ie, celui dont dépend le
  champ cible ;o)). Sa valeur doit être celle de l'attribut @field du champ
  cible.
  Au niveau du champ cible, on peut définir une clé i18n invitant à sélectionner
  une valeur au niveau du critère source... ou non.
  Toujours au niveau de la cible, on peut configurer la liste des choix de
  manière à ce que la liste soit vide tant que l'utilisateur n'a pas sélectionné
  de valeur au niveau du critère source.

  Exemples :
  <key type="list" field="insee" updateSelect="type" variant="combo" [...]>
   {champ source}
  </key>
  <key type="list" field="type" variant="combo" [...]>
    {champ cible)
    Solution 1 : au chargement de la page, le critère est "rempli" normalement,
                 son premier item invite l'utilisateur à sélectionner une valeur.
    <no-choice i18n-key="form-no-choice-choix"/>
    Solution 2 : au chargement de la page, le critère est vide et ne contient
                 qu'une unique valeur : un message invitant l'utilisateur à
                 sélectionner une valeur dans le champ source.
    <items>
      <item value="" i18n-key="form-no-choice-since-insee-choix" />
    </items>
  </key>

boolean @field obligatoire (pas de field en dessous)

critère date :

  si @field non vide alors l'intervalle se fait sur ce @field
  si @field est vide alors intervalle "à la Pleade", ie sur les champs du, db,
  de (respectivement : date unique, date début, date fin).

  date @i18n-key obligatoire
  i18n :
    date-label = label du champs "complet"
    date-label.du : label du input de "date unique"
    date-label.db : label du input de "date begin"
    date-label.de : label du input de "date end"

operateurs intra-champ
par défaut AND pour tout ce qui n'est pas list ou suggest
par défaut OR pour list et suggest

@op permet de surcharger cette valeur par défaut : @op=AND/OR exclusivement
@op-display permets de gérér l'affichage de ces opérateurs dans l'interface. @op-display=true/false exclusivement.
avec @op-display=)true => dans le cas des [list @variant=fixed @selection=multiple] et des suggest multiples les operateurs s'afficheront losrqu'il y a plus de 2 selections.

suggest@multiple=false : suggest simple à une seule entrée

key@type=list@variant=combo@selection=multiple : fonctionnement similaire au suggest
pb losrqu'on l'associe avec un @display-field ça ne s'affiche pas correctement

@type=text
Pour ajouter la troncature automatiquement à la validation du formulaire, il suffit d'ajouter l'attribut @truncate="left|right|both" au key@type='text'.
Le pré-remplissage du formulaire après avoir cliquer sur "Modifier votre requete" fonctionne aussi. (erreur sdx sur left à vérifier )

@terms-browse
Active l'aide à la recherche de type "navigation d'index" ; c'est-à-dire un
bouton supplémentaire dans l'interface de recherche affichant un panneau listant
tous les termes indexés dans un le champ de recherche courant.
@terms-browse peut contenir "true" ou "1" pour activer cette aide.
@terms-browse peut être vide ou contenir "false" ou "0" pour désactiver cette aide.
@terms-browse peut contenir le nom d'un autre champ pour activer cette aide en utilisant le contenu de cet autre champ.
-->
