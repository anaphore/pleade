//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion du formulaire de modification des paramètres d'affichage
 * @author Julien Huet
 */
/**
 * @class Gestion du formulaire de modification des paramètres d'affichage
 * @author Julien Huet
 * @augments HelpPanels
 * @augments EditFormValidator
 */
var DisplayParamsForm = new Class( /** @lends DisplayParamsForm.prototype */ {
	Implements: [HelpPanels,EditFormValidator],
	
	/**
	 * @param {Array} indexLocal
	 */
	initialize: function(indexLocal){
		this.initHelpPanels();
		this.suggest = new AdminSuggest();
		this.suggest.initSuggests(null,null, indexLocal);
	},
	
	/**
	 * Fonction d'appeler la validation du formulaire
	 * @param {Object} form
	 */
	submitForm: function(form){
		//alert('submitForm');
		return this.form2work(form);
	},
	
	/**
	 * Verification du formulaire
	 * @param {Element} myForm Forumaire a vérifier
	 */
	form2work: function(myForm) {
		if( myForm.elements.length == 0 ) {
			return false;
		}
		var _this = this;
		$A(this.suggest.suggests).clean().each(function(o, i) {
			var idi = o.id + '-input',
					idv = o.id + '-values';
			if( $(idi)==null || $(idv)==null );
			else _this.suggest.suggest2work( $(idi), $(idv) );
		});
		var mod = myForm.elements['modify'];
		if (mod == null || mod.value == null) {
			return false;
		}
		mod.value = 'true';
		return true;
	}
	
	
});
