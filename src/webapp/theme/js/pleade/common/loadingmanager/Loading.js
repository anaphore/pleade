//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion des messages d'attente
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion des messages d'attente
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
var Loading = new Class( /** @lends Loading.prototype */ {

	/**
	* <strong>Constructor</strong><br/>  Creation d'un message d'attente. Ce message est contenu dans un div 
	* @param {Element} div le div ou devra apparaitre la fenetre d'attente
	* @param {String} imgPath l'image qui devra apparaitre lors de la bar d'attente
	* @param {String} message le message  qui devra apparaitre pour la bar d'attente
	*/
	initialize: function(div,imgPath,message){
		this.div = $(div).getParent();
		if ( !imgPath ) {
			this.imgPath = 'theme/images/pnl-loading.gif';
		} else {
			this.imgPath = imgPath;
		}
		if ( !message ) {
			this.msg = 'Chargement en cours...'; //TODO i18n ; mais dans quel catalogue ? La présente classe est utilisée à plusieurs endroits
		} else {
			this.msg = message;
		}
		this.autoDestroy = false;
	},

	/**
	* Fonction qui permet de creer le div qui contiendra le message
	* @private 
	*/
	initLoadingDiv: function(){
		return new Element('div', { 'id': 'loading' });
	},

	/**
	* Pour afficher le message
	* @public
	*/
	start: function(){
		this.loadingDiv = this.initLoadingDiv();
		var img = new Element('img', { 'src': this.imgPath })
		var msg = new Element('span').set('text',this.msg);
		this.loadingDiv.adopt(img,msg);
		this.div.adopt(this.loadingDiv);

		//center on screen
		this.loadingDiv.setStyles({
			left: (this.div.getSize().x - this.loadingDiv.getSize().x ) / 2,
			top: (this.div.getSize().y - this.loadingDiv.getSize().y ) /2,
			'z-index': 1000
		});

		$('body').setStyle('cursor','wait');
	},

	/**
	* Pour arrêter et faire disparaitre le message
	* @public
	*/
	stop: function(){
		if ( this.loadingDiv ) {
			this.loadingDiv.dispose();
		}
		$('body').setStyle('cursor', 'auto');
	}
});