<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		| XSLT qui adapte le fichier de conf du formulaire de recherche personnalisé 
		| pour qu'il filtre sur l'IR en question dans la fenêtre EAD.
		+-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

		<xsl:param name="eadid" select="''"/>

		<xsl:template match="/">
			<xsl:apply-templates />
		</xsl:template>

		<xsl:template match="form">
			<form>
				<xsl:apply-templates select="@*"/>
				<key type="hidden" connector="2" field="root-id" value="{$eadid}" />	
				<xsl:apply-templates select="node()"/>
			</form>
		</xsl:template>

		<xsl:template match="key">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*"/>
				<filters>
					<filter field="root-id" value="{$eadid}"/>
			 	</filters>
			</xsl:copy>
		</xsl:template>

		<!--un template de copie-->
		<xsl:template match="node()|@*">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*"/>
			</xsl:copy>
		</xsl:template>

</xsl:stylesheet>