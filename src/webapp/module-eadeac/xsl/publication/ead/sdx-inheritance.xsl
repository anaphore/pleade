<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:saxon="http://saxonf.sf.net"
	exclude-result-prefixes="saxon xs xsl fn"
>

	<!-- XSLT qui permet de copier les valeurs héritées -->
	
	<!-- Des utilitaires pour la publication -->
	<xsl:import href="publication-helpers.xsl"/>
	
	<xsl:param name="debug" select="'false'"/>

	<!-- Un pointeur sur le fichier de propriétés de publication -->
	<!--<xsl:variable name="properties-url" select="concat('cocoon://functions/ead/get-properties/', /sdx:document/@id, '/publication.xml')"/>
	<xsl:variable name="pub-properties" select="if(doc-available($properties-url)) then document($properties-url)/properties else ()"/>-->

	<!-- Les types d'index à hériter -->
	<xsl:variable name="inheritance-index" select="fn:tokenize($pub-properties/property[@name='inheritance-index'], '\s')"/>
	<!-- Les champs à hériter -->
	<xsl:variable name="inheritance-always" select="fn:tokenize($pub-properties/property[@name='inheritance-always'], '\s')"/>
	<xsl:variable name="inheritance-empty" select="fn:tokenize($pub-properties/property[@name='inheritance-empty'], '\s')"/>

	<xsl:template match="/">
		<xsl:message>	Traitement de l'heritage des champs (etape 13 / 14) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
		<xsl:choose>
			<xsl:when test="$debug='true'">
				<xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
				<xsl:copy-of saxon:read-once="yes" select="$result"/>
				<xsl:if test="$debug='true'">
					<xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{sdx:document/@id}/{sdx:document/@id}-13.xml"
						xmlns:sys="java:java.lang.System">
						<xsl:copy-of saxon:read-once="yes" select="$result"/>
					</xsl:result-document>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement d'un document SDX -->
	<xsl:template match="sdx:document">
		<!-- On le copie -->
		<xsl:copy>
			<!-- On copie les attributs -->
			<xsl:apply-templates select="@*"/>
			<!-- Un pointeur sur l'élément courant -->
			<xsl:variable name="current" select="."/>
			<!-- On ajoute les informations héritées dans une variable -->
			<xsl:variable name="new-fields">
				<!-- Les index -->
				<xsl:for-each select="$inheritance-index">
					<xsl:variable name="type" select="."/>
					<xsl:copy-of saxon:read-once="yes" select="$current/ancestor::sdx:document/sdx:field[@pleade:type=concat($type, '-sdxfield')]"/>
					<!-- Le cas particulier de l'index word -->
					<xsl:if test="$type = 'global'">
						<xsl:copy-of saxon:read-once="yes" select="$current/ancestor::sdx:document/sdx:field[@pleade:type='word-sdxfield']"/>
					</xsl:if>
				</xsl:for-each>
				<!-- Les champs à toujours hériter -->
				<xsl:for-each select="$inheritance-always">
					<xsl:variable name="name" select="."/>
					<xsl:copy-of saxon:read-once="yes" select="$current/ancestor::sdx:document/sdx:field[@name=$name]"/>
				</xsl:for-each>
				<!-- Les champs à hériter si on n'en n'a pas déjà un -->
				<xsl:for-each select="$inheritance-empty">
					<xsl:variable name="name" select="."/>
					<xsl:if test="not($current/sdx:field[@name = $name])">
						<xsl:copy-of saxon:read-once="yes" select="$current/ancestor::sdx:document[sdx:field[@name=$name]][1]/sdx:field[@name=$name]"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<!-- On copie les nouveaux champs -->
			<xsl:copy-of saxon:read-once="yes" select="$new-fields"/>
			<!-- On ajoute au plein texte les nouveaux champs -->
			<xsl:if test="$new-fields">
				<sdx:field name="fulltext">
					<xsl:apply-templates select="$new-fields" mode="fulltext"/>
				</sdx:field>
			</xsl:if>
			<!-- On copie les sous-éléments -->
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- Traitement d'un noeud pour un champ word -->
	<xsl:template match="node()" mode="fulltext">
		<xsl:choose>
			<xsl:when test="self::*">
				<xsl:text> </xsl:text>
				<xsl:apply-templates mode="fulltext"/>
			</xsl:when>
			<xsl:when test="self::text()">
				<xsl:value-of select="normalize-space(.)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- On intercepte l'attribut @pleade:type qui ne sert à rien dans SDX -->
	<xsl:template match="sdx:field/@pleade:type"/>

	<!-- De manière générale, on copie tout -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
