//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion de la validation le formulaire de publication et d'edition de la partie admin
 * @author Julien Huet
 */
/**
 * @class Gestion de la validation le formulaire de publication et d'edition de la partie admin
 * @author Julien Huet
 * @augments SuggestManager
 */
var EditFormValidator = new Class(/** @lends EditFormValidator.prototype */{
	Implements: [SuggestManager],
	
	/**
	 * <strong>Constructor</strong><br/>
	 */
	initialize: function(){},
	
	/** Valider le parametre du document selectionne a publier
	 * @private
	 * @param {Object} o Objet a valider
	 */
	valideLfc: function(o){
		if(o == null || o.id == null || o.id.blank() ) {
			return false;
		}
		if( o.id == 'docs-select' ) {
			var v = o.options[o.selectedIndex];
			if ( v == null || v.value == null || v.value.blank() ) {
				return false;
			}
			if ( $('docs-zip') != null ) {
				$('docs-zip').value = '';
			}
			lfc = o.id;
		} else if ( o.id == 'docs-zip' ) {
			if ( o.value == null || o.value.blank() ) {
				return false;
			}
			if ( $('docs-select') != null ) {
				$('docs-select').selectedIndex = 0;
			}
			lfc = o.id;
		}
		return true;
	},
	
	/**
	 * Verification des champs du formulaire d'edition 
	 * @param {Element} myForm
	 */
	simpleForm2work: function(myForm) {
		if ( myForm.elements.length==0 ) {
			return false;
		}
		var go = false;
		//Modification du @name du controle de selection d'un repertoire ou d'un fichier
		var zipz = $('docs-zip');
		var dird = $('docs-select');
		var lfcv = '';
		if ( lfc == null ) {
			return false;
		}
		/* Pour avoir la possibilité de publier depuis une liste */
		if ( lfc == 'docs-select' ) {
			var opt = dird.options[dird.selectedIndex];
			if ( opt == null || opt.value == null || opt.value.blank() );
			else {
				lfcv = opt.value;
				dird.name = 'dird';
				if ( lfcv.toLowerCase().endsWith('.xml') ) {
					dird.name = 'filef';
				}
				if ( zipz != null ) {
					zipz.name += 'no';
					zipz.value = '';
				}
			}
		} else if ( lfc == 'docs-zip' ) {
			lfcv = zipz.value;
			zipz.name = 'zipz';
			if ( !lfcv.toLowerCase().endsWith('.zip') ) {
				zipz.name='filef';
			}
			if ( lfcv.toLowerCase().endsWith('.csv') ) {
				myForm.action = 'csv-publish.html';
			}
			if ( lfcv.toLowerCase().endsWith('.txt') ) {
				myForm.action = 'tdd-publish.html';
			}
		}
		if ( lfcv == null || lfcv.blank() ) {
			go = false;
		} else {
			go = true;
		}
		//Un message d'erreur si on n'a pas selectionner de document
		if ( !go ) {
			alert(_usersMessages.edit_ead_publish_msg_no_doc_selected);
			return go;
		}
		return go;
	},
	
	/**
	 * Verification du formulaire de publication avant envoi
	 * @param {Object} myForm
	 */
	form2work: function(myForm) {
		if( myForm.elements.length == 0 ) {
			return false;
		}
		var go = false;
		//Modification du @name du controle de selection d'un repertoire ou d'un fichier
		var dird = $('docs-select');
		var zipz = $('docs-zip');
		var docurl = $('docurl');
		var lfcv = '';
		var _this = this;
		if(lfc==null) return false;
		if ( lfc.blank() ) {
			if( docurl == null || docurl.value == null || docurl.value.blank() );
			else lfc = 'docurl';
		}
		if ( lfc == 'docs-select' ) {
			var opt = dird.options[dird.selectedIndex];
			if (opt==null || opt.value==null || opt.value.blank());
			else {
				lfcv = opt.value;
				dird.name = 'dird';
				if ( lfcv.toLowerCase().endsWith('.xml') ) {
					dird.name = 'filef';
				}
				if ( zipz != null ) {
					zipz.name += 'no';
					zipz.value = '';
				}
				if ( docurl != null ) {
					docurl.name += 'no';
				}
			}
		} else if ( lfc == 'docs-zip' ) {
			lfcv = zipz.value;
			zipz.name = 'zipz';
			if ( !lfcv.toLowerCase().endsWith('.zip') ) {
				zipz.name = 'filef';
			}
			if ( lfcv.toLowerCase().endsWith('.csv') ) {
				myForm.action = 'csv-publish.debug';
			}
			if ( dird != null ) {
				dird.selectedIndex = 0;
				dird.name += 'no';
			}
			if ( docurl != null ) {
				docurl.name += 'no';
			}
		} else if ( docurl != null ) {
			if( docurl.value == null || docurl.value.blank() );
			else {
				lfcv = docurl.value;
				docurl.name = 'filef';
				if ( zipz != null ) {
					zipz.name += 'no';
					zipz.value = '';
				}
				if ( dird != null ) {
					dird.name += 'no';
					dird.selectedIndex = 0;
				}
			}
		}
		if( lfcv == null || lfcv.blank() ) {
			go=false;
		} else {
			go=true;
		}
		//DEBUG alert("lfc: "+(lfc==null?"null":lfc)+" :: lfcv:"+(lfcv==null?"null":lfcv)+" :: lfc.name: "+(lfc==null?"null":$(lfc).name));
		// DEBUG alert("myForm.enctype: "+myForm.enctype);
		//Un message d'erreur si on n'a pas selectionner de document
		if(!go) {
			alert(_usersMessages.edit_ead_publish_msg_no_doc_selected);
			return go;
		}
		// Formatage des "autocomplete": on place les valeurs selectionnees dans l'input/@type=texte correspondant -->
		var suggests = $$('div.suggest');
		if ( $A(suggests) != null ) {
			$A(suggests).clean().each( function(o,i){
				var idi = o.id + '-input',
				    idv = o.id + '-values';
				if( $(idi)==null || $(idv)==null );
				else _this.suggest2work( $(idi), $(idv) );
			});
		}
		// Construction de la valeur du champ inheritance-index
		var oId = 'inheritance-index';
		if ( $(oId)!=null ) {
			var v = "";
			if ( $(oId+'-global').checked==true ) {
				v = ' global ';
			}
			if ( $(oId+'-local').checked==true ) {
				v += ' local ';
			}
			$(oId).value = v.trim();
		}
		return go;
	}
});
