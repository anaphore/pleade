<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->

<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    xmlns:pleade="http://pleade.org/ns/pleade/1.0"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xsl xs i18n pleade fn">

  <!-- XSLT qui produit une partie de table des matières à sérialiser en JSON.
      Elle s'applique au fichier de table des matières généré par le processus
      de publication de Pleade, mais inclut dans un élément root qui contient
      également des propriétés de publication pour la partie dynamique. -->

  <!-- Méthodes communes -->
  <xsl:import href="common.xsl"/>

  <!-- Pour le mode statique -->
  <xsl:output method="text" indent="yes" encoding="UTF-8" omit-xml-declaration="no"  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>
  <xsl:output name="json" method="text" encoding="UTF-8"/>

  <!-- Le nombre maximal d'enfants à afficher avant de couper (passé par le sitemap) -->
  <xsl:param name="max" select="50"/>

  <!-- Si ce paramètre est un nombre > 1, alors on doit envoyer seulement une
      partie -->
  <xsl:param name="n" select="0"/>

  <!-- Composant à visualiser : valeur du paramètre d'URL c -->
  <xsl:param name="child2show" select="''"/>

  <!-- Le mode de consultation : dyn ou static -->
  <xsl:param name="mode" select="'static'"/>

  <!-- L'identifiant de l'entrée de tdm à sortir (pour mode dynamique uniquement) -->
  <xsl:param name="c" select="''"/>

  <xsl:variable name="qid" select="''"/><!-- obligatoire pour l'import de common.xsl, meme si pas utilisé ici : FIXME  [MR] : comment faire pour éviter ça ?-->

  <!-- On se définit une variable pour traiter le cas de l'absence du paramètre -->
  <xsl:variable name="no">
    <xsl:choose>
      <xsl:when test="number($n) and number($n) > 0"><xsl:value-of select="number($n)"/></xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- La propriété sur le contrôle des caractères -->
  <xsl:variable name="title-limit">
    <xsl:variable name="prop" select="/root/properties[@id = 'publication']/property[@name='title-limit']"/>
    <xsl:choose>
      <xsl:when test="$prop and number($prop) and number($prop) > 0"><xsl:value-of select="number($prop)"/></xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!--=> FIXME :  ne pas s'occuper de @pleade:break = 'true' ni des dsc-->

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test=" $mode = 'static' ">
        <xsl:apply-templates select="ead"/>
      </xsl:when>
      <xsl:otherwise> <!--$mode='dyn'-->
        <!-- En dynamique, on traite l'élément c souhaité -->
        <root array="true"><xsl:apply-templates select="//*[@pleade:id = $c][1]" mode="make-json-dyn"/></root>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- FIXME: toujours utile? -->
  <xsl:template match="*" />

  <!-- Traitement du document complet, en mode statique -->
  <xsl:template match="ead">
    <!--génère le fichier json pour le 1er niveau de l'arbre-->
    <xsl:result-document href="{@pleade:id}.json" format="json">
      <xsl:call-template name="make-json"/>
    </xsl:result-document>
    <!--générer les autres fichiers json-->
    <xsl:apply-templates mode="make-json"/>
  </xsl:template>

  <!-- Traitement d'une entrée en mode statique -->
  <xsl:template match="c" mode="make-json">
    <xsl:if test="c"> <!--pas de json s'il n'y a rien à mettre dedans-->
      <xsl:result-document href="{@pleade:id}.json" format="json">
          <xsl:call-template name="make-json"/>
      </xsl:result-document>
    </xsl:if>
    <xsl:apply-templates mode="make-json"/>
  </xsl:template>

  <!-- Un noeud racine en mode dynamique -->
  <xsl:template match=" ead | c " mode="make-json-dyn">
    <xsl:call-template name="make-json"/>
  </xsl:template>

  <!-- Les entrées d'un noeud racine de la toc à sortir -->
  <xsl:template name="make-json">
    <!-- Le nombre d'enfants -->
    <xsl:variable name="nb" select="count(*)"/>
    <!-- L'identifiant -->
    <xsl:variable name="id" select="@pleade:id"/>
    <xsl:choose>
      <!-- Le cas où l'on demande une partie seulement : $no > 0, suite à un regroupement préalable -->
      <xsl:when test="$no > 0">
        <xsl:apply-templates select="*[(position() >= $no) and (position() &lt; $no + $max)]" mode="json-content"/>
      </xsl:when>
      <!-- Le cas où on ne dépasse pas la limite -->
      <xsl:when test="$nb &lt;= $max">
        <xsl:apply-templates select="*" mode="json-content"/>
      </xsl:when>
      <!-- Le cas où on dépasse la limite -->
      <xsl:otherwise>
        <!-- On boucle sur les début de groupes -->
        <xsl:for-each select="(1 to $nb)[. mod $max = 1]">
          <c>
            <!-- FIXME: i18n pour cela? -->
            <label><xsl:value-of select="."/> ... <xsl:value-of select="fn:min((number(.) + $max - 1, $nb))"/></label>
            <!-- Adresse -->
            <href><xsl:value-of select="pleade:get-href($id, ., $mode)"/></href><!-- FIXME: #? -->
            <!-- Il a nécessairement des enfants -->
            <hasChildren>true</hasChildren>
          </c>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- [CB] MENAGE : n'est utilisé nulle part 
  <xsl:template name="make-json-old">
    <xsl:text>[</xsl:text>
      <xsl:apply-templates mode="json-content"/>
    <xsl:text>]</xsl:text>
  </xsl:template> -->

  <!-- Sortie d'un élément c en json -->
  <xsl:template match="c" mode="json-content">
    <c>
      <xsl:call-template name="process-c-json"/>
    </c>
  </xsl:template>

  <!-- Le contenu réel d'un <c> en json -->
  <xsl:template name="process-c-json">
    <!-- Etiquette -->
    <label datatype="string">
      <xsl:copy-of select="pleade:get-label(@pleade:title, .)" />
    </label>
    <!-- Adresse -->
    <href><xsl:value-of select="concat(@pleade:id, '.html')"/></href><!-- FIXME: #? -->
    <!-- Enfants -->
    <hasChildren><xsl:value-of select="exists(c)"/></hasChildren>
    <xsl:if test="$child2show != '' and descendant::c[@pleade:id=$child2show]">
      <subroot array="true">
        <xsl:apply-templates select="child::c" mode="json-content"/>
      </subroot>
    </xsl:if>

  </xsl:template>

  <!-- Une fonction pour retourner le label -->
  <xsl:function name="pleade:get-label">
    <xsl:param name="title"/>
    <xsl:param name="element"/>
    <xsl:choose>
      <!-- Si on a un titre, on l'utilise -->
      <xsl:when test="$title and $title != ''">
        <xsl:choose>
          <xsl:when test="$title-limit > 0">
            <xsl:choose>
              <xsl:when test="string-length($title) > $title-limit">
                <xsl:variable name="_v" select="substring($title, 1, $title-limit)" />
                <xsl:variable name="_s" select="if ( substring($_v, string-length($_v)) ='.' ) then ' [...]' else '...'" />
                <xsl:value-of select="concat($_v, $_s)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$title"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="$title"/></xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <i18n:text key="ead.toc.{$element/@pleade:original-element}"></i18n:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>


  <!-- Une fonction pour retourner une adresse d'un noeud -->
  <xsl:function name="pleade:get-href" as="xs:string">
    <!-- L'identifiant du noeud (peut être vide pour les groupes -->
    <xsl:param name="id"/>
    <!-- Le numéro du groupe -->
    <xsl:param name="no"/>
    <!-- Le mode -->
    <xsl:param name="mode"/>
    <xsl:choose>
      <xsl:when test="$mode = 'static'">
        <!-- Ici, on retourne simplement l'id avec .html -->
        <xsl:value-of select="concat($id, '.html')"/>
      </xsl:when>
      <xsl:when test="$no > 0">
        <!-- On a un numéro en mode dynamique, donc c'est une entrée groupée -->
        <xsl:value-of select="concat($id, '.html?n=', $no)"/>
      </xsl:when>
      <xsl:when test="$id != ''">
        <!-- On a un identifiant en mode dynamique, donc c'est une entrée normale -->
        <xsl:value-of select="concat($id, '.html')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>