<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	Construction de l'application.xconf
		+-->
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
	xmlns="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:map="http://apache.org/cocoon/sitemap/1.0"
	 version="2.0"
	exclude-result-prefixes="sdx map">

  <xsl:param name="cocoon-context" select="''" />
  <xsl:param name="application-id"/>

	<xsl:param name="sdx-data-dir" select="''" />
	<xsl:param name="sdx-database" select="''" />
	<xsl:param name="sdx-ead-repository" select="''" />
	<xsl:param name="sdx-eac-repository" select="''" />

	<!-- Les paramètres nécessaires pour configurer un entrepôt OAI -->
	<xsl:param name="ead-oai-repo-name" select="''"/>
	<xsl:param name="ead-oai-repo-admin-email" select="''"/>
	<xsl:param name="ead-oai-repo-nb" select="''"/>
	<xsl:param name="ead-oai-repo-records" select="''"/>
	<xsl:param name="public-url" select="''"/>

  <!-- Les variables globales -->
  <xsl:variable name="global" select="/root/map:sitemap/map:pipelines/map:component-configurations/global-variables" />

  <!-- Le application/@dataDir -->
  <xsl:variable name="_dataDir">
		<xsl:value-of select="$sdx-data-dir" />
  </xsl:variable>
  <xsl:variable name="dataDir">
    <xsl:choose>
      <xsl:when test="starts-with($_dataDir, 'context://')">
        <xsl:value-of select="concat($cocoon-context, substring-after($_dataDir, 'context://'))" />
      </xsl:when>
			<xsl:when test="starts-with($_dataDir, '${')">
				<xsl:variable name="n" select="substring-before(substring-after(normalize-space($_dataDir), '${'),'}')" />
				<xsl:variable name="u" select="concat('cocoon://functions/commons/get-input-module.xml?module=global&amp;name=', $n)" />
				<xsl:value-of select="if(doc-available($u)) then document($u)/get-input-module/input:attribute-values[@name=$n]/input:value[.!=''][1] else ''" xmlns:input="http://apache.org/cocoon/xsp/input/1.0" />
			</xsl:when>
      <xsl:otherwise><xsl:value-of select="$_dataDir" /></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

	<!--+
	    |	Création de la racine du fichier appication.xconf
	    +-->
	<xsl:template match="sdx:application | application">

		<application>

			<xsl:apply-templates select="@*" />
			<xsl:attribute name="id" select="$application-id"/>

			<xsl:if test="$dataDir!=''">
				<xsl:attribute name="dataDir">
					<xsl:choose>
						<xsl:when test="starts-with($dataDir, '/')">
							<xsl:value-of select="concat('file://', $dataDir)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dataDir" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>

			<xsl:apply-templates select="." mode="build-sdx-database" />
			<xsl:apply-templates select="node()[not(local-name()='database')]" />
			<!--<xsl:apply-templates select="@* | node()" />-->

		</application>

	</xsl:template>

	<!--+
			|	Définir le sdx:database dynamique si nécessaire
			|	La valeur est donnée par un paramètre de Sitemap : sdx-database
			|	Sa forme est : TYPE:codeDSI. Exemple : MYSQL:pleade3
			|	S'il en existe déjà un dans le fichier application.xconf original, on le
			|	surcharge
			+-->
	<xsl:template match="sdx:application | application" mode="build-sdx-database">

		<xsl:if test="$sdx-database!=''">
			<xsl:variable name="t" select="substring-before($sdx-database, ':')" />
			<xsl:variable name="d" select="substring-after($sdx-database, ':')" />
			<!-- On ne peut créer de sdx:database que si on a un type (eg, MYSQL) et un identifiant "dsi" (eg, pleade3) -->
			<xsl:if test="$t!='' and $d!=''">
				<database type="{$t}" dsi="{$d}" />
			</xsl:if>
		</xsl:if>

	</xsl:template>

	<!--+
			|	Définir le sdx:repository dynamique si nécessaire
			|	La valeur est donnée par un paramètre de Sitemap : sdx-ead-repository ou sdx-eac-repository
			|	Sa forme est : TYPE:codeDSI. Exemple : MYSQL:pleade3
			|	S'il en existe déjà un dans le fichier ea(d|c)-documentbase.xconf original,
			|	on le	surcharge
			+-->
	<xsl:template match="sdx:repositories | repositories">

		<!-- La base en cours de traitement -->
		<xsl:variable name="db" select="string(ancestor::sdx:documentBase/@id | ancestor::documentBase/@id)"/>

		<repositories>

			<xsl:apply-templates select="@*" />

			<!-- Le paramètre qui nous indique si on doit modifier les paramètres -->
			<xsl:variable name="param">
				<xsl:choose>
					<xsl:when test="$db = 'ead'"><xsl:value-of select="$sdx-ead-repository"/></xsl:when>
					<xsl:when test="$db = 'eac'"><xsl:value-of select="$sdx-eac-repository"/></xsl:when>
				</xsl:choose>
			</xsl:variable>

			<xsl:choose>

				<!-- On a de quoi créer un entrepôt dynamiquement -->
				<xsl:when test="$param!=''">
					<xsl:variable name="t" select="substring-before($param, ':')" />
					<xsl:variable name="d" select="substring-after($param, ':')" />
					<xsl:if test="$t!='' and $d!=''">
						<repository type="{$t}" dsi="{$d}" id="{$db}-docs" default="true" />
					</xsl:if>
				</xsl:when>

				<!-- Pas de quoi créer un entrepôt dynamiquement. On s'en remet au fichier
				original. -->
				<xsl:otherwise>
					<xsl:apply-templates select="node()" />
				</xsl:otherwise>

			</xsl:choose>

		</repositories>

	</xsl:template>

	<!--+
	    | Création du sdx:respository[@type='FS']/@baseDirectory
			|	Le but est de définir la racine d'un entrepôt de type "FileSystem"
			| suivant le paramètre de Sitemap "sdx-data-dir" indiquant le chemin
			| complet du dossier devant contenir les données systèmes.
	    +-->
	<xsl:template match="sdx:repository[@type='FS']/@baseDirectory
												| repository[@type='FS']/@baseDirectory">
		<xsl:variable name="basedir" select="normalize-space(.)" />
		<xsl:variable name="s">
			<xsl:choose>
				<xsl:when test="$dataDir!=''">
					<xsl:choose>
						<xsl:when test="ends-with($dataDir, '/')
										and starts-with($dataDir, '/')">
								<xsl:value-of select="concat( $dataDir, substring-after($basedir, '/') )" />
						</xsl:when>
						<xsl:when test="not( ends-with( $dataDir, '/' ) )
														and not( starts-with( $basedir, '/' ) )">
								<xsl:value-of select="concat( $dataDir, '/', $basedir )" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat( $dataDir, $basedir )" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$basedir" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:attribute name="{name()}">
			<xsl:choose>
				<xsl:when test="starts-with($s, '/')">
					<xsl:value-of select="concat('file://', $s)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$s" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>

	<!-- Interception de la racine de l'entrepôt OAI (TODO: tenir compte de l'EAC) -->
	<xsl:template match="oai-repository">
    <!-- La base en cours de traitement -->
		<xsl:variable name="db" select="string(ancestor::sdx:documentBase/@id | ancestor::documentBase/@id)"/>
    <xsl:variable name="oai-repo-name" select="normalize-space($global/*[local-name()=concat($db,'-oai-repo-name')])" />
    <xsl:variable name="oai-repo-admin-email" select="$global/*[local-name()=concat($db,'-oai-repo-admin-email')]" />
		<!-- On le sort uniquement si la configuration contient un name et un admin-email. -->
		<xsl:if test="$oai-repo-name != '' and $oai-repo-admin-email != ''">
			<xsl:copy>
				<xsl:apply-templates select="@*">
          <xsl:with-param name="db" select="$db" />
          <xsl:with-param name="oai-repo-name" select="$oai-repo-name" />
          <xsl:with-param name="oai-repo-admin-email" select="$oai-repo-admin-email" />
        </xsl:apply-templates>
        <xsl:if test="not(@id!='')">
          <xsl:attribute name="id"><xsl:value-of select="concat('oai-', parent::documentBase/@id)"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="not(@default!='') or not(contains(' true false ',@default))">
          <xsl:attribute name="default"><xsl:value-of select="if(count(../oai-repository)=1) then 'true' else 'false'"/></xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="node()">
          <xsl:with-param name="db" select="$db" />
          <xsl:with-param name="oai-repo-name" select="$oai-repo-name" />
          <xsl:with-param name="oai-repo-admin-email" select="$oai-repo-admin-email" />
        </xsl:apply-templates>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!-- On intercepte les attributs OAI à modifier -->
	<xsl:template match="oai-repository/@name">
    <xsl:param name="oai-repo-name" select="''" />
		<xsl:attribute name="name"><xsl:value-of select="$oai-repo-name"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="oai-repository/@baseURL">
		<xsl:attribute name="baseURL"><xsl:value-of select="$public-url"/>/ead-oai</xsl:attribute>
	</xsl:template>
	<xsl:template match="oai-repository/@adminEmail">
    <xsl:param name="oai-repo-admin-email" select="''" />
		<xsl:attribute name="adminEmail"><xsl:value-of select="$oai-repo-admin-email"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="oai-repository/@noPerResponse">
    <xsl:param name="db" select="''" />
		<xsl:attribute name="noPerResponse"><xsl:value-of select="normalize-space($global/*[local-name()=concat($db,'-oai-repo-nb')])"/></xsl:attribute>
	</xsl:template>
	<!-- Pour le subset, on ajuste en fonction du type d'enregistrements souhaités -->
	<xsl:template match="oai-repository/oai-subset">
    <xsl:param name="db" select="''" />
    <xsl:variable name="oai-repo-records" select="normalize-space($global/*[local-name()=concat($db,'-oai-repo-records')])" />
		<xsl:copy>
			<xsl:choose>
				<xsl:when test="$oai-repo-records = 'components'">
					<!-- Toutes les unités de description -->
					<exclude query="type:ead type:eadheader type:div type:frontmatter type:titlepage"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- Uniquement les instruments de recherche eux-mêmes -->
					<include query="type:ead"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>
	<!-- Recopier les noeuds -->
	<xsl:template match="comment()">
		<!-- FIXME: Veut-on les commentaires ? Ici, ils sont interceptés [MP] -->
	</xsl:template>
  <xsl:template match="map:sitemap"/>
  <xsl:template match="/root">
    <xsl:apply-templates />
  </xsl:template>
	<xsl:template match="node()|@*" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
