<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: pre-processing.xsl 18711 2010-04-15 11:23:22Z jcwiklinski $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Les documents EAC arrivent avec :
			- élément racine eac (ancienne version de la DTD) ou eac-cpf
			- namespace par défaut => à supprimer pour la suite des traitements
			- peuvent inclure d'autres namespaces, à conserver
		N.B. : les fiches EAC valident suivant l'ancienne version de la DTD doivent être converties dans le nouveau format de la DTD. Les changements ne concernent pas que l'élément racine.
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:saxon="http://saxonf.sf.net"
  xmlns=""
	exclude-result-prefixes="xsl fn saxon xsi"
>

	<!-- Transformation d'identité. A surcharger si les documents EAC doivent être
			modifiés à l'entrée, avant publication.
	-->

    <xsl:param name="debug" select="'false'"/>

		<xsl:template match="/">
			<xsl:message></xsl:message>
			<xsl:message>Indexation de <xsl:value-of select="//*[local-name() = 'control']/*[local-name() = 'recordId']"/></xsl:message>
			<xsl:message>Debut de l'analyse : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
      <xsl:choose>
        <xsl:when test="$debug='true'">
          <xsl:variable name="result">
            <xsl:copy>
              <xsl:apply-templates />
            </xsl:copy>
          </xsl:variable>
          <xsl:copy-of saxon:ead-once="yes" select="$result"/>
          <xsl:if test="$debug='true'">
            <xsl:message> Le mode debogage est active ; le document sera copie apres chaque transformation dans le dossier : <xsl:value-of select="concat(sys:getProperty('java.io.tmpdir'),'/',//*[local-name() = 'control']/*[local-name() = 'recordId'])" xmlns:sys="java:java.lang.System"/></xsl:message>
            <xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{//*[local-name() = 'control']/*[local-name() = 'recordId']}/{//*[local-name() = 'control']/*[local-name() = 'recordId']}-1.xml"
              xmlns:sys="java:java.lang.System">
              <xsl:copy-of saxon:read-once="yes" select="$result"/>
            </xsl:result-document>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy>
            <xsl:apply-templates />
          </xsl:copy>
        </xsl:otherwise>
      </xsl:choose>

		</xsl:template>
		
	<!-- Dans la dernière version de l'EAC, l'élément racine n'est plus eac mais eac-cpf
			 On va convertir les anciennes racines.	
	-->
	<xsl:template match="*[local-name() = 'eac']">
		<xsl:element name="eac-cpf">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>
	
	<!-- On supprime le namespace par défaut (urn:isbn:1-931666-33-4) -->
	<xsl:template match="*[local-name() = 'eac-cpf']">	
		<xsl:copy>
			<xsl:apply-templates select="@*[not(namespace-uri() = 'http://www.w3.org/2001/XMLSchema-instance')]|node()"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- On copie tout -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

