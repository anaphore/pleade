<?xml version="1.0" encoding="UTF-8"?>
<!--$Id: edit-get-logs2xhtml.xsl 15636 2009-07-08 14:29:23Z mpichot $-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		| Administration du module EAD-EAC
    | Exporter les logs de publication en format CSV (Comma Separated Values)
    | Pour Excel et OpenOffice, le séparateur des CSV est le point-virgule par
    | défaut.
		|
		| La sortie :
		| Une ligne par document publié avec succès ou non.
		|
		| La première colonne contient l'identifiant du document.
		| Dans le cas d'une publication complètement échouée (le processus n'a pas
		| pu être mené jusqu'à son terme), la colonne donnant l'identifiant du
		| document est "ECHEC DE LA PUBLICATION". Dans le cas d'un document dont la
		| publication a échouée, son identifiant est son nom de fichier.
		|
		| Une colonne marquant le succès ou l'échec de publication du document.
		| Une colonne pour l'identifiant de la publication
		| Une colonne pour la date de début de l'indexation
		| Une colonne pour la date de fin de l'indexation
		| Une colonne pour l'identifiant de la base de documents
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		xmlns:csv="http://apache.org/cocoon/csv/1.0"
		exclude-result-prefixes="xsl i18n xs csv">

	<xsl:import href="edit-get-logs-publication-cols-numbers.xsl"/>

	<xsl:param name="sep" select="';'" />
	<xsl:param name="newline" select="'&#10;'" />

  <!--+
      | Construction de la racine textuelle
      | Dans Cocoon, tout est XML, on doit avoir une racine XML temporaire que
      | le sérialiseur textuel supprimera.
      +-->
	<xsl:template match="/">

		<root-temp>
			<xsl:apply-templates select="csv:document" />
		</root-temp>

	</xsl:template>

	<!--+
	    | Match un csv:document, racine du document
	    +-->
	<xsl:template match="csv:document">

		<!-- Construction des en-têtes -->
		<xsl:apply-templates select="." mode="build-headers" />

		<!-- On s'appuie sur les "INDEXATION_INIT" uniquement car ce sont les seuls à être toujours présents, même si la publication n'a pas fonctionnée -->
		<xsl:for-each select="csv:record[csv:field = 'INDEXATION_INIT']">
			<xsl:apply-templates select="." mode="indexation-init">
				<xsl:sort select="csv:field[@number=$colID]"/>
			</xsl:apply-templates>
		</xsl:for-each>

	</xsl:template>

	<!--+
	    | Construction des en-têtes
	    +-->
	<xsl:template match="csv:document" mode="build-headers">
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.documents-ids">documents</i18n:text><xsl:value-of select="$sep" />
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-is-ok">résultat de publication</i18n:text><xsl:value-of select="$sep" />
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-number">numéro de publication</i18n:text><xsl:value-of select="$sep" />
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-date.start">date de début</i18n:text><xsl:value-of select="$sep" />
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-date.end">date de fin</i18n:text><xsl:value-of select="$sep" />
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.publication-date.end">base de documents</i18n:text><xsl:value-of select="$sep" />
		<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.headers.message">message</i18n:text><xsl:value-of select="$sep" />
	</xsl:template>

	<!--+
	    | Match un csv:record : INDEXATION_INIT
	    +-->
	<xsl:template match="csv:record" mode="indexation-init">

		<!-- La clé de regroupement (ie, l'identifiant de la publication) -->
		<xsl:variable name="k" select="csv:field[@number=$colID]" />

		<!-- La liste des informations de cette publication -->
		<xsl:variable name="groupe" select="../csv:record[csv:field[@number=$colID] = $k]"/>

		<xsl:variable name="debut" select="csv:field[@number=$colStarts]"/>
		<xsl:variable name="fin" select="$groupe[csv:field[@number=$colStep]='INDEXATION_END']/csv:field[@number=$colEnds]"/>
		<xsl:variable name="dbid" select="csv:field[@number=$colDBID]"/>
		<xsl:variable name="pub-ok" select="if($fin!='') then 'Oui' else 'Non'"/>
		<xsl:variable name="docs-added-ids" select="$groupe[csv:field[@number=$colStep]='DOCS_IDS']/csv:field[@number=$colDocsAddedID]"/>
		<xsl:variable name="docs-error-ids" select="$groupe[csv:field[@number=$colStep]='DOCS_IDS']/csv:field[@number=$colDocsErrorID]"/>
		<xsl:variable name="error-messages" select="$groupe[csv:field[@number=$colStep]='ERROR_MESSAGES']/csv:field[@number=$colErrorMessages]"/>

		<xsl:choose>

		  <xsl:when test="$pub-ok = 'Non'">
				<!-- Echec de publication -->
				<xsl:value-of select="$newline" />
				<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publication-is-ok">echec de la publication</i18n:text><xsl:value-of select="$sep" />
				<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publication-is-ok.false">ko</i18n:text><xsl:value-of select="$sep" />
				<xsl:value-of select="concat($k,$sep,$debut,$sep,$fin,$sep,$dbid,$sep,'')" />
		  </xsl:when>

		  <xsl:otherwise>

				<!-- Les identifiants des documents publiés -->
				<xsl:if test="$docs-added-ids!=''">

					<xsl:variable name="l" select="replace(substring($docs-added-ids,2,(string-length($docs-added-ids) - 2)),', ',',')"/>

					<xsl:for-each select="tokenize($l,',')">
						<xsl:sort select="format-number(number(substring-before(.,'=')),'00000')"/>

						<!-- Une ligne pour chaque document publié avec succès -->
						<xsl:value-of select="$newline" />
						<xsl:value-of select="concat(substring-after(.,'='),$sep)" />
						<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publication-is-ok.true">ok</i18n:text><xsl:value-of select="$sep" />
						<xsl:value-of select="concat($k,$sep,$debut,$sep,$fin,$sep,$dbid,$sep,'')" />

					</xsl:for-each>

				</xsl:if>

				<!-- Les messages d'erreurs -->
				<xsl:if test="$error-messages!='' and $error-messages!='{}'">

					<xsl:variable name="e" select="replace(substring($docs-error-ids,2,(string-length($docs-error-ids) - 2)),', ',',')"/>
					<!--select="replace(substring($error-messages,2,(string-length($error-messages) - 2)),', ',',')"/>-->
					<xsl:variable name="m" select="replace(substring($error-messages,2,(string-length($error-messages) - 2)),', ',',')"/>
					<xsl:variable name="ms" select="tokenize($m,',')" />

					<xsl:for-each select="tokenize($e,',')">
						<xsl:sort select="format-number(number(substring-before(.,'=')),'00000')"/>

						<!-- Le rang du document -->
						<xsl:variable name="r" select="substring-before(.,'=')" />
						<!-- Le message d'erreur à prendre dans une autre ligne du log. On s'appuie sur le rang du document pour trouver le message du document courant. -->
						<xsl:variable name="msg" select="$ms[substring-before(.,'=') = $r][1]" />
						<!-- L'identifiant du document si on l'a (après le signe "=") sinon on prend le rang -->
						<!-- INFO (MP) : Dans les documents avec erreur, l'erreur peut
						intervenir avant que Pleade n'attribut un identifiant au document.
						Dans ce cas, on peut récupérer le nom du fichier qui sera plus
						explicite que son numéro dans le lot de documents à publier. La
						récupération du nom de fichier se fait en retirant la chaîne
						"file:/[...]" dans le message d'erreur. Ce nom de fichier avec path
						complet est passé à une classe Java statique qui découpe le path et
						renvoie uniquement le nom de fichier.
						Si la récupération du nom de fichier ne fonctionne pas, on prend le
						rang du document. -->
						<xsl:variable name="i" select="
								if ( substring-after(.,'=')!='' ) then substring-after(.,'=')
								else if ( $msg!='' and contains($msg,' file:/') ) then nu:getFilePart( concat( 'file:/', substring-before(substring-after($msg,' file:/'),' : ') ) )
								else $r
						" xmlns:nu="java:org.pleade.utils.NetUtilities"/>

						<!-- Une ligne pour chaque document dont la publication a échouée -->
						<xsl:value-of select="$newline" />
						<xsl:value-of select="concat($i,$sep)" />
						<i18n:text catalogue="{$i18n-cat}" key="{$i18n-root}.publication-is-ok.false">ko</i18n:text><xsl:value-of select="$sep" />
						<xsl:value-of select="concat($k,$sep,$debut,$sep,$fin,$sep,$dbid,$sep,substring-after($msg,concat($r,'=')))" />

					</xsl:for-each>

				</xsl:if>

			</xsl:otherwise>

		</xsl:choose>

	</xsl:template>

</xsl:stylesheet>
