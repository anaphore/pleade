<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSL qui pilote l'affichage de la page principale de gestion
		des utilisateurs.
-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="xsl i18n xhtml">

	<!-- Les paramètres suivants permettent de savoir quelles sources
			d'information sur les utilisateurs sont définies -->
	<xsl:param name="auth-file" select="'false'"/>
	<xsl:param name="auth-ldap" select="'false'"/>
	<xsl:param name="auth-sql" select="'false'"/>
	<xsl:param name="auth-arkheia" select="'false'"/>


	<!-- Cette XSLT opère sur un XML bidon (empty.xml) -->
	<xsl:template match="/">

		<html>
			<head>
				<title><i18n:text key="admin.users.home.title"/></title>
				<!-- TODO: quelles librairies devraient être dans toutes les pages ? -->
				<link href="yui/assets/skins/pleade/datatable.css" type="text/css" rel="stylesheet"/>
				<script type="text/javascript" src="yui/utilities/utilities.js">&#160;</script>
        <script type="text/javascript" src="js/mootools/mootools-core-1.3.1-nc.js">&#160; </script>
        <script type="text/javascript" src="js/mootools/mootools-more-1.3.1.1-nc.js">&#160; </script>
				<script type="text/javascript" src="js/pleade/common/mootools-common.js">&#160;</script>
				<script type="text/javascript" src="yui/datasource/datasource-min.js">&#160;</script>
				<script type="text/javascript" src="yui/button/button-min.js">&#160;</script>
				<script type="text/javascript" src="yui/datatable/datatable-min.js">&#160;</script>
				<!-- La librairie de gestion des utilisateurs -->
				<script type="text/javascript" src="i18n/pleade-admin-js.js">&#160;</script>
				<script type="text/javascript" src="js/pleade-admin.js">&#160;</script>
			</head>
			<body>
				<h1 class="pl-title"><i18n:text key="admin.users.home.title"/></h1>

				<!-- Une introduction avec des explications -->
				<div><i18n:text key="admin.users.home.intro"/></div>

				<!-- TODOLATER: il pourrait être intéressant de rendre ces sources plus génériques
						(plusieurs fichiers par exemple) et d'avoir leurs propriétés paramétrables. -->
				<!-- FIXME: mieux styler le tableau. -->
				<div id="sources">
					<table id="sources-table">
						<xsl:if test="$auth-file = 'true'">
							<xsl:call-template name="output-source">
								<xsl:with-param name="code" select="'file'"/>
								<xsl:with-param name="edit" select="true()"/>
								<xsl:with-param name="list" select="true()"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="$auth-sql = 'true'">
							<xsl:call-template name="output-source">
								<xsl:with-param name="code" select="'sql'"/>
								<xsl:with-param name="edit" select="false()"/>
								<xsl:with-param name="list" select="true()"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="$auth-ldap = 'true'">
							<xsl:call-template name="output-source">
								<xsl:with-param name="code" select="'ldap'"/>
								<xsl:with-param name="edit" select="false()"/>
								<xsl:with-param name="list" select="false()"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="$auth-arkheia = 'true'">
							<xsl:call-template name="output-source">
								<xsl:with-param name="code" select="'arkheia'"/>
								<xsl:with-param name="edit" select="false()"/>
								<xsl:with-param name="list" select="true()"/>
							</xsl:call-template>
						</xsl:if>
					</table>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- Sortie d'une rangée de tableau pour une source -->
	<xsl:template name="output-source">
		<xsl:param name="code"/>
		<xsl:param name="edit" select="false()"/>
		<xsl:param name="list" select="false()"/>
		<tr>
			<td><i18n:text key="admin.users.source.{$code}"/></td>
			<td>
				<xsl:choose>
					<xsl:when test="$list">
						<a href="users-list.html?src={$code}" title="admin.users.home.list.title" i18n:attr="title"><i18n:text key="admin.users.home.list"/></a>
					</xsl:when>
					<xsl:otherwise>&#xA0;</xsl:otherwise>
				</xsl:choose>
			</td>
			<td>
				<xsl:choose>
					<xsl:when test="$edit">
						<a href="users-list.html?src={$code}&amp;edit=true" title="admin.users.home.edit.title" i18n:attr="title"><i18n:text key="admin.users.home.edit"/></a>
					</xsl:when>
					<xsl:otherwise>&#xA0;</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>

