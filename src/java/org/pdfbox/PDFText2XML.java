/*
PLEADE: Free platform for viewing and searching XML/EAD finding aids.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
louis.colombani@arkheia.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/
package org.pdfbox;

import java.io.IOException;
import java.util.Iterator;

import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;
import org.pdfbox.util.TextPosition;

public class PDFText2XML extends PDFTextStripper 
{
    private static final int INITIAL_PDF_TO_HTML_BYTES = 8192;

    private String startPageMarker = "<page>";
    private String endPageMarker = "</page>";
    private String startParagraphMarker = "<para>";
    private String endParagraphMarker = "</para>";
    private boolean suppressParagraphs;
    private boolean onFirstPage = true;
   
    /**
     * Constructor.
     * 
     * @throws IOException If there is an error during initialization.
     */
    public PDFText2XML(String spage, String epage, String spara, String epara) throws IOException 
		{
			suppressParagraphs = false;
			this.startPageMarker = spage;
			this.endPageMarker = epage;
			this.startParagraphMarker = spara;
			this.endParagraphMarker = epara;
		}
    public PDFText2XML() throws IOException 
    {
        suppressParagraphs = false;
    }

    /**
     * Write the header to the output document.
     * 
     * @throws IOException If there is a problem writing out the header to the document.
     */
    protected void writeHeader() throws IOException 
    {
        StringBuffer buf = new StringBuffer(INITIAL_PDF_TO_HTML_BYTES);
        buf.append("<?xml version=\"1.0\"?>\n");
        buf.append("<pdf-content>\n");
        getOutput().write(buf.toString());
    }
   
    /**
     * {@inheritDoc}
     */
    protected void flushText() throws IOException 
    {
        Iterator textIter = getCharactersByArticle().iterator();
      
        if (onFirstPage) 
        {
            writeHeader();
            onFirstPage = false;
        }
        super.flushText();
    }
    
    /**
     * {@inheritDoc}
     */
    public void endDocument(PDDocument pdf) throws IOException 
    {
        output.write("</pdf-content>");      
    }

    protected void startPagae() throws IOException 
		{
			getOutput().write( this.startPageMarker );
		}

    protected void endPagae() throws IOException 
		{
			getOutput().write( this.endPageMarker );
		}

    /**
     * Write out the paragraph separator.
     * 
     * @throws IOException If there is an error writing to the stream.
     */
    protected void startParagraph() throws IOException 
    {
        if (! suppressParagraphs) 
        {
            getOutput().write( this.startParagraphMarker );
        }
    }
    /**
     * Write out the paragraph separator.
     * 
     * @throws IOException If there is an error writing to the stream.
     */
    protected void endParagraph() throws IOException 
    {
        if (! suppressParagraphs) 
        {
            getOutput().write( this.endParagraphMarker );
        }
    }
    
    /**
     * {@inheritDoc}
     */
    protected void writeCharacters(TextPosition position ) throws IOException 
    {
      
        String chars = position.getCharacter();

        for (int i = 0; i < chars.length(); i++) 
        {
            char c = chars.charAt(i);
//          Modification echapper les caracteres interdits en XML
            if ( !( c==0x9 || c==0xA || c==0xD 
									|| ( c >= 0x20 && c <= 0xD7FF )  
									|| ( c >= 0xE000 && c <= 0xFFFD ) ) ) 
						{
								output.write(" ");
						}
            else if ((c < 32) || (c > 126)) 
            {
                int charAsInt = c;
                output.write("&#" + charAsInt + ";");
            } 
            else 
            {
                switch (c) 
                {
                    case 34:
                        output.write("&quot;");
                        break;
                    case 38:
                        output.write("&amp;");
                        break;
                    case 60:
                        output.write("&lt;");
                        break;
                    case 62:
                        output.write("&gt;");
                        break;
                    default:
                        output.write(c);
                }
            }
        }
    }
    
    /**
     * @return Returns the suppressParagraphs.
     */
    public boolean isSuppressParagraphs()
    {
        return suppressParagraphs;
    }
    /**
     * @param shouldSuppressParagraphs The suppressParagraphs to set.
     */
    public void setSuppressParagraphs(boolean shouldSuppressParagraphs)
    {
        this.suppressParagraphs = shouldSuppressParagraphs;
    }
}
