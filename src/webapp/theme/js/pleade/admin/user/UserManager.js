//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Gestion des utilisateurs (ajout, suppréssion et affichage du tableau YUI)
 * @author Julien Huet
 */
/**
 * @class Gestion des utilisateurs (ajout, suppréssion et affichage du tableau YUI)
 * @author Julien Huet
 */
var UserManager = new Class(/**@lends UserManager.prototype */{

	/**
	 * <strong>Constructor</strong><br/> Inialisation du tableau
	 */
	initialize: function(){
		this.initUserTable();
	},

	/**
	* Initialisation du tableau des sources de données utilisateurs.
	* FIXME: à priori cette fonction n'est utilisée nulle part
	*/
	initSourcesTable: function() {
		// La source de données est un tableau dans le HTML
		var dataSource = new YAHOO.util.DataSource($('sources-table'));
		dataSource.responseType = YAHOO.util.DataSource.TYPE_HTMLTABLE;
		dataSource.responseSchema = {fields: [{key: 'source'}, {key: 'actionList'}, { key: 'actionEdit'}]}
		// Le datatable comme tel
		new YAHOO.widget.DataTable('sources',[
				{key: 'source', label: _usersMessages.users_sources_header_description, resizable: true},
				{key: 'actionList', label: _usersMessages.users_sources_header_action_list, resizable: true},
				{key: 'actionEdit', label: _usersMessages.users_sources_header_action_edit, resizable: true}
			],
			dataSource,
			{ summary: _usersMessages.sources_table_summary }
		);
	},

	/**
	* Initialisation du tableau qui liste des utilisateurs, éditables ou non.
	*/
	initUserTable: function() {
		// La définition des colonnes
		if ( sourceEditable ) {
			var _txtEditor = new YAHOO.widget.TextboxCellEditor({LABEL_SAVE: _usersMessages.users_list_button_save_text, LABEL_CANCEL:_usersMessages.users_list_button_cancel_text});
			var columns = [
				{
					key:'code',
					label:_usersMessages.users_list_headers_code,	
					sortable:true,
					resizeable:true,
					editor:_txtEditor
				},
				{
					key:'password',
					label:_usersMessages.users_list_headers_password,
					sortable:true,
					resizeable:true,
					editor:_txtEditor
				},
				{
					key:'roles',
					label:_usersMessages.users_list_headers_roles,
					sortable:true,
					resizeable:true,
					editor:_txtEditor
				},
				{
					key:'name',
					label:_usersMessages.users_list_headers_name,
					sortable:true,
					resizeable:true,
					editor:_txtEditor
				},
				{
					key:'email',
					label:_usersMessages.users_list_headers_email,
					sortable:true,
					resizeable:true,
					editor:_txtEditor
				},
				{
					key:'save',
					label:_usersMessages.users_list_headers_save,
					formatter:YAHOO.widget.DataTable.formatButton
				},
				{
					key:'delete',
					label:_usersMessages.users_list_headers_delete,
					formatter:YAHOO.widget.DataTable.formatButton
				}
			];
		} else {
			var columns = [
				{
					key:'code',
					label:_usersMessages.users_list_headers_code,
					sortable:true,
					resizeable:true
				},
				{
					key:'password',
					label:_usersMessages.users_list_headers_password,
					sortable:true,
					resizeable:true
				},
				{
					key:'roles',
					label:_usersMessages.users_list_headers_roles,
					sortable:true,
					resizeable:true
				},
				{
					key:'name',
					label:_usersMessages.users_list_headers_name,
					sortable:true,
					resizeable:true
				},
				{
					key:'email',
					label:_usersMessages.users_list_headers_email,
					sortable:true,
					resizeable:true
				}
			];
		}

		// La source de données : le tableau dans le HTML
		var dataSource = new YAHOO.util.DataSource($('users-table'));
		dataSource.responseType = YAHOO.util.DataSource.TYPE_HTMLTABLE;
		if ( sourceEditable ) {
			dataSource.responseSchema = {
				fields: [
					{key:'code'},
					{key:'password'},
					{key:'roles'},
					{key:'name'},
					{key:'email'},
					{key:'save'},
					{key:'delete'}
				]
			};
		} else {
			dataSource.responseSchema = {
				fields: [
					{key:'code'},
					{key:'password'},
					{key:'roles'},
					{key:'name'},
					{key:'email'}
				]
			};
		}

		// Le datable comme tel
		userTable = new YAHOO.widget.DataTable(
			'userslist',
			columns,
			dataSource,
			{
				summary: _usersMessages.users_list_table_summary,
				sortedBy:{key:'code'},
				paginated:false
			}
		);

		if ( sourceEditable ) {
			// On intercepte l'évnement qui correspond à un clic dans une cellule pour afficher l'éditeur
			userTable.subscribe('cellClickEvent', userTable.onEventShowCellEditor);
			// On intercepte l'événement qui indique une modification à une cellule
			userTable.subscribe('editorSaveEvent', this.handleEditorSave);
			// On intercepte l'événement qui indique qu'un bouton a été cliqué
			userTable.subscribe('buttonClickEvent', this.handleButtonClick);
			// Si on ajoute, on doit avoir un bouton pour ajouter un utilisateur
			var btnAdd = new YAHOO.widget.Button('btn-user-add', {onclick: {fn: this.handleButtonAddClick}});
		}
	},

	/**
	* Fonction qui est appelée lorsque le bouton d'ajout d'un utilisateur est activé.
	* @param {Event} e
	*/
	handleButtonAddClick: function(e) {
		userTable.addRow({
			code: '',
			password: '',
			roles: '',
			name: '',
			email: '',
			save: _usersMessages.users_list_button_save_text,
			'delete': _usersMessages.users_list_button_delete_text
		});
	},
	
	/**
	* Fonction qui est appelée une fois que l'éditeur enregistre
	* les données. S'il s'agit de la colonne "code", on conserve
	* l'information afin de pouvoir la récupérer pour mettre à jour
	* le bon utilisateur.
	
	* @param {Array} args contient trois propriétés: <br/>
	*		editor : l'éditeur associé à la cellule <br/>
	*		newData : la donnée saisie <br/>
	*		oldData : l'ancienne donnée <br/>
	*
	*		L'objet args.editor a une propriété (voir getCellEditor())
	*		record qui retourne un objet Record pour
	*		la ligne en cours d'édition. <br/>
	*
	*		Un objet Record a une méthode getData() qui retourne <br/>
	*		les données sous la forme de propriétés, selon la <br/>
	*		définition des sources. Par exemple getData().email. <br/>
	*/
	handleEditorSave: function(args) {
		//alert("UserManager.js handleEditorSave()");
		if ( args.editor.getColumn().key == 'code' && !originalUsernames[args.editor.getRecord().getId()] ) {
			originalUsernames[args.editor.getRecord().getId()] = args.oldData;
		}
	},

	/**
	* Fonction qui permet de sauvegarder les données d'une rangée.
	* @param {String} id L'identifiant de l'utilisateur, soit son code original
	* @param{Record} record  Les données du datatable.
	*/
	saveUser: function(id, record) {
		//alert("UserManager.js saveUser()");
		// On construit un tableau de paramètres
		var data = record.getData();
		//var params = JSON.encode({id: id, code: data.code, password: data.password, roles: data.roles, name: data.name, email: data.email});
		var params = 'id=' + id + '&code=' + data.code  + '&password=' + data.password + '&roles=' + data.roles + '&name=' + data.name + '&email=' + data.email;
		
		// On envoie les données par Ajax
		var req = new Request.JSON({
			url : 'users-edit-' + sourceCode + '.json',
			method: 'get',
			onSuccess: function(req){
				alert(_usersMessages.users_list_user_created);
	        	},
		});
		req.send(params);
	},

	/**
	* Supprime un utilisateur.
	* @param {String} id L'identifiant de l'utilisateur, soit son code original
	*/
	deleteUser: function(id, record) {
		var params = 'id=' + id + '&delete=true';

		var req = new Request({
			url : 'users-edit-' + sourceCode + '.json',
			method: 'get',
			onSuccess: function(req){
				// TODO: le faire savoir (CSS)
				userTable.deleteRow(record);
	        },
		});
		req.send(params);
	},

	/**
	* Fonction appelée lorsqu'un des boutons est cliqué.
	* @params {Array} args contient: <br/>
	*			event : l'événement <br/>
	*			target : le bouton <br/>
	*			this est le datatable. <br/>
	*/
	handleButtonClick: function(args) {
		//alert("UserManager.js handleButtonClick()");
		// On doit d'abord obtenir la clé, soit le code utilisateur original
		var record = this.getRecord(args.target);
		var primaryKey = primaryKey = originalUsernames[record.getId()];
		if ( !primaryKey ) primaryKey = record.getData().code;
		
		// Maintenant on peut lancer la suppression ou la sauvegarde
		var column = this.getColumn(args.target);
		if ( column.key == 'save' ) {
			// Sauvegarde
			userManager.saveUser(primaryKey, record);
		} else {
			// Suppression
			userManager.deleteUser(primaryKey, record);
		}
	}
});
