<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
    | Transformation de documents SDX en documents SOlr pour indexation
    | /!\ Uniquement disponible à titre de tests ; très gourmand en mémoire ! /!\
    +-->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
  xmlns:pleade="http://pleade.org/ns/pleade/1.0"
  exclude-result-prefixes="xsl fn">

  <xsl:template match="/">
    <add>
      <xsl:for-each select="//sdx:document">
        <xsl:call-template name="process-subdocs"/>
      </xsl:for-each>
    </add>
  </xsl:template>

  <xsl:template name="process-subdocs">
    <xsl:choose>
      <xsl:when test="//sdx:document">
        <xsl:call-template name="process-subdocs"/>
      </xsl:when>
      <xsl:otherwise>
        <doc>
          <field name="id"><xsl:value-of select="@id"/></field>
          <xsl:apply-templates />
        </doc>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="sdx:field">
    <field>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates/>
    </field>
  </xsl:template>

  <!-- For now, remove elements we cannot handle properly -->
  <xsl:template match="pleade:subdoc"/>
  <xsl:template match="sdx:document"/>

  <!-- We do not use comments -->
  <xsl:template match="comment()"/>

  <!-- All other contents will be copied -->
  <xsl:template match="*|text()|processing-instruction()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" />
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
