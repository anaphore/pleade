<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--+
		|	XSLT qui affiche le résultat d'une recherche
		+-->
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:sdx="http://www.culture.gouv.fr/ns/sdx/sdx"
		xmlns:xsp="http://apache.org/xsp"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:pleade="http://pleade.org/ns/pleade/1.0"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		exclude-result-prefixes="xsl xsp sdx i18n pleade">

		<!-- Des indications par défaut pour l'ouverture automatique des entrées -->
		<xsl:param name="auto-expand-default" select="''"/>

		<!-- Des indications dynamiques pour l'ouverture automatique des entrées -->
		<xsl:param name="auto-expand" select="''"/>

		<!--nom du formulaire-->
		<xsl:param name="name" select="''" />

    <!-- Indique si on souhaite récupérer uniquement les scripts nécessaires à
         la construction de la liste des résultats. On s'en sert notamment pour les subsets. -->
		<xsl:param name="get-cdc-results-script" select="''"/>

		<!--id du div dans lequel doivent apparaître les résultast-->
		<xsl:param name="divID" select="'_cdc_'"/>

		<xsl:param name="active-basket" select="'true'" />

		<!-- Langue d'interface -->
		<xsl:param name="locale" select="'fr'"/>

		<!--separateur de Liste pour javascript-->
		<xsl:param name="list-sep" select="''" />

		<xsl:variable name="form-id">
			<xsl:choose>
				<xsl:when test="$name!=''">
					<xsl:value-of select="$name" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>ead-default</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="real-div-id">
			<xsl:choose>
				<xsl:when test="$divID = ''">_cdc_</xsl:when>
				<xsl:otherwise><xsl:value-of select="$divID"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="n-start">
			<xsl:choose>
				<xsl:when test="/sdx:document/sdx:parameters/sdx:parameter[@name = 'n-start']">
					<xsl:value-of select="/sdx:document/sdx:parameters/sdx:parameter[@name = 'n-start']/@value"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="nbsp" select="'&#160;'"/>

    <!--+
        | Match la racine
        +-->
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="$get-cdc-results-script='true'">
        <!-- On renvoit juste le nécessaire pour la construction de la liste des documents. On s'en sert surtout pour la résolution des subsets. -->
        <div>
          <xsl:if test="$active-basket='true'">
          <script type="text/javascript" src="i18n/module-eadeac-basket-js.js">//</script>
          <!--<script type="text/javascript" src="js/module-eadeac-basket.js">//</script>-->
          </xsl:if>
          <script type="text/javascript">
            <xsl:comment>
            <xsl:apply-templates select="sdx:document" mode="cdc-results-script"/>
            //</xsl:comment>
          </script>
        </div>
			</xsl:when>
			<xsl:otherwise>
        <!-- On traite un résultat de recherche normal. -->
				<xsl:apply-templates select="sdx:document"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <!--+
      | Construction de la racine XHTML
      +-->
	<xsl:template match="/sdx:document">

	<!--<xsl:message>[cdc-result.xsl] n-start = <xsl:value-of select="$n-start"/></xsl:message>-->
	<!--<xsl:message>[cdc-result.xsl] sep ="<xsl:value-of select="$list-sep" />"</xsl:message>-->

		<html>

			<head>

				<title>
          <i18n:text key="results-title.page.{$form-id}">results-title.page.<xsl:value-of select="$form-id" /></i18n:text>
        </title>

                <link type="text/css" href="yui/assets/skins/pleade/treeview.css" rel="stylesheet"/>

				<script type="text/javascript" src="yui/treeview/treeview-min.js">//</script>
                <xsl:if test="$active-basket='true'">
                <script type="text/javascript" src="i18n/module-eadeac-basket-js.js">//</script>
                <!--<script type="text/javascript" src="js/module-eadeac-basket.js">//</script>-->
                </xsl:if>
				<script type="text/javascript" src="i18n/module-eadeac-js.js">//</script>
				<script type="text/javascript">
        	<xsl:comment>
						<xsl:apply-templates select="." mode="cdc-results-script"/>
						<xsl:if test="sdx:parameters/sdx:parameter[@name = 'linkBack']/@value = 'true'">
							<xsl:text>
								function back2form(){
									var re = new RegExp("name=([^&amp;]+)");
									var m = re.exec(location.href);
									if (m == null || m[1] == null || m[1] == '' ) {
										//pas de nom ici, on remplace simplement 'results.html' par 'search-form.html'
										location.href = location.href.replace(/\results/g, 'search-form."');
									} else { // on a un nom, on l'utilise !
										var reurl = new RegExp("[^/]*results\.html", "g");
										location.href = location.href.replace(reurl, m[1] + '-search-form.html');
									}
								}
							</xsl:text>
						</xsl:if>
						//</xsl:comment>
				</script>

			</head>

			<body>

          <xsl:apply-templates select="." mode="pl-title" />

					<i18n:text key="results-intro.query"/>

          <!--affichage des critères de recherche-->
          <xsl:apply-templates select="sdx:parameters" mode="criteria"/>

          <!--affichage des résultats-->
          <xsl:apply-templates select="sdx:results | message" />

					<i18n:text key="results-intro.results"/>

          <div id="{$real-div-id}" class="pl-cdc-search-results"><xsl:text>&#xA0;</xsl:text></div>

		  <!-- On prépare l'appel qui laisse une trace dans les logs de statistiques -->
      <xsl:variable name="bases">
        <xsl:for-each select="/sdx:document/sdx:parameters/sdx:parameter[@name='base' and @value!='']">
          <xsl:value-of select="concat( if(position()=1) then '?' else '&amp;','base=', @value )" />
        </xsl:for-each>
      </xsl:variable>
		  <xsl:variable name="stats-url" select="concat('cocoon:/stats.xml', $bases, '&amp;name=', /sdx:document/sdx:parameters/sdx:parameter[@name='name']/@value, '&amp;nb-crit=', /sdx:document/criteres/@nb, '&amp;nb-docs=', /sdx:document/sdx:terms[@name = 'root-id']/@nb, '&amp;nb-units=', /sdx:document/sdx:results/@nb, '&amp;query=')"/>
		  <div id="pl-results-stats-info">
		  	<xsl:value-of select="$stats-url"/>
		  </div>
      </body>

    </html>
	</xsl:template>

  <!--+
      | Les scripts nécessaires à la construction de la liste des résultats
      +-->
	<xsl:template match="sdx:document" mode="cdc-results-script">
		<!-- On récupère le nombre d'unité documentaire concernée par la recherche -->
		<xsl:variable name="nbunits" select="sdx:results/@nb"/>
		<!-- On récupère le nombre -->
		<xsl:variable name="nbdocs" select="sdx:terms[@name ='root-id']/@nb"/>
		
		<!-- TODO (MP) : Définir une variable JS pour la limite de la taille du titre  -->
		<xsl:text>&#10;var activeBasket = "</xsl:text><xsl:value-of select="$active-basket" /><xsl:text>";</xsl:text>
		<!-- On va chercher la vraie valeur du paramètre: URL ou par défaut -->
		<xsl:variable name="autoExpand">
			<xsl:variable name="ae">
				<xsl:choose>
					<xsl:when test="$auto-expand != ''"><xsl:value-of select="$auto-expand"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$auto-expand-default"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Une variable pour avoir le résultat -->
			<xsl:variable name="res">
				<!-- On a la forme "docs:n;units:n" où les deux parties sont optionnelles -->
				<xsl:for-each select="fn:tokenize($ae, ';')">
					<!-- Seulement si on a un : -->
					<xsl:if test="contains(., ':')">
						<xsl:variable name="parts" select="fn:tokenize(., ':')"/>
						<!-- Seulement si la deuxième partie est un nombre -->
						<xsl:if test="number($parts[2])">
							<xsl:variable name="nb" select="number($parts[2])"/>
							<xsl:choose>
								<xsl:when test="$parts[1] = 'docs' and $nbdocs &gt;= $nb">true</xsl:when>
								<xsl:when test="$parts[1] = 'units' and $nbunits &gt;= $nb">true</xsl:when>
								<!-- Tous les autres cas on n'ouvre pas -->
							</xsl:choose>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$ae = '_all'"><xsl:value-of select="$ae"/></xsl:when>
				<xsl:when test="number($ae)"><xsl:value-of select="$ae"/></xsl:when>
				<xsl:when test="$res !=''">_all</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>var ae = "</xsl:text><xsl:value-of select="$autoExpand"/><xsl:text>";</xsl:text>
		<xsl:text>var qidcdcall = "</xsl:text><xsl:value-of select="sdx:terms[@name ='cdcall']/@id"/><xsl:text>";</xsl:text>
		<xsl:text>var qidcdc = "</xsl:text><xsl:value-of select="sdx:terms[@name ='cdc']/@id"/><xsl:text>";</xsl:text>
		<xsl:text>var qidrootid = "</xsl:text><xsl:value-of select="sdx:terms[@name ='root-id']/@id"/><xsl:text>";</xsl:text>
		<xsl:text>var oid = "</xsl:text><xsl:value-of select="sdx:results/@id"/><xsl:text>";</xsl:text>
		<xsl:text>window.addEvent("load", closure_initCdcResults('</xsl:text><xsl:value-of select="$real-div-id"/><xsl:text>'));
		var cdcResult = "";
		function closure_initCdcResults(id){
			return function(){
				cdcResult = new MainResultTreeView(id);
				cdcResult.load();
			}
		}
		</xsl:text>

	</xsl:template>

    <!--+
        | Construction du titre
        +-->
    <xsl:template match="sdx:document" mode="pl-title">

      <xsl:choose>

          <!-- Affichage d'un lien de retour vers le formulaire de recherche -->
          <xsl:when test="sdx:parameters/sdx:parameter[@name = 'linkBack']/@value = 'true'">
						<xsl:apply-templates select="." mode="linkBack"/>
          </xsl:when>

					<!-- Affichage d'un lien de retour vers la navigation dans les index-->
					<xsl:when test="sdx:parameters/sdx:parameter[@name = 'linkBack']/@value = 'navindex'">
						<xsl:apply-templates select="." mode="linkBack-navindex"/>
          </xsl:when>

          <!-- Affichage du titre seulement -->
          <xsl:otherwise>
            <h1 class="pl-title">
              <i18n:text key="results-title.{$form-id}">results-title.<xsl:value-of select="$form-id" /></i18n:text>
            </h1>
          </xsl:otherwise>

        </xsl:choose>

    </xsl:template>
		
		<!--+
      | Titre de la page de résultats et barre d'outils
			| La barre d'outils comprend :
			|    * bouton Modifier votre requête
      +-->
		<xsl:template match="sdx:document" mode="linkBack">
			<table class="pl-title pl-box" summary="Placement du titre" cellpadding="0" cellspacing="0">
				<tr>
					<td class="pl-box-title">
						<h1>
							<i18n:text key="results-title.{$form-id}">results-title.<xsl:value-of select="$form-id" /></i18n:text>
						</h1>
					</td>
					<td class="pl-box-title-button">
						<button		id="pl-results-button-back2search"
										 title="results-change-request" i18n:attr="title"
										 class="yui-button yui-push-button"
										 onclick="back2form();" onkeypress="back2form();">
									<i18n:text key="results-change-request">results-change-request</i18n:text>
						</button>
					</td>
				</tr>
			</table>
		</xsl:template>
		
		<!--+
      | Titre de la page de résultats d'un navindex et barre d'outils
			| La barre d'outils comprend :
			|    * bouton Retour
      +-->
		<xsl:template match="sdx:document" mode="linkBack-navindex">
			<table class="pl-title pl-box" summary="Placement du titre" cellpadding="0" cellspacing="0">
				<tr>
					<td class="pl-box-title">
						<h1>
							<i18n:text key="results-title">results-title</i18n:text>
						</h1>
					</td>
					<td class="pl-box-title-button">
						<button		id="pl-results-button-back2search"
										 title="results-change-request" i18n:attr="title"
										 class="yui-button yui-push-button"
										 onclick="history.back();" onkeypress="history.back();">
									<i18n:text key="navindex-linkback">navindex-linkback</i18n:text>
						</button>
					</td>
				</tr>
			</table>
		</xsl:template>

  <!--+
      | Résumé de la requête : affichage des critères utilisés
      +-->
	<xsl:template match="sdx:parameters" mode="criteria">

		<div id="pl-results-criteria">

			<span class="pl-results-criteria-intro">
				<i18n:text key="results-your-request">results-your-request</i18n:text>
			</span>

			<xsl:variable name="criteria">
				<xsl:apply-templates select="sdx:parameter[starts-with(@name, 'champ')] " mode="criteria">
					<xsl:sort select="number(substring-after( @name, 'champ'  ))"/>
					<xsl:with-param name="current_form-id" select="$form-id"/>
				</xsl:apply-templates>
			</xsl:variable>

<!--<xsl:message>CRITERIA
<xsl:copy-of select="$criteria" /></xsl:message>-->

			<!-- On sort un span pour intercepter ultérieurement (statistiques) -->
			<span id="pl-results-query-text">

				<xsl:choose>

					<xsl:when test="$criteria/pleade:criterion">
						<xsl:apply-templates select="$criteria/pleade:criterion" mode="criteria"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:text>Aucun critère de recherche</xsl:text> <!--FIXME-->
					</xsl:otherwise>
				</xsl:choose>

			</span>

		</div>

	</xsl:template>

  <!--+
      | Traitement des critères de recherche
      | La sortie est utilisée pour l'affichage du résumé de la requête
      +-->
	<xsl:template match="sdx:parameter[starts-with(@name, 'champ')]" mode="criteria">
		<xsl:param name="current_form-id"/>

		<xsl:variable name="i" select="substring-after( @name, 'champ' )"/>
		<xsl:variable name="cop" select="../sdx:parameter[@name = concat('cop', $i )]/@value"/>
		<xsl:variable name="champ" select="@value"/>

    <xsl:variable name="params" select="../sdx:parameter[@value!='']
      [@name=concat('query', $i) or @name=concat('date', $i) or @name=concat('from', $i) or @name=concat('to', $i) or @name=concat('du', $i) or @name=concat('db', $i) or @name=concat('de', $i)]" />

		<xsl:variable name="query">
			<xsl:for-each select="$params">
				<xsl:value-of select="@value"/>
				<xsl:if test="not(position() = last())">
					<xsl:value-of select="$list-sep"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>

    <!-- Pas la peine d'aller plus loin si le critère n'a pas été utilisé -->
    <xsl:if test="$query!=''">
      <xsl:variable name="op" select="../sdx:parameter[@name = concat('op', $i )]/@value"/>

      <xsl:variable name="query-formated">
        <xsl:choose>
          <xsl:when test="contains($champ, 'id_')">
            <!--cas d'un champs de date avec intervalle-->
            <xsl:variable name="du" select="../sdx:parameter[@name = concat ('du', $i)]/@value"/>
            <xsl:variable name="db" select="../sdx:parameter[@name = concat ('db', $i)]/@value"/>
            <xsl:variable name="de" select="../sdx:parameter[@name = concat ('de', $i)]/@value"/>
            <xsl:choose>
              <xsl:when test="$du != ''">
                <!--exact-date-->
                <i18n:text key="form.date-label.du.search-form.{$current_form-id}">
                  form.date-label.du.search-form.<xsl:value-of select="$current_form-id"/>
                </i18n:text>
                <xsl:value-of select="concat($nbsp,$du)"/>
              </xsl:when>
              <xsl:when test="$db != '' or $de !=''">
                <!--intevalle-->
                <i18n:text key="form.date-label.db.search-form.{$current_form-id}">
                  form.date-label.db.search-form.<xsl:value-of select="$current_form-id"/>
                </i18n:text>
                <xsl:value-of select="concat($nbsp, $db, $nbsp)"/>
                <i18n:text key="form.date-label.de.search-form.{$current_form-id}">
                  form.date-label.de.search-form.<xsl:value-of select="$current_form-id"/>
                </i18n:text>
                <xsl:value-of select="concat($nbsp,$de)"/>
              </xsl:when>
            </xsl:choose>
          </xsl:when>
          <!-- Date exacte "classique" -->
          <xsl:when test="$params[@name=concat('date',$i)]">

            <xsl:variable name="date" select="$params[@name = concat ('date', $i)]/@value"/>

            <xsl:if test="$date != ''">
              <i18n:text key="form.date-label.du.search-form.{$current_form-id}">
                form.date-label.du.search-form.<xsl:value-of select="$current_form-id"/>
              </i18n:text>
              <xsl:value-of select="concat($nbsp,$date)"/>
            </xsl:if>

          </xsl:when>

          <!-- Intervalle date "classique" -->
          <xsl:when test="$params[@name=concat('from',$i) or @name=concat('to',$i)]">

            <xsl:variable name="from" select="$params[@name = concat ('from', $i)]/@value"/>
            <xsl:variable name="to" select="$params[@name = concat ('to', $i)]/@value"/>

            <xsl:choose>

              <xsl:when test="$from != '' or $to !=''">
                <!--intevalle-->
                <i18n:text key="form.date-label.db.search-form.{$current_form-id}">
                  form.date-label.db.search-form.<xsl:value-of select="$current_form-id"/>
                </i18n:text>
                <xsl:value-of select="concat($nbsp, $from, $nbsp)"/>
                <i18n:text key="form.date-label.de.search-form.{$current_form-id}">
                  form.date-label.de.search-form.<xsl:value-of select="$current_form-id"/>
                </i18n:text>
                <xsl:value-of select="concat($nbsp,$to)"/>
              </xsl:when>

            </xsl:choose>

          </xsl:when>
          <xsl:when test="$champ = 'cdcall'">
            <xsl:variable name="cdc-url" select="'cocoon://functions/ead/cdc.xml'"/>
            <xsl:choose>
              <xsl:when test="doc-available($cdc-url)">
                <xsl:for-each select="tokenize($query, $list-sep)">
                  <xsl:value-of select="document($cdc-url)/ead//c[@pleade:id = current() ]/did/unittitle"/>
                  <xsl:if test="position() != last()">
                    <xsl:value-of select="$list-sep"/>
                  </xsl:if>
                </xsl:for-each>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$query"/>
                <xsl:message>[cdc-results.xsl] le document <xsl:value-of select="$cdc-url"/> n'est pas accessible</xsl:message>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:when>

					<!-- Cas d'une rubrique -->
					<xsl:when test="$champ = 'subsetall'">
						<xsl:variable name="subsets-list" select="'cocoon://functions/get-subset-hierarchy.xml'"/>
						<xsl:choose>
							<xsl:when test="doc-available($subsets-list)">
								<xsl:for-each select="tokenize($query, $list-sep)">
									<xsl:choose>
										<xsl:when test="document($subsets-list)/subsets/subset[@id = current()]/name[@xml:lang = $locale or not(@xml:lang)]">
											<xsl:value-of select="document($subsets-list)/subsets/subset[@id = current()]/name[@xml:lang = $locale or not(@xml:lang)]"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:if test="position() != last()">
                    <xsl:value-of select="$list-sep"/>
                  </xsl:if>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$query"/>
								<xsl:message>[cdc-results] le document <xsl:value-of select="$subsets-list"/> n'est pas accessible</xsl:message>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>

					<!--cas d'un champ avec @display-field-->
					<xsl:when test="../sdx:parameter[@name = concat ('displayfield_', $i)]/@value != ''">
						<xsl:variable name="display-field" select="../sdx:parameter[@name = concat ('displayfield_', $i)]/@value"/>
						<!--on doit aller chercher la valeur sur champs correpondand via une fieldQuery-->
						<xsl:for-each select="tokenize($query, $list-sep)">
							<xsl:variable name="fieldQuery-url" select="concat('cocoon:/functions/sdx-fieldQuery.xml?field=', $champ , '&amp;value=', current(), '&amp;hpp=1&amp;cache-cocoon=true')"/>
							<!--on active la cache cocoon sur ce résultat pour alleger le traitement-->
							<!--DEBUG -->
							<!--<xsl:message>fieldQuery-url = <xsl:value-of select="$fieldQuery-url"/></xsl:message>-->
							<xsl:choose>
								<xsl:when test="doc-available($fieldQuery-url)">
									<xsl:value-of select="document($fieldQuery-url)/sdx:document/sdx:results/sdx:result[1]/sdx:field[@name=$display-field]/@value"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:message>[pleade3][search.xsl] erreur document('<xsl:value-of select="$fieldQuery-url"/>')</xsl:message>
									<xsl:value-of select="current()" />
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="position() != last()">
								<xsl:value-of select="$list-sep"/>
							</xsl:if>
            			</xsl:for-each>
					</xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$query"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

<!-- <xsl:message>[cdc-result.xsl]
              i = <xsl:value-of select="$i"/>
            cop = <xsl:value-of select="$cop"/>
          champ = <xsl:value-of select="$champ"/>
          query = <xsl:value-of select="$query"/>
             op = <xsl:value-of select="$op"/>
query-formated  = <xsl:copy-of select="$query-formated"/>
</xsl:message> -->

      <xsl:if test="$query-formated != '' and not(../sdx:parameter[@name = concat ('hidden', $i)]/@value = 'true') and $i > $n-start">

        <pleade:criterion>

          <pleade:field>

            <xsl:choose>

              <xsl:when test="contains($champ, 'id_')">
                <!--cas d'un champs de date avec intervalle-->
                <i18n:text key="form.date-label.id.{substring-after( $champ, '_' )}.search-form.{$current_form-id}">
                  form.date-label.id.<xsl:value-of select="substring-after( $champ, '_' )"/>.search-form.<xsl:value-of select="$current_form-id"/>
                </i18n:text>
              </xsl:when>

              <xsl:otherwise>
                <i18n:text key="form.field.{$champ}.search-form.{$current_form-id}">form.field.<xsl:value-of select="$champ"/>.search-form.<xsl:value-of select="$current_form-id"/></i18n:text>
              </xsl:otherwise>

            </xsl:choose>

          </pleade:field>

					<xsl:if test="$champ != 'object'">
						<pleade:query>
							<xsl:copy-of select="$query-formated"/>
						</pleade:query>
					</xsl:if>

          <pleade:cop>
            <xsl:call-template name="display-operator-criteria">
              <xsl:with-param name="value" select="$cop"/>
            </xsl:call-template>
          </pleade:cop>

        </pleade:criterion>

      </xsl:if>

    </xsl:if>

	</xsl:template>

  <!--+
      | Affichage d'un critère dans le résumé de la requête
      +-->
	<xsl:template match="pleade:criterion" mode="criteria">
		<!--<pleade:criterion field="..." query="..." cop="..."/>-->
			<!--cop-->
			<xsl:if test="position() != 1">
				<xsl:value-of select="' '"/>
					<span class="pl-result-criteria-cop">
						<xsl:copy-of select="pleade:cop/*"/>
					</span>
				<xsl:value-of select="' '"/>
			</xsl:if>
			<xsl:if test="count(../pleade:criterion) > 1">
				<xsl:value-of select="concat('(', $nbsp)"/>
			</xsl:if>
			<span class="pl-result-criteria-field">
				<!--champ-->
				<xsl:copy-of select="pleade:field/*"/>
			</span>
			<xsl:if test="pleade:query/* or pleade:query/text()">
				<xsl:value-of select="concat($nbsp, ':', $nbsp)"/>
				<span class="pl-result-criteria-query">
					<!--query-->
					<xsl:copy-of select="pleade:query/* | pleade:query/text()"/>
				</span>
			</xsl:if>
			<xsl:if test="count(../pleade:criterion) > 1">
				<xsl:value-of select="concat($nbsp, ')')"/>
			</xsl:if>
	</xsl:template>

	<xsl:template name="display-operator-criteria">
		<xsl:param name="value" select="''"/>
		<i18n:text key="results-operator.{$value}">results-operator.<xsl:value-of select="$value"/></i18n:text>
	</xsl:template>

	<xsl:template match="sdx:results">
		<p class="pl-results-count">
			<xsl:value-of select="@nb"/>
			<xsl:choose>
				<xsl:when test="@nb &lt;= 1"><i18n:text key="results-in.nb.one"> résultat dans </i18n:text></xsl:when>
				<xsl:otherwise><i18n:text key="results-in.nb.many"> résultats dans </i18n:text></xsl:otherwise>
			</xsl:choose>
			<xsl:variable name="nb-ir" select="count(/sdx:document/sdx:terms[@name = 'root-id']/sdx:term)"/>
			<xsl:value-of select="$nb-ir"/>
			<xsl:choose>
				<xsl:when test="$nb-ir &lt;=1 "><i18n:text key="results.nb-ir.one"> instrument de recherche.</i18n:text></xsl:when>
				<xsl:otherwise><i18n:text key="results.nb-ir.many"> instruments de recherche.</i18n:text></xsl:otherwise>
			</xsl:choose>
		</p>
	</xsl:template>

	<xsl:template match="message[@type = 'with-dynamic-data' and @key]">
		<p class="pl-message">
			<xsl:variable name="ik" select="@key"/>
			<xsl:for-each select="fn:tokenize(normalize-space(.), ';')">
				<i18n:text key="{concat($ik, '.', position())}"><xsl:value-of select="concat($ik, '.', position())"/></i18n:text>
				<xsl:value-of select="."/>
				<xsl:if test="position() = last()">
					<i18n:text key="{concat($ik, '.end')}"><xsl:value-of select="concat($ik, '.end')"/></i18n:text>
				</xsl:if>
			</xsl:for-each>
		</p>
	</xsl:template>

	<xsl:template match="message">
		<p class="pl-message"><xsl:apply-templates /></p>
	</xsl:template>

</xsl:stylesheet>
