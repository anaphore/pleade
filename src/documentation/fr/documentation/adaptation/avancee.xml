<?xml version="1.0"?>
<?xml-stylesheet href="../../../_config/docbook.css" type="text/css"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "../../../_config/docbookx.dtd">
<article lang="fr"> 
  <title>Formulaires de recherche avancée</title>
  
  <titleabbrev>Formulaires de
	 recherche</titleabbrev> 
  <articleinfo> 
	 <author><firstname>Martin</firstname><surname>Sévigny</surname>
		
		<affiliation> 
		  <orgname>AJLSM</orgname> 
		</affiliation> 
		<email>sevigny@ajlsm.com</email> 
	 </author> 
	 <author><firstname>Florence</firstname><surname>Clavaud</surname>
		
		<affiliation> 
		  <orgname>Anaphore</orgname> 
		</affiliation> 
		<email>florence.clavaud@arkheia.net</email>
		
	 </author>
	 <abstract> 
		<para>PLEADE offre un mécanisme simple et
		  descriptif pour construire des formulaires de
		  recherche avancée. Les spécifications techniques
		  de ce mécanisme sont présentées dans ce
		  document.</para> 
	 </abstract><date
	 role="modification">2006-02-18</date> 
  </articleinfo> 
  <section> 
	 <title>Approche générale</title> 
	 <para>Dans une application SDX telle que
		PLEADE, il est possible d'offrir aux utilisateurs
		des formulaires de recherche relativement
		complexes et souples. Toutefois, pour chaque
		formulaire, il faut non seulement préciser
		comment il sera affiché, mais également comment
		il sera traité par SDX pour réellement effectuer
		la recherche souhaitée depuis les informations
		saisies ou choisies par l'utilisateur.</para> 
	 <para>Pour simplifier le travail des
		administrateurs de sites PLEADE, nous avons
		implanté un mécanisme de création de formulaires
		de recherche et de programmes de traitement basé
		sur une simple description de la structure du
		formulaire, description qui sert à la fois à
		générer un affichage et un programme de
		traitement. Ainsi, il suffit de fournir des
		informations comme le titre du formulaire, une
		introduction, un nom court, et enfin la liste des
		champs à chercher et la manière de les chercher.
		PLEADE fera ensuite le nécessaire pour que tout
		fonctionne correctement.</para> 
	 <para>Les formulaires de recherche sont
		décrits à l'aide d'un document XML qui doit se
		situer dans le dossier
		<filename
		 class="directory">pleade-local/search-forms</filename>.
		La structure de ces documents XML est précisée
		dans les sections suivantes de ce
		document.</para> 
  </section> 
  <section> 
	 <title>Interface de paramétrage</title> 
	 <warning> 
		<para>Cette interface de paramétrage est
		  disponible depuis la version 2.0 de PLEADE
		  uniquement.</para> 
	 </warning> 
	 <para>Une interface de paramétrage peut être
		utilisée pour créer et modifier des formulaires
		de recherche avancée. Elle peut également être
		utilisée pour récupérer la définition de
		formulaires existants, ou encore charger de
		telles définitions sur le serveur.</para> 
	 <para>Cette interface est décrite 
		<ulink
		 url="../../../docs/interface-formulaires-fr.pdf">dans
		  un document à part</ulink>.</para> 
  </section> 
  <section> 
	 <title>Structure de la description</title> 
	 <section> 
		<title>Informations générales</title> 
		<para>La première partie de la structure du
		  formulaire consiste à fournir des informations
		  générales qui seront utilisées pour introduire le
		  formulaire ou encore offrir des liens vers ce
		  formulaire. Voici d'abord un exemple de
		  description de formulaire de recherche, en
		  insistant sur cette première partie :</para> 
		<programlisting>
  &lt;form action="search-a.xsp" general="true" name="standard" documents="ead"&gt;
    &lt;title xml:lang="fr"&gt;Formulaire de recherche avancée&lt;/title&gt;
    &lt;title xml:lang="en"&gt;Advanced search form&lt;/title&gt;
    &lt;short-title xml:lang="fr"&gt;Général&lt;/short-title&gt;
    &lt;short-title xml:lang="en"&gt;General&lt;/short-title&gt;
    &lt;introduction xml:lang="fr"&gt;
      &lt;p&gt;Vous pouvez utiliser ce formulaire de recherche pour trouver
          des documents dans l'ensemble de la base.
      &lt;/p&gt;
    &lt;/introduction&gt;
    &lt;introduction xml:lang="en"&gt;
      &lt;p&gt;You may use this search form to query all the database.&lt;/p&gt;
    &lt;/introduction&gt;

    &lt;!-- Ici devraient suivre les champs de recherche... --&gt;
  &lt;/form&gt;
</programlisting> 
		<para>Il y a donc six éléments
		  d'information à fournir dans ces généralités, à
		  l'aide de trois attributs de l'élément
		  <sgmltag class="element">form</sgmltag> et de
		  trois sous-éléments de ce dernier.</para> 
		<variablelist> 
		  <varlistentry><term>Attribut
				<sgmltag
				class="attribute">action</sgmltag></term> 
			 <listitem> 
				<para>Cet attribut permet de préciser
				  l'URL du programme de traitement du formulaire de
				  recherche avancée. Pour l'instant,
				  <emphasis role="bold">la valeur de cet attribut
				  doit toujours être <sgmltag
				  class="attvalue">search-a.xsp</sgmltag></emphasis>.</para>
				
			 </listitem> 
		  </varlistentry> 
		  <varlistentry><term>Attribut
				<sgmltag
				class="attribute">general</sgmltag></term> 
			 <listitem> 
				<para>Cet attribut peut prendre les
				  valeurs <sgmltag class="attvalue">true</sgmltag>
				  ou <sgmltag class="attvalue">false</sgmltag>, et
				  permet de préciser si ce formulaire sera offert
				  parmi la liste des formulaires de recherche
				  avancée ou non. En effet, dans l'interface par
				  défaut de PLEADE, on retrouve en haut de page un
				  formulaire de recherche simple mais également un
				  lien vers un ou plusieurs formulaires de
				  recherche avancée. Les formulaires de recherche
				  avancée qui sont présentés dans cette liste sont
				  ceux dont l'attribut <sgmltag
				  class="attribute">general</sgmltag> a la valeur
				  <sgmltag class="attvalue">true</sgmltag>.</para> 
			 </listitem> 
		  </varlistentry> 
		  <varlistentry><term>Attribut
				<sgmltag class="attribute">name</sgmltag></term> 
			 <listitem> 
				<para>Cet attribut permet de donner
				  un identifiant au formulaire, identifiant qui
				  sera utilisé par PLEADE pour en assurer la
				  gestion, autant à l'affichage qu'au traitement.
				  <emphasis role="bold">Les attributs
				  <sgmltag class="attribute">name</sgmltag> des
				  formulaires doivent tous avoir une valeur
				  différente dans une installation
				  PLEADE</emphasis>.</para> 
			 </listitem> 
		  </varlistentry> 
		  <varlistentry><term>Attribut
				<sgmltag
				class="attribute">documents</sgmltag></term>
			 <listitem>
				<para><emphasis role="bold">Cet
				  attribut sert, dans une installation construite à
				  partir des sources de PLEADE 2 uniquement, à
				  spécifier quel corpus de documents on souhaite
				  interroger</emphasis> ; les valeurs possibles
				  sont donc <sgmltag class="attvalue">ead</sgmltag>
				  et <sgmltag class="attvalue">eac</sgmltag>.
				  </para>
			 </listitem>
		  </varlistentry>
		  <varlistentry><term>Sous-élément
				<sgmltag class="element">title</sgmltag></term> 
			 <listitem> 
				<para>Cet élément contient le titre
				  du formulaire de recherche avancée. Ce titre sera
				  présenté en début de page lorsque le formulaire
				  est affiché. Ce titre doit avoir un attribut
				  <sgmltag class="attribute">xml:lang</sgmltag>
				  pour chaque langue d'interface que vous offrez à
				  vos utilisateurs. Il peut y avoir autant de
				  langues que vous souhaitez.</para> 
			 </listitem> 
		  </varlistentry> 
		  <varlistentry><term>Sous-élément
				<sgmltag
				 class="element">short-title</sgmltag></term> 
			 <listitem> 
				<para>Cet élément contient le titre
				  court du formulaire de recherche avancée. Ce
				  titre court sera utilisé lorsqu'on offre un lien
				  vers le formulaire de recherche à partir d'un
				  menu déroulant, par exemple dans l'en-tête des
				  pages s'il s'agit d'un formulaire général. Ce
				  titre court doit avoir un attribut
				  <sgmltag class="attribute">xml:lang</sgmltag>
				  pour chaque langue d'interface que vous offrez à
				  vos utilisateurs. Il peut y avoir autant de
				  langues que vous souhaitez.</para> 
			 </listitem> 
		  </varlistentry> 
		  <varlistentry><term>Sous-élément
				<sgmltag
				class="element">introduction</sgmltag></term> 
			 <listitem> 
				<para>Cet élément contient un texte
				  d'introduction au formulaire. Ce texte
				  d'introduction est optionnel, mais s'il est
				  présent il sera présenté sous le titre lorsqu'on
				  affiche le formulaire. Le contenu de cet élément
				  doit être du texte balisé en XHTML, ce qui vous
				  permet d'enrichir le texte ou de le structurer
				  visuellement. Cette introduction doit avoir un
				  attribut <sgmltag
				  class="attribute">xml:lang</sgmltag> pour chaque
				  langue d'interface que vous offrez à vos
				  utilisateurs. Il peut y avoir autant de langues
				  que vous souhaitez.</para> 
			 </listitem> 
		  </varlistentry> 
		</variablelist> 
	 </section> 
	 <section> 
		<title>Critères de recherche</title> 
		<para>Après les informations générales, ce
		  sont les critères de recherche qui doivent être
		  spécifiés. Le mécanisme des formulaires de
		  recherche avancée dans PLEADE supporte une liste
		  <emphasis role="italic">plate</emphasis> de
		  critères de recherche, chaque critère étant relié
		  au précédent par un opérateur booléen. L'exemple
		  ci-dessous présente une description générique de
		  critères de recherche, permettant de voir toute
		  l'étendue des possibilités :</para> 
		<programlisting>
  &lt;form ...&gt;
    &lt;!-- Les informations générales ne sont pas reprises dans cet exemple --&gt;

    &lt;!-- Un critère caché --&gt;
    &lt;key type="hidden" field="[champ]" value="[valeur]" connector="[opérateur]"/&gt;

    &lt;!-- Une zone de texte avec choix ou pas du champ de recherche --&gt;
    &lt;key type="text" connector="[opérateur]" style="[instructions CSS]"&gt;

      &lt;!-- Le libellé pour le critère --&gt;
      &lt;label xml:lang="fr"&gt;Intitulé en français&lt;/label&gt;
      &lt;label xml:lang="en"&gt;English label&lt;/label&gt;

      &lt;!-- Le ou les champs de recherche --&gt;
      &lt;field name="[champ1]"&gt;
        &lt;label xml:lang="fr"&gt;Intitulé en français&lt;/label&gt;
        &lt;label xml:lang="en"&gt;English label&lt;/label&gt;
      &lt;/field&gt;
      &lt;field name="[champ2]"&gt;
        &lt;label xml:lang="fr"&gt;Intitulé en français&lt;/label&gt;
        &lt;label xml:lang="en"&gt;English label&lt;/label&gt;
      &lt;/field&gt;
      ...
    &lt;/key&gt;

    &lt;!-- Autres critères possibles définis ci-après --&gt;

  &lt;/form&gt;
</programlisting> 
		<para>L'exemple ci-dessus permet de voir,
		  de manière générale, comment sont définis les
		  critères de recherche. Chaque critère est défini
		  avec des informations qui sont soit communes à
		  tous les types de critères, soit spécifique à un
		  type de critère.</para> 
		<para>Les informations communes
		  sont  :</para> 
		<itemizedlist> 
		  <listitem> 
			 <para>L'élément
				<sgmltag class="element">key</sgmltag> représente
				toujours un critère de recherche.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">type</sgmltag> de
				l'élément <sgmltag class="element">key</sgmltag>
				permet de préciser le type de critères dont il
				s'agit, ce qui aura une influence sur l'apparence
				de ce critère et la manière de l'utiliser. Les
				valeurs possibles sont <sgmltag
				class="attvalue">text</sgmltag>,
				<sgmltag class="attvalue">list</sgmltag>,
				<sgmltag class="attvalue">hidden</sgmltag>,
				<sgmltag class="attvalue">date</sgmltag> et
				<sgmltag class="attvalue">boolean</sgmltag>. Des
				précisions sur chaque type de critères sont
				apportées ci-dessous.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">connector</sgmltag> de
				l'élément <sgmltag class="element">key</sgmltag>
				permet d'indiquer quels opérateurs booléens
				affecter à ce critère pour le relier au
				précédent. Les valeurs possibles sont 1 (ET), 2
				(OU), 3 (ET/OU), 4 (SAUF).</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">style</sgmltag> de
				l'élément <sgmltag class="element">key</sgmltag>
				permet de fournir des spécifications CSS qui
				seront copiées dans l'élément HTML correspondant
				au critère. Cela est utile pour indquer la
				largeur souhaitée d'une zone de texte par
				exemple.</para> 
		  </listitem> 
		  <listitem> 
			 <para>Le sous-élément
				<sgmltag class="element">label</sgmltag>,
				qualifié par un code de langue, permet de
				spécifier l'étiquette à associer au champ, et ce
				dans les différentes langues d'interface.</para> 
		  </listitem> 
		</itemizedlist> 
	 </section> 
  </section> 
  <section> 
	 <title>Présentation des différents critères
		de recherche</title> 
	 <para>Chaque type de critère de recherche
		partage des éléments communs spécifiés dans la
		section précédente. Ici, nous allons les
		reprendre un par un et indiquer les
		spécifications complémentaires propres à chaque
		type, ainsi qu'un exemple complet d'un critère de
		ce type.</para> 
	 <section> 
		<title><sgmltag
		  class="attvalue">text</sgmltag> = zone de
		  texte</title> 
		<para>Ce type de critère permet d'afficher
		  une zone de texte dans laquelle l'utilisateur
		  pourra saisir des mots ou expressions à
		  rechercher. Cela s'apparente à la recherche site
		  <emphasis role="italic">simple</emphasis>. La
		  recherche peut s'effectuer dans un champ
		  prédéfini, ou encore elle peut s'effectuer dans
		  un champ spécifié par l'utilisateur à partir
		  d'une liste.</para> 
		<para>Voici un exemple complet lorsqu'il y
		  a un champ de recherche prédéfini :</para> 
		<programlisting>

&lt;!-- Une zone de texte champ de recherche prédéfini --&gt;
&lt;key type="text" connector="3" style="width: 40em;" field="fulltext"&gt;

  &lt;!-- Le libellé pour le critère --&gt;
  &lt;label xml:lang="fr"&gt;Recherche plein texte :&lt;/label&gt;
  &lt;label xml:lang="en"&gt;Full text search&lt;/label&gt;

&lt;/key&gt;

</programlisting> 
		<para>Dans cet exemple, cette nouvelle
		  spécification est introduite  :</para> 
		<itemizedlist> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">field</sgmltag> de
				l'élément <sgmltag class="element">key</sgmltag>
				indique le champ de recherche prédéfini.</para> 
		  </listitem> 
		</itemizedlist> 
		<para>Un second exemple permet de
		  comprendre comment se construit un critère de
		  recherche où l'utilisateur peut indiquer dans
		  quel champ il veut effectuer sa recherche, depuis
		  une liste déroulante :</para> 
		<programlisting>

&lt;!-- Une zone de texte avec choix du champ de recherche --&gt;
&lt;key type="text" connector="3" style="width: 40em;"&gt;

  &lt;!-- Les champs de recherche --&gt;
  &lt;field name="fulltext"&gt;
    &lt;label xml:lang="fr"&gt;Texte intégral&lt;/label&gt;
    &lt;label xml:lang="en"&gt;Full text&lt;/label&gt;
  &lt;/field&gt;
  &lt;field name="scopecontent" default="true"&gt;
    &lt;label xml:lang="fr"&gt;Description du contenu&lt;/label&gt;
    &lt;label xml:lang="en"&gt;Scope content&lt;/label&gt;
  &lt;/field&gt;
  &lt;field name="unittitle"&gt;
    &lt;label xml:lang="fr"&gt;Intitulés seulement&lt;/label&gt;
    &lt;label xml:lang="en"&gt;Unit titles only&lt;/label&gt;
  &lt;/field&gt;
  ...
&lt;/key&gt;


</programlisting> 
		<para>Dans cet exemple, ces nouvelles
		  spécifications sont introduites  :</para> 
		<itemizedlist> 
		  <listitem> 
			 <para>Le sous-élément
				<sgmltag class="element">field</sgmltag> indique
				un champ de recherche qui fera partie de la liste
				présentée à l'utilisateur.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">name</sgmltag> de
				l'élément <sgmltag
				class="element">field</sgmltag> indique le champ
				de recherche.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">default</sgmltag> de
				l'élément <sgmltag
				class="element">field</sgmltag>, lorsqu'il a pour
				valeur <emphasis>true</emphasis>, permet
				d'indiquer le champ sur lequel sera positionné le
				menu déroulant.</para> 
		  </listitem> 
		  <listitem> 
			 <para>Le sous-élément
				<sgmltag class="element">label</sgmltag> de
				l'élément <sgmltag
				class="element">field</sgmltag> indique le
				l'étiquette du champ dans la liste présentée à
				l'utilisateur.</para> 
		  </listitem> 
		</itemizedlist> 
		<para>Ce type de critère peut être utilisé
		  à la fois pour une recherche dans un champ de
		  type <emphasis role="italic">mot</emphasis> (type
		  <emphasis role="italic">word</emphasis> dans la
		  terminologie SDX) ou pour une recherche dans un
		  champ de type <emphasis
		  role="italic">champ</emphasis> (type
		  <emphasis role="italic">field</emphasis> dans la
		  terminologie SDX). PLEADE saura reconnaître la
		  nature du champ et fabriquera la bonne
		  requête.</para> 
	 </section> 
	 <section> 
		<title><sgmltag
		  class="attvalue">list</sgmltag> = liste
		  déroulante</title> 
		<para>Ce type de critère permet de montrer
		  à l'utilisateur une liste déroulante contenant
		  toutes les valeurs d'un champ, ou une partie des
		  valeurs d'un champ selon certains critères de
		  filtre. Différentes variantes de listes sont
		  possibles, et l'exemple suivant présente toutes
		  les valeurs possibles de tous les paramètres,
		  même si toutes les combinaisons ne sont pas
		  utiles.</para> 
		<programlisting>

&lt;key
     type="list"
     connector="3"
     field="fpersname"
     variant="combo|fixed|radio|checkbox"
     display="vertical|horizontal"
     placement="right|left"
     rows="10"
     selection="single|multiple|double-list"
&gt;
  &lt;label xml:lang="fr"&gt;Nom de personne&lt;/label&gt;
  &lt;label xml:lang="en"&gt;Personal name&lt;/label&gt;
  &lt;no-choice xml:lang="fr"&gt;-- Sélectionner un nom --&lt;/no-choice&gt;
  &lt;no-choice xml:lang="en"&gt;-- Please select a name --&lt;/no-choice&gt;
  &lt;items&gt;
    &lt;item value="Bonaparte, Napoléon"&gt;
      &lt;label xml:lang="fr"&gt;Napoléon Bonaparte&lt;/label&gt;
      &lt;label xml:lang="en"&gt;Bonaparte&lt;/label&gt;
    &lt;/item&gt;
    &lt;item value="De Gaulle, Charles"&gt;
      &lt;label xml:lang="fr"&gt;Charles de Gaulle&lt;/label&gt;
      &lt;label xml:lang="en"&gt;CDG&lt;/label&gt;
    &lt;/item&gt;
  &lt;/items&gt;
&lt;/key&gt;

</programlisting> 
		<para>Ces nouvelles spécifications sont
		  introduites dans l'exemple :</para> 
		<itemizedlist> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">field</sgmltag> permet
				de préciser le champ de recherche.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">variant</sgmltag>
				permet de spécifier la variante de liste, et les
				valeurs possibles sont  :</para> 
			 <itemizedlist> 
				<listitem> 
				  <para><sgmltag
					 class="attvalue">combo</sgmltag> : une liste de
					 type <emphasis role="italic">combo
					 box</emphasis>, soit une liste déroulante dont
					 seule l'entrée sélectionnée est visible.</para> 
				</listitem> 
				<listitem> 
				  <para><sgmltag
					 class="attvalue">fixed</sgmltag> : une liste avec
					 un nombre fixe d'entrées qui apparaisent à
					 l'écran.</para> 
				</listitem> 
				<listitem> 
				  <para><sgmltag
					 class="attvalue">radio</sgmltag> : une liste où
					 chaque entrée peut être sélectionnée à l'aide
					 d'un bouton de radion, ce qui permet la sélection
					 d'une seule valeur.</para> 
				</listitem> 
				<listitem> 
				  <para><sgmltag
					 class="attvalue">checkbox</sgmltag> : une liste
					 où chaque entrée peut être sélectionnée à l'aide
					 d'une case à cocher, ce qui permet la sélection
					 de plusieurs valeurs.</para> 
				</listitem> 
			 </itemizedlist> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">display</sgmltag>
				permet d'indiquer plus précisément le type
				d'affichage ; il est pertinent seulement pour les
				variantes <sgmltag
				class="attvalue">checkbox</sgmltag> et
				<sgmltag class="attvalue">radio</sgmltag>, et il
				peut prendre les valeurs
				<sgmltag class="attvalue">horizontal</sgmltag>
				pour obtenir des valeurs qui s'affichent les unes
				à côté des autres (pour les petites listes...) ou
				<sgmltag class="attvalue">vertical</sgmltag> pour
				obtenir des valeurs qui s'affichent les unes
				au-dessus des autres. Cet élément est optionnel
				et sa valeur par défaut est
				<sgmltag
				 class="attvalue">vertical</sgmltag>.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">placement</sgmltag>
				permet de déterminer si on veut placer les cases
				à cocher ou les boutons de radion à gauche de la
				valeur (<sgmltag class="attvalue">left</sgmltag>)
				ou à droite de la valeur (<sgmltag
				class="attvalue">right</sgmltag>). Ce paramètre
				est pertinent seulement pour ces deux types de
				liste, et la valeur par défaut est
				<sgmltag class="attribute">left</sgmltag>.</para>
			 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">rows</sgmltag> permet
				d'indiquer le nombre de lignes visibles dans la
				liste. Il est optionnel et sa valeur par défaut
				est déterminée par le navigateur Web. Ce
				paramètre est pertinent seulement pour les listes
				de type <sgmltag class="attvalue">combo</sgmltag>
				ou <sgmltag
				class="attvalue">fixed</sgmltag>.</para> 
		  </listitem> 
		  <listitem> 
			 <para>L'attribut
				<sgmltag class="attribute">selection</sgmltag>
				permet d'indiquer si on peut sélectionner une
				seule valeur (<sgmltag
				class="attvalue">single</sgmltag>) ou plusieurs
				valeurs <sgmltag
				class="attvalue">multiple</sgmltag>). Cet
				attribut est optionnel et la valeur par défaut
				est <sgmltag class="attvalue">single</sgmltag>.
				Veuillez noter également que la valeur
				<sgmltag class="attvalue">multiple</sgmltag>
				n'est pas possible avec une liste de type
				<emphasis role="italic">combo</emphasis> et elle
				sera donc ignorée dans ce cas. On peut également
				utiliser la valeur <sgmltag
				class="attvalue">double-list</sgmltag> pour
				indiquer que les sélections multiples sont
				possibles et que les valeurs sélectionnées seront
				présentées dans une seconde liste, présentée à
				droite de la liste complète des valeurs. Cette
				méthode est pratique pour permettre aux
				utilisateurs de voir toutes les valeurs
				sélectionnées sans avoir à dérouler la liste
				principale de l'ensemble des valeurs.</para> 
		  </listitem> 
		  <listitem> 
			 <para>Le sous-élément
				<sgmltag class="element">label</sgmltag> permet
				d'indiquer le libellé qui présente le champ, dans
				toutes les langues souhaitées. A noter que ce
				sous-élément est optionnel, et s'il n'est pas
				présent, aucun espace ne sera alloué au libellé.
				Ceci est pratique lorsque, par exemple, la ligne
				visible d'une liste (voir ci-après) indique déjà
				clairement ce que contient la liste.</para> 
		  </listitem> 
		  <listitem> 
			 <para>Le sous-élément
				<sgmltag class="element">no-choice</sgmltag>
				permet d'indiquer le texte qui sera utilisé comme
				première entrée de la liste, pour indiquer
				<emphasis role="italic">aucune
				sélection</emphasis> ou pour inviter
				l'utilisateur à sélectionner une valeur.</para> 
		  </listitem> 
		  <listitem> 
			 <para>Le sous-élément
				<sgmltag class="element">items</sgmltag> permet
				d'indiquer les items à insérer dans la liste. Le
				sous-élément <sgmltag
				class="element">items</sgmltag> permet donc de
				définir une liste avec des valeurs
				prédéterminées, par exemple lorsqu'on veut
				seulement proposer certaines options de recherche
				et pas nécessairement toutes celles qui sont dans
				un champ, où lorsqu'on veut
				<emphasis role="italic">diriger</emphasis>
				l'utilisateur dans l'expression de ses requêtes.
				L'exemple ci-dessus fournit toutes les
				informations nécessaires pour comprendre
				l'utilisation de ces listes prédéfinies : chaque
				item à présenter dans la liste est dans un
				élément <sgmltag class="element">item</sgmltag>,
				dont l'attribut <sgmltag
				class="attribute">value</sgmltag> indique la
				valeur à chercher si cet item est sélectionné par
				l'utilisateur. Ensuite, dans un élément
				<sgmltag class="element">item</sgmltag>, on
				indique le libellé de l'item (ce qui sera affiché
				à l'écran), dans toutes les langues souhaitées, à
				l'aide des éléments <sgmltag
				class="element">label</sgmltag>. L'ordre de
				présentation des items de la liste sera l'ordre
				de leur présence dans le fichier qui spécifie le
				formulaire.</para> 
			 <para>En résumé, s'il y a un
				sous-élément <sgmltag
				class="element">items</sgmltag>, ce sont ces
				items qui sont offerts dans la liste, sinon ce
				sont les valeurs contenues dans le champ
				identifié par l'attribut
				<sgmltag
				class="attribute">field</sgmltag>.</para> 
		  </listitem> 
		</itemizedlist> 
		<section> 
		  <title>Filtrer une liste</title> 
		  <para>Parfois, ce ne sont pas toutes les
			 valeurs d'un champ que l'on veut voir dans la
			 liste, mais seulement celles qui correspondent à
			 certains critères. Par exemple, si on a un
			 formulaire de recherche que l'on veut utiliser
			 seulement pour les unités documentaires qui ont
			 une ou des images associées, une liste de valeurs
			 de l'élément <sgmltag
			 class="element">persname</sgmltag> (par exemple)
			 ne serait pas intéressante car elle présenterait
			 toutes les valeurs, pas seulement celles qui
			 correspondent à des unités documentaires
			 illustrées.</para> 
		  <para>C'est pourquoi il est possible de
			 <emphasis role="italic">filtrer</emphasis> les
			 listes de valeurs en fonction d'un certain nombre
			 de critères de filtres. Pour ce faire, il suffit
			 d'ajouter un élément <sgmltag
			 class="element">filters</sgmltag> à l'intérieur
			 de l'élément <sgmltag
			 class="element">key</sgmltag> spécifiant un
			 critère de type liste, comme dans l'exemple
			 (incomplet quant à la définition de la liste
			 elle-même) suivant :</para> 
		  <programlisting>

&lt;key type="list" ...&gt;
  ...
  &lt;filters&gt;
    &lt;filter field="object" value="1"/&gt;
    &lt;filter field="unitid" value="400AP*"/&gt;
  &lt;/filters&gt;
&lt;/key&gt;

</programlisting> 
		  <para>Dans cet exemple, deux filtres sont
			 définis, un indiquant que le champ
			 <emphasis role="italic">object</emphasis> doit
			 avoir la valeur 1 (présence d'une illustration
			 dans l'unité documentaire) ET que la cote doit
			 débuter par <emphasis
			 role="italic">400AP</emphasis>. On peut donc
			 mettre plusieurs filtres, et ceux-ci sont reliés
			 nécessairement par un ET booléen. Un filtre doit
			 nécessairement être composé d'un champ et d'une
			 valeur.</para> 
		</section> 
		<section> 
		  <title>Filtres implicites</title> 
		  <para>Lorsque vous associez un formulaire
			 de recherche à un document EAD, lorsqu'un
			 utilisateur affiche ce formulaire depuis la
			 fenêtre de consultation de ce document, un filtre
			 implicite sera ajouté pour rechercher uniquement
			 dans les unités documentaires de ce document.
			 Mais aussi, les éventuelles listes dans le
			 formulaire seront automatiquement filtrées en
			 fonction de ce critère (appartenance au document
			 EAD), en plus de tout autre filtre que vous avez
			 explicitement déclaré.</para> 
		  <para>C'est pourquoi il est inutile de
			 définir des filtres pour un document EAD dans un
			 formulaire, car PLEADE le fera pour vous. Par
			 conséquent, il est tout à fait possible
			 d'utiliser un même formulaire de recherche pour
			 plusieurs documents EAD, sans modifier les
			 spécifications de ce formulaire. C'est le
			 contexte d'appel qui permettra à PLEADE de
			 déterminer quel filtre implicite utiliser.</para>
		  
		  <para>Les filtres implicites sont
			 également utilisés par PLEADE lorsqu'un
			 formulaire de recherche est associé à un
			 sous-ensemble de documents, une rubrique du
			 système d'organisations. Ainsi, lorsqu'un
			 utilisateur passe par cette rubrique et qu'il
			 voit un formulaire de recherche, un filtre
			 implicite sera ajouté pour que les unités
			 retrouvées proviennent toujours de documents
			 appartenant à cette rubrique.</para> 
		</section> 
	 </section> 
	 <section> 
		<title><sgmltag
		  class="attvalue">boolean</sgmltag> = critère
		  booléen</title> 
		<para>Un critère booléen permet à
		  l'utilisateur de chercher aisément dans un champ
		  une valeur prédéterminée, ou encore de ne pas
		  inclure ce critère de recherche. Il se présente
		  toujours avec une case à cocher et un libellé. Si
		  la case à cocher, le critère fera partie de la
		  requête. Voici un exemple complet de critère
		  booléen  :</para> 
		<programlisting>

&lt;key
     type="boolean"
     connector="3"
     field="object"
     value-on="1"
     default="on|off"
     placement="right|left"
&gt;
  &lt;label xml:lang="fr"&gt;Unités illustrées seulement&lt;/label&gt;
  &lt;label xml:lang="en"&gt;Only illustrated units&lt;/label&gt;
&lt;/key&gt;


</programlisting> 
		<para>Voici la signification des différents
		  attributs : </para> 
		<itemizedlist> 
		  <listitem> 
			 <para><sgmltag
				class="attribute">field</sgmltag> indique le
				champ de recherche.</para> 
		  </listitem> 
		  <listitem> 
			 <para><sgmltag
				class="attribute">value-on</sgmltag> indique la
				valeur à rechercher dans le champ, si la case est
				cochée par l'utilisateur.</para> 
		  </listitem> 
		  <listitem> 
			 <para><sgmltag
				class="attribute">default</sgmltag> indique si la
				case à cocher doit être présentée cochée
				(<sgmltag class="attvalue">on</sgmltag>) ou pas
				(<sgmltag class="attvalue">off</sgmltag>) lorsque
				le formulaire est affiché. Cet attribut est
				optionnel et la valeur par défaut est
				<sgmltag class="attvalue">off</sgmltag>.</para> 
		  </listitem> 
		  <listitem> 
			 <para><sgmltag
				class="attribute">placement</sgmltag> indique si
				la case à cocher doit être à gauche (<sgmltag
				class="attvalue">left</sgmltag>) du libellé ou à
				droite (<sgmltag
				class="attvalue">right</sgmltag>). Cet attribut
				est optionnel et la valeur par défaut est
				<sgmltag class="attvalue">left</sgmltag>.</para> 
		  </listitem> 
		</itemizedlist> 
	 </section> 
	 <section> 
		<title><sgmltag
		  class="attvalue">hidden</sgmltag> = critère
		  caché</title> 
		<para>Un critère caché est un critère de
		  recherche que l'utilisateur ne verra pas lorsque
		  le formulaire sera affiché. Cela permet de
		  prédéterminer des critères de recherche. Voici un
		  exemple complet d'utilisation des critères
		  cachés  :</para> 
		<programlisting>

&lt;key type="hidden" field="unitid" value="400AP*"/&gt;

</programlisting> 
		<para>Dans cet exemple, l'attribut
		  <sgmltag class="attribute">field</sgmltag> permet
		  d'identifier le champ de recherche alors que
		  l'attribut <sgmltag
		  class="attribute">value</sgmltag> permet
		  d'indiquer la valeur à chercher. A noter que
		  plusieurs champs cachés peuvent être placés dans
		  un formulaire. Ils sont toujours reliés aux
		  autres critères par un opérateur booléen
		  ET.</para> 
		<para>Il est important aussi de rappeler
		  que ce critère caché n'est pas totalement
		  invisible pour l'utilisateur. En effet, celui-ci
		  peut le constater d'au moins 3 façons :</para> 
		<orderedlist> 
		  <listitem> 
			 <para>S'il regarde la source de la page
				HTML où se trouve le formulaire, car il apparaît
				sous la forme d'un champ HTML caché.</para> 
		  </listitem> 
		  <listitem> 
			 <para>S'il regarde l'URL de la
				recherche effectuée avec le formulaire, car le
				nom du champ et sa valeur y figurent.</para> 
		  </listitem> 
		</orderedlist> 
	 </section> 
	 <section> 
		<title><sgmltag
		  class="attvalue">date</sgmltag> = critère de
		  date</title> 
		<para>Dans PLEADE, certains champs de
		  recherche sont de type <emphasis
		  role="italic">date</emphasis>, ce qui permet
		  notamment d'y faire des requêtes par intervalle
		  de dates. C'est pourquoi nous proposons ce
		  critère de date, qui permettra non seulement
		  d'effectuer de telles recherches, mais aussi de
		  bien gérer la validation des dates saisies ou
		  encore le nombre de critères à afficher.</para> 
		<para>Pour bien expliquer le comportement
		  de la recherche par date, nous allons prendre un
		  cas simple : une unité de description dans un
		  document EAD possède un élément
		  <sgmltag class="element">unitdate</sgmltag>
		  exprimé ainsi :</para> 
		<programlisting>

&lt;unitdate normal="1700/1750"&gt;1700 à 1750&lt;/unitdate&gt;

</programlisting> 
		<para>Devant un tel élément, PLEADE va
		  remplir deux champs de date pour cette unité
		  documentaire :</para> 
		<orderedlist> 
		  <listitem> 
			 <para>Le champ 
				<varname>bdate</varname> (<emphasis
				role="italic">begin date</emphasis>, date de
				début), aura la valeur 1700.</para> 
		  </listitem> 
		  <listitem> 
			 <para>Le champ 
				<varname>edate</varname> (<emphasis
				role="italic">end date</emphasis>, date de fin),
				aura la valeur 1750.</para> 
		  </listitem> 
		</orderedlist> 
		<para>Ensuite, on voudrait que les
		  utilisateurs puissent exprimer des requêtes sur
		  les dates telles que :</para> 
		<itemizedlist> 
		  <listitem> 
			 <para><emphasis
				role="bold">1725</emphasis> : dans ce cas cette
				unité devra être retournée</para> 
		  </listitem> 
		  <listitem> 
			 <para><emphasis
				role="bold">1760</emphasis> : dans ce cas cette
				unité ne sera bien sûr pas retournée</para> 
		  </listitem> 
		  <listitem> 
			 <para><emphasis role="bold">Entre 1650
				et 1800</emphasis> : dans ce cas cette unité sera
				retournée</para> 
		  </listitem> 
		  <listitem> 
			 <para><emphasis role="bold">Entre 1725
				et 1800</emphasis> : dans ce cas elle sera aussi
				retournée</para> 
		  </listitem> 
		</itemizedlist> 
		<para>Pour atteindre cet objectif, des
		  critères de type date pourront être inclus dans
		  les formulaires de recherche. Ces critères
		  devront nécessairement opérer sur des champs de
		  type dates, ce qui n'est pas vérifié par le
		  mécanisme de fabrication des formulaires et qui
		  est donc à la charge de l'administrateur.</para> 
		<para>Voici un exemple complet d'un critère
		  de type date :</para> 
		<programlisting>

&lt;key
     type="date"
     connector="3"
     field="bdate,edate"
     format="\d\d\d\d,\d\d\d\d"
&gt;
  &lt;label xml:lang="fr"&gt;Unités illustrées seulement&lt;/label&gt;
  &lt;label xml:lang="en"&gt;Only illustrated units&lt;/label&gt;
  &lt;label-begin xml:lang="fr"&gt;De :&lt;/label-begin&gt;
  &lt;label-begin xml:lang="en"&gt;From:&lt;/label-begin&gt;
  &lt;label-end xml:lang="fr"&gt;A :&lt;/label-end&gt;
  &lt;label-end xml:lang="en"&gt;To:&lt;/label-end&gt;
&lt;/key&gt;


</programlisting> 
		<para>Dans cet exemple, voici la
		  signification des différents attributs :</para> 
		<itemizedlist> 
		  <listitem> 
			 <para><sgmltag
				class="attribute">field</sgmltag> : spécifie le
				ou les champs de recherche. Si on veut chercher
				dans une paire de champs de recherche (comme les
				champs date de début et date de fin), on doit les
				séparer par une virgule (sans espace), et le
				champ qui signifie <emphasis role="italic">date
				de début</emphasis> doit être le premier. Si veut
				chercher dans un seule champ, alors on indique
				seulement ce champ, et la valeur de la date
				cherchée devra être exacte. Cet attribut est bien
				sûr obligatoire.</para> 
		  </listitem> 
		  <listitem> 
			 <para><sgmltag
				class="attribute">format</sgmltag> indique le
				format de date à respecter pour l'utilisateur. Ce
				format est indiqué sous la forme d'une expression
				régulière acceptée par le langage Javascript. Les
				valeurs possibles sont <emphasis
				role="italic">\d\d\d\d</emphasis> pour une année
				sur quatre chiffres, <emphasis
				role="italic">\d\d\d\d-\d\d-\d\d</emphasis> pour
				une date complète année-mois-jour, ou encore
				<emphasis role="italic">\d\d\d\d-\d\d</emphasis>
				pour une année et un mois. Cette indication sera
				utilisée pour alerter l'utilisateur que la date
				saisie n'est pas valide.</para> 
		  </listitem> 
		</itemizedlist> 
		<para>Par ailleurs, on remarque que ce
		  critère supporte un sous-élément
		  <sgmltag class="element">label</sgmltag> pour
		  indiquer l'intitulé placé devant le champ. Mais
		  on peut aussi associer des sous-éléments
		  <sgmltag class="element">label-begin</sgmltag> et
		  <sgmltag class="element">label-end</sgmltag> pour
		  indiquer respectivement le texte à placer devant
		  la zone de saisie de la date de début ou de la
		  date de fin. On peut ne pas inclure ces intitulés
		  si l'on ne souhaite pas donner
		  d'indication.</para> 
	 </section> 
  </section> 
</article> 
