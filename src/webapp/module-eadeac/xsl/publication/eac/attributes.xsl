<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!-- Ajoute des informations sur le document EAC, en particulier des attributs dans le
	namespace pleade. -->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxonf.sf.net"
  exclude-result-prefixes="saxon"
>


  <xsl:param name="debug" select="'false'"/>

	<!-- On conserve l'identifiant du document car on le réutilise souvent. -->
	<xsl:variable name="eacid">
		<xsl:call-template name="get-eacid">
			<xsl:with-param name="eacid" select="//*[local-name() = 'control']/*[local-name() = 'recordId']"/>
		</xsl:call-template>
	</xsl:variable>
	
  <!-- Template racine -->
  <xsl:template match="/">
    <xsl:message>	Traitement des attributs Pleade (etape 2 / 6) : <xsl:value-of select="date:to-string(date:new())" xmlns:date="java:java.util.Date"/></xsl:message>
    <xsl:choose>
      <xsl:when test="$debug='true'">
        <xsl:variable name="result"><xsl:apply-templates /></xsl:variable>
        <xsl:copy-of saxon:read-once="yes" select="$result"/>
        <xsl:result-document indent="yes" href="{sys:getProperty('java.io.tmpdir')}/{$eacid}/{$eacid}-2.xml"
          xmlns:sys="java:java.lang.System">
          <xsl:copy-of saxon:read-once="yes" select="$result"/>
        </xsl:result-document>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

	<!-- On intercepte tous les éléments pour ajouter leur identifiant et éventuellement d'autres informations. -->
	<xsl:template match="*">
		<xsl:copy>
			<!-- Les attributs déjà présents -->
			<xsl:apply-templates select="@*"/>
			<!-- L'identifiant -->
			<xsl:attribute name="pleade:id">
				<xsl:call-template name="get-id"/>
			</xsl:attribute>
			<!-- Le cas particulier de l'élément racine -->
			<xsl:if test="self::*[local-name()='eac-cpf']">
				<xsl:attribute name="pleade:eacid">
					<xsl:value-of select="$eacid"/>
				</xsl:attribute>
				<!-- TODO: autres informations générales ? -->
			</xsl:if>
			<!-- Ensuite le contenu -->
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- On supprime les commentaires -->
	<xsl:template match="comment()"/>
	
	<!-- Les autres contenus (texte, instructions de traitement, attributs) sont copiés -->
	<xsl:template match="text()|processing-instruction()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- Retourne l'identifiant unique d'un élément -->
	<xsl:template name="get-id">

		<!-- L'élément en question, par défaut l'élément courant -->
		<xsl:param name="element" select="."/>
		
		<xsl:choose>
			<xsl:when test="$element/self::eac-cpf">
				<!-- Pour le document EAC, on a déjà calculé l'information -->
				<xsl:value-of select="$eacid"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- D'abord l'identifiant du document -->
				<xsl:value-of select="concat($eacid, '_')"/>
				<!-- Puis celui du document -->
				<xsl:choose>
					<xsl:when test="$element/@id"><xsl:value-of select="$element/@id"/></xsl:when>
					<xsl:when test="$element/@pleade:no">e<xsl:value-of select="format-number($element/@pleade:no, '0000000')"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="generate-id($element)"/></xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Retourne le eacid, en utilisant notamment ses attributs. -->
	<xsl:template name="get-eacid">
		<xsl:param name="eacid" select="."/>
		<xsl:variable name="value" select="normalize-space($eacid)"/>
		<!-- On commente le code de la méthode avec attributs, mais elle existe
				si nécessaire. -->
		<!-- <xsl:variable name="sep" select="'-'"/>
		<xsl:if test="$eacid/@xml:id">
			<xsl:value-of select="$eacid/@xml:id"/>
			<xsl:if test="$value != ''"><xsl:value-of select="$sep"/></xsl:if>
		</xsl:if> -->
		<xsl:value-of select="$value"/>
	</xsl:template>

</xsl:stylesheet>

